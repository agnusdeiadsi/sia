<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sia'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        /*
        * Conexiones MySQL
        *
        */
        'sia' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'correos' => [
            'driver' => 'mysql',
            'host' => env('CORREOS_HOST', '127.0.0.1'),
            'port' => env('CORREOS_PORT', '3306'),
            'database' => env('CORREOS_DATABASE', 'forge'),
            'username' => env('CORREOS_USERNAME', 'forge'),
            'password' => env('CORREOS_PASSWORD', ''),
            'unix_socket' => env('CORREOS_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'certificados' => [
            'driver' => 'mysql',
            'host' => env('CERTIFICADOS_HOST', '127.0.0.1'),
            'port' => env('CERTIFICADOS_PORT', '3306'),
            'database' => env('CERTIFICADOS_DATABASE', 'forge'),
            'username' => env('CERTIFICADOS_USERNAME', 'forge'),
            'password' => env('CERTIFICADOS_PASSWORD', ''),
            'unix_socket' => env('CERTIFICADOS_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'compras' => [
            'driver' => 'mysql',
            'host' => env('COMPRAS_HOST', '127.0.0.1'),
            'port' => env('COMPRAS_PORT', '3306'),
            'database' => env('COMPRAS_DATABASE', 'forge'),
            'username' => env('COMPRAS_USERNAME', 'forge'),
            'password' => env('COMPRAS_PASSWORD', ''),
            'unix_socket' => env('COMPRAS_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'erc' => [
            'driver' => 'mysql',
            'host' => env('ERC_HOST', '127.0.0.1'),
            'port' => env('ERC_PORT', '3306'),
            'database' => env('ERC_DATABASE', 'forge'),
            'username' => env('ERC_USERNAME', 'forge'),
            'password' => env('ERC_PASSWORD', ''),
            'unix_socket' => env('ERC_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'egresados' => [
            'driver' => 'mysql',
            'host' => env('EGRESADOS_HOST', '127.0.0.1'),
            'port' => env('EGRESADOS_PORT', '3306'),
            'database' => env('EGRESADOS_DATABASE', 'forge'),
            'username' => env('EGRESADOS_USERNAME', 'forge'),
            'password' => env('EGRESADOS_PASSWORD', ''),
            'unix_socket' => env('EGRESADOS_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'encuesta' => [
            'driver' => 'mysql',
            'host' => env('ENCUESTA_HOST', '127.0.0.1'),
            'port' => env('ENCUESTA_PORT', '3306'),
            'database' => env('ENCUESTA_DATABASE', 'forge'),
            'username' => env('ENCUESTA_USERNAME', 'forge'),
            'password' => env('ENCUESTA_PASSWORD', ''),
            'unix_socket' => env('ENCUESTA_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'cancelaciones' => [
            'driver' => 'mysql',
            'host' => env('CANCELACIONES_HOST', '127.0.0.1'),
            'port' => env('CANCELACIONES_PORT', '3306'),
            'database' => env('CANCELACIONES_DATABASE', 'forge'),
            'username' => env('CANCELACIONES_USERNAME', 'forge'),
            'password' => env('CANCELACIONES_PASSWORD', ''),
            'unix_socket' => env('CANCELACIONES_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'encuesta' => [
           'driver' => 'mysql',
           'host' => env('ENCUESTA_HOST', '127.0.0.1'),
           'port' => env('ENCUESTA_PORT', '3306'),
           'database' => env('ENCUESTA_DATABASE', 'forge'),
           'username' => env('ENCUESTA_USERNAME', 'forge'),
           'password' => env('ENCUESTA_PASSWORD', ''),
           'unix_socket' => env('ENCUESTA_SOCKET', ''),
           'charset' => 'utf8mb4',
           'collation' => 'utf8mb4_unicode_ci',
           'prefix' => '',
           'strict' => true,
           'engine' => null,
       ],

        'solicitudes' => [
           'driver' => 'mysql',
           'host' => env('SOLICITUDES_HOST', '127.0.0.1'),
           'port' => env('SOLICITUDES_PORT', '3306'),
           'database' => env('SOLICITUDES_DATABASE', 'forge'),
           'username' => env('SOLICITUDES_USERNAME', 'forge'),
           'password' => env('SOLICITUDES_PASSWORD', ''),
           'unix_socket' => env('SOLICITUDES_SOCKET', ''),
           'charset' => 'utf8mb4',
           'collation' => 'utf8mb4_unicode_ci',
           'prefix' => '',
           'strict' => false,
           'engine' => null,
       ],

        /*
        * Conexion PostgreSQL
        *
        */
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        /*
        * Conexiones Oracle
        *
        */
        'banner' => [
            'driver'        => 'oracle',
            'tns'           => env('BANNER_TNS', ''),
            'host'          => env('BANNER_HOST', '186.28.232.16'),
            'port'          => env('BANNER_PORT', '1521'),
            'database'      => env('BANNER_DATABASE', 'uccprod'),
            'username'      => env('BANNER_USERNAME', 'informes'),
            'password'      => env('BANNER_PASSWORD', 'LuM3N_1420'),
            'charset'       => env('BANNER_CHARSET', 'AL32UTF8'),
            'prefix'        => env('BANNER_PREFIX', ''),
            'prefix_schema' => env('BANNER_SCHEMA_PREFIX', ''),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];

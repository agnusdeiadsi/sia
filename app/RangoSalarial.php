<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RangoSalarial extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'rangosalario_id';
    protected $table = "sia_rangos_salarios";
    protected $fillable = ['rangosalario_id', 'rangosalario_codigo', 'rangosalario_nombre', 'rangosalario_descripcion'];
    public $timestamps = false;

    public function laboral()
	{
	    return $this->belongsTo('App\Model\Egresados\Laboral', 'nivelcargo_id');
	}
	
}

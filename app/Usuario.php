<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'id';
    protected $table = "sia_usuarios";
    protected $fillable = ['id', 'email', 'password', 'rol_sia', 'rol_modulo', 'created_at', 'updated_at', 'remember_token'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    public $incrementing = false;
    

    public function cuenta() // relacion de uno a uno con la tabla correos_cuentas
    {
    	return $this->belongsTo('App\Model\Correos\Cuenta', 'id', 'cuenta_id'); // un usuario pertenece a una cuenta
    }

    public function permisos()
    {
        return $this->belongsToMany('App\Sistemarol', 'sia_permisos', 'usuario_id', 'sistemarol_id');
    }

    public function enrolamientos()
    {
        return $this->belongsToMany('App\Sistemarol', 'sia_permisos', 'usuario_id', 'sistemarol_id');
    }

    public function autorizacionesAreas()
    {
        return $this->belongsToMany('App\Area', 'app_compras.compras_autorizaciones', 'usuario_id_area', 'area_id');
    }

    public function autorizacionesSubareas()
    {
        return $this->belongsToMany('App\Subarea', 'app_compras.compras_autorizaciones', 'usuario_id_subarea', 'subarea_id');
    }

    public function revisionesAreas()
    {
        return $this->belongsToMany('App\Area', 'app_compras.compras_revisiones', 'usuario_id_area', 'area_id');
    }

    public function revisionesSubareas()
    {
        return $this->belongsToMany('App\Subarea', 'app_compras.compras_revisiones', 'usuario_id_subarea', 'subarea_id');
    }

    public function solicitudes()
    {
        return $this->hasMany('App\Model\Compras\Solicitud', 'usuario_id');
    }

    public function Comentarios()
    {
        return $this->hasMany('App\Model\Compras\Comentario', 'usuario_id');
    }

    public function recibos()
    {
        return $this->hasMany('App\Model\Erc\Recibo', 'usuario_id');
    }

    public function entrances()
    {
      return $this->hasMany('App\Entrance', 'usuario_id');
    }


    //certificados
    public function impresiones()
    {
        return $this->hasMany('App\Model\Certificados\Solicitud', 'solicitud_id');
    }

    public function acopios()
    {
        return $this->hasMany('App\Model\Certificados\Solicitud', 'solicitud_id');
    }

    public function entregas()
    {
        return $this->hasMany('App\Model\Certificados\Solicitud', 'solicitud_id');
    }

    /*
    * Cancelaciones
    *
    */
    public function soportes()
    {
        return $this->hasMany('App\Model\Cancelaciones\Soporte', 'usuario_id', 'usuario_id'); //un usuario sube muchos soportes
    }

    public function tipos()
    {
        return $this->belongsToMany('App\Model\Cancelaciones\Tipo', 'app_cancelaciones.cancelaciones_autorizaciones', 'usuario_id', 'tipo_id'); // un usuario autoriza muchos tipos de solicitudes
    }

    public function respuestas()
    {
        return $this->hasMany('App\Model\Cancelaciones\Respuesta', 'usuario_id', 'usuario_id'); //un usuario sube muchos soportes
    }

    public function firmaRespuestaUno()
    {
        return $this->hasMany('App\Model\Cancelaciones\Respuesta', 'usuario_id', 'respuesta_firma_uno'); //un usuario sube muchos soportes
    }

    public function firmaRespuestaDos()
    {
        return $this->hasMany('App\Model\Cancelaciones\Respuesta', 'usuario_id', 'respuesta_firma_dos'); //un usuario sube muchos soportes
    }
}

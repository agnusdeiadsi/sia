<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
	protected $connection = 'sia';
    protected $primaryKey = 'colorclass_id';
    protected $table = 'sia_colorclasses';
    protected $fillable = ['colorclass_id', 'colorclass_codigo', 'colorclass_nombre', 'colorclass_descripcion'];
    public $timestamps = false;
}

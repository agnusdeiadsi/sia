<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Alert;

//modelo
use App\Sistema;
use App\Entrance;
use App\Rol;
use App\Color;
use App\Subarea;

class SistemasController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    public function index() // lista los registros de la tabla sia_sistemas
    {   
        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista sistemas'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

    	$sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->paginate(25);

    	return view('browse.site.sistemas.index')->with('sistemas', $sistemas);
    }

    public function create()
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $roles = Rol::orderBy('rol_nombre', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
        $colores = Color::orderBy('colorclass_codigo', 'ASC')->pluck('colorclass_nombre', 'colorclass_nombre');

        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Crear nuevo sistema'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

        return view('browse.site.sistemas.create')
        ->with('colores', $colores)
        ->with('subareas', $subareas)
        ->with('roles', $roles)
        ->with('sistemas', $sistemas);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'sistema_codigo' => 'unique:sia_sistemas',
            'sistema_icon' => 'image|mimes:png',
        ]);


        $file = $request->file('sistema_icon');
        $name = $request->sistema_codigo.'_'.time().'.'.$file->getClientOriginalExtension();
        $path = public_path().'/images/icon/';
        $file->move($path, $name);

        $sistema = new Sistema($request->all());
        $sistema->sistema_icon = $name;
        $sistema->enabled = 1;
        $sistema->save();

        $sistema->roles()->sync($request->rol_id); //inserta registros en la tabla pivote sia_sistema_rol

        flash::overlay('Sistema <b>'.$sistema->sistema_nombre.'</b> ha sido registrado correctamente.', 'Notificación');

        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' guardado exitosamente'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

    	return redirect()->route('browse.site.sistemas.create');
    }

    public function edit($id)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $sistema = Sistema::find($id);
        $sistema_rol = $sistema->roles->pluck('rol_id')->ToArray();

        $roles = Rol::orderBy('rol_nombre', 'ASC')->pluck('rol_nombre', 'rol_id');
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
        $colores = Color::orderBy('colorclass_codigo', 'ASC')->pluck('colorclass_nombre', 'colorclass_nombre');

        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Editar sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

    	return view('browse.site.sistemas.edit')
        ->with('colores', $colores)
        ->with('subareas', $subareas)
        ->with('roles', $roles)
        ->with('sistema_rol', $sistema_rol)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    public function update(Request $request, Sistema $sistema)
    {   
        //valida que el logo del mosulo sea formato .png
    	$this->validate($request, [
            'sistema_icon' => 'image|mimes:png',
            'sistema_manual' => 'file|mimes:pdf',
        ]);

        //verifica si el logo y el manual ha sido cargado y guarda los cambios
        if($request->file('sistema_icon') != null && $request->file('sistema_manual') != null)
        {
            $icon = $request->file('sistema_icon');
            $icon_name = $request->sistema_codigo.'_'.time().'.'.$icon->getClientOriginalExtension();
            $path = public_path().'/images/icon/';
            $icon->move($path, $icon_name);

            $manual = $request->file('sistema_manual');
            $manual_name = $request->sistema_codigo.'_'.time().'.'.$manual->getClientOriginalExtension();
            $path = public_path().'/files/manuales/';
            $manual->move($path, $manual_name);

            $sistema->sistema_codigo = $request->sistema_codigo;
            $sistema->sistema_nombre = $request->sistema_nombre;
            $sistema->sistema_descripcion = $request->sistema_descripcion;
            $sistema->sistema_released = $request->sistema_released;
            $sistema->sistema_version = $request->sistema_version;
            $sistema->sistema_icon = $icon_name;
            $sistema->sistema_manual = $manual_name;
            $sistema->sistema_colorclass = $request->sistema_colorclass;
            $sistema->sistema_desarrollador = $request->sistema_desarrollador;
            $sistema->subarea_id = $request->subarea_id;
            $sistema->update();
        }
        elseif($request->file('sistema_icon') != null && $request->file('sistema_manual') == null)
        {
            $icon = $request->file('sistema_icon');
            $icon_name = $request->sistema_codigo.'_'.time().'.'.$icon->getClientOriginalExtension();
            $path = public_path().'/images/icon/';
            $icon->move($path, $icon_name);

            $sistema->sistema_codigo = $request->sistema_codigo;
            $sistema->sistema_nombre = $request->sistema_nombre;
            $sistema->sistema_descripcion = $request->sistema_descripcion;
            $sistema->sistema_released = $request->sistema_released;
            $sistema->sistema_version = $request->sistema_version;
            $sistema->sistema_icon = $icon_name;
            $sistema->sistema_colorclass = $request->sistema_colorclass;
            $sistema->sistema_desarrollador = $request->sistema_desarrollador;
            $sistema->subarea_id = $request->subarea_id;
            $sistema->update();
        }
        elseif($request->file('sistema_icon') == null && $request->file('sistema_manual') != null)
        {
            $manual = $request->file('sistema_manual');
            $manual_name = $request->sistema_codigo.'_'.time().'.'.$manual->getClientOriginalExtension();
            $path = public_path().'/files/manuales/';
            $manual->move($path, $manual_name);

            $sistema->sistema_codigo = $request->sistema_codigo;
            $sistema->sistema_nombre = $request->sistema_nombre;
            $sistema->sistema_descripcion = $request->sistema_descripcion;
            $sistema->sistema_released = $request->sistema_released;
            $sistema->sistema_version = $request->sistema_version;
            $sistema->sistema_manual = $manual_name;
            $sistema->sistema_colorclass = $request->sistema_colorclass;
            $sistema->sistema_desarrollador = $request->sistema_desarrollador;
            $sistema->subarea_id = $request->subarea_id;
            $sistema->update();
        }
        else
        {
            $sistema->sistema_codigo = $request->sistema_codigo;
            $sistema->sistema_nombre = $request->sistema_nombre;
            $sistema->sistema_descripcion = $request->sistema_descripcion;
            $sistema->sistema_released = $request->sistema_released;
            $sistema->sistema_version = $request->sistema_version;
            $sistema->sistema_colorclass = $request->sistema_colorclass;
            $sistema->sistema_desarrollador = $request->sistema_desarrollador;
            $sistema->subarea_id = $request->subarea_id;
            $sistema->update();
        }

        $sistema->roles()->sync($request->rol_id); //inserta registros en la tabla pivote sia_sistema_rol

    	//muestra mensaje de alerta
    	Alert::info('<p>El sistema <b>'.$sistema->sistema_nombre.'</b> ha sido actualizado correctamente.</p>', 'Notificación')->html()->persistent();

      $entrance = new Entrance([
                            'sistema_id' => null, 
                            'usuario_id' => \Auth::user()->id,
                            'usuario_rol_sia' => \Auth::user()->rol_sia, 
                            'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                            'entrance_url_anterior' => url()->previous(),
                            'entrance_url_actual' => url()->current(),
                            'entrance_accion' => nl2br('Actualización sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' exitosa'),  
                            'entrance_startdate' => date('Y-m-d H:i:s'),
                          ]);
      $entrance->save();

    	//muestra la lista de sistemas de la tabla sia_sistemas
    	return redirect()->route('browse.site.sistemas.index');


    }

    public function read(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $sistema->entrances()->create([
                              'sistema_id' => $sistema->sistema_id, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Leer manual sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.site.sistemas.read')
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    public function enable(Sistema $sistema)
    {
        $sistema->enabled = 1;
        $sistema->update();

        $sistema->entrances()->create([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Habilitar sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        //muestra alerta
        Alert::info('<p>El sistema <b>'.$sistema->sistema_nombre.'</b> ha sido habilitado.</p>')->html()->persistent();

        //muestra la lista de sistemas de la tabla sia_sistemas
        return redirect()->route('browse.site.sistemas.index');
    }

    public function disable(Sistema $sistema)
    {
        $sistema->enabled = 0;
        $sistema->update();

        $sistema->entrances()->create([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Deshabilitar sistema ID '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        //muestra alerta
        Alert::info('<p>El sistema <b>'.$sistema->sistema_nombre.'</b> ha sido deshabilitado.</p>')->html()->persistent();

        //muestra la lista de sistemas de la tabla sia_sistemas
        return redirect()->route('browse.site.sistemas.index');
    }

    /*public function destroy($id)
    {
        //crea objeto sistema y busca el sistema en la tabla sia_sistemas
        $sistema = Sistema::find($id);

        //$sistema->delete(); //elimina el sistema de la tabla sia_sistemas

        //muestra alerta
        Flash::message('<div class="w3-container">
                        <div class="w3-panel w3-pale-yellow w3-leftbar w3-border-orange">
                        <h3>Alerta</h3>
                        <p>El sistema <b>'.$sistema->sistema_nombre.'</b> ha sido eliminado correctamente.</p>
                        </div>
                        </div>', null);

        //muestra la lista de sistemas de la tabla sia_sistemas
        return redirect()->route('browse.site.sistemas.index');
    }*/
}

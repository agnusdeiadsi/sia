<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

//model
use App\Sistema;
use App\Entrance;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display main site menu.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        
        //guarda una entrada   
        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Administración del Sitio'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();
        return view('browse.site.home')
        ->with('sistemas', $sistemas);
    }
}

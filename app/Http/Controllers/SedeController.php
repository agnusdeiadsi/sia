<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Alert;

use App\Sede;

class SedeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sedes = Sede::orderBy('sede_nombre')->paginate(25);

        return view('browse.site.sedes.index')
        ->with('sedes', $sedes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('browse.site.sedes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sede_codigo' => 'unique:sia_sedes',
        ]);

        $sede = new Sede($request->all());
        $sede->save();


        Alert::success('<p>La sede <b>'.$sede->sede_nombre.'</b> ha sido creada exitosamente.</p>', '¡Muy bien!')->html()->persistent();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sede  $sede
     * @return \Illuminate\Http\Response
     */
    public function show(Sede $sede)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sede  $sede
     * @return \Illuminate\Http\Response
     */
    public function edit(Sede $sede)
    {
        return view('browse.site.sedes.edit')
        ->with('sede', $sede);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sede  $sede
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sede $sede)
    {
        $sede->update($request->all());

        Alert::info('<p>La sede ha sido actualizada exitosamente.</p>', 'Atención')->html()->persistent();
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sede  $sede
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sede $sede)
    {
        //
    }
}

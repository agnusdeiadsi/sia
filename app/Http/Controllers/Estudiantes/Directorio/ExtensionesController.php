<?php

namespace App\Http\Controllers\Estudiantes\Directorio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Extension;
use App\Sistema;
use App\Entrance;

use App\Model\Correo\Cuenta;

class ExtensionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('estudiante');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $extensiones = Extension::orderBy('extension_extension', 'ASC')
        ->join('app_correos.correos_cuentas', 'sia_extensiones.extension_id', '=', 'app_correos.correos_cuentas.extension_id')
        ->join('sia_subareas', 'app_correos.correos_cuentas.subarea_id', '=', 'sia_subareas.subarea_id')
        ->join('sia_areas', 'sia_subareas.area_id', '=', 'sia_areas.area_id')
        ->Buscar($request->busqueda)
        ->paginate(50);

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::guard('estudiante')->user()->id,
                                    'usuario_rol_sia' => null, 
                                    'usuario_rol_modulo' => 'Estudiante',
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Vista extensiones'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('estudiantes.directorio.index')
        ->with('extensiones', $extensiones)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Estudiantes\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Mail;
use Alert;

//Mail models
use App\Mail\Cancelaciones\NotificacionNuevaSolicitud;

use App\Sistema;
use App\Sede;
use App\Subarea;
use App\Matriculado;

use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Estado;
use App\Model\Cancelaciones\Tipo;
use App\Model\Cancelaciones\Adjunto;

class SolicitudController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('estudiante');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema)
    {
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->get();
        $sedes = Sede::orderBy('sede_nombre', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();

        return view('estudiantes.cancelaciones.solicitudes.create')
        ->with('sedes', $sedes)
        ->with('subareas', $subareas)
        ->with('tipos', $tipos)
        ->with('sistema', $sistema);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
        $this->validate($request, [
            'subarea_id' => 'required',
            'sede_id' => 'required',
            'registro_civil' => 'mimes:pdf|max:5120',
            'documento_acudiente' => 'mimes:pdf|max:5120',
            'solicitud_adjunto.*' => 'mimes:pdf|max:5120',
        ]);

        //valida el ID mayor de las solicitudes
        $consecutivo = Solicitud::max('solicitud_id');

        if($consecutivo == null)
        {
            $consecutivo = 'CA'.date('Y').'000001';
        }
        else
        {
            $consecutivo = explode('CA', $consecutivo);
            $consecutivo = 'CA'.($consecutivo[1]+1);
        }
                
        $solicitud = new Solicitud($request->all());
        $solicitud->solicitud_id = $consecutivo; //time().''.\Auth::guard('estudiante')->user()->matriculado_pidm;
        $solicitud->estado_id = 1; //procesamiento
        $solicitud->save();

        //Valida si se ha cargado el archivo registro civil y lo guarda
        if(count($request->file('registro_civil')) > 0)
        {
            $adjunto_nombre = $archivo;
            $adjunto_codigo = 'rc_'.$solicitud->solicitud_id.'.'.$adjunto_nombre->getClientOriginalExtension();
            $path = public_path().'/files/cancelaciones/adjuntos/';
            $adjunto_nombre->move($path, $adjunto_codigo);

            $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
            $adjunto_nombre = $adjunto_nombre[0];

            $adjunto = new Adjunto();
            $adjunto->adjunto_codigo = $adjunto_codigo;
            $adjunto->adjunto_nombre = $adjunto_nombre;
            $adjunto->solicitud()->associate($solicitud);
            $adjunto->save();
        }

        if(count($request->file('documento_acudiente')) > 0)
        {
            $adjunto_nombre = $archivo;
            $adjunto_codigo = 'da_'.$solicitud->solicitud_id.'.'.$adjunto_nombre->getClientOriginalExtension();
            $path = public_path().'/files/cancelaciones/adjuntos/';
            $adjunto_nombre->move($path, $adjunto_codigo);

            $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
            $adjunto_nombre = $adjunto_nombre[0];

            $adjunto = new Adjunto();
            $adjunto->adjunto_codigo = $adjunto_codigo;
            $adjunto->adjunto_nombre = $adjunto_nombre;
            $adjunto->solicitud()->associate($solicitud);
            $adjunto->save();
        }

        $adjuntos = array();
        $load = false;
        if(count($request->file('solicitud_adjunto')) > 0)
        {
            $i = 0;
            foreach ($request->file('solicitud_adjunto') as $archivo)
            {
                $adjunto_nombre = $archivo;
                $adjunto_codigo = $solicitud->solicitud_id.'_'.$i.'.'.$adjunto_nombre->getClientOriginalExtension(); //'sol_'.time().$i.'.'.$adjunto_nombre->getClientOriginalExtension();
                $path = public_path().'/files/cancelaciones/adjuntos/';
                $adjunto_nombre->move($path, $adjunto_codigo);

                $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
                $adjunto_nombre = $adjunto_nombre[0];

                $adjunto = new Adjunto();
                $adjunto->adjunto_codigo = $adjunto_codigo;
                $adjunto->adjunto_nombre = $adjunto_nombre;
                $adjunto->solicitud()->associate($solicitud);
                $adjunto->save();

                $adjuntos = ['nombre' => $adjunto->adjunto_nombre];

                $i++;
            }

            $load = true;
        }

        //Envía notificacion al email del solicitante
        Mail::to($request->matriculado_email)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionNuevaSolicitud($sistema, $solicitud));
        
        Alert::info('Su solicitud N° <b>'.$solicitud->solicitud_id.'</b> ha sido guardada correctamente, un correo de confirmación ha sido enviado a su cuenta <b>'.$request->matriculado_email.'</b>. El estado actual es <i>en procesamiento</i>', 'Notificación')->html()->persistent("OK");

        //return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sistema $sistema, Solicitud $solicitud)
    {
        return view('estudiantes.cancelaciones.solicitudes.show')
        ->with('solicitud', $solicitud)
        ->with('sistema', $sistema);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, Sistema $sistema)
    {
        $solicitud = Solicitud::find($request->solicitud_id);
        //dd($solicitud);
        if(count($solicitud) != null /*&& $solicitud->matriculado_pidm == \Auth::guard('estudiante')->user()->matriculado_pidm*/)
        {
            return redirect()->route('estudiantes.'.$sistema->sistema_nombre_corto.'.solicitudes.show', [$sistema, $solicitud]);
        }
        else
        {
            Alert::warning('No se han encontrado registros. Verifique que haya ingresado correctamente el ID e inténtelo nuevamente.', ' ')->html()->persistent('Cerrar');

            return redirect()->back();
        }
    }
}

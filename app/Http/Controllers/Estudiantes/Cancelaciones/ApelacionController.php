<?php

namespace App\Http\Controllers\Estudiantes\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Alert;

use App\Sistema;
use App\Model\Cancelaciones\Apelacion;
use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Adjunto;

class ApelacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('estudiante');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema, Solicitud $solicitud)
    {
        $apelacion = $solicitud->apelacion;

        if($apelacion != null)
        {            
            Alert::warning('La solicitud ya ha sido apelada. A continuación, encontrará los datos de la apelación.', ' ')->html()->persistent("OK");

            return redirect()->route('estudiantes.cancelaciones.apelaciones.show', [$sistema, $apelacion]);
        }
        else
        {
            return view('estudiantes.cancelaciones.apelaciones.create')
            ->with('solicitud', $solicitud)
            ->with('sistema', $sistema);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        $this->validate($request, [
                'apelacion_adjunto.*' => 'mimes:pdf|max:5120',
        ],
        [
            'apelacion_adjunto.mimes' => ' Los archivos adjuntos deben ser de tipo PDF'
        ]);

        $consecutivo = Apelacion::max('apelacion_id');

        if($consecutivo == null)
        {
            $consecutivo = 'AP'.date('Y').'000001';
        }
        else
        {
            $consecutivo = explode('AP', $consecutivo);
            $consecutivo = 'AP'.($consecutivo[1]+1);
        }

        $apelacion = new Apelacion($request->all());
        $apelacion->apelacion_id = $consecutivo; //time().''.\Auth::guard('estudiante')->user()->matriculado_pidm;
        $apelacion->estado_id = 1; //procesamiento
        $apelacion->solicitud()->associate($solicitud);
        $apelacion->save();

        $adjuntos = array();
        $load = false;
        if(count($request->file('apelacion_adjunto')) > 0)
        {
            $i = 0;
            foreach ($request->file('apelacion_adjunto') as $archivo)
            {
                $adjunto_nombre = $archivo;
                $adjunto_codigo = $apelacion->apelacion_id.'_'.$i.'.'.$adjunto_nombre->getClientOriginalExtension(); //'ap_'.time().$i.'.'.$adjunto_nombre->getClientOriginalExtension();
                $path = public_path().'/files/cancelaciones/adjuntos/';
                $adjunto_nombre->move($path, $adjunto_codigo);

                $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
                $adjunto_nombre = $adjunto_nombre[0];

                $adjunto = new Adjunto();
                $adjunto->adjunto_codigo = $adjunto_codigo;
                $adjunto->adjunto_nombre = $adjunto_nombre;
                $adjunto->apelacion()->associate($apelacion);
                $adjunto->save();

                $adjuntos = ['nombre' => $adjunto->adjunto_nombre];

                $i++;
            }

            $load = true;
        }


        Alert::success('La apelación ha sido guardada correctamente.<br><br>Apelación <b>N° '.$apelacion->apelacion_id.'</b>, Solicitud <b>N° '.$apelacion->solicitud_id.'</b>. Una notificación ha sido enviada a su correo.', ' ')->html()->persistent("OK");

        return redirect()->route('estudiantes.cancelaciones', $sistema);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Sistema $sistema, Apelacion $apelacion)
    {
        return view('estudiantes.cancelaciones.apelaciones.show')
        ->with('apelacion', $apelacion)
        ->with('sistema', $sistema);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

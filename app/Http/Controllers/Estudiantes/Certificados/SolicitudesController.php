<?php

namespace App\Http\Controllers\Estudiantes\Certificados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;
use Alert;

use App\Sistema;
use App\Matriculado;
use App\Subarea;
use App\Semestre;
use App\Sede;
use App\Entrance;

use App\Model\Certificados\Tipo;
use App\Model\Certificados\Solicitud;
use App\Model\Certificados\Materia;

use App\Mail\Certificados\NotificacionNuevaSolicitud;
use App\Mail\Certificados\NotificacionProcesarSolicitud;

class SolicitudesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('estudiante');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema, Matriculado $matriculado)
    {   
        $programas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_codigo');
        $semestres = Semestre::orderBy('semestre_codigo', 'ASC')->pluck('semestre_nombre', 'semestre_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $sedes = Sede::orderBy('sede_nombre', 'ASC')->pluck('sede_nombre', 'sede_id');

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::guard('estudiante')->user()->id,
                                    'usuario_rol_sia' => null, 
                                    'usuario_rol_modulo' => 'Estudiante',
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Crear nueva solicitud'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('estudiantes.certificados.solicitudes.create')
        ->with('programas', $programas)
        ->with('semestres', $semestres)
        ->with('tipos', $tipos)
        ->with('sedes', $sedes)
        ->with('matriculado', $matriculado)
        ->with('sistema', $sistema);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
        $this->validate($request, [
            'solicitud_codigo_liquidacion' => 'unique:certificados.certificados_solicitudes',
            'solicitud_comprobante_liquidacion' => 'required:certificados.certificados_solicitudes|mimes:pdf|max:2048',
            'solicitud_carnet_foto' => 'mimes:jpg,png|max:5120',
            'solicitud_carnet_documento' => 'mimes:pdf|max:2048',
            //'solicitud_condiciones' => 'required:certificados_solicitudes',
        ]);

        //valida el ID mayor de las solicitudes
        $consecutivo = Solicitud::max('solicitud_id');

        if($consecutivo == null)
        {
            $consecutivo = 'CE'.date('Y').'000001';
        }
        else
        {
            $consecutivo = explode('CE', $consecutivo);
            $consecutivo = 'CE'.($consecutivo[1]+1);
        }

        //valida y guarda los archivos adjuntos
        if($request->file('solicitud_carnet_foto') != null || $request->file('solicitud_carnet_documento') != null)
        {
            $foto_carnet = $request->file('solicitud_carnet_foto');
            $nombre_foto_carnet = 'FOTO_'.$consecutivo.'.'.$foto_carnet->getClientOriginalExtension();
            $path = public_path().'/files/certificados/carnets/';
            $foto_carnet->move($path, $nombre_foto_carnet);

            $documento_carnet = $request->file('solicitud_carnet_documento');
            $nombre_documento_carnet = 'DOC_'.$consecutivo.'.'.$documento_carnet->getClientOriginalExtension();
            $path = public_path().'/files/certificados/carnets/';
            $documento_carnet->move($path, $nombre_documento_carnet);
        }

        $comprobante = $request->file('solicitud_comprobante_liquidacion');
        $nombre_comprobante = 'COD_'.$consecutivo.'.'.$comprobante->getClientOriginalExtension();
        $path = public_path().'/files/certificados/comprobantes/';
        $comprobante->move($path, $nombre_comprobante);


        //guarda la nueva solicitud
        $solicitud = new Solicitud($request->all());
        $solicitud->solicitud_id = $consecutivo; //\Auth::guard('estudiante')->user()->matriculado_id.''.time();
        $solicitud->solicitud_comprobante_liquidacion = $nombre_comprobante;
        $solicitud->solicitud_matriculado_tipo_documento = $request->solicitud_matriculado_tipo_documento;
        $solicitud->solicitud_matriculado_documento = $request->solicitud_matriculado_documento;
        $solicitud->solicitud_matriculado_fecha_nacimiento = $request->solicitud_matriculado_fecha_nacimiento;
        $solicitud->solicitud_matriculado_programa = $request->solicitud_matriculado_programa;
        $solicitud->solicitud_matriculado_tipo = $request->solicitud_matriculado_tipo;
        $solicitud->solicitud_carnet_denuncio = $request->solicitud_carnet_denuncio;
        $solicitud->solicitud_pension_valor = $request->solicitud_pension_valor;
        $solicitud->solicitud_pension_entidad = $request->solicitud_pension_entidad;
        $solicitud->estado_id = 1; //procesamiento
        $solicitud->save();

        if($request->solicitud_carnet_foto && $request->solicitud_carnet_documento)
        {
            $solicitud->solicitud_carnet_foto = $nombre_foto_carnet;
            $solicitud->solicitud_carnet_documento = $nombre_documento_carnet;
        }

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::guard('estudiante')->user()->id,
                                    'usuario_rol_sia' => null, 
                                    'usuario_rol_modulo' => 'Estudiante',
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Solicitud N° '.$solicitud->solicitud_id.' guardada exitosamente'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        if($request->materia_nombre)
        {
            $materias = new Materia([ 'materia_nombre' => $request->materia_nombre]);
            $solicitud->materias()->save($materias);   

        }

        //envia notificacion al email
        Mail::to($request->solicitud_matriculado_email)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionNuevaSolicitud($solicitud));

        //mensaje
        Alert::success('<p>La solicitud ha sido guardada correctamente. ID '.$solicitud->solicitud_id.'. Un correo de confirmación fue enviado al correo: <b>'.$solicitud->solicitud_matriculado_email.'</b>.</p>')->html()->persistent('Cerrar');

        //redirecciona al formulario de creación
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Estudiantes\Grado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Alert;
use App\Sistema;
use App\Matriculado;
use App\Ciudad;
use App\Departamento;
use App\Sede;
use App\Academico;
use App\Identificacion;
use App\programa;

use App\Model\Solicitudes\Solicitud;
use App\Model\Solicitudes\SolicitudArchivos;
use App\Model\Solicitudes\SolicitudFaseEstado;

use App\Model\Banner\BMatricula;
use App\Model\Banner\BMatriculado;

class SolicitudGradoController extends Controller
{
	//Esta funcion sirve para retornar al login si pasa mucho tiempo sin utilizar el sitio
	public function __construct()
    {
        $this->middleware('estudiante');
    }

    //Retorna el menu del modulo
    public function menu(Sistema $sistema)
    {

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('estudiantes.grados.menu')
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema);
    
    }

    //Retornar la vista para crear la solicitud
    public function create(Sistema $sistema, Matriculado $pidm)
    {

    	//Se consultan las variables necesarias para la visualizacion de la vista
    	$matricula = BMatricula::where('bmtr_pidm', $pidm->matriculado_pidm)->orderBy('bmtr_periodo', 'DESC')->first(); 
    	$programa = Programa::where('programa_alfa', $matricula->bmtr_cod_programa)->first();
    	$Bmatriculado = BMatriculado::where('PERS_PIDM', $pidm->matriculado_pidm)->first();

    	$sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
    	$ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();
    	$departamento = Departamento::orderBy('departamento_nombre', 'ASC')->get();
    	$sede = Sede::orderBy('sede_nombre', 'ASC')->get();

    	$solicitud = Solicitud::where(['programa_id' => $programa->programa_id,
	    								   'tipoSolicitud_id' =>  1,
	    								   'matriculado_pidm' => $pidm->matriculado_pidm])
    	                        ->orderBy('created_at', 'DESC');

    	/*
			Retornamos a la vista con las variables necesarias
    	*/
        return view('estudiantes.grados.postulacion.create')
        		->with('Bmatriculado', $Bmatriculado)
        		->with('programa', $programa)
        		->with('sede', $sede)
        		->with('departamento', $departamento)
        		->with('ciudad', $ciudad)
        		->with('matriculado', $pidm)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema);
    
    }

    public function docs(Sistema $sistema, Matriculado $pidm)
    {

    	$matricula = BMatricula::where('bmtr_pidm', $pidm->matriculado_pidm)->orderBy('bmtr_periodo', 'DESC')->first(); 
    	$programa = Programa::where('programa_alfa', $matricula->bmtr_cod_programa)->first();
    	$Bmatriculado = BMatriculado::where('PERS_PIDM', $pidm->matriculado_pidm)->first();

    	$sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
    	$ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();
    	$departamento = Departamento::orderBy('departamento_nombre', 'ASC')->get();
    	$sede = Sede::orderBy('sede_nombre', 'ASC')->get();

    	$solicitud = Solicitud::where(['programa_id' => $programa->programa_id,
	    								   'tipoSolicitud_id' =>  1,
	    								   'matriculado_pidm' => $pidm->matriculado_pidm])
    	                        ->orderBy('created_at', 'DESC');



    	/*
    	*	Con el siguiente arreglo se sabra si alguno de los documentos solicitados no ha sido subido, para en la vista 
    		procesar esto y indarle al estudiante que archivos no has sido subidos
    	*
    	*/
    	$doc = [
    			
    			"Cedula" => 'contrast-color',
    			"Acta de sustentación" => 'contrast-color',
    			"Acta de grado de bachiller" => 'contrast-color',
    			"Resultado pruebas saber 11" => 'contrast-color',
    			"Certificado encuesta momento de grado" => 'contrast-color',
    			"Acta de grado tecnico o tecnologo" => 'contrast-color',
    			"Foto de perfil" => 'contrast-color'
    		
    			];

    	/*
    	*	Preguntamos si la solicitud perteneciente a un postulado (Estudiante) tiene archivos amarrados
    	*/
    	if ($solicitud->first() != null && $solicitud->first()->archivos->count() > 0) 
    	{
    		/*
	    	*	El siguiente foreach recorre los archivos pertenecientes al estudiante
	    	*/
	    	foreach ($solicitud->first()->archivos as $archivo) 
	    	{
	    		/*
	    			1 = Cedula
	    			2 = Acta de sustentación
	    			3 = Acta de grado de bachiller
	    			4 = Resultado pruebas saber 11
	    			5 = Certificado encuesta momento de grado
	    			6 = Acta de sustentación
	    			7 = Foto de perfil
	    		*/

	    		/*
					Se pregunta si el archivo actual dentro del ciclo, en su campo solicitdDocumento_id es = 1
	    		*/

				switch ($archivo->solicitudDocumento_id) 
				{
				    case 1:
				        $doc["Cedula"] = 'teal';
				        break;
				    case 2:
				        $doc["Acta de sustentación"] = 'teal';
				        break;
				    case 3:
				        $doc["Acta de grado de bachiller"] = 'teal';
				        break;
				    case 4:
				    	$doc["Resultado pruebas saber 11"] = 'teal';
				        break;
				    case 5:
				    	$doc["Certificado encuesta momento de grado"] = 'teal';
				        break;
				    case 6:
				    	$doc["Acta de grado tecnico o tecnologo"] = 'teal';
				        break;
				    case 7:
				    	$doc["Foto de perfil"] = 'teal';
				        break;
				       
				}

	    	}

    	}

    	/*
			Retornamos a la vista con las variables necesarias
    	*/
        return view('estudiantes.grados.postulacion.docs')
        		->with('doc', $doc)
        		->with('programa', $programa)
        		->with('solicitud', $solicitud)
        		->with('matriculado', $pidm)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema);

    }

    public function store(Sistema $sistema, Matriculado $matriculado, Request $request)
    {
    	/*
			Se declara un try catch para controlar las excepciones a nivel de BD
    	*/
    	try 
    	{
    		
    		/*
				Validamos que el archivo que se subira (Foto) corresponda a las siguientes reglas
    		*/
    		$this->validate($request, [
	            'foto' => 'mimes:jpeg,png,jpg'
	        ]);

		    /*
				Se guardan los datos del estudiante
		    */
	    	$matriculado->matriculado_primer_apellido = $request->primer_apellido;
	    	$matriculado->matriculado_segundo_apellido = $request->segundo_apellido;
	    	$matriculado->matriculado_primer_nombre = $request->primer_nombre;
	    	$matriculado->matriculado_segundo_nombre = $request->segundo_nombre;
	    	$matriculado->matriculado_email = $request->email;
	    	$matriculado->matriculado_telefono = $request->fijo;
	    	$matriculado->matriculado_celular = $request->celular;
	    	$matriculado->matriculado_direccion = $request->direccion;
	    	$matriculado->matriculado_barrio = $request->barrio;
	    	$matriculado->ciudad_id = $request->ciudad;

	    	$matriculado->save();

	    	/*
				En este caso, sucede lo mismo al querer guardar la foto de perfil.

				Siempre y cuando el programa y el matriculado no esten, o sean diferentes, se guarda el registro

				EJ: PIDM = 123
					PROGRAMA_ID = 1

						Se guarda el registro si esos dos datos no coinciden con ningun registro dentro de la BD
	    	*/

			/*
				Para ser mas excato, el primer arreglo es el condicional de guradar o acualizar
			*/
	    	$academico = Academico::updateOrCreate(['matriculado_pidm' => $matriculado->matriculado_pidm,
	    											'programa_id' => $request->programa_id],
				    								['academico_codigo' => $request->codigo_id,
				    								'academico_fecha_finalizacion' => 
				    									$request->finalizacionEstudio.'-'.$request->finalizacionPeriodo,
				    								'sede_id' => $request->sede]);	    

	    	Identificacion::updateOrCreate(['matriculado_pidm' => $matriculado->matriculado_pidm,
	    									'identificacion_numero' => $request->numero_documento],
	    									['identificacion_tipo' => $request->tipo_documento,
	    									'identificacion_lugar_expedicion' => $request->lugar_expedicion]);

	    	/*
				booleanPrograma, se define con el objetivo de saber si el estudiante ya tiene una solicitud perteneciente 
				a el programa en que se encuentra actualmete
	    	*/
	    	$booleanPrograma = false;

	    	//Se obtiene las solicitudes de tipo grado del postulante
    		$solicitud = Solicitud::where(['programa_id' => $request->programa_id,
	    								   'tipoSolicitud_id' =>  1, //Solicitud tipo grado
	    								   'matriculado_pidm' => $matriculado->matriculado_pidm])
    	                            ->get();

    	    //dd(Solicitud::all()->last());

	    	// Si el postulante tiene una solicitud de tipo grado
	    	if ($solicitud->count() > 0) 
	    	{

	    		// Se reocorre las solicitudes de tipo grado
		    	foreach ($solicitud as $data) 
		    	{
		    		/*
		    			Si el postulante tiene una solicitud de tipo grado correspondiente al programa enviado
		    		*/
			    	if ($data->programa_id == $request->programa_id) 
			    	{

			    		$booleanPrograma = true; //El matriculado ya se ha postulado para el programa enviado
			    		
			    	} 

		    	}

		    	//Si booleanPrograma es false
		    	if (!$booleanPrograma) 
		    	{
		    		//Creamos la solicitud
			    	Solicitud::create(['tipoSolicitud_id' => 1,//Solicitud de tipo grado
		    						'programa_id' => $request->programa_id,
		    						'matriculado_pidm' => $matriculado->matriculado_pidm]);

			    }

			// Si el postulante no tiene una solicitud de tipo grado
	    	} else {
	    		
	    		Solicitud::create(['tipoSolicitud_id' => 1,//Solicitud de tipo grado
							'programa_id' => $request->programa_id,
							'matriculado_pidm' => $matriculado->matriculado_pidm]);

	    	}

	   	$solicitud_ = Solicitud::where(['programa_id' => $request->programa_id,
	    								'tipoSolicitud_id' =>  1, //Solicitud tipo grado
	    								'matriculado_pidm' => $matriculado->matriculado_pidm])
	   	                        ->orderBy('created_at', 'DESC')->first();
	   	//dd($solicitud_);
    	/*
			preguntamos si se ha enviado la foto
		*/
		if ($request->hasFile('foto')) 
    	{
    		//dd($request->solicitud);
    		// $file = archivo
    		$file = $request->file('foto');
    		
    		/*
    			Se guarda el archivo en una direccion, esta dirreccion sera creada (si no lo esta), con el numero de 
				documento (guion) el numero de la solicitud
			*/
	        $file->store('public/'.$request->numero_documento.'-'.$solicitud_->solicitud_id.'/perfil');

	        /*
				Se obtiene de la carpeta del postulante la foto de perfil, en la subcarpeta perfil, donde se alojan las fotos almacenadas por el estudiante
	        */
	       	$files = Storage::files('public/'.$request->numero_documento.'-'.$solicitud_->solicitud_id.'/perfil');

	    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
	    	$this->createFile($request->numero_documento, $solicitud_->solicitud_id);

	    	/*
				Se recorre el contenido de la variable $files, con el objetivo de identificar un cambio en el directorio
	    	*/
	    	foreach ($files as $file) 
	    	{
	  			
	  			// Se divide el string
	    		$fileArray = explode('/', $file);

	    		/*
	    			Se divide el string en la posicion 3, es donde se encuentra el nombre del archivo junto a su extencion
	    		*/
	    		$filename = explode('.', $fileArray[3]);
	    		
	    		/*
					Mientras que el nombre del archivo se diferente a index, se entra en la condicion
	    		*/
	    		if ($filename[0] != 'index') 
	    		{

	    			/*
						Siempre y cuando el numero de la solicitud y el nombre del archivo no se encuentren en la BD
						creamos el registro, sino, se actualiza el registro que corresponda a las anteriores variables mencionadas
	    			*/

					SolicitudArchivos::updateOrCreate(
									    		['solicitud_id' => $solicitud_->solicitud_id,
									    		'solicitudDocumento_id' => 7 /*Foto perfil*/], 
									    		['solicitudArchivos_nombre' => $filename[0],
									    		'solicitudArchivos_tipo' => $filename[1],
									    		'solicitudArchivos_ruta' => '/'.$request->numero_documento
									    											.'-'.
									    										$solicitud_->solicitud_id.'/perfil']);
	    			/*SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $request->solicitud,
		    		'solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1]],
		    		['solicitudArchivos_ruta' => '/'.$request->numero_documento.'-'.$request->solicitud.'/perfil']);*/	
	    			
	    		}
	    		
	    	}

	    }

	    	Alert::success('<p>Datos ingresados exitosamente</p>')->html()->persistent("OK");
	    	//return redirect()->back();

	    	return $this->docs($sistema, $matriculado);

	    //En caso de presentar un excepcion de tipo SQL
	    } catch (\Illuminate\Database\QueryException $e) {
	    	//dd($e);
	    	Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
	    	return redirect()->back();

		}

    }

    public function storeArchive(Sistema $sistema, Matriculado $matriculado, Request $request)
    {
    	/*
			Se declara un try catch para controlar las excepciones a nivel de BD
    	*/
    	try {

	    	$message = '';

	    	/*
				Se valida que los archivos enviados correspondan a las siguientes reglas
	    	*/
	    	$this->validate($request, [
	            //'archivo.*' => 'required|mimes:jpeg,pdf,png,doc'
	            'cedula' => 'mimes:jpeg,pdf,png,jpg|max:5000',
	            'sustencion' => 'mimes:jpeg,pdf,png,jpg|size:5000',
	            'actaBachiller' => 'mimes:jpeg,pdf,png,jpg|size:5000',
	            'pruebasSaber' => 'mimes:jpeg,pdf,png,jpg|size:5000',
	            'certificadoEncuesta' => 'mimes:jpeg,pdf,png,jpg|size:5000',
	            'actaTecTec' => 'mimes:jpeg,pdf,png,jpg|size:5000',
	        ]);

	    	$archivos = new SolicitudArchivos();

	    	$documento_data = Identificacion::where('matriculado_pidm', $matriculado->matriculado_pidm)
												->orderBy('created_at', 'DESC')->first(); 	

			$matricula = BMatricula::where('bmtr_pidm', $matriculado->matriculado_pidm)
									->orderBy('bmtr_periodo', 'DESC')->first(); 

	    	$programa = Programa::where('programa_alfa', $matricula->bmtr_cod_programa)->first();

	    	$solicitud = Solicitud::where(['programa_id' => $programa->programa_id,
	    								   'tipoSolicitud_id' =>  1,
	    								   'matriculado_pidm' => $matriculado->matriculado_pidm])->get();

	    	/*foreach ($request->file('archivo') as $file) 
	    	{	
	    		$file->store('public/'.$documento_data->identificacion_numero.'-'.
	    								$solicitud->first()->solicitud_id);
	    	}

	    	$files = Storage::files('public/'.$documento_data->identificacion_numero.'-'.
	    								$solicitud->first()->solicitud_id);*/


	    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
	    	//$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    	/*foreach ($files as $file) 
	    	{
	  
	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);

	    		if ($filename[0] != 'index') 
	    		{
	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,											
		    		'solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1]],
		    		['solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero.'-'.
	    											$solicitud->first()->solicitud_id]);	
	    		
	    		}
	    		
	    	}*/

	    	/*-------------------------------------------------------------------------------------------------
				La siguiente documentacion, relacionara a los archivos que deben ser subidos. Esto por que son independientes, es decir todos tienen un campo propio dentro del formulario
	    	---------------------------------------------------------------------------------------------------*/

	    	/*
				Si el archivo (Cedula en este caso) se encuentra definido
	    	*/

			//dd($request->all());
	    	if ($request->hasFile('cedula')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 1) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}

	    		/*
					Se guarda en la variable $file, la ruta donde se almacenara el archivo.

					En este caso la ruta es "../public/documento-solicitud"
	    		*/
	    		$file = $request->file('cedula')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

		    	// Se divide el string
	    		$fileArray = explode('/', $file);

	    		/*
		    		Se divide el string en la posicion 2, es donde se encuentra el nombre del archivo junto a su extencion
		    	*/
	    		$filename = explode('.', $fileArray[2]);
		    	
	    		/*
					Mientras que el nombre del archivo se diferente a index, se entra en la condicion
	    		*/
		    	if ($filename[0] != 'index') 
	    		{

	    			/*
						Si la solicitud y el numero de documento coninciden con algun registro delntro de la base de datos, se actualiza el registro, de lo contrario se crea
	    			*/
	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 1 /*Cedula*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }

		    if ($request->hasFile('sustencion')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 2) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}
	    		
		        $file = $request->file('sustencion')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);
		    	

		    	if ($filename[0] != 'index') 
	    		{

	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 2 /*Acta de sustentacion*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }

		    if ($request->hasFile('actaBachiller')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 3) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}

	    		$file = $request->file('actaBachiller')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);
		    	

		    	if ($filename[0] != 'index') 
	    		{

	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 3 /*Acta de bachiller*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }

		    if ($request->hasFile('pruebasSaber')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 4) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}

	    		$file = $request->file('pruebasSaber')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);
		    	

		    	if ($filename[0] != 'index') 
	    		{

	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 4 /*Resultado pruebas icfes*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }
		    
		    if ($request->hasFile('certificadoEncuesta')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 5) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}

				$file = $request->file('certificadoEncuesta')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);
		    	

		    	if ($filename[0] != 'index') 
	    		{

	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 5 /*certificado encuesta momento de grado*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }

		    if ($request->hasFile('actaTecTec')) 
	    	{

	    		foreach ($solicitud->first()->archivos as $documento) 
	    		{
	    			
	    			if($documento->solicitudDocumento_id == 6) 
	    			{

	    				$var = Storage::delete('public/'.$documento_data->identificacion_numero
		    											.'-'.$solicitud->first()->solicitud_id
		    											.'/'.$documento->solicitudArchivos_nombre
		    											.'.'.$documento->solicitudArchivos_tipo);

	    			}

	    		}

	    		$file = $request->file('actaTecTec')
		                          ->store('public/'.$documento_data->identificacion_numero.'-'.$solicitud
		                          ->first()->solicitud_id);

		    	//Se realiza el llamado a la funcion que permite la creacion del archivo index.php
		    	$this->createFile($documento_data->identificacion_numero, $solicitud->first()->solicitud_id);

	    		$fileArray = explode('/', $file);

	    		$filename = explode('.', $fileArray[2]);
		    	

		    	if ($filename[0] != 'index') 
	    		{

	    			SolicitudArchivos::updateOrCreate(
		    		['solicitud_id' => $solicitud->first()->solicitud_id,
		    		'solicitudDocumento_id' => 6 /*Acta de grado tecnico tecnologo*/], 
		    		['solicitudArchivos_nombre' => $filename[0],
		    		'solicitudArchivos_tipo' => $filename[1],
		    		'solicitudArchivos_ruta' => '/'.$documento_data->identificacion_numero
		    											.'-'.
		    										$solicitud->first()->solicitud_id]);	
	    			
	    		}

		    }

			/*if ($request->file('cedula') != null) 
			{
				if ($request->file('cedula')->getClientOriginalName() == 
												$documento_data->identificacion_numero.'-Cedula.pdf') 
				{
					
				
					SolicitudArchivos::updateOrCreate(['solicitud_id' => $solicitud->first()->solicitud_id,
													   'solicitudArchivos_nombre' => 'cedula'],
													  ['solicitudArchivos_ruta' => $request->file('cedula')->store('public/'.$documento_data->identificacion_numero)]);
				
				} else {

					$message = 'Verifica que el nombre del archivo cedula concuerde con los estandares';

				}
			}

			if ($request->file('sustencion') != null) 
			{
				if ($request->file('sustencion')->getClientOriginalName() == 
												$documento_data->identificacion_numero.'-Sustencion.pdf') 
				{
					
				
					SolicitudArchivos::updateOrCreate(['solicitud_id' => $solicitud->first()->solicitud_id,
													   'solicitudArchivos_nombre' => 'sustencion'],
													  ['solicitudArchivos_ruta' => $request->file('sustencion')->store('public/'.$documento_data->identificacion_numero)]);
				
				} else {

					$message = 'Verifica que el nombre del archivo sustentacion concuerde con los estandares';

				}
			}

			if ($message == '') 
			{

				Alert::success('<p>Los archivos han sido cargados exitosamente</p>')->html()->persistent('OK');
				return redirect()->back();

			} else {

				Alert::warning('<p>'.$message.'</p>')->html()->persistent('OK');
				return redirect()->back();

			}*/


			/*
				Retornamos a la vista del formulario de postulacion 
			*/

			Alert::success('<p>Los archivos han sido cargados exitosamente</p>')->html()->persistent('OK');
			return redirect()->back();

		} catch (\Illuminate\Database\QueryException $e) {
			//dd($e);
			Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
	    	return redirect()->back();

		}
		
    }
    /*
    	Funcion que permite crear el archivo index.php para la porteccion de los direcctorios que contiene los archivos de los estudiantes
    */
    public function createFile($documento, $solicitud)
    {
    	//Se crea (Si no esta) y se accede al directorio donde se guardara el archivo index
    	$handle = fopen("../storage/app/public/".$documento."-".$solicitud."/index.php", 'wb') 
    					or die('Cannot open file');
    	//La siguiente variable almacenara el contenido del archi index
		$data = "header('Location: https://siadev.unicatolica.edu.co/public/browse/login');";
		/*
			El primer parametro que se resive es el alchivo al cual accederemos, y el segundo parametro indica los que escribiremos
		*/
		fwrite($handle, $data);
		//Para finalizar el proceso, se cierra el archivo
		fclose($handle);
    
    }

}

<?php

namespace App\Http\Controllers\Estudiantes\Grado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function getSolicitud($matriculado, $programa)
    {
    	return \App\Model\solicitudes\Solicitud::where([
					'matriculado_pidm' => $matriculado,
					'tipoSolicitud_id' => 1,
					'programa_id' => $programa])//Solicitud grado
					->orderBy('created_at', 'DESC')->first();
    }
}

<?php

namespace App\Http\Controllers\Estudiantes\Egresados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Matriculado;
use App\Telefono;
use App\Ubicacion;
use App\Biografia;
use App\Email;
use App\Academico;
use App\Sistema;
use App\Subarea;
use App\RangoSalarial;
use App\SectorEmpresa;
use App\Ciudad;
use App\NivelAcademico;
use App\EstadoCivil;
use App\Programa;
use Excel;
use Alert;
use App\Cargo;
use App\Contrato;
use App\TipoEmpresa;
use App\Sede;

use App\Model\Egresados\Grado;
use App\Model\Egresados\Laboral;
use App\Model\Egresados\AreaPertenece;
use App\Model\Egresados\DuracionEmpresa;
use App\Model\Egresados\Ocupacion;
use App\Model\Egresados\RelacionPrograma;
use App\Model\Egresados\SituacionLaboral;
use App\Model\Egresados\Vinculacion;
use App\Model\Egresados\TamanoEmpresa;
use App\Model\Egresados\Logros;
use App\Model\Egresados\Emprendimiento;
use App\Model\Egresados\Reconocimiento; 
use App\Model\Egresados\Estudio; 
use App\Model\Egresados\Familia; 
use App\Model\Egresados\Referencia;     

use App\Model\Encuesta\Censal; 
use App\Model\Encuesta\Comunidad; 
use App\Model\Encuesta\Beca;
use App\Model\Encuesta\Credito;
use App\Model\Encuesta\Satisfacion_Competencias;
use App\Model\Encuesta\Ingles_Competencias;
use App\Model\Encuesta\Plan_Vida;
use App\Model\Encuesta\Idea_Empresa;
use App\Model\Encuesta\Busca_Trabajo;
use App\Model\Encuesta\Nivel_Identidad;
use App\Model\Encuesta\NivelIdentidad_Apoyo;
use App\Model\Encuesta\NivelIdentidad_Estudios;
use App\Model\Encuesta\Satisfacion;

use App\Model\Banner\BMatriculado;
/*use App\Model\Banner\Egresados\Matriculado as BannerMatriculado;
use App\Model\Banner\Egresados\Matriculado_Contacto as BannerContacto;
use App\Model\Banner\Egresados\Matriculado_Email as BannerEmail;
use App\Model\Banner\Egresados\Matriculado_Residencia as BannerResidencia;
use App\Model\Banner\Egresados\Matriculado_Personal as BannerPersonal;*/


class FormularioController extends Controller
{

   /**
   * Create a new controller instance.
   *
   * @return void
   */
    public function __construct()
    {
        $this->middleware('estudiante');
    }

    public function showMenu(Sistema $sistema)
    {

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('estudiantes.egresados.menu')
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema);
    }

    public function showSurvey(Sistema $sistema, $key)
    {
        $Begresado = BMatriculado::find($key);
        $egresado = Matriculado::find($key);
        //$estudiante_info = BannerMatriculado::Student($key)->get();
        //dd(BannerMatriculado::all());
        /*$estudiante_infoC = BannerContacto::Student($key)->get();
        $estudiante_infoE = BannerEmail::Student($key)->get();
        $estudiante_infoR = BannerResidencia::Student($key)->get();
        $estudiante_infoP = BannerPersonal::Student($key)->get();
        dd($estudiante_infoE);
        $fecha = explode(" ", $estudiante_infoP->first()->biop_fec_nmto);*/

        $estrato = array('1' => 'Estrato 1',
                         '2' => 'Estrato 2',
                         '3' => 'Estrato 3',
                         '4' => 'Estrato 4',
                         '5' => 'Estrato 5',
                         '6' => 'Estrato 6');

        $genero = array('M' => 'Masculino',
                        'F' => 'Femenino');

        $sino = array('si' => 'Si',
                      'no' => 'No' );

        $discapacidad = array('autismo' => 'Autismo',
                              'baja_vision_diagnostica' => 'Baja vision diagnostica',
                              'ceguera' => 'Ceguera',
                              'deficiensia_cognitiva' => 'Deficiensia Cognitiva',
                              'hipoacusia_baja_audicio' => 'Hipoacusia a baja audicio',
                              'lesion_neuromuscular' => 'Lesion neuromuscular',
                              'multiple' => 'Multiple',
                              'paralisis_cerebral' => 'Paralisis Cerebral',
                              'sindrome_de_down' => 'Sindrome de down',
                              'sordera_profunda' => 'Sordera profunda',
                              'ninguna' => 'Ninguna de las anteriores');

        $comunidad_asociacion = array('comunidades_asociacion_academica' => 'Comunidades o asociación academica',
                                      'comunidades_asociacion_cientifica' => 'Comunidades o asociación cientifica',
                                      'comunidades_asociacion_profesional' => 'Comunidades o asociación profesional',
                                      'no_pertenece' => 'No pertenece a ningun tipo de comunidad');

        $familia_select = array('0' => '0',
                                 '1' => '1',
                                 '2' => '2',
                                 '3' => '3',
                                 '4' => '4',
                                 '5' => '5',
                                 '6' => '6',
                                 '7' => '7',
                                 '8' => '8',
                                 '9' => '9',
                                 '10' => '10',
                                 'mas_de_10' => 'Mas de 10');

        $entidades_finan = array('cooperativa_uniminuto' => 'Cooperativa Uniminuto',
                                 'icetex' => 'Icetex',
                                 'entidad_bancaria' => 'Entidad Bancaria',
                                 'gobierno_municipal' => 'Gobierno municipal',
                                 'Gobierno_nacional_o_departamental' => 'Gobierno nacional o departamental',
                                 'empresa_donde_usted_o_familiar_trabaja' => 'Empresa donde usted o un familiar trabaja',
                                 'institución_donde_curso_estudios' => 'Institución donde cursó sus estudios',
                                 'ninguna' => 'Ninguna de las anteriores');

        $entidades_beca = array('icetex' => 'Icetex',
                                 'entidad_bancaria' => 'Entidad Bancaria',
                                 'gobierno_municipal' => 'Gobierno municipal',
                                 'gobierno_nacional_o_departamental' => 'Gobierno nacional o departamental',
                                 'empresa_donde_usted_o_familiar_trabaja' => 'Empresa donde usted o un familiar trabaja',
                                 'institución_donde_curso_estudios' => 'Institución donde cursó sus estudios',
                                 'unicatolica' => 'UNICATOLICA',
                                 'ninguna' => 'Ninguna de las anteriores');

        $satisfacion_competencias = array('excelente' => 'Excelente',
                                          'bueno' => 'Bueno',
                                          'regular' => 'Regular',
                                          'deficiente' => 'Deficiente');

        $satisfacion_idioma = array('alto' => 'Alto',
                                    'medio' => 'Medio',
                                    'bajo' => 'Bajo');

        $planes_vida = array('iniciar_nueva_carrera_universitaria' => 'Iniciar una nueva carrera Universitaria',
                             'estudiaria_posgrado_en_colombia' => 'Estudiaría un Posgrado en Colombia',
                             'estudiaria_posgrado_fuera_de_colombia' => 'Estudiaría un Posgrado fuera de Colombia',
                             'trabajar_en_colombia' => 'Trabajar en Colombia',
                             'trabajar_fuera_de_colombia' => 'Trabajar fuera de Colombia');

        $dificultades = array('Falta de recursos económicos y propios' => 'falta_recursos_económicos',
                              'No estar seguro si la idea se pueda convertirse en un negocio exitoso' => 'idea_no_exitosa',
                              'Poco conocimiento para la creación de una empresa' => 'poco_conocimiento',
                              'Difícil acceso a entidades financieras' => 'difícil_acceso_a_entidades_financieras',
                              'Falta de apoyo del gobierno' => 'falta_de_apoyo_del_gobierno',
                              'La costumbre de tener un salario fijo' => 'costumbre_de_tener_salario_fijo',
                              'Miedo para asumir riesgo' => 'miedo_para_asumir_riesgo',
                              'No poder encontrar socios de confianza' => 'no_encontrar_socios_confianza');

        $cursos = array('seminario_cursos' => 'Seminario / cursos',
                        'diplomados' => 'Diplomados',
                        'tecnicos' => 'Técnicos', 
                        'tecnologicos' => 'Tecnológicos',
                        'universitarios' => 'Universitarios',
                        'especialización' => 'Especialización',
                        'maestria' => 'Maestria',
                        'doctorado' => 'Doctorado',
                        'no_aplica' => 'No aplica');

        $apoyos = array('Articulación' => 'articulación',
                        'Cultura' => 'cultura',
                        'Deportes y recreación' => 'deportes_recreación',
                        'Desarrollo humano(psicología)' => 'psicología',
                        'Egresados (talleres y acompañamiento profesional)' => 'egresados_talleres_y_acompañamiento',
                        'Investigaciones' => 'investigaciones',
                        'Permanencia' => 'permanencia',
                        'Proyección social' => 'proyección_social',
                        'Relaciones interinstitucionales' => 'relaciones_interinstitucionales',
                        'Salud integral' => 'salud_integral');

        $parentesco = array('padre' => 'Padre',
                            'madre' => 'Madre',
                            'familiar' => 'Familiar',
                            'amigo' => 'Amigo',
                            'otro' => 'Otro');

        $familia = Familia::SearchPidm($key)->get();

        //$familia = Familia::SearchPidm($key)->get();
        $ope_mes;
        $ope_año;
        //dd($familia);
        $num = 0;

        if (!empty($familia) && $familia->count() > 0) 
        {
          

          foreach($familia as $data)
          {

            $date = explode(' ', $data->updated_at);

          }

            //dd($date);
            
            $date_explode = explode('-', $date[0]);

            $mes = $date_explode[1];
            $año = $date_explode[0];

            $ope_mes = $mes - date("m");

            $ope_año = $año - date("Y");

            //dd($ope_año);

            if ($ope_mes <= 0 && $ope_año > 0) 
            {
              //dd("entre");
              $num = 0;

            } else {

              $num = 1;

            }

        }

        //$identificacion = Identificacion::SearchPidm($key)->get();
        //dd($identificacion->first()->identificacion_numero);
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();

        $nivel = NivelAcademico::orderBy('nivelacademico_nombre', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $cargo = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();
        $tipoEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();
        $contrato = Contrato::orderBy('contrato_nombre', 'ASC')->get();
        $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();
        $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_nombre', 'ASC')->get();
        $ocupacion = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();
        $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();
        $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();
        $vinculacion = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $tamanoEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();
        $logros = Logros::orderBy('logro_nombre', 'ASC')->get();
        $estadosCiviles = EstadoCivil::orderBy('estadocivil_nombre', 'ASC')->get();
        $programas = Programa::orderBy('programa_nombre', 'ASC')->get();
        $sedes = Sede::orderBy('sede_nombre', 'ASC')->get();

        
        return view('estudiantes.egresados.encuesta.survey')
            ->with('estadosCiviles', $estadosCiviles)
            ->with('nivel', $nivel)
            ->with('tamanoEmpresa', $tamanoEmpresa)
            ->with('ciudad', $ciudad)
            ->with('subareas', $subareas)
            ->with('areaPertenece', $areaPertenece)
            ->with('tipoEmpresa', $tipoEmpresa)
            ->with('cargo', $cargo)
            ->with('contrato', $contrato)
            ->with('duracionEmpresa', $duracionEmpresa)
            ->with('ocupacion', $ocupacion)
            ->with('relacionPrograma', $relacionPrograma)
            ->with('situacionLaboral', $situacionLaboral)
            ->with('vinculacion', $vinculacion)
            ->with('rangoSalarial', $rangoSalarial)
            ->with('sectorEmpresa', $sectorEmpresa)
            ->with('logros', $logros)
            ->with('sistemas', $sistemas)
            ->with('sistema', $sistema)
            //->with('identificacion', $identificacion)
            ->with('Begresado', $Begresado)
            ->with('programas', $programas)
            ->with('sedes', $sedes)
            ->with('egresado', $egresado)
            /*->with('estudiante_infoC', $estudiante_infoC)
            ->with('estudiante_infoE', $estudiante_infoE)
            ->with('estudiante_infoR', $estudiante_infoR)
            ->with('estudiante_infoP', $estudiante_infoP)
            ->with('fecha', $fecha)*/
            ->with('genero', $genero)
            ->with('estrato', $estrato)
            ->with('sino', $sino)
            ->with('discapacidad', $discapacidad)
            ->with('comunidad_asociacion', $comunidad_asociacion)
            ->with('familia_select', $familia_select)
            ->with('entidades_finan', $entidades_finan)
            ->with('entidades_beca', $entidades_beca)
            ->with('satisfacion_idioma', $satisfacion_idioma)
            ->with('satisfacion_competencias', $satisfacion_competencias)
            ->with('planes_vida', $planes_vida)
            ->with('dificultades', $dificultades)
            ->with('cursos', $cursos)
            ->with('apoyos', $apoyos)
            ->with('parentesco', $parentesco)
            /*->with('ope_mes', $ope_mes)
            ->with('ope_año', $ope_año)*/
            ->with('num', $num);  
    }


    public function edit(Sistema $sistema, $key)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        //$estudiante_info = BannerMatriculado::Student($key)->get();
        /*$estudiante_infoC = BannerContacto::Student($key)->get();
        $estudiante_infoE = BannerEmail::Student($key)->get();
        $estudiante_infoR = BannerResidencia::Student($key)->get();
        $estudiante_infoP = BannerPersonal::Student($key)->get();

        $fecha = explode(" ", $estudiante_infoP->first()->biop_fec_nmto);*/

        $egresado = Matriculado::find($key);
        

        //dd($egresado);
        //$grado = Grado::scopeGetStudent($key);       
 	  
        $ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();

        $nivel = NivelAcademico::orderBy('nivelacademico_nombre', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $cargo = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();
        $tipoEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();
        $contrato = Contrato::orderBy('contrato_nombre', 'ASC')->get();
        $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();
        $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_nombre', 'ASC')->get();
        $ocupacion = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();
        $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();
        $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();
        $vinculacion = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $tamanoEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();
        $logros = Logros::orderBy('logro_nombre', 'ASC')->get();

        return view('estudiantes.egresados.actualizacion.form')
              ->with('nivel', $nivel)
              ->with('tamanoEmpresa', $tamanoEmpresa)
              ->with('ciudad', $ciudad)
              ->with('subareas', $subareas)
              ->with('areaPertenece', $areaPertenece)
              ->with('tipoEmpresa', $tipoEmpresa)
              ->with('cargo', $cargo)
              ->with('contrato', $contrato)
              ->with('duracionEmpresa', $duracionEmpresa)
              ->with('ocupacion', $ocupacion)
              ->with('relacionPrograma', $relacionPrograma)
              ->with('situacionLaboral', $situacionLaboral)
              ->with('vinculacion', $vinculacion)
              ->with('rangoSalarial', $rangoSalarial)
              ->with('sectorEmpresa', $sectorEmpresa)
              ->with('logros', $logros)
              ->with('sistemas', $sistemas)
              ->with('sistema', $sistema)
              ->with('egresado', $egresado);

    }

    public function update(Request $request, Sistema $sistema, $key)
    {
      
        /*
            $key = pidm
            $request = datos del formulario
            $sistema = los datos del modulo
            $egresado = los datos del egresado
            $grado = los datos del grado del egresado
        */
       try {
          //Se realzia una busqueda con la variable $key al modelo Matriculado para traer al egresado que se va a actualizar
          $egresado = Matriculado::find($key);

          if (empty($egresado)) 
          {

            $egresado = new Matriculado();
            $egresado->matriculado_pidm = $key;

          }

          //de la variable egresados, quien tiene las propiedades del modelo matriculado, se le guardaran los datos del formulario
          
          $egresado->matriculado_primer_nombre = $request->first_name;
          $egresado->matriculado_segundo_nombre = $request->second_name;
          $egresado->matriculado_primer_apellido = $request->last_name_first;
          $egresado->matriculado_segundo_apellido= $request->last_name_second;
          $egresado->matriculado_telefono = $request->tel;
          $egresado->matriculado_celular = $request->cel;
          $egresado->matriculado_email = $request->email;

          $egresado->save();

          //ciudad
          /*$ubicacion->ubicacion_direccion = $request->direccion_estudiante;
          $ubicacion->save();
          $biografia->biografia_genero = $request->genero;
          //estadocivil
          $biografia->biografia_fecha_nacimiento = $request->fecha_nacimiento;
          $biografia->save();*/

          
          if ($request->laboral_situacion != 2) 
          {

              if (!empty($request->name_empresa)) 
              {

                  //Se instacia el modelo a la variable $laboral
                  $laboral = new Laboral();
                  
                  //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                  $laboral->situacionlaboral_id = $request->laboral_situacion;
                  $laboral->laboral_empresa = $request->name_empresa;
                  $laboral->sectorempresa_id = $request->sector;
                  $laboral->tipoempresa_id = $request->tipoEmpresa;
                  $laboral->rangosalario_id = $request->salario;
                  $laboral->nivelcargo_id = $request->cargo;
                  $laboral->areapertenece_id = $request->area;
                  $laboral->ocupacion_id = $request->ocupacion;
                  $laboral->relacionprograma_id = $request->relacionPrograma;
                  $laboral->duracionempresa_id = $request->duracionEmpresa;
                  $laboral->vinculacion_id = $request->vinculacion;
                  $laboral->ciudad_id = $request->ciudad;
                  $laboral->contrato_id = $request->contrato;

                  $laboral->matriculado_pidm = $key;

                  $laboral->save();

              }

          } else {

                  //Se instacia el modelo a la variable $laboral
                  $laboral = new Laboral();
                  
                  //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                  $laboral->situacionlaboral_id = $request->laboral_situacion;
                  $laboral->matriculado_pidm = $key;
                  
                  $laboral->save();
          }
              

          /*
              $estudios = se pueden ingresar mas de dos estudios diferentes
          */

          if (!empty($request->estudio_nivel_estudiante)) 
          {
              $estudio = new Estudio();

              $estudio->nivelacademico_id = $request->estudio_nivel_estudiante;
              $estudio->estudio_institucion = $request->estudio_nombreInstitucion_estudiante;
              $estudio->estudio_titulo = $request->estudio_titulo_estudiante;
              $estudio->estudio_inicio = $request->estudio_inicio_estudiante;
              $estudio->estudio_fin = $request->estudio_fin_estudiante;
              $estudio->matriculado_pidm = $key;

              $estudio->save();    
          }


          $estudios = array();
          $i = 0;

          //Si la pripiedad nombre_empresa_em es diferente a vacio
          if (!empty($request->nombre_empresa_em)) 
          {

              //Instaciamos el modelo Emprendimiento y guardamos las propiedas del request a las propiedades del modelo
              $emprendimiento = new Emprendimiento();
              
              $emprendimiento->emprendimiento_empresa = $request->nombre_empresa_em;
              $emprendimiento->emprendimiento_direccion = $request->direccion_empresa_em;
              $emprendimiento->emprendimiento_telefono = $request->telefono_empresa_em;
              $emprendimiento->sectorempresa_id = $request->sector_em;
              $emprendimiento->tamanoempresa_id = $request->tamano_em;
              $emprendimiento->ciudad_id = $request->ciudad_em;
              $emprendimiento->matriculado_pidm = $key;

              $emprendimiento->save();

          }

          //Si la propiedad reconocimiento_nombre es diferente a vacio
          if (!empty($request->reconocimiento_nombre)) 
          {
              
              //Se instancia el modelo reconocimiento y guardamos las propiedas del request a las propiedades del modelo
              $reconocimiento = new Reconocimiento();

              $reconocimiento->reconocimiento_nombre = $request->reconocimiento_nombre;
              $reconocimiento->reconocimiento_organizacion = $request->reconocimiento_organizacion;
              $reconocimiento->reconocimiento_fecha = $request->reconocimiento_fecha;
              $reconocimiento->matriculado_pidm = $key;
              
              $reconocimiento->save();
              
          }

          $mensaje = '¡Registro Actualizado Exitosamente!'; 
          //$encabezado = 'AVISO!';
          $metodo = 'success';

        } catch (\Illuminate\Database\QueryException  $e){

          $metodo = 'error';
          //$encabezado = '¡ERROR!';
          $mensaje = '¡Error al guardar, Verifica que los campos a actualizar esten bien dijitados';

        }

        Alert::$metodo($mensaje)->persistent("OK"); 

        //Mostramos el mensaje
        

        //Retornamos al formulario con las variables actualizadas
        return redirect()->back();

    }

    public function create(Request $request, Sistema $sistema, $key)
    {
      
        /*
            $key = pidm
            $request = datos del formulario
            $sistema = los datos del modulo
            $egresado = los datos del egresado
            $grado = los datos del grado del egresado
        */
        try {
          //Se realzia una busqueda con la variable $key al modelo Matriculado para traer al egresado que se va a actualizar
          $egresado = Matriculado::find($key);

          if (empty($egresado)) 
          {

            $egresado = new Matriculado();
            $egresado->matriculado_pidm = $key;

          }
          
          $egresado->matriculado_primer_nombre = $request->first_name;
          $egresado->matriculado_segundo_nombre = $request->second_name;
          $egresado->matriculado_primer_apellido = $request->last_name_first;
          $egresado->matriculado_segundo_apellido= $request->last_name_second;
          $egresado->matriculado_telefono = $request->tel;
          $egresado->matriculado_celular = $request->cel;
          $egresado->matriculado_email = $request->email;
          $egresado->ciudad_id = $request->ciudad_residencia;
          $egresado->matriculado_direccion = $request->direccion_estudiante;
          $egresado->matriculado_fecha_nacimiento = $request->fecha_nacimiento;
          $egresado->matriculado_estrato = $request->estrato;
          $egresado->matriculado_genero = $request->genero;
          $egresado->estadocivil_id = $request->estado_civil;    
        

          $egresado->save();
          
          /*
          * Tabla academico
          */

          $academico = new Academico();

          $academico->academico_codigo = $request->codigo_estudiante;
          $academico->convenio_id = $request->Convenio_academico;
          $academico->sede_id = $request->sede_academico;
          $academico->programa_id = $request->Programa_academico;
          $academico->matriculado_pidm = $key;

          $academico->save();

          /*
          * Tabla Censal
          */

          $censal = new Censal();

          $censal->censa_poblacion_necesidades = $request->censal_necesidades_edu;
          $censal->censal_descapacidad = $request->censal_discapacidad;
          $censal->censal_patrocinio = $request->censal_patrocino;
          $censal->matriculado_pidm = $key;

          if ($request->censal_patrocino == 'si') 
          {

             $censal->censal_patrocinio_empresa = $request->censal_name_empresa;

          }

          $censal->save();

          //$censal_row = Censal::GetRawCensal($key);

          
          if (!empty($request->cs_comunidad)) 
          {
            $comunidades = array();
            $i = 0;
            foreach ($request->cs_comunidad as $comunidad) 
            {
              
              $comunidades[] = ['matriculado_pidm' => $key,
                                'comunidad' => $request->cs_comunidad[$i]];

              ++$i;

            }

            $ii = 0;
            $comunidad = new Comunidad();

            foreach ($request->cs_comunidad as $data) 
            {

              $comunidad->create($comunidades[$ii]);

              ++$ii;

            }

          }

          /*
          * Tabla familia
          */

          if (!empty($egresado->familia)) 
          {

            $egresado->familia->familia_integrantes = $request->famila_intigrantes;
            $egresado->familia->familia_hijos = $request->famila_hijos;
            $egresado->familia->familia_hermanos = $request->famila_hermanos;
            $egresado->familia->familia_hermanos_estudios_superiores = $request->famila_hermanos_superiores;
            $egresado->familia->matriculado_pidm = $key;

            $egresado->familia->save();

          } else {


            $familiar = new Familia();

            $familiar->familia_integrantes = $request->famila_intigrantes;
            $familiar->familia_hijos = $request->famila_hijos;
            $familiar->familia_hermanos = $request->famila_hermanos;
            $familiar->familia_hermanos_estudios_superiores = $request->famila_hermanos_superiores;
            $familiar->matriculado_pidm = $key;

            $familiar->save();

          }
          

          /*
          * Tabla financiacion credito(financiacion)
          */

          $creditos = array();
          $c = 0;

          if ($request->financiacion_patrocino == 'si') 
          {

            

            foreach ($request->cs_finan as $credito) 
            {
              
              $creditos[] = ['financiacion_financiacion' => $request->financiacion_patrocino,
                             'financiacion_financiacion_entidad' => $request->cs_finan[$c],
                             'matriculado_pidm' => $key];

              ++$c;

            }

            $cc = 0;
            $credito = new Credito();

            foreach ($request->cs_finan as $data) 
            {

              $credito->create($creditos[$cc]);

              ++$cc;

            }

          } else {

            $credito = new Credito();

            $credito->financiacion_financiacion = $request->financiacion_patrocino;
            $credito->matriculado_pidm = $key;

            $credito->save();

          }

          /*
          * Tabla beca
          */

          $becas = array();
          $b = 0;

          if ($request->financiacion_beca == 'si') 
          {

            

            foreach ($request->cs_beca as $beca) 
            {
              
              $becas[] = ['financiacion_beca' => $request->financiacion_beca,
                             'financiacion_beca_entidad' => $request->cs_beca[$b],
                             'matriculado_pidm' => $key];

              ++$b;

            }

            $bb = 0;
            $beca = new Beca();

            foreach ($request->cs_beca as $data) 
            {

              $beca->create($becas[$bb]);

              ++$bb;

            }

          } else {

            $beca = new Beca();

            $beca->financiacion_beca = $request->financiacion_beca;
            $beca->matriculado_pidm = $key;

            $beca->save();

          }
          
          /*
          * Tabla satisfacion competencias
          */

          $satisfacion_competencias = new Satisfacion_Competencias();

          $satisfacion_competencias->matriculado_pidm = $key;
          $satisfacion_competencias->competencias_convivencia = $request->satisfacion_culcon;
          $satisfacion_competencias->competencias_liderazgo = $request->satisfacion_lider;
          $satisfacion_competencias->competencias_trabajo_equipo = $request->satisfacion_traequi;
          $satisfacion_competencias->competencias_manejo_discurso = $request->satisfacion_manedisc;
          $satisfacion_competencias->competencias_analisis = $request->satisfacion_capanacri;
          $satisfacion_competencias->competencias_comunicacion = $request->satisfacion_comuvernover;
          $satisfacion_competencias->competencias_adaptabilidad = $request->satisfacion_adapca;
          $satisfacion_competencias->competencias_creatividad = $request->satisfacion_creinno;
          $satisfacion_competencias->competencias_desiciones = $request->satisfacion_todeci;
          $satisfacion_competencias->competencias_investigacion = $request->satisfacion_inves;

          $satisfacion_competencias->save();

           /*
          * Tabla ingles competencias
          */

           $ingles_competencias = new Ingles_Competencias();

           $ingles_competencias->matriculado_pidm = $key;
           $ingles_competencias->inglescompetencias_habla = $request->satisfacion_inhabla;
           $ingles_competencias->inglescompetencias_escucha = $request->satisfacion_inescucha;
           $ingles_competencias->inglescompetencias_lectura = $request->satisfacion_inlectura;
           $ingles_competencias->inglescompetencias_escritura = $request->satisfacion_inescritura;

           $ingles_competencias->save();

           /*
          * Tabla pla de vida
          */

          $pvida = array();
          $pv = 0;

          foreach ($request->cs_plan as $plan) 
          {
            
            $pvida[] = ['planvida_plan' => $request->cs_plan[$pv],
                         'matriculado_pidm' => $key];

            ++$pv;

          }

          $pvi = 0;
          $plan_vida = new Plan_Vida();

          foreach ($request->cs_plan as $data) 
          {

            $plan_vida->create($pvida[$pvi]);

            ++$pvi;

          }

          /*
          * Tabla idea empresa  
          */

          $idea_empresa = new Idea_Empresa();

          if ($request->empresa_idea == 'si') 
          {

            $idea_empresa->matriculado_pidm = $key;
            $idea_empresa->crear_empresa = $request->empresa_idea;
            $idea_empresa->crearempresa_idea = $request->idea_company;
            $idea_empresa->sectorempresa_id = $request->idea_company_sector;
            $idea_empresa->crearempresa_dificultad = $request->dificultad_crear_empresa;

            $idea_empresa->save();

          } else {

            $idea_empresa->matriculado_pidm = $key;
            $idea_empresa->crear_empresa = $request->empresa_idea;
            $idea_empresa->crearempresa_dificultad = $request->dificultad_crear_empresa;

            $idea_empresa->save();

          }

          /*
          * Tabla laboral
          */

          //Se instacia el modelo a la variable $laboral
          $laboral = new Laboral();

          if ($request->laboral_situacion != 2) 
          {

              if (!empty($request->name_empresa)) 
              {
                  
                  //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                  $laboral->situacionlaboral_id = $request->laboral_situacion;
                  $laboral->laboral_empresa = $request->name_empresa;
                  $laboral->sectorempresa_id = $request->sector;
                  $laboral->tipoempresa_id = $request->tipoEmpresa;
                  $laboral->rangosalario_id = $request->salario;
                  $laboral->nivelcargo_id = $request->cargo;
                  $laboral->areapertenece_id = $request->area;
                  $laboral->ocupacion_id = $request->ocupacion;
                  $laboral->relacionprograma_id = $request->relacionPrograma;
                  $laboral->duracionempresa_id = $request->duracionEmpresa;
                  $laboral->vinculacion_id = $request->vinculacion;
                  $laboral->ciudad_id = $request->ciudad;
                  $laboral->contrato_id = $request->contrato;

                  $laboral->matriculado_pidm = $key;

                  $laboral->save();

              }

          } else {
                  
                  //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                  $laboral->situacionlaboral_id = $request->laboral_situacion;
                  $laboral->matriculado_pidm = $key;
                  
                  $laboral->save();
          }

          /*
          * Tabla busca trabajo
          */
          
          $trabajo = new Busca_Trabajo();

          if ($request->busca_trabajo == 'si') 
          {
            
            $trabajo->matriculado_pidm = $key;
            $trabajo->buscatrabajo_1ervez = $request->busca_trabajo_primeVez;
            $trabajo->buscatrabajo_dificultad = $request->dificultad_trabajo;
            $trabajo->buscatrabajo_tiempo = $request->buscando_trabajo;
            $trabajo->buscatrabajo_canalbusqueda = $request->canalbusqueda_trabajo;

            $trabajo->save();

            $laboral->situacionlaboral_id = 2;
            $laboral->matriculado_pidm = $key;
            
            $laboral->save();

          }

          /*
          * Tabla Nivel Identidad
          */

          $nivel_identidad = new Nivel_Identidad();

          $nivel_identidad->matriculado_pidm = $key;
          $nivel_identidad->nivelidentidad_estudiar = $request->volveria_estudiar;
          $nivel_identidad->nivelidentidad_volver = $request->razon_si_estudiar;
          $nivel_identidad->nivelidentidad_novolver = $request->razon_no_estudiar;
          $nivel_identidad->nivelidentidad_recomendacion = $request->recomendar;

          if ($request->recomendar == 'no') 
          {

           $nivel_identidad->nivelidentidad_norazon = $request->razon_no_recomendar;

          }

          $nivel_identidad->save();

          $oestudios = array();
          $o = 0;       

          foreach ($request->otros_estudios as $estudios) 
          {
            
            $oestudios[] = ['otro_estudio' => $request->otros_estudios[$o],
                            'matriculado_pidm' => $key];

            ++$o;

          }

          $oe = 0;
          $niOestudio = new NivelIdentidad_Estudios();

          foreach ($request->otros_estudios as $data) 
          {

            $niOestudio->create($oestudios[$oe]);

            ++$oe;

          }

          $apoyos = array();
          $a = 0;       

          foreach ($request->eapoyo as $apoyo) 
          {
            
            $apoyos[] = ['apoyo' => $request->eapoyo[$a],
                         'matriculado_pidm' => $key];

            ++$a;

          }

          $aa = 0;
          $niApoyo = new NivelIdentidad_Apoyo();

          foreach ($request->eapoyo as $data) 
          {

            $niApoyo->create($apoyos[$aa]);

            ++$aa;

          }

          /*
          * Tabla Satisfacion
          */
          
          $msatisfacion = new Satisfacion();


          $msatisfacion->matriculado_pidm = $key;
          $msatisfacion->satisfacion_salones = $request->satisfacion_salones;
          $msatisfacion->satisfacion_laboratorios = $request->satisfacion_talleres;
          $msatisfacion->satisfacion_espacios = $request->satisfacion_espacios;
          $msatisfacion->satisfacion_audiovisuales = $request->satisfacion_audiovisual;
          $msatisfacion->satisfacion_aulasinformatica = $request->satisfacion_aulas_informatica;
          $msatisfacion->satisfacion_espaciosdeporte = $request->satisfacion_practica_deportiva;
          $msatisfacion->satisfacion_redessociales = $request->satisfacion_redes_sociales;
          $msatisfacion->satisfacion_biblioteca = $request->satisfacion_biblioteca;
          $msatisfacion->satisfacion_medioscomunicacion = $request->satisfacion_medio_comunicacion;
          $msatisfacion->satisfacion_actividades = $request->satisfacion_actividades_artisticas;

          $msatisfacion->save();

          /*
          * $estudios = se pueden ingresar mas de dos estudios diferentes
          */

          if (!empty($request->estudio_nivel_estudiante)) 
          {
              $estudio = new Estudio();

              $estudio->nivelacademico_id = $request->estudio_nivel_estudiante;
              $estudio->estudio_institucion = $request->estudio_nombreInstitucion_estudiante;
              $estudio->estudio_titulo = $request->estudio_titulo_estudiante;
              $estudio->estudio_inicio = $request->estudio_inicio_estudiante;
              $estudio->estudio_fin = $request->estudio_fin_estudiante;
              $estudio->matriculado_pidm = $key;

              $estudio->save();    
          }

          $estudios = array();
          $i = 0;

          /*
          * Tabla emprendimiento
          */
          //Si la pripiedad nombre_empresa_em es diferente a vacio
          if (!empty($request->nombre_empresa_em)) 
          {

              //Instaciamos el modelo Emprendimiento y guardamos las propiedas del request a las propiedades del modelo
              $emprendimiento = new Emprendimiento();
              
              $emprendimiento->emprendimiento_empresa = $request->nombre_empresa_em;
              //$emprendimiento->emprendimiento_direccion = $request->direccion_empresa_em;
              $emprendimiento->emprendimiento_telefono = $request->telefono_empresa_em;
              $emprendimiento->sectorempresa_id = $request->sector_em;
              $emprendimiento->tamanoempresa_id = $request->tamano_em;
              $emprendimiento->ciudad_id = $request->ciudad_em;
              $emprendimiento->matriculado_pidm = $key;

              $emprendimiento->save();

          }

          /*
          * Tabla reconocimiento
          */
          //Si la propiedad reconocimiento_nombre es diferente a vacio
          if (!empty($request->reconocimiento_nombre)) 
          {
              
              //Se instancia el modelo reconocimiento y guardamos las propiedas del request a las propiedades del modelo
              $reconocimiento = new Reconocimiento();

              $reconocimiento->reconocimiento_nombre = $request->reconocimiento_nombre;
              $reconocimiento->reconocimiento_organizacion = $request->reconocimiento_organizacion;
              $reconocimiento->reconocimiento_fecha = $request->reconocimiento_fecha;
              $reconocimiento->matriculado_pidm = $key;
              
              $reconocimiento->save();

          }

          /*
          * Tabla refencia
          */

          $referencia = new Referencia();

          $referencia->matriculado_pidm = $key;
          $referencia->referencia_nombres = $request->referencia_nombre;
          $referencia->referencia_parentesco = $request->referencia_parentesco;
          $referencia->referencia_telefono = $request->referencia_telefono;
          $referencia->referencia_celular = $request->referencia_celular;

          $referencia->save();

          $mensaje = '¡Datos ingresados exitosamente!'; 
          $metodo = 'success';

        } catch (\Illuminate\Database\QueryException  $e){

          $metodo = 'error';
          $mensaje = 'Se ha producido un problema al guardar';

        }

          Alert::$metodo($mensaje)->persistent("OK"); 
          
          return redirect()->back();

    }

    public function pdf($pidm)
    {

        $matriculado = Matriculado::find($pidm);
        $programa = Programa::SearchProgram($matriculado->academico->first()->programa_id)->get();
        //$estudiante_info = BannerPersonal::Student($pidm)->get();
        //dd($estudiante_info);


        //dd($programa);

        $pdf = \PDF::loadView('estudiantes.egresados.pdf.pdf', ['matriculado' => $matriculado,
                                                                'programa' => $programa]);

        return $pdf->download('Certificado.pdf');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

use App\Sistema;
use App\Entrance;
use App\Area;
use App\Usuario;
use App\Rol;
use App\Matriculado;

use App\Model\Certificados\Solicitud;

class EstudianteController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('estudiante');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->whereIn('sistema_nombre_corto', ['egresados',
                                                                                                'grados', 
                                                                                                'certificados', 
                                                                                                'directorio', 
                                                                                                'cancelaciones'])->paginate(6);    
        return view('estudiantes.home')
        ->with('sistemas', $sistemas);
    }

    /**
     * Show contac form.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('estudiantes.contact');
    }

    /**
     * Show student profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('estudiantes.profile');
    }

    public function certificados(Request $request, Sistema $sistema, Matriculado $matriculado)
    {
        $solicitud = Solicitud::orderBy('solicitud_id')
        ->Buscar(\Auth::guard('estudiante')->user()->matriculado_id, $request->busqueda)
        ->first();

        return view('estudiantes.certificados.menu')
        ->with('matriculado', $matriculado)
        ->with('solicitud', $solicitud)
        ->with('sistema', $sistema);
    }

    public function directorio(Matriculado $matriculado)
    {
        return view('estudiantes.directorio.menu')
        ->with('matriculado', $matriculado);
    }

    public function cancelaciones(Sistema $sistema, Matriculado $matriculado)
    {
        return view('estudiantes.cancelaciones.menu')
        ->with('matriculado', $matriculado)
        ->with('sistema', $sistema);
    }
}

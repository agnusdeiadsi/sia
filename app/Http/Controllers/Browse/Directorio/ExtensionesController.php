<?php

namespace App\Http\Controllers\Browse\Directorio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

use App\Extension;
use App\Sistema;
use App\Entrance;

use App\Model\Correo\Cuenta;

class ExtensionesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function directory(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        /*$extensiones = Extension::orderBy('extension_extension', 'ASC')
        ->join('sia_areas', 'sia_extensiones.area_id', '=', 'sia_areas.area_id')
        ->join('sia_subareas', 'sia_extensiones.subarea_id', '=', 'sia_subareas.subarea_id')
        ->join('correos_cuentas', 'sia_extensiones.extension_id', '=', 'correos_cuentas.extension_id')
        ->Buscar($request->busqueda)
        ->paginate(25);*/

        $extensiones = Extension::orderBy('extension_extension', 'ASC')
        ->join('app_correos.correos_cuentas', 'sia_extensiones.extension_id', '=', 'app_correos.correos_cuentas.extension_id')
        ->join('sia_subareas', 'app_correos.correos_cuentas.subarea_id', '=', 'sia_subareas.subarea_id')
        ->join('sia_areas', 'sia_subareas.area_id', '=', 'sia_areas.area_id')
        ->join('sia_sedes', 'app_correos.correos_cuentas.sede_id', '=', 'sia_sedes.sede_id')
        ->join('sia_cargos', 'app_correos.correos_cuentas.cargo_id', '=', 'sia_cargos.cargo_id')
        ->Buscar($request->busqueda)
        ->paginate(50);

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Vista directorio telefónico'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('browse.directorio.directory')
        ->with('extensiones', $extensiones)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $extensiones = Extension::orderBy('extension_extension', 'ASC')
        //->Buscar($request->busqueda)
        ->paginate(50);

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Vista extensiones'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('browse.directorio.index')
        ->with('extensiones', $extensiones)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Crear extensión'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('browse.directorio.create')
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
        $this->validate($request, [
            'extension_extension' => 'unique:sia_extensiones',
        ]);

        $extension = new Extension($request->all());
        $extension->save();

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Extensión '.$extension->extension_extension.' creada'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        Alert::success('<p>La extensión <b>'.$extension->extension_extension.'</b> ha sido creada exitosamente.</p>', '¡Muy bien!')->html()->persistent();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Alert;

use App\Sistema;
use App\Entrance;
use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Respuesta;
use App\Model\Cancelaciones\Estado;
use App\Model\Cancelaciones\Tipo;
use App\Model\Cancelaciones\Adjunto;

class SolicitudController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        if(\Auth::user()->rol_sia == 'Administrador' || \Auth::user()->rol_modulo == 'Manager')
        {
            $solicitudes = Solicitud::orderBy('created_at', 'ASC')->paginate(25);
        }
        else
        {
            if($request->filtro == 1)
            {
                $solicitudes = DB::connection('cancelaciones')->table('cancelaciones_solicitudes')
                ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                        ->from('cancelaciones_autorizaciones')
                        ->whereRaw('cancelaciones_autorizaciones.usuario_id ='.\Auth::user()->id)
                        ->whereRaw('cancelaciones_autorizaciones.tipo_id = cancelaciones_autorizaciones.tipo_id')
                        ->whereRaw('cancelaciones_autorizaciones.estado_id = 1');
                })->paginate(25);

                //guarda los registros del usuario en sia_entrances
                $sistema->entrances()->create([
                                            'usuario_id' => \Auth::user()->id,
                                            'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                            'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                            'entrance_url_anterior' => url()->previous(),
                                            'entrance_url_actual' => url()->current(),
                                            'entrance_accion' => nl2br('Solicitudes: vista solicitudes en Procesamiento'),  
                                            'entrance_startdate' => date('Y-m-d H:i:s'),
                                          ]);

            }
            elseif($request->filtro == 2)
            {
                $solicitudes = DB::connection('cancelaciones')->table('cancelaciones_solicitudes')
                ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                        ->from('cancelaciones_autorizaciones')
                        ->whereRaw('cancelaciones_autorizaciones.usuario_id ='.\Auth::user()->id)
                        ->whereRaw('cancelaciones_autorizaciones.tipo_id = cancelaciones_autorizaciones.tipo_id')
                        ->whereRaw('cancelaciones_autorizaciones.estado_id = 2');
                })->paginate(25);

                //guarda los registros del usuario en sia_entrances
                $sistema->entrances()->create([
                                            'usuario_id' => \Auth::user()->id,
                                            'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                            'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                            'entrance_url_anterior' => url()->previous(),
                                            'entrance_url_actual' => url()->current(),
                                            'entrance_accion' => nl2br('Solicitudes: vista solicitudes en Firmas'),  
                                            'entrance_startdate' => date('Y-m-d H:i:s'),
                                          ]);
            }

            elseif($request->filtro == 3)
            {
                $solicitudes = DB::connection('cancelaciones')->table('cancelaciones_solicitudes')
                  ->whereExists(function ($query) {
                      $query->select(DB::raw(1))
                            ->from('cancelaciones_autorizaciones')
                            ->whereRaw('cancelaciones_autorizaciones.usuario_id ='.\Auth::user()->id)
                            ->whereRaw('cancelaciones_autorizaciones.tipo_id = cancelaciones_autorizaciones.tipo_id')
                            ->whereRaw('cancelaciones_autorizaciones.estado_id = 3');
                  })->paginate(25);

                //guarda los registros del usuario en sia_entrances
                $sistema->entrances()->create([
                                                'usuario_id' => \Auth::user()->id,
                                                'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                                'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                                'entrance_url_anterior' => url()->previous(),
                                                'entrance_url_actual' => url()->current(),
                                                'entrance_accion' => nl2br('Solicitudes: vista solicitudes Procesadas'),  
                                                'entrance_startdate' => date('Y-m-d H:i:s'),
                                            ]);
            }
        }

        //dd($solicitudes);

        return view('browse.cancelaciones.solicitudes.index')
        ->with('solicitudes', $solicitudes)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sistema $sistema, Solicitud $solicitud)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.cancelaciones.solicitudes.show')
        ->with('solicitud', $solicitud)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Process the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        //dd($request->filtro);

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $solicitudes = Solicitud::orderBy('created_at', 'ASC')->paginate(50);

        //actualiza el estado de la solicitud
        $solicitud->estado_id = 2; //estado 2: firmas
        $solicitud->save();

        //------------------------------------

        //Guarda el documento de respuesta PDF
        $archivo = $request->file('respuesta_dc');
        $adjunto_nombre = $archivo;
        $adjunto_codigo = time().'.'.$adjunto_nombre->getClientOriginalExtension();
        $path = public_path().'/files/cancelaciones/respuestas/';
        $adjunto_nombre->move($path, $adjunto_codigo);

        $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
        $adjunto_nombre = $adjunto_nombre[0];

        //-----------------------------------------------

        $respuesta = new Respuesta($request->all());
        $respuesta->respuesta_nombre = $adjunto_nombre;
        $respuesta->respuesta_dc = $adjunto_codigo;
        $respuesta->usuario_id = \Auth::user()->id;
        $respuesta->solicitud()->associate($solicitud);
        $respuesta->save();

        Alert::success('La solicitud <b>'.$respuesta->solicitud->solicitud_id.'</b> ha sido procesada correctamente.', 'Notificación')->html()->persistent("OK");

        return view('browse.cancelaciones.solicitudes.index')
        ->with('solicitudes', $solicitudes)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }
}

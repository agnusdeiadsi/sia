<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Alert;

use App\Sistema;
use App\Usuario;
use App\Model\Cancelaciones\Tipo;
use App\Model\Cancelaciones\Estado;
use App\Model\Cancelaciones\Autorizacion;

class AutorizacionController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $autorizaciones = Autorizacion::paginate(25);

        $usuarios = Usuario::paginate(25);

        return view('browse.cancelaciones.autorizaciones.index')
        ->with('usuarios', $usuarios)
        ->with('autorizaciones', $autorizaciones)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $estados = Estado::orderBy('estado_nombre', 'ASC')->get();
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->get();
        $usuarios = Usuario::get();

        return view('browse.cancelaciones.autorizaciones.create')
        ->with('usuarios', $usuarios)
        ->with('tipos', $tipos)
        ->with('estados', $estados)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'usuario_id' => 'unique:cancelaciones.cancelaciones_autorizaciones,usuario_id,null,null,tipo_id,'.$request->tipo_id.'',
        ]);

        $autorizacion = new Autorizacion($request->all());
        $autorizacion->save();

        Alert::success('<i>La nueva autorización ha sido definida</i>', 'Notificación')->html()->persistent("OK");

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->validate($request, [
            'usuario_id' => 'unique:cancelaciones.cancelaciones_autorizaciones,usuario_id,null,null,tipo_id,'.$request->tipo_id.'',
        ]);

        $autorizacion = new Autorizacion($request->all());
        $autorizacion->save();

        //flash()->overlay('La nueva autorización ha sido definida', 'Notificación');

        Alert::success('La autorización ha sido actualizada', 'Notificación');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

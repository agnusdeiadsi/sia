<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Mail;

use App\Sistema;

use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Soporte;

class SoporteController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        $this->validate($request, [
            'soporte' => 'required',
        ]);

        //dd(\Auth()->user());

        $soporte_nombre = $request->file('soporte');
        $soporte_codigo = time().'.'.$soporte_nombre->getClientOriginalExtension();
        $path = public_path().'/files/cancelaciones/soportes/';
        $soporte_nombre->move($path, $soporte_codigo);

        $soporte_nombre = explode('.', $soporte_nombre->getClientOriginalName());
        $soporte_nombre = $soporte_nombre[0];

        $soporte = new Soporte();
        $soporte->soporte_codigo = $soporte_codigo;
        $soporte->soporte_nombre = $soporte_nombre;
        $soporte->soporte_descripcion = $request->soporte_descripcion;
        $soporte->usuario()->associate(\Auth()->user());
        $soporte->solicitud()->associate($solicitud);
        $soporte->save();

        /*flash()->overlay('Su solicitud <b>'.$solicitud->solicitud_id.'</b> ha sido guardada correctamente, un correo de confirmación ha sido enviado a su cuenta <b>'.$request->matriculado_email.'</b>. El estado actual es <i>en procesamiento</i>');*/

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Download the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Sistema $sistema, Soporte $soporte)
    {        
        $fichero = 'files/'.$sistema->sistema_nombre_corto.'/soportes/'.$soporte->soporte_codigo;
        
        return response()->download($fichero);
    }
}

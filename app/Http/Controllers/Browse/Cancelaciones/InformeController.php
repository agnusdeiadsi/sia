<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Alert;

use App\Sistema;
use App\Entrance;

use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Tipo;
use App\Model\Cancelaciones\Estado;

class InformeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.cancelaciones.informes.index')
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function requests(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $periodos = DB::connection('cancelaciones')->select('SELECT COUNT(solicitud_id) AS total, IF(SUBSTRING(created_at, 6, 2) <= 6, CONCAT(SUBSTRING(created_at, 1, 4),1), CONCAT(SUBSTRING(created_at, 1, 4),2)) AS periodo FROM cancelaciones_solicitudes GROUP BY periodo');

        $periodos = collect($periodos);

        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->get();

        return view('browse.cancelaciones.informes.charts.linechart-requestsallperiods')
        ->with('tipos', $tipos)
        ->with('periodos', $periodos)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function requestsFilter(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $periodos = DB::connection('cancelaciones')->select('SELECT IF(SUBSTRING(created_at, 6, 2) <= 06, CONCAT(SUBSTRING(created_at, 1, 4),1), CONCAT(SUBSTRING(created_at, 1, 4),2)) AS periodo FROM cancelaciones_solicitudes GROUP BY periodo');
        $periodos = collect($periodos);
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->get();
        $estados = Estado::orderBy('estado_nombre', 'ASC')->get();

        if($request->periodo == null && $request->tipo == null)
        {
            Alert::error('No has seleccionado ninguna opción de filtro', ' ')->html()->persistent('OK');

            return redirect()->back();
        }
        elseif($request->periodo != null && $request->tipo == null)
        {
            $datos = DB::connection('cancelaciones')->select('SELECT COUNT(solicitud_id) AS total, tipo_id AS tipo, IF(SUBSTRING(created_at, 6, 2) <= 06, CONCAT(SUBSTRING(created_at, 1, 4),1), CONCAT(SUBSTRING(created_at, 1, 4),2)) AS periodo  FROM cancelaciones_solicitudes GROUP BY tipo, periodo');

            $datos = collect($datos);
            $datos = $datos->where('periodo', $request->periodo);

            if(count($datos) != null)
            {
                return view('browse.cancelaciones.informes.charts.barchart-requeststype')
                ->with('datos', $datos)
                ->with('tipos', $tipos)
                ->with('periodos', $periodos)
                ->with('sistema', $sistema)
                ->with('sistemas', $sistemas);
            }
            else
            {
                Alert::info('No se encontraron registros', ' ')->html()->persistent('OK');

                return redirect()->back();
            }
        }
        elseif($request->periodo == null && $request->tipo != null)
        {
            $datos = DB::connection('cancelaciones')->select('SELECT COUNT(solicitud_id) AS total, IF(SUBSTRING(created_at, 6, 2) <= 06, CONCAT(SUBSTRING(created_at, 1, 4),1), CONCAT(SUBSTRING(created_at, 1, 4),2)) AS periodo FROM cancelaciones_solicitudes WHERE tipo_id = '.$request->tipo.' GROUP BY periodo');

            $datos = collect($datos);

            if(count($datos) != null)
            {
                return view('browse.cancelaciones.informes.charts.requests-barchart')
                ->with('datos', $datos)
                ->with('tipos', $tipos)
                ->with('periodos', $periodos)
                ->with('sistema', $sistema)
                ->with('sistemas', $sistemas);
            }
            else
            {
                Alert::info('No se encontraron registros', ' ')->html()->persistent('OK');

                return redirect()->back();
            }
        }
        elseif($request->periodo != null && $request->tipo != null)
        {
            $datos = DB::connection('cancelaciones')->select('SELECT COUNT(solicitud_id) AS total, tipo_id AS tipo, estado_id AS estado, IF(SUBSTRING(created_at, 6, 2) <= 06, CONCAT(SUBSTRING(created_at, 1, 4),1), CONCAT(SUBSTRING(created_at, 1, 4),2)) AS periodo  FROM cancelaciones_solicitudes GROUP BY estado, tipo, periodo');

            $datos = collect($datos);
            $datos = $datos->where('periodo', $request->periodo)->where('tipo', $request->tipo);

            if(count($datos) != null)
            {
                return view('browse.cancelaciones.informes.charts.barchart-requestsstate')
                ->with('datos', $datos)
                ->with('tipos', $tipos)
                ->with('estados', $estados)
                ->with('periodos', $periodos)
                ->with('sistema', $sistema)
                ->with('sistemas', $sistemas);
            }
            else
            {
                Alert::info('No se encontraron registros', ' ')->html()->persistent('OK');

                return redirect()->back();
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

use App\Sistema;
use App\Model\Cancelaciones\Apelacion;
use App\Model\Cancelaciones\Respuesta;

class ApelacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $apelaciones = Apelacion::orderBy('created_at', 'ASC')->where('estado_id', 1)->paginate(50);

        return view('browse.cancelaciones.apelaciones.index')
        ->with('apelaciones', $apelaciones)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Sistema $sistema, Apelacion $apelacion)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.cancelaciones.apelaciones.show')
        ->with('apelacion', $apelacion)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request, Sistema $sistema, Apelacion $apelacion)
    {      
        $this->validate($request, [
            'respuesta_dc' => 'mimes:pdf|max:5048'
        ]);

        //actualiza el estado de la solicitud
        $apelacion->estado_id = 3; //estado 3: procesada
        $apelacion->save();

        //------------------------------------

        //Guarda el documento de respuesta PDF
        $archivo = $request->file('respuesta_dc');
        $adjunto_nombre = $archivo;
        $adjunto_codigo = time().'.'.$adjunto_nombre->getClientOriginalExtension();
        $path = public_path().'/files/cancelaciones/respuestas/';
        $adjunto_nombre->move($path, $adjunto_codigo);

        $adjunto_nombre = explode('.', $adjunto_nombre->getClientOriginalName());
        $adjunto_nombre = $adjunto_nombre[0];

        //-----------------------------------------------

        $respuesta = new Respuesta($request->all());
        $respuesta->respuesta_nombre = $adjunto_nombre;
        $respuesta->respuesta_dc = $adjunto_codigo;
        $respuesta->usuario_id = \Auth::user()->id;
        $respuesta->apelacion()->associate($apelacion);
        $respuesta->save();

        Alert::success('La apelación ha sido procesada correctamente. Un correo de notificación ha sido enviado al solicitante')->html()->persistent('Cerrar');

        return redirect()->route('browse.cancelaciones.apelaciones.index', $sistema);
    }
}

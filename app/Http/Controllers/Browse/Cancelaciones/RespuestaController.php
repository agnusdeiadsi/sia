<?php

namespace App\Http\Controllers\Browse\Cancelaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

use App\Sistema;
use App\Model\Cancelaciones\Solicitud;
use App\Model\Cancelaciones\Respuesta;

class RespuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Sign the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sign(Request $request, Sistema $sistema, Respuesta $respuesta)
    {
        if($respuesta->respuesta_firma_uno == null)
        {
            $respuesta->respuesta_firma_uno = \Auth::user()->id;
            $respuesta->update();
        }
        else
        {
            $respuesta->respuesta_firma_dos = \Auth::user()->id;
            $respuesta->update();
        }

        if($respuesta->respuesta_firma_uno != null && $respuesta->respuesta_firma_dos != null)
        {
            $solicitud = $respuesta->solicitud;

            $solicitud->estado_id = 3;
            $solicitud->update();
        }

        //dd($respuesta);

        Alert::success('La solicitud <b>'.$respuesta->solicitud->solicitud_id.'</b> ha sido firmada.', 'Notificación')->html()->persistent("OK");

        return redirect()->route('browse.cancelaciones.solicitudes.index', [$sistema, 'filtro' => $request->filtro]);
    }

    /**
     * Download the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Sistema $sistema, Respuesta $respuesta)
    {        
        $fichero = 'files/'.$sistema->sistema_nombre_corto.'/respuestas/'.$respuesta->respuesta_dc;
        
        return response()->download($fichero);
    }
}

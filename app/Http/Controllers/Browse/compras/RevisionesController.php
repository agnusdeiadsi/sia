<?php

namespace App\Http\Controllers\Browse\Compras;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Alert;

use App\Sistema;
use App\Usuario;
use App\Area;
use App\Subarea;

class RevisionesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sistema $sistema, Usuario $usuario)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $usuario_cuenta = $usuario->cuenta;
        $usuario_area = DB::connection('compras')->table('compras_revisiones')->select('area_id')->distinct()->where('usuario_id_area', '=', $usuario->id)->get(); //$usuario->revisionesAreas->select('area_id')->distinct();

        $usuario_subarea = DB::connection('compras')->table('compras_revisiones')->select('subarea_id')->distinct()->where('usuario_id_subarea', '=', $usuario->id)->get(); //$usuario->revisionesSubareas;

        $areas = Area::orderBy('area_nombre', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar revisiones usuario: '.$usuario->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('browse.compras.revisiones.edit')
        ->with('areas', $areas)
        ->with('subareas', $subareas)
        ->with('usuario', $usuario)
        ->with('usuario_area', $usuario_area)
        ->with('usuario_subarea', $usuario_subarea)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema, Usuario $usuario)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        if($request->area_id != null)
        {
            foreach($request->area_id as $areas)
            {
                $query = DB::connection('compras')->select("CALL compras_revision_areas($areas, $usuario->id)");
            }
        }
        else
        {
            $query = DB::connection('compras')->table("compras_revisiones")->where('usuario_id_area', '=', $usuario->id)->update(['usuario_id_area' => null]);
        }

        if($request->subarea_id != null)
        {
            foreach($request->subarea_id as $subareas)
            {
              $area = Subarea::find($subareas);
              $query = DB::connection('compras')->select("CALL compras_revision_subareas($area->area_id, $subareas, $usuario->id)");
            }
        }
        else
        {
            $query = DB::connection('compras')->table("compras_revisiones")->where('usuario_id_subarea', '=', $usuario->id)->update(['usuario_id_subarea' => null]);
        }

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Actualización revisiones usuario: '.$usuario->email.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        //mensaje de alerta
        //flash()->overlay('Las autorizaciones en Áreas y Subareas del usuario <b>'.$usuario->email.'</b> han sido actualizadas correctamente.', 'Notificación');
        Alert::success('<p>Las revisiones en Áreas y Subareas del usuario <b>'.$usuario->email.'</b> han sido actualizadas correctamente.</p>')->html()->persistent('Cerrar');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

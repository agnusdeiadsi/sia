<?php

namespace App\Http\Controllers\Browse\compras;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Excel;
use PHPExcel_Worksheet_Drawing;

//models
use App\Model\Compras\Solicitud;
use App\Model\Compras\Insumo;
use App\Model\Compras\UnidadMedida;
use App\Sistema;

class InsumosController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Vista Insumos solicitud N°'.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

        return view('browse.compras.solicitudes.insumos.index')
        ->with('sistema', $sistema)
        ->with('medidas', $medidas)
        ->with('solicitud', $solicitud)
        ->with('filtro', $request->filtro)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema, Solicitud $solicitud, Insumo $insumo, $estado)
    {
        $insumo->estado_id = $estado;
        $insumo->update();

        if($estado == 4)
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Insumo solicitud N° '.$solicitud->solicitud_id.' Aprobado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif ($estado == 5) {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Insumo solicitud N° '.$solicitud->solicitud_id.'  No Aprobado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }

        return redirect()->route('browse.compras.solicitudes.insumos.index', [$sistema, $solicitud, 'filtro' => $request->filtro]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insumo $insumo)
    {
        //
    }

    public function downloadXls(Sistema $sistema, Solicitud $solicitud)
    {
        Excel::create('Insumos_'.$solicitud->solicitud_id, function($excel) use ($solicitud)
        {
            $excel->sheet('Insumos', function($sheet) use ($solicitud) {
                /*$objDrawing = new PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('images/Logo-Color-Vertical.png'));
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWorksheet($sheet);*/

                $sheet->loadView('browse.compras.solicitudes.insumos.download')
                ->with('solicitud', $solicitud);
            })->export('xlsx');
        });
    }
}

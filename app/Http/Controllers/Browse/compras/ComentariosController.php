<?php

namespace App\Http\Controllers\Browse\compras;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;
use Alert;

use App\Sistema;
use App\Model\Compras\Solicitud;
use App\Model\Compras\Comentario;

use App\Mail\Compras\NotificacionNuevoComentario;

class ComentariosController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        $comentario = new Comentario(['solicitudcomentario_comentario' => nl2br($request->solicitudcomentario_comentario), 'usuario_id' => \Auth::user()->id]);

        $solicitud->comentarios()->save($comentario);

        //envía notificacion al correo electronico del solicitante y copia al administrador de sia
        Mail::to(\Auth::user()->email)
        //->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionNuevoComentario($sistema, $solicitud, $comentario));

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Nuevo comentario publicado ID '.$comentario->solicitudcomentario_id.'. Solicitud N°'.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]); 

        //muestra ventana modal de alerta
        Alert::success('<p>Comentario publicado exitosamente.</p>', '¡Muy bien!')->html()->persistent();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

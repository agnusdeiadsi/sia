<?php

namespace App\Http\Controllers\Browse\Grados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Programa;

class ServiceController extends Controller
{
    public function loadProgramGeneral()
    {
    	return \DB::connection('sia')->table('sia_programas')
    				->select(
    							'programa_id',
    							'programa_nombre', 
    							'convenio_nombre'
    						)
    				->join('sia_convenio', 'sia_programas.convenio_id', 'sia_convenio.convenio_id')
    				->orderBy('programa_nombre', 'ASC')
    				->get();

    }

    public function loadProgramDirector($programa)
    {
    	
    	return \DB::connection('sia')->table('sia_dir_programa')
    					->select('sia_dir_programa.programa_id', 'programa_nombre', 'convenio_nombre')
						->join(env('DB_DATABASE').'.sia_programas', 'sia_dir_programa.Programa_id', 'sia_programas.programa_id')
						->join('sia_convenio', 'sia_programas.convenio_id', 'sia_convenio.convenio_id')
						->where('sia_dir_programa.subarea_id', $programa)
						->get();
    }
}

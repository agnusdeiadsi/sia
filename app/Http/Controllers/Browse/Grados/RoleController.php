<?php

namespace App\Http\Controllers\Browse\Grados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Cookie;

use App\Mail\Solicitudes\Notificacionsefa;

use Alert;
use App\Sistema;
use App\Matriculado;
use App\Identificacion;
use App\Academico;
use App\DirPrograma;
use App\Programa;

use App\Model\Solicitudes\Solicitud;
use App\Model\Solicitudes\Estados;
use App\Model\Solicitudes\SolicitudFaseEstado;
use App\Model\Solicitudes\SolicitudCancelada;

class RoleController extends Controller
{	
    public $postulantes;
    
    public function __construct()
    {
        $this->middleware('browse');

        /*try 
        {

            $this->postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->leftJoin('solicitud_fase_estado', 'JAsolicitud.solicitud_id', 
                                                                'solicitud_fase_estado.solicitud_id')
                            ->leftJoin('solicitud_estados', 'solicitud_fase_estado.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo')
                            ->orderBy('matriculado_primer_apellido')
                            ->paginate(10);

        } catch (\Illuminate\Database\QueryException  $e) {

            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }*/

    }
	/*
	*
	*
		Sefa = Sefretaria de la faultad
	*
	*
	*/

	/*
	|------------------------------------
	|	 Secretaria de la facultad
	|------------------------------------
	*/

    public function sefaIndex(Sistema $sistema)
    {

        try 
        {

            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->leftJoin('solicitud_fase_estado', function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'solicitud_fase_estado.solicitud_id')
                               ->On('solicitud_fase_estado.subarea_id', \DB::raw('46')); 
                            })
                            ->leftJoin('solicitud_estados', 'solicitud_fase_estado.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo')
                            ->orderBy('matriculado_primer_apellido')
                            ->paginate(10);

            //dd($postulantes);

        	$sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.sefa.index')
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);

        } catch (\Illuminate\Database\QueryException  $e) {

            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    	/*$estados = Estados::orderBy('estado_nombre', 'ASC')->get();

    	return view('browse.grados.sefa.index')
    			->with('solicitudes', Solicitud::all())
                ->with('estados', $estados)
    			->with('sistemas', $sistemas)
                ->with('sistema', $sistema);*/

    }

    public function sefaSearch(Request $request, Sistema $sistema)
    {
    	$sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

    	$postulante = Academico::where('academico_codigo', $request->codigo)->get();
    	$estados = Estados::orderBy('estado_nombre', 'ASC')->get();

    	if ($postulante->count() > 0) 
    	{
    		
    		$solicitud = Solicitud::where(['matriculado_pidm' => $postulante->first()->matriculado_pidm,
    										'tipoSolicitud_id' => 1])
    								->orderBy('created_at', 'DESC');

	    	if ($solicitud->count() > 0) 
	    	{
	    		
	    		return view('browse.grados.sefa.formEstado')
	    					->with('estados', $estados)
	    					->with('solicitud', $solicitud)
	    					->with('sistemas', $sistemas)
	    					->with('sistema', $sistema );
	    	
	    	} else {

	    		Alert::warning('<p>No se ha encontrado una solicitud asociada al codigo  ID<b>'
	    							.$request->codigo.
	    						'</b></p>')
	    				->html()->persistent("OK");

	    		return view('browse.grados.sefa.index')
	    		    		->with('estados', $estados)
	    					->with('solicitud', $solicitud)
	    					->with('sistemas', $sistemas)
                            ->with('postulantes', $this->postulantes)
	    					->with('sistema', $sistema );

	    	}

    	} else {
    		
	    	Alert::warning('<p>No se ha encontrado al postulante</p>')->html()->persistent("OK");

	    	return view('browse.grados.sefa.index')
	    					->with('estados', $estados)
	    					->with('sistemas', $sistemas)
                            ->with('postulantes', $this->postulantes)
	    					->with('sistema', $sistema);
    	
    	}

    }

    /*public function sefaEstado(Request $request, Sistema $sistema)
    {
    	
    	try 
        {

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
            $estados = Estados::orderBy('estado_nombre', 'ASC')->get();
            $solicitud = Solicitud::where(['solicitud_id' => $request->solicitud])
                                    ->orderBy('created_at', 'DESC');

            $fase = SolicitudFaseEstado::where('solicitud_id', $request->solicitud)->get();		

            SolicitudFaseEstado::updateOrCreate(['solicitud_id' => $request->solicitud, 
                                                 'subarea_id' => Auth::user()->cuenta->subarea_id,
                                                 'user_id' => Auth::user()->id],
                                                ['estado_id' => $request->estado]);
            
            //Si la solicitud es rechazada
            if ($request->estado == 2) 
            {

                $content = 'Su solicitud a postulación a grado fue rechazada por las secretarias de su respectiva 
                            facultad. ';
                $subject = 'Solicitud rechazado';
                $motivo = 'El motivo; '.$request->description;
                
                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

                SolicitudCancelada::updateOrCreate([
                                                        'solicitud_id' => $request->solicitud,
                                                        'subarea_id' => Auth::user()->cuenta->subarea_id
                                                    ],[
                                                        'estado_id' => $request->estado,
                                                        'user_id' => Auth::user()->id,
                                                        'solicitudCancelada_descripcion' => $request->description
                                                    ]);
            
            } else {

                $content = 'Su solicitud a postulación a grado fue aprobada por las secretarias 
                            de su respectiva facultad';
                $subject = 'Solicitud aprobada';
                $motivo = '';

                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

            }

            Alert::success('<p>Datos enviados exitosamente</p>')->html()->persistent("OK");

            return view('browse.grados.sefa.formEstado')
                            ->with('estados', $estados)
                            ->with('sistemas', $sistemas)
                            ->with('sistema', $sistema)
                            ->with('solicitud', $solicitud);

    	} catch (\Illuminate\Database\QueryException  $e) {

    		Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

    	}

    }*/

    public function sefaSearchTable(Sistema $sistema, Matriculado $postulante)
    {
        
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.sefa.formEstado')
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);
    
    }
    /*
    |-------------------------------------
    |	Final secretaria de la facultad
    |-------------------------------------
    */

    /*
    |------------------------------------
    |    Biblioteca
    |------------------------------------
    */
    public function bibliotecaIndex(Sistema $sistema)
    {

        try 
        {
            //Cookie::queue('programa', $request->programa, 60);

            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->leftJoin('solicitud_fase_estado', function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'solicitud_fase_estado.solicitud_id')
                               ->On('solicitud_fase_estado.subarea_id', \DB::raw('101')); 
                            })
                            ->leftJoin('solicitud_estados', 'solicitud_fase_estado.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         subarea_id')
                            ->orderBy('matriculado_primer_apellido')

                            ->paginate(10);
            //dd($postulantes);
            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.biblioteca.index')
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);

        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function bibliotecaSearchTable(Sistema $sistema, Matriculado $postulante)
    {
        
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.biblioteca.formEstado')
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);

    }

    public function bibliotecaPosproIndex(Request $request, Sistema $sistema)
    {
        try 
        {
            Cookie::queue('programa', $request->programa, 60);

            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->leftJoin('solicitud_fase_estado', function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'solicitud_fase_estado.solicitud_id')
                               ->On('solicitud_fase_estado.subarea_id', \DB::raw('101')); 
                            })
                            ->leftJoin('solicitud_estados', 'solicitud_fase_estado.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            ->where('JAsolicitud.programa_id', $request->programa)
                            ->paginate(2);
            //dd($postulantes);
            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.biblioteca.index')
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);

        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    /*public function bibliotecaEstado(Request $request, Sistema $sistema)
    {
        
        try 
        {
            //dd($request->all());
            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
            $estados = Estados::orderBy('estado_nombre', 'ASC')->get();
            $solicitud = Solicitud::where(['solicitud_id' => $request->solicitud])
                                    ->orderBy('created_at', 'DESC');

            $fase = SolicitudFaseEstado::where('solicitud_id', $request->solicitud)->get();     

            SolicitudFaseEstado::updateOrCreate(['solicitud_id' => $request->solicitud, 
                                                 'subarea_id' => Auth::user()->cuenta->subarea_id,
                                                 'user_id' => Auth::user()->id],
                                                ['estado_id' => $request->estado]);
            
            //Si la solicitud es rechazada
            if ($request->estado == 2) 
            {

                $content = 'Su solicitud a postulación a grado fue rechazada el area de biblioteca. ';
                $subject = 'Solicitud rechazado';
                $motivo = 'El motivo; '.$request->description;
                //dd($motivo);
                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

                SolicitudCancelada::updateOrCreate([
                                                        'solicitud_id' => $request->solicitud,
                                                        'subarea_id' => Auth::user()->cuenta->subarea_id
                                                    ],[
                                                        'estado_id' => $request->estado,
                                                        'user_id' => Auth::user()->id,
                                                        'solicitudCancelada_descripcion' => $request->description
                                                    ]);
            
            } else {

                $content = 'Su solicitud a postulación a grado fue aprobada el area de biblioteca';
                $subject = 'Solicitud aprobada';
                $motivo = '';

                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

            }

            Alert::success('<p>Datos enviados exitosamente</p>')->html()->persistent("OK");


            return view('browse.grados.biblioteca.formEstado')
                            ->with('estados', $estados)
                            ->with('sistemas', $sistemas)
                            ->with('sistema', $sistema)
                            ->with('solicitud', $solicitud);

        } catch (\Illuminate\Database\QueryException  $e) {

            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }

    }*/
    /*
    |-------------------------------------
    |   Final Biblioteca
    |-------------------------------------
    */

    /*
    |------------------------------------
    |    Egresados
    |------------------------------------
    */

    public function egresadosIndex(Sistema $sistema)
    {
        try 
        {
            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 46
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 71
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 46
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 71
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            ->paginate(10);

            $programas = Programa::orderBy('programa_nombre', 'ASC');

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.egresados.index')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);

        } catch (\Illuminate\Database\QueryException  $e) {
            //dd($e);
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function egresadosSearchTable(Sistema $sistema, Matriculado $postulante)
    {
        
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.egresados.formEstado')
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);

    }

    public function egresadosPosproIndex(Request $request, Sistema $sistema)
    {
        try 
        {
            Cookie::queue('programa', $request->programa, 60);

            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 46
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 71
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 46
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 71
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->where('JAsolicitud.programa_id', $request->programa)
                            ->orderBy('matriculado_primer_apellido')
                            ->paginate(10);

            $programas = Programa::orderBy('programa_nombre', 'ASC');

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.egresados.index')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);

        } catch (\Illuminate\Database\QueryException  $e) {
            //dd($e);
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    /*public function egresadosEstado(Request $request, Sistema $sistema)
    {
        
        try 
        {
            
            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
            $estados = Estados::orderBy('estado_nombre', 'ASC')->get();
            $solicitud = Solicitud::where(['solicitud_id' => $request->solicitud])
                                    ->orderBy('created_at', 'DESC');

            $fase = SolicitudFaseEstado::where('solicitud_id', $request->solicitud)->get();     

            SolicitudFaseEstado::updateOrCreate(['solicitud_id' => $request->solicitud, 
                                                 'subarea_id' => Auth::user()->cuenta->subarea_id,
                                                 'user_id' => Auth::user()->id],
                                                ['estado_id' => $request->estado]);
            //Si la solicitud es rechazada
            if ($request->estado == 2) 
            {

                $content = 'Su solicitud a postulación a grado fue rechazada por '.Auth::user()->cuenta->subarea->subarea_nombre.'. ';
                $subject = 'Solicitud rechazado';
                $motivo = 'El motivo; '.$request->description;
                //dd($motivo);
                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

                SolicitudCancelada::updateOrCreate([
                                                        'solicitud_id' => $request->solicitud,
                                                        'subarea_id' => Auth::user()->cuenta->subarea_id
                                                    ],[
                                                        'estado_id' => $request->estado,
                                                        'user_id' => Auth::user()->id,
                                                        'solicitudCancelada_descripcion' => $request->description
                                                    ]);
            
            } else {

                $content = 'Su solicitud a postulación a grado fue aprobada por '.Auth::user()->cuenta->subarea->subarea_nombre;
                $subject = 'Solicitud aprobada';
                $motivo = '';

                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

            }

            Alert::success('<p>Datos enviados exitosamente</p>')->html()->persistent("OK");


            return view('browse.grados.biblioteca.formEstado')
                            ->with('estados', $estados)
                            ->with('sistemas', $sistemas)
                            ->with('sistema', $sistema)
                            ->with('solicitud', $solicitud);

        } catch (\Illuminate\Database\QueryException  $e) {

            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }

    }*/

    /*
    |-------------------------------------
    |   Final Egresados
    |-------------------------------------
    */

    /*
    |------------------------------------
    |    Director Programa
    |------------------------------------
    */

    public function directorBaseIndex(Sistema $sistema)
    {
        try 
        {
            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            $programas = DirPrograma::programa(Auth::user()->cuenta->subarea_id)->get();

            return view('browse.grados.dirPrograma.index')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema);

        } catch (\Illuminate\Database\QueryException  $e) {

            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function directorIndex(Request $request, Sistema $sistema)
    {
        try 
        {

            Cookie::queue('programa', $request->programa, 60);
            
            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 46
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 
                                                            '.Auth::user()->cuenta->subarea_id.'
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 46
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 
                                                                '.Auth::user()->cuenta->subarea_id.'
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            ->where('JAsolicitud.programa_id', $request->programa)
                            ->paginate(10); 

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            $programas = DirPrograma::programa(Auth::user()->cuenta->subarea_id)->get();

            return view('browse.grados.dirPrograma.index')
                    ->with('postulantes', $postulantes)
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema);
    
        } catch (\Illuminate\Database\QueryException $e) {

            //dd($e);
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function directorSearchTable(Sistema $sistema, Matriculado $postulante)
    {

        $programa = Cookie::get('programa');
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.dirPrograma.formEstado')
                ->with('programa', $programa)
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);
   
    }
    /*
    |-------------------------------------
    |   Final Director Programa
    |-------------------------------------
    */

    /*
    |-------------------------------------
    |   Regsitro Academico
    |-------------------------------------
    */
    public function reacaIndex(Sistema $sistema)
    {
        
        try 
        {
            
            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 104
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 
                                                            '.Auth::user()->cuenta->subarea_id.'
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 104
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 
                                                                '.Auth::user()->cuenta->subarea_id.'
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            //->where('JAsolicitud.programa_id', $request->programa)
                            ->paginate(10); 

            $programas = Programa::orderBy('programa_nombre', 'ASC');

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.reaca.index')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);
        
        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function reacaPosproIndex(Request $request, Sistema $sistema)
    {
        try 
        {
            
            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 104
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 
                                                            '.Auth::user()->cuenta->subarea_id.'
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 104
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 
                                                                '.Auth::user()->cuenta->subarea_id.'
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            ->where('JAsolicitud.programa_id', $request->programa)
                            ->paginate(10); 

            $programas = Programa::orderBy('programa_nombre', 'ASC');

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.reaca.index')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);
        
        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }

    }

    public function reacaSearchTable(Sistema $sistema, Matriculado $postulante)
    {

        $programa = Cookie::get('programa');
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.reaca.formEstado')
                ->with('programa', $programa)
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);
   
    }
    /*
    |-------------------------------------
    |   Final Registro Academico
    |-------------------------------------
    */

    /*
    |-------------------------------------
    |   Cartera
    |-------------------------------------
    */
    public function carFinanIndex(Sistema $sistema)
    {
        try 
        {
            
            $postulantes = \DB::connection('solicitudes')->table(env('DB_DATABASE').'.sia_matriculados AS Matriculado')
                            ->join(\DB::raw('(  SELECT
                                                    Jsolicitud.*
                                                FROM
                                                    solicitud Jsolicitud
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2solicitud.created_at) AS fecha,
                                                        J2solicitud.matriculado_pidm
                                                    FROM
                                                        solicitud J2solicitud
                                                    GROUP BY
                                                        J2solicitud.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jsolicitud.matriculado_pidm 
                                                AND RU.fecha = Jsolicitud.created_at
                                            ) AS JAsolicitud'), 
                                function ($join)
                                {
                                    $join->on('Matriculado.matriculado_pidm', 'JAsolicitud.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    Jacademico.*
                                                FROM
                                                    '.env('DB_DATABASE').'.sia_academico Jacademico
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(J2academico.created_at) AS fecha,
                                                        J2academico.matriculado_pidm
                                                    FROM
                                                        '.env('DB_DATABASE').'.sia_academico J2academico
                                                    GROUP BY
                                                        J2academico.matriculado_pidm
                                                ) RU ON RU.matriculado_pidm = Jacademico.matriculado_pidm 
                                                AND RU.fecha = Jacademico.created_at
                                            ) AS JAacademico'), 
                                function ($join)
                                {
                                    $join->on('JAsolicitud.matriculado_pidm', 'JAacademico.matriculado_pidm');
                                })
                            ->join(\DB::raw('(  SELECT
                                                    solicitudSefa.*
                                                FROM
                                                    (
                                                        SELECT
                                                            solicitudFase.*
                                                        FROM
                                                            solicitud_fase_estado AS solicitudFase
                                                        WHERE
                                                            (
                                                                solicitudFase.subarea_id = 100
                                                                AND solicitudFase.estado_id = 1
                                                            )
                                                        OR (
                                                            solicitudFase.subarea_id = 
                                                            '.Auth::user()->cuenta->subarea_id.'
                                                        )
                                                    ) AS solicitudSefa
                                                INNER JOIN (
                                                    SELECT
                                                        MAX(solicitud1.created_at) AS fecha,
                                                        solicitud1.solicitud_id
                                                    FROM
                                                        (
                                                            SELECT
                                                                solicitudFase.*
                                                            FROM
                                                                solicitud_fase_estado AS solicitudFase
                                                            WHERE
                                                                (
                                                                    solicitudFase.subarea_id = 100
                                                                    AND solicitudFase.estado_id = 1
                                                                )
                                                            OR (
                                                                solicitudFase.subarea_id = 
                                                                '.Auth::user()->cuenta->subarea_id.'
                                                            )
                                                        ) AS solicitud1
                                                    GROUP BY
                                                        solicitud1.solicitud_id
                                                ) RU ON RU.solicitud_id = solicitudSefa.solicitud_id
                                                AND RU.fecha = solicitudSefa.created_at
                                            ) IFASE'), function ($join)
                            {
                               $join->on('JAsolicitud.solicitud_id', 'IFASE.solicitud_id');  
                            })
                            ->leftJoin('solicitud_estados', 'IFASE.estado_id', 
                                                                'solicitud_estados.estado_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_identificacion', 'Matriculado.matriculado_pidm', 
                                                                'sia_identificacion.matriculado_pidm')
                            ->leftJoin(env('DB_DATABASE').'.sia_programas', 'JAacademico.programa_id', 
                                                                'sia_programas.programa_id')
                            ->leftJoin(env('DB_DATABASE').'.sia_facultades', 'sia_programas.facultad_id', 
                                                                'sia_facultades.facultad_id')
                            ->selectRaw('Matriculado.matriculado_pidm,
                                         identificacion_numero,
                                         matriculado_primer_nombre,
                                         matriculado_segundo_nombre,
                                         matriculado_primer_apellido,
                                         matriculado_segundo_apellido,
                                         JAsolicitud.solicitud_id,
                                         facultad_codigo,
                                         estado_nombre,
                                         estado_color,
                                         programa_nombre,
                                         academico_codigo,
                                         IFASE.subarea_id')
                            ->orderBy('matriculado_primer_apellido')
                            //->where('JAsolicitud.programa_id', $request->programa)
                            ->paginate(10); 

            $programas = Programa::orderBy('programa_nombre', 'ASC');

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

            return view('browse.grados.cartera.indexFinan')
                    ->with('programas', $programas)
                    ->with('sistemas', $sistemas)
                    ->with('sistema', $sistema)
                    ->with('postulantes', $postulantes);
        
        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }
    
    }

    public function carfinanSearchTable(Sistema $sistema, Matriculado $postulante)
    {

        $programa = Cookie::get('programa');
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $estados = Estados::orderBy('estado_nombre', 'ASC')->get();

        $solicitud = Solicitud::where(['matriculado_pidm' => $postulante->matriculado_pidm,
                                            'tipoSolicitud_id' => 1])
                                    ->orderBy('created_at', 'DESC');

        return view('browse.grados.cartera.formEstadoFinan')
                ->with('programa', $programa)
                ->with('estados', $estados)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('solicitud', $solicitud);
   
    }
    /*
    |------------------------------------
    |   Final Cartera
    |------------------------------------
    */

    /*
    |------------------------------------
    |    General
    |------------------------------------
    */

    public function estado(Request $request, Sistema $sistema)
    {
        
        try 
        {

            $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
            $estados = Estados::orderBy('estado_nombre', 'ASC')->get();
            $solicitud = Solicitud::where(['solicitud_id' => $request->solicitud])
                                    ->orderBy('created_at', 'DESC');

            $fase = SolicitudFaseEstado::where('solicitud_id', $request->solicitud)->get();     

            SolicitudFaseEstado::updateOrCreate(['solicitud_id' => $request->solicitud, 
                                                 'subarea_id' => Auth::user()->cuenta->subarea_id,
                                                 'user_id' => Auth::user()->id],
                                                ['estado_id' => $request->estado]);
            
            //Si la solicitud es rechazada
            if ($request->estado == 2) 
            {

                $content = 'Su solicitud a postulación a grado fue rechazada por '.Auth::user()->cuenta->subarea->subarea_nombre.'. ';
                $subject = 'Solicitud rechazado';
                $motivo = 'El motivo; '.$request->description;
                //dd($motivo);
                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

                SolicitudCancelada::updateOrCreate([
                                                        'solicitud_id' => $request->solicitud,
                                                        'subarea_id' => Auth::user()->cuenta->subarea_id
                                                    ],[
                                                        'estado_id' => $request->estado,
                                                        'user_id' => Auth::user()->id,
                                                        'solicitudCancelada_descripcion' => $request->description
                                                    ]);
            
            } else {

                $content = 'Su solicitud a postulación a grado fue aprobada por '.Auth::user()->cuenta->subarea->subarea_nombre;
                $subject = 'Solicitud aprobada';
                $motivo = '';

                Mail::to($request->email)->send(new Notificacionsefa($content, $subject, $motivo));

            }
            //dd("Entre");
            Alert::success('<p>Datos enviados exitosamente</p>')->html()->persistent("OK");

            switch ($request->area) 
            {
            
                case 'sefa':
                        return $this->sefaIndex($sistema);
                    break;
                case 'biblioteca':
                        return $this->bibliotecaIndex($sistema);
                    break;
                case 'egresados':
                        return $this->egresadosIndex($sistema);
                    break;
                case 'dirPrograma':
                        return $this->directorIndex($request, $sistema);
                    break;
                case 'reaca':
                        return $this->reacaIndex($sistema);
                    break;
                case 'carfinan':
                        return $this->carFinanIndex($sistema);
                    break;
            
            }

        } catch (\Illuminate\Database\QueryException  $e) {
            
            Alert::error('<p>Se ha producido un error<br>Por favor intentalo de nuevo</p>')->html()->persistent("OK");
            return redirect()->back();

        }

    }

    /*
    |-------------------------------------
    |   Final General
    |-------------------------------------
    */

}

<?php

namespace App\Http\Controllers\Browse\Egresados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade;

use App\Mail\Egresados\NotificacionEgresado;

use App\Matriculado;
use App\Sistema;
use App\Subarea;
use App\RangoSalarial;
use App\SectorEmpresa;
use App\Ciudad;
use App\Identificacion;
use App\NivelAcademico;
use App\Model\Egresados\Estudio;
use Excel;
use Alert;
use App\Model\Egresados\Grado;
use App\Model\Egresados\MatriculadoLogros;
use App\Model\Egresados\Laboral;
use App\Model\Egresados\AreaPertenece;
use App\Model\Egresados\DuracionEmpresa;
use App\Model\Egresados\Ocupacion;
use App\Model\Egresados\RelacionPrograma;
use App\Model\Egresados\SituacionLaboral;
use App\Model\Egresados\Vinculacion;
use App\Model\Egresados\TamanoEmpresa;
use App\Model\Egresados\Logros;
use App\Model\Egresados\Emprendimiento;
use App\Model\Egresados\Reconocimiento;
use App\Cargo;
use App\Contrato;
use App\TipoEmpresa;
use App\Departamento;

class EgresadoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('browse');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Este metodo funciona para renderizar a la vista del formulario de actualizacion de datos desde el buscador
    public function edit(Sistema $sistema, Request $request)
    {

        //Se cargan las variables necesarias para cargar el formulario
        $egresado = Identificacion::search($request->search)->first();
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();
        $departamento = Departamento::orderBy('departamento_nombre', 'ASC')->get();
        //dd($egresado->matriculado->ciudad->ciudad_nombre);

        $nivel = NivelAcademico::orderBy('nivelacademico_id', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $cargo = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();
        $tipoEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();
        $contrato = Contrato::orderBy('contrato_nombre', 'ASC')->get();
        $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();
        $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_id', 'ASC')->get();
        $ocupacion = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();
        $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();
        $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();
        $vinculacion = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $tamanoEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();
        $logros = Logros::orderBy('logro_nombre', 'ASC')->get();

        //La variable egresados contiene los resultados de la busqueda en el modelo, si el dato ingresado esta en la BD se renderiza al formulario con las variables anteriores
        if ($egresado != null) {
            
            return view('browse.egresados.formulario.form')
                ->with('nivel', $nivel)
                ->with('tamanoEmpresa', $tamanoEmpresa)
                ->with('departamento', $departamento)
                ->with('ciudad', $ciudad)
                ->with('subareas', $subareas)
                ->with('areaPertenece', $areaPertenece)
                ->with('tipoEmpresa', $tipoEmpresa)
                ->with('cargo', $cargo)
                ->with('contrato', $contrato)
                ->with('duracionEmpresa', $duracionEmpresa)
                ->with('ocupacion', $ocupacion)
                ->with('relacionPrograma', $relacionPrograma)
                ->with('situacionLaboral', $situacionLaboral)
                ->with('vinculacion', $vinculacion)
                ->with('rangoSalarial', $rangoSalarial)
                ->with('sectorEmpresa', $sectorEmpresa)
                ->with('logros', $logros)
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema)
                ->with('egresado', $egresado);

        //Si la variable egresados es null quiere decir que no encontro conincidencia en el modelo "Identificacion", imprimira un mesaje de alvertencia
        } else {

            Alert::warning('<p>¡No existe ningun registro!</p>')->html()->persistent("OK");
           
            return redirect()->route('browse.egresados.formulario.form', $sistema);
        }
    }

    //Este metodo funciona para renderizar a la vista del formulario de actualizacion de datos desde la tabla     
    public function edit_table(Sistema $sistema, $egresado)
    {
        $egresado = Identificacion::search($egresado)->first();
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();
        $departamento = Departamento::orderBy('departamento_nombre', 'ASC')->get();

        $nivel = NivelAcademico::orderBy('nivelacademico_id', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $cargo = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();
        $tipoEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();
        $contrato = Contrato::orderBy('contrato_nombre', 'ASC')->get();
        $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();
        $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_nombre', 'ASC')->get();
        $ocupacion = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();
        $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();
        $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();
        $vinculacion = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $tamanoEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();
        $logros = Logros::orderBy('logro_nombre', 'ASC')->get();
        
        //La variable egresados contiene los resultados de la busqueda en el modelo, si el dato ingresado esta en la BD se renderiza al formulario con las variables anteriores
        return view('browse.egresados.formulario.form')
            ->with('nivel', $nivel)
            ->with('tamanoEmpresa', $tamanoEmpresa)
            ->with('ciudad', $ciudad)
            ->with('departamento', $departamento)
            ->with('subareas', $subareas)
            ->with('areaPertenece', $areaPertenece)
            ->with('tipoEmpresa', $tipoEmpresa)
            ->with('cargo', $cargo)
            ->with('contrato', $contrato)
            ->with('duracionEmpresa', $duracionEmpresa)
            ->with('ocupacion', $ocupacion)
            ->with('relacionPrograma', $relacionPrograma)
            ->with('situacionLaboral', $situacionLaboral)
            ->with('vinculacion', $vinculacion)
            ->with('rangoSalarial', $rangoSalarial)
            ->with('sectorEmpresa', $sectorEmpresa)
            ->with('logros', $logros)
            ->with('sistemas', $sistemas)
            ->with('sistema', $sistema)
            ->with('egresado', $egresado);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion que sirve para actualizar los datos del egresado, tanto del modulo de egresado como para el de estudiante
    public function update(Request $request, Sistema $sistema, $key)
    {
        /*
            $key = pidm
            $request = datos del formulario
            $sistema = los datos del modulo
            $egresado = los datos del egresado
            $grado = los datos del grado del egresado
        */
         try {
            //Se realzia una busqueda con la variable $key al modelo Matriculado para traer al egresado que se va a actualizar
            $egresado = Matriculado::find($key);
            
            //Se realzia una busqueda con la variable $key al modelo de grado para traer el grado del egresado
            $grado = Grado::GetStudent($key);

            //de la variable egresados, quien tiene unas propiedades del modelo matriculado, se le guardaran los datos del formulario
            $egresado->matriculado_primer_nombre = $request->first_name;
            $egresado->matriculado_segundo_nombre = $request->second_name;
            $egresado->matriculado_primer_apellido = $request->last_name_first;
            $egresado->matriculado_segundo_apellido= $request->last_name_second;
            $egresado->matriculado_telefono = $request->tel;
            $egresado->matriculado_celular = $request->cel;
            $egresado->matriculado_email = $request->email;

            $egresado->save();

            //Recoremos la variable grado con el fin de actualzar el puntaje, siempre cuando la propiedad puntaje del request sea diferende de null

            if ($request->puntaje != null) 
            {
                foreach ($grado as $grado) 
                {

                $puntaje = $grado->grado_puntaje + $request->puntaje;
                
                $grado->grado_puntaje = $puntaje;

                $grado->save();

                }

            }

            if ($request->laboral_situacion != 2) 
            {

                if (!empty($request->name_empresa)) 
                {

                    //Se instacia el modelo a la variable $laboral
                    $laboral = new Laboral();
                    
                    //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                    $laboral->situacionlaboral_id = $request->laboral_situacion;
                    $laboral->laboral_empresa = $request->name_empresa;
                    $laboral->laboral_telefono = $request->tel_empresa;
                    $laboral->sectorempresa_id = $request->sector;
                    $laboral->tipoempresa_id = $request->tipoEmpresa;
                    $laboral->rangosalario_id = $request->salario;
                    $laboral->nivelcargo_id = $request->cargo;
                    $laboral->areapertenece_id = $request->area;
                    $laboral->ocupacion_id = $request->ocupacion;
                    $laboral->relacionprograma_id = $request->relacionPrograma;
                    $laboral->duracionempresa_id = $request->duracionEmpresa;
                    $laboral->vinculacion_id = $request->vinculacion;
                    $laboral->ciudad_id = $request->ciudad;
                    $laboral->contrato_id = $request->contrato;

                    $laboral->matriculado_pidm = $key;

                    $laboral->save();

                }

            } else {

                    //Se instacia el modelo a la variable $laboral
                    $laboral = new Laboral();
                    
                    //se le guardaran los datos del formulario a las propiedades del modelo Laboral
                    $laboral->situacionlaboral_id = $request->laboral_situacion;
                    $laboral->matriculado_pidm = $key;
                    
                    $laboral->save();
            }
            /*
                $estudios = se pueden ingresar mas de dos estudios diferentes

                Para el modulo de egresados
            */

                
            $estudios = array();
            $i = 0;

            //Se pregunta si la propiedad de request estudio_nivel es diferente a vacio, si es asi...
            if(!empty($request->estudio_nivel))
            {   
                //Recoremos la pripiedad estudio nivel, nos disra cuantas posiciones en el arreglo existen
                foreach ($request->estudio_nivel as $estudio) 
                {
                    //Se insertan a las propiedades del modelo la informacion del formulario
                    $estudios[] = [
                                "nivelacademico_id" => $estudio,
                                "estudio_institucion" => $request->estudio_nombreInstitucion[$i],
                                "estudio_titulo" => $request->estudio_titulo[$i],
                                "estudio_inicio" => $request->estudio_inicio[$i],
                                "estudio_fin" => $request->estudio_fin[$i],
                                "matriculado_pidm" => $egresado->matriculado_pidm];

                    ++$i;
                }


                $ii = 0;
                //Recoremos la pripiedad estudio nivel, nos disra cuantas posiciones en el arreglo existen
                foreach ($request->estudio_nivel as $nivel)
                {
                  //Se instacia el model, y guardamos el arreglo 
                  $estudio = new Estudio();
                  $estudio->create($estudios[$ii]);
                  $ii++;
                }

            }

            //Si la pripiedad nombre_empresa_em es diferente a vacio
            if (!empty($request->nombre_empresa_em)) 
            {

                //Instaciamos el modelo Emprendimiento y guardamos las propiedas del request a las propiedades del modelo
                $emprendimiento = new Emprendimiento();
                
                $emprendimiento->emprendimiento_empresa = $request->nombre_empresa_em;
                $emprendimiento->emprendimiento_direccion = $request->direccion_empresa_em;
                $emprendimiento->emprendimiento_telefono = $request->telefono_empresa_em;
                $emprendimiento->sectorempresa_id = $request->sector_em;
                $emprendimiento->tamanoempresa_id = $request->tamano_em;
                $emprendimiento->ciudad_id = $request->ciudad_em;
                $emprendimiento->matriculado_pidm = $key;

                $emprendimiento->save();

            }

            if (!empty($request->logroEgresado)) 
            {
                MatriculadoLogros::updateOrCreate(['logro_id' => $request->logroEgresado,
                                                    'matriculado_pidm' => $key],
                                                    ['matriculadologro_fecha' 
                                                                        => $request->logroEgresadodate ]);   
            }

            //Si la propiedad reconocimiento_nombre es diferente a vacio
            if (!empty($request->reconocimiento_nombre)) 
            {
                
                //Se instancia el modelo reconocimiento y guardamos las propiedas del request a las propiedades del modelo
                $reconocimiento = new Reconocimiento();

                $reconocimiento->reconocimiento_nombre = $request->reconocimiento_nombre;
                $reconocimiento->reconocimiento_organizacion = $request->reconocimiento_organizacion;
                $reconocimiento->reconocimiento_fecha = $request->reconocimiento_fecha;
                $reconocimiento->matriculado_pidm = $key;
                
                $reconocimiento->save();

            }

            $mensaje = '<p>¡los datos han sido guardados exitosamente!</p>'; 
            //$encabezado = 'AVISO!';
            $metodo = 'success';

        } catch (\Illuminate\Database\QueryException  $e){
            
                //dd($e);
              $metodo = 'error';
              //$encabezado = '¡ERROR!';
              $mensaje = '<p>Se ha producido un error al guardar los datos</p>';

        }
        //Mostramos el mensaje
        Alert::$metodo($mensaje)->html()->persistent("OK"); 

        //Retornamos al formulario con las variables actualizadas
        return redirect()->back();

        /*
        //Mostramos el mensaje
        Alert::success('¡Registro Actualizado Exitosamente!', '¡AVISO!')->persistent("OK"); 

        //Retornamos al formulario con las variables actualizadas
        return redirect()->back();*/

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Funcion que sirve para mostrar la vista donde se cargara el archivo de alimentacion a la BD
    public function import(Sistema $sistema)
    {   
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.egresados.import')
                ->with('sistema', $sistema)
                ->with('sistemas', $sistemas);
    }
    
    //Funcion que sirve para validar las llaves foraneas alojadas en el archivo de carge
    function validarTipoDato($key, $table, $columna, $connection)
    {
        /*
            $texto = texto que se retornara la funcion uploadxls
            $key = llave primaria
            $table = nombre de la tabla
            $columna = columna de la BD
            $connection = la coneccion | BD
            $variable = la respuesta del query para traer los datos de la llave

        */
        $texto = '';
        //Si la llave foranea es null se retorna un mensaje true
        if ($key == null) {

            $texto = 'true';
            return $texto;
            
        } else {

            //Si la llave es numerica...
            if (gettype($key) == 'double') {

                //Realizamos la consulta para traer los datos de la llave foranea
                $variable = DB::connection($connection)->select('SELECT * FROM '.$table.' WHERE '.$columna.' = '.$key);
                
                //si $variable es dieferente de null, retorna un texto $true
                if ($variable != null) {
                    
                    $texto = 'true';
                    return $texto;
                
                // sSi es null quiere decir que no existen datos de esa llave foranea
                } else {
                    
                    $texto = 'NO COINCIDEN LOS DATOS';
                    return $texto;
                
                }
            
            //Si la llave no es numerica, retorna un mesaje SOLO SON PERMITIDOS NUMEROS
            } else {

                $texto = 'SOLO SON PERMITIDOS NUMEROS';
                return $texto;
            
            }

        }
        
    }

    //Funcion que nos permite controlar si informacion como referencia, emprendimiento, familiar, estan definidas en el archivo de carge
    public function validarDatosNull($arreglo)   
    {
        /*
            $arreglo = variable que recibe como parametro la funcion desde el meto uploadxls
            $null = contador para saber no existen datos en $arreglo
        */
        $null = 0;

        //recoremos el arreglo
        foreach ($arreglo as $dato) 
        {
            //Si el arreglo es null, contador autosuma
            if ($dato == null) {
                
                $null++;

            }
        
        }

        //return de la variable null
        return $null;

    }

    //Metodo que funciona para el archivo de carge
    public function uploadxls(Request $request)
    {

        $egresados = Matriculado::orderBy('matriculado_pidm', 'ASC')->get();

        //Valdiamos si el archivo corresponde a los requerimientos,
        $this->validate($request, [
            'estudiantes_file' => 'required|mimes:xls,xlsx,csv',
        ]);

        //Para saber que archivo se va cargar
        $archivo = $request->file('estudiantes_file');

        //Obtenemos la ruta del archivo
        $ruta_archivo = $archivo->getRealPath();

        //Cargamos el archivo en la varaible $estudiantes
        $estudiantes = Excel::load($ruta_archivo, function ($reader) {
        })->get();

        //Variables para saber cuantos datos fueron actualizados o ingresados
        //$total_no_insertados = 0;
        $total_insertados    = 0;
        $total_actualizados  = 0;
        //Variable que funciona para identificar cuantas vueltas lleva el carge del archivo
        $contador            = 1;
        //Variable que funciona para que se rompa la carga cuando pase a false
        $dinamico = true;

        //recoremos el archivo estudiantes
        foreach ($estudiantes as $estudiante) 
        {
            //Preguntamos si las promiedades identificacion_numero y pidm son munericas y diferentes a null
            if (isset($estudiante->identificacion_numero) &&
                $estudiante->identificacion_numero != null && 
                gettype($estudiante->identificacion_numero) == 'double' && 
                gettype($estudiante->pidm) == 'double') 
            {

                //Trae del modelo Identificacion el estudiante con el numero de documento
                $egresado_iden = Identificacion::where('identificacion_numero', $estudiante->identificacion_numero)->get();

                //Guardamos las propiedades del archivo de carga en las propiedades del modelo Identificacion
                $nuevo_identificacion = [
                    'identificacion_tipo' => $estudiante->identificacion_tipo,
                    'identificacion_numero' => $estudiante->identificacion_numero,
                    'matriculado_pidm' => $estudiante->pidm,
                    'created_at' => $estudiante->fecha,
                    'updated_at' => date("Y-m-d G:i:s"),
                ];
                /*
                    Se realiza el llamado a la funcion validarTipoDato con todas las lalves foraneas existentes en el archivo de carge
                */
                //Llamamos a la funcion validarTipoDato quien nos recibe 4 parametros
                $validador_pais = $this->validarTipoDato($estudiante->pais_id, 'sia_paises', 'pais_id', 'sia');
                $validador_ciudad = $this->validarTipoDato($estudiante->ciudad_id, 'sia_ciudades', 'ciudad_id', 'sia');
                $validador_estadosciviles = $this->validarTipoDato($estudiante->estadocivil_id, 'sia_estados_civiles', 'estadocivil_id', 'sia');

                //Arreglo que funciona para indicar la columna del archivo de carge y su correspondiente valor
                $array_columnas_con = array('PAIS_ID' => $validador_pais, 'CIUDAD_ID' => $validador_ciudad, 'ESTADOCIVIL_ID' => $validador_estadosciviles);
                
                //Recoremos el arreglo
                foreach ($array_columnas_con as $columna => $value) {
                    
                    //Si todas los posicioens del arreglo tienen como value un mensaje true...
                    if($value == 'true') {
                   
                    //Se guarda en las propiedades del modelo Matriculado, las propiedades del archivo de carge
                    $nuevo_estudiante = [
                        'matriculado_pidm'    => $estudiante->pidm,
                        'matriculado_primer_nombre' => $estudiante->primer_nombre,
                        'matriculado_segundo_nombre' => $estudiante->segundo_nombre,
                        'matriculado_primer_apellido' => $estudiante->primer_apellido,
                        'matriculado_segundo_apellido' => $estudiante->segundo_apellido,
                        'matriculado_email'   => $estudiante->email,
                        'matriculado_direccion' => $estudiante->direccion_residencia,
                        'matriculado_telefono' => $estudiante->telefono,
                        'matriculado_celular' => $estudiante->celular,
                        'matriculado_estrato' => $estudiante->estrato, 
                        'matriculado_fecha_nacimiento' => $estudiante->fecha_nacimiento,
                        'matriculado_discapacidad' => $estudiante->discapacidad,
                        'matriculado_genero' => $estudiante->genero,
                        'pais_id' => $estudiante->pais_id,
                        'ciudad_id' => $estudiante->ciudad_id,
                        'estadocivil_id' => $estudiante->estadocivil_id,   
                        'created_at' => $estudiante->fecha,
                        'updated_at' => date("Y-m-d G:i:s"),     
                    ];
                    
                    //Si alguna de las posiciones del arreglo tiene un mensaje diferente a true...
                    } else {

                        //La varaible dinamico pasa a ser false
                        $dinamico = false;
                        
                        //Se crea el mensaje
                        Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '<h4><b>La carga se ha detenido</b></h4>')->html()->persistent("OK");

                    }

                }

                //Preguntamos si $dinamico es false, si es asi...
                if ($dinamico == false) {

                    //Rompemos el carge y segido se muestra el mensaje
                    break;
                }

                /*
                    Lo anterior aplica para las demas validaciones que le corresponda
                */

                $validar_nivelacademico_id = $this->validarTipoDato($estudiante->nivelacademico_id, 'sia_niveles_academicos', 'nivelacademico_id', 'sia');
                $validar_snies =  $this->validarTipoDato($estudiante->snies, 'sia_programas', 'programa_id', 'sia');

                $array_columnas_gra = array('NIVELACADEMICO_ID' => $validar_nivelacademico_id, 'SNIES' => $validar_snies);

                foreach ($array_columnas_gra as $columna => $value) 
                {
                
                    if ($value == 'true') {

                        $nuevo_grado = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'grado_fecha' => $estudiante->grado_fecha,
                            'grado_periodo' => $estudiante->grado_periodo,
                            'grado_titulo' => $estudiante->titulo_obtenido,
                            'nivelacademico_id' => $estudiante->nivelacademico_id,
                            'programa_id' => $estudiante->snies,
                            'created_at' => $estudiante->fecha,
                            'updated_at' => date("Y-m-d G:i:s"),
                        ];

                    } else {

                       $dinamico = false;

                        Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '<h4><b>La carga se ha detenido</b></h4>')->html()->persistent("OK");

                    }

                }

                if ($dinamico == false) {
                    break;
                }

                $validar_sectorempresa = $this->validarTipoDato($estudiante->sectorempresa_id, 'sia_sectores_empresas','sectorempresa_id', 'sia');
                $validar_tipoempresa = $this->validarTipoDato($estudiante->tipoempresa_id, 'sia_tipos_empresas','tipoempresa_id', 'sia');
                $validar_tipocontrato = $this->validarTipoDato($estudiante->contrato_id, 'sia_contratos','contrato_id', 'sia');
                $validar_rangosalario = $this->validarTipoDato($estudiante->rangosalario_id, 'sia_rangos_salarios','rangosalario_id', 'sia');
                $validar_nivelcargo = $this->validarTipoDato($estudiante->nivelcargo_id, 'sia_niveles_cargos','nivelcargo_id', 'sia');
                $validar_situacion = $this->validarTipoDato($estudiante->situacionlaboral_id, 'egresados_situacion_laboral','situacionlaboral_id', 'egresados');
                $validar_area = $this->validarTipoDato($estudiante->areapertenece_id, 'egresados_area_pertenece','areapertenece_id', 'egresados');
                $validar_ocupacion = $this->validarTipoDato($estudiante->ocupacion_id, 'egresados_ocupacion','ocupacion_id', 'egresados');
                $validador_paislaboral = $this->validarTipoDato($estudiante->laboral_pais_id, 'sia_paises', 'pais_id', 'sia');
                $validador_ciudadlaboral = $this->validarTipoDato($estudiante->laboral_ciudad_id, 'sia_ciudades', 'ciudad_id', 'sia');
                $validar_relacion = $this->validarTipoDato($estudiante->relacionprograma_id, 'egresados_relacion_programa', 'relacionprograma_id', 'egresados');
                $validar_duracion = $this->validarTipoDato($estudiante->duracionempresa_id, 'egresados_duracion_empresa', 'duracionempresa_id', 'egresados');
                $validar_vinculacion = $this->validarTipoDato($estudiante->vinculacion_id, 'egresados_vinculacion', 'vinculacion_id', 'egresados');

                $array_columnas_la = array('SECTOREMPRESA_ID' => $validar_sectorempresa,
                                           'TIPOEMPRESA_ID' => $validar_tipoempresa,
                                           'CONTRATO_ID' => $validar_tipocontrato,
                                           'RANGOSALARIO_ID' => $validar_rangosalario,
                                           'NIVELCARGO_ID' => $validar_nivelcargo,
                                           'SITUACIONLABORAL_ID' => $validar_situacion,
                                           'AREAPERTENECE_ID' => $validar_area,
                                           'OCUPACION_ID' => $validar_ocupacion,
                                           'LABORAL_PAIS_ID' => $validador_paislaboral,
                                           'LABORAL_CIUDAD_ID' => $validador_ciudadlaboral,
                                           'RELACIONPROGRAMA_ID' => $validar_relacion,
                                           'DURACIONEMPRESA_ID' => $validar_duracion,
                                           'VINCULACION_ID' => $validar_vinculacion,);
                
                foreach ($array_columnas_la as $columna => $value) {
                    
                    if ($value == 'true') {
                        
                        $nuevo_laboral = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'laboral_empresa' => $estudiante->nombre_empresa,
                            //'laboral_empresadireccion' => $estudiante->direccion_empresa,
                            'laboral_telefono' => $estudiante->laboral_telefono_empresa,
                            'sectorempresa_id' => $estudiante->sectorempresa_id,
                            'contrato_id' => $estudiante->contrato_id,
                            'laboral_inicio' => $estudiante->laboral_inicio,
                            'laboral_fin' => $estudiante->laboral_fin,
                            'tipoempresa_id' => $estudiante->tipoempresa_id,
                            'rangosalario_id' => $estudiante->rangosalario_id,
                            'nivelcargo_id' => $estudiante->nivelcargo_id,
                            'situacionlaboral_id' => $estudiante->situacionlaboral_id,
                            'areapertenece_id' => $estudiante->areapertenece_id,
                            'ocupacion_id' => $estudiante->ocupacion_id,
                            'pais_id' => $estudiante->laboral_pais_id,
                            'ciudad_id' => $estudiante->laboral_ciudad_id,
                            'relacionprograma_id' => $estudiante->relacionprograma_id,
                            'duracionempresa_id' => $estudiante->duracionempresa_id,
                            'vinculacion_id' => $estudiante->vinculacion_id,
                            'created_at' => $estudiante->fecha,
                            'updated_at' => date("Y-m-d G:i:s"),
                            ];

                    } else {

                        $dinamico = false;

                        Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '<h4><b>La carga se ha detenido</b></h4>')->html()->persistent("OK");

                    }

                }

                if ($dinamico == false) {
                
                    break;
                
                }

                /*
                    * Se comentan las siguientes lineas por que no se esta haciendo uso de las columnas en el archivo de cargue las cuales se debe validar
                */
                /*
                $validar_nivelacademico_estudios = $this->validarTipoDato($estudiante->estudios_nivelacademico_id, 
                                                                            'sia_niveles_academicos', 'nivelacademico_id', 'sia');

                $array_columnas_es = array('ESTUDIOS_NIVELACADEMICO_ID' => $validar_nivelacademico_estudios);

                foreach ($array_columnas_es as $columna => $value) {
                    
                    if ($value == 'true') {

                        $nuevo_estudios = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'nivelacademico_id' => $estudiante->estudios_nivelacademico_id,
                            'estudio_institucion' => $estudiante->estudios_institucion,
                            'estudio_titulo' => $estudiante->estudio_titulo,
                            'estudio_inicio' => $estudiante->estudio_inicio,
                            'estudio_fin' => $estudiante->estudio_fin,  
                        ];
                    
                    } else {

                        $dinamico = false;

                        Alert::error("<h4><b>La carga se ha detenido</b></h4><br><b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '¡ERROR!')->html()->persistent("OK");
                    }

                }

                if ($dinamico == false) {
                
                    break;
                
                }

                $validar_sectorempresa_empren = $this->validarTipoDato($estudiante->emprendimiento_sectorempresa_id, 
                                                                        'sia_sectores_empresas','sectorempresa_id', 'sia');
                $validar_pais_empre = $this->validarTipoDato($estudiante->emprendimiento_pais_id, 'sia_paises', 'pais_id', 'sia');
                $valdiar_ciudad_empre = $this->validarTipoDato($estudiante->emprendimiento_ciudad_id, 'sia_ciudades', 'ciudad_id', 'sia');
                $validar_tamano_empresa = $this->validarTipoDato($estudiante->tamanoempresa_id, 'egresados_tamano_empresa', 'tamanoempresa_id', 'egresados');

                $array_columnas_empren = array('EMPRENDIMIENTO_SECTOREMPRESA_ID' => $validar_sectorempresa_empren, 
                                               'EMPRENDIMIENTO_PAIS_ID' => $validar_pais_empre,
                                               'EMPRENDIMIENTO_CIUDAD_ID' => $valdiar_ciudad_empre,
                                               'TAMANOEMPRESA_ID' => $validar_tamano_empresa);



                foreach ($array_columnas_empren as $columna => $value) { 
                    
                    if ($value == 'true') {

                        $nuevo_emprendimiento = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'emprendimiento_empresa' => $estudiante->emprendimiento_nombre_empresa,
                            //'emprendimiento_direccion' => $estudiante->emprendimiento_direccion_empresa,
                            //'emprendimiento_telefono' => $estudiante->emprendimiento_telefono_empresa,
                            'sectorempresa_id' => $estudiante->emprendimiento_sectorempresa_id,
                            'pais_id' => $estudiante->emprendimiento_pais_id,
                            'ciudad_id' => $estudiante->emprendimiento_ciudad_id,
                            'emprendimiento_couching' => $estudiante->requiere_asesoria_emprendimiento,
                            'tamanoempresa_id'=> $estudiante->tamanoempresa_id,

                        ];
                        
                    } else {
                        
                        $dinamico = false;

                       Alert::error("<h4><b>La carga se ha detenido</b></h4><br><b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '¡ERROR!')->html()->persistent("OK");
                    }

                }

                if ($dinamico == false) {
                
                    break;
                
                }

                $nuevo_familia = [
                    'matriculado_pidm' => $estudiante->pidm,
                    'familia_integrantes' => $estudiante->familia_integrantes,
                    'familia_hijos' => $estudiante->hijos,
                    'familia_hermanos' => $estudiante->hermanos,
                    'familia_hermanos_estudios_superiores' => $estudiante->hermanos_estudios_superiores,
                ];

                $validar_pais_refe = $this->validarTipoDato($estudiante->referencia_pais_id, 'sia_paises', 'pais_id', 'sia');
                $validiar_ciudad_refe = $this->validarTipoDato($estudiante->referencia_ciudad_id, 'sia_ciudades', 'ciudad_id', 'sia');

                $array_columnas_refe = array('REFERENCIA_PAIS_ID' => $validar_pais_refe, 'REFERENCIA_CIUDAD_ID' => $validiar_ciudad_refe );

                foreach ($array_columnas_refe as $columna => $value) {
            
                    if ($value == 'true') {
                    
                        $nuevo_referencia = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'referencia_nombres' => $estudiante->referencia_nombres,
                            'referencia_apellidos' => $estudiante->referencia_apellidos,
                            'referencia_telefono' => $estudiante->referencia_telefono,
                            'referencia_celular' => $estudiante->referencia_celular,
                            'referencia_email' => $estudiante->referencia_email,
                            'referencia_direccion' => $estudiante->referencia_direccion,
                            'pais_id' => $estudiante->referencia_pais_id,
                            'ciudad_id' => $estudiante->referencia_ciudad_id,
                        ];
                    
                    } else {

                        $dinamico = false;

                       Alert::error("<h4><b>La carga se ha detenido</b></h4><br><b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, '¡ERROR!')->html()->persistent("OK");
                    }

                }

                if ($dinamico == false) {
                
                    break;
                
                }*/

                $nuevo_referencia = [
                            'matriculado_pidm' => $estudiante->pidm,
                            'referencia_nombres' => $estudiante->referencia_nombres,
                            'referencia_parentesco' => $estudiante->referencia_parentesco,
                            'referencia_telefono' => $estudiante->referencia_telefono,
                            'referencia_celular' => $estudiante->referencia_celular,
                            'created_at' => $estudiante->fecha,
                            'updated_at' => date("Y-m-d G:i:s"),
                        ];
                /*
                    * Se comentan las siguientes lineas por que no se esta haciendo uso de las columnas en el archivo de cargue las cuales se debe validar
                */
                /*
                $nuevo_reconocimiento = [
                    'matriculado_pidm' => $estudiante->pidm,
                    'reconocimiento_nombre' => $estudiante->nombre_reconocimiento_externo,
                    'reconocimiento_organizacion' => $estudiante->organizacion_realiza_reconocimiento,
                    'reconocimiento_fecha' => $estudiante->fecha_reconocimiento, 
                ];
                */
                
                //si $egresado_iden esta vacia..
                if (empty($egresado_iden) || $egresado_iden->count() == 0) 
                {
                    //la variable $egresado contiene el estudiante
                    $egresado = Matriculado::find($estudiante->pidm);

                    /*
                        Puede ser que el numero de identificacion no este en la BD pero el estudiante si este -> Actualiza por que si existe el estudiante
                        
                        Puede ser que el numero de identificacion y el pidm no esten en la BD -> Ingresa el estudiante en la BD
                    */

                    //Preguntamos si $egresado esta vacia, si es asi...
                    if (empty($egresado) || $egresado->count() == 0) 
                    {

                        //Autoincrementa $total_insertados
                        ++$total_insertados;
                        //Creamos la conexion para insertar los arreglos en sus correspondientes tablas
                        DB::connection('sia')->table('sia_matriculados')->insert($nuevo_estudiante);
                        DB::connection('sia')->table('sia_identificacion')->insert($nuevo_identificacion);
                        DB::connection('egresados')->table('egresados_grados')->insert($nuevo_grado);
                        DB::connection('egresados')->table('egresados_laboral')->insert($nuevo_laboral);
                        DB::connection('egresados')->table('egresados_referencias')->insert($nuevo_referencia);
                        
                    } else {

                        ++$total_actualizados;

                        DB::connection('sia')->table('sia_identificacion')->insert($nuevo_identificacion);

                        DB::connection('sia')->table('sia_matriculados')
                                             ->where('matriculado_pidm', $estudiante->pidm)
                                             ->update($nuevo_estudiante);

                        $boolean = false;

                        foreach ($egresado->grados as $grado) 
                        {

                            if ($grado->programa_id == $estudiante->snies) 
                            {
                               
                                $boolean = true;

                            }

                        }

                        if ($boolean == false) 
                        {
                            DB::connection('egresados')->table('egresados_grados')->insert($nuevo_grado);
                        }

                        if (empty($egresado->laboral) || $egresado->laboral->count() == 0) 
                        {
                            
                            DB::connection('egresados')->table('egresados_laboral')->insert($nuevo_laboral);
                        }

                        if (empty($egresado->referencia) || $egresado->referencia->count() == 0) 
                        {
                            
                            DB::connection('egresados')->table('egresados_referencias')->insert($nuevo_referencia);

                        }

                    }

                //Si $egresado_iden no esta vecia
                } else {

                    //Se recore $egresado_iden con el fin de indentificar a quien le pertenece el numero de identificacion
                    foreach ($egresado_iden as $data) 
                    {
                        //$egresado tiene el resultado de la busqueda
                        $egresado = Matriculado::find($data->matriculado_pidm);
                        
                        //Autoincrementa
                        ++$total_actualizados;
                        //398953
                        //Se realiza la conexion y actualizamos los datos
                        DB::connection('sia')->table('sia_matriculados')
                                             ->where('matriculado_pidm', $data->matriculado_pidm)
                                             ->update($nuevo_estudiante);

                        $boolean = false;

                        foreach ($egresado->grados as $grado) 
                        {

                            if ($grado->programa_id == $estudiante->snies) 
                            {
                               
                                $boolean = true;

                            }

                        }

                        if ($boolean == false) 
                        {
                            DB::connection('egresados')->table('egresados_grados')->insert($nuevo_grado);
                        }

                        if (empty($egresado->laboral) || $egresado->laboral->count() == 0) {
                                                        
                            DB::connection('egresados')->table('egresados_laboral')->insert($nuevo_laboral);
                        }
                        
                        if (empty($egresado->referencia) || $egresado->referencia->count() == 0) {
                            
                            DB::connection('egresados')->table('egresados_referencias')->insert($nuevo_referencia);

                        }

                }
            }

            //Si el pidm y la propiedad $identificacion_numero no son correctas
            } else {

                //Se crea el mensaje y posterior a esto se rompe la carga del archivo
                Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Verifica que el numero de documento y el PIDM esten bien insertados</b></br><b><br>Total registros insertados:</b>" . $total_insertados . "<br><b>Total registros actualizados:</b>" . $total_actualizados, "<h4><b>La carga se ha detenido</b></h4>")->html()->persistent("OK");

                break;

            }

            //Si $dinamico es diferente de true...
            if ($dinamico != false) {
                
                //Se crea el mensaje donde indica que todos los datos fueron insertados y/o actulzados extosamente
                Alert::success('</h4><b>Total registros insertados:</b>' . $total_insertados . '<br><b>Total registros actualizados:</b> ' . $total_actualizados, "<h4><b>Los registros han sido cargados</b>")->html()->persistent("OK");

            }
            //Auto incrementa, para saber en que fila se encuentra el problema si lo llega haber
            ++$contador;
            set_time_limit(30);

        }

        return redirect()->back();

    }

    //Funcion que sirve para renderizar a la vista de filtros para listar y exportar
    public function index_formData(Sistema $sistema)
    {
 
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $ciudad = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();
        $departamento = Departamento::orderBy('departamento_nombre', 'ASC')->get();

        $cargo = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();
        $tipoEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();
        $contrato = Contrato::orderBy('contrato_nombre', 'ASC')->get();
        $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();
        $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_id', 'ASC')->get();
        $ocupacion = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();
        $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();
        $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();
        $vinculacion = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $tamanoEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();
        $logros = Logros::orderBy('logro_nombre', 'ASC')->get();        

        return view('browse.egresados.reportes.formData')
            ->with('tamanoEmpresa', $tamanoEmpresa)
            ->with('ciudad', $ciudad)
            ->with('departamento', $departamento)
            ->with('subareas', $subareas)
            ->with('areaPertenece', $areaPertenece)
            ->with('tipoEmpresa', $tipoEmpresa)
            ->with('cargo', $cargo)
            ->with('contrato', $contrato)
            ->with('duracionEmpresa', $duracionEmpresa)
            ->with('ocupacion', $ocupacion)
            ->with('relacionPrograma', $relacionPrograma)
            ->with('situacionLaboral', $situacionLaboral)
            ->with('vinculacion', $vinculacion)
            ->with('rangoSalarial', $rangoSalarial)
            ->with('sectorEmpresa', $sectorEmpresa)
            ->with('logros', $logros)
            ->with('sistemas', $sistemas)
            ->with('sistema', $sistema);

    }

    public function form_graphics(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.egresados.reportes.formGraphics')
                    ->with('sistema', $sistema)
                    ->with('sistemas', $sistemas);
    }

    public function form(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();

        return view('browse.egresados.formulario.form')
            ->with('subareas', $subareas)
            ->with('rangoSalarial', $rangoSalarial)
            ->with('sectorEmpresa', $sectorEmpresa)
            ->with('sistemas', $sistemas)
            ->with('sistema', $sistema);
    }

    //Generar Filtros, exportar excel, Listar datos
    public function generate_data(Request $request, Sistema $sistema)
    {
        //dd($request->all());
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();
        $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
        
        if (isset($_REQUEST['action'])) 
        {
            $action = $_REQUEST['action'];
        }

        $url_pagination = array();

        $select_situacion_laboral = null;
        $select_duracion = null;
        $select_cargo = null;
        $select_contrato = null;
        $select_sectorEmpresa = null;
        $select_rangoSalarial = null;
        $select_relacionPrograma = null;
        $select_tipoVinculacion = null;
        $select_ocupacionActual = null;
        $select_areaPertenece = null;
        $select_tipoEmpresa = null;
        $select_sectorEmpresa_Bu = null;
        $select_Ciudad_Bu = null;
        $select_tamaño_empresa = null;
        $select_merito = null;

        $var_laboral = '';
        $var_duracion = '';
        $contenedor_sql= '';
        $contenedor_duracion = '';
        $consulta_general = '';
        $var_cargo = '';
        $var_contrato = '';
        $var_sectorEmpresa = '';
        $var_salarial = '';
        $var_vinculacion = '';
        $var_ocupacion = '';
        $datos_list = '';
        $var_sectorEmpresa_Bu = '';
        $var_ciudad_Bu = '';
        $var_tamaño_empresa = '';
        $var_areaPertenece = '';
        $var_tipoempresa = '';

        //Join que tiene por objetivo traer el nombre de las variables que se seleccionan para su visualizacion en un archivo excel 
        $join = '';
        $colunm = '';

        $array = ['CEDULA', 
                 'PRIMER NOMBRE',
                 'SEGUNDO NOMBRE', 
                 'APELLIDOS',  
                 'TELEFONO', 
                 'CELULAR', 
                 'CORREO', 
                 'PROGRAMA', 
                 'PERIODO',
                 'SEXO',
                 'ESTDO CIVIL',
                 'DIA',
                 'MES',
                 'AÑO'];

        //Arreglo | Insercion de los filtros seleccionados
        $array_content_filters = array();


        if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) 
            || isset($_GET['select_cargo']) || isset($_GET['select_contrato']) 
            || isset( $_GET['select_sectorEmpresa']) || isset($_GET['select_rangoSalarial']) 
            || isset($_GET['select_relacionPrograma']) || isset($_GET['select_tipoVinculacion']) 
            || isset($_GET['select_ocupacionActual']) || isset($_GET['select_areaPertenece'])
            || isset($_GET['select_tipoEmpresa']))
        {
            
            $colunm .= ', egresados_laboral.updated_at as date_laboral, 
                        egresados_laboral.laboral_empresa,
                        egresados_laboral.laboral_telefono';

           $join .= "INNER JOIN (
                        SELECT
                            ELaboral.*
                        FROM 
                            ".env('EGRESADOS_DATABASE').".egresados_laboral ELaboral
                        INNER JOIN (
                            SELECT
                                MAX(EL2.updated_at) AS updated_at1,
                                EL2.matriculado_pidm
                            FROM 
                                ".env('EGRESADOS_DATABASE').".egresados_laboral EL2
                            GROUP BY
                                EL2.matriculado_pidm
                        ) RU ON RU.matriculado_pidm = ELaboral.matriculado_pidm
                        AND RU.updated_at1 = ELaboral.updated_at
                    ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                    ";

        }

        //Si esta definido situacion laboral
        if (isset($_GET['select_situacion_laboral']))    
        {

         array_push($array, 'SITUACION LABORAL');
         //dd($array);
         $colunm .= ", situacionlaboral_nombre";
         $join .= "LEFT JOIN (
                            SELECT
                                situacionLaboral.situacionlaboral_id,
                                situacionLaboral.situacionlaboral_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_situacion_laboral situacionLaboral
                        ) AS EL ON egresados_laboral.situacionlaboral_id = EL.situacionlaboral_id";

            $select_situacion_laboral = $_GET['select_situacion_laboral'];

            $situacionLaboral = SituacionLaboral::orderBy('situacionlaboral_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_situacion_laboral); $i++) 
            { 
                
                foreach ($situacionLaboral as $laboral) 
                { 
                    if ($select_situacion_laboral[$i] == $laboral->situacionlaboral_id) 
                    {
                        array_push($array_content_filters, $laboral->situacionlaboral_nombre);
                    }
                }

                if ($i == 0) {

                    $var_laboral = " (egresados_laboral.situacionlaboral_id = '".$select_situacion_laboral[$i]."'";

                } else {
                    $var_laboral = " OR egresados_laboral.situacionlaboral_id = '".$select_situacion_laboral[$i]."'";
                }
                 $contenedor_sql = $contenedor_sql . $var_laboral;
            }  
           

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido duracion en su ultimo empleo
        if (isset($_GET['select_duracion'])) 
        {

            array_push($array, 'DURACION EMPRESA');

            $colunm .= ", duracionempresa_nombre";

            $join .= " LEFT JOIN (
                        SELECT
                            duracion.duracionempresa_id,
                            duracion.duracionempresa_nombre
                        FROM
                            ".env('EGRESADOS_DATABASE').".egresados_duracion_empresa duracion
                    ) AS ELD ON egresados_laboral.duracionempresa_id = ELD.duracionempresa_id";
            //Se coloca el array del select en una variable
            $select_duracion = $_GET['select_duracion'];
            $duracionEmpresa = DuracionEmpresa::orderBy('duracionempresa_nombre', 'ASC')->get();
            //Se recorre la varriable que se manda por el formulario
            for ($i=0; $i < sizeof($select_duracion); $i++) 
            { 

                //Se recore el arreglo para poder guardar el nombre de lo que se esta mandado
                foreach ($duracionEmpresa as $duracion) 
                { 
                    if ($select_duracion[$i] == $duracion->duracionempresa_id) 
                    {
                        array_push($array_content_filters, $duracion->duracionempresa_nombre);
                    }
                }
                //Si el anterior select esta definido
                if (isset($_GET['select_situacion_laboral'])) 
                {
                    //si i es = 0, esto con el fin realizar la sentencia
                    if ($i == 0) {
                        //Se coloca una cadena de carracteres junto la variable en la posicion i del arreglo
                    $var_duracion = " AND (egresados_laboral.duracionempresa_id = '".$select_duracion[$i]."'";
                    //de lo contrario
                    } else {
                        //Se coloca una cadena de carracteres junto la variable en la posicion i del arreglo
                        $var_duracion = " OR egresados_laboral.duracionempresa_id = '".$select_duracion[$i]."'";
                    }
                    //una variable la cual concatenara el resultado de la variable $var_duracion
                    $contenedor_sql = $contenedor_sql . $var_duracion;

                } else {

                    if ($i == 0) {
                    $var_duracion = " (egresados_laboral.duracionempresa_id = '".$select_duracion[$i]."'";
                    } else {
                        $var_duracion = " OR egresados_laboral.duracionempresa_id = '".$select_duracion[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_duracion;
                }
            }

            $contenedor_sql = $contenedor_sql . ')' ;
        }
        //Si esta definido el cargo laboral
        if (isset($_GET['select_cargo'])) 
        {
            array_push($array, 'CARGO');

            $colunm .= ", nivelcargo_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                cargo.nivelCargo_id,
                                cargo.nivelcargo_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_niveles_cargos cargo
                        ) AS ELC ON egresados_laboral.nivelcargo_id = ELC.nivelCargo_id";

            $select_cargo = $_GET['select_cargo'];

            $cargos = Cargo::orderBy('nivelcargo_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_cargo); $i++) 
            { 

                foreach ($cargos as $cargo) 
                { 
                    if ($select_cargo[$i] == $cargo->nivelcargo_id) 
                    {
                        array_push($array_content_filters, $cargo->nivelcargo_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']))
                {

                    if ($i == 0) {
                    $var_cargo = " AND (egresados_laboral.nivelcargo_id = '".$select_cargo[$i]."'";
                    } else {
                        $var_cargo = " OR egresados_laboral.nivelcargo_id = '".$select_cargo[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_cargo;

                } else {

                    if ($i == 0) {
                    $var_cargo = " (egresados_laboral.nivelcargo_id = '".$select_cargo[$i]."'";
                    } else {
                        $var_cargo = " OR egresados_laboral.nivelcargo_id = '".$select_cargo[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_cargo;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el contrato esta definido
        if (isset($_GET['select_contrato'])) 
        {

            array_push($array, 'CONTRATO');

            $colunm .= ", contrato_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                contrato.contrato_id,
                                contrato.contrato_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_contratos contrato
                        ) AS ELCT ON egresados_laboral.contrato_id = ELCT.contrato_id";

            $select_contrato = $_GET['select_contrato'];

            $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_contrato); $i++) 
            { 
                foreach ($contratos as $contrato) 
                { 
                    if ($select_contrato[$i] == $contrato->contrato_id) 
                    {
                        array_push($array_content_filters, $contrato->contrato_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || 
                    isset($_GET['select_cargo']))
                {

                    if ($i == 0) {
                    $var_contrato = " AND (egresados_laboral.contrato_id = '".$select_contrato[$i]."'";
                    } else {
                        $var_contrato = " OR egresados_laboral.contrato_id = '".$select_contrato[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_contrato;

                } else {

                    if ($i == 0) {
                    $var_contrato = " (egresados_laboral.contrato_id = '".$select_contrato[$i]."'";
                    } else {
                        $var_contrato = " OR egresados_laboral.contrato_id = '".$select_contrato[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_contrato;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el selector de la empresa
        if (isset($_GET['select_sectorEmpresa'])) 
        {

            array_push($array, 'SECTRO EMPRESA LABORAL');

            $colunm .= ", sectorempresa_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                sectroEmpresa.sectorempresa_id,
                                sectroEmpresa.sectorempresa_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_sectores_empresas sectroEmpresa
                        ) AS ELS ON egresados_laboral.sectorempresa_id = ELS.sectorempresa_id";

            $select_sectorEmpresa = $_GET['select_sectorEmpresa'];

            $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_sectorEmpresa); $i++) 
            { 
                foreach ($sectorEmpresa as $sector) 
                { 
                    if ($select_sectorEmpresa[$i] == $sector->sectorempresa_id) 
                    {
                        array_push($array_content_filters, $sector->sectorempresa_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) || 
                    isset($_GET['select_contrato']))
                {

                    if ($i == 0) {
                    $var_sectorEmpresa = " AND (egresados_laboral.sectorempresa_id = '".$select_sectorEmpresa[$i]."'";
                    } else {
                    $var_sectorEmpresa = " OR egresados_laboral.sectorempresa_id = '".$select_sectorEmpresa[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_sectorEmpresa;

                } else {
                   
                    if ($i == 0) {
                    $var_sectorEmpresa = " (egresados_laboral.sectorempresa_id = '".$select_sectorEmpresa[$i]."'";
                    } else {
                    $var_sectorEmpresa = " OR egresados_laboral.sectorempresa_id = '".$select_sectorEmpresa[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_sectorEmpresa;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el selector de la rango salario
        if (isset($_GET['select_rangoSalarial'])) 
        {

            array_push($array, 'SALARIO');

            $colunm .= ", rangosalario_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                salario.rangosalario_id,
                                salario.rangosalario_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_rangos_salarios salario
                        ) AS ELRS ON egresados_laboral.rangosalario_id = ELRS.rangosalario_id";

            $select_rangoSalarial = $_GET['select_rangoSalarial'];

            $rangoSalarial = RangoSalarial::orderBy('rangosalario_id', 'ASC')->get();   
            
            for ($i=0; $i < sizeof($select_rangoSalarial); $i++) 
            {  
                foreach ($rangoSalarial as $rango) 
                { 
                    if ($select_rangoSalarial[$i] == $rango->rangosalario_id) 
                    {
                        array_push($array_content_filters, $rango->rangosalario_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) || 
                    isset($_GET['select_contrato']) || isset($_GET['select_sectorEmpresa']))
                {

                    if ($i == 0) {
                    $var_salarial = " AND (egresados_laboral.rangosalario_id = '".$select_rangoSalarial[$i]."'";
                    } else {
                        $var_salarial = " OR egresados_laboral.rangosalario_id = '".$select_rangoSalarial[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_salarial;

                } else {

                    if ($i == 0) {
                    $var_salarial = " (egresados_laboral.rangosalario_id = '".$select_rangoSalarial[$i]."'";
                    } else {
                        $var_salarial = " OR egresados_laboral.rangosalario_id = '".$select_rangoSalarial[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_salarial;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el selector de realacion con el programa
        if (isset($_GET['select_relacionPrograma'])) 
        {

            array_push($array, 'RELACION PROGRAMA');

            $colunm .= ", relacionprograma_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                relacion.relacionprograma_id,
                                relacion.relacionprograma_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_relacion_programa relacion
                        ) AS ELRP ON egresados_laboral.relacionPrograma_id = ELRP.relacionPrograma_id";

            $select_relacionPrograma = $_GET['select_relacionPrograma'];

            $relacionPrograma = RelacionPrograma::orderBy('relacionprograma_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_relacionPrograma); $i++) 
            { 

                foreach ($relacionPrograma as $relacion) 
                { 
                    if ($select_relacionPrograma[$i] == $relacion->relacionprograma_id) 
                    {
                        array_push($array_content_filters, $relacion->relacionprograma_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) || 
                    isset($_GET['select_contrato']) || isset($_GET['select_sectorEmpresa']) || 
                    isset($_GET['select_rangoSalarial']))
                {

                    if ($i == 0) {
                    $var_relacion = " AND (egresados_laboral.relacionprograma_id = '".$select_relacionPrograma[$i]."'";
                    } else {
                        $var_relacion = " OR egresados_laboral.relacionprograma_id = '".$select_relacionPrograma[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_relacion;

                } else {

                    if ($i == 0) {
                    $var_relacion = " (egresados_laboral.relacionprograma_id = '".$select_relacionPrograma[$i]."'";
                    } else {
                        $var_relacion = " OR egresados_laboral.relacionprograma_id = '".$select_relacionPrograma[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_relacion;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el selector tipo de vinculacion con el programa
        if (isset($_GET['select_tipoVinculacion'])) 
        {

            array_push($array, 'TIPO VINCULACION');

            $colunm .= ", vinculacion_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                vinculacion.vinculacion_id,
                                vinculacion.vinculacion_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_vinculacion vinculacion
                        ) AS ELV ON egresados_laboral.vinculacion_id = ELV.vinculacion_id";

            $select_tipoVinculacion = $_GET['select_tipoVinculacion'];

            $vinculaciones = Vinculacion::orderBy('vinculacion_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_tipoVinculacion); $i++) 
            { 

               foreach ($vinculaciones as $vinculacion) 
                { 
                    if ($select_tipoVinculacion[$i] == $vinculacion->vinculacion_id) 
                    {
                        array_push($array_content_filters, $vinculacion->vinculacion_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) || 
                    isset($_GET['select_contrato']) || isset($_GET['select_sectorEmpresa']) || 
                    isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma'])) 
                {

                    if ($i == 0) {
                    $var_vinculacion = " AND (egresados_laboral.vinculacion_id = '".$select_tipoVinculacion[$i]."'";
                    } else {
                        $var_vinculacion = " OR egresados_laboral.vinculacion_id = '".$select_tipoVinculacion[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_vinculacion;

                } else {

                    if ($i == 0) {
                    $var_vinculacion = " (egresados_laboral.vinculacion_id = '".$select_tipoVinculacion[$i]."'";
                    } else {
                        $var_vinculacion = " OR egresados_laboral.vinculacion_id = '".$select_tipoVinculacion[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_vinculacion;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definido el selector de ocupacion actual
        if (isset($_GET['select_ocupacionActual'])) 
        {

            array_push($array, 'OCUPACION');

            $colunm .= ", ocupacion_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                ocupacion.ocupacion_id,
                                ocupacion.ocupacion_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_ocupacion ocupacion
                        ) AS ELO ON egresados_laboral.ocupacion_id = ELO.ocupacion_id";

            $select_ocupacionActual = $_GET['select_ocupacionActual'];

            $ocupaciones = Ocupacion::orderBy('ocupacion_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_ocupacionActual); $i++) 
            { 

               foreach ($ocupaciones as $ocupacion) 
                { 
                    if ($select_ocupacionActual[$i] == $ocupacion->ocupacion_id) 
                    {
                        array_push($array_content_filters, $ocupacion->ocupacion_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) || 
                    isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                    isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                    isset($_GET['select_tipoVinculacion']))
                {

                    if ($i == 0) {
                    $var_ocupacion = " AND (egresados_laboral.ocupacion_id = '".$select_ocupacionActual[$i]."'";
                    } else {
                        $var_ocupacion = " OR egresados_laboral.ocupacion_id = '".$select_ocupacionActual[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_ocupacion;

                } else {

                    if ($i == 0) {
                    $var_ocupacion = " (egresados_laboral.ocupacion_id = '".$select_ocupacionActual[$i]."'";
                    } else {
                        $var_ocupacion = " OR egresados_laboral.ocupacion_id = '".$select_ocupacionActual[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_ocupacion;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si el esta el tipo de empresa esta definida
        if (isset($_GET['select_areaPertenece'])) 
        {

            array_push($array, 'AREA A LA QUE PERTENECE');

            $colunm .= ", areapertenece_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                area.areapertenece_id,
                                area.areapertenece_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_area_pertenece area
                        ) AS ELA ON egresados_laboral.areapertenece_id = ELA.areapertenece_id";

            
            $select_areaPertenece = $_GET['select_areaPertenece'];

            $areaPertenece = AreaPertenece::orderBy('areapertenece_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_areaPertenece); $i++) 
            { 
                foreach ($areaPertenece as $area) 
                { 
                    if ($select_areaPertenece[$i] == $area->areapertenece_id) 
                    {
                        array_push($array_content_filters, $area->areapertenece_nombre);
                    }
                }

            if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) ||
                isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']))
               {

                    if ($i == 0) {
                    $var_areaPertenece = " AND (egresados_laboral.areapertenece_id = '".$select_areaPertenece[$i]."'";
                    } else {
                    $var_areaPertenece = " OR egresados_laboral.areapertenece_id = '".$select_areaPertenece[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_areaPertenece;

                } else {
                   
                    if ($i == 0) {
                    $var_areaPertenece = " (egresados_laboral.areapertenece_id = '".$select_areaPertenece[$i]."'";
                    } else {
                    $var_areaPertenece = " OR egresados_laboral.areapertenece_id = '".$select_areaPertenece[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_areaPertenece;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }

        if (isset($_GET['select_tipoEmpresa'])) 
        {

            array_push($array, 'TIPO EMPRESA');

            $colunm .= ", tipoempresa_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                tiposEmpresas.tipoempresa_id,
                                tiposEmpresas.tipoempresa_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_tipos_empresas tiposEmpresas
                        ) AS ELTE ON egresados_laboral.tipoempresa_id = ELTE.tipoempresa_id";
            
            $select_tipoEmpresa = $_GET['select_tipoEmpresa'];

            $tiposEmpresa = TipoEmpresa::orderBy('tipoempresa_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_tipoEmpresa); $i++) 
            { 
                foreach ($tiposEmpresa as $tipoEmpresa) 
                { 
                    if ($select_tipoEmpresa[$i] == $tipoEmpresa->tipoempresa_id) 
                    {
                        array_push($array_content_filters, $tipoEmpresa->tipoempresa_nombre);
                    }
                }

            if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) ||
                isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']) || 
                isset($_GET['select_areaPertenece']))
               {

                    if ($i == 0) {
                    $var_tipoempresa = " AND (egresados_laboral.tipoempresa_id = '".$select_tipoEmpresa[$i]."'";
                    } else {
                    $var_tipoempresa = " OR egresados_laboral.tipoempresa_id = '".$select_tipoEmpresa[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_tipoempresa;

                } else {
                   
                    if ($i == 0) {
                    $var_tipoempresa = " (egresados_laboral.tipoempresa_id = '".$select_tipoEmpresa[$i]."'";
                    } else {
                    $var_tipoempresa = " OR egresados_laboral.tipoempresa_id = '".$select_tipoEmpresa[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_tipoempresa;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }

        if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) 
            || isset($_GET['select_cargo']) || isset($_GET['select_contrato']) 
            || isset( $_GET['select_sectorEmpresa']) || isset($_GET['select_rangoSalarial']) 
            || isset($_GET['select_relacionPrograma']) || isset($_GET['select_tipoVinculacion']) 
            || isset($_GET['select_ocupacionActual']) || isset($_GET['select_areaPertenece'])
            || isset($_GET['select_tipoEmpresa']))
        {
            
            array_push($array,  'LABORAL_EMPRESA',
                                'LABORAL_TELEFONO',
                                'AÑO ACTUALIZACION INFO LABORAL', 
                                'MES ACTUALIZACION INFO LABORAL',
                                'DIA ACTUALIZACION INFO LABORAL');

        }

        //Si el esta definido el sector de la empresa de info emprendimiento
        if (isset($_GET['select_sectorEmpresa_Bu'])) 
        {

            array_push($array, 'SECTOR EMPRESA EMPRENDIMIENTO');

            $colunm .= ", sectorempresa_nombre_Emprendimiento 
                        , egresados_emprendimiento.updated_at AS date_emprendimiento";

            $join .= " LEFT JOIN (
                            SELECT
                                sectorEmpresaEmprendimiento.sectorempresa_id,
                                sectorEmpresaEmprendimiento. sectorempresa_nombre AS sectorempresa_nombre_Emprendimiento
                            FROM
                                ".env('DB_DATABASE').".sia_sectores_empresas sectorEmpresaEmprendimiento
                        ) AS EESE ON egresados_emprendimiento.sectorempresa_id = EESE.sectorempresa_id";
            
            $select_sectorEmpresa_Bu = $_GET['select_sectorEmpresa_Bu'];

            $sectorEmpresa = SectorEmpresa::orderBy('sectorempresa_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_sectorEmpresa_Bu); $i++) 
            { 
                foreach ($sectorEmpresa as $sector) 
                { 
                    if ($select_sectorEmpresa_Bu[$i] == $sector->sectorempresa_id) 
                    {
                        array_push($array_content_filters, $sector->sectorempresa_nombre);
                    }
                }

            if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) || isset($_GET['select_cargo']) ||
                isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']) || 
                isset($_GET['select_areaPertenece']))
               {

                    if ($i == 0) {
                    $var_sectorEmpresa_Bu = " AND (egresados_emprendimiento.sectorempresa_id = '".$select_sectorEmpresa_Bu[$i]."'";
                    } else {
                    $var_sectorEmpresa_Bu = " OR egresados_emprendimiento.sectorempresa_id = '".$select_sectorEmpresa_Bu[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_sectorEmpresa_Bu;

                } else {
                   
                    if ($i == 0) {
                    $var_sectorEmpresa_Bu = " (egresados_emprendimiento.sectorempresa_id = '".$select_sectorEmpresa_Bu[$i]."'";
                    } else {
                    $var_sectorEmpresa_Bu = " OR egresados_emprendimiento.sectorempresa_id = '".$select_sectorEmpresa_Bu[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_sectorEmpresa_Bu;
                }
            }

            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definidad la Ciudad
        if (isset($_GET['select_Ciudad_Bu']) && $_GET['select_Ciudad_Bu'][0] != null) 
        {

            array_push($array, 'CIUDAD EMPRENDIMIENTO');

            $colunm .= ", ciudad_nombre";
            
            $join .= " LEFT JOIN (
                            SELECT
                                ciudadEmprendimiento.ciudad_id,
                                ciudadEmprendimiento.ciudad_nombre
                            FROM
                                ".env('DB_DATABASE').".sia_ciudades ciudadEmprendimiento
                        ) AS EEC ON egresados_emprendimiento.ciudad_id = EEC.ciudad_id";

            $select_Ciudad_Bu = $_GET['select_Ciudad_Bu'];

            $ciudades = Ciudad::orderBy('ciudad_nombre', 'ASC')->get();

            for ($i=1; $i < sizeof($select_Ciudad_Bu) ; $i++) 
            { 

                foreach ($ciudades as $ciudad) 
                { 
                    if ($select_Ciudad_Bu[$i] == $ciudad->ciudad_id) 
                    {
                        array_push($array_content_filters, $ciudad->ciudad_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) ||
                    isset($_GET['select_cargo']) ||
                    isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                    isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                    isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']) ||  
                    isset($_GET['select_sectorEmpresa_Bu']) || isset($_GET['select_areaPertenece']) || 
                    isset($_GET['select_tipoEmpresa']))

               {

                    if ($i == 1) {
                    $var_ciudad_Bu = " AND (egresados_emprendimiento.ciudad_id = '".
                                            $select_Ciudad_Bu[$i]."'";
                    } else {
                    $var_ciudad_Bu = " OR egresados_emprendimiento.ciudad_id = '".
                                            $select_Ciudad_Bu[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_ciudad_Bu;

                } else {

                    $colunm.= ', egresados_emprendimiento.updated_at AS date_emprendimiento';

                    if ($i == 1) {
                    $var_ciudad_Bu = " (egresados_emprendimiento.ciudad_id = '".
                                            $select_Ciudad_Bu[$i]."'";
                    } else {
                    $var_ciudad_Bu = " OR egresados_emprendimiento.ciudad_id = '".
                                            $select_Ciudad_Bu[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_ciudad_Bu;
                }
            }
            $contenedor_sql = $contenedor_sql . ')';
        }
        //Si esta definida la Ciudad de ubicacion de la empresa
        if (isset($_GET['select_tamaño_empresa'])) 
        {

            array_push($array, 'TAMAÑO EMPRESA');

            $colunm .= ", tamanoempresa_nombre";

            $join .= " LEFT JOIN (
                            SELECT
                                tamanoEmpresa.tamanoempresa_id,
                                tamanoEmpresa.tamanoempresa_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_tamano_empresa tamanoEmpresa
                        ) AS EETE ON egresados_emprendimiento.tamanoempresa_id = EETE.tamanoEmpresa_id";

            
            $select_tamaño_empresa = $_GET['select_tamaño_empresa'];

            $tamanosEmpresa = TamanoEmpresa::orderBy('tamanoempresa_nombre', 'ASC')->get();

            for ($i=0; $i < sizeof($select_tamaño_empresa); $i++) 
            { 

                foreach ($tamanosEmpresa as $tamanoEmpresa) 
                { 
                    if ($select_tamaño_empresa[$i] == $tamanoEmpresa->tamanoempresa_id) 
                    {
                        array_push($array_content_filters, $tamanoEmpresa->tamanoempresa_nombre);
                    }
                }

                if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) ||
                isset($_GET['select_cargo']) ||
                isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']) || 
                isset($_GET['select_sectorEmpresa_Bu']) || isset($_GET['select_areaPertenece']) || 
                isset($_GET['select_tipoEmpresa']) || isset($_GET['select_Ciudad_Bu']))

               {

                if ($i == 0) {
                    $var_tamaño_empresa = " AND (egresados_emprendimiento.tamanoempresa_id = '".$select_tamaño_empresa[$i]."'";
                    } else {
                    $var_tamaño_empresa = " OR egresados_emprendimiento.tamanoempresa_id = '".$select_tamaño_empresa[$i]."'";
                    }
                    $contenedor_sql = $contenedor_sql . $var_tamaño_empresa;

                } else {

                    $colunm.= ', egresados_emprendimiento.updated_at AS date_emprendimiento';
                   
                    if ($i == 0) {
                    $var_tamaño_empresa = " (egresados_emprendimiento.tamanoempresa_id = '".$select_tamaño_empresa[$i]."'";
                    } else {
                    $var_tamaño_empresa = " OR egresados_emprendimiento.tamanoempresa_id = '".$select_tamaño_empresa[$i]."'";
                    }

                    $contenedor_sql = $contenedor_sql . $var_tamaño_empresa;
                }
            }
            $contenedor_sql = $contenedor_sql . ')';
        }   

        if (isset($_GET['select_sectorEmpresa_Bu']) 
            || isset($_GET['select_Ciudad_Bu']) && $_GET['select_Ciudad_Bu'][0] != null
            || isset($_GET['select_tamaño_empresa'])) 
        {

            array_push($array, 'AÑO ACTUALIZACION INFO EMPRENDIMIENTO', 
                                        'MES ACTUALIZACION INFO EMPRENDIMIENTO',
                                        'DIA ACTUALIZACION INFO EMPRENDIMIENTO');
        }

        if (isset($_GET['select_merito'])) 
        {
            
            array_push($array, 'LOGRO', 
                                'AÑO ACTUALIZACION INFO LOGRO', 
                                'MES ACTUALIZACION INFO LOGRO',
                                'DIA ACTUALIZACION INFO LOGRO');

            $colunm .= ", logro_nombre, egresados_matriculado_logro.updated_at as date_logro";

            $join .= " LEFT JOIN (
                            SELECT
                                logro.logro_id,
                                logro.logro_nombre
                            FROM
                                ".env('EGRESADOS_DATABASE').".egresados_logros logro
                        ) AS ELo ON egresados_matriculado_logro.logro_id = ELo.logro_id";

                $select_merito = $_GET['select_merito'];

                $logros = Logros::orderBy('logro_nombre', 'ASC')->get();  

                for ($i=0; $i < sizeof($select_merito); $i++) 
                { 
                         
                    foreach ($logros as $logro) 
                    { 
                        if ($select_merito[$i] == $logro->logro_id) 
                        {
                            array_push($array_content_filters, $logro->logro_nombre);
                        }
                    }

                    if (isset($_GET['select_situacion_laboral']) || isset($_GET['select_duracion']) ||
                        isset($_GET['select_cargo']) ||
                        isset($_GET['select_contrato']) || isset( $_GET['select_sectorEmpresa']) ||
                        isset($_GET['select_rangoSalarial']) || isset($_GET['select_relacionPrograma']) || 
                        isset($_GET['select_tipoVinculacion']) || isset($_GET['select_ocupacionActual']) || 
                        isset($_GET['select_sectorEmpresa_Bu']) || isset($_GET['select_tamaño_empresa']) || 
                        isset($_GET['select_areaPertenece']) || isset($_GET['select_tipoEmpresa']) || 
                        isset($_GET['select_Ciudad_Bu']))
                    {

                        if ($i == 0) {
                        $var_logro = " AND (egresados_matriculado_logro.logro_id = '".$select_merito[$i]."'";
                        } else {
                        $var_logro = " OR egresados_matriculado_logro.logro_id = '".$select_merito[$i]."'";
                        }
                        $contenedor_sql = $contenedor_sql . $var_logro;

                    } else {

                        if ($i == 0) {
                        $var_logro = " (egresados_matriculado_logro.logro_id = '".$select_merito[$i]."'";
                        } else {
                        $var_logro = " OR egresados_matriculado_logro.logro_id = '".$select_merito[$i]."'";
                        }

                        $contenedor_sql = $contenedor_sql . $var_logro;
                    }
                }

            $contenedor_sql = $contenedor_sql . ')';                
        }         
                
                $filters = implode(', ', $array_content_filters);

                $queryWhere = '';
                $programSelected = '';
                $i = 0;

                //$arrayProgram = explode(',', $request->name_program);
                //dd($request->name_program);
                foreach ($request->name_program as $program) 
                { 
                    if ($program != 'all') 
                    {
                    
                        if ($i > 0) {

                            $queryWhere .= " OR EG.programa_id = ".$program;

                        } else {

                            $queryWhere = " EG.programa_id = ".$program;
                            $i++;
                        }

                    } else {

                        $programSelected = 'all';

                    }

                }

                //Si el contenedor no esta nulo
                if ($contenedor_sql != '') 
                {
                     //dd($contenedor_sql);
                    if ($programSelected == 'all' && $request->name_period != 'all' 
                        && isset($request->name_period) && !isset($request->name_period_until)) {
                        //Consulta Sql
                        $consulta_general = $contenedor_sql . " AND (grado_periodo = '$request->name_period')";

                    } elseif ($programSelected != 'all' && $request->name_period == 'all') {
                        //Consulta Sql
                        $consulta_general = $contenedor_sql . " AND (".$queryWhere.")";

                    } elseif ($programSelected == 'all' && $request->name_period == 'all') {
                        //Consulta Sql
                        $consulta_general = $contenedor_sql;

                    } elseif ($programSelected != 'all' && $request->name_period != 'all' 
                        && isset($request->name_period) && !isset($request->name_period_until)) {
                        //Consulta Sql
                        $consulta_general = $contenedor_sql . " AND (".$queryWhere.")
                                                                AND (grado_periodo = '$request->name_period')";

                    } elseif ($programSelected != 'all' && !isset($request->name_period) && 
                                                                                        isset($request->name_period_until)) {

                       $consulta_general = $contenedor_sql . " AND (".$queryWhere.")
                                                               AND grado_periodo BETWEEN '$request->name_period_since'
                                                               AND '$request->name_period_until'";

                    } elseif ($programSelected == 'all' && !isset($request->name_period) && 
                                                                                        isset($request->name_period_until)) {

                       $consulta_general = $contenedor_sql . " AND grado_periodo BETWEEN '$request->name_period_since'                                                     AND '$request->name_period_until'";

                    } elseif ($programSelected == 'all' && !isset($request->name_period) && isset($request->name_year_graduate)) {

                        $consulta_general = $contenedor_sql . " AND grado_fecha BETWEEN date_sub(now(), 
                                                                interval '$request->name_year_graduate' year)  
                                                                AND NOW()";
                    
                    } elseif ($programSelected != 'all' && !isset($request->name_period) && isset($request->name_year_graduate)) {
                      
                        $consulta_general = $contenedor_sql . " AND grado_fecha BETWEEN date_sub(now(), 
                                                                interval '$request->name_year_graduate' year)  
                                                                AND NOW() 
                                                                AND (".$queryWhere.")";
                    }

                } else {
                    //dd($request->all());
                    if($request->name_period != null || isset($request->name_period_until) ||
                        $request->name_year_graduate != null)
                    {
                        if ($programSelected == 'all' && $request->name_period != 'all' 
                            && isset($request->name_period) && !isset($request->name_period_until)) {
                            //Consulta Sql
                            $consulta_general = " (grado_periodo = '$request->name_period')";

                        } elseif ($programSelected != 'all' && $request->name_period == 'all') {
                            //Consulta Sql
                            $consulta_general = " (".$queryWhere.")";

                        } elseif ($programSelected == 'all' && $request->name_period == 'all') {
                            //Consulta Sql
                            $consulta_general = $contenedor_sql;

                        } elseif ($programSelected != 'all' && $request->name_period != 'all' 
                            && isset($request->name_period) && !isset($request->name_period_until)) {
                            //Consulta Sql
                            $consulta_general = " (".$queryWhere.") 
                                                                    AND (grado_periodo = '$request->name_period')";

                        } elseif ($programSelected != 'all' && !isset($request->name_period) && 
                                    isset($request->name_period_until)) {
                           $consulta_general = " (".$queryWhere.")
                                                                   AND grado_periodo BETWEEN '$request->name_period_since'
                                                                   AND '$request->name_period_until'";

                        } elseif ($programSelected == 'all' && !isset($request->name_period) && 
                                    isset($request->name_period_until)) {
                           $consulta_general = " grado_periodo BETWEEN '$request->name_period_since'                                                     AND '$request->name_period_until'";

                        } elseif ($programSelected == 'all' && !isset($request->name_period) && isset($request->name_year_graduate)) {

                            $consulta_general = " grado_fecha BETWEEN date_sub(now(), 
                                                                    interval '$request->name_year_graduate' year)  
                                                                    AND NOW()";
                        
                        } elseif ($programSelected != 'all' && !isset($request->name_period) && isset($request->name_year_graduate)) {
                            
                            $consulta_general = " grado_fecha BETWEEN date_sub(now(), 
                                                                    interval '$request->name_year_graduate' year)  
                                                                    AND NOW() 
                                                                    AND (".$queryWhere.")";
                        }
                    } else {
                        
                        Alert::warning('<p>Asegurate de seleccionar un tiempo (Perido, tiempo de graduado)</p>')->html()->persistent("OK");
                        return redirect()->back();

                    }
                }

                //Si la accion direccionada desde el formulario Wizard es listar...
                if (!isset($action) || $action == 'list')
                {   
                    
                    $datos_list = Matriculado::queryFilters_list($consulta_general)->paginate(10);
                    //dd($datos_list);
                    //Se retorna a la vista con las variables necesarias para la carga de la tabla
                    return view('browse.egresados.reportes.table')
                            ->with('subareas', $subareas)
                            ->with('sistemas', $sistemas)
                            ->with('rangoSalarial', $rangoSalarial)
                            ->with('sectorEmpresa', $sectorEmpresa)
                            ->with('sistema', $sistema)
                            ->with('array', $array_content_filters)
                            ->with('datos_list', $datos_list)
                            ->with('program', $request->name_program)
                            ->with('period', $request->name_period)
                            ->with('period_since', $request->name_period_since)
                            ->with('period_until', $request->name_period_until)
                            ->with('period_year', $request->name_year_graduate)
                            ->with('select_situacion_laboral', $select_situacion_laboral)
                            ->with('select_duracion', $select_duracion)
                            ->with('select_cargo', $select_cargo)
                            ->with('select_contrato', $select_contrato)
                            ->with('select_sectorEmpresa', $select_sectorEmpresa)
                            ->with('select_rangoSalarial', $select_rangoSalarial)
                            ->with('select_relacionPrograma', $select_relacionPrograma)
                            ->with('select_tipoVinculacion', $select_tipoVinculacion)
                            ->with('select_ocupacionActual', $select_ocupacionActual)
                            ->with('select_areaPertenece', $select_areaPertenece)
                            ->with('select_tipoEmpresa', $select_tipoEmpresa)
                            ->with('select_sectorEmpresa_Bu', $select_sectorEmpresa_Bu)
                            ->with('select_Ciudad_Bu', $select_Ciudad_Bu)
                            ->with('select_tamaño_empresa', $select_tamaño_empresa)
                            ->with('select_merito', $select_merito);

                //Si la accion es exportar
                } elseif ($action == 'export_email' || $action == 'export_option') {

                    $data_list = Matriculado::queryFilters_download($consulta_general, $join, $colunm);
                    //Se crea un archivo excel egresados
                    //dd($data_list);
                    if ($data_list > 0 ) 
                    {
                     
                        Excel::create('Egresado', function ($excel) use ($data_list, $filters, $array)
                        {
                            //Creamos la hoja dentreo del excel
                            $excel->sheet('Datos', function ($sheet) use ($data_list, $filters, $array)
                            {   

                                //Insertamos en la primera fila los filtros aplicados
                                $sheet->row(1, ['Los filtros aplicados son: '.$filters]);
                                //Insertams en la segunda fila los las columnas que tendra el archivo
                                $sheet->row(2, $array);

                                /*$sheet->setColumnFormat(array(
                                  'P' => \PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY
                                ));*/

                                //Recoremos $datos_list
                                foreach ($data_list as $data) 
                                {

                                    $dateLaboral = false;
                                    $dateEmprendimiento = false;
                                    $dateLogros = false;

                                    $fecha = explode('-', $data->matriculado_fecha_nacimiento);
                                    
                                    //Insertamos las promiedades de $datos_list, en las columnas correspondientes del archivo excel
                                    $row = [];
                                    $row[0] = $data->identificacion_numero;
                                    $row[1] = $data->matriculado_primer_nombre;
                                    $row[2] = $data->matriculado_segundo_nombre;
                                    $row[3] = $data->matriculado_primer_apellido.' '.$data->matriculado_segundo_apellido;
                                    $row[4] = $data->matriculado_telefono;
                                    $row[5] = $data->matriculado_celular;
                                    $row[6] = $data->matriculado_email;
                                    $row[7] = $data->programa_nombre;
                                    $row[8] = $data->grado_periodo;
                                    $row[9] = $data->matriculado_genero;
                                    $row[10] = $data->estadocivil_nombre;

                                    if(count($fecha) > 1)
                                    {
                                        $row[11] = $fecha[2];
                                        $row[12] = $fecha[1];
                                        $row[13] = $fecha[0];  
                                    
                                    } else {

                                        $row[11] = "";
                                        $row[12] = "";
                                        $row[13] = ""; 

                                    } 

                                    $variable = 14;

                                    if (isset($_GET['select_situacion_laboral']))    
                                    {
                                        $row[$variable] = $data->situacionlaboral_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_duracion']))    
                                    {
                                        $row[$variable] = $data->duracionempresa_nombre;
                                        $dateLaboral = true;
                                        $variable++; 
                                    }

                                    if (isset($_GET['select_cargo']))    
                                    {
                                        $row[$variable] = $data->nivelcargo_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_contrato']))    
                                    {
                                        $row[$variable] = $data->contrato_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_sectorEmpresa']))    
                                    {
                                        $row[$variable] = $data->sectorempresa_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_rangoSalarial']))    
                                    {
                                        $row[$variable] = $data->rangosalario_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_relacionPrograma']))    
                                    {
                                        $row[$variable] = $data->relacionprograma_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_tipoVinculacion']))    
                                    {
                                        $row[$variable] = $data->vinculacion_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_ocupacionActual']))    
                                    {
                                        $row[$variable] = $data->ocupacion_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_areaPertenece']))    
                                    {
                                        $row[$variable] = $data->areapertenece_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_tipoEmpresa']))    
                                    {
                                        $row[$variable] = $data->tipoempresa_nombre;
                                        $dateLaboral = true;
                                        $variable++;
                                    }

                                    if ($dateLaboral) 
                                    {   
                                        
                                        $row[$variable] = $data->laboral_empresa;
                                        $variable++;
                                        $row[$variable] = $data->laboral_telefono;
                                        $variable++;
                                        
                                        $dateTime = explode(' ', $data->date_laboral);

                                        $fecha = $dateTime[0];
                                        
                                        $fechaEx = explode('-', $fecha);
                                        
                                        foreach ($fechaEx as $date) 
                                        {
                                            
                                            $row[$variable] = $date;
                                            $variable++;    

                                        }

                                    }

                                    if (isset($_GET['select_sectorEmpresa_Bu']))    
                                    {   
 
                                        $row[$variable] = $data->sectorempresa_nombre_Emprendimiento;
                                        $dateEmprendimiento = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_Ciudad_Bu']) && $_GET['select_Ciudad_Bu'][0] != null)    
                                    {
                                        $row[$variable] = $data->ciudad_nombre;
                                        $dateEmprendimiento = true;
                                        $variable++;
                                    }

                                    if (isset($_GET['select_tamaño_empresa']))    
                                    {
                                        $row[$variable] = $data->tamanoempresa_nombre;
                                        $dateEmprendimiento = true;
                                        $variable++;
                                    }

                                    if ($dateEmprendimiento) 
                                    {
                                        $dateTime = explode(' ', $data->date_emprendimiento);

                                        $fecha = $dateTime[0];
                                        
                                        $fechaEx = explode('-', $fecha);
                                        
                                        foreach ($fechaEx as $date) 
                                        {
                                            
                                            $row[$variable] = $date;
                                            $variable++;    

                                        }
                                    }

                                    if (isset($_GET['select_merito']))    
                                    {
                                        $row[$variable] = $data->logro_nombre;
                                        $dateLogros = true;
                                        $variable++;
                                    }

                                    

                                    if ($dateLogros) 
                                    {
                                        $dateTime = explode(' ', $data->date_logro);

                                        $fecha = $dateTime[0];
                                        
                                        $fechaEx = explode('-', $fecha);
                                        
                                        foreach ($fechaEx as $date) 
                                        {
                                            
                                            $row[$variable] = $date;
                                            $variable++;    

                                        }
                                    }

                                    $sheet->appendRow($row);    
                                }

                            });
                        //Se exporta en formato xls
                        })->export('xls');
                    
                    } elseif ($data_list == 'error') {
                        
                        Alert::error('<p>Se ha presentado un error</p>')->html()->persistent("OK");
                        return redirect()->back();

                    } else {

                        Alert::warning('<p>No se encontraron resultados</p>')->html()->persistent("OK");
                        return redirect()->back();

                    }

                } elseif ($action == 'fppDownload') {

                    $data_list = Matriculado::queryFilters_download($consulta_general, $join, $colunm);
                     //Se crea un archivo excel egresados
                    //dd($data_list);
                    if ($data_list > 0) {

                        Excel::create('Egresado', function ($excel) use ($data_list, $filters)
                        {
                            //Creamos la hoja dentreo del excel
                            $excel->sheet('Datos', function ($sheet) use ($data_list, $filters)
                            {   
                                //Insertamos en la segunda fila los las columnas que tendra el archivo
                                $sheet->row(1, [ 'CEDULA', 
                                                 'PRIMER NOMBRE',
                                                 'SEGUNDO NOMBRE', 
                                                 'APELLIDOS',  
                                                 'TELEFONO', 
                                                 'CELULAR', 
                                                 'CORREO', 
                                                 'PROGRAMA', 
                                                 'PERIODO',
                                                 'SEXO',
                                                 'ESTDO CIVIL',
                                                 'DIA',
                                                 'MES',
                                                 'AÑO'
                                                 ]);

                                //Recoremos $datos_list
                                foreach ($data_list as $data) 
                                {

                                    $fecha = explode('-', $data->matriculado_fecha_nacimiento);
                                    
                                    //Insertamos las promiedades de $datos_list, en las columnas correspondientes del archivo excel
                                    $row = [];
                                    $row[0] = $data->identificacion_numero;
                                    $row[1] = $data->matriculado_primer_nombre;
                                    $row[2] = $data->matriculado_segundo_nombre;
                                    $row[3] = $data->matriculado_primer_apellido.' '.$data->matriculado_segundo_apellido;
                                    $row[4] = $data->matriculado_telefono;
                                    $row[5] = $data->matriculado_celular;
                                    $row[6] = $data->matriculado_email;
                                    $row[7] = $data->programa_nombre;
                                    $row[8] = $data->grado_periodo;
                                    $row[9] = $data->matriculado_genero;
                                    $row[10] = $data->estadocivil_nombre;
                                    if(count($fecha) > 1)
                                    {
                                        $row[11] = $fecha[2];
                                        $row[12] = $fecha[1];
                                        $row[13] = $fecha[0];  
                                    }

                                    $sheet->appendRow($row);    
                                }

                            });
                        //Se exporta en formato xls
                        })->export('xls');

                    } elseif ($data_list == 'error') {
                        
                        Alert::error('<p>Se ha presentado un error</p>')->html()->persistent("OK");
                        return redirect()->back();

                    } else {

                        Alert::warning('<p>No se encontraron resultados</p>')->html()->persistent("OK");
                        return redirect()->back();

                    }
                
                } elseif ($action == 'lDownload') { //Laboral Download
                    
                   $function = $this->allLDownload($consulta_general);

                   if ($function == 'error') 
                   {
                        
                        Alert::error('<p>Se ha presentado un error</p>')->html()->persistent("OK");
                        return redirect()->back();

                   } else {

                        Alert::warning('<p>No se encontraron resultados</p>')->html()->persistent("OK");
                        return redirect()->back();

                   }

                } elseif ($action == 'eDownload') { //Emprendimiento Download
                    
                    $this->allEDownload($consulta_general);

                    $function = $this->allEDownload($consulta_general);

                   if ($function == 'error') 
                   {
                        
                        Alert::error('<p>Se ha presentado un error</p>')->html()->persistent("OK");
                        return redirect()->back();

                   } else {

                        Alert::warning('<p>No se encontraron resultados</p>')->html()->persistent("OK");
                        return redirect()->back();

                   }

                } elseif ($action == 'loDownload') { //Logros Download

                    $function = $this->allLoDownload($consulta_general);

                    if ($function == 'error') 
                    {
                        
                        Alert::error('<p>Se ha presentado un error</p>')->html()->persistent("OK");
                        return redirect()->back();

                    } else {

                        Alert::warning('<p>No se encontraron resultados</p>')->html()->persistent("OK");
                        return redirect()->back();

                    }
                }
    
    }

    // Funcion que nos genera la data laboral de los egresados
    public function allLDownload($query)
    {
        $data = Matriculado::queryAll_lDownload($query);
         
        if ($data != 'error' && $data->count() > 0) 
        {

            Excel::create('Egresado', function ($excel) use ($data)
            {
                //Creamos la hoja dentreo del excel
                $excel->sheet('Datos', function ($sheet) use ($data)
                {
                    //Insertamos en la segunda fila los las columnas que tendra el archivo
                    $sheet->row(1, [ 'CEDULA', 
                                     'PRIMER NOMBRE',
                                     'SEGUNDO NOMBRE', 
                                     'APELLIDOS',  
                                     'TELEFONO', 
                                     'CELULAR', 
                                     'CORREO', 
                                     'PROGRAMA', 
                                     'PERIODO',
                                     'SEXO',
                                     'ESTDO CIVIL',
                                     'DIA',
                                     'MES',
                                     'AÑO',
                                     'SITUACION LABORAL',
                                     'TIPO DE CONTRATO',
                                     'RELACION CON EL PROGRAMA',
                                     'AREA O DEPTO AL QUE PERTENECE',
                                     'DURACION EN SU ULTIMO EMPLEO',
                                     'SECTOR DE LA EMPRESA',
                                     'TIPO DE VINCULACION',
                                     'TIPO DE EMPRESA',
                                     'CARGO LABORAL',
                                     'RANGO DEVENGADO',
                                     'OCUPACION ACTUAL',
                                     'NOMBRE EMPRESA',
                                     'TELEFONO EMPRESA',
                                     'AÑO ACTUALIZACION INFO LABORAL', 
                                     'MES ACTUALIZACION INFO LABORAL',
                                     'DIA ACTUALIZACION INFO LABORAL'
                                     ]);

                    //Recoremos $datos_list
                    foreach ($data as $data) 
                    {
                        //dd($data);
                        $fecha = explode('-', $data->matriculado_fecha_nacimiento);
                        
                        //Insertamos las promiedades de $datos_list, en las columnas correspondientes del archivo excel
                        $row = [];
                        $row[0] = $data->identificacion_numero;
                        $row[1] = $data->matriculado_primer_nombre;
                        $row[2] = $data->matriculado_segundo_nombre;
                        $row[3] = $data->matriculado_primer_apellido.' '.$data->matriculado_segundo_apellido;
                        $row[4] = $data->matriculado_telefono;
                        $row[5] = $data->matriculado_celular;
                        $row[6] = $data->matriculado_email;
                        $row[7] = $data->programa_nombre;
                        $row[8] = $data->grado_periodo;
                        $row[9] = $data->matriculado_genero;
                        $row[10] = $data->estadocivil_nombre;

                        if(count($fecha) > 1)
                        {
                            $row[11] = $fecha[2];
                            $row[12] = $fecha[1];
                            $row[13] = $fecha[0];  
                        }

                        $row[14] = $data->situacionlaboral_nombre;
                        $row[15] = $data->contrato_nombre;
                        $row[16] = $data->relacionprograma_nombre;
                        $row[17] = $data->areapertenece_nombre;
                        $row[18] = $data->duracionempresa_nombre;
                        $row[19] = $data->sectorempresa_nombre;
                        $row[20] = $data->vinculacion_nombre;
                        $row[21] = $data->tipoempresa_nombre;
                        $row[22] = $data->nivelcargo_nombre;
                        $row[23] = $data->rangosalario_nombre;
                        $row[24] = $data->ocupacion_nombre;
                        $row[25] = $data->laboral_empresa;
                        $row[26] = $data->laboral_telefono;

                        $variable = 27;

                        $dateTime = explode(' ', $data->date_laboral);

                        $fecha = $dateTime[0];
                        
                        $fechaEx = explode('-', $fecha);
                        
                        foreach ($fechaEx as $date) 
                        {
                            
                            $row[$variable] = $date;
                            $variable++;    

                        }

                        $sheet->appendRow($row);    
                    }

                });
                //Se exporta en formato xls
            })->export('xls');

        } elseif ($data == 'error') {
                        
            return 'error';

        } else {

            return 'vacio';

        }
             
    }

    public function allEDownload($query)
    {
        $data = Matriculado::queryAll_eDownload($query);

        if ($data != 'error' && $data->count() > 0) 
        {

            Excel::create('Egresado', function ($excel) use ($data)
            {
                //Creamos la hoja dentreo del excel
                $excel->sheet('Datos', function ($sheet) use ($data)
                {
                    //Insertamos en la segunda fila los las columnas que tendra el archivo
                    $sheet->row(1, [ 'CEDULA', 
                                     'PRIMER NOMBRE',
                                     'SEGUNDO NOMBRE', 
                                     'APELLIDOS',  
                                     'TELEFONO', 
                                     'CELULAR', 
                                     'CORREO', 
                                     'PROGRAMA', 
                                     'PERIODO',
                                     'SEXO',
                                     'ESTDO CIVIL',
                                     'DIA',
                                     'MES',
                                     'AÑO',
                                     'SECTOR DE LA EMPRESA',
                                     'TAMAÑO DE LA EMPRESA',
                                     'CIUDAD EMPRESA',
                                     'AÑO ACTUALIZACION INFO EMPRENDIMIENTO', 
                                     'MES ACTUALIZACION INFO EMPRENDIMIENTO',
                                     'DIA ACTUALIZACION INFO EMPRENDIMIENTO'
                                     ]);

                    //Recoremos $datos_list
                    foreach ($data as $data) 
                    {

                        $fecha = explode('-', $data->matriculado_fecha_nacimiento);
                        
                        //Insertamos las promiedades de $datos_list, en las columnas correspondientes del archivo excel
                        $row = [];
                        $row[0] = $data->identificacion_numero;
                        $row[1] = $data->matriculado_primer_nombre;
                        $row[2] = $data->matriculado_segundo_nombre;
                        $row[3] = $data->matriculado_primer_apellido.' '.$data->matriculado_segundo_apellido;
                        $row[4] = $data->matriculado_telefono;
                        $row[5] = $data->matriculado_celular;
                        $row[6] = $data->matriculado_email;
                        $row[7] = $data->programa_nombre;
                        $row[8] = $data->grado_periodo;
                        $row[9] = $data->matriculado_genero;
                        $row[10] = $data->estadocivil_nombre;

                        if(count($fecha) > 1)
                        {
                            $row[11] = $fecha[2];
                            $row[12] = $fecha[1];
                            $row[13] = $fecha[0];  
                        }

                        $row[14] = $data->sectorempresa_nombre;
                        $row[15] = $data->tamanoempresa_nombre;
                        $row[16] = $data->ciudad_nombre;

                        $variable = 17;

                        $dateTime = explode(' ', $data->date_emprendimiento);

                        $fecha = $dateTime[0];
                        
                        $fechaEx = explode('-', $fecha);
                        
                        foreach ($fechaEx as $date) 
                        {
                            
                            $row[$variable] = $date;
                            $variable++;    

                        }

                        $sheet->appendRow($row);    
                    }

                });
                //Se exporta en formato xls
            })->export('xls');

        } elseif ($data == 'error') {
               
            return 'error';

        } else {

            return 'vacio';

        }

    }

    public function allLoDownload($query)
    {
        $data = Matriculado::queryAll_loDownload($query);
        
        if ($data != 'error' && $data->count() > 0) 
        {

            Excel::create('Egresado', function ($excel) use ($data)
            {
                //Creamos la hoja dentreo del excel
                $excel->sheet('Datos', function ($sheet) use ($data)
                {
                    //Insertamos en la segunda fila los las columnas que tendra el archivo
                    $sheet->row(1, [ 'CEDULA', 
                                     'PRIMER NOMBRE',
                                     'SEGUNDO NOMBRE', 
                                     'APELLIDOS',  
                                     'TELEFONO', 
                                     'CELULAR', 
                                     'CORREO', 
                                     'PROGRAMA', 
                                     'PERIODO',
                                     'SEXO',
                                     'ESTDO CIVIL',
                                     'DIA',
                                     'MES',
                                     'AÑO',
                                     'NOMBRE LOGRO',
                                     'AÑO ACTUALIZACION INFO EMPRENDIMIENTO', 
                                     'MES ACTUALIZACION INFO EMPRENDIMIENTO',
                                     'DIA ACTUALIZACION INFO EMPRENDIMIENTO'
                                     ]);

                    //Recoremos $datos_list
                    foreach ($data as $data) 
                    {

                        $fecha = explode('-', $data->matriculado_fecha_nacimiento);
                        
                        //Insertamos las promiedades de $datos_list, en las columnas correspondientes del archivo excel
                        $row = [];
                        $row[0] = $data->identificacion_numero;
                        $row[1] = $data->matriculado_primer_nombre;
                        $row[2] = $data->matriculado_segundo_nombre;
                        $row[3] = $data->matriculado_primer_apellido.' '.$data->matriculado_segundo_apellido;
                        $row[4] = $data->matriculado_telefono;
                        $row[5] = $data->matriculado_celular;
                        $row[6] = $data->matriculado_email;
                        $row[7] = $data->programa_nombre;
                        $row[8] = $data->grado_periodo;
                        $row[9] = $data->matriculado_genero;
                        $row[10] = $data->estadocivil_nombre;

                        if(count($fecha) > 1)
                        {
                            $row[11] = $fecha[2];
                            $row[12] = $fecha[1];
                            $row[13] = $fecha[0];  
                        }

                        $row[14] = $data->logro_nombre;

                        $variable = 15;

                        $dateTime = explode(' ', $data->date_logros);

                        $fecha = $dateTime[0];
                        
                        $fechaEx = explode('-', $fecha);
                        
                        foreach ($fechaEx as $date) 
                        {
                            
                            $row[$variable] = $date;
                            $variable++;    

                        }

                        $sheet->appendRow($row);    
                    }

                });
                //Se exporta en formato xls
            })->export('xls');

        } elseif ($data == 'error') {
               
            return 'error';

        } else {

            return 'vacio';

        }
    }

    //Muestra la vista para enviar correos
    public function index_send_email(Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        return view('browse.egresados.send')
                ->with('sistemas', $sistemas)
                ->with('sistema', $sistema);
    }

    //Enviar correo a egresados con link de la encuesta de actualizacion
    public function send_email(Request $request, Sistema $sistema)
    {

        $arrayProgram = array();
        $program = true;

        foreach ($request->name_program as $data) 
        {
            array_push($arrayProgram, $data);
        }

        $queryWhere = '';

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = 'programa_id = '.$arrayProgram[$i];

                }

            } else {

                $program = false;//No se mandaron porgramas, por lo contrario se seleccionaron todos

            }

        }

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $no_email = Grado::get_info_noEmail($request->name_year_graduate_send, $program, $queryWhere)->paginate(5);

        if (isset($_GET['paginate'])) {

            return view('browse.egresados.send')
                   ->with('name_year_graduate_send', $request->name_year_graduate_send)
                   ->with('name_program', $request->name_program)
                   ->with('no_email', $no_email)
                   ->with('sistema', $sistema)
                   ->with('sistemas', $sistemas);

        } else {

            //Mostramos el mensaje
            
            $metodo = '';
            $mensaje = '';

            $egresados = Grado::get_email($request->name_year_graduate_send, $program, $queryWhere);
            
            if (count($egresados) > 0) {
                    
                $mensaje = '¡Correos enviados satisfactoriamente!';
                $metodo = 'success';

            } else {

                $mensaje = '¡NO existen egresados correspondientes a '.$request->name_year_graduate_send.' año(s) de graduado!';
                $metodo = 'warning';

            }

            Alert::$metodo('<p>'.$mensaje.'</p>')->html()->persistent("OK");

            foreach ($egresados as $email) 
            {
               
                if ($email->matriculado->matriculado_email != null && filter_var($email->matriculado->matriculado_email, FILTER_VALIDATE_EMAIL))
                {

                    Mail::bcc($email->matriculado->matriculado_email)->send(new NotificacionEgresado($request->name_year_graduate_send));
                    
               } 

                set_time_limit(30);

            }

            return view('browse.egresados.send')
                   ->with('name_year_graduate_send', $request->name_year_graduate_send)
                   ->with('name_program', $request->name_program)
                   ->with('no_email', $no_email)
                   ->with('sistema', $sistema)
                   ->with('sistemas', $sistemas);

        }        

    }


    public function create_grado(Sistema $sistema)
    {

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        return view('browse.egresados.grade.grado')
                ->with('sistema', $sistema)
                ->with('sistemas', $sistemas);

    }

    public function uploadXls_grade(Request $request, Sistema $sistema)
    {
        //$egresados = Matriculado::orderBy('matriculado_pidm', 'ASC')->get();

        //Valdiamos si el archivo corresponde a los requerimientos,
        $this->validate($request, [
            'grados_file' => 'required|mimes:xls,xlsx,csv',
        ]);

        //Para saber que archivo se va cargar
        $archivo = $request->file('grados_file');

        //Obtenemos la ruta del archivo
        $ruta_archivo = $archivo->getRealPath();

        //Cargamos el archivo en la varaible $grados
        $grados = Excel::load($ruta_archivo, function ($reader) {
        })->get(); 

        //Variables para saber cuantos datos fueron actualizados o ingresados
        //$total_no_insertados = 0;
        $total_insertados    = 0;
        //Variable que funciona para identificar cuantas vueltas lleva el carge del archivo
        $contador            = 1;
        //Variable que funciona para que se rompa la carga cuando pase a false
        $dinamico = true;

        foreach ($grados as $grado) 
        {
            //Trae del modelo Identificacion el estudiante con el numero de documento
            $egresado_iden = Identificacion::where('identificacion_numero', $grado->identificacion_numero)->get();
            //Preguntamos si las promiedades identificacion_numero y pidm son munericas y diferentes a null
            if (isset($grado->identificacion_numero) &&
                $grado->identificacion_numero != null && 
                gettype($grado->identificacion_numero) == 'double') 
            {
                if (count($egresado_iden) > 0) 
                {

                    /*
                        Se realiza el llamado a la funcion validarTipoDato con todas las lalves foraneas existentes en el archivo de carge
                    */
                    //Llamamos a la funcion validarTipoDato quien nos recibe 4 parametros
                     $validar_nivelacademico_id = $this->validarTipoDato($grado->nivelacademico_id, 'sia_niveles_academicos', 'nivelacademico_id', 'sia');
                    $validar_snies =  $this->validarTipoDato($grado->snies, 'sia_programas', 'programa_id', 'sia');

                    //Arreglo que funciona para indicar la columna del archivo de carge y su correspondiente valor
                    $array_columnas_gra = array('NIVELACADEMICO_ID' => $validar_nivelacademico_id, 'SNIES' => $validar_snies);
                    
                    //Recoremos el arreglo
                    foreach ($array_columnas_gra as $columna => $value) 
                    {
                        //Si todas los posicioens del arreglo tienen como value un mensaje true...
                        if ($value == 'true') {
                            //Se guarda en las propiedades del modelo Matriculado, las propiedades del archivo de carge
                            $nuevo_grado = [
                                'matriculado_pidm' => $egresado_iden->first()->matriculado_pidm,
                                'grado_fecha' => $grado->grado_fecha,
                                'grado_periodo' => $grado->grado_periodo,
                                'grado_titulo' => $grado->titulo_obtenido,
                                'nivelacademico_id' => $grado->nivelacademico_id,
                                'programa_id' => $grado->snies,
                                'created_at' => $grado->fecha,
                                'updated_at' => date("Y-m-d G:i:s"),
                            ];

                        //Si alguna de las posiciones del arreglo tiene un mensaje diferente a true...
                        } else {

                            //La varaible dinamico pasa a ser false
                            $dinamico = false;
                            //Se crea el mensaje
                            Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Problemas en la Columna:</b> $columna</br><br><b><i>$value</i></b></br><b><br>Total registros insertados:</b> " . $total_insertados, '<h4><b>La carga se ha detenido</b></h4>')->html()->persistent("OK");

                        }

                    }
                    //Preguntamos si $dinamico es false, si es asi...
                    if ($dinamico == false) 
                    {
                        //Rompemos el carge y segido se muestra el mensaje
                        break;
                    }
                //Si el pidm y la propiedad $identificacion_numero no son correctas
                } else {

                    //Se crea el mensaje y posterior a esto se rompe la carga del archivo
                    
                    Alert::error("<b>Problemas en la fila:</b> $contador</br><br>No se ha encontrado al estudiante<b> " . 
                    $grado->primer_nombre 
                    ." ". 
                    $grado->segundo_nombre ." ".
                    $grado->primer_apellido ." ". 
                    $grado->segundo_apellido ."</b>", "<h4><b>La carga se ha detenido</b></h4>")->html()->persistent("OK");

                    break;

                }

                foreach ($egresado_iden as $data) 
                {
                    //$egresado tiene el resultado de la busqueda
                    $egresado = Matriculado::find($data->matriculado_pidm);
                    
                    //Autoincrementa
                    
                    $boolean = false;

                    foreach ($egresado->grados as $data) 
                    {

                        if ($data->programa_id == $grado->snies) 
                        {
                           
                            $boolean = true;

                        }

                    }

                    if ($boolean == false) 
                    {
                        ++$total_insertados;
                        DB::connection('egresados')->table('egresados_grados')->insert($nuevo_grado);
                    }
                }

            } else {

                Alert::error("<b>Problemas en la fila:</b> $contador</br><br><b>Verifica que el numero de documento este bien insertado</b></br><b><br>Total registros insertados: </b>" . $total_insertados, "<h4><b>La carga se ha detenido</b></h4>")->html()->persistent("OK");

                    break;

            }
            
            //Si $dinamico es diferente de true...
            if ($dinamico != false) 
            {
                
                //Se crea el mensaje donde indica que todos los datos fueron insertados y/o actulzados extosamente
                Alert::success('</h4><b>Total registros insertados: </b>' . $total_insertados, "<h4><b>Los registros han sido cargados</b>")->html()->persistent("OK");

                 return redirect()->back();

            }
            
            //Auto incrementa, para saber en que fila se encuentra el problema si lo llega haber
            ++$contador;
            set_time_limit(30);
        }

        return redirect()->back();
        
    }
}




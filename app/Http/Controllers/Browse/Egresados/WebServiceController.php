<?php

namespace App\Http\Controllers\Browse\Egresados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Egresados\Grado;
use App\Model\Egresados\Emprendimiento;
use App\Programa;
use App\Facultad;
use Illuminate\Support\Facades\DB;
use App\RangoSalarial;
use App\SectorEmpresa;
use App\Ciudad;
use App\Model\Egresados\Laboral;
use App\Model\Egresados\AreaPertenece;
use App\Model\Egresados\DuracionEmpresa;
use App\Model\Egresados\Ocupacion;
use App\Model\Egresados\RelacionPrograma;
use App\Model\Egresados\SituacionLaboral;
use App\Model\Egresados\Vinculacion;
use App\Model\Egresados\TamanoEmpresa;
use App\Model\Egresados\Logros;
use App\Cargo;
use App\Contrato;
use App\TipoEmpresa;
use App\Convenio;

//Clase que funciona para la carga asincronica de las graficas, y select de programa y periodo
class WebServiceController extends Controller
{
    
    public function loadFacultad()
    {
        return Facultad::orderBy('facultad_nombre', 'ASC')->get();
    }

    public function loadPeriodRange()
    {
        return Grado::select('grado_periodo')
                            ->from('egresados_grados')
                            ->orderBy('grado_periodo', 'ASC')
                            ->distinct()
                            ->get();    
    }

    //Funcion que sirve para cargar los programa
    public function loadProgram($facultad)
    {

     if ($facultad == 'all') 
        {
            return Grado::select('programa_nombre', 'programa_id', 'convenio_nombre')
                                ->join(env('DB_DATABASE').'.sia_convenio', 
                                       env('DB_DATABASE').'.sia_programas.convenio_id', 
                                       env('DB_DATABASE').'.sia_convenio.convenio_id')
                                ->from(env('DB_DATABASE').'.sia_programas')
                                ->orderBy('programa_nombre', 'ASC')
                                ->distinct()
                                ->get();
        } else { 

            return Grado::select('programa_nombre', 'programa_id', 'convenio_nombre')
                                ->join(env('DB_DATABASE').'.sia_convenio', 
                                       env('DB_DATABASE').'.sia_programas.convenio_id', 
                                       env('DB_DATABASE').'.sia_convenio.convenio_id')
                                ->from(env('DB_DATABASE').'.sia_programas')
                                ->where('facultad_id', $facultad)
                                ->orderBy('programa_nombre', 'ASC')
                                ->distinct()
                                ->get();
        }  
    }
    //Funcion que sirve para cargar los periodos correspondientes al periodo señalado
    public function loadPeriod($program)
    {
        /*
        if ($program == 'all') 
        {
            return Grado::select('grado_periodo')
                                ->from('egresados_grados')
                                ->orderBy('grado_periodo', 'ASC')
                                ->distinct()
                                ->get();
        } else { 

            return Grado::select('grado_periodo')
                                ->from('egresados_grados')
                                ->where('programa_id', $program)
                                ->orderBy('grado_periodo', 'ASC')
                                ->distinct()
                                ->get();
        }
        */
        $queryWhere = '';
        $queryAll = false;

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $queryAll = true;

            }

        }


        if ($queryAll) 
        {
            return Grado::select('grado_periodo')
                                ->from('egresados_grados')
                                ->orderBy('grado_periodo', 'ASC')
                                ->distinct()
                                ->get();
        } else { 

            return Grado::select('grado_periodo')
                                ->from('egresados_grados')
                                ->whereRaw('programa_id = '.$queryWhere)
                                ->orderBy('grado_periodo', 'ASC')
                                ->distinct()
                                ->get();
        }
    	
    }

    public function getConvenio($program)
    {
        return \DB::connection("sia")->table('sia_programas')
                         ->join('sia_convenio', 'sia_programas.convenio_id', '=', 'sia_convenio.convenio_id')
                         ->select('*')
                         ->where('programa_id', $program)->get();
    }

    public function loadCity($departament)
    {
        return \DB::connection("sia")->table('sia_ciudades')
                            ->select('*')
                            ->where('departamento_id', $departament)
                            ->orderBy('ciudad_nombre', 'ASC')->get();
    }

    public function nameProgram($program)
    {

        $queryWhere = '';
        $queryAll = false;

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $queryAll = true;

            }

        }


        if ($queryAll) 
        {
            $query = \DB::select('SELECT
                                    programa_nombre
                                FROM 
                                    '.env('DB_DATABASE').'.sia_programas');
            
        } else {

            $query = \DB::select('SELECT
                                    programa_nombre
                                FROM 
                                    '.env('DB_DATABASE').'.sia_programas
                                WHERE
                                    programa_id = '.$queryWhere);
        }

        $html = "";

        if ($queryAll) 
        {
        
            $html = "Todos los programas";

        } else {

            for ($i=0; $i < sizeof($query) ; $i++) 
            { 
                
                if ($i == 0) 
                {
                    
                    $html = $query[$i]->programa_nombre;
                
                } else {

                    $html .= " y ".$query[$i]->programa_nombre; 

                }

            }

        } 

        return $html;
    }

    //Cosulta para las graficas | Modulo Graficas

    public function getPaint_situacion($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = '  FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.situacionlaboral_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env('EGRESADOS_DATABASE').'.egresados_situacion_laboral 
                            ON egresados_laboral.situacionlaboral_id = egresados_situacion_laboral.situacionlaboral_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }
        
        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    situacionlaboral_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') 
                                GROUP BY
                                    situacionlaboral_nombre');

        } elseif ( $program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    situacionlaboral_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    situacionlaboral_nombre');
                    
        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    situacionlaboral_nombre');

        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                               '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    situacionlaboral_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    situacionlaboral_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    situacionlaboral_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year) 
                                AND NOW())
                                GROUP BY
                                    situacionlaboral_nombre');
        }
    }

    public function getPaint_relacion($program, $period, $since, $until, $year_graduate)
    {


        $queryBody = ' FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.relacionprograma_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env('EGRESADOS_DATABASE').'.egresados_relacion_programa 
                            ON egresados_laboral.relacionprograma_id = egresados_relacion_programa.relacionprograma_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                               '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.' AND grado_periodo = "'.$period.'")
                                GROUP BY
                                    relacionprograma_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    relacionprograma_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    relacionprograma_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    relacionprograma_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    relacionprograma_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    relacionprograma_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW() 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    programa_nombre,
                                    relacionprograma_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    relacionprograma_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)
                                AND NOW())
                                GROUP BY
                                    relacionprograma_nombre');
        }
    }
    /*
    *   EL = Egresado Laboral
    */
    public function getPaint_cargo($program, $period, $since, $until, $year_graduate)
    {
        $queryBody = '  FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.nivelcargo_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env('DB_DATABASE').'.sia_niveles_cargos 
                            ON egresados_laboral.nivelcargo_id = sia_niveles_cargos.nivelcargo_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.' AND grado_periodo = "'.$period.'")
                                GROUP BY,
                                    nivelcargo_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    nivelcargo_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    nivelcargo_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    nivelcargo_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    nivelcargo_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    nivelcargo_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    nivelcargo_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    nivelcargo_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    )grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    nivelcargo_nombre');
        }
        
    } 

    public function getPaint_duracion($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = '  FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.duracionempresa_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env("EGRESADOS_DATABASE").'.egresados_duracion_empresa 
                            ON egresados_laboral.duracionempresa_id = egresados_duracion_empresa.duracionempresa_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    duracionempresa_nombre,
                                    egresados_laboral.duracionempresa_id
                                ORDER BY    
                                    egresados_laboral.duracionempresa_id');
        }

    }

    public function getPaint_contrato($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = 'FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.contrato_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env("DB_DATABASE").'.sia_contratos 
                            ON egresados_laboral.contrato_id = sia_contratos.contrato_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    contrato_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                 '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    contrato_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    contrato_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    contrato_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    contrato_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    contrato_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    contrato_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    contrato_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    contrato_nombre');
        }

    }

    public function getPaint_salario($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = 'FROM '.
                             env('EGRESADOS_DATABASE').'.egresados_grados EG
                        INNER JOIN (
                            SELECT
                                EL.matriculado_pidm,
                                EL.rangosalario_id
                            FROM '
                                .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                            INNER JOIN (
                                SELECT
                                    MAX(EL2.updated_at) AS updated_at1,
                                    EL2.matriculado_pidm
                                FROM '
                                    .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                GROUP BY
                                    EL2.matriculado_pidm
                            ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                            AND RU.updated_at1 = EL.updated_at
                        ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                        INNER JOIN '.env("DB_DATABASE").'.sia_rangos_salarios 
                            ON egresados_laboral.rangosalario_id = sia_rangos_salarios.rangosalario_id
                        INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                            ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$program.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$program.')
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$program.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$program.')
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    rangosalario_nombre,
                                    egresados_laboral.rangosalario_id,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    rangosalario_nombre
                                ORDER BY 
                                    egresados_laboral.rangosalario_id');
        }

    }

    public function getPaint_ocupacion($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = ' FROM '.
                                     env('EGRESADOS_DATABASE').'.egresados_grados EG
                                INNER JOIN (
                                    SELECT
                                        EL.matriculado_pidm,
                                        EL.ocupacion_id
                                    FROM '
                                        .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                                    INNER JOIN (
                                        SELECT
                                            MAX(EL2.updated_at) AS updated_at1,
                                            EL2.matriculado_pidm
                                        FROM '
                                            .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                        GROUP BY
                                            EL2.matriculado_pidm
                                    ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                                    AND RU.updated_at1 = EL.updated_at
                                ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                                INNER JOIN '.env("EGRESADOS_DATABASE").'.egresados_ocupacion 
                                    ON egresados_laboral.ocupacion_id = egresados_ocupacion.ocupacion_id
                                INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                                    ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    ocupacion_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    ocupacion_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    ocupacion_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    ocupacion_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    ocupacion_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    ocupacion_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    ocupacion_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    ocupacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    ocupacion_nombre');
        }

    }

    public function getPaint_sector($program, $period, $since, $until, $year_graduate)
    {   
        $queryBody = 'FROM '.
                                     env('EGRESADOS_DATABASE').'.egresados_grados EG
                                INNER JOIN (
                                    SELECT
                                        EL.matriculado_pidm,
                                        EL.sectorempresa_id
                                    FROM '
                                        .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                                    INNER JOIN (
                                        SELECT
                                            MAX(EL2.updated_at) AS updated_at1,
                                            EL2.matriculado_pidm
                                        FROM '
                                            .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                        GROUP BY
                                            EL2.matriculado_pidm
                                    ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                                    AND RU.updated_at1 = EL.updated_at
                                ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                                INNER JOIN '.env("DB_DATABASE").'.sia_sectores_empresas 
                                    ON egresados_laboral.sectorempresa_id = sia_sectores_empresas.sectorempresa_id
                                INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                                    ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    sectorempresa_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    sectorempresa_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    sectorempresa_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    sectorempresa_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    sectorempresa_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    sectorempresa_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND EG.programa_id = '.$queryWhere.'
                                GROUP BY
                                    sectorempresa_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    sectorempresa_nombre,
                                    COUNT(*) AS total
                               '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    sectorempresa_nombre');
        }

    }

    public function getPaint_vinculacion($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = 'FROM '.
                                     env('EGRESADOS_DATABASE').'.egresados_grados EG
                                INNER JOIN (
                                    SELECT
                                        EL.matriculado_pidm,
                                        EL.vinculacion_id
                                    FROM '
                                        .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                                    INNER JOIN (
                                        SELECT
                                            MAX(EL2.updated_at) AS updated_at1,
                                            EL2.matriculado_pidm
                                        FROM '
                                            .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                        GROUP BY
                                            EL2.matriculado_pidm
                                    ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                                    AND RU.updated_at1 = EL.updated_at
                                ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                                INNER JOIN '.env("EGRESADOS_DATABASE").'.egresados_vinculacion 
                                    ON egresados_laboral.vinculacion_id = egresados_vinculacion.vinculacion_id
                                INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                                    ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    vinculacion_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    vinculacion_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    vinculacion_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    vinculacion_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    vinculacion_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    vinculacion_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    vinculacion_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    vinculacion_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    vinculacion_nombre');
        }

    }

    public function getPaint_area($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = 'FROM '.
                                     env('EGRESADOS_DATABASE').'.egresados_grados EG
                                INNER JOIN (
                                    SELECT
                                        EL.matriculado_pidm,
                                        EL.areapertenece_id
                                    FROM '
                                        .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                                    INNER JOIN (
                                        SELECT
                                            MAX(EL2.updated_at) AS updated_at1,
                                            EL2.matriculado_pidm
                                        FROM '
                                            .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                        GROUP BY
                                            EL2.matriculado_pidm
                                    ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                                    AND RU.updated_at1 = EL.updated_at
                                ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                                INNER JOIN '.env("EGRESADOS_DATABASE").'.egresados_area_pertenece 
                                    ON egresados_laboral.areapertenece_id = egresados_area_pertenece.areapertenece_id
                                INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                                    ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    areapertenece_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    areapertenece_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    areapertenece_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    areapertenece_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    areapertenece_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    areapertenece_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    areapertenece_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    areapertenece_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    areapertenece_nombre');
        }

        /*if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "grado_periodo", "areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "grado_periodo", "areapertenece_nombre")
                    ->where([['egresados_grados.programa_id', $program], ['egresados_grados.grado_periodo', $period]])->get();

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "areapertenece_nombre")
                    ->where('egresados_grados.programa_id', $program)->get();

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("grado_periodo", "areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("grado_periodo", "areapertenece_nombre")
                    ->where('egresados_grados.grado_periodo', $period)->get();


        } elseif($program == 'all' && $period == 'all') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("areapertenece_nombre")->get();

         
        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("areapertenece_nombre")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

         
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "areapertenece_nombre")
                    ->where('egresados_grados.programa_id', $program)
                    ->whereBetween("grado_periodo", [$since, $until])->get();
         
        } elseif($program != 'all' && $period == 1 && $year_graduate != 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "areapertenece_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW() 
                                        AND egresados_grados.programa_id = '$program'")->get();

         
        } elseif($program == 'all' && $period == 1 && $year_graduate != 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join('egresados_area_pertenece', 'egresados_laboral.areapertenece_id', '=',                                                             'egresados_area_pertenece.areapertenece_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("areapertenece_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("areapertenece_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()")->get();

         
        }*/

    }

    public function getPaint_tipoEmpresa($program, $period, $since, $until, $year_graduate)
    {

        $queryBody = 'FROM '.
                                     env('EGRESADOS_DATABASE').'.egresados_grados EG
                                INNER JOIN (
                                    SELECT
                                        EL.matriculado_pidm,
                                        EL.tipoempresa_id
                                    FROM '
                                        .env('EGRESADOS_DATABASE').'.egresados_laboral EL
                                    INNER JOIN (
                                        SELECT
                                            MAX(EL2.updated_at) AS updated_at1,
                                            EL2.matriculado_pidm
                                        FROM '
                                            .env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                        GROUP BY
                                            EL2.matriculado_pidm
                                    ) RU ON RU.matriculado_pidm = EL.matriculado_pidm
                                    AND RU.updated_at1 = EL.updated_at
                                ) AS egresados_laboral ON EG.matriculado_pidm = egresados_laboral.matriculado_pidm
                                INNER JOIN '.env("DB_DATABASE").'.sia_tipos_empresas 
                                    ON egresados_laboral.tipoempresa_id = sia_tipos_empresas.tipoempresa_id
                                INNER JOIN '.env('DB_DATABASE').'.sia_programas 
                                    ON EG.programa_id = sia_programas.programa_id';

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR EG.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = $arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    programa_nombre,
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo = "'.$period.'")
                                GROUP BY
                                    tipoempresa_nombre');

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    tipoempresa_nombre');

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::select('SELECT
                                    grado_periodo,
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo = "'.$period.'")
                                GROUP BY
                                    tipoempresa_nombre');

        } elseif($program == 'all' && $period == 'all') {

            return \DB::select('SELECT
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                GROUP BY
                                    tipoempresa_nombre');

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    tipoempresa_nombre');
     
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (EG.programa_id = '.$queryWhere.') AND (grado_periodo BETWEEN "'.$since.'" AND "'.$until.'")
                                GROUP BY
                                    tipoempresa_nombre');
                    
        } elseif ($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::select('SELECT
                                    programa_nombre,
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW()) 
                                AND (EG.programa_id = '.$queryWhere.')
                                GROUP BY
                                    tipoempresa_nombre');

        } elseif ($program == 'all' && $period == 1 && $year_graduate != 'null') {

                        return \DB::select('SELECT
                                    tipoempresa_nombre,
                                    COUNT(*) AS total
                                '.$queryBody.'
                                WHERE
                                    (grado_fecha BETWEEN date_sub(now(), interval '.$year_graduate.' year)  
                                AND NOW())
                                GROUP BY
                                    tipoempresa_nombre');
        }

        /*if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "grado_periodo", "tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "grado_periodo", "tipoempresa_nombre")
                    ->where([['egresados_grados.programa_id', $program], ['egresados_grados.grado_periodo', $period]])->get();

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tipoempresa_nombre")
                    ->where('egresados_grados.programa_id', $program)->get();

        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("grado_periodo", "tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("grado_periodo", "tipoempresa_nombre")
                    ->where('egresados_grados.grado_periodo', $period)->get();

        } elseif($program == 'all' && $period == 'all') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tipoempresa_nombre")->get();

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tipoempresa_nombre")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tipoempresa_nombre")
                    ->where('egresados_grados.programa_id', $program)
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate != 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tipoempresa_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW() 
                                        AND egresados_grados.programa_id = '$program'")->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate != 'null') {

          return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_laboral', 'egresados_grados.matriculado_pidm', '=', 'egresados_laboral.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_tipos_empresas', 'egresados_laboral.tipoempresa_id', '=',                                                                   'sia_tipos_empresas.tipoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tipoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tipoempresa_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()")->get();

        }*/

    }

    public function getPaint_business_city($program, $period, $since, $until, $year_graduate)
    {

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR egresados_grados.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = 'egresados_grados.programa_id = '.$arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "grado_periodo", "ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                    ->whereRaw("(".$queryWhere.") AND (egresados_grados.grado_periodo = '".$period."')")->get();

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select( "programa_nombre", "ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                    ->whereRaw("(".$queryWhere.")")->get();


        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("grado_periodo", "ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                    ->where('egresados_grados.grado_periodo', $period)->get();

        } elseif($program == 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados') 
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")->get();

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados') 
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados') 
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "ciudad_nombre")
                    ->whereRaw("(".$queryWhere.")")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate != 'null' ) {

            return \DB::connection("egresados")->table('egresados_grados') 
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                    ->whereRaw("(grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()) 
                                        AND (".$queryWhere.")")->get();

        } elseif($program == 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::connection("egresados")->table('egresados_grados') 
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                            'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_ciudades', 'egresados_emprendimiento.ciudad_id', '=', 'sia_ciudades.ciudad_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("ciudad_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("ciudad_nombre")
                  ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()")->get();
        }
    }

    public function getPaint_business_size($program, $period, $since, $until, $year_graduate)
    {

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR egresados_grados.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = 'egresados_grados.programa_id = '.$arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "grado_periodo", "tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "grado_periodo", "tamanoempresa_nombre")
                    ->whereRaw("(".$queryWhere.") AND (egresados_grados.grado_periodo = '".$period."')")->get();

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tamanoempresa_nombre")
                     ->whereRaw("(".$queryWhere.")")->get();


        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("grado_periodo", "tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("grado_periodo", "tamanoempresa_nombre")
                    ->where('egresados_grados.grado_periodo', $period)->get();

        } elseif($program == 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tamanoempresa_nombre")->get();

        } elseif($program == 'all' && $period == 1  && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tamanoempresa_nombre")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1  && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tamanoempresa_nombre")
                    ->whereRaw("(".$queryWhere.")")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1  && $year_graduate != 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "tamanoempresa_nombre")
                    ->whereRaw("(grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()) 
                                        AND (".$queryWhere.")")->get();

        } elseif($program == 'all' && $period == 1  && $year_graduate != 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('EGRESADOS_DATABASE').'.egresados_tamano_empresa', 'egresados_emprendimiento.tamanoempresa_id', '=',                                                         'egresados_tamano_empresa.tamanoempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("tamanoempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("tamanoempresa_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()")->get();

        } 
    }

    public function getPaint_business_sector($program, $period, $since, $until, $year_graduate)
    {

        $queryWhere = '';

        $arrayProgram = explode(',', $program);

        for ($i=0; $i < sizeof($arrayProgram); $i++) 
        { 
            if ($arrayProgram[$i] != 'all') 
            {
            
                if ($i > 0) {

                    $queryWhere .= ' OR egresados_grados.programa_id = '.$arrayProgram[$i];

                } else {

                    $queryWhere = 'egresados_grados.programa_id = '.$arrayProgram[$i];

                }

            } else {

                $program = 'all';

            }

        }

        if ($program != 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "grado_periodo", "sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "grado_periodo", "sectorempresa_nombre")
                    ->whereRaw("(".$queryWhere.") AND (egresados_grados.grado_periodo = '".$period."')")->get();

        } elseif ($program != 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "sectorempresa_nombre")
                    ->whereRaw("(".$queryWhere.")")->get();


        } elseif ($program == 'all' && $period != 'all' && $period != 1) {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("grado_periodo", "sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("grado_periodo", "sectorempresa_nombre")
                    ->where('egresados_grados.grado_periodo', $period)->get();

        } elseif($program == 'all' && $period == 'all') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("sectorempresa_nombre", "egresados_emprendimiento.sectorempresa_id")->get();

        } elseif($program == 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("sectorempresa_nombre")
                    ->whereBetween("grado_periodo", [$since, $until])->get();
        
        } elseif($program != 'all' && $period == 1 && $year_graduate == 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "sectorempresa_nombre")
                    ->whereRaw("(".$queryWhere.")")
                    ->whereBetween("grado_periodo", [$since, $until])->get();

        } elseif($program != 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("programa_nombre", "sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("programa_nombre", "sectorempresa_nombre")
                    ->whereRaw("(grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()) 
                                        AND (".$queryWhere.")")->get();

        } elseif($program == 'all' && $period == 1 && $year_graduate != 'null') {

            return \DB::connection("egresados")->table('egresados_grados')
                    ->join('egresados_emprendimiento', 'egresados_grados.matriculado_pidm', '=',                                                         'egresados_emprendimiento.matriculado_pidm')
                    ->join(env('DB_DATABASE').'.sia_sectores_empresas', 'egresados_emprendimiento.sectorempresa_id', '=',                                                         'sia_sectores_empresas.sectorempresa_id')
                    ->join(env('DB_DATABASE').'.sia_programas', 'egresados_grados.programa_id', '=', 'sia_programas.programa_id')
                    ->select("sectorempresa_nombre", \DB::raw("COUNT(*) as total") )
                    ->groupBy("sectorempresa_nombre")
                    ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$year_graduate' year)  
                                        AND NOW()")->get();
        }
    }


    //Buscar nombre del filtro | Modulo Wizard

    public function name_laboral()
    {   
        $datos = json_decode( \Input::get("datos") );
        return SituacionLaboral::whereIn('situacionlaboral_id', $datos)->pluck("situacionlaboral_nombre")->toJson();            
    }

    public function name_duracion()
    {   
        $datos = json_decode( \Input::get("datos") );
        return DuracionEmpresa::whereIn('duracionempresa_id', $datos)->pluck("duracionempresa_nombre")->toJson();            
    }

    public function name_cargo()
    {   
        $datos = json_decode( \Input::get("datos") );
        return Cargo::whereIn('nivelcargo_id', $datos)->pluck("nivelcargo_nombre")->toJson();            
    }

    public function name_contrato()
    {   
        $datos = json_decode( \Input::get("datos") );
        return Contrato::whereIn('contrato_id', $datos)->pluck("contrato_nombre")->toJson();            
    }

    public function name_sector()
    {
        $datos = json_decode( \Input::get("datos") );
        return SectorEmpresa::whereIn('sectorempresa_id', $datos)->pluck("sectorempresa_nombre")->toJson();
    }

    public function name_salario()
    {
        $datos = json_decode( \Input::get("datos") );
        return RangoSalarial::whereIn('rangosalario_id', $datos)->pluck("rangosalario_nombre")->toJson();
    }

    public function name_relacion()
    {
        $datos = json_decode( \Input::get("datos") );
        return RelacionPrograma::whereIn('relacionprograma_id', $datos)->pluck("relacionprograma_nombre")->toJson();
    }

    public function name_vinculacion()
    {
        $datos = json_decode( \Input::get("datos") );
        return Vinculacion::whereIn('vinculacion_id', $datos)->pluck("vinculacion_nombre")->toJson();
    }

    public function name_ocupacion()
    {
        $datos = json_decode( \Input::get("datos") );
        return Ocupacion::whereIn('ocupacion_id', $datos)->pluck("ocupacion_nombre")->toJson();
    }

    public function name_area()
    {
        $datos = json_decode( \Input::get("datos") );
        return AreaPertenece::whereIn('areapertenece_id', $datos)->pluck("areapertenece_nombre")->toJson();
    }

    public function name_tipoEmpresa()
    {
        $datos = json_decode( \Input::get("datos") );
        return TipoEmpresa::whereIn('tipoempresa_id', $datos)->pluck("tipoempresa_nombre")->toJson();
    }

    public function name_ciudad()
    {
        $datos = json_decode( \Input::get("datos") );
        return Ciudad::whereIn('ciudad_id', $datos)->pluck("ciudad_nombre")->toJson();
    }

    public function name_tamanoEmpresa()
    {
        $datos = json_decode( \Input::get("datos") );
        return TamanoEmpresa::whereIn('tamanoempresa_id', $datos)->pluck("tamanoempresa_nombre")->toJson();
    }

    public function name_merito()
    {
        $datos = json_decode( \Input::get("datos") );
        return Logros::whereIn('logro_id', $datos)->pluck("logro_nombre")->toJson();
    }
}

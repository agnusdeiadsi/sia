<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Alert;

// modelos
use App\Sistema;
use App\Entrance;
use App\Area;
use App\Usuario;
use App\Rol;

use App\Model\Correos\Cuenta;

class BrowseController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Show all systems.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sistemas = Sistema::Buscar($request->buscar)
        ->orderBy('sistema_entrances', 'DESC')
        ->orderBy('sistema_nombre', 'ASC')
        ->paginate(9);

        $usuario = Usuario::find(\Auth::user()->id);
        $usuario->rol_modulo = null;
        $usuario->save();

        //guarda los registros del usuario
        $usuario->entrances()->create([
                                      'sistema_id' => null, 
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Home SIA'),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return view('browse.home')->with('sistemas', $sistemas);
    }

    public function menu($sistema)
    {
      
      $sistema = Sistema::find($sistema);
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      return view('browse.'.$sistema->sistema_nombre_corto.'.menu')
      ->with('sistema', $sistema)
      ->with('sistemas', $sistemas);
    }

    /*
    //load certificados module
    public function certificados($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.certificados.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        $sistema->sistema_entrances += 1;
        $sistema->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado.'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.certificados.menu', $sistema);
        }
      }
    }

    public function compras(Sistema $sistema)
    {
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.compras.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $sistema->sistema_id)->pluck('rol_id')->all();

        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado.'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');      

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
          return redirect()->route('browse.compras.menu', $sistema);
        }
      }
    }

    public function erc($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.erc.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado.'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(');      

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.erc.menu', $sistema);
        }
      }
    }

    public function correos($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.correos.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(');  

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.correos.menu', $sistema);
        }
      }
    }

    public function directorio($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.directorio.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');    

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.directorio.menu', $sistema);
        }
      }
    }

    public function contratos($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');      

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
        }
      }
    }

    public function egresados($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');      

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
        }
      }
    }

    public function cancelaciones($id)
    {
      $sistema = Sistema::find($id);
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      if(\Auth::user()->rol_sia == 'Administrador') //si el usuario autenticado es Administrador SIA
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
      }
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $id)->pluck('rol_id')->all();
        $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

        $permiso = Usuario::find(\Auth::user()->id);
        $permiso->rol_modulo = $role_name[0];
        $permiso->save();

        if($permiso->rol_modulo == "Ninguno" || $permiso == null)
        {
          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          flash()->overlay('¡Lo sentimos!<br>Ud no tiene acceso al servicio <b>'.$sistema->sistema_nombre.'</b>.', 'Oops! :(', 'danger');      

          return redirect()->route('browse.home');
        }
        else
        {
          //suma la entrada al sistema
          $sistema->sistema_entrances += 1;
          $sistema->save();

          //guarda los registros del usuario
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);

          return redirect()->route('browse.'.$sistema->sistema_nombre_corto.'.menu', $sistema);
        }
      }
    }*/

    //Load module
    public function modulo(Sistema $sistema)
    {
      $sistema_rol = $sistema->roles;
      $sistema_subarea = $sistema->subarea;

      //si el usuario autenticado es Administrador SIA
      if(\Auth::user()->rol_sia == 'Administrador')
      {
        //suma la entrada al sistema
        $sistema->sistema_entrances += 1;
        $sistema->save();

        //guarda los registros del usuario
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        return redirect()->route('browse.'.strtolower($sistema->sistema_nombre_corto).'.menu', $sistema);
      }
      //si el usuario autenticado no es adminsitrador SIA
      else
      {
        $permisos = Usuario::find(\Auth::user()->id)->permisos->where('sistema_id', '=', $sistema->sistema_id)->pluck('rol_id')->all();

        if(!empty($permisos))
        {
          $role_name = Rol::find($permisos)->pluck('rol_nombre')->all();

          $permiso = Usuario::find(\Auth::user()->id);
          $permiso->rol_modulo = $role_name[0];
          $permiso->save();
        
          $sistema->sistema_entrances += 1;
          $sistema->save();

          if($permiso->rol_modulo == "Ninguno" || $permiso == null)
          {
            //guarda los registros del usuario
            $sistema->entrances()->create([
                                          'usuario_id' => \Auth::user()->id,
                                          'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                          'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                          'entrance_url_anterior' => url()->previous(),
                                          'entrance_url_actual' => url()->current(),
                                          'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre.' denegado.'),  
                                          'entrance_startdate' => date('Y-m-d H:i:s'),
                                        ]);
            Alert::error('<p>No tienes permisos en el módulo <b>'.$sistema->sistema_nombre.'</b>.</p>', 'Acceso denegado')->html()->persistent();

            return redirect()->route('browse.home');
          }
        
          else
          {
            //suma la entrada al sistema
            $sistema->sistema_entrances += 1;
            $sistema->save();

            //guarda los registros del usuario
            $sistema->entrances()->create([
                                          'usuario_id' => \Auth::user()->id,
                                          'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                          'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                          'entrance_url_anterior' => url()->previous(),
                                          'entrance_url_actual' => url()->current(),
                                          'entrance_accion' => nl2br('Ingreso al sistema id '.$sistema->sistema_id.' - '.$sistema->sistema_nombre),  
                                          'entrance_startdate' => date('Y-m-d H:i:s'),
                                        ]);

            return redirect()->route('browse.'.strtolower($sistema->sistema_nombre_corto).'.menu', $sistema);
          }
        }
        else
        {
          Alert::warning('<p>No tienes permisos en el módulo <b>'.$sistema->sistema_nombre.'</b>.</p>', 'Acceso denegado')->html()->persistent();

          return redirect()->back();
        }
      }
    }
}

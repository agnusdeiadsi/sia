<?php

namespace App\Http\Controllers\Browse\erc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Barryvdh\DomPDF\Facade;

use App\CentroOperacion;
use App\Sistema;
use App\Matriculado;
use App\Model\Banner\BMatriculado;

use App\Model\Erc\Pago;
use App\Model\Erc\Recibo;
use App\Model\Erc\Concepto;

class RecibosController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
        $pagos = Pago::orderBy('pago_nombre', 'ASC')->pluck('pago_nombre', 'pago_id');
        $recibos = Recibo::join('app_sia.sia_centros_operacion', 'erc_recibos.centrooperacion_id', '=', 'app_sia.sia_centros_operacion.centrooperacion_id')
        ->join('app_sia.sia_usuarios', 'erc_recibos.recibo_created_usuario', '=', 'app_sia.sia_usuarios.id')
        ->join('erc_estados', 'erc_recibos.estado_id', '=', 'erc_estados.estado_id')
        ->orderBy('recibo_created_at', 'ASC')
        ->Buscar($request->busqueda)
        ->paginate(25);

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista Historial recibos'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.erc.recibos.historial')
        ->with('centros_operacion', $centros_operacion)
        ->with('pagos', $pagos)
        ->with('recibos', $recibos)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Sistema $sistema)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      
      $pagos = Pago::orderBy('pago_nombre', 'ASC')->get();

      /*$matriculado = Matriculado::Search($request->codigo)
      ->orderBy('matriculado_periodo','DESC')
      ->first();*/
      $matriculado = BMatriculado::where('pers_num_doc', '=', $request->codigo)
      ->orWhere('pers_id', '=', $request->codigo)
      //->orderBy('matriculado_periodo','DESC')
      ->first();
      //dd($matriculado->matriculas->where('bmtr_periodo', '=', $matriculado->matriculas->max('bmtr_periodo'))->first());
      if(empty($matriculado))
      {
        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Crear nuevo recibo'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.erc.recibos.new')
        ->with('centros_operacion', $centros_operacion)
        ->with('pagos', $pagos)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
      }
      else
      {
        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Crear nuevo recibo usuario existente'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.erc.recibos.create')
        ->with('centros_operacion', $centros_operacion)
        ->with('pagos', $pagos)
        ->with('matriculado', $matriculado)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
      $this->validate($request, [
          'app_erc.erc_recibos.recibo_codigo_liquidacion' => 'unique:app_erc.erc_recibos',
      ]);

      $consecutivo = Recibo::where('app_erc.erc_recibos.centrooperacion_id', '=', $request->centrooperacion_id)->max('recibo_consecutivo_centro');

      if($consecutivo == null)
      {
        $consecutivo = 1;
      }else{
        $consecutivo = $consecutivo+1;
      }

      $recibo = new Recibo($request->all());

      $recibo->recibo_consecutivo_centro = $consecutivo;
      $recibo->recibo_created_usuario = \Auth::user()->id;
      $recibo->recibo_created_at = date('Y-m-d H:i:s');
      $recibo->estado_id = 1; //estado predeterminado: No contabilizado.
      $recibo->save();

      $pagos_id = array();
      $i = 0;

      if(!empty($request->pago_id))
      {
        foreach ($request->pago_id as $pagos) {
            $pagos_id[] = [
                        'pago_id' => $pagos,
                        'recibopago_valor' => $request->recibopago_valor[$i],
                        'recibopago_entidad_bancaria' => $request->recibopago_entidad_bancaria[$i],
                        'recibopago_codigo' => $request->recibopago_codigo[$i],
                        'recibopago_plaza' => $request->recibopago_plaza[$i]
                      ];
            $i++;
        }
      }

      //inserta registros en la tabla pivote erc_recibo_pagos
      $recibo->pagos()->attach($pagos_id);

      //inserta registros en la tabla erc_recibo_concepto
      $concepto = new Concepto($request->all());
      $concepto->recibo()->associate($recibo);
      $concepto->save();

      $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Recibo N° '.$recibo->recibo_id.' Centro de Operación '.$recibo->centroOperacion->centrooperacion_codigo.' guardado exitosamente'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

      //mensaje de alerta
      flash()->overlay('El recibo ha sido generado correctamente <b>N° '.$recibo->recibo_consecutivo_centro.'</b> Centro de operación <b>'.$recibo->centrooperacion_id.'</b>.', 'Notificación');

    	return redirect()->route('erc.recibos.create', $sistema);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Sistema $sistema, Recibo $recibo)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $recibo_centro = $recibo->centroOperacion;
      $recibo_concepto = $recibo->concepto;
      $recibo_pago = $recibo->pagos;

      //$sistema = Sistema::find($sistema);
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $pagos = Pago::orderBy('pago_nombre', 'ASC')->get();

      $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Ver recibo N° '.$recibo->recibo_id.' Centro de Operación '.$recibo->centroOperacion->centrooperacion_codigo),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
      
      return view('browse.erc.recibos.show')
      ->with('centros_operacion', $centros_operacion)
      ->with('pagos', $pagos)
      ->with('sistema', $sistema)
      ->with('recibo', $recibo)
      ->with('filtro', $request->filtro)
      ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sistema $sistema, Recibo $recibo)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
      //$recibo = Recibo::find($recibo);
      $recibo_centro = $recibo->centroOperacion;
      $recibo_concepto = $recibo->concepto;
      $recibo_pago = $recibo->pagos;
      $recibo_usuario = $recibo->usuario;
      $recibo_usuario_update = $recibo->usuarioUpdate;

      //$sistema = Sistema::find($sistema);
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $pagos = Pago::orderBy('pago_nombre', 'ASC')->get();

      $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Editar recibo N° '.$recibo->recibo_id.' Centro de Operación '.$recibo->centroOperacion->centrooperacion_codigo),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

      return view('browse.erc.recibos.edit')
      ->with('centros_operacion', $centros_operacion)
      ->with('pagos', $pagos)
      ->with('sistema', $sistema)
      ->with('recibo', $recibo)
      ->with('sistemas', $sistemas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema, Recibo $recibo)
    {
      $recibo_concepto = $recibo->concepto;
      $recibo_centro = $recibo->centroOperacion;

      $recibo->fill($request->all()); //la funcion fill recibe por parametro los campos datos personales del recibo para actualizar
      $recibo->recibo_updated_usuario = \Auth::user()->id; //usuario que actualiza el recibo
      $recibo->recibo_updated_at = date('Y-m-d H:i:s');
      $recibo->save(); //guarda cambios del recibo

      //crea el arreglo para llenar la tabla pivote erc_recibo_pago
      $pagos_id = array();
      $i = 0;

      if(!empty($request->pago_id))
      {
        foreach ($request->pago_id as $pagos) {
            $pagos_id[] = [
                        'pago_id' => $pagos,
                        'recibopago_valor' => $request->recibopago_valor[$i],
                        'recibopago_entidad_bancaria' => $request->recibopago_entidad_bancaria[$i],
                        'recibopago_codigo' => $request->recibopago_codigo[$i],
                        'recibopago_plaza' => $request->recibopago_plaza[$i]
                      ];
            $i++;
        }
      }

      $ver = $recibo->pagos()->detach(); //elimina todos los medios de pagos actuales del recibo
      $ver = $recibo->pagos()->attach($pagos_id); // agrega los nuevos medios de pagos del recibo

      //inserta registros en la tabla erc_recibo_concepto
      $concepto = Concepto::find($recibo->recibo_id);

      $concepto->fill($request->all());
      $concepto->recibo()->associate($recibo);
      $concepto->save();

      //guarda registro del usuario
      $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Actualización recibo N° '.$recibo->recibo_id.' Centro de Operación '.$recibo->centroOperacion->centrooperacion_codigo.' exitosa'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

      //mensaje de exito
      flash::overlay('El <b>eRC N° '.$recibo->recibo_consecutivo_centro.'</b> del Centro de Operación <b>'.$recibo->centroOperacion->centrooperacion_nombre.'</b> ha sido actualizado correctamente.', 'Notificación');

      return redirect()->route('erc.recibos.edit', [$sistema, $recibo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cartera(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');

        $pagos = Pago::orderBy('pago_nombre', 'ASC')->pluck('pago_nombre', 'pago_id');

        $recibos = Recibo::join('app_sia.sia_centros_operacion', 'erc_recibos.centrooperacion_id', '=', 'app_sia.sia_centros_operacion.centrooperacion_id')
        ->join('app_sia.sia_usuarios', 'recibo_created_usuario', '=', 'app_sia.sia_usuarios.id')
        ->join('erc_estados', 'erc_recibos.estado_id', '=', 'erc_estados.estado_id')
        ->orderBy('recibo_created_at', 'ASC')
        ->Filtro($request->filtro)
        ->Buscar($request->busqueda)
        ->paginate(25);

        if($request->filtro == 1)
        {
          $sistema->entrances()->create([
                                'usuario_id' => \Auth::user()->id,
                                'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                'entrance_url_anterior' => url()->previous(),
                                'entrance_url_actual' => url()->current(),
                                'entrance_accion' => nl2br('Vista Cartera recibos No Contabilizados'),  
                                'entrance_startdate' => date('Y-m-d H:i:s'),
                              ]);
        }
        elseif($request->filtro == 2)
        {
          $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista Cartera recibos Contabilizados'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        }

        return view('browse.erc.recibos.cartera')
        ->with('centros_operacion', $centros_operacion)
        ->with('pagos', $pagos)
        ->with('recibos', $recibos)
        ->with('sistema', $sistema)
        ->with('filtro', $request->filtro)
        ->with('busqueda', $request->busqueda)
        ->with('sistemas', $sistemas);
    }

    public function contabilizar(Request $request, Sistema $sistema, Recibo $recibo)
    {
      $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->get();
      $recibo->estado_id = 2; //cambia el estado a: Contabilizado
      $recibo->save();

      $recibo_centroperacion = $recibo->centroOperacion;

      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      
      $pagos = Pago::orderBy('pago_nombre', 'ASC')->pluck('pago_nombre', 'pago_id');
      
      $recibos = Recibo::join('app_sia.sia_centros_operacion', 'erc_recibos.centrooperacion_id', '=', 'app_sia.sia_centros_operacion.centrooperacion_id')
      ->join('app_sia.sia_usuarios', 'recibo_created_usuario', '=', 'app_sia.sia_usuarios.id')
      ->orderBy('recibo_created_at', 'ASC')
      ->Buscar($request->busqueda)
      ->paginate(25);

      $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Recibo N° '.$recibo->recibo_id.' Centro de Operación '.$recibo->centroOperacion->centrooperacion_codigo.' contabilizado'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

      //mensaje de alerta
      flash::overlay('El recibo <b>N° '.$recibo->recibo_consecutivo_centro.'</b> del Centro de operación <b>'.$recibo->centroOperacion->centrooperacion_nombre.'</b> ha sido marcado como <i>Contabilizado</i>.', 'Notificación');

      return redirect()->back();
    }
}

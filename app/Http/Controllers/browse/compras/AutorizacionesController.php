<?php

namespace App\Http\Controllers\Browse\compras;
//namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use Alert;

use App\Sistema;
use App\Usuario;
use App\Permiso;
use App\Rol;
use App\Area;
use App\Subarea;


use App\Model\Correos\Cuenta;
use App\Model\Compras\Solicitud;

class AutorizacionesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Sistema $sistema)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $usuarios = DB::connection('correos')->table('correos_cuentas')
      ->where('correos_cuentas.cuenta_id', '=', function($query) use ($sistema) {
        $query->select('app_sia.sia_usuarios.id')
        ->from('app_sia.sia_usuarios')
        ->whereRaw('app_sia.sia_usuarios.id = correos_cuentas.cuenta_id')
        ->where('app_sia.sia_usuarios.id', '=', function($query) use ($sistema){
          $query->select('app_sia.sia_permisos.usuario_id')
          ->from('app_sia.sia_permisos')
          ->whereRaw('app_sia.sia_permisos.usuario_id = app_sia.sia_usuarios.id')
          ->where('app_sia.sia_permisos.sistemarol_id', '=', function($query) use ($sistema){
            $query->select('app_sia.sia_sistema_rol.sistemarol_id')
            ->from('app_sia.sia_sistema_rol')
            ->whereRaw('app_sia.sia_sistema_rol.sistemarol_id = app_sia.sia_permisos.sistemarol_id')
            ->where('app_sia.sia_sistema_rol.sistema_id', '=', $sistema->sistema_id);
          });
        });
      })
      ->orderBy('correos_cuentas.cuenta_primer_apellido', 'ASC')
      ->orderBy('correos_cuentas.cuenta_segundo_apellido', 'ASC')
      ->orderBy('correos_cuentas.cuenta_primer_nombre', 'ASC')
      ->orderBy('correos_cuentas.cuenta_segundo_nombre', 'ASC')
      ->orderBy('correos_cuentas.cuenta_cuenta', 'ASC')
      ->paginate(25);

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Vista Autorizaciones'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.autorizaciones.index')
      ->with('sistema', $sistema)
      ->with('usuarios', $usuarios)
      ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sistema $sistema, Usuario $usuario)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $usuario_cuenta = $usuario->cuenta;
      $usuario_area = DB::connection('compras')->table('compras_autorizaciones')->select('area_id')->distinct()->where('usuario_id_area', '=', $usuario->id)->get(); //$usuario->autorizacionesAreas->select('area_id')->distinct();

      $usuario_subarea = DB::connection('compras')->table('compras_autorizaciones')->select('subarea_id')->distinct()->where('usuario_id_subarea', '=', $usuario->id)->get(); //$usuario->autorizacionesSubareas;

      $areas = Area::orderBy('area_nombre', 'ASC')->get();
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar Autorizaciones usuario: '.$usuario->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.autorizaciones.edit')
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('usuario', $usuario)
      ->with('usuario_area', $usuario_area)
      ->with('usuario_subarea', $usuario_subarea)
      ->with('sistema', $sistema)
      ->with('sistemas', $sistemas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema, Usuario $usuario)
    {
      /*$this->validate($request, [
          'area_id' => 'required:compras_autorizaciones',
          'subarea_id' => 'required:compras_autorizaciones'
      ]);*/

      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      if($request->area_id != null)
      {
        foreach($request->area_id as $areas)
        {
          $query = DB::connection('compras')->select("CALL compras_autorizacion_areas($areas, $usuario->id)");
        }
      }
      else
      {
        $query = DB::connection('compras')->table("compras_autorizaciones")->where('usuario_id_area', '=', $usuario->id)->update(['usuario_id_area' => null]);
      }

      if($request->subarea_id != null)
      {
        foreach($request->subarea_id as $subareas)
        {
          $area = Subarea::find($subareas);
          $query = DB::connection('compras')->select("CALL compras_autorizacion_subareas($area->area_id, $subareas, $usuario->id)");
        }
      }
      else
      {
        $query = DB::connection('compras')->table("compras_autorizaciones")->where('usuario_id_subarea', '=', $usuario->id)->update(['usuario_id_subarea' => null]);
      }

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Actualización Autorizaciones usuario: '.$usuario->email.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      //mensaje de alerta
      //flash()->overlay('Las autorizaciones en Áreas y Subareas del usuario <b>'.$usuario->email.'</b> han sido actualizadas correctamente.', 'Notificación');
      Alert::success('<p>Las autorizaciones en Áreas y Subareas del usuario <b>'.$usuario->email.'</b> han sido actualizadas correctamente.</p>')->html()->persistent('Cerrar');
      
      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

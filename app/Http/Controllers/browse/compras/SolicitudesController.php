<?php

namespace App\Http\Controllers\Browse\compras;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade;
use Laracasts\Flash\Flash;
use Alert;
use Excel;
use PDF;

//models
use App\Mail\Compras\NotificacionNuevaSolicitud;
use App\Mail\Compras\NotificacionActualizacionSolicitud;
use App\Mail\Compras\NotificacionAplazarSolicitud;
use App\Mail\Compras\NotificacionEvaluacionSolicitud;
use App\Mail\Compras\NotificacionArchivarSolicitud;
use App\Mail\Compras\NotificacionRechazarSolicitud;

use App\Area;
use App\Subarea;
use App\CentroCostos;
use App\CentroOperacion;
use App\Sistema;
use App\Entrance;
use App\Proyecto;
use App\Usuario;
use App\Model\Correos\Cuenta;

use App\Model\Compras\Solicitud;
use App\Model\Compras\Insumo;
use App\Model\Compras\Comentario;
use App\Model\Compras\Autorizacion;
use App\Model\Compras\Revision;
use App\Model\Compras\UnidadMedida;
use App\Model\Compras\Estado;

class SolicitudesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
      $estado = Estado::find($request->filtro);

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Gestionar solicitudes: vista solicitudes en estado: '.$estado->estado_nombre),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      if(\Auth::user()->rol_sia == 'Administrador' || \Auth::user()->rol_modulo == 'Manager' || \Auth::user()->rol_modulo == 'Super Revisor')
      {
        $solicitudes = Solicitud::orderBy('created_at', 'ASC')->get()/*->paginate(50)*/;

        return view('browse.compras.solicitudes.index')
        ->with('sistema', $sistema)
        ->with('solicitudes', $solicitudes)
        ->with('sistemas', $sistemas);
      }
      elseif(\Auth::user()->rol_modulo == 'Revisor')
      {
        $solicitudes = DB::connection('compras')->table('compras_solicitudes')
        ->whereExists(function ($query) use ($request) {
            $query->select(DB::raw(1))
                ->from('compras_revisiones')
                ->whereRaw('compras_revisiones.usuario_id_area ='.\Auth::user()->id)
                ->whereRaw('compras_solicitudes.area_id = compras_revisiones.area_id');
                //->whereRaw('compras_solicitudes.estado_id = '.$request->filtro);
        })->paginate(50);

        $solicitudes_subareas = DB::connection('compras')->table('compras_solicitudes')
        ->whereExists(function ($query) use ($request) {
            $query->select(DB::raw(1))
                ->from('compras_revisiones')
                ->whereRaw('compras_revisiones.usuario_id_subarea ='.\Auth::user()->id)
                ->whereRaw('compras_solicitudes.subarea_id = compras_revisiones.subarea_id');
                //->whereRaw('compras_solicitudes.estado_id = '.$request->filtro);
        })->paginate(50);

        return view('browse.compras.solicitudes.index')
        ->with('sistema', $sistema)
        ->with('solicitudes', $solicitudes)
        ->with('solicitudes_subareas', $solicitudes_subareas)
        ->with('sistemas', $sistemas);
      }
      else
      {
        $solicitudes = DB::connection('compras')->table('compras_solicitudes')
        ->whereExists(function ($query) use ($request) {
            $query->select(DB::raw(1))
                ->from('compras_autorizaciones')
                ->whereRaw('compras_autorizaciones.usuario_id_area ='.\Auth::user()->id)
                ->whereRaw('compras_solicitudes.area_id = compras_autorizaciones.area_id');
                //->whereRaw('compras_solicitudes.estado_id = '.$request->filtro);
        })->paginate(50);

        $solicitudes_subareas = DB::connection('compras')->table('compras_solicitudes')
        ->whereExists(function ($query) use ($request) {
            $query->select(DB::raw(1))
                ->from('compras_autorizaciones')
                ->whereRaw('compras_autorizaciones.usuario_id_subarea ='.\Auth::user()->id)
                ->whereRaw('compras_solicitudes.subarea_id = compras_autorizaciones.subarea_id');
                //->whereRaw('compras_solicitudes.estado_id = '.$request->filtro);
        })->paginate(50);

        return view('browse.compras.solicitudes.index')
        ->with('sistema', $sistema)
        ->with('solicitudes', $solicitudes)
        ->with('solicitudes_subareas', $solicitudes_subareas)
        ->with('sistemas', $sistemas);
      }
    }

    public function my(Request $request, Sistema $sistema)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $solicitudes = Solicitud::FilterMyRequests($request->solicitante)
      ->join('app_sia.sia_areas', 'compras_solicitudes.area_id', '=', 'app_sia.sia_areas.area_id')
      ->orderBy('created_at', 'ASC')->paginate(50);

      if($request->filtro == 1)
        {
          //guarda los registros del usuario en sia_entrances
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Mis Solicitudes: vista solicitudes en Procesamiento'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->filtro == 2)
        {
          //guarda los registros del usuario en sia_entrances
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Mis Solicitudes: vista solicitudes en Comité de Evaluación'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->filtro == 3)
        {
          //guarda los registros del usuario en sia_entrances
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Mis Solicitudes: vista solicitudes Aplazadas'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif ($request->filtro == 6) {
          //guarda los registros del usuario en sia_entrances
          $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Mis Solicitudes: vista Historial de solicitudes'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }

      return view('browse.compras.solicitudes.my')
      ->with('sistema', $sistema)
      ->with('solicitudes', $solicitudes)
      ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $areas = Area::orderBy('area_nombre', 'ASC')->get();
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
      $centros_costos = CentroCostos::orderBy('centrocosto_codigo', 'ASC');
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $proyectos = Proyecto::orderBy('proyecto_codigo', 'ASC');
      $cuenta = Cuenta::find(\Auth::user()->id);
      $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Crear nueva solicitud'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      Alert::warning('<p>Antes de realizar los requerimientos, tenga en cuenta los siguientes prerrequisitos: <ul><li>Si va a solicitar <b>piezas gráficas</b>, debe anexar la aprobación del Departamento de Comunicación Institucional.</li><li>Si va a solicitar <b>hardware o software</b>, debe anexar la aprobación del Departamento de Tecnología y Conectividad.</li><li>Si va a solicitar <b>producción editorial</b> (libros, revistas, etc.), debe anexar la aprobación de Biblioteca.</li></ul><br><b>Nota:</b> Si su requerimiento contiene los anteriores tipos de insumos, debe anexar todas las aprobaciones respectivas; de lo contrario, su requerimiento será devuelto y se retrasará.</p>', 'Atención')->html()->persistent('Entendido');

      return view('browse.compras.solicitudes.create')
      ->with('medidas', $medidas)
      ->with('cuenta', $cuenta)
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('centros_costos', $centros_costos)
      ->with('centros_operacion', $centros_operacion)
      ->with('proyectos', $proyectos)
      ->with('sistema', $sistema)
      ->with('sistemas', $sistemas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
      $this->validate($request, [
          'centrocostos_id' => 'required:compras_solicitud_centro_costos',
          'centrooperacion_id' => 'required:compras_solicitud_centro_operacion',
          'solicitud_presupuesto' => 'max:51200|mimes:zip,rar,xls,xlsx,doc,docx,pdf,jpg,jpeg,png', //50MB
          'proyecto_id' => 'required:compras_solicitud_proyecto',
      ]);

      //declaración variable nombre_presupuesto
      $nombre_presupuesto = null;

      //si solicitud_presupuesto no está vacío entonces cambia el nombre del archivo por la fecha actual y los guarda en la ruta public/files/compras/presupuestos
      if($request->file('solicitud_presupuesto') != null)
      {
        $file = $request->file('solicitud_presupuesto');
        $nombre_presupuesto = time().'.'.$file->getClientOriginalExtension();
        $path = public_path().'/files/compras/presupuestos/';
        $file->move($path, $nombre_presupuesto);
      }

      //valida el ID mayor de las solicitudes
      $consecutivo = Solicitud::max('solicitud_id');

      if($consecutivo == null)
      {
          $consecutivo = 'CO'.date('Y').'000001';
      }
      else
      {
          $consecutivo = explode('CO', $consecutivo);
          $consecutivo = 'CO'.($consecutivo[1]+1);
      }

      //instancia la clase Solicitud y envía por parametro los campos del formulario create.blade.php
      $solicitud = new Solicitud($request->all());
      $solicitud->solicitud_id = $consecutivo; //\Auth::user()->id.time();
      $solicitud->usuario_id = \Auth::user()->id;
      $solicitud->estado_id = 1; //estado de Procesamiento

      if($nombre_presupuesto != null)
      {
        $solicitud->solicitud_presupuesto = $nombre_presupuesto;
      }
      //guarda la solicitud
      $solicitud->save();

      //guarda los centros de costos en la tabla privote compras_solicitud_centro_costos
      $solicitud->centrosCostos()->sync($request->centrocostos_id);

      //guarda los centros de operacion en la tabla privote compras_solicitud_centro_operacion
      $solicitud->centrosOperacion()->sync($request->centrooperacion_id);

      //guarda los centros de operacion en la tabla privote compras_solicitud_centro_operacion
      $solicitud->proyectos()->sync($request->proyecto_id);

      //guarda los insumos en la tabla privote compras_solicitud_insumo
      $solicitud_id = array();
      $i = 0;

      if(!empty($request->solicitudinsumo_referencia))
      {
        foreach ($request->solicitudinsumo_referencia as $referencia)
        {
          $solicitud_id[] = [
                        'solicitudinsumo_referencia' => $referencia,
                        'solicitudinsumo_medida' => $request->solicitudinsumo_medida[$i],
                        'solicitudinsumo_cantidad' => $request->solicitudinsumo_cantidad[$i],
                        'solicitudinsumo_caracteristica' => $request->solicitudinsumo_caracteristica[$i],
                        'solicitudinsumo_costo' => $request->solicitudinsumo_costo[$i],
                        'solicitud_id' => $solicitud->solicitud_id,
                        'estado_id' => '1',
                      ];
          $i++;
        }

        $ii = 0;
        foreach ($request->solicitudinsumo_referencia as $referencia)
        {
          $insumo = new Insumo($request->all());
          $insumo->create($solicitud_id[$ii]);
          $ii++;
        }
      }

      //envia notificacion al email
      Mail::to(\Auth::user()->email)
      ->bcc('sia@unicatolica.edu.co')
      ->send(new NotificacionNuevaSolicitud($sistema, $solicitud));

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Solicitud N° '.$solicitud->solicitud_id.' guardada exitosamente'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      Alert::success('<p>La solicitud <b>N° '.$solicitud->solicitud_id.'</b> ha sido guardada correctamente. Un correo electrónico ha sido enviado a la dirección <b>'.$solicitud->solicitud_email_solicitante.'</b>.</p>', '¡Muy bien!')->html()->persistent();

      return redirect()->route('browse.compras.solicitudes.create', $sistema);
    }

    /**
     * Display the specified resource.
     *
     * @param  object  $sistema
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
      //dd($solicitud->subarea->autorizacionesUsuarios->first()->email);
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
      
      $areas = Area::orderBy('area_nombre', 'ASC')->pluck('area_nombre', 'area_id');
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
      $centros_costos = CentroCostos::orderBy('centrocosto_nombre', 'ASC')->pluck('centrocosto_nombre', 'centrocosto_id');
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $proyectos = Proyecto::orderBy('proyecto_nombre', 'ASC')->pluck('proyecto_nombre', 'proyecto_id');
      $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Ver solicitud N° '.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.solicitudes.show')
      ->with('medidas', $medidas)
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('centros_costos', $centros_costos)
      ->with('centros_operacion', $centros_operacion)
      ->with('proyectos', $proyectos)
      ->with('sistema', $sistema)
      ->with('solicitud', $solicitud)
      ->with('filtro', $request->filtro)
      ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sistema $sistema, Solicitud $solicitud)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $solicitud_centros_costos = $solicitud->centrosCostos;
      $solicitud_centros_operacion = $solicitud->centrosOperacion;
      $solicitud_proyecto = $solicitud->proyecto;
      $solicitud_insumo = $solicitud->insumos;
      $solicitud_comentario = $solicitud->comentarios;

      $areas = Area::orderBy('area_nombre', 'ASC')->get();
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->get();
      $centros_costos = CentroCostos::orderBy('centrocosto_codigo', 'ASC');
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $proyectos = Proyecto::orderBy('proyecto_codigo', 'ASC');
      $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');

      //Guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar solicitud N° '.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.solicitudes.edit')
      ->with('medidas', $medidas)
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('centros_costos', $centros_costos)
      ->with('centros_operacion', $centros_operacion)
      ->with('proyectos', $proyectos)
      ->with('sistema', $sistema)
      ->with('solicitud', $solicitud)
      ->with('sistemas', $sistemas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        $this->validate($request, [
            'centrocostos_id' => 'required:compras_solicitud_centro_costos',
            'centrooperacion_id' => 'required:compras_solicitud_centro_operacion',
            'solicitud_presupuesto' => 'max:51200|mimes:zip,rar,pdf,xls,xlsx,doc,docx,jpg,jpeg,png', //50MB
        ]);

        //declaración varable nombre_presupuesto
        $nombre_presupuesto = null;

        //si solicitud_presupuesto no está vacío entonces cambia el nombre del archivo por la fecha actual y los guarda en la ruta public/files/compras/presupuestos
        if($request->file('solicitud_presupuesto') != null)
        {
          $file = $request->file('solicitud_presupuesto');
          $nombre_presupuesto = time().'.'.$file->getClientOriginalExtension();
          $path = public_path().'/files/compras/presupuestos/';
          $file->move($path, $nombre_presupuesto);
        }

        //instancia la clase Solicitud y envía por parametro los campos del formulario create.blade.php
        //$solicitud = new Solicitud($request->all());
        $solicitud->fill($request->all());
        $solicitud->estado_id = 1; //estado de Procesamiento

        if($nombre_presupuesto != null)
        {
          $solicitud->solicitud_presupuesto = $nombre_presupuesto;
        }

        //ejecuta el metodo update para guardar los cambios
        $solicitud->update();

        //guarda los centros de costos en la tabla privote compras_solicitud_centro_costos
        $solicitud->centrosCostos()->sync($request->centrocostos_id);

        //guarda los centros de operacion en la tabla privote compras_solicitud_centro_operacion
        $solicitud->centrosOperacion()->sync($request->centrooperacion_id);

        //guarda los proyectos en la tabla privote compras_solicitud_centro_operacion
        $solicitud->proyectos()->sync($request->proyecto_id);

        //guarda los insumos en la tabla privote compras_solicitud_insumo
        $solicitud_id = array();
        $i = 0;

        //si existen insumos entonces guarda los insumos antiguos y nuevos
        if(!empty($request->solicitudinsumo_referencia))
        {
          foreach ($request->solicitudinsumo_referencia as $referencia)
          {
            $solicitud_id[] = [
                          'solicitudinsumo_referencia' => $referencia,
                          'solicitudinsumo_medida' => $request->solicitudinsumo_medida[$i],
                          'solicitudinsumo_cantidad' => $request->solicitudinsumo_cantidad[$i],
                          'solicitudinsumo_caracteristica' => $request->solicitudinsumo_caracteristica[$i],
                          'solicitudinsumo_costo' => $request->solicitudinsumo_costo[$i],
                          'solicitud_id' => $solicitud->solicitud_id,
                          'estado_id' => '1',
                        ];
            $i++;
          }

          //si la solicitud tiene insumos entonces los elimina y los vuelve a guardar con los nuevos registros 
          if($request->solicitudinsumo_id != null)
          {
            foreach ($request->solicitudinsumo_id as $id)
            {
              $insumo = Insumo::where('solicitud_id', '=', $solicitud->solicitud_id);
              $insumo->delete();
            }

            $ii = 0;
            foreach ($request->solicitudinsumo_referencia as $referencia)
            {
              $insumo = new Insumo($request->all());
              $insumo->create($solicitud_id[$ii]);
              $ii++;
            }
          }
          //de lo contrario, si no tiene insumos ingresa los nuevos
          else
          {
            $ii = 0;
            foreach ($request->solicitudinsumo_referencia as $referencia)
            {
              $insumo = new Insumo($request->all());
              $insumo->create($solicitud_id[$ii]);
              $ii++;
            }
          }
        }

        //sino, elimina todos los insumos guardados al crear la solicitud
        else
        {
          $insumo = Insumo::where('solicitud_id', '=', $solicitud->solicitud_id);
          $insumo->delete();
        }

        //envía notificacion al correo electronico del solicitante y copia al administrador de sia
        Mail::to(\Auth::user()->email)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionActualizacionSolicitud($sistema, $solicitud));

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Actualización solicitud N° '.$solicitud->solicitud_id.' exitosa'),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        //muestra ventana modal de alerta
        Alert::info('<p>Su solicitud <b>N° '.$solicitud->solicitud_id.'</b> se ha actualizado exitosamente. Un correo electrónico ha sido enviado a la dirección <b>'.$solicitud->solicitud_email_solicitante.'</b>.', 'Atención')->html()->persistent();

        //Vuelve a cargar el formulario de edicion y muestra los nuevos datos de la solcitud
        return redirect()->back();
    }

    /**
     * Muestra el formulario para firmar o autorizar la solicitud al usuario autorizante.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema $sistema
     * @param  \App\Model\Compras\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function sign(Request $request, Sistema $sistema, Solicitud $solicitud)
    {

      /*--------------------*/
        //verifica si el area necesita revisión
        $revisiones = DB::connection('compras')->table('compras_revisiones')->where('area_id', $solicitud->area_id)->get();
        
        $revisor_area = 0;
        foreach ($revisiones as $revision) 
        {
          if($revision->usuario_id_area != null)
          {
            $revisor_area = 1;
          }
        }

        //verifica si el subarea necesita revisión
        $revisiones = DB::connection('compras')->table('compras_revisiones')->where('subarea_id', $solicitud->subarea_id)->first();

        $revisor_subarea = 0;
        if($revisiones->usuario_id_subarea != null)
        {
            $revisor_subarea = 1;
        }

        $revisores = $revisor_area + $revisor_subarea;
      /*--------------------*/

      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $solicitud_comentario = $solicitud->comentarios;

      $areas = Area::orderBy('area_nombre', 'ASC')->pluck('area_nombre', 'area_id');
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
      $centros_costos = CentroCostos::orderBy('centrocosto_nombre', 'ASC')->pluck('centrocosto_nombre', 'centrocosto_id');
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $proyectos = Proyecto::orderBy('proyecto_nombre', 'ASC')->pluck('proyecto_nombre', 'proyecto_id');
      $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Firmar solicitud N° '.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.solicitudes.sign')
      ->with('medidas', $medidas)
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('centros_costos', $centros_costos)
      ->with('centros_operacion', $centros_operacion)
      ->with('proyectos', $proyectos)
      ->with('sistema', $sistema)
      ->with('solicitud', $solicitud)
      ->with('filtro', $request->filtro)
      ->with('sistemas', $sistemas);
    }

    /**
     * Guarda la firma o autorizacion de la solicitud.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema $sistema
     * @param  \App\Model\Compras\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function updateSign(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
      $autorizaciones = Autorizacion::where([['area_id', '=', $solicitud->area_id],['subarea_id', '=', $solicitud->subarea_id]])->get();

      //dd($autorizaciones);

      if($autorizaciones['0']['usuario_id_area'] == \Auth::user()->id)
      {
        if($autorizaciones['0']['usuario_id_subarea'] == null && $solicitud->solicitud_tercera_firma != null)
        {
          $solicitud->solicitud_primera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_firma = date('Y-m-d H:i:s');
          $solicitud->estado_id = 2; //cambia el estado a Evaluación Comité
          $solicitud->save();

          //envía notificacion al solicitante al email
          Mail::to($solicitud->solicitud_email_solicitante)
          ->cc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionEvaluacionSolicitud($sistema, $solicitud));
        }
        elseif($solicitud->solicitud_segunda_firma != null && $solicitud->solicitud_tercera_firma != null)
        {
          $solicitud->solicitud_primera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_firma = date('Y-m-d H:i:s');
          $solicitud->estado_id = 2; //cambia el estado a Evaluación Comité
          $solicitud->save();

          //envía notificacion al solicitante al email
          Mail::to($solicitud->solicitud_email_solicitante)
          ->cc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionEvaluacionSolicitud($sistema, $solicitud));
        }
        else
        {
          $solicitud->solicitud_primera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_firma = date('Y-m-d H:i:s');
          $solicitud->save();
        }        
      }
      elseif($autorizaciones['0']['usuario_id_subarea'] == \Auth::user()->id)
      {
        if($solicitud->solicitud_primera_firma != null || $solicitud->solicitud_segunda_firma != null)
        {
          $solicitud->solicitud_segunda_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_segunda_firma = date('Y-m-d H:i:s');
          $solicitud->estado_id = 2; //cambia el estado a Evaluación Comité
          $solicitud->save();

          //envía notificacion al solicitante al email
          Mail::to($solicitud->solicitud_email_solicitante)
          ->cc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionEvaluacionSolicitud($sistema, $solicitud));
        }
        else
        {
          $solicitud->solicitud_segunda_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_segunda_firma = date('Y-m-d H:i:s');
          $solicitud->save();
        }
      }
      else
      {
        if($autorizaciones['0']['usuario_id_subarea'] == null && $solicitud->solicitud_primera_firma != null)
        {
          $solicitud->solicitud_tercera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_firma = date('Y-m-d H:i:s');
          $solicitud->estado_id = 2; //cambia el estado a Evaluación Comité
          $solicitud->save();

          //envía notificacion al solicitante al email
          Mail::to($solicitud->solicitud_email_solicitante)
          ->cc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionEvaluacionSolicitud($sistema, $solicitud));
        }
        elseif($solicitud->solicitud_primera_firma != null || $solicitud->solicitud_segunda_firma != null)
        {
          $solicitud->solicitud_tercera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_firma = date('Y-m-d H:i:s');
          $solicitud->estado_id = 2; //cambia el estado a Evaluación Comité
          $solicitud->save();

          //envía notificacion al solicitante al email
          Mail::to($solicitud->solicitud_email_solicitante)
          ->cc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionEvaluacionSolicitud($sistema, $solicitud));
        }
        else
        {
          $solicitud->solicitud_tercera_firma = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_firma = date('Y-m-d H:i:s');
          $solicitud->save();
        }
      }

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Solicitud N° '.$solicitud->solicitud_id.' firmada exitosamente'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      //muestra ventana modal de alerta
      //flash()->overlay('Usted ha autorizado la solicitud N° <b>'.$solicitud->solicitud_id.'</b>.', 'Notificación');
      Alert::success('<p>Usted ha autorizado la solicitud <b>N° '.$solicitud->solicitud_id.'</b>.</p>')->html()->persistent();

      //redirecciona al listado de las solicitudes
      return redirect()->route('browse.compras.solicitudes.index', [$sistema, 'filtro' => $request->filtro, 'autorizante' => \Auth::user()->id]);
    }

    /**
     * Guarda la revision de la solicitud.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema $sistema
     * @param  \App\Model\Compras\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
      $revisiones = Revision::where([['area_id', '=', $solicitud->area_id],['subarea_id', '=', $solicitud->subarea_id]])->get();

      if($revisiones['0']['usuario_id_area'] == \Auth::user()->id)
      {
        if($revisiones['0']['usuario_id_subarea'] == null && $solicitud->solicitud_tercera_revision != null)
        {
          $solicitud->solicitud_primera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
        elseif($solicitud->solicitud_segunda_revision != null && $solicitud->solicitud_tercera_revision != null)
        {
          $solicitud->solicitud_primera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
        else
        {
          $solicitud->solicitud_primera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_primera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }        
      }
      elseif($revisiones['0']['usuario_id_subarea'] == \Auth::user()->id)
      {
        if($solicitud->solicitud_primera_revision != null || $solicitud->solicitud_segunda_revision != null)
        {
          $solicitud->solicitud_segunda_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_segunda_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
        else
        {
          $solicitud->solicitud_segunda_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_segunda_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
      }
      else
      {
        if($revisiones['0']['usuario_id_subarea'] == null && $solicitud->solicitud_primera_revision != null)
        {
          $solicitud->solicitud_tercera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
        elseif($solicitud->solicitud_primera_revision != null || $solicitud->solicitud_segunda_revision != null)
        {
          $solicitud->solicitud_tercera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
        else
        {
          $solicitud->solicitud_tercera_revision = \Auth::user()->id;
          $solicitud->solicitud_fecha_tercera_revision = date('Y-m-d H:i:s');
          $solicitud->save();
        }
      }

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Solicitud N° '.$solicitud->solicitud_id.' marcada como revisada.'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      //muestra ventana modal de alerta
      Alert::success('<p>Usted ha marcado como revisada la solicitud <b>N° '.$solicitud->solicitud_id.'</b>.</p>')->html()->persistent('Cerrar');

      //redirecciona al listado de las solicitudes
      return redirect()->route('browse.compras.solicitudes.index', [$sistema, 'filtro' => $request->filtro, 'autorizante' => \Auth::user()->id]);
    }

    /**
     * Procesa la solicitud y cambia el estado a Aplazada
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema $sistema
     * @param  \App\Model\Compras\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function deny(Request $request, Sistema $sistema, Solicitud $solicitud)
    { 
      $solicitud->solicitud_primera_firma = null;
      $solicitud->solicitud_fecha_primera_firma = null;
      $solicitud->solicitud_segunda_firma = null;
      $solicitud->solicitud_fecha_segunda_firma = null;
      $solicitud->solicitud_tercera_firma = null;
      $solicitud->solicitud_fecha_tercera_firma = null;
      $solicitud->solicitud_primera_revision = null;
      $solicitud->solicitud_fecha_primera_revision = null;
      $solicitud->solicitud_segunda_revision = null;
      $solicitud->solicitud_fecha_segunda_revision = null;
      $solicitud->solicitud_tercera_revision = null;
      $solicitud->solicitud_fecha_tercera_revision = null;
      $solicitud->solicitud_usuario_aplazamiento = \Auth::user()->id;
      $solicitud->solicitud_fecha_aplazamiento = date('Y-m-d H:i:s');
      $solicitud->estado_id = 3; //cambia a estado Aplazada
      $solicitud->save();

      $comentario = new \App\Model\Compras\Comentario(['solicitudcomentario_comentario' => nl2br($request->solicitudcomentario_comentario), 'usuario_id' => \Auth::user()->id]);
      $solicitud->comentarios()->save($comentario);

      //envía notificacion al solicitante al email
      Mail::to($solicitud->solicitud_email_solicitante)
      ->cc(['sia@unicatolica.edu.co'])
      ->send(new NotificacionAplazarSolicitud($sistema, $solicitud, $request->solicitudcomentario_comentario));

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Aplazamiento solicitud N° '.$solicitud->solicitud_id.' exitoso'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      //muestra ventana modal de alerta
      Alert::info('<p>Usted ha solicitado la revisión del requerimiento con código: <b>'.$solicitud->solicitud_id.'</b>.<br><br>Un correo electrónico ha sido enviado al solicitante a la dirección <b>'.$solicitud->solicitud_email_solicitante.'</b>.</p>', 'Atención')->html()->persistent();

      //retorna el index de gestionar solicitudes
      return redirect()->route('browse.compras.solicitudes.index', [$sistema, 'filtro' => $request->filtro, 'autorizante' => \Auth::user()->id]);
    }

    /**
     * Muestra el formulario de evaluacion al comité de Compras
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema $sistema
     * @param  \App\Model\Compras\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */

    public function evaluate(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
      $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

      $areas = Area::orderBy('area_nombre', 'ASC')->pluck('area_nombre', 'area_id');
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
      $centros_costos = CentroCostos::orderBy('centrocosto_nombre', 'ASC')->pluck('centrocosto_nombre', 'centrocosto_id');
      $centros_operacion = CentroOperacion::orderBy('centrooperacion_nombre', 'ASC')->pluck('centrooperacion_nombre', 'centrooperacion_id');
      $proyectos = Proyecto::orderBy('proyecto_nombre', 'ASC')->pluck('proyecto_nombre', 'proyecto_id');
      $medidas = UnidadMedida::orderBy('umedida_nombre', 'ASC');

      //guarda los registros del usuario en sia_entrances
      $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Comité de Evaluación: evaluar solicitud N° '.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);

      return view('browse.compras.solicitudes.evaluate')
      ->with('medidas', $medidas)
      ->with('areas', $areas)
      ->with('subareas', $subareas)
      ->with('centros_costos', $centros_costos)
      ->with('centros_operacion', $centros_operacion)
      ->with('proyectos', $proyectos)
      ->with('sistema', $sistema)
      ->with('solicitud', $solicitud)
      ->with('filtro', $request->filtro)
      ->with('sistemas', $sistemas);
    }

    public function archive(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
      $count = 0;

      foreach ($solicitud->insumos as $insumo) {
        if($insumo->estado_id == 1)
        {
          $count++;
        }
      }

      if($count == 0)
      {
        $solicitud->estado_id = 6; //cambia el estado a Archivada
        $solicitud->solicitud_usuario_archiva = \Auth::user()->id;
        $solicitud->solicitud_archived_at = date('Y-m-d H:i:s');
        $solicitud->save();

        //Guarda en PDF el requerimiento para evitar cambios
        $areas = Area::get();
        $subareas = Subarea::get();
        $centros_costos = CentroCostos::get();
        $centros_operacion = CentroOperacion::get();
        $proyectos = Proyecto::get();

        $usuario_imprime = \Auth::user()->email;
        $pdf = PDF::loadView('browse.compras.solicitudes.download', [
          'solicitud' => $solicitud,
          'sistema' => $sistema,
          'areas' => $areas,
          'subareas' => $subareas,
          'centros_costos' => $centros_costos,
          'centros_operacion' => $centros_operacion,
          'proyectos' => $proyectos,
        ])->save('files/compras/solicitudes/'.$solicitud->solicitud_id.'.pdf');


        //envía notificacion al correo electronico del solicitante y copia al administrador de sia
        Mail::to($solicitud->solicitud_email_solicitante)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionArchivarSolicitud($sistema, $solicitud));

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                      'usuario_id' => \Auth::user()->id,
                                      'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                      'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                      'entrance_url_anterior' => url()->previous(),
                                      'entrance_url_actual' => url()->current(),
                                      'entrance_accion' => nl2br('Archivar solicitud. Solicitud N° '.$solicitud->solicitud_id.' archivada exitosamente'),  
                                      'entrance_startdate' => date('Y-m-d H:i:s'),
                                    ]);

        //muestra ventana modal de alerta
        Alert::info('<p>La solicitud <b>N° '.$solicitud->solicitud_id.'</b> ha sido archivada. Un correo electrónico ha sido enviado al solicitante a la dirección <b>'.$solicitud->solicitud_email_solicitante.'</b>.</p>')->html()->persistent('Cerrar');

        //return redirect()->back();
        //retorna el index de gestionar solicitudes
        return redirect()->route('browse.compras.solicitudes.index', [$sistema, 'filtro' => $request->filtro, 'autorizante' => \Auth::user()->id]);
      }
      else
      {
        Alert::error('<p>La solicitud <b>N° '.$solicitud->solicitud_id.'</b> no puede ser archivada debido a que aún hay <b>'.$count.' insumos pendientes</b> por procesar. Haz clic en la opción <i>Ver Insumos</i>.</p>')->html()->persistent('Cerrar');

        return redirect()->back();
      }
  }

  public function reject(Request $request, Sistema $sistema, Solicitud $solicitud)
  {
    $solicitud->estado_id = 5; //cambia el estado a Rechazada
    $solicitud->solicitud_usuario_rechaza = \Auth::user()->id;
    $solicitud->solicitud_rejected_at = date('Y-m-d H:i:s');
    $solicitud->save();

    //Guarda en PDF el requerimiento para evitar cambios
    $areas = Area::get();
    $subareas = Subarea::get();
    $centros_costos = CentroCostos::get();
    $centros_operacion = CentroOperacion::get();
    $proyectos = Proyecto::get();

    $usuario_imprime = \Auth::user()->email;
    $pdf = PDF::loadView('browse.compras.solicitudes.download', [
      'solicitud' => $solicitud,
      'sistema' => $sistema,
      'areas' => $areas,
      'subareas' => $subareas,
      'centros_costos' => $centros_costos,
      'centros_operacion' => $centros_operacion,
      'proyectos' => $proyectos,
    ])->save('files/compras/solicitudes/'.$solicitud->solicitud_id.'.pdf');

    //envía notificacion al correo electronico del solicitante y copia al administrador de sia
    Mail::to($solicitud->solicitud_email_solicitante)
    ->cc(['sia@unicatolica.edu.co'])
    ->send(new NotificacionRechazarSolicitud($sistema, $solicitud));

    //guarda los registros del usuario en sia_entrances
    $sistema->entrances()->create([
                                'usuario_id' => \Auth::user()->id,
                                'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                'entrance_url_anterior' => url()->previous(),
                                'entrance_url_actual' => url()->current(),
                                'entrance_accion' => nl2br('Archivar solicitud. Solicitud N° '.$solicitud->solicitud_id.' rechazada exitosamente'),  
                                'entrance_startdate' => date('Y-m-d H:i:s'),
                              ]);

    //muestra ventana modal de alerta
    Alert::info('<p>La solicitud <b>N° '.$solicitud->solicitud_id.'</b> ha sido archivada. Un correo electrónico ha sido enviado al solicitante a la dirección <b>'.$solicitud->solicitud_email_solicitante.'</b>.</p>')->html()->persistent('Cerrar');

    //return redirect()->back();
    //retorna el index de gestionar solicitudes
    return redirect()->route('browse.compras.solicitudes.index', [$sistema, 'filtro' => $request->filtro, 'autorizante' => \Auth::user()->id]);
  }
}

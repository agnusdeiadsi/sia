<?php

namespace App\Http\Controllers\browse\correos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Mail;
use Alert;

use App\Sistema;
use App\Subarea;
use App\Contrato;
use App\Extension;
use App\Rol;
use App\CargoUnicatolica as Cargo;
use App\Sede;

use App\Model\Correos\Cuenta;
use App\Model\Correos\Tipo;
use App\Model\Correos\Estado;

use App\Mail\Correos\NotificacionNuevaSolicitud;
use App\Mail\Correos\NotificacionActualizarDatos;
use App\Mail\Correos\NotificacionActualizacionDatos;
use App\Mail\Correos\NotificacionProcesarSolicitud;
use App\Mail\Correos\NotificacionRestablecerContrasena;
use App\Mail\Correos\NotificacionRestablecimientoContrasena;


class CuentasController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request, $sistema) // lista los registros de la tabla correos_cuentas
     {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $sistema = Sistema::find($sistema);
        $cuentas = Cuenta::orderBy('cuenta_primer_apellido', 'ASC')
        ->join('correos_estados', 'correos_cuentas.estado_id', '=', 'correos_estados.estado_id')
        ->join('correos_tipos', 'correos_cuentas.tipo_id', '=', 'correos_tipos.tipo_id')
        ->Filtrar($request->filtrar)
        ->paginate(25);

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista cuentas'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.correos.cuentas.index')
        ->with('cuentas', $cuentas)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Sistema $sistema)
     {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
        $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Crear nueva cuenta'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        //carga formulario de creación
        return view('browse.correos.cuentas.create')
        ->with('sedes', $sedes)
        ->with('cargos', $cargos)
        ->with('tipos', $tipos)
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('subareas', $subareas)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sistema $sistema)
    {
        //dd($sistema);

        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        //$sistema = Sistema::find($sistema);

        $array1 = array('ñ','á','é','í','ó','ú','à','è','ì','ò','ù','â','ê','î','ô','û','ä','ë','ï','ö','ü','Ñ','Á','É','Í','Ó','Ú','À','È','Ì','Ò','Ù','Â','Ê','Î','Ô','Û','Ä','Ë','Ï','Ö','Ü');

  	    $array2 = array('n','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','N','A','E','I','O','U','A','E','I','O','U','A','E','I','O','U','A','E','I','O','U');

        //reemplaza los caracteres especiales introducidos en el formulario para los nombres y apellidos
        $primer_nombre = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_primer_nombre)));
      	$segundo_nombre = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_segundo_nombre)));
      	$primer_apellido = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_primer_apellido)));
      	$segundo_apellido = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_segundo_apellido)));

        //toma los letras iniciales del primer y segundo nombre y segundo apellido y las pasa a minusculas
        $inicial_primer_nombre = strtolower(substr($primer_nombre, 0, 1));
  	    $inicial_segundo_nombre = strtolower(substr($segundo_nombre, 0, 1));
  	    $inicial_segundo_apellido = strtolower(substr($segundo_apellido, 0, 1));

        //pasa a minusculas el primer apellido
        $primer_apellido_lower = strtolower($primer_apellido);

        //posibles combinaciones de usuarios para el nuevo registro
        $opcion_1 = $inicial_primer_nombre.$primer_apellido_lower;
        $opcion_2 = $inicial_primer_nombre.$inicial_segundo_nombre.$primer_apellido_lower;
        $opcion_3 = $inicial_primer_nombre.$primer_apellido_lower.$inicial_segundo_apellido;
        $opcion_4 = $inicial_primer_nombre.$inicial_segundo_nombre.$primer_apellido_lower.$inicial_segundo_apellido;

        //se crea el arreglo con las posibles combinaciones de usuarios
        $opciones = [$opcion_1, $opcion_2, $opcion_3, $opcion_4];

        //valida si existen las posibles combinaciones para crear una nueva cuenta en la base de datos
        $cuenta_usuario = null;

        $usuario = new Cuenta();
        for ($i=0; $i < 4; $i++) {

          $existe = $usuario->where('cuenta_usuario', '=', ''.$opciones[$i].'')->get()->all();

          if(empty($existe) || $existe == null)
          {
            $cuenta_usuario = $opciones[$i];
          }
        }

        //si hay usuarios disponibles entonces guarda la nueva cuenta y retorna al formulario de creacion de nueva cuenta
        if($cuenta_usuario != null)
        {
            $this->validate($request, [
            'cuenta_identificacion' => 'unique:correos.correos_cuentas,cuenta_identificacion,1,tipo_id', //valida si el usuario ya tiene una cuenta de tipo docencia
            ]);

            $cuenta = new Cuenta($request->all());
            $cuenta->cuenta_usuario = $cuenta_usuario;
            $cuenta->cuenta_cuenta = $cuenta_usuario."@unicatolica.edu.co";
            $cuenta->estado_id = 1; //estado procesamiento
            $cuenta->tipo_id = 2; //correo docente
            $cuenta->save();

            $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Cuenta '.$cuenta->cuenta_cuenta.' guardada exitosamente.'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

            //Envía notificación al email del solicitante y del suscriptor
            $bcc_recipients[0] = 'sia@unicatolica.edu.co';

            if(!empty($sistema->roles->where('rol_codigo', 'MAN')))
            {
              $recipients = $sistema->roles->where('rol_codigo', 'MAN')->first()->sistemaroles->where('sistema_id', $sistema->sistema_id)->first()->usuarios;
              
              if(!empty($recipients))
              {
                $i = 1;
                foreach ($recipients as $recipient) {
                  $bcc_recipients[$i] = $recipient->email;
                  ++$i;
                }
              }
            }
                        
            Mail::to([\Auth::user()->email, $cuenta->cuenta_email])
            ->bcc($bcc_recipients)
            ->send(new NotificacionNuevaSolicitud($cuenta));

            //muestra ventana modal de alerta
            Alert::success('<p>La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido guardada exitosamente.</p>', '¡Muy bien!')->html()->persistent();
        }
        //si no hay usuarios disponibles entonces muestra mensaje de alerta y retorna al formulario de creacion de nueva cuenta
        else
        {
          $sistema->entrances()->create([
                            'usuario_id' => \Auth::user()->id,
                            'usuario_rol_sia' => \Auth::user()->rol_sia, 
                            'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                            'entrance_url_anterior' => url()->previous(),
                            'entrance_url_actual' => url()->current(),
                            'entrance_accion' => nl2br('Error al guardar cuenta. La cuenta ya existe'),  
                            'entrance_startdate' => date('Y-m-d H:i:s'),
                          ]);
          //muestra ventana modal de alerta
          //flash()->overlay('No es posible crear la cuenta debido a que no existen usuarios disponibles.', 'Notificación');
          Alert::error('<p>No es posible crear la cuenta debido a que no existen usuarios disponibles.</p>')->html()->persistent();
        }

        //retorna al formulario de crear cuenta
        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $extensiones = Extension::orderBy('extension_id', 'ASC')->pluck('extension_extension', 'extension_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        /*return view('browse.correos.cuentas.create')
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('tipos', $tipos)
        ->with('subareas', $subareas)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);*/

        return redirect()->back();
    }

    public function storeAdmin(Request $request, $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();
        $sistema = Sistema::find($sistema);

        $this->validate($request, [
            'cuenta_cuenta' => 'unique:correos.app_correos.correos_cuentas', //connection.db.table
        ]);   

        $cuenta = new Cuenta($request->all());
        $cuenta->estado_id = 1; //estado procesamiento
        $cuenta->tipo_id = 1; //correo administrativo
        $cuenta->save();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Cuenta '.$cuenta->cuenta_cuenta.' guardada exitosamente'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        //envia notificacion al email del solicitante y del suscriptor
        $bcc_recipients[0] = 'sia@unicatolica.edu.co';

        if(!empty($sistema->roles->where('rol_codigo', 'MAN')))
        {
          $recipients = $sistema->roles->where('rol_codigo', 'MAN')->first()->sistemaroles->where('sistema_id', $sistema->sistema_id)->first()->usuarios;
          
          if(!empty($recipients))
          {
            $i = 1;
            foreach ($recipients as $recipient) {
              $bcc_recipients[$i] = $recipient->email;
              ++$i;
            }
          }
        }
                    
        Mail::to([\Auth::user()->email, $cuenta->cuenta_email])
        ->bcc($bcc_recipients)
        ->send(new NotificacionNuevaSolicitud($cuenta));

        //muestra ventana modal de alerta
        //flash()->overlay('La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido guardada correctamente.', 'Notificación');
        Alert::success('<p>La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido guardada correctamente</p>')->html()->persistent('Cerrar');

        //retorna al formulario de crear cuenta
        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $extensiones = Extension::orderBy('extension_id', 'ASC')->pluck('extension_extension', 'extension_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        return view('browse.correos.cuentas.create')
        ->with('sedes', $sedes)
        ->with('cargos', $cargos)
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('tipos', $tipos)
        ->with('subareas', $subareas)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cuenta $cuenta)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function account($sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $cuenta = Cuenta::find(\Auth::user()->id);
        $sistema = Sistema::find($sistema);
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
        $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Ver/Editar mi cuenta '.$cuenta->cuenta_cuenta.''),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);


        //carga formulario de creación
        return view('browse.correos.cuentas.account')
        ->with('sedes', $sedes)
        ->with('cargos', $cargos)
        ->with('tipos', $tipos)
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('subareas', $subareas)
        ->with('sistema', $sistema)
        ->with('cuenta', $cuenta)
        ->with('sistemas', $sistemas);
    }

    public function updateAccount(Request $request, $sistema, $cuenta_id)
    {
        //dd($request->cuenta_fecha_nacimiento);

        $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->get();
        $sistema = Sistema::find($sistema);

        $cuenta = Cuenta::find($cuenta_id);
        $cuenta->cuenta_codigo = $request->cuenta_identificacion;
        $cuenta->cuenta_tipo_identificacion = $request->cuenta_tipo_identificacion;
        $cuenta->cuenta_identificacion = $request->cuenta_identificacion;
        $cuenta->cuenta_primer_nombre = $request->cuenta_primer_nombre;
        $cuenta->cuenta_segundo_nombre = $request->cuenta_segundo_nombre;
        $cuenta->cuenta_primer_apellido = $request->cuenta_primer_apellido;
        $cuenta->cuenta_segundo_apellido = $request->cuenta_segundo_apellido;
        $cuenta->cuenta_email = $request->cuenta_email;
        $cuenta->cuenta_genero = $request->cuenta_genero;
        $cuenta->contrato_id = $request->contrato_id;
        $cuenta->subarea_id = $request->subarea_id;
        $cuenta->cargo_id = $request->cargo_id;
        $cuenta->sede_id = $request->sede_id;
        $cuenta->extension_id = $request->extension_id;
        $cuenta->cuenta_fecha_nacimiento = $request->cuenta_fecha_nacimiento;
        $cuenta->estado_id = '6'; //cambia a estado actualizar datos (en el servidor)
        $cuenta->cuenta_condiciones = $request->cuenta_condiciones;

        //guarda los cambios del registro
        $cuenta->save();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Actualización de mi cuenta '.$cuenta->cuenta_cuenta.' exitosa'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        //envia notificacion al email
        $bcc_recipients[0] = 'sia@unicatolica.edu.co';

        if(!empty($sistema->roles->where('rol_codigo', 'MAN')))
        {
          $recipients = $sistema->roles->where('rol_codigo', 'MAN')->first()->sistemaroles->where('sistema_id', $sistema->sistema_id)->first()->usuarios;
          
          if(!empty($recipients))
          {
            $i = 1;
            foreach ($recipients as $recipient) {
              $bcc_recipients[$i] = $recipient->email;
              ++$i;
            }
          }
        }
                    
        Mail::to([\Auth::user()->email])
        ->bcc($bcc_recipients)
        ->send(new NotificacionActualizarDatos($cuenta));

        Alert::info('<p>Los datos de su cuenta en <b>SIA</b> han sido actualizados exitosamente. Estos cambios también serán aplicados a su correo electrónico <b>'.$cuenta->cuenta_cuenta.'</b>, por el Departamento de Sistemas.</p>', 'Atención')->html()->persistent();

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('browse.correos.cuentas.account', $sistema);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sistema, $id)
    {
        $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->get();

        $sistema = Sistema::find($sistema);
        $cuenta = Cuenta::find($id);

        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Editar cuenta '.$cuenta->cuenta_cuenta.''),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        return view('browse.correos.cuentas.edit')
        ->with('sedes', $sedes)
        ->with('cargos', $cargos)
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('tipos', $tipos)
        ->with('subareas', $subareas)
        ->with('cuenta', $cuenta)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $sistema, $cuenta_id)
     {
        $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->get();
        $sistema = Sistema::find($sistema);

        $cuenta = Cuenta::find($cuenta_id);
        $cuenta->cuenta_codigo = $request->cuenta_identificacion;
        $cuenta->cuenta_tipo_identificacion = $request->cuenta_tipo_identificacion;
        $cuenta->cuenta_identificacion = $request->cuenta_identificacion;
        $cuenta->cuenta_primer_nombre = $request->cuenta_primer_nombre;
        $cuenta->cuenta_segundo_nombre = $request->cuenta_segundo_nombre;
        $cuenta->cuenta_primer_apellido = $request->cuenta_primer_apellido;
        $cuenta->cuenta_segundo_apellido = $request->cuenta_segundo_apellido;
        $cuenta->cuenta_email = $request->cuenta_email;
        $cuenta->cuenta_genero = $request->cuenta_genero;
        $cuenta->contrato_id = $request->contrato_id;
        $cuenta->subarea_id = $request->subarea_id;
        $cuenta->cargo_id = $request->cargo_id;
        $cuenta->sede_id = $request->sede_id;
        $cuenta->extension_id = $request->extension_id;
        $cuenta->cuenta_fecha_nacimiento = $request->cuenta_fecha_nacimiento;
        $cuenta->estado_id = '6'; //cambia a estado actualizar datos (en el servidor)
        $cuenta->cuenta_condiciones = $request->cuenta_condiciones;

        //guarda los cambios del registro
        $cuenta->update();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Actualización cuenta '.$cuenta->cuenta_cuenta.' exitosa'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);

        //envia notificacion al email
        $bcc_recipients[0] = 'sia@unicatolica.edu.co';

        if(!empty($sistema->roles->where('rol_codigo', 'MAN')))
        {
          $recipients = $sistema->roles->where('rol_codigo', 'MAN')->first()->sistemaroles->where('sistema_id', $sistema->sistema_id)->first()->usuarios;
          
          if(!empty($recipients))
          {
            $i = 1;
            foreach ($recipients as $recipient) {
              $bcc_recipients[$i] = $recipient->email;
              ++$i;
            }
          }
        }
                    
        Mail::to($cuenta->cuenta_cuenta)
        ->bcc($bcc_recipients)
        ->send(new NotificacionActualizarDatos($cuenta));

        Alert::info('<p>Los datos de su cuenta en <b>SIA</b> han sido actualizados exitosamente. Estos cambios también serán aplicados a su correo electrónico <b>'.$cuenta->cuenta_cuenta.'</b>, por el Departamento de Sistemas.</p>', 'Atención')->html()->persistent();

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('browse.correos.cuentas.index', $sistema);
    }

    public function requests(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $cuentas = Cuenta::orderBy('created_at', 'ASC')
        //->Filtro($request->filtro)
        ->paginate(25);

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista solicitudes cuentas'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);


        return view('browse.correos.cuentas.requests')
        ->with('cuentas', $cuentas)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    public function process(Sistema $sistema, Cuenta $cuenta, $estado)
    {
        $estado_anterior = $cuenta->estado_id;

        $cuenta->estado_id = $estado; //cambia el estado de la solicitud de la cuenta

        //guarda los cambios del registro
        $cuenta->save();

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Solicitud cuenta '.$cuenta->cuenta_cuenta.' procesada'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        //si el estado de la cuenta cambia a Restablecer Contraseña
        if($cuenta->estado_id == 3)
        {
          //envía notificación al email
          Mail::to([$cuenta->cuenta_email, \Auth::user()->email])
          ->bcc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionRestablecerContrasena($cuenta));

          //muestra ventana modal de alerta
          Alert::success('<p>Se ha solicitado restablecer la contraseña de la cuenta <b>'.$cuenta->cuenta_cuenta.'</b>. Un correo de confirmación fue enviado al correo <b>'.$cuenta->cuenta_email.'</b>.</p>', '¡Muy bien!')->html()->persistent();
        }
        elseif($estado_anterior == 3)
        {
          //envia notificacion al email institucional del colaborador
          Mail::to($cuenta->cuenta_email)
          ->bcc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionRestablecimientoContrasena($cuenta));
        }        

        //muestra la lista de cuentas de la tabla sia_cuentas
        //return redirect()->route('browse.correos.cuentas.index', $sistema);
        return redirect(url()->previous());
    }

    public function processRequest(Request $request, Sistema $sistema, Cuenta $cuenta, $estado)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        if($cuenta->estado_id == 1) //si el estado actual es: procesamiento.
        {
          //envia notificacion al email personal del colaborador
          Mail::to($cuenta->cuenta_email)
          ->bcc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionProcesarSolicitud($cuenta));
        }
        elseif($cuenta->estado_id == 3) //si el estado actual es: restablecer contraseña.
        {
          //envia notificacion al email institucional del colaborador
          Mail::to($cuenta->cuenta_email)
          ->bcc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionRestablecimientoContrasena($cuenta));
        }
        elseif($cuenta->estado_id == 6) //si el estado actual es: actualizar datos.
        {
          //envia notificacion al email
          Mail::to($cuenta->cuenta_cuenta)
          ->bcc(['sia@unicatolica.edu.co'])
          ->send(new NotificacionActualizacionDatos($cuenta));
        }

        $cuenta->estado_id = $estado; //cambia el estado de la solicitud de la cuenta

        $cuenta->save(); //guarda los cambios del registro

        $sistema->entrances()->create([
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Solicitud cuenta '.$cuenta->cuenta_cuenta.' procesada'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        
        Alert::info('<p>La solicitud de la cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido procesada exitosamente.</p>', 'Atención')->html()->persistent();

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('browse.correos.cuentas.requests', [$sistema, 'filtro' => $request->filtro]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

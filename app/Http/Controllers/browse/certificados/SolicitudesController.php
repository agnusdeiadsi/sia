<?php

namespace App\Http\Controllers\Browse\certificados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Alert;
use PDF;

use App\Sistema;
use App\Matriculado;
use App\Subarea;
use App\Semestre;
use App\Sede;
use App\Entrance;

use App\Model\Certificados\Tipo;
use App\Model\Certificados\Solicitud;
use App\Model\Certificados\Materia;

use App\Mail\Certificados\NotificacionNuevaSolicitud;
use App\Mail\Certificados\NotificacionProcesarSolicitud;

class SolicitudesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Sistema $sistema)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $solicitudes = Solicitud::join('certificados_tipos', 'certificados_solicitudes.tipo_id', '=', 'certificados_tipos.tipo_id')
        ->orderBy('created_at', 'ASC')
        ->where('estado_id', '=', $request->filtro)
        ->Filtrar($request->busqueda)
        ->paginate(25);

        if($request->filtro == 1)
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Vista solicitudes en Procesamiento'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->filtro == 2)
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Vista solicitudes: recibir certificado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->filtro == 3)
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Vista solicitudes: entregar certificado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->filtro == 4)
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Vista solicitudes: certificados entregados'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        else
        {
            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Vista historial de solicitudes'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }

        return view('browse.certificados.solicitudes.index')
        ->with('solicitudes', $solicitudes)
        ->with('sistema', $sistema)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sistema $sistema, Matriculado $matriculado)
    {   
        $programas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_codigo');
        $semestres = Semestre::orderBy('semestre_codigo', 'ASC')->pluck('semestre_nombre', 'semestre_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $sedes = Sede::orderBy('sede_nombre', 'ASC')->pluck('sede_nombre', 'sede_id');

        return view('estudiantes.certificados.solicitudes.create')
        ->with('programas', $programas)
        ->with('semestres', $semestres)
        ->with('tipos', $tipos)
        ->with('sedes', $sedes)
        ->with('matriculado', $matriculado)
        ->with('sistema', $sistema);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $this->validate($request, [
            'solicitud_codigo_liquidacion' => 'unique:certificados_solicitudes',
            'solicitud_comprobante_liquidacion' => 'required:certificados_solicitudes|file|mimes:pdf|max:2048',
            'solicitud_carnet_foto' => 'file|mimes:jpg,png|max:5120',
            'solicitud_carnet_documento' => 'file|mimes:pdf|max:2048',
            //'solicitud_condiciones' => 'required:certificados_solicitudes',
        ]);


        //valida y guarda los archivos adjuntos
        if($request->solicitud_carnet_foto && $request->solicitud_carnet_documento)
        {
            $foto_carnet = $request->file('solicitud_carnet_foto');
            $nombre_foto_carnet = time().'.'.$foto_carnet->getClientOriginalExtension();
            $path = public_path().'/files/certificados/carnets/';
            $foto_carnet->move($path, $nombre_foto_carnet);

            $documento_carnet = $request->file('solicitud_carnet_documento');
            $nombre_documento_carnet = time().'.'.$documento_carnet->getClientOriginalExtension();
            $path = public_path().'/files/certificados/carnets/';
            $documento_carnet->move($path, $nombre_documento_carnet);
        }

        $comprobante = $request->file('solicitud_comprobante_liquidacion');
        $nombre_comprobante = time().'.'.$comprobante->getClientOriginalExtension();
        $path = public_path().'/files/certificados/comprobantes/';
        $comprobante->move($path, $nombre_comprobante);


        //guarda la nueva solicitud
        $solicitud = new Solicitud($request->all());
        $solicitud->solicitud_id = \Auth::guard('estudiante')->user()->matriculado_id.''.time();
        $solicitud->solicitud_comprobante_liquidacion = $nombre_comprobante;
        $solicitud->solicitud_matriculado_tipo_documento = $request->solicitud_matriculado_tipo_documento;
        $solicitud->solicitud_matriculado_documento = $request->solicitud_matriculado_documento;
        $solicitud->solicitud_matriculado_fecha_nacimiento = $request->solicitud_matriculado_fecha_nacimiento;
        $solicitud->solicitud_matriculado_programa = $request->solicitud_matriculado_programa;
        $solicitud->solicitud_matriculado_tipo = $request->solicitud_matriculado_tipo;
        $solicitud->solicitud_carnet_denuncio = $request->solicitud_carnet_denuncio;
        $solicitud->solicitud_pension_valor = $request->solicitud_pension_valor;
        $solicitud->solicitud_pension_entidad = $request->solicitud_pension_entidad;
        $solicitud->estado_id = 1; //procesamiento

        if($request->solicitud_carnet_foto && $request->solicitud_carnet_documento)
        {
            $solicitud->solicitud_carnet_foto = $nombre_foto_carnet;
            $solicitud->solicitud_carnet_documento = $nombre_documento_carnet;
        }


        $solicitud->save();

        if($request->materia_nombre)
        {
            $materias = new Materia([ 'materia_nombre' => $request->materia_nombre]);
            $solicitud->materias()->save($materias);   

        }

        //envia notificacion al email
        Mail::to($request->solicitud_matriculado_email)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionNuevaSolicitud($solicitud));

        //mensaje
        flash()->overlay('La solicitud ha sido guardada correctamente. ID '.$solicitud->solicitud_id.'. Un correo de confirmación fue enviado al correo: '.$solicitud->solicitud_matriculado_email);

        //redirecciona al formulario de creación
        //return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sistema $sistema, Solicitud $solicitud)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $solicitud_estado = $solicitud->estado;

        //guarda los registros del usuario en sia_entrances
        $sistema->entrances()->create([
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Ver solicitud N° '.$solicitud->solicitud_id),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                  ]);
    

        return view('browse.certificados.solicitudes.show')
        ->with('sistema', $sistema)
        ->with('solicitud', $solicitud)
        ->with('sistemas', $sistemas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Solicitud $solicitud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitud $solicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitud $solicitud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        //si e suna solicitud de estudio
        if($solicitud->tipo_id == 1)
        {
            $datos = DB::connection('banner')->table('AAVCEST_V3')->where('CEST_PIDM', '136033')->orderBy('cest_periodo', 'ASC')->get();

            $pdf = PDF::loadView('browse.certificados.plantillas.estudio', [
                'solicitud' => $solicitud,
                'datos' => $datos,
            ]);

            return $pdf->setPaper('letter', 'portrait')->download($solicitud->solicitud_id.'.pdf'); 
        }
        else
        {
            dd($solicitud);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request, Sistema $sistema, Solicitud $solicitud)
    {
        if($request->estado == 2)
        {
            $solicitud->solicitud_usuario_imprime = \Auth::user()->id;
            $solicitud->solicitud_fecha_impresion = date('Y-m-d H:i:s');
            $solicitud->estado_id = $request->estado; //2: Transporte

            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Solicitud N° '.$solicitud->solicitud_id.' procesada'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->estado == 3)
        {
            $solicitud->solicitud_usuario_recibe = \Auth::user()->id;
            $solicitud->solicitud_fecha_recibido = date('Y-m-d H:i:s');
            $solicitud->estado_id = $request->estado; //3: Sede

            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Certificado de la Solicitud N° '.$solicitud->solicitud_id.' recibido en sede'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }
        elseif($request->estado == 4)
        {
            $solicitud->solicitud_usuario_entrega = \Auth::user()->id;
            $solicitud->solicitud_fecha_entrega = date('Y-m-d H:i:s');
            $solicitud->solicitud_observaciones_entrega = $request->solicitud_observaciones_entrega;
            $solicitud->solicitud_reclamante_documento = $request->solicitud_reclamante_documento;
            $solicitud->solicitud_reclamante_nombres = $request->solicitud_reclamante_nombres;
            $solicitud->solicitud_reclamante_carta = $request->solicitud_reclamante_carta;
            $solicitud->estado_id = $request->estado; //4: Entregado

            //guarda los registros del usuario en sia_entrances
            $sistema->entrances()->create([
                                        'usuario_id' => \Auth::user()->id,
                                        'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                        'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                        'entrance_url_anterior' => url()->previous(),
                                        'entrance_url_actual' => url()->current(),
                                        'entrance_accion' => nl2br('Certificado de la Solicitud N° '.$solicitud->solicitud_id.' entregado'),  
                                        'entrance_startdate' => date('Y-m-d H:i:s'),
                                      ]);
        }

        $solicitud->save();

        //envia notificacion al email
        Mail::to($solicitud->solicitud_matriculado_email)
        ->cc(['sia@unicatolica.edu.co'])
        ->send(new NotificacionProcesarSolicitud($solicitud));

        flash()->overlay('La solicitud <b>'.$solicitud->solicitud_id.'</b> ha sido procesada. Un correo de notificación fue enviado al estudiante.', 'Atención');

        return redirect()->route('browse.certificados.solicitudes', [$sistema, 'filtro' => ($request->estado-1)]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;

use App\Contrato;

class ContratoController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contratos = Contrato::orderBy('contrato_nombre')->paginate(25);

        return view('browse.site.contratos.index')
        ->with('contratos', $contratos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('browse.site.contratos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contrato_codigo' => 'unique:sia_contratos',
        ]);

        $contrato = new Contrato($request->all());
        $contrato->save();


        Alert::success('<p>El tipo de contrato <b>'.$contrato->contrato_nombre.'</b> ha sido creado exitosamente.</p>', '¡Muy bien!')->html()->persistent();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contrato $contrato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contrato $contrato)
    {
        return view('browse.site.contratos.edit')
        ->with('contrato', $contrato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contrato $contrato)
    {
        $contrato->update($request->all());

        Alert::info('<p>El tipo contrato ha sido actualizado exitosamente.</p>', 'Atención')->html()->persistent();
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contrato $contrato)
    {
        //
    }
}

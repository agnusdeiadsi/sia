<?php

namespace App\Http\Controllers\EstudianteAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Alert;

use App\Model\Banner\BMatriculado;
use App\Matriculado;
use App\Identificacion;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/estudiantes/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('estudiante.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('estudiantes.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'identificacion_numero' => 'required|string',
        ]);

        $usuario = BMatriculado::where('pers_num_doc', '=', $request->identificacion_numero)->get();

        if(count($usuario) > 0)
        {   
            $matriculado = Matriculado::find($usuario->first()->pers_pidm);

            if(empty($matriculado))
            {
                $apellidos = explode(' ', $usuario->first()->pers_ape);

                Matriculado::create(['matriculado_pidm' => $usuario->first()->pers_pidm,
                                                    'matriculado_primer_nombre' => $usuario->first()->pers_nom1,
                                                    'matriculado_segundo_nombre' => $usuario->first()->pers_nom2,
                                                    'matriculado_primer_apellido' => $apellidos[0],
                                                    'matriculado_segundo_apellido' => $apellidos[1]]);

                Identificacion::create(['matriculado_pidm' => $usuario->first()->pers_pidm,
                                        'identificacion_tipo' => $usuario->first()->pers_tipo_doc,
                                        'identificacion_numero' => $usuario->first()->pers_num_doc]);
            }

            if(Auth::guard('estudiante')->loginUsingId($usuario->first()->pers_pidm, true))
            {

                return redirect()->intended('/estudiantes/home');
            }
            else
            {
                //Flash::message('Usuario no encontrado. Vuelva a intentarlo.', 'danger');
                Alert::info('Usuario no encontrado. Vuelva a intentarlo.')->html()->persistent('Intentar de Nuevo');
                
                return redirect()->intended('/estudiantes/login');
            }
        }
        else
        {
            Alert::error('<p>Usuario no encontrado. Vuelva a intentarlo.</p>')->html()->persistent('Aceptar');

            return redirect()->back();
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('estudiante');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard('estudiante')->logout();

        /*$request->session()->flush();

        $request->session()->regenerate();*/

        return redirect('/estudiantes');
    }
}

<?php

namespace App\Http\Controllers\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Alert;

use App\Model\Banner\BMatriculado;

class BannerController extends Controller
{
    public function fetchMatriculados()
    {
    	//recupera los registros de matriculados desde banner
    	$matriculados = DB::connection('banner')->select('select * from aavpers_v3');

    	$recuperados = count($matriculados);

    	//elimina todos los registros de la tabla sia_banner_matriculados
    	$delete = DB::connection('sia')->table('sia_banner_matriculados')->delete();

    	$total = 0;

    	foreach($matriculados as $matriculado)
    	{
    		//inserta los registros de matriculados recuperados desde banner
    		$nuevo = DB::connection('sia')->table('sia_banner_matriculados')->insert([
    			'matriculado_pidm' => $matriculado->pers_pidm, 
    			'matriculado_codigo' => $matriculado->pers_id,
    			'matriculado_apellidos' => $matriculado->pers_ape,
    			'matriculado_primer_nombre' => $matriculado->pers_nom1,
    			'matriculado_segundo_nombre' => $matriculado->pers_nom2,
    			'matriculado_tipo_documento' => $matriculado->pers_tipo_doc,
    			'matriculado_documento' => $matriculado->pers_num_doc,
    			'matriculado_lugar_expedicion' => $matriculado->pers_cod_ciu_expdoc,
    			'matriculado_fecha_expedicion' => $matriculado->pers_fec_expdoc,
    			'matriculado_estrato' => $matriculado->pers_estrato,
    			//'matriculado_sisben' => $matriculado->pers_sisben,
    		]);

    		//dd($matriculado->pers_pidm);
    		//$nuevo = new BMatriculado($matriculado);
    		//$nuevo->save();

    		//dd($nuevo); //devuelve TRUE

    		if($nuevo == true)
    		{
    			++$total;
    		}

    		//echo $matriculado->pers_pidm." - ".$matriculado->pers_id."<br>";
    		set_time_limit(5);
    	}

    	Alert::success('<p>Los matriculados han sido importados.<br>Total registros importados '.$total.'/'.$recuperados.'</p>')->html()->persistent('Aceptar');

    	return redirect()->back();
    }
}

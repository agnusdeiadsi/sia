<?php

namespace App\Http\Controllers;

use App\Entrance;
use Illuminate\Http\Request;

class EntranceController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entrance  $entrance
     * @return \Illuminate\Http\Response
     */
    public function show(Entrance $entrance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entrance  $entrance
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrance $entrance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entrance  $entrance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrance $entrance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entrance  $entrance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrance $entrance)
    {
        //
    }
}

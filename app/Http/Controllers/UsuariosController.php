<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Laracasts\Flash\Flash;
use Alert;

//modelos
use App\Usuario;
use App\Sistema;
use App\Entrance;
use App\Area;
use App\Subarea;
use App\Contrato;
use App\Extension;
use App\Rol;
use App\Model\Correos\Cuenta;
use App\Model\Correos\Tipo;
use App\Model\Correos\Estado;
use App\CargoUnicatolica as Cargo;
use App\Sede;

//requests
use App\Http\Requests\UsuarioRequest;

class UsuariosController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    public function index()
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

    	$usuarios = Usuario::where('sia_usuarios.id', '!=', \Auth::user()->id)
        ->orderBy('sia_usuarios.id', 'ASC')
        ->join('app_correos.correos_cuentas', 'sia_usuarios.id', '=', 'app_correos.correos_cuentas.cuenta_id')
        ->paginate(25);

        $sistema = Sistema::where('sistema_nombre_corto', 'correos')->get()->first();

        $entrance = new Entrance([
                              'sistema_id' => null, 
                              'usuario_id' => \Auth::user()->id,
                              'usuario_rol_sia' => \Auth::user()->rol_sia, 
                              'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                              'entrance_url_anterior' => url()->previous(),
                              'entrance_url_actual' => url()->current(),
                              'entrance_accion' => nl2br('Vista usuarios SIA'),  
                              'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

    	return view('browse.site.usuarios.index')
        ->with('sistema', $sistema)
        ->with('usuarios', $usuarios)
        ->with('sistemas', $sistemas);
    }

     public function activate(Cuenta $cuenta)
    {
        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Activar usuario '.$cuenta->cuenta_cuenta),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        return view('browse.site.usuarios.activate')
        ->with('cuenta', $cuenta);
    }

    public function storeActivation(Request $request)
    {
        $this->validate($request, [
          'id' => 'unique:sia_usuarios',
        ]);

        $usuario = new Usuario($request->all());
        $usuario->password = bcrypt('system2000');
        $usuario->save();

        Alert::success('<p>El usuario <b>'.$request->cuenta_cuenta.'</b> ha sido Activado en SIA.<br>Un correo ha sido enviado al usuario con los datos de acceso.</p>', '¡Muy bien!')->html()->persistent();

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Activación del usuario '.$usuario->email.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        $sistema = Sistema::where('sistema_nombre_corto', 'correos')->get()->first();

        return redirect()->route('browse.correos.cuentas.index', $sistema);
    }

    public function edit(Usuario $usuario)
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $usuario_cuenta = $usuario->cuenta;

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar usuario '.$usuario->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

    	return view('browse.site.usuarios.edit')
        ->with('usuario', $usuario)
        ->with('sistemas', $sistemas);
    }

    public function update(Request $request, $id)
    {
    	$usuario = Usuario::find($id);
    	$usuario->rol_sia = $request->rol_id;
      //$usuario->usuario_password = sha1('system2000');
    	$usuario->update();

    	//muestra mensaje de alerta
    	Alert::info('<p>El usuario <b>'.$usuario->cuenta->cuenta_cuenta.'</b> ha sido actualizado exitosamente.</p>', 'Atención')->html()->persistent();

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Actualización del usuario '.$usuario->email.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

    	//muestra la lista de usuarios de la tabla sia_usuarios
    	return redirect()->route('browse.site.usuarios.index');
    }

    public function destroy($id)
    {
    	//crea objeto usuario y busca el usuario en la tabla sia_usuarios
    	$usuario = Usuario::find($id);

    	//$usuario->delete(); //elimina el usuario de la tabla sia_usuarios

    	//muestra alerta
    	Flash::message('<div class="w3-content">
						<div class="w3-panel w3-pale-yellow w3-leftbar w3-border-orange">
						<p class="w3-padding"><br />El usuario <b>'.$usuario->usuario_email.'</b> ha sido eliminado correctamente.</p>
						</div>
						</div>', null);
    	//muestra la lista de usuarios de la tabla sia_usuarios
    	return redirect()->route('browse.site.usuarios.index');
    }

    public function permisos(Usuario $usuario)
    {
        //$permisos = $usuario->permisos->pluck('sistema_id')->ToArray();
        $permisos = $usuario->enrolamientos->pluck('sistema_id')->ToArray();
        $cuenta = $usuario->cuenta;

        $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->pluck('sistema_id')->ToArray();

        foreach ($sistemas as $sistema) {
            $objSistema = Sistema::find($sistema);
            $sistema_rol = $objSistema->roles->ToArray();
            $sistemas_roles[] = array('sistema' => $objSistema, 'rol' => $sistema_rol); //arreglo de los sistemas y sus respectivos roles
        }

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar permisos usuario '.$usuario->cuenta->cuenta_cuenta),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        return view('browse.site.usuarios.permisos')
        ->with('usuario', $usuario)
        ->with('sistemas_roles', $sistemas_roles)
        ->with('sistemas', $sistemas);
    }

    public function updatepermisos(Request $request, Usuario $usuario)
    {
        $usuario->permisos()->sync($request->sistemarol_id);

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Actualización permisos usuario '.$usuario->cuenta->cuenta_cuenta.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        //muestra mensaje de alerta
        Alert::success('<p>Los permisos del usuario <b>'.$usuario->cuenta->cuenta_cuenta.'</b> han sido actualizados exitosamente.</p>', '¡Muy bien!')->html()->persistent('Cerrar');

        //redirecciona a permisos del usuario
        return redirect()->route('browse.site.usuarios.permisos', $usuario);
    }

    public function profile()
    {
        $sistemas = Sistema::orderBy('sistema_entrances', 'DESC')->get();

        $sistema = New Sistema(['sistema_colorclass' => '2017-navy-peony']);
        $cuenta = Cuenta::find(\Auth::user()->id);
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
        $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');

        $cargos = Cargo::orderBy('cargo_nombre', 'asc')->get();

        $sedes = Sede::orderBy('sede_nombre', 'asc')->get();

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Editar Mi Perfil '.$cuenta->cuenta_cuenta),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        return view('browse.site.usuarios.account')
        ->with('sedes', $sedes)
        ->with('cargos', $cargos)
        ->with('tipos', $tipos)
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('subareas', $subareas)
        ->with('cuenta', $cuenta)
        ->with('sistemas', $sistemas);
    }

    public function updateProfile(Request $request)
    {
        $cuenta = Cuenta::find(\Auth::user()->id);
        $cuenta->cuenta_codigo = $request->cuenta_identificacion;
        $cuenta->cuenta_tipo_identificacion = $request->cuenta_tipo_identificacion;
        $cuenta->cuenta_identificacion = $request->cuenta_identificacion;
        $cuenta->cuenta_primer_nombre = $request->cuenta_primer_nombre;
        $cuenta->cuenta_segundo_nombre = $request->cuenta_segundo_nombre;
        $cuenta->cuenta_primer_apellido = $request->cuenta_primer_apellido;
        $cuenta->cuenta_segundo_apellido = $request->cuenta_segundo_apellido;
        $cuenta->cuenta_email = $request->cuenta_email;
        $cuenta->cuenta_genero = $request->cuenta_genero;
        $cuenta->contrato_id = $request->contrato_id;
        $cuenta->subarea_id = $request->subarea_id;
        $cuenta->extension_id = $request->extension_id;
        $cuenta->estado_id = '6'; //cambia a estado actualizar datos (en el servidor)
        $cuenta->cuenta_condiciones = $request->cuenta_condiciones;

        //guarda los cambios del registro
        $cuenta->save();

        //muestra ventana modal de alerta
        Alert::info('<p>Los datos de su cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido actualizados correctamente. Éstos serán actualizados en el servidor de UNICATÓLICA por el Departamento de Sistemas y le llegará un correo de notificación de esta acción en un estimado de 72 horas hábiles o más.</p>', 'Atención')->html()->persistent();

        $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Actualización perfil '.$cuenta->cuenta_cuenta.' exitosa'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
        $entrance->save();

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('browse.site.usuarios.perfil');
    }

    public function password(Request $request)
    {
        $sistema = null;
        if (Hash::check($request->oldpassword, \Auth::user()->password)) {
            if($request->confirmpassword == $request->password)
            {
                if($request->password == $request->oldpassword)
                {
                    $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Error al intentar cambiar la contraseña. Cuenta '.\Auth::user()->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
                    $entrance->save();

                    Alert::info('<p>La nueva contraseña no puede ser igual a la contraseña actual. Inténtelo nuevamente.</p>', 'Atención')->html()->persistent();
                }
                else
                {
                    Alert::info('<p>La contraseña ha sido actualizada correctamente.</p>', 'Atención')->html()->persistent();

                    $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Contraseña cuenta '.\Auth::user()->email.' actualizada'),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
                    $entrance->save();

                    //guarda la nueva contraseña
                    \Auth::user()->fill([
                        'password' => Hash::make($request->password)
                    ])->save();
                }

                return redirect()->route('browse.site.usuarios.perfil');
            }
            else
            {
                Alert::warning('<p>La nueva contraseña no coincide con la confirmación. Inténtelo nuevamente.</p>', 'Atención')->html()->persistent();

                $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Error al intentar cambiar la contraseña. Cuenta '.\Auth::user()->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
                $entrance->save();

                return redirect()->route('browse.site.usuarios.perfil');
            }        
        }else
        {
            Alert::error('<p>La contraseña actual ingresada está errada. Inténtelo nuevamente.</p>', 'Error')->html()->persistent();

            $entrance = new Entrance([
                                    'sistema_id' => null, 
                                    'usuario_id' => \Auth::user()->id,
                                    'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                    'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                    'entrance_url_anterior' => url()->previous(),
                                    'entrance_url_actual' => url()->current(),
                                    'entrance_accion' => nl2br('Error al intentar cambiar la contraseña. Cuenta '.\Auth::user()->email),  
                                    'entrance_startdate' => date('Y-m-d H:i:s'),
                                ]);
            $entrance->save();

            return redirect()->route('browse.site.usuarios.perfil');
        }
    }

    public function entrances(Request $request, Usuario $usuario)
    {
        $entrances = Entrance::orderBy('entrance_startdate', 'desc')->where('usuario_id', '=', $usuario->id)->paginate(100);

        $entrance = new Entrance([
                                'sistema_id' => null, 
                                'usuario_id' => \Auth::user()->id,
                                'usuario_rol_sia' => \Auth::user()->rol_sia, 
                                'usuario_rol_modulo' => \Auth::user()->rol_modulo,
                                'entrance_url_anterior' => url()->previous(),
                                'entrance_url_actual' => url()->current(),
                                'entrance_accion' => nl2br('Ver entrances usuario '.$usuario->email.' página '.$request->page.'.'),  
                                'entrance_startdate' => date('Y-m-d H:i:s'),
                            ]);
        $entrance->save();

        return view('browse.site.usuarios.entrances')
        ->with('entrances', $entrances)
        ->with('usuario', $usuario);
    }
}

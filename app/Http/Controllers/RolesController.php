<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Rol;

class RolesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('browse');
    }
    
    public function index()
    {
    	$roles = Rol::orderBy('rol_nombre', 'ASC')->paginate(25);
    	
        return view('browse.site.roles.index')->with('roles', $roles);
    }

    public function create()
    {
    	return view('browse.site.roles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'rol_codigo' => 'unique:sia_roles'
        ]);

    	$rol = new Rol($request->all());
    	$rol->save();

    	Flash::message('<div class="w3-panel w3-pale-green w3-leftbar w3-border-green">
						<p class="w3-padding"><br />El rol <b>'.$rol->rol_nombre.'</b> ha sido registrado correctamente.</p>
						</div>', null);
    	return redirect()->route('roles.create');
    }

    public function edit($id)
    {
    	$rol = Rol::find($id);
    	return view('site.roles.edit')->with('datos', $rol);
    }

    public function update(Request $request, $id)
    {
    	$rol = Rol::find($id);
    	$rol->rol_codigo = $request->rol_codigo;
    	$rol->rol_nombre = $request->rol_nombre;
    	$rol->rol_descripcion = $request->rol_descripcion;
    	$rol->save();

    	//muestra alerta de exito
    	Flash::message('<div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
						<p class="w3-padding"><br />El rol <b>'.$rol->rol_nombre.'</b> ha sido actualizado correctamente.</p>
						</div>', null);

    	//muestra la lista de roles de la tabla sia_usuarios
    	return redirect()->route('site.roles.index');


    }

    public function destroy($id)
    {
    	//crea objeto usuario y busca el usuario en la tabla sia_usuarios
    	$rol = Rol::find($id);

    	//$usuario->delete(); //elimina el usuario de la tabla sia_usuarios

    	//muestra alerta
    	Flash::message('<div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
						<p class="w3-padding"><br />El rol <b>'.$rol->rol_nombre.'</b> ha sido eliminado correctamente.</p>
						</div>', null);
    	//muestra la lista de usuarios de la tabla sia_usuarios
    	return redirect()->route('site.roles.index');
    }
}

<?php

namespace App\Http\Controllers\Site\Correos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CorreosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('site.correos.index'); //menú de Correos
    }
}

<?php

namespace App\Http\Controllers\Site\Correos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

//modelos
use App\Model\Correos\Estado;

class EstadosController extends Controller
{
    public function menu() // muestra el menu de tipos de correos
    {
        return view('site.correos.estados.menu');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::orderBy('estado_nombre', 'ASC')->paginate(5);

        return view('site.correos.estados.index')->with('lista', $estados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.correos.estados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //crea un nuevo objeto Tipo y envia a traves del constructor el arreglo recibido en store
        $estado = new Estado($request->all());
        $estado->save();

        //muestra mensaje de exito
        Flash::success('<div class="w3-container">
                        <div class="w3-panel w3-pale-green w3-leftbar w3-border-green">
                        <h3>Alerta</h3>
                        <p>El nuevo estado de correo <b>'.$estado->estado_nombre.'</b> ha sido registrado correctamente.</p>
                        </div>
                        </div>');
        // retorna al formulario para crear un nuevo tipo
        return view('site.correos.estados.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $estado = Estado::find($id);

        //retorna el formulario de edición
        return view('site.correos.estados.edit')
        ->with('datos', $estado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = estado::find($id);
        $estado->estado_nombre = $request->estado_nombre;
        $estado->estado_descripcion = $request->estado_descripcion;

        //guarda los cambios del registro
        $estado->save();

        //muestra alerta de exito
        Flash::success('<div class="w3-container">
                        <div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
                        <h3>Alerta</h3>
                        <p>El estado de correo <b><i>'.$estado->estado_nombre.'</i></b> ha sido actualizado correctamente.</p>
                        </div>
                        </div>');

        //retrona el listado de registro
        return redirect()->route('estados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

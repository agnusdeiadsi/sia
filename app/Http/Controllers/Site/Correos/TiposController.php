<?php

namespace App\Http\Controllers\Site\Correos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
//modelos
use App\Model\Correos\Tipo;

class TiposController extends Controller
{

    public function menu() // muestra el menu de tipos de correos
    {
        return view('site.correos.tipos.menu');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->paginate(10);

        return view('site.correos.tipos.index')->with('lista', $tipos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.correos.tipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //crea un nuevo objeto Tipo y envia a traves del constructor el arreglo recibido en store
        $tipo = new Tipo($request->all());
        $tipo->save();

        //muestra mensaje de exito
        Flash::success('<div class="w3-content">
                        <div class="w3-panel w3-pale-green w3-leftbar w3-border-green">
                        <p class="w3-padding"><br />El nuevo tipo de correo <b>'.$tipo->tipo_nombre.'</b> ha sido registrado correctamente.</p>
                        </div>
                        </div>');
        // retorna al formulario para crear un nuevo tipo
        return view('site.correos.tipos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo = Tipo::find($id);

        //retorna el formulario de edición
        return view('site.correos.tipos.edit')
        ->with('datos', $tipo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $tipo = Tipo::find($id);
        $tipo->tipo_nombre = $request->tipo_nombre;
        $tipo->tipo_descripcion = $request->tipo_descripcion;

        //guarda los cambios del registro
        $tipo->save();

        //muestra alerta de exito
        Flash::message('<div class="w3-content">
                        <div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
                        <p class="w3-padding"><br />El tipo de correo <b><i>'.$tipo->tipo_nombre.'</i></b> ha sido actualizado correctamente.</p>
                        </div>
                        </div>', null);

        //retrona el listado de registro
        return redirect()->route('site.correos.tipos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

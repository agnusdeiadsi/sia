<?php

namespace App\Http\Controllers\Site\Correos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

//modelos
use App\Model\Correos\Cuenta;
use App\Model\Correos\Tipo;
use App\Model\Correos\Estado;

//SIA
use App\Subarea;
use App\Contrato;
use App\Extension;

class CuentasController extends Controller
{
    public function index() // lista los registros de la tabla correos_cuentas
    {
      $cuentas = Cuenta::orderBy('cuenta_primer_apellido', 'ASC')
      ->join('correos_estados', 'correos_cuentas.estado_id', '=', 'correos_estados.estado_id')
      ->join('correos_tipos', 'correos_cuentas.tipo_id', '=', 'correos_tipos.tipo_id')
      ->paginate(5);

      return view('site.correos.cuentas.index')->with('cuentas', $cuentas);
    }

    public function menu() // muestra el menu de cuentas
    {
      return view('site.correos.cuentas.menu');
    }

    public function menutipo() // muestra el menu de cuentas
    {
      return view('site.correos.cuentas.menutipo');
    }

    public function create()
    {
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');
      $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
      $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
      $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');

      //carga formulario de creación
      return view('site.correos.cuentas.create')
      ->with('tipos', $tipos)
      ->with('contratos', $contratos)
      ->with('extensiones', $extensiones)
      ->with('subareas', $subareas);
    }

    public function store(Request $request)
    {
      $array1 = array('ñ','á','é','í','ó','ú','à','è','ì','ò','ù','â','ê','î','ô','û','ä','ë','ï','ö','ü','Ñ','Á','É','Í','Ó','Ú','À','È','Ì','Ò','Ù','Â','Ê','Î','Ô','Û','Ä','Ë','Ï','Ö','Ü');
	    $array2 = array('n','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','N','A','E','I','O','U','A','E','I','O','U','A','E','I','O','U','A','E','I','O','U');

      //reemplaza los caracteres especiales introducidos en el formulario para los nombres y apellidos
      $primer_nombre = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_primer_nombre)));
    	$segundo_nombre = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_segundo_nombre)));
    	$primer_apellido = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_primer_apellido)));
    	$segundo_apellido = trim(strtoupper(str_replace($array1, $array2, $request->cuenta_segundo_apellido)));

      //toma los letras iniciales del primer y segundo nombre y segundo apellido y las pasa a minusculas
      $inicial_primer_nombre = strtolower(substr($primer_nombre, 0, 1));
	    $inicial_segundo_nombre = strtolower(substr($segundo_nombre, 0, 1));
	    $inicial_segundo_apellido = strtolower(substr($segundo_apellido, 0, 1));

      //pasa a minusculas el primer apellido
      $primer_apellido_lower = strtolower($primer_apellido);

      //posibles combinaciones de usuarios para el nuevo registro
      $opcion_1 = $inicial_primer_nombre.$primer_apellido_lower;
      $opcion_2 = $inicial_primer_nombre.$inicial_segundo_nombre.$primer_apellido_lower;
      $opcion_3 = $inicial_primer_nombre.$primer_apellido_lower.$inicial_segundo_apellido;
      $opcion_4 = $inicial_primer_nombre.$inicial_segundo_nombre.$primer_apellido_lower.$inicial_segundo_apellido;

      //se crea el arreglo con las posibles combinaciones de usuarios
      $opciones = [$opcion_1, $opcion_2, $opcion_3, $opcion_4];

      //valida si existen las posibles combinaciones para crear una nueva cuenta en la base de datos
      $cuenta_usuario = null;

      $usuario = new Cuenta();
      for ($i=0; $i < 4; $i++) {

        $existe = $usuario->where('cuenta_usuario', '=', ''.$opciones[$i].'')->get()->all();

        if(empty($existe) || $existe == null)
        {
          $cuenta_usuario = $opciones[$i];
        }
      }

      //si hay usuarios disponibles entonces guarda la nueva cuenta y retorna al formulario de creacion de nueva cuenta
      if($cuenta_usuario != null)
      {
      	$cuenta = new Cuenta($request->all());
        $cuenta->cuenta_usuario = $cuenta_usuario;
        $cuenta->cuenta_cuenta = $cuenta_usuario."@unicatolica.edu.co";
        $cuenta->estado_id = 1; //estado procesamiento
        $cuenta->tipo_id = 2; //correo docente
      	$cuenta->save();

      	Flash::message('<div class="w3-content">
  						<div class="w3-panel w3-pale-green w3-leftbar w3-border-green">
  						<p class="w3-padding"><br />La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido registrada correctamente.</p>
  						</div>
  						</div>', null);
      }
      //si no hay usuarios disponibles entonces muestra mensaje de alerta y retorna al formulario de creacion de nueva cuenta
      else {
        Flash::message('<div class="w3-content">
  						<div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
  						<p class="w3-padding"><br />No es posible crear la cuenta debido a que no existen usuarios disponibles.</p>
  						</div>
  						</div>', null);
      }

      //retorna al formulario de crear cuenta
      $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
      $extensiones = Extension::orderBy('extension_id', 'ASC')->pluck('extension_extension', 'extension_id');
      $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
      $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');

      return view('site.correos.cuentas.create')
      ->with('contratos', $contratos)
      ->with('extensiones', $extensiones)
      ->with('tipos', $tipos)
      ->with('subareas', $subareas);
    }

    public function edit($id)
    {
        $cuenta = Cuenta::find($id);

        $contratos = Contrato::orderBy('contrato_nombre', 'ASC')->pluck('contrato_nombre', 'contrato_id');
        $extensiones = Extension::orderBy('extension_extension', 'ASC')->pluck('extension_extension', 'extension_id');
        $tipos = Tipo::orderBy('tipo_nombre', 'ASC')->pluck('tipo_nombre', 'tipo_id');
        $subareas = Subarea::orderBy('subarea_nombre', 'ASC')->pluck('subarea_nombre', 'subarea_id');

        //dd($cuenta, $contratos);

        return view('site.correos.cuentas.edit')
        ->with('contratos', $contratos)
        ->with('extensiones', $extensiones)
        ->with('tipos', $tipos)
        ->with('subareas', $subareas)
        ->with('cuenta', $cuenta);
    }

    public function update(Request $request, $cuenta_id)
    {
        $cuenta = Cuenta::find($cuenta_id);

        $cuenta->cuenta_codigo = $request->cuenta_identificacion;
        $cuenta->cuenta_tipo_identificacion = $request->cuenta_tipo_identificacion;
        $cuenta->cuenta_identificacion = $request->cuenta_identificacion;
        $cuenta->cuenta_primer_nombre = $request->cuenta_primer_nombre;
        $cuenta->cuenta_segundo_nombre = $request->cuenta_segundo_nombre;
        $cuenta->cuenta_primer_apellido = $request->cuenta_primer_apellido;
        $cuenta->cuenta_segundo_apellido = $request->cuenta_segundo_apellido;
        $cuenta->cuenta_email = $request->cuenta_email;
        $cuenta->cuenta_genero = $request->cuenta_genero;
        $cuenta->contrato_id = $request->contrato_id;
        $cuenta->subarea_id = $request->subarea_id;
        $cuenta->extension_id = $request->extension_id;
        $cuenta->estado_id = '6'; //cambia a estado actualizar datos (en el servidor)
        $cuenta->cuenta_condiciones = $request->cuenta_condiciones;

        //guarda los cambios del registro
        $cuenta->save();

        //muestra mensaje de exito
        Flash::message('<div class="w3-content">
                        <div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
                        <p class="w3-padding"><br />La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido actualizada correctamente.</p>
                        </div>
                        </div>', null);

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('site.correos.cuentas.index');
    }

    public function process($cuenta_id, $estado_id)
    {
        $cuenta = Cuenta::find($cuenta_id);
        $cuenta->estado_id = $estado_id; //cambia el estado de la solicitud de la cuenta

        //guarda los cambios del registro
        $cuenta->save();

        //muestra mensaje de exito
        Flash::message('<div class="w3-content">
                        <div class="w3-panel w3-pale-blue w3-leftbar w3-border-blue">
                        <p class="w3-padding"><br />La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido procesada correctamente.</p>
                        </div>
                        </div>', null);

        //muestra la lista de cuentas de la tabla sia_cuentas
        return redirect()->route('site.correos.cuentas.index');
    }

    public function destroy($cuenta_id)
    {
        $cuenta = Cuenta::find($cuenta_id); //crea objeto y busca el registro en el modelo
        $cuenta->delete(); //elimina el registro del modelo

        //muestra alerta
        Flash::success('<div class="w3-content">
                        <div class="w3-panel w3-pale-yellow w3-leftbar w3-border-orange">
                        <p class="w3-padding"><br />La cuenta <b>'.$cuenta->cuenta_cuenta.'</b> ha sido eliminada correctamente.</p>
                        </div>
                        </div>');

        //renorna el listado de registros
        return redirect()->route('cuentas.index');
    }
}

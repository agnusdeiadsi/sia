<?php

namespace App;

use App\Notifications\EstudianteResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Estudiante extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = 'estudiante';
    protected $connection = 'sia';//'banner';
    protected $primaryKey = 'matriculado_pidm'; //'pers_num_doc'; //'identificacion_numero';
    protected $table = 'sia_matriculados'; //'AAVPERS_V3'; //'sia_identificacion';
    protected $fillable = [
        //'pers_num_doc',
        'matriculado_documento',
        'remember_token',
        //'identificacion_id', 'identificacion_numero', 'matriculado_pidm',
    ];

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new EstudianteResetPassword($token));
    }

    /**
    * Estudiante con AAVPERS_V3 de banner
    * Uno a Uno
    *
    */

    public function persona()
    {
        return $this->hasOne('App\Model\Banner\BMatriculado', 'pers_pidm', 'matriculado_pidm');
    }
}

<?php

namespace App\Mail\Correos;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Model\Correos\Cuenta;

class NotificacionRestablecimientoContrasena extends Mailable
{
    use Queueable, SerializesModels;

    protected $cuenta;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Cuenta $cuenta)
    {
        $this->cuenta = $cuenta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('browse.correos.emails.resetedpassword')
        ->subject('Notificación Correos - Solicitud Restablecer Contraseña: Cuenta '.$this->cuenta->cuenta_cuenta)
        ->with('cuenta', $this->cuenta);
    }
}

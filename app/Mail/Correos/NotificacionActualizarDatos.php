<?php

namespace App\Mail\Correos;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Model\Correos\Cuenta;

class NotificacionActualizarDatos extends Mailable
{
    use Queueable, SerializesModels;

    protected $sistema;
    protected $cuenta;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Cuenta $cuenta)
    {
        $this->cuenta = $cuenta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('browse.correos.emails.updateaccount')
        ->subject('Notificación Correos - Solicitud Actualización de Datos')
        ->with('cuenta', $this->cuenta);
    }
}

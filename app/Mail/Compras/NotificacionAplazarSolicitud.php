<?php

namespace App\Mail\Compras;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//models
use App\Model\Compras\Solicitud;
use App\Model\Compras\Insumo;

use App\Area;
use App\Subarea;
use App\CentroCostos;
use App\CentroOperacion;
use App\Sistema;
use App\Proyecto;

class NotificacionAplazarSolicitud extends Mailable
{
    use Queueable, SerializesModels;

    protected $sistema;
    protected $solicitud;
    protected $comentario;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sistema $sistema, Solicitud $solicitud, $comentario)
    {
      $this->sistema = $sistema;
      $this->solicitud = $solicitud;
      $this->comentario = $comentario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $solicitud_centro_costos = $this->solicitud->centrosCostos;
        $solicitud_centro_operacion = $this->solicitud->centrosOperacion;
        $solicitud_insumo = $this->solicitud->insumos;

        return $this->markdown('browse.compras.emails.deny')
        ->subject('Notificación Compras - Solicitud de Requerimientos N° '.$this->solicitud->solicitud_id)
        ->with('comentario', $this->comentario)
        ->with('solicitud', $this->solicitud)
        ->with('sistema', $this->sistema);
    }
}

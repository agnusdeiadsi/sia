<?php

namespace App\Mail\Compras;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//models
use App\Model\Compras\Solicitud;
use App\Model\Compras\Insumo;

use App\Area;
use App\Subarea;
use App\CentroCostos;
use App\CentroOperacion;
use App\Sistema;
use App\Proyecto;

class SolicitudCompras extends Mailable
{
    use Queueable, SerializesModels;

    protected $solicitud;
    protected $sistema;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sistema $sistema, Solicitud $solicitud)
    {
        $this->sistema = $sistema;
        $this->solicitud = $solicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $solicitud_centro_costos = $this->solicitud->centrosCostos;
        $solicitud_centro_operacion = $this->solicitud->centrosOperacion;
        $solicitud_insumo = $this->solicitud->insumos;

        $areas = Area::get();
        $subareas = Subarea::get();
        $centros_costos = CentroCostos::get();
        $centros_operacion = CentroOperacion::get();
        $proyectos = Proyecto::get();

        return $this->view('browse.compras.solicitudes.mail')
        ->subject('Notificación Compras - Solicitud de Requerimientos N° '.$this->solicitud->solicitud_id)
        ->attach(asset('files/compras/presupuestos/'.$this->solicitud->solicitud_presupuesto))
        ->with('areas', $areas)
        ->with('subareas', $subareas)
        ->with('centros_costos', $centros_costos)
        ->with('centros_operacion', $centros_operacion)
        ->with('proyectos', $proyectos)
        ->with('sistema', $this->sistema)
        ->with('solicitud', $this->solicitud);
    }
}

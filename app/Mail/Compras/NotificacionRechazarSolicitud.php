<?php

namespace App\Mail\Compras;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//models
use App\Sistema;
use App\Model\Compras\Solicitud;

class NotificacionRechazarSolicitud extends Mailable
{
    use Queueable, SerializesModels;

    protected $sistema;
    protected $solicitud;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sistema $sistema, Solicitud $solicitud)
    {
        $this->sistema = $sistema;
        $this->solicitud = $solicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('browse.compras.emails.reject')
        ->subject('Notificación Compras - Solicitud de Requerimientos N° '.$this->solicitud->solicitud_id)
        ->with('sistema', $this->sistema)
        ->with('solicitud', $this->solicitud);
    }
}

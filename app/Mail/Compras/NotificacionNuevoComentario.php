<?php

namespace App\Mail\Compras;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Sistema;
use App\Model\Compras\Solicitud;

class NotificacionNuevoComentario extends Mailable
{
    use Queueable, SerializesModels;

    protected $sistema;
    protected $solicitud;
    protected $comentario;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sistema $sistema, Solicitud $solicitud, $comentario)
    {
        $this->sistema = $sistema;
        $this->solicitud = $solicitud;
        $this->comentario = $comentario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('browse.compras.emails.comentarios.new')
        ->subject('Notificación Compras - Solicitud de Requerimientos N° '.$this->solicitud->solicitud_id)
        ->with('comentario', $this->comentario)
        ->with('solicitud', $this->solicitud)
        ->with('sistema', $this->sistema);
    }
}

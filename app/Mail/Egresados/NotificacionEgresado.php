<?php

namespace App\Mail\Egresados;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionEgresado extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $year;

    public function __construct($year)
    {
        $this->year = $year;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('browse.egresados.emails.form')
                    ->subject('ASUNTO: Seguimiento '.$this->year.' año(s) después de grado');
    }
}

<?php

namespace App\Mail\Cancelaciones;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Sistema;
use App\Model\Cancelaciones\Solicitud;

class NotificacionNuevaSolicitud extends Mailable
{
    use Queueable, SerializesModels;

    protected $sistema;
    protected $solicitud;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sistema $sistema, Solicitud $solicitud)
    {
        $this->solicitud = $solicitud;
        $this->sistema = $sistema;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('browse.cancelaciones.emails.solicitudes.new')
        ->subject('Notificación Cancelaciones - Solicitud de Aplazamiento y Cancelación N° '.$this->solicitud->solicitud_id)
        ->with('solicitud', $this->solicitud)
        ->with('sistema', $this->sistema);
    }
}

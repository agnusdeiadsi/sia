<?php

namespace App\Mail\Certificados;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Model\Certificados\Solicitud;

class NotificacionProcesarSolicitud extends Mailable
{
    use Queueable, SerializesModels;

    protected $solicitud;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Solicitud $solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $solicitud_estado = $this->solicitud->estado;

        return $this->markdown('browse.certificados.emails.process')
        ->subject('Notificación Certificados - Solicitud de Certificados Académicos y Financieros N° '.$this->solicitud->solicitud_id)
        ->with('solicitud', $this->solicitud);
    }
}

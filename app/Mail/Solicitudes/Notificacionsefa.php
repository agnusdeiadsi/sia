<?php

namespace App\Mail\Solicitudes;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notificacionsefa extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $subject;
    public $motivo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $subject, $motivo)
    {
        $this->content = $content;
        $this->subject = $subject;
        $this->motivo = $motivo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->content);
        return $this->markdown('browse.grados.sefa.email')
                    ->subject($this->subject);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirPrograma extends Model
{
    protected $connecction = 'sia';
    protected $table = 'sia_dir_programa';
    protected $primaryKey = 'dirPrograma_id';
    protected $fillable = ['dirPrograma_id',
							'subarea_id',
							'programa_id'];


	public function scopePrograma($query, $subarea)
	{
		
		return $query->select('sia_dir_programa.programa_id', 'programa_nombre')
						->join(env('DB_DATABASE').'.sia_programas', 'sia_dir_programa.Programa_id', 'sia_programas.programa_id')
						->where('sia_dir_programa.subarea_id', $subarea);

	}
}

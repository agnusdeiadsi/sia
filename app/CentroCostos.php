<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroCostos extends Model
{
	protected $connection = 'sia';
    protected $primaryKey = 'centrocosto_id';
    protected $table = "sia_centros_costos";
    protected $fillable = ['centrocosto_id', 'centrocosto_codigo', 'centrocosto_nombre', 'centrocosto_descripcion'];
    public $timestamps = false;

    public function SolicitudesCompras()
    {
    	return $this->belongsToMany('App\Model\Compras\Solicitud', 'compras_solicitud_centro_costos','centrocostos_id', 'solicitud_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identificacion extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'identificacion_id';
    protected $table = 'sia_identificacion';
    protected $fillable = ['identificacion_id',
                           'identificacion_tipo',
                           'identificacion_numero',
                           'matriculado_pidm',
                           'identificacion_lugar_expedicion',
                           'created_at',
                           'updated_at'];

    public function identificacion()
    {
        return $this->belongsTo('App\Matriculado');
    }

    public function scopeSearch($query, $q)
    {
      return $query->where("identificacion_numero", "=", $q);
    }

    public function scopeSearchPidm($query, $q)
    {
      return $query->where("matriculado_pidm", "=", $q);
    }
    
    public function matriculado()
      {
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm', 'matriculado_pidm');
      }
}

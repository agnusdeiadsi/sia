<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $connection = 'sia';
	protected $primaryKey = 'rol_id';
    protected $table = "sia_roles";
    protected $fillable = ['rol_id', 'rol_codigo', 'rol_nombre', 'rol_descripcion'];
	
    public $timestamps = false;

    public function sistemas()
    {
    	return $this->belongsToMany('App\Sistema', 'sia_sistema_rol', 'rol_id', 'sistema_id');
    }

    public function usuarios()
    {
    	return $this->belongsToMany('App\Usuario', 'sia_permisos', 'rol_id', 'usuario_id');
    }

    public function sistemaroles()
    {
      return $this->hasMany('App\Sistemarol', 'rol_id', 'rol_id');
    }
}

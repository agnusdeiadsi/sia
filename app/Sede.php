<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'sede_id';
    protected $table = 'sia_sedes';
    protected $fillable = [
    						'sede_id',
    						'sede_codigo',
    						'sede_nombre',
    						'sede_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('\App\Model\Certificados\Solicitud', 'solicitud_id');
    }

    /*
    * Matriculados
    *
    */

    public function academicos()
    {
        return $this->hasMany('App\Academico', 'sede_id', 'sede_id');
    }

    public function cuenta()
    {
        return $this->hasMany('App\Model\Correos\Cuenta', 'sede_id', 'sede_id');
    }
}

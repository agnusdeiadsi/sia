<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
	protected $connection = 'sia';
    protected $primaryKey = "contrato_id";
    protected $table = "sia_contratos";
    protected $fillable = ['contrato_id', 'contrato_codigo', 'contrato_nombre', 'contrato_descripcion'];
    public $timestamps = false;

    public function cuentas() // relacion de uno a muchos con la tabla correos_cuentas
    {
    	return $this->hasMany('App\Model\Correos\Cuenta'); // un tipo de contrato tiene muchas cuentas
    }
}

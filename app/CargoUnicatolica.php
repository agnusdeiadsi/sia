<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoUnicatolica extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'cargo_id';
    protected $table = 'sia_cargos';
    protected $fillable = [
    						'cargo_id',
    						'cargo_codigo',
    						'cargo_nombre',
    						'cargo_descripcion',
    					  ];

	public function cuenta()
	{
	    return $this->belongsTo('App\Model\Correos\Cuenta', 'cargo_id', 'cargo_id');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'nivelcargo_id';
    protected $table = 'sia_niveles_cargos';
    protected $fillable = [
    						'nivelcargo_id',
    						'nivelcargo_codigo',
    						'nivelcargo_nombre',
    						'nivelcargo_descripcion'
    					  ];

	public function laboral()
	{
	    return $this->belongsTo('App\Model\Egresados\Laboral', 'nivelcargo_id');
	}
}

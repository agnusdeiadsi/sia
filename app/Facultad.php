<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'facultad_id';
    protected $table = "sia_facultades";
    protected $fillable = ['facultad_id', 'facultad_codigo', 'facultad_nombre', 'facultad_descripcion'];
    public $timestamps = false;

    public function programa()
    {
    	return $this->hasMay('App\Programa', 'facultad_id', 'facultad_id');
    }
}

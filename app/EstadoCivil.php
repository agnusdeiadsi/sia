<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'estadicivil_id';
    protected $table = "sia_estados_civiles";
    protected $fillable = ['estadicivil_id', 'estadicivil_codigo', 'estadicivil_nombre', 'estadicivil_descripcion'];

    public function matriculado()
  	{
   		return $this->belongsTo('App\Matriculado', 'estadicivil_id', 'estadicivil_id');
  	}
}

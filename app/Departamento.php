<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'departamento_id';
    protected $table = 'sia_departamentos';
    protected $fillable = [
                            'departamento_id', 
                            'departamento_codigo', 
                            'departamento_nombre', 
                            'departamento_descripcion', 
                            'pais_id',  
                           ];
}

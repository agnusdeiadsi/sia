<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    
    protected $connection = 'sia';
    protected $primaryKey = 'ciudad_id';
    protected $table = 'sia_ciudades';
    protected $fillable = [
                            'ciudad_id', 
                            'ciudad_codigo', 
                            'ciudad_nombre', 
                            'ciudad_descripcion', 
                            'departamento_id',  
                           ];

  public function emprendimiento()
  {
    return $this->belongsTo('App\Model\Egresados\Emprendimiento', 'ciudad_id');  
  }
  
  public function matriculado()
  {
    return $this->hasMay('App\Matriculado', 'ciudad_id', 'ciudad_id');
  }
  
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semestre extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'semestre_id';
    protected $table = 'sia_semestres';
    protected $fillable = [
    						'semestre_id',
    						'semestre_codigo',
    						'semestre_nombre',
    						'semestre_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('\App\Model\Certificados\Solicitud', 'solicitud_id');
    }
}

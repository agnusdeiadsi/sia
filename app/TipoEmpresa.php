<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEmpresa extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'tipoempresa_id';
    protected $table = 'sia_tipos_empresas';
    protected $fillable = [
    						'tipoempresa_id',
    						'tipoempresa_codigo',
    						'tipoempresa_nombre',
    						'tipoempresa_descripcion'
    					  ];
}

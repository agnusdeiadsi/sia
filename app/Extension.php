<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'extension_id';
    protected $table = "sia_extensiones";
    protected $fillable = ['extension_id', 'extension_extension', 'area_id', 'subarea_id'];
    public $timestamps = false;


    public function scopeBuscar($query, $valor)
    {
    	return $query->where('extension_extension', 'LIKE', '%'.$valor.'%')
    	->orWhere('cuenta_primer_nombre', 'LIKE', '%'.$valor.'%')
    	->orWhere('cuenta_segundo_nombre', 'LIKE', '%'.$valor.'%')
    	->orWhere('cuenta_primer_apellido', 'LIKE', '%'.$valor.'%')
    	->orWhere('cuenta_segundo_apellido', 'LIKE', '%'.$valor.'%')
    	->orWhere('cuenta_email', 'LIKE', '%'.$valor.'%')
    	->orWhere('area_nombre', 'LIKE', '%'.$valor.'%')
    	->orWhere('subarea_nombre', 'LIKE', '%'.$valor.'%');
    }

    public function cuentas() // relacion de uno a muchos con la tabla correos_cuentas
    {
    	return $this->hasMany('App\Model\Correos\Cuenta'); // un tipo de contrato tiene muchas cuentas
    }
}

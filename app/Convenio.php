<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = "convenio_id";
    protected $table = "sia_convenio";
    protected $fillable = ['convenio_id', 'convenio_codigo', 'convenio_nombre', 'convenio_descripcion'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $connection = 'sia';
	protected $primaryKey = 'area_id';
	protected $table = "sia_areas";
	protected $fillable = ['area_id','area_codigo', 'area_nombre', 'area_descripcion'];

	public function subareas()
	{
			return $this->hasMany('App\Subarea');
	}

	public function autorizacionesUsuarios()
	{
		return $this->belongsToMany('App\Usuario', 'app_compras.compras_autorizaciones', 'area_id', 'usuario_id_area');
	}

	public function revisionesUsuarios()
	{
		return $this->belongsToMany('App\Usuario', 'app_compras.compras_revisiones', 'area_id', 'usuario_id_area');
	}
}

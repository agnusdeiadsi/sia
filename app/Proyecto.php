<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
	protected $connection = 'sia';
  protected $primaryKey = 'proyecto_id';
  protected $table = 'sia_proyectos';
  protected $fillable = ['proyecto_id', 'proyecto_codigo', 'proyecto_nombre', 'proyecto_descripcion'];
  public $timestamps = false;

  public function solicitudes()
  {
     //return $this->hasMany('App\Model\Compras\Solicitud', 'proyecto_id', 'proyecto_id');
     return $this->belongsToMany('App\Model\Compras\Solicitud', 'compras_solicitud_proyecto','proyecto_id', 'solicitud_id');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matriculado extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'matriculado_pidm';
    protected $table = 'sia_matriculados';
    protected $fillable = [
                            'matriculado_pidm', 
                            'matriculado_periodo', 
                            'matriculado_primer_nombre', 
                            'matriculado_segundo_nombre', 
                            'matriculado_primer_apellido', 
                            'matriculado_segundo_apellido', 
                            'matriculado_email', 
                            'matriculado_direccion', 
                            'matriculado_telefono',
                            'matriculado_celular',
                            'matriculado_estrato',
                            'matriculado_fecha_nacimiento',
                            'matriculado_discapacidad',
                            'matriculado_fallecido',
                            'matriculado_genero',
                            'pais_id',
                            'ciudad_id',
                            'ciudad_residencia',
                            'estadocivil_id',
                            'etnia_id',
                            'remember_token'
                           ];

    public $incrementing = false;
    
    public function scopeSearch($query, $q)
    {
      return $query->where("matriculado_pidm", "=", $q);
    }

    public function recibos() //relacion con la uno a muchos con la tabla erc_recibos
    {
    	return $this->hasMany('App\Recibo', 'matriculado_pidm'); // un estudiante tiene muchos recibos
    }

    public function identificacion() //relacion con la uno a muchos con la tabla erc_recibos
    {
        return $this->hasMany('App\Identificacion', 'matriculado_pidm', 'matriculado_pidm'); // un estudiante tiene muchos recibos
    }

    public function academico()
    {
        return $this->hasMany('App\Academico', 'matriculado_pidm', 'matriculado_pidm');
    }

    /*
    * Cancelaciones
    *
    */
    public function solicitudesCancelaciones()
    {
        return $this->hasMany('App\Model\Cancelaciones\Solicitud', 'matriculado_pidm', 'matriculado_pidm');
    }

    /*
    *
    * Egresado
    *
    */

    public function scopequeryFilters_list($query, $consulta)
    {
        try 
        {
         return \DB::connection('egresados')->table('egresados_grados as EG')
            ->join(\DB::raw('
                                (SELECT 
                                    ELaboral.*
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_laboral ELaboral

                                  INNER JOIN (
                                          SELECT
                                              MAX(EL2.updated_at) AS updated_at1,
                                              EL2.matriculado_pidm
                                          FROM 
                                              '.env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                          GROUP BY
                                              EL2.matriculado_pidm
                                      ) RU ON RU.matriculado_pidm = ELaboral.matriculado_pidm
                                      AND RU.updated_at1 = ELaboral.updated_at
                                  ) AS egresados_laboral'), function ($join)
                                {
                                  
                                  $join->on('EG.matriculado_pidm', 
                                    'egresados_laboral.matriculado_pidm');

                                }
                      )
            ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
            /*->leftJoin(env('EGRESADOS_DATABASE').'.egresados_laboral', 'sia_matriculados.matriculado_pidm', 'egresados_laboral.matriculado_pidm')*/
            ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_emprendimiento', 'EG.matriculado_pidm', 'egresados_emprendimiento.matriculado_pidm')
            ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_matriculado_logro', 'EG.matriculado_pidm', 'egresados_matriculado_logro.matriculado_pidm')
            ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 
              'sia_matriculados.matriculado_pidm')
            ->join(env('DB_DATABASE').'.sia_programas', env('EGRESADOS_DATABASE').'.EG.programa_id', 
                   env('DB_DATABASE').'.sia_programas.programa_id')
            ->select('EG.matriculado_pidm', 
                     'matriculado_primer_nombre',
                     'matriculado_segundo_nombre',
                     'matriculado_primer_apellido', 
                     'matriculado_segundo_apellido', 
                     'matriculado_email', 
                     'matriculado_telefono', 
                     'matriculado_celular', 
                     'matriculado_direccion', 
                     'grado_periodo', 
                     'programa_nombre', 
                     'grado_fecha', 
                     'identificacion_numero')
            ->whereRaw($consulta)
            ->groupBy("EG.matriculado_pidm");    
        
        } catch (\Illuminate\Database\QueryException $e) {
          //dd($e);
          return "error";

        }

    }

    public function scopequeryFilters_download($query, $consulta, $join, $colunm)
    {

      try {

        if ($consulta != "") 
        {
            
            //dd($consulta);
            $asd = \DB::select(\DB::raw("
                        SELECT
                          sia_matriculados.matriculado_pidm,
                          matriculado_primer_nombre,
                          matriculado_segundo_nombre,
                          matriculado_primer_apellido,
                          matriculado_segundo_apellido,
                          matriculado_fecha_nacimiento,
                          matriculado_genero,
                          matriculado_email,
                          matriculado_telefono,
                          matriculado_celular,
                          matriculado_direccion,
                          estadocivil_nombre,
                          grado_periodo,
                          programa_nombre,
                          grado_fecha,
                          identificacion_numero
                          ".$colunm."
                        FROM
                          ".env('EGRESADOS_DATABASE').".egresados_grados EG

                        INNER JOIN sia_identificacion 
                          ON EG.matriculado_pidm = sia_identificacion.matriculado_pidm

                        LEFT JOIN ".env('EGRESADOS_DATABASE').".egresados_emprendimiento 
                          ON EG.matriculado_pidm = egresados_emprendimiento.matriculado_pidm

                        LEFT JOIN ".env('EGRESADOS_DATABASE').".egresados_matriculado_logro 
                          ON EG.matriculado_pidm = egresados_matriculado_logro.matriculado_pidm

                        INNER JOIN ".env('DB_DATABASE').".sia_matriculados 
                          ON EG.matriculado_pidm = sia_matriculados.matriculado_pidm

                        INNER JOIN ".env('DB_DATABASE').".sia_programas 
                          ON EG.programa_id = sia_programas.programa_id

                        LEFT JOIN ".env('DB_DATABASE').".sia_estados_civiles 
                          ON sia_matriculados.estadocivil_id = ".env('DB_DATABASE').".sia_estados_civiles.estadocivil_id

                        ".$join."

                        WHERE
                          ".$consulta."
                        Group By 
                          EG.matriculado_pidm"));

                if (count($asd) > 0) 
                {
                  
                  return $asd;

                } else {

                  return "false";

                }

          } else {
            
            $asd = \DB::select(\DB::raw("SELECT
                          EG.matriculado_pidm,
                          matriculado_primer_nombre,
                          matriculado_segundo_nombre,
                          matriculado_primer_apellido,
                          matriculado_segundo_apellido,
                          matriculado_fecha_nacimiento,
                          matriculado_genero,
                          matriculado_email,
                          matriculado_telefono,
                          matriculado_celular,
                          matriculado_direccion,
                          estadocivil_nombre,
                          grado_periodo,
                          programa_nombre,
                          grado_fecha,
                          identificacion_numero
                          ".$colunm."
                        FROM
                          ".env('EGRESADOS_DATABASE').".egresados_grados EG

                        INNER JOIN sia_identificacion 
                          ON EG.matriculado_pidm = sia_identificacion.matriculado_pidm

                        LEFT JOIN ".env('EGRESADOS_DATABASE').".egresados_emprendimiento 
                          ON EG.matriculado_pidm = egresados_emprendimiento.matriculado_pidm

                        LEFT JOIN ".env('EGRESADOS_DATABASE').".egresados_matriculado_logro 
                          ON EG.matriculado_pidm = egresados_matriculado_logro.matriculado_pidm

                        INNER JOIN ".env('DB_DATABASE').".sia_matriculados 
                          ON EG.matriculado_pidm = sia_matriculados.matriculado_pidm

                        INNER JOIN ".env('DB_DATABASE').".sia_programas 
                          ON EG.programa_id = ".env('DB_DATABASE').".sia_programas.programa_id

                        LEFT JOIN ".env('DB_DATABASE').".sia_estados_civiles 
                          ON sia_matriculados.estadocivil_id = ".env('DB_DATABASE').".sia_estados_civiles.estadocivil_id
                          "
                        .$join));

                if (count($asd) > 0) 
                {
                  
                  return $asd;

                } else {

                  return "false";

                }
          }

      } catch (\Illuminate\Database\QueryException  $e) {
        dd($e);
        return 'error';

      } 
      
    }

    public function scopequeryAll_lDownload($query, $consulta)
    {
      
      try {

        if ($consulta == '') 
        {

          return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                /*->leftJoin(env('EGRESADOS_DATABASE').'.egresados_laboral', 'sia_matriculados.matriculado_pidm', 'egresados_laboral.matriculado_pidm')*/
                ->join(\DB::raw('
                                (SELECT 
                                    ELaboral.*
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_laboral ELaboral

                                  INNER JOIN (
                                          SELECT
                                              MAX(EL2.updated_at) AS updated_at1,
                                              EL2.matriculado_pidm
                                          FROM 
                                              '.env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                          GROUP BY
                                              EL2.matriculado_pidm
                                      ) RU ON RU.matriculado_pidm = ELaboral.matriculado_pidm
                                      AND RU.updated_at1 = ELaboral.updated_at
                                  ) AS egresados_laboral'), function ($join)
                                {
                                  
                                  $join->on('EG.matriculado_pidm', 
                                    'egresados_laboral.matriculado_pidm');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT situacionlaboral_id,
                                        situacionlaboral_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_situacion_laboral) 
                                AS situacionL'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.situacionlaboral_id', 'situacionL.situacionlaboral_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT contrato_id,
                                             contrato_nombre
                                FROM '.env('DB_DATABASE').'.sia_contratos) 
                                AS tipoCon'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.contrato_id', 
                                    'tipoCon.contrato_id');

                                }
                      )
                 ->leftjoin(\DB::raw('(SELECT relacionprograma_id,
                                         relacionprograma_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_relacion_programa) 
                                AS ralcionP'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.relacionprograma_id', 
                                    'ralcionP.relacionprograma_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT areapertenece_id,
                                         areapertenece_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_area_pertenece) 
                                AS areaP'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.areapertenece_id', 
                                    'areaP.areapertenece_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT duracionempresa_id,
                                             duracionempresa_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_duracion_empresa) 
                                AS duracionE'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.duracionempresa_id', 
                                    'duracionE.duracionempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT sectorempresa_id,
                                             sectorempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_sectores_empresas) 
                                AS sectoresE'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.sectorempresa_id', 
                                    'sectoresE.sectorempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT vinculacion_id,
                                             vinculacion_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_vinculacion) 
                                AS vinculacion'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.vinculacion_id', 
                                    'vinculacion.vinculacion_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT tipoempresa_id,
                                             tipoempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_tipos_empresas) 
                                AS tiposEm'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.tipoempresa_id', 
                                    'tiposEm.tipoempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT nivelcargo_id,
                                             nivelcargo_nombre
                                FROM '.env('DB_DATABASE').'.sia_niveles_cargos) 
                                AS nivelCar'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.nivelcargo_id', 
                                    'nivelCar.nivelcargo_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT rangosalario_id,
                                             rangosalario_nombre
                                FROM '.env('DB_DATABASE').'.sia_rangos_salarios) 
                                AS rangoSa'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.rangosalario_id', 
                                    'rangoSa.rangosalario_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT ocupacion_id,
                                             ocupacion_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_ocupacion) 
                                AS ocupacion'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.ocupacion_id', 
                                    'ocupacion.ocupacion_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 
                                                               'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', 'EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'situacionlaboral_nombre',
                          'contrato_nombre',
                          'relacionprograma_nombre',
                          'areapertenece_nombre',
                          'duracionempresa_nombre',
                          'sectorempresa_nombre',
                          'vinculacion_nombre',
                          'tipoempresa_nombre',
                          'nivelcargo_nombre',
                          'rangosalario_nombre',
                          'ocupacion_nombre',
                          'laboral_empresa',
                          'laboral_telefono',
                          'egresados_laboral.updated_at AS date_laboral')
                ->get();

        } else {

          return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                ->join(\DB::raw('
                                (SELECT 
                                    ELaboral.*
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_laboral ELaboral

                                  INNER JOIN (
                                          SELECT
                                              MAX(EL2.updated_at) AS updated_at1,
                                              EL2.matriculado_pidm
                                          FROM 
                                              '.env('EGRESADOS_DATABASE').'.egresados_laboral EL2
                                          GROUP BY
                                              EL2.matriculado_pidm
                                      ) RU ON RU.matriculado_pidm = ELaboral.matriculado_pidm
                                      AND RU.updated_at1 = ELaboral.updated_at
                                  ) AS egresados_laboral'), function ($join)
                                {
                                  
                                  $join->on('EG.matriculado_pidm', 
                                    'egresados_laboral.matriculado_pidm');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT situacionlaboral_id,
                                        situacionlaboral_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_situacion_laboral) 
                                AS situacionL'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.situacionlaboral_id', 'situacionL.situacionlaboral_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT contrato_id,
                                             contrato_nombre
                                FROM '.env('DB_DATABASE').'.sia_contratos) 
                                AS tipoCon'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.contrato_id', 
                                    'tipoCon.contrato_id');

                                }
                      )
                 ->leftjoin(\DB::raw('(SELECT relacionprograma_id,
                                         relacionprograma_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_relacion_programa) 
                                AS ralcionP'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.relacionprograma_id', 
                                    'ralcionP.relacionprograma_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT areapertenece_id,
                                         areapertenece_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_area_pertenece) 
                                AS areaP'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.areapertenece_id', 
                                    'areaP.areapertenece_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT duracionempresa_id,
                                             duracionempresa_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_duracion_empresa) 
                                AS duracionE'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.duracionempresa_id', 
                                    'duracionE.duracionempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT sectorempresa_id,
                                             sectorempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_sectores_empresas) 
                                AS sectoresE'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.sectorempresa_id', 
                                    'sectoresE.sectorempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT vinculacion_id,
                                             vinculacion_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_vinculacion) 
                                AS vinculacion'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.vinculacion_id', 
                                    'vinculacion.vinculacion_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT tipoempresa_id,
                                             tipoempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_tipos_empresas) 
                                AS tiposEm'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.tipoempresa_id', 
                                    'tiposEm.tipoempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT nivelcargo_id,
                                             nivelcargo_nombre
                                FROM '.env('DB_DATABASE').'.sia_niveles_cargos) 
                                AS nivelCar'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.nivelcargo_id', 
                                    'nivelCar.nivelcargo_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT rangosalario_id,
                                             rangosalario_nombre
                                FROM '.env('DB_DATABASE').'.sia_rangos_salarios) 
                                AS rangoSa'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.rangosalario_id', 
                                    'rangoSa.rangosalario_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT ocupacion_id,
                                             ocupacion_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_ocupacion) 
                                AS ocupacion'), function ($join)
                                {
                                  
                                  $join->on('egresados_laboral.ocupacion_id', 
                                    'ocupacion.ocupacion_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', 'EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'situacionlaboral_nombre',
                          'contrato_nombre',
                          'relacionprograma_nombre',
                          'areapertenece_nombre',
                          'duracionempresa_nombre',
                          'sectorempresa_nombre',
                          'vinculacion_nombre',
                          'tipoempresa_nombre',
                          'nivelcargo_nombre',
                          'rangosalario_nombre',
                          'ocupacion_nombre',
                          'laboral_empresa',
                          'laboral_telefono',
                          'egresados_laboral.updated_at AS date_laboral')
                ->whereRaw($consulta)
                ->get();

        } 

      } catch (\Illuminate\Database\QueryException  $e) {
        //dd($e);
        return 'error';

      }

    }

    public function scopequeryAll_eDownload($query, $consulta)
    {
      
      try {

        if ($consulta == '') 
        {
        
           return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_emprendimiento', 
                  'EG.matriculado_pidm', 'egresados_emprendimiento.matriculado_pidm')
                ->leftjoin(\DB::raw('(SELECT sectorempresa_id,
                                             sectorempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_sectores_empresas) 
                                AS sectoresE'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.sectorempresa_id', 
                                    'sectoresE.sectorempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT tamanoempresa_id,
                                             tamanoempresa_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_tamano_empresa) 
                                AS tamanoE'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.tamanoempresa_id', 
                                    'tamanoE.tamanoempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT ciudad_id,
                                             ciudad_nombre
                                FROM '.env('DB_DATABASE').'.sia_ciudades) 
                                AS ciudad'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.ciudad_id', 
                                    'ciudad.ciudad_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', env('EGRESADOS_DATABASE').'.EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'sectorempresa_nombre',
                          'tamanoempresa_nombre',
                          'ciudad_nombre',
                          'egresados_emprendimiento.updated_at AS date_emprendimiento')
                ->get();

        } else {

          return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_emprendimiento', 
                  'sia_matriculados.matriculado_pidm', 'egresados_emprendimiento.matriculado_pidm')
                ->leftjoin(\DB::raw('(SELECT sectorempresa_id,
                                             sectorempresa_nombre
                                FROM '.env('DB_DATABASE').'.sia_sectores_empresas) 
                                AS sectoresE'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.sectorempresa_id', 
                                    'sectoresE.sectorempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT tamanoempresa_id,
                                             tamanoempresa_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_tamano_empresa) 
                                AS tamanoE'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.tamanoempresa_id', 
                                    'tamanoE.tamanoempresa_id');

                                }
                      )
                ->leftjoin(\DB::raw('(SELECT ciudad_id,
                                             ciudad_nombre
                                FROM '.env('DB_DATABASE').'.sia_ciudades) 
                                AS ciudad'), function ($join)
                                {
                                  
                                  $join->on('egresados_emprendimiento.ciudad_id', 
                                    'ciudad.ciudad_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', 'EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'sectorempresa_nombre',
                          'tamanoempresa_nombre',
                          'ciudad_nombre',
                          'egresados_emprendimiento.updated_at AS date_emprendimiento')
                ->whereRaw($consulta)
                ->get();

        }


      } catch (\Illuminate\Database\QueryException  $e) {
        //dd($e);
        return 'error';

      }

    }

    public function scopequeryAll_Lodownload($query, $consulta)
    {
      
      try {

        if ($consulta == '') 
        {
        
          return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_matriculado_logro', 
                  'EG.matriculado_pidm', 'egresados_matriculado_logro.matriculado_pidm')
                ->leftjoin(\DB::raw('(SELECT logro_id,
                                             logro_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_logros) 
                                AS logro'), function ($join)
                                {
                                  
                                  $join->on('egresados_matriculado_logro.logro_id', 
                                    'logro.logro_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', env('EGRESADOS_DATABASE').'.EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'logro_nombre',
                          'egresados_matriculado_logro.updated_at AS date_logros')
                ->get();

        } else {

          return \DB::connection('egresados')->table('egresados_grados as EG')
                ->join(env('DB_DATABASE').'.sia_identificacion', 'EG.matriculado_pidm', 'sia_identificacion.matriculado_pidm')
                ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_matriculado_logro', 
                  'EG.matriculado_pidm', 'egresados_matriculado_logro.matriculado_pidm')
                ->leftjoin(\DB::raw('(SELECT logro_id,
                                             logro_nombre
                                FROM '.env('EGRESADOS_DATABASE').'.egresados_logros) 
                                AS logro'), function ($join)
                                {
                                  
                                  $join->on('egresados_matriculado_logro.logro_id', 
                                    'logro.logro_id');

                                }
                      )
                ->join(env('DB_DATABASE').'.sia_matriculados', 'EG.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                ->join(env('DB_DATABASE').'.sia_programas', 'EG.programa_id', 
                       env('DB_DATABASE').'.sia_programas.programa_id')
                ->leftJoin(env('DB_DATABASE').'.sia_estados_civiles','sia_matriculados.estadocivil_id', 
                       env('DB_DATABASE').'.sia_estados_civiles.estadocivil_id')
                ->select('identificacion_numero',
                          'matriculado_primer_nombre',
                          'matriculado_segundo_nombre',
                          'matriculado_primer_apellido',
                          'matriculado_segundo_apellido',
                          'matriculado_telefono',
                          'matriculado_celular',
                          'matriculado_email',
                          'programa_nombre',
                          'grado_periodo',
                          'matriculado_genero',
                          'estadocivil_nombre',
                          'matriculado_fecha_nacimiento',
                          'logro_nombre',
                          'egresados_matriculado_logro.updated_at AS date_logros')
                ->whereRaw($consulta)
                ->get();

        }


      } catch (\Illuminate\Database\QueryException  $e) {
        //dd($e);
        return 'error';

      }

    }

    public function laboral()
    {
        return $this->hasMany('App\Model\Egresados\Laboral', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function grados()
    {
        return $this->hasMany('App\Model\Egresados\Grado', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function identificaciones()
    {
        return $this->hasMany('App\Identificacion', 'matriculado_pidm', 'matriculado_pidm');
    }

    /*public function academico()
    {
        return $this->hasMany('App\Academico', 'matriculado_pidm', 'matriculado_pidm');
    }*/

    public function estudios()
    {
        return $this->hasMany('App\Model\Egresados\Estudio', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function familia()
    {
        return $this->hasOne('App\Model\Egresados\Familia', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function referencia()
    {
        return $this->hasMany('App\Model\Egresados\Referencia', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function emprendimiento()
    {
        return $this->hasMany('App\Model\Egresados\Emprendimiento', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function reconocimientos()
    {
        return $this->hasMany('App\Model\Egresados\Reconocimiento', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function estadocivil()
    {
        return $this->hasMany('App\EstadoCivil', 'estadocivil_id', 'estadocivil_id');
    }

    public function solicitud()
    {
        return $this->hasMany('App\Model\Solicitudes\Solicitud', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad', 'ciudad_id', 'ciudad_id');
    }

    public function matriculadoLogros()
    {
      return $this->hasMany('App\Model\Egresados\MatriculadoLogros', 'matriculado_pidm', 'matriculado_pidm');
    }

}


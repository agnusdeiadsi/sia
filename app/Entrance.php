<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrance extends Model
{
    protected $connection = 'sia';
	protected $primaryKey = 'entrance_id';
    protected $table = 'sia_entrances';
    protected $fillable = [
    						'entrance_id',
    						'sistema_id',
    						'usuario_id',
    						'usuario_rol_sia',
    						'usuario_rol_modulo',
    						'entrance_url_anterior',
    						'entrance_url_actual',
                            'entrance_accion',  
    						'entrance_startdate',
    						'entrance_enddate',
    					];

    public $timestamps = false;

    public function sistema()
    {
    	return $this->belongsTo('App\Sistema', 'sistema_id', 'sistema_id');
    }

    public function usuario()
    {
    	return $this->belongsTo('App\Usuario');
    }

}

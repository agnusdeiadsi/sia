<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subarea extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'subarea_id';
    protected $table = "sia_subareas";
    protected $fillable = ['subarea_id', 'subarea_codigo', 'subarea_nombre', 'subarea_descripcion'];
    public $timestamps = false;

    public function area()
    {
    	return $this->belongsTo('App\Area');
    }

    public function sistemas()
    {
    	return $this->hasMany('App\Sistema');
    }

    public function cuentas()
    {
        return $this->hasMany('App\Model\Correos\Cuenta');
    }

    public function academicos()
    {
        return $this->hasMany('App\Academico', 'subarea_id', 'programa_id');
    }

    //Compras
    public function autorizacionesUsuarios()
    {
        return $this->belongsToMany('App\Usuario', 'app_compras.compras_autorizaciones', 'subarea_id', 'usuario_id_subarea');
    }

    public function revisionesUsuarios()
    {
        return $this->belongsToMany('App\Usuario', 'app_compras.compras_revisiones', 'subarea_id', 'usuario_id_subarea');
    }
}

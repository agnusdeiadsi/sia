<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
	protected $connection = 'sia';
    protected $primaryKey = 'sistemarol_id';
    protected $table = "sia_permisos";
    protected $fillable = [
    						'usuario_id', 
                            'sistemarol_id',
    					];

    public $timestamps = false;
    

    public function usuarios()
    {
    	return $this->belongsToMany('App\Usuario', 'sia_permisos', 'sistemarol_id', 'usuario_id'); // un permiso pertenece a uno o varios usuarios de SIA
    }

    public function sistema($sistema)
    {
    	return $this->belongsTo('App\Sistema', 'sia_sistema_rol', $sistema, 'sistemarol_id');
    }

    public function sistemaroles()
    {
        return $this->belongsToMany('App\Sistemarol', 'sia_permisos', 'sistemarol_id', 'usuario_id');
    }
}

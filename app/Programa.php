<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    protected $connection = 'sia';
	protected $primaryKey = 'programa_id';
	protected $table = "sia_programas";
	protected $fillable = ['programa_id','programa_nombre', 'programa_descripcion', 'convenio_id', 'facultad_id'];

	public function scopeSearchProgram($query, $q)
	{
		return $query->where("programa_id", "=", $q);
	}

	public function facultad()
	{
		return $this->belongsTo('App\Facultad', 'facultad_id', 'facultad_id');
	}

}

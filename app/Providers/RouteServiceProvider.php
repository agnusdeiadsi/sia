<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapEstudianteRoutes();

        $this->mapGuestRoutes();

        $this->mapBrowseRoutes();

        $this->mapSiteRoutes();

        //
    }

    /**
     * Define the "site" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapSiteRoutes()
    {
        Route::group([
            'middleware' => ['web', 'site', 'auth:site'],
            'prefix' => 'site',
            'as' => 'site.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/site.php');
        });
    }

    /**
     * Define the "browse" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapBrowseRoutes()
    {
        Route::group([
            'middleware' => ['web', 'browse', 'auth:browse'],
            'prefix' => 'browse',
            'as' => 'browse.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/browse.php');
        });
    }

    /**
     * Define the "guest" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapGuestRoutes()
    {
        Route::group([
            'middleware' => ['web', 'guest', 'auth:guest'],
            'domain' => 'guest.' . env('APP_DOMAIN'),
            'as' => 'guest.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/guest.php');
        });
    }

    /**
     * Define the "estudiante" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapEstudianteRoutes()
    {
        Route::group([
            'middleware' => ['web', 'estudiante', 'auth:estudiante'],
            'prefix' => 'estudiante',
            'as' => 'estudiante.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/estudiante.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}

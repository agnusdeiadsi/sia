<?php

namespace App;

use App\Notifications\GuestResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Guest extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = 'guest';
    protected $primaryKey = 'matriculado_id';
    protected $table = 'sia_matriculados';
    protected $fillable = [
        'matriculado_id', 'matriculado_identificacion',
    ];

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', remember_token',
    ];*/

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new GuestResetPassword($token));
    }
}

<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $connection = 'compras';
    protected $primaryKey = 'solicitudcomentario_id';
    protected $table = 'compras_solicitud_comentario';
    protected $fillable = ['solicitudcomentario_id', 'solicitudcomentario_comentario', 'solicitud_id', 'usuario_id', 'solicitudcomentario_fecha'];

    public $timestamps = true;

    //

    public function solicitud()
    {
    	return $this->belongsTo('App\Solicitud', 'solicitud_id');
    }

    public function usuario()
    {
    	return $this->belongsTo('App\Usuario', 'id');
    }
}

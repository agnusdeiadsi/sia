<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model
{
    protected $connection = 'compras';
    protected $primaryKey = 'solicitudinsumo_id';
    protected $table = 'compras_solicitud_insumo';
    protected $fillable = ['solicitudinsumo_id', 'solicitudinsumo_referencia', 'solicitudinsumo_medida', 'solicitudinsumo_cantidad', 'solicitudinsumo_caracteristica', 'solicitudinsumo_costo', 'solicitud_id', 'estado_id'];
    public $timestamps = false;


    public function solicitud()
    {
    	return $this->belongsTo('App\Model\Compras\Solicitud');
    }

    public function estado()
    {
    	return $this->belongsTo('App\Model\Compras\Estado', 'estado_id');
    }

    public function medida()
    {
        return $this->belongsTo('App\Model\Compras\UnidadMedida', 'solicitudinsumo_medida', 'umedida_codigo');
    }
}

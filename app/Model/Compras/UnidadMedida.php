<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $connection = 'compras';
    protected $table = 'compras_unidades_medida';
    protected $fillable = [
    					'umedida_id',
    					'umedida_codigo',
    					'umedida_nombre',
    				];

    public $timestamps = false;

    public function insumos()
    {
    	return $this->hasMany('App\Model\Compras\Insumo', 'umedida_codigo');
    }
}

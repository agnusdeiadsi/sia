<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    protected $connection = 'compras';
    protected $primaryKey = null;
    protected $table = 'compras_revisiones';
    protected $fillable = ['area_id', 'usuario_id_area', 'subarea_id', 'usuario_id_subarea'];
    public $timestamps = false;

    public function areaUsuario()
    {
    	return $this->belongsTo('App\Usuario');
    }

    public function subareaUsuario()
    {
    	return $this->belongsTo('App\Usuario');
    }
}

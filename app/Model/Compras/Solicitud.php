<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Solicitud extends Model
{
    protected $connection = 'compras';
    protected $primaryKey = 'solicitud_id';
    protected $table = 'compras_solicitudes';
    protected $fillable = [
    						'solicitud_id',
    						'solicitud_codigo_formulario',
    						'solicitud_version_formulario',
    						'area_id',
    						'subarea_id',
    						'tipo_id',
    						'usuario_id',
    						'solicitud_nombres_solicitante',
    						'solicitud_telefono_solicitante',
    						'solicitud_extension_solicitante',
    						'solicitud_email_solicitante',
    						'solicitud_nombres_responsable',
    						'solicitud_justificacion',
    						'estado_id',
    						'solicitud_presupuesto',
    						'solicitud_orden_compra',
    						'solicitud_orden_compra_usuario',
    						'solicitud_usuario_aplazamiento',
    						'solicitud_fecha_aplazamiento',
    						'solicitud_primera_firma',
    						'solicitu_fecha_primera_firma',
    						'solicitud_segunda_firma',
    						'solicitud_fecha_segunda_firma',
    						'solicitud_tercera_firma',
    						'solicitud_fecha_tercera_firma',
                'solicitud_usuario_archiva',
                'solicitud_archived_at',
    					];
    protected $keyType = 'varchar';
    public $incrementing = false;


    public function scopeFilterMyRequests($query, $solicitante)
    {
      return $query->where([
        ['usuario_id', '=', $solicitante],
      ]);
    }

    public function scopeFilterRequests($query, $estado)
    {
      return $query->where('estado_id', '=', $estado);
    }

    public function scopeFilterRequestsManager($query, $estado)
    {
      return $query->where('estado_id', '=', $estado);
    }

    public function insumos()
   	{
   		return $this->hasMany('App\Model\Compras\Insumo', 'solicitud_id');
   	}

   	public function comentarios()
   	{
   		return $this->hasMany('App\Model\Compras\Comentario', 'solicitud_id');
   	}

   	public function centrosOperacion()
   	{
   		return $this->belongsToMany('App\CentroOperacion', 'app_compras.compras_solicitud_centro_operacion', 'solicitud_id', 'centrooperacion_id');
   	}

   	public function centrosCostos()
   	{
   		return $this->belongsToMany('App\CentroCostos', 'app_compras.compras_solicitud_centro_costos', 'solicitud_id', 'centrocostos_id');
   	}

   	public function estado()
   	{
   		return $this->belongsTo('App\Model\Compras\Estado');
   	}

    public function usuario()
   	{
   		return $this->belongsTo('App\Usuario');
   	}

    public function proyectos()
   	{
   		//return $this->belongsTo('App\Proyecto');
      return $this->belongsToMany('App\Proyecto', 'app_compras.compras_solicitud_proyecto','solicitud_id', 'proyecto_id');
   	}

    public function area()
    {
      return $this->belongsTo('App\Area', 'area_id', 'area_id');
    }

    public function subarea()
    {
      return $this->belongsTo('App\Subarea', 'subarea_id', 'subarea_id');
    }
}

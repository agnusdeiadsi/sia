<?php

namespace App\Model\Compras;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = 'compras';
    protected $primaryKey = 'estado_id';
    protected $table = 'compras_estados';
    protected $fillable = ['estado_id', 'estado_codigo', 'estado_nombre', 'estado_descripcion'];

    public $timestamps = false;

    public function solicitudes()
    {
    	return $this->hasMany('Solicitud', 'estado_id');
    }

    public function insumos()
    {
    	return $this->hasMany('Insumo');
    }
}

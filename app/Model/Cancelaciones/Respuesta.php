<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'respuesta_id';
    protected $table = 'cancelaciones_respuestas';
    protected $fillable = [
    						'respuesta_id',
    						'respuesta_nombre',
    						'respuesta_ni',
    						'respuesta_dc',
    						'respuesta_valor',
    						'respuesta_observacion',
    						'solicitud_id',
    						'apelacion_id',
    						'usuario_id',
    						'respuesta_firma_uno',
    						'respuesta_firma_dos',
    						'created_at',
    						'updated_at'
    					];

    public function solicitud()
    {
    	return $this->belongsTo('App\Model\Cancelaciones\Solicitud', 'solicitud_id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }

    public function usuarioFimarUno()
    {
        return $this->belongsTo('App\Usuario', 'respuesta_firma_uno');
    }

    public function usuarioFimarDos()
    {
        return $this->belongsTo('App\Usuario', 'respuesta_firma_dos');
    }

    public function apelacion()
    {
        return $this->belongsTo('App\Model\Cancelaciones\Apelacion', 'apelacion_id');
    }
}

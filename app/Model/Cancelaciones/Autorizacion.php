<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Autorizacion extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'autorizacion_id';
    protected $table = 'cancelaciones_autorizaciones';
    protected $fillable = [
    						'autorizacion_id',
    						'usuario_id',
    						'tipo_id',
    						'estado_id',
    						'created_at',
    						'updated_at',
    					];

    public function usuario()
    {
    	return $this->belongsTo('App\Usuario');
    }

    public function tipo()
    {
    	return $this->belongsTo('App\Model\Cancelaciones\Tipo', 'tipo_id');
    }

    public function estado()
    {
    	return $this->belongsTo('App\Model\Cancelaciones\Estado', 'estado_id', 'estado_id');
    }
}

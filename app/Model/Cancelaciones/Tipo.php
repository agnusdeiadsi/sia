<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'tipo_id';
    protected $table = 'cancelaciones_tipos';
    protected $fillable = [
    						'tipo_id',
    						'tipo_codigo',
    						'tipo_nombre',
    						'tipo_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('App\Model\Cancelaciones\Solicitud', 'tipo_id');
    }
    

    /*
    * Sia
    *
    */
    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'app_cancelaciones.cancelaciones_autorizaciones', 'tipo_id', 'usuario_id')->withPivot('estado_id'); //un tipo de solicitud es autorizadaa por muchos usuarios
    }
}

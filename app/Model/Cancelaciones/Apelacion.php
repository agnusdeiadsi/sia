<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Apelacion extends Model
{
	protected $connection = 'cancelaciones';
    protected $primaryKey = 'apelacion_id';
    protected $table = 'cancelaciones_apelaciones';
    protected $fillable = [
    						'apelacion_id',
    						'apelacion_descripcion',
    						'apelacion_respuesta',
    						'solicitud_id',
    						'estado_id',
    						'created_at',
    						'updated_at',
    					];
    public $incrementing = false;

    //Métodos
    public function solicitud()
    {
        return $this->belongsTo('App\Model\Cancelaciones\Solicitud', 'solicitud_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\Model\Cancelaciones\Estado', 'estado_id');
    }

    public function respuesta()
    {
        return $this->hasOne('App\Model\Cancelaciones\Respuesta', 'apelacion_id', 'apelacion_id');
    }

    public function adjuntos()
    {
        return $this->hasMany('App\Model\Cancelaciones\Adjunto', 'apelacion_id', 'apelacion_id');
    }
}

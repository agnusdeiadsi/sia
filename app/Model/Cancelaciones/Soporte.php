<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Soporte extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'soporte_id';
    protected $table = 'cancelaciones_soportes';
    protected $fillable = [
    						'soporte_id',
    						'soporte_codigo',
    						'soporte_nombre',
    						'soporte_descripcion',
    						'solicitud_id',
    						'apelacion_id',
    						'usuario_id',
    						'created_at',
    						'updated_at',
    					];
   	//public $timestamps = false;

   	public function solicitud()
    {
    	return $this->belongsTo('\App\Model\Cancelaciones\Solicitud', 'solicitud_id');
    }

    public function usuario()
    {
    	return $this->belongsTo('\App\Usuario', 'usuario_id');
    }
}

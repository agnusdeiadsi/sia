<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'solicitud_id';
    protected $table = 'cancelaciones_solicitudes';
    protected $fillable = [
    						'solicitud_id',
    						'solicitud_descripcion',
    						'matriculado_pidm',
    						'estado_id',
    						'tipo_id',
    						'created_at',
    						'updated_at',
    					];
   	public $incrementing = false;

    public function scopeFiltrar($query, $filtro)
    {
        return $query->where([
            ['estado_id', '=', $filtro],
        ]);
    }
   	public function matriculado()
    {
    	return $this->belongsTo('App\Matriculado', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function estado()
    {
    	return $this->belongsTo('App\Model\Cancelaciones\Estado', 'estado_id');
    }

    public function tipo()
    {
    	return $this->belongsTo('App\Model\Cancelaciones\Tipo', 'tipo_id');
    }

    public function adjuntos()
    {
    	return $this->hasMany('App\Model\Cancelaciones\Adjunto', 'solicitud_id', 'solicitud_id');
    }

    public function soportes()
    {
        return $this->hasMany('App\Model\Cancelaciones\Soporte', 'solicitud_id', 'solicitud_id');
    }

    public function respuesta()
    {
        return $this->hasOne('App\Model\Cancelaciones\Respuesta', 'solicitud_id', 'solicitud_id');
    }

    public function apelacion()
    {
        return $this->hasOne('App\Model\Cancelaciones\Apelacion', 'solicitud_id', 'solicitud_id');
    }
}

<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'adjunto_id';
    protected $table = 'cancelaciones_adjuntos';
    protected $fillable = [
    						'adjunto_id',
    						'adjunto_codigo',
    						'adjunto_nombre',
    						'solicitud_id',
    						'apelacion_id',
    						'created_at',
    						'updated_at',
    					];

    //métodos                    
   	public function solicitud()
    {
    	return $this->belongsTo('\App\Model\Cancelaciones\Solicitud', 'solicitud_id');
    }

    public function apelacion()
    {
        return $this->belongsTo('\App\Model\Cancelaciones\Apelacion', 'apelacion_id', 'apelacion_id');
    }
}

<?php

namespace App\Model\Cancelaciones;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = 'cancelaciones';
    protected $primaryKey = 'estado_id';
    protected $table = 'cancelaciones_estados';
    protected $fillable = [
    						'estado_id',
    						'estado_codigo',
    						'estado_nombre',
    						'estado_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('\App\Model\Cancelaciones\Solicitud', 'estado_id');
    }

        public function apelaciones()
    {
        return $this->hasMany('\App\Model\Cancelaciones\Apelacion', 'estado_id');
    }
}

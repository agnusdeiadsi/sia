<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BTelefono extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'telp_pidm';
    protected $table = 'aavtelp_v3';
    protected $fillable = [
                            'telp_pidm',
                        ];
    public $timestamps = false;
    public $incrementing = false;

    public function matriculado()
    {
    	return $this->belongsTo('App\Model\Banner\BMatriculado', 'pers_pidm', 'telp_pidm');
    }
}

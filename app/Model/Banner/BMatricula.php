<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BMatricula extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'bmtr_pidm';
    protected $table = 'aavbmtr_v3';
    protected $fillable = [
                            'bmtr_pidm',
                            'bmtr_cod_programa'
                        ];
    public $timestamps = false;
    public $incrementing = false;

    public function programa()
    {
    	return $this->hasOne('App\Model\Banner\BPrograma', 'stvmajr_code', 'bmtr_cod_programa');
    }
}

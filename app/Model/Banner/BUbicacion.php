<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BUbicacion extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'ubip_pidm';
    protected $table = 'aavubip_v3';
    protected $fillable = [
                            'ubip_pidm',
                        ];
    //public $timestamps = false;
    public $incrementing = false;

    public function matriculado()
    {
    	return $this->belongsTo('App\Model\Banner\BMatriculado', 'pers_pidm', 'ubip_pidm');
    }
}

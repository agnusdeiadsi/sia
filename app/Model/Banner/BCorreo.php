<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BCorreo extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'emal_pidm';
    protected $table = 'aavemal_v3';
    protected $fillable = [
                            'emal_pidm',
                            'emal_altr',
                            'emal_estu',
                            'emal_doce',
                            'emal_egre',
                        ];
    //public $timestamps = false;
    public $incrementing = false;

    public function matriculado()
    {
    	return $this->belongsTo('App\Model\Banner\BMatriculado', 'pers_pidm', 'emal_pidm');
    }
}

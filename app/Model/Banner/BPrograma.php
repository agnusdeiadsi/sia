<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BPrograma extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'stvmajr_code';
    protected $table = 'stvmajr';
    protected $fillable = [
                            'stvmajr_code',
                        ];
    public $timestamps = false;
    public $incrementing = false;
}

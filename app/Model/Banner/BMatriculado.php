<?php

namespace App\Model\Banner;

use Illuminate\Database\Eloquent\Model;

class BMatriculado extends Model
{
    protected $connection = 'banner';
    protected $primaryKey = 'pers_pidm';
    protected $table = 'AAVPERS_V3';
    protected $fillable = [
                            'pers_pidm',
                        ];
    public $timestamps = false;
    public $incrementing = false;

    //Un estudiante tiene un correo
    public function correo()
    {
    	return $this->hasOne('App\Model\Banner\Bcorreo', 'emal_pidm', 'pers_pidm');
    }

    //un estudiante tiene una ubicación
    public function ubicacion()
    {
    	return $this->hasOne('App\Model\Banner\BUbicacion', 'ubip_pidm', 'pers_pidm');
    }

    public function telefono()
    {
    	return $this->hasOne('App\Model\Banner\BTelefono', 'telp_pidm', 'pers_pidm');
    }

    public function matriculas()
    {
    	return $this->hasMany('App\Model\Banner\BMatricula', 'bmtr_pidm', 'pers_pidm');
    }
}

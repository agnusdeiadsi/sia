<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Satisfacion_Competencias extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'competencias_id';
    protected $table = 'encuesta_satisfacion_competencias';
    protected $fillable = ['competencias_id',
    					   'matriculado_pidm',
    					   'competencias_convivencia',
    					   'competencias_liderazgo',
    					   'competencias_trabajo_equipo',
    					   'competencias_manejo_discurso',
    					   'competencias_analisis',
    					   'competencias_comunicacion',
    					   'competencias_adaptabilidad',
    					   'competencias_creatividad',
    					   'competencias_desiciones',
    					   'competencias_investigacion',
    					   'created_at',
    					   'updated_at'];
}

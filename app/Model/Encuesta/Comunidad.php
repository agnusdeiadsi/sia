<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'censal_id';
    protected $table = 'encuesta_censal_comunidad';
    protected $fillable = ['censalcomunidad_id',
    					   'matriculado_pidm',
    					   'comunidad'];
   	public $timestamps = false;
}

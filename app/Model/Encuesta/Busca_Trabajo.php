<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Busca_Trabajo extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'buscatrabajo_id';
    protected $table = 'encuesta_busca_trabajo';
    protected $fillable = ['buscatrabajo_id',
    					   'matriculado_pidm',
    					   'buscatrabajo_1ervez',
    					   'buscatrabajo_dificultad',
    					   'buscatrabajo_tiempo',
    					   'buscatrabajo_canalbusqueda',
    					   'created_at',
    					   'updated_at'];
}

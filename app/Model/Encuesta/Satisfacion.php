<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Satisfacion extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'satisfacion_id';
    protected $table = 'encuesta_satisfacion';
    protected $fillable = ['satisfacion_id',
    					   'matriculado_pidm',
    					   'satisfacion_salones',
    					   'satisfacion_laboratorios',
    					   'satisfacion_espacios',
    					   'satisfacion_audiovisuales',
    					   'satisfacion_aulasinformatica',
    					   'satisfacion_espaciosdeporte',
    					   'satisfacion_redessociales',
    					   'satisfacion_biblioteca',
    					   'satisfacion_medioscomunicacion',
    					   'satisfacion_actividades',
    					   'created_at',
    					   'updated_at'];
}

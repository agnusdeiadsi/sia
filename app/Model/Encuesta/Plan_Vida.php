<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Plan_Vida extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'planvida_id';
    protected $table = 'encuesta_planvida';
    protected $fillable = ['planvida_id',
    					   'matriculado_pidm',
    					   'planvida_plan',
    					   'created_at',
    					   'updated_at'];
}

<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Nivel_Identidad extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'nivelidentidad_id';
    protected $table = 'encuesta_nivel_identidad';
    protected $fillable = ['nivelidentidad_id',
    					   'matriculado_pidm',
    					   'nivelidentidad_estudiar',
    					   'nivelidentidad_volver',
    					   'nivelidentidad_novolver',
    					   'nivelidentidad_recomendacion',
    					   'nivelidentidad_norazon',
    					   'created_at',
    					   'updated_at'];
}

<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'financiacion_financiacion_id';
    protected $table = 'encuesta_financiacion_financiacion';
    protected $fillable = ['financiacion_financiacion_id',
    					   'matriculado_pidm',
    					   'financiacion_financiacion',
    					   'financiacion_financiacion_entidad',
    					   'created_at',
    					   'updated_at'];
}

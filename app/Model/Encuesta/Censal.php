<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Censal extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'censal_id';
    protected $table = 'encuesta_censal';
    protected $fillable = ['censal_id',
    					   'matriculado_pidm',
    					   'censa_poblacion_necesidades',
    					   'censal_descapacidad',
    					   'censal_patrocinio',
    					   'censal_patrocinio_empresa',
    					   'censal_comunidad',
    					   'created_at',
    					   'updated_at'
                          ];

    public function scopeGetRawCensal($query, $value)
    {
         return $query->select('*')
                        ->from('encuesta_censal')
                        ->where('matriculado_pidm', $value)->get();
    }
}

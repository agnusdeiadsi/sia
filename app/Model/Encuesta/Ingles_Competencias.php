<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Ingles_Competencias extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'inglescompetencias_id';
    protected $table = 'encuesta_ingles_competencias';
    protected $fillable = ['inglescompetencias_id',
    					   'matriculado_pidm',
    					   'inglescompetencias_habla',
    					   'inglescompetencias_escucha',
    					   'inglescompetencias_lectura',
    					   'inglescompetencias_escritura',
    					   'created_at',
    					   'updated_at'];
}

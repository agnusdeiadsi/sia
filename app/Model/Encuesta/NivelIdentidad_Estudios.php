<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class NivelIdentidad_Estudios extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'apoyo_id';
    protected $table = 'encuesta_nivel_identidad_otroestudio';
    protected $fillable = ['otroestudio_id',
    					   'matriculado_pidm',
    					   'otro_estudio'];
   	public $timestamps = false;
}

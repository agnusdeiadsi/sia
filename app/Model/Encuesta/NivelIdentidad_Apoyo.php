<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class NivelIdentidad_Apoyo extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'apoyo_id';
    protected $table = 'encuesta_nivel_identidad_apoyo';
    protected $fillable = ['apoyo_id',
    					   'matriculado_pidm',
    					   'apoyo'];
   	public $timestamps = false;
}

<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Idea_Empresa extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'crearempresa_id';
    protected $table = 'encuesta_crear_empresa';
    protected $fillable = ['crearempresa_id',
    					   'matriculado_pidm',
    					   'crear_empresa',
    					   'crearempresa_idea',
    					   'crearempresa_dificultad',
    					   'sectorempresa_id',
    					   'created_at',
    					   'updated_at'];
}

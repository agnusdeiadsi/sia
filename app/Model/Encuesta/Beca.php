<?php

namespace App\Model\Encuesta;

use Illuminate\Database\Eloquent\Model;

class Beca extends Model
{
    protected $connection = 'encuesta';
    protected $primaryKey = 'financiacion_beca_id';
    protected $table = 'encuesta_financiacion_beca';
    protected $fillable = ['financiacion_beca_id',
    					   'matriculado_pidm',
    					   'financiacion_beca',
    					   'financiacion_beca_entidad',
    					   'created_at',
    					   'updated_at'];
}

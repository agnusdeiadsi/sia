<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Emprendimiento extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'emprendimiento_id';
    protected $table = 'egresados_emprendimiento';
    protected $fillable = ['emprendimiento_id',
                           'emprendimiento_empresa',   
                           'sectorempresa_id', 
                           'pais_id', 
                           'ciudad_id', 
                           'matriculado_pidm', 
                           'emprendimiento_couching', 
                           'tamanoempresa_id'
                          ];


  public function matriculado()
  {   
      return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
  }

  public function tamanoempresas()
  {
      return $this->hasMany('App\Model\Egresados\TamanoEmpresa', 'tamanoempresa_id', 'tamanoempresa_id');
  }

  public function ciudades()
  {
      return $this->hasMany('App\Ciudad', 'ciudad_id', 'ciudad_id');
  }

  public function sectoresempresas()
  {
      return $this->hasMany('App\SectorEmpresa', 'sectorempresa_id', 'sectorempresa_id');
  }
  
}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Vinculacion extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'vinculacion_id';
    protected $table = 'egresados_vinculacion';
    protected $fillable = [
    						'vinculacion_id',
    						'vinculacion_codigo',
    						'vinculacion_nombre',
    						'vinculacion_descripcion'
    					  ];

    public function laboral()
	{
	    return $this->belongsTo('App\Model\Egresados\Laboral', 'nivelcargo_id');
	}
}

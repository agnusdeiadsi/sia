<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class DuracionEmpresa extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'duracionempresa_id';
    protected $table = 'egresados_duracion_empresa';
    protected $fillable = [
    						'duracionempresa_id',
    						'duracionempresa_codigo',
    						'duracionempresa_nombre',
    						'duracionempresa_descripcion'
    					  ];

    public function laboral()
	{
	    return $this->belongsTo('App\Model\Egresados\Laboral', 'nivelcargo_id');
	}
}

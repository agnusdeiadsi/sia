<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'familia_id';
    protected $table = 'egresados_familia';
    protected $fillable = ['familia_id',
						   'familia_integrantes',
						   'familia_hijos',
						   'familia_hermanos',
						   'familia_hermanos_estudios_superiores',
						   'matriculado_pidm',
						  ];

	//public $timestamps = false;

    public function scopeSearchPidm($query, $q)
    {

      return $query->where("matriculado_pidm", "=", $q);
    }
                          
	public function matriculado()
    {   
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }
    /*
    public function bannerMatriculado()
    {   
        return $this->belongsTo('App\Model\Banner\Egresados\Matriculado', 'matriculado_pidm');  
    }*/
}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Laboral extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'laboral_id';
    protected $table = 'egresados_laboral';
    protected $fillable = ['laboral_id',
                           'laboral_empresa',
                           'laboral_telefono',
                           'sectorempresa_id',
                           'contrato_id',
                           'laboral_inicio',
                           'laboral_fin',
                           'tipoempresa_id',
                           'rangosalario_id',
                           'nivelcargo_id',
                           'situacionlaboral_id',
                           'areapertenece_id',
                           'ocupacion_id',
                           'pais_id',
                           'ciudad_id',
                           'matriculado_pidm',
                           'ralacionprograma_id',
                           'duracionempresa_id',
                           'vinculacion_id'
                          ];

    public function matriculado()
    {   
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }

    /*public function scopePidm($query, $pidm)
    {
         return $query->select('*')
                        ->from('egresados_laboral')
                        ->where('matriculado_pidm', $pidm);
    }

   public function scopequeryFilters($query, $consulta)
    {
        return DB::connection('egresados')
                            ->select($consulta);

    }*/

    public function cargos()
    {
         return $this->hasMany('App\Cargo', 'nivelcargo_id', 'nivelcargo_id');
    }

    public function situacionlaboral()
    {
         return $this->hasMany('App\Model\Egresados\SituacionLaboral', 'situacionlaboral_id', 'situacionlaboral_id');
    }

    public function duraciones()
    {
         return $this->hasMany('App\Model\Egresados\DuracionEmpresa', 'duracionempresa_id', 'duracionempresa_id');
    }

    public function contratos()
    {
         return $this->hasMany('App\Contrato', 'contrato_id', 'contrato_id');
    }

    public function vinculaciones()
    {
         return $this->hasMany('App\Model\Egresados\Vinculacion', 'vinculacion_id', 'vinculacion_id');
    }

    public function salarios()
    {
         return $this->hasMany('App\RangoSalarial', 'rangosalario_id', 'rangosalario_id');
    }
}

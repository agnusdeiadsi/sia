<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Reconocimiento extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'reconocimiento_id';
    protected $table = 'egresados_reconocimientos';
    protected $fillable = [
    						'reconocimiento_id',
    						'reconocimiento_nombre',
    						'reconocimiento_organizacion',
    						'reconocimiento_fecha',
    						'matriculado_pidm',
    					  ];

    public function matriculado()
    {   
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }
}

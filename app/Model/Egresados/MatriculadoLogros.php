<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class MatriculadoLogros extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'matriculadologro_id';
    protected $table = 'egresados_matriculado_logro';
    protected $fillable = ['matriculadologro_id',
                           'matriculadologro_fecha',    
                           'matriculado_pidm',
                           'logro_id'
                          ];

    public function matriculado()
    {
    	return $this->belongsTo('App\Matriculado', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function logro()
    {
    	return $this->belongsTo('App\Model\Egresados\Logros', 'logro_id', 'logro_id');
    }
}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class TamanoEmpresa extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'tamanoempresa_id';
    protected $table = 'egresados_tamano_empresa';
    protected $fillable = [
    						'tamanoempresa_id',
    						'tamanoempresa_codigo',
    						'tamanoempresa_nombre',
    						'tamanoempresa_descripcion'
    					  ];

  public function emprendimiento()
  {
  	return $this->belongsTo('App\Model\Egresados\Emprendimiento', 'tamanoempresa_id');	
  }

}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class SituacionLaboral extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'situacionlaboral_id';
    protected $table = 'egresados_situacion_laboral';
    protected $fillable = [
    						'situacionlaboral_id',
    						'situacionlaboral_codigo',
    						'situacionlaboral_nombre',
    						'situacionlaboral_descripcion'
    					  ];


    public function laboral()
	{
	    return $this->belongsTo('App\Model\Egresados\Laboral', 'situacionlaboral_id');
	}
}

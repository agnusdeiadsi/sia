<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Logros extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'logro_id';
    protected $table = 'egresados_logros';
    protected $fillable = [
    						'logro_id',
    						'logro_codigo',
    						'logro_nombre',
    						'logro_descripcion'
    					  ];

   	public function matriculadoLogros()
   	{
   		return $this->hasMany('App\Model\Egresados\MatriculadoLogros', 'logro_id', 'logro_id');
   	}
}

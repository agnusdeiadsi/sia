<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    protected $connection = 'egresados';
    protected $primarykey = 'referencia_id';
    protected $table = 'egresados_referencias';
    protected $fillable = ['referencia_id',
						   'referencia_nombres',
						   'referencia_parentesco',
						   'referencia_telefono',
						   'referencia_celular',
						   'matriculado_pidm',
						   'created_at',
						   'updated_at',
						  ];

	public function matriculado()
    {   
       return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }

}

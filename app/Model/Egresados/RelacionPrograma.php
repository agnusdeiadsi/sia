<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class RelacionPrograma extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'relacionprograma_id';
    protected $table = 'egresados_relacion_programa';
    protected $fillable = [
    						'relacionprograma_id',
    						'relacionprograma_codigo',
    						'relacionprograma_nombre',
    						'relacionprograma_descripcion'
    					  ];
}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grado extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'grado_id';
    protected $table = 'egresados_grados';
    protected $fillable = [
    						'grado_id',
    						'grado_titulo',
    						'grado_fecha',
    						'grado_periodo',
                            'grado_puntaje',
    						'nivelacademico_id',
    						'programa_id',
    						'matriculado_pidm',
    						'create_at',
    						'updated_at'
    					  ];
    public $timestamps = false;

    public function scopeGetStudent($query, $value)
    {
         return $query->select('*')
                        ->from('egresados_grados')
                        ->where('matriculado_pidm', $value)->get();
    }

    public function scopeget_email($query, $value, $program, $queryWhere)
    {   

        if ($program) {

            return $query->select('*')
                            ->from('egresados_grados')
                            ->whereRaw("(".$queryWhere.") AND (grado_fecha BETWEEN date_sub(now(), interval '$value' year)  AND NOW())")->get();

        } else {

            return $query->select('*')
                            ->from('egresados_grados')
                            ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$value' year)  AND NOW()")->get();

        }

    }

    public function scopeget_info_noEmail($query, $value, $program, $queryWhere)
    {   

        if ($program) {
        
           return $query->select('*')
                        ->from('egresados_grados')
                        ->join(env('DB_DATABASE').'.sia_matriculados', 'egresados_grados.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                        ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_referencias', 
                            'egresados_grados.matriculado_pidm', 'egresados_referencias.matriculado_pidm')
                        ->whereRaw("(".$queryWhere.") AND (grado_fecha BETWEEN date_sub(now(), interval '$value' year) AND NOW()) 
                                    AND (sia_matriculados.matriculado_email IS null
                                    OR matriculado_email NOT REGEXP '^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}$')"); 

        } else {

            return $query->select('*')
                        ->from('egresados_grados')
                        ->join(env('DB_DATABASE').'.sia_matriculados', 'egresados_grados.matriculado_pidm', 'sia_matriculados.matriculado_pidm')
                        ->leftJoin(env('EGRESADOS_DATABASE').'.egresados_referencias', 
                            'egresados_grados.matriculado_pidm', 'egresados_referencias.matriculado_pidm')
                        ->whereRaw("grado_fecha BETWEEN date_sub(now(), interval '$value' year) AND NOW() 
                                    AND (sia_matriculados.matriculado_email IS null
                                    OR matriculado_email NOT REGEXP '^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}$')");

        }
        

    }
   /* public function scopeDataFilters($query, $programa, $periodo)
    {
        if ($programa == 'all' && $periodo == 'all') 
        {
            return $query->select('*')
                        ->from('egresados_grados');
        } elseif($programa == 'all' && $periodo != 'all') {

            return $query->select('*')
                        ->from('egresados_grados')
                        ->where('grado_periodo', $periodo);
        } elseif($programa != 'all' && $periodo == 'all') {

            return $query->select('*')
                        ->from('egresados_grados')
                        ->where('subarea_id', $programa);
        } else {
            return $query->select('*')
                            ->from('egresados_grados')
                            ->where([['subarea_id', $programa], ['grado_periodo', $periodo]]);
                            //->where('grado_periodo', $periodo);
        }

    }*/

    public function matriculado()
    {   
       return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }

    public function programas()
    {
        return $this->belongsTo('App\Subarea', 'subarea_id');
    }
}

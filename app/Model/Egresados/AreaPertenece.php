<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class AreaPertenece extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'areapertenece_id';
    protected $table = 'egresados_area_pertenece';
    protected $fillable = [
    						'areapertenece_id',
    						'areapertenece_codigo',
    						'areapertenece_nombre',
    						'areapertenece_descripcion'
    					  ];
}

<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
   protected $connection = 'egresados';
    protected $primaryKey = 'estudio_id';
    protected $table = 'egresados_estudios';
    protected $fillable = [
    						'estudio_id',
    						'nivelacademico_id',
    						'estudio_institucion',
    						'estudio_titulo',
    						'estudio_inicio',
    						'estudio_fin',
    						'matriculado_pidm',
    						'create_at',
    						'updated_at'
    					  ];

    public function matriculado()
    {   
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm');  
    }

	public function nivelAcademico()
	{
		return $this->belongsTo('App\NivelAcademico', 'nivelacademico_id');
	}
}

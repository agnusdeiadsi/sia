<?php

namespace App\Model\Egresados;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    protected $connection = 'egresados';
    protected $primaryKey = 'ocupacion_id';
    protected $table = 'egresados_ocupacion';
    protected $fillable = [
    						'ocupacion_id',
    						'ocupacion_codigo',
    						'ocupacion_nombre',
    						'ocupacion_descripcion'
    					  ];
}

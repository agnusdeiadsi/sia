<?php

namespace App\Model\Correos;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
	protected $connection = 'correos';
	protected $primaryKey = 'tipo_id';
    protected $table = "correos_tipos";
    protected $fillable = ['tipo_id', 'tipo_codigo', 'tipo_nombre', 'tipo_descripcion'];
		public $timestamps = false;

    public function cuentas() // relacion de uno a muchos con la tabla correos_cuentas
    {
    	return $this->hasMany('App\Model\Correos\Cuenta'); // un tipo tiene muchas cuentas
    }
}

<?php

namespace App\Model\Correos;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
	protected $connection = 'correos';
    public $timestamps = false;
	protected $primaryKey = 'estado_id';
    protected $table = "correos_estados";
    protected $fillable = ['estado_id', 'estado_codigo', 'estado_nombre', 'estado_descripcion'];

    public function cuentas() // relacion de uno a muchos con la tabla correos_cuentas
    {
    	return $this->hasMany('App\Model\Correos\Cuenta'); // un estado tiene muchas cuentas
    }
}

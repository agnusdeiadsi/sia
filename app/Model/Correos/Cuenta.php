<?php

namespace App\Model\Correos;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $connection = 'correos';
    protected $primaryKey = 'cuenta_id';
    protected $table = 'correos_cuentas';
    protected $fillable = [
                            'cuenta_id',
                            'cuenta_codigo',
                            'cuenta_tipo_identificacion',
                            'cuenta_identificacion',
                            'cuenta_usuario',
                            'cuenta_cuenta',
                            'cuenta_primer_nombre',
                            'cuenta_segundo_nombre',
                            'cuenta_primer_apellido',
                            'cuenta_segundo_apellido',
                            'tipo_id',
                            'cuenta_email',
                            'cuenta_genero',
                            'contrato_id',
                            'subarea_id',
                            'estado_id',
                            'extension_id',
                            'cuenta_condiciones',
                            'cuenta_fecha_activacion',
                            'cuenta_fecha_desactivacion',
                            'cargo_id',
                            'sede_id',
                            'cuenta_fecha_nacimiento',
                          ];

    //scope
    public function scopeFiltro($query, $estado)
    {
      return $query->where("correos_cuentas.estado_id", "=", $estado);
    }

    public function scopeFiltrar($query, $valor)
    {
      return $query->where("correos_cuentas.cuenta_identificacion", "=", ''.$valor.'')
      ->orWhere("correos_cuentas.cuenta_primer_apellido", "LIKE", '%'.$valor.'%')
      ->orWhere("correos_cuentas.cuenta_segundo_apellido", "LIKE", '%'.$valor.'%')
      ->orWhere("correos_cuentas.cuenta_primer_nombre", "LIKE", '%'.$valor.'%')
      ->orWhere("correos_cuentas.cuenta_segundo_nombre", "LIKE", '%'.$valor.'%')
      ->orWhere("correos_cuentas.cuenta_cuenta", "LIKE", '%'.$valor.'%');
    }

    public function estado() // relacion de uno a muchos con la tabla correos_estados
    {
    	return $this->belongsTo('App\Model\Correos\Estado', 'estado_id'); // una cuenta pertenece a un estado
    }

    public function tipo() // relacion de uno a muchos con la tabla correos_tipos
    {
    	return $this->belongsTo('App\Model\Correos\Tipo', 'tipo_id'); // una cuenta pertenece a un tipo
    }

    public function extension() // relacion de uno a muchos con la tabla sia_extensiones
    {
    	return $this->belongsTo('App\Extension', 'extension_id'); // una cuenta tiene una extension
    }

    public function contrato() // relacion de uno a muchos con la tabla sia_contratos
    {
    	return $this->belongsTo('App\Contrato', 'contrato_id'); // una cuenta tiene un tipo de contrato
    }

    public function usuario() // relacion de uno a uno con la tabla sia_usuarios
    {
        return $this->hasOne('App\Usuario', 'id', 'cuenta_id'); // una cuenta tiene un usuario
    }

    public function subarea() // relacion de uno a uno con la tabla sia_contratos
    {
        return $this->belongsTo('App\Subarea', 'subarea_id'); // una cuenta tiene un usuario
    }

    public function cargo()
    {
        return $this->hasOne('App\CargoUnicatolica', 'cuenta_id', 'cuenta_id');
    }

    public function sede()
    {
        return $this->belongsTo('App\Sede', 'sede_id', 'sede_id');
    }
}

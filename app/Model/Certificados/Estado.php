<?php

namespace App\Model\Certificados;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = 'certificados';
    protected $primaryKey = 'estado_id';
    protected $table = 'certificados_estados';
    protected $fillable = [
    						'estado_id',
    						'estado_codigo',
    						'estado_nombre',
    						'estado_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('\App\Model\Certificados\Solicitud', 'solicitud_id');
    }
}

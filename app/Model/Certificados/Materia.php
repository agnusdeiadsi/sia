<?php

namespace App\Model\Certificados;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $connection = 'certificados';
    protected $primaryKey = 'materia_id';
    protected $table = 'certificados_materias';
    protected $fillable = [
    						'materia_id',
    						'materia_nombre',
    						'solicitud_id',
    					];
   	public $timestamps = false;

   	public function solicitud()
    {
    	return $this->belongsTo('\App\Model\Certificados\Solicitud', 'solicitud_id');
    }
}

<?php

namespace App\Model\Certificados;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $connection = 'certificados';
	protected $keyType = 'varchar';
   	protected $primaryKey = 'solicitud_id';
   	protected $table = 'certificados_solicitudes';
   	protected $fillable = [
							  'solicitud_id',
							  'solicitud_codigo_formulario',
							  'solicitud_version_formulario',
							  'solicitud_codigo_liquidacion',
							  'solicitud_comprobante_liquidacion',
							  'matriculado_id',
							  'solicitud_matriculado_identificacion',
							  'solicitud_matriculado_nombres',
							  'solicitud_matriculado_apellidos',
							  'solicitud_matriculado_semestre',
							  'solicitud_matriculado_email',
							  'solicitud_matriculado_telefono',
							  'solicitud_matriculado_celular',
							  'solicitud_foto_carnet',
							  'solicitud_documento_carnet',
							  'solicitud_url_denuncio_carnet',
							  'solicitud_identificacion_reclamante',
							  'solicitud_nombres_reclamante',
							  'solicitud_apellidos_reclamante',
							  'solicitud_carta_autorizacion',
							  'solicitud_observaciones',
							  'solicitud_valor_adeudado',
							  'solicitud_anio_declaracion',
							  'solicitud_condiciones',
							  'solicitud_fecha_impresion',
							  'solicitud_usuario_impresion',
							  'solicitud_fecha_recibido',
							  'solicitud_usuario_recibido',
							  'solicitud_fecha_entrega',
							  'solicitud_usuario_entrega',
							  'tipo_id',
							  'subarea_id',
							  'uso_id',
							  'otro_uso',
							  'estado_id',
							  'sede_id',
							  'tipo_estudiante',
							  'periodo_id',
							  'created_at',
							  'updated_at',
   						];

    public $incrementing = false;

    /*
    *	Busca una solicitud por codigo de liquidacion y codigo matriculado
    *	o por id solicitud y codigo matriculado
    */
    public function scopeBuscar($query, $matriculado, $valor)
    {
    	return $query->where([
    		['certificados_solicitudes.matriculado_id', '=', $matriculado],
        	['certificados_solicitudes.solicitud_id', '=', $valor],        	
      	])
    	->orWhere([
    		['certificados_solicitudes.matriculado_id', '=', $matriculado],
        	['certificados_solicitudes.solicitud_codigo_liquidacion', '=', $valor],
      	]);
    }

    public function scopeFiltrar($query, $valor)
    {
        return $query->where('certificados_solicitudes.solicitud_id', '=', $valor)
        ->orWhere('certificados_solicitudes.matriculado_id', 'LIKE', '%'.$valor.'%')
        ->orWhere('certificados_solicitudes.solicitud_matriculado_nombres', 'LIKE', '%'.$valor.'%')
        ->orWhere('certificados_solicitudes.solicitud_codigo_liquidacion', 'LIKE', '%'.$valor.'%')
        ->orWhere('certificados_tipos.tipo_nombre', 'LIKE', '%'.$valor.'%');
    }

    public function materias()
    {
        return $this->hasMany('\App\Model\Certificados\Materia', 'solicitud_id');
    }

    public function tipo()
    {
    	return $this->belongsTo('\App\Model\Certificados\Tipo', 'tipo_id');
    }

    public function sede()
    {
    	return $this->belongsTo('\App\Sede', 'sede_id');
    }

    public function semestre()
    {
    	return $this->belongsTo('\App\Semestre', 'solicitud_matriculado_semestre');
    }

    public function estado()
    {
    	return $this->belongsTo('\App\Model\Certificados\Estado', 'estado_id');
    }

    public function usuarioimprime()
    {
    	return $this->belongsTo('\App\Usuario', 'solicitud_usuario_imprime');
    }

    public function usuariorecibe()
    {
    	return $this->belongsTo('\App\Usuario', 'solicitud_usuario_recibe');
    }

    public function usuarioentrega()
    {
    	return $this->belongsTo('\App\Usuario', 'solicitud_usuario_entrega');
    }			
}

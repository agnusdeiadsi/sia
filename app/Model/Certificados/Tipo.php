<?php

namespace App\Model\Certificados;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $connection = 'certificados';
    protected $primaryKey = 'tipo_id';
    protected $table = 'certificados_tipos';
    protected $fillable = [
    						'tipo_id',
    						'tipo_codigo',
    						'tipo_nombre',
    						'tipo_descripcion',
    					];
   	public $timestamps = false;

   	public function solicitudes()
    {
    	return $this->hasMany('\App\Model\Certificados\Solicitud', 'solicitud_id');
    }
}

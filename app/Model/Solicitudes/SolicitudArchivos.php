<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class SolicitudArchivos extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'solicitudArchivos_id';
    protected $table = 'solicitud_archivos';
    protected $fillable = ['solicitudArchivos_id',
							'solicitud_id',
							'solicitudArchivos_ruta',
                            'solicitudDocumento_id',
							'solicitudArchivos_nombre',
							'solicitudArchivos_tipo'];

	//public $timestamps = false;

	public function solicitud()
    {
        return $this->belongsTo('App\Model\Solicitudes\solicitud', 'solicitud_id', 'solicitud_id');
    }

    public function documento()
    {
        return $this->belongsTo('App\Model\Solicitudes\SolicitudDocumentos', 'solicitudDocumento_id',                                                                 'solicitudDocumento_id');
    }

}

<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class SolicitudDocumentos extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'solicitudDocumento_id';
    protected $table = 'solicitud_documento';
    protected $fillable = ['solicitudDocumento_id',
							'solicitudDocumento_nombre',
							'solicitudDocumento_descripcion'];


	public function archivo()
	{
		return $this->hasMany('App\Model\Solicitudes\SolicitudArchivos', 
							'solicitudDocumento_id', 'solicitudDocumento_id');
	}
}

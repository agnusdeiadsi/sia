<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'solicitud_id';
    protected $table = 'solicitud';
    protected $fillable = ['solicitud_id',
							'tipoSolicitud_id',
							'matriculado_pidm',
                            'programa_id',
							'solicitud_descripcion'];


	public function matriculado()
    {
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm', 'matriculado_pidm');
    }

    public function archivos()
    {
        return $this->hasMany('App\Model\Solicitudes\SolicitudArchivos', 'solicitud_id', 'solicitud_id');
    }

    public function faseEstado()
    {
        return $this->hasMany('App\Model\Solicitudes\SolicitudFaseEstado', 'solicitud_id', 'solicitud_id');
    }

}

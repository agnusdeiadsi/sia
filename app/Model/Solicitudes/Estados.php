<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'estado_id';
    protected $table = 'solicitud_estados';
    protected $fillable = ['estado_id',
							'estado_nombre'];
}

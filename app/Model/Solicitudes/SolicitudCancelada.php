<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class SolicitudCancelada extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'solicitudCancelada_id';
    protected $table = 'solicitud_cancelada';
    protected $fillable = ['solicitudCancelada_id',
							'solicitud_id',
							'user_id',
							'subarea_id',
							'solicitudCancelada_descripcion'];
}

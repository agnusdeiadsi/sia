<?php

namespace App\Model\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class SolicitudFaseEstado extends Model
{
    protected $connection = 'solicitudes';
    protected $primaryKey = 'solicitud_fase_estado_id';
    protected $table = 'solicitud_fase_estado';
    protected $fillable = ['solicitud_fase_estado_id',
							'solicitud_id',
							'estado_id',
                            'subarea_id',
							'user_id'];


	public function solicitud()
	{
		return $this->belongsTo('App\Model\Solicitudes\Solicitud', 'solicitud_id', 'solicitud_id');
	}
}

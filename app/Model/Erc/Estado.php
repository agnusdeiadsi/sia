<?php

namespace App\Model\Erc;

use Illuminate\Database\Eloquent\Model;

class Estado_erc extends Model
{
	protected $connection = 'erc';
	protected $primaryKey = 'estado_id';
    protected $table = "erc_estados";
    protected $fillable = ['estado_id, estado_codigo', 'estado_nombre', 'estado_descripcion'];
    public $incrementing = false;

    public function recibos()
    {
    	return $this->hasMany('App\Model\Erc\Recibo'); // un estado tiene muchos recibos
    }
}

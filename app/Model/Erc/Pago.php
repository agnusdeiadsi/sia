<?php

namespace App\Model\erc;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $connection = 'erc';
    protected $table = 'erc_pagos';
    protected $primaryKey = 'pago_id';
    protected $fillable = ['pago_id', 'pago_codigo', 'pago_nombre', 'pago_descripcion'];
    public $timestamps = false;

    public function recibos()
    {
      return $this->belongsToMany('App\Model\Erc\Recibo', 'erc_recibo_pago', 'pago_id', 'recibo_id');
      //->withPivot('recibopago_id', 'recibopago_entidad_bancaria', 'recibopago_codigo', 'recibopago_plaza', 'recibopago_valor');
    }
}

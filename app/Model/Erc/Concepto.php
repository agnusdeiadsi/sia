<?php

namespace App\Model\Erc;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $connection = 'erc';
    protected $primaryKey = 'recibo_id';
    protected $table = "erc_recibo_concepto";
    protected $fillable = [
                            'concepto_matricula',
                            'concepto_carnet',
                            'concepto_precooperativa',
                            'concepto_curso_verano',
                            'concepto_certificado',
                            'concepto_derechos_grado',
                            'concepto_habilitacion',
                            'concepto_seguro_estudiantil',
                            'concepto_estampilla',
                            'concepto_inscripcion',
                            'concepto_homologacion',
                            'concepto_otros',
                            'concepto_total',
                            'recibo_id'
                          ];
    public $timestamps = false;

    public function recibo() // relacion de uno a uno con la tabla erc_recibos
    {
    	return $this->belongsTo('App\Model\Erc\Recibo', 'recibo_id'); // un concepto pertenece a un recibo
    }
}

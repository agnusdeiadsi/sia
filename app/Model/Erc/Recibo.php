<?php

namespace App\Model\Erc;

use Illuminate\Database\Eloquent\Model;

class Recibo extends Model
{
    protected $connection = 'erc';
    protected $primaryKey = 'recibo_id';
    protected $table = "erc_recibos";
    protected $fillable = [
                            'recibo_id',
                            'recibo_consecutivo_centro',
                            'centrooperacion_id',
                            'recibo_codigo_liquidacion',
                            'recibo_cliente_identificacion',
                            'recibo_cliente_nombres',
                            'recibo_cliente_programa',
                            'recibo_cliente_direccion',
                            'recibo_cliente_telefono',
                            'recibo_cliente_ciudad',
                            'recibo_observaciones',
                            'recibo_created_usuario',
                            'recibo_created_at',
                            'recibo_updated_usuario',
                            'recibo_updated_at',
                            'estado_id',
                          ];
    public $timestamps = false;

    public function scopeBuscar($query, $valor)
    {
      return $query->where("centrooperacion_nombre", "LIKE", "%$valor%")
                    ->orWhere('erc_recibos.centrooperacion_id', '=', "$valor")
                    ->orWhere('sia_usuarios.email', 'LIKE', "%$valor%")
                    ->orWhere('erc_recibos.recibo_consecutivo_centro', 'LIKE', "%$valor%")
                    ->orWhere('erc_recibos.recibo_created_at', 'LIKE', "%$valor%")
                    ->orWhere('erc_recibos.recibo_codigo_liquidacion', 'LIKE', "%$valor%")
                    ->orWhere('erc_recibos.recibo_cliente_identificacion', 'LIKE', "%$valor%");
    }

    public function scopeFiltro($query, $estado)
    {
      return $query->where("erc_recibos.estado_id", "=", "$estado");
    }

    public function estado() //relacion de uno a muchos con la tabla erc_estados
    {
    	return $this->belongsTo('App\Model\Erc\Estado'); // un recibo tiene un estado
    }

    public function centroOperacion() //relacion de uno a muchos con la tabla sia_centros_operacion
    {
    	return $this->belongsTo('App\CentroOperacion', 'centrooperacion_id'); //muchos recibos pertenecen a un centro de operacion
    }

    public function matriculado() //relacion de uno a muchos con la tabla sia_matriculados
    {
    	return $this->belongsTo('App\Matriculado', 'recibo_cliente_identificacion'); // muchos recibos pertenecen a un matriculado
    }

    public function concepto() //relacion de uno a uno con la tabla erc_recibo_concepto
    {
    	return $this->hasOne('App\Model\Erc\Concepto', 'recibo_id'); //un recibo tiene un concepto
    }

    public function pagos()
    {
      return $this->belongsToMany('App\Model\Erc\Pago', 'erc_recibo_pago', 'recibo_id', 'pago_id')
      ->withPivot('recibopago_id', 'recibopago_entidad_bancaria', 'recibopago_codigo', 'recibopago_plaza', 'recibopago_valor');
    }

    public function usuario()
    {
      return $this->belongsTo('App\Usuario', 'recibo_created_usuario');
    }

    public function usuarioUpdate()
    {
      return $this->belongsTo('App\Usuario', 'recibo_updated_usuario');
    }
}

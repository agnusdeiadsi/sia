<?php

namespace App;

use App\Notifications\BrowseResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Browse extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = 'browse';
    protected $table = 'sia_usuarios';
    protected $fillable = [
        'email', 'password', 'rol_sia',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new BrowseResetPassword($token));
    }

    public function cuenta() // relacion de uno a uno con la tabla correos_cuentas
    {
        return $this->belongsTo('App\Model\Correos\Cuenta', 'id', 'cuenta_id'); // un usuario pertenece a una cuenta
    }
}

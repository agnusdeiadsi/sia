<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroOperacion extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'centrooperacion_id';
    protected $table = "sia_centros_operacion";
    protected $fillable = ['centrooperacion_id', 'centrooperacion_codigo', 'centrooperacion_nombre', 'centrooperacion_descripcion'];

    public function recibos() //relacion de uno a muchos con la tabla erc_recibos
    {
    	return $this->hasMany('App\Model\Erc\Recibo', 'recibo_id'); // un centro de operacion tiene muchos recibos
    }

    public function SolicitudesCompras()
    {
    	return $this->belongsToMany('App\Model\Compras\Solicitud', 'compras_solicitud_centro_operacion','centrooperacion_id', 'solicitud_id');
    }
}

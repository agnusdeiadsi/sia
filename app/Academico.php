<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Academico extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'academico_id';
    protected $table = 'sia_academico';
    protected $fillable = ['academico_id', 
                            'matriculado_pidm', 
                            'academico_codigo', 
                            'academico_fecha_inicio', 
                            'academico_fecha_finalizacion', 
                            'convenio_id', 
                            'sede_id', 
                            'periodo_id', 
                            'programa_id', 
                            'academico_programa_alfa', 
                            'academico_tipo'];

    //public $timestamps = false;
    //public $incrementing = false;

    //selecciona los periodos unicos de sia_academico
    public function scopePeriodos($query, $programa)
    {
        if($programa != null)
        {
            /*return $query->where('programa_id', '=', $programa)
                        ->groupBy('periodo_id');*/
            return $query->select('*')
                        ->from('sia_academico')
                        ->where('programa_id', '=', $programa);
        }
    }

    public function matriculado()
    {
        return $this->belongsTo('App\Matriculado', 'matriculado_pidm');
    }

    public function programa()
    {
        return $this->belongsTo('App\Programa', 'programa_id');
    }

    public function sede()
    {
        return $this->belongsTo('App\Sede', 'sede_id');
    }
}

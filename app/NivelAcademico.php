<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelAcademico extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'nivelacademico_id';
    protected $table = 'sia_niveles_academicos';
    protected $fillable = [
                            'nivelacademico_id', 
                            'nivelacademico_codigo', 
                            'nivelacademico_nombre', 
                            'nivelacademico_descripcion'
                           ];

    public function estudios($value='')
    {
    	return $this->hasMany('App\Model\Egresado\Estudio', 'nivelacademico_id');
    }
}

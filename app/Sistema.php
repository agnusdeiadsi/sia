<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'sistema_id';
    protected $table = "sia_sistemas";
    protected $fillable = [
                            'sistema_id', 
                            'sistema_codigo', 
                            'sistema_nombre', 
                            'sistema_nombre_corto', 
                            'sistema_descripcion', 
                            'sistema_released', 
                            'sistema_version', 
                            'sistema_icon', 
                            'sistema_manual', 
                            'sistema_colorclass', 
                            'sistema_entrances', 
                            'sistema_desarrollador', 
                            'updated_at', 
                            'created_at', 
                            'subarea_id',
                            'enabled',
                          ];

    public function scopeBuscar($query, $valor)
    {
      return $this->where('sistema_nombre', 'LIKE', '%'.$valor.'%');
    }

    public function roles()
    {
    	return $this->belongsToMany('App\Rol', 'sia_sistema_rol', 'sistema_id', 'rol_id')
        ->withPivot('sistemarol_id');
    }

    public function usuarios()
    {
    	return $this->belongsToMany('App\Usuario', 'sia_sistema_rol', 'sistema_id', 'id');
    }

    public function subarea()
    {
      return $this->belongsTo('App\Subarea', 'subarea_id');
    }

    public function entrances()
    {
      return $this->hasMany('App\Entrance', 'sistema_id', 'sistema_id');
    }

    public function Sistemaroles()
    {
      return $this->hasMany('App\Sistemarol', 'sistema_id', 'sistema_id');
    }
}

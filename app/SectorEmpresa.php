<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectorEmpresa extends Model
{
    protected $connection = 'sia';
    protected $primaryKey = 'sectorempresa_id';
    protected $table = "sia_sectores_empresas";
    protected $fillable = ['sectorempresa_id', 'sectorempresa_codigo', 'sectorempresa_nombre', 'sectorempresa_descripcion'];
    public $timestamps = false;
    
  public function emprendimiento()
  {
  	return $this->belongsTo('App\Model\Egresados\Emprendimiento', 'sectorempresa_id');	
  }
}

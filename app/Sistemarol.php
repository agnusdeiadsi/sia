<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistemarol extends Model
{
    protected $connection = 'sia';
	protected $primaryKey = 'sistemarol_id';
    protected $table = "sia_sistema_rol";
    protected $fillable = ['sistemarol_id', 'sistema_id', 'rol_id'];
	
    public $timestamps = false;

    public function permisos()
    {
    	return $this->belongsToMany('App\Permiso', 'sia_permisos', 'sistemarol_id', 'sistemarol_id');
    }

    public function sistema()
    {
        return $this->belongsTo('App\Sistema', 'sistema_id', 'sistema_id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Rol', 'rol_id', 'rol_id');
    }

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'sia_permisos', 'sistemarol_id', 'usuario_id');
    }

    /*public function permisos()
    {
    	return $this->belongsTo('App\Sistema', 'sistemarol_id', 'sistema_id');
    }*/
}

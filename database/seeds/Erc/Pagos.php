<?php

use Illuminate\Database\Seeder;

class Pagos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('erc')->table('erc_pagos')->insert([
		  'pago_id' => '1',
		  'pago_codigo' => 'EFCT',
		  'pago_nombre' => 'Efectivo',
		  'pago_descripcion' => null
		]);

		DB::connection('erc')->table('erc_pagos')->insert([
		  'pago_id' => '2',
		  'pago_codigo' => 'TRJT',
		  'pago_nombre' => 'Tarjeta',
		  'pago_descripcion' => null
		]);

		DB::connection('erc')->table('erc_pagos')->insert([
		  'pago_id' => '3',
		  'pago_codigo' => 'CHQ',
		  'pago_nombre' => 'Cheque',
		  'pago_descripcion' => null
		]);

		DB::connection('erc')->table('erc_pagos')->insert([
		  'pago_id' => '4',
		  'pago_codigo' => 'TRNSF',
		  'pago_nombre' => 'Transferencia',
		  'pago_descripcion' => null
		]);
    }
}

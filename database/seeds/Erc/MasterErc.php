<?php

use Illuminate\Database\Seeder;

class MasterErc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstadosErc::class);
        $this->call(Pagos::class);
    }
}

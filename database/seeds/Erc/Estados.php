<?php

use Illuminate\Database\Seeder;

class EstadosErc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('erc')->table('erc_estados')->insert([
		  'estado_id' => '1',
		  'estado_codigo' => 'NCONT',
		  'estado_nombre' => 'No Contabilizado',
		  'estado_descripcion' => null
		]);

		DB::connection('erc')->table('erc_estados')->insert([
		  'estado_id' => '2',
		  'estado_codigo' => 'CONT',
		  'estado_nombre' => 'Contabilizado',
		  'estado_descripcion' => null
		]);
    }
}

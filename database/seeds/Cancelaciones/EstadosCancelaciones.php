<?php

use Illuminate\Database\Seeder;

class EstadosCancelaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cancelaciones')->table('cancelaciones_estados')->insert([
		  'estado_id' => '1',
		  'estado_codigo' => 'PROC',
		  'estado_nombre' => 'Procesamiento',
		  'estado_descripcion' => null
		]);

		DB::connection('cancelaciones')->table('cancelaciones_estados')->insert([
		  'estado_id' => '2',
		  'estado_codigo' => 'EST',
		  'estado_nombre' => 'Estudio',
		  'estado_descripcion' => null
		]);

		DB::connection('cancelaciones')->table('cancelaciones_estados')->insert([
		  'estado_id' => '3',
		  'estado_codigo' => 'PRDA',
		  'estado_nombre' => 'Procesada',
		  'estado_descripcion' => null
		]);
    }
}

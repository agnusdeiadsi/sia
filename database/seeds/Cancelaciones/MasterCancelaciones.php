<?php

use Illuminate\Database\Seeder;

class MasterCancelaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TiposCancelaciones::class);
        $this->call(EstadosCancelaciones::class);
    }
}

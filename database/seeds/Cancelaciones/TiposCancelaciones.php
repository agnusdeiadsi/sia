<?php

use Illuminate\Database\Seeder;

class TiposCancelaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cancelaciones')->table('cancelaciones_tipos')->insert([
		  'tipo_id' => '1',
		  'tipo_codigo' => 'DEV',
		  'tipo_nombre' => 'Devolución',
		  'tipo_descripcion' => null
		]);

		DB::connection('cancelaciones')->table('cancelaciones_tipos')->insert([
		  'tipo_id' => '2',
		  'tipo_codigo' => 'ACRED',
		  'tipo_nombre' => 'Anulación de Crédito',
		  'tipo_descripcion' => null
		]);

		DB::connection('cancelaciones')->table('cancelaciones_tipos')->insert([
		  'tipo_id' => '3',
		  'tipo_codigo' => 'APLZ',
		  'tipo_nombre' => 'Aplazamiento',
		  'tipo_descripcion' => null
		]);

		DB::connection('cancelaciones')->table('cancelaciones_tipos')->insert([
		  'tipo_id' => '4',
		  'tipo_codigo' => 'CCTA',
		  'tipo_nombre' => 'Cruce de Cuentas',
		  'tipo_descripcion' => null
		]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MasterCertificados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TiposCertificados::class);
        $this->call(EstadosCertificados::class);
        $this->call(Periodos::class);
    }
}

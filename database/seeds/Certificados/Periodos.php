<?php

use Illuminate\Database\Seeder;

class Periodos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('certificados')->table('certificados_periodos')->insert([
		  'periodo_id' => '1',
		  'periodo_codigo' => '201710',
		  'periodo_nombre' => '2017-1',
		  'periodo_descripcion' => null
		]);
    }
}

<?php

use Illuminate\Database\Seeder;

class TiposCertificados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '1',
		  'tipo_codigo' => 'CEST',
		  'tipo_nombre' => 'Estudio',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '2',
		  'tipo_codigo' => 'CNOT',
		  'tipo_nombre' => 'Notas',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '3',
		  'tipo_codigo' => 'CNOTS',
		  'tipo_nombre' => 'Notas Todos los Semestres',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '4',
		  'tipo_codigo' => 'CHOR',
		  'tipo_nombre' => 'Horario',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '5',
		  'tipo_codigo' => 'CHORC',
		  'tipo_nombre' => 'Horario Toda la Carrera',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '6',
		  'tipo_codigo' => 'CCOND',
		  'tipo_nombre' => 'Conducta',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '7',
		  'tipo_codigo' => 'CTERA',
		  'tipo_nombre' => 'Terminó Asignatura',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '8',
		  'tipo_codigo' => 'CFFP',
		  'tipo_nombre' => 'Financiero: Fondo de Pensiones',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '9',
		  'tipo_codigo' => 'CFPS',
		  'tipo_nombre' => 'Financiero: Paz y Salvo',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '10',
		  'tipo_codigo' => 'CCPP',
		  'tipo_nombre' => 'Ciclos Propedéuticos',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '11',
		  'tipo_codigo' => 'CCPRO',
		  'tipo_nombre' => 'Ciclo Profesional',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '12',
		  'tipo_codigo' => 'CDTT',
		  'tipo_nombre' => 'Doble Titulación',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '13',
		  'tipo_codigo' => 'CEGRD',
		  'tipo_nombre' => 'Egresado-Grado',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '14',
		  'tipo_codigo' => 'CCPMAT',
		  'tipo_nombre' => 'Contenido Programático Materia',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '15',
		  'tipo_codigo' => 'CPCAR',
		  'tipo_nombre' => 'Contenido Programático Carrera',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '16',
		  'tipo_codigo' => 'CNTEST',
		  'tipo_nombre' => 'Carnet Estudiantil',
		  'tipo_descripcion' => null
		]);

		DB::connection('certificados')->table('certificados_tipos')->insert([
		  'tipo_id' => '17',
		  'tipo_codigo' => 'CFDRTA',
		  'tipo_nombre' => 'Financiero: Declaración de Renta',
		  'tipo_descripcion' => null
		]);
    }
}

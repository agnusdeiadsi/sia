<?php

use Illuminate\Database\Seeder;

class EstadosCertificados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('certificados')->table('certificados_estados')->insert([
		  'estado_id' => '1',
		  'estado_codigo' => 'PROC',
		  'estado_nombre' => 'Procesamiento',
		  'estado_descripcion' => 'El certificado se encuentra en espera para para ser impreso.'
		]);

		DB::connection('certificados')->table('certificados_estados')->insert([
		  'estado_id' => '2',
		  'estado_codigo' => 'TRAN',
		  'estado_nombre' => 'Transporte',
		  'estado_descripcion' => 'El certificado ha sido impreso y se está transportando hacia la sede de reclamo.'
		]);

		DB::connection('certificados')->table('certificados_estados')->insert([
		  'estado_id' => '3',
		  'estado_codigo' => 'SEDE',
		  'estado_nombre' => 'En Sede',
		  'estado_descripcion' => 'El certificado se encuentra en la sede listo para ser reclamado.'
		]);

		DB::connection('certificados')->table('certificados_estados')->insert([
		  'estado_id' => '4',
		  'estado_codigo' => 'ENTR',
		  'estado_nombre' => 'Entregado',
		  'estado_descripcion' => 'El certificado ha sido entregado al estudiante o acudiente.'
		]);
    }
}

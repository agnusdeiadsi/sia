<?php

use Illuminate\Database\Seeder;

class EstadosCompras extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '1',
        	'estado_codigo' => 'PROC',
        	'estado_nombre' => 'Procesamiento',
        	'estado_descripcion' => null
        ]);

        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '2',
        	'estado_codigo' => 'EVAL',
        	'estado_nombre' => ' Evaluación / Comité',
        	'estado_descripcion' => null
        ]);

        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '3',
        	'estado_codigo' => 'APLA',
        	'estado_nombre' => 'Aplazada',
        	'estado_descripcion' => null
        ]);

        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '4',
        	'estado_codigo' => 'APRO',
        	'estado_nombre' => 'Aprobado',
        	'estado_descripcion' => 'Estado para insumo'
        ]);

        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '5',
        	'estado_codigo' => 'RECH',
        	'estado_nombre' => 'Rechazado',
        	'estado_descripcion' => 'Estado para insumo'
        ]);

        DB::connection('compras')->table('compras_estados')->insert([
        	'estado_id' => '6',
        	'estado_codigo' => 'ARCH',
        	'estado_nombre' => 'Archivo',
        	'estado_descripcion' => null
        ]);
    }
}

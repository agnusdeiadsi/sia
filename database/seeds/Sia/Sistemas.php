<?php

use Illuminate\Database\Seeder;

class Sistemas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_sistemas')->insert([
            'sistema_id' => '1',
            'sistema_codigo' => 'dir2015',
            'sistema_nombre' => 'Directorio IP',
            'sistema_nombre_corto' => 'directorio',
            'sistema_descripcion' => 'Directorio de Extensiones.',
            'sistema_released' => '2015-06-01',
            'sistema_version' => '1.0',
            'sistema_icon' => 'dir2015_1516742679.png',
            'sistema_manual' => null,
            'sistema_colorclass' => 'vivid-blue',
            'sistema_desarrollador' => null,
            'sistema_entrances' => null,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
            'subarea_id' => 1,
            'colorclass_id' => null,
            'enabled' => 1,
        ]);

        DB::table('sia_sistemas')->insert([
            'sistema_id' => '2',
            'sistema_codigo' => 'eml2015',
            'sistema_nombre' => 'Correos Institucionales',
            'sistema_nombre_corto' => 'correos',
            'sistema_descripcion' => 'Solicitud de Correos institucionales para docentes y administrativos.',
            'sistema_released' => '2015-06-01',
            'sistema_version' => '1.0',
            'sistema_icon' => 'eml2015_1516742773.png',
            'sistema_manual' => null,
            'sistema_colorclass' => 'highway-blue',
            'sistema_desarrollador' => 'Yeison Rodríguez',
            'sistema_entrances' => null,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
            'subarea_id' => 1,
            'colorclass_id' => null,
            'enabled' => 1,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class Contratos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_contratos')->insert([
            'contrato_id' => '1',
            'contrato_codigo' => 'TC',
            'contrato_nombre' => 'Tiempo Completo',
            'contrato_descripcion' => null
        ]);

        DB::table('sia_contratos')->insert([
            'contrato_id' => '2',
            'contrato_codigo' => 'MT',
            'contrato_nombre' => 'Medio Tiempo',
            'contrato_descripcion' => null
        ]);

        DB::table('sia_contratos')->insert([
            'contrato_id' => '3',
            'contrato_codigo' => 'HC',
            'contrato_nombre' => 'Hora Cátedra',
            'contrato_descripcion' => null
        ]);

        DB::table('sia_contratos')->insert([
            'contrato_id' => '4',
            'contrato_codigo' => 'TF',
            'contrato_nombre' => 'Término Fijo',
            'contrato_descripcion' => null
        ]);

        DB::table('sia_contratos')->insert([
            'contrato_id' => '5',
            'contrato_codigo' => 'TI',
            'contrato_nombre' => 'Término Indefinido',
            'contrato_descripcion' => null
        ]);
    }
}

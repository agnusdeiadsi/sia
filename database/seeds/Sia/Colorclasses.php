<?php

use Illuminate\Database\Seeder;

class Colorclasses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//completar
        DB::table('sia_colorclasses')->insert([
            'colorclass_id' => '1',
            'colorclass_codigo' => 'vivid',
            'colorclass_nombre' => 'vivid-blue',
            'colorclass_descripcion' => 'w3-vivid-blue'
        ]);

        DB::table('sia_colorclasses')->insert([
            'colorclass_id' => '2',
            'colorclass_codigo' => 'highway',
            'colorclass_nombre' => 'highway-blue',
            'colorclass_descripcion' => 'w3-highway-blue'
        ]);
    }
}

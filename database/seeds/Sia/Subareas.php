<?php

use Illuminate\Database\Seeder;

class Subareas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('sia_subareas')->insert([
            'subarea_id' => '1',
            'subarea_codigo' => 'DEF',
            'subarea_nombre' => 'Default',
            'subarea_descripcion' => null,
            'area_id' => 1,
        ]);
    }
}

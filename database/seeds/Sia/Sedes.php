<?php

use Illuminate\Database\Seeder;

class Sedes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_sedes')->insert([
        	[
                'sede_id' => '1',
                'sede_codigo' => 'PAN',
                'sede_nombre' => 'Pance',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '2',
                'sede_codigo' => 'MEL',
                'sede_nombre' => 'Meléndez',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '3',
                'sede_codigo' => 'ALPZ',
                'sede_nombre' => 'Alfonso López',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '4',
                'sede_codigo' => 'YUM',
                'sede_nombre' => 'Yumbo',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '5',
                'sede_codigo' => 'COMP',
                'sede_nombre' => 'Compartir',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '6',
                'sede_codigo' => 'JAM',
                'sede_nombre' => 'Jamundí',
                'sede_descripcion' => null
            ],
            [
                'sede_id' => '7',
                'sede_codigo' => 'CENT',
                'sede_nombre' => 'Centro',
                'sede_descripcion' => null
            ],
        ]);
    }
}

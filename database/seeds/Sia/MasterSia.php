<?php

use Illuminate\Database\Seeder;

class MasterSia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Sedes::class);
        $this->call(Semestres::class);
        $this->call(Roles::class);
        $this->call(Usuarios::class);
        $this->call(Areas::class);
        $this->call(Subareas::class);
        $this->call(Colorclasses::class);
        $this->call(Sistemas::class);
        $this->call(Contratos::class);
    }
}

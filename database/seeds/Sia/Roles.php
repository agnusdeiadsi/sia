<?php

use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_roles')->insert([
        	[
                'rol_id' => '1',
                'rol_codigo' => 'ADM',
                'rol_nombre' => 'Administrador',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '2',
                'rol_codigo' => 'MAN',
                'rol_nombre' => 'Manager',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '3',
                'rol_codigo' => 'AUT',
                'rol_nombre' => 'Autorizante',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '4',
                'rol_codigo' => 'SOL',
                'rol_nombre' => 'Solicitante',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '5',
                'rol_codigo' => 'NIN',
                'rol_nombre' => 'Ninguno',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '6',
                'rol_codigo' => 'SEC',
                'rol_nombre' => 'Secretaria',
                'rol_descripcion' => 'Secretaria Certificados'
            ],
            [
                'rol_id' => '7',
                'rol_codigo' => 'REV',
                'rol_nombre' => 'Revisor',
                'rol_descripcion' => 'Revisor Compras'
            ],
            [
                'rol_id' => '8',
                'rol_codigo' => 'IMPR',
                'rol_nombre' => 'Impresor',
                'rol_descripcion' => 'Persona que imprime Certificados'
            ],
            [
                'rol_id' => '9',
                'rol_codigo' => 'FAC',
                'rol_nombre' => 'Facturador',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '10',
                'rol_codigo' => 'CART',
                'rol_nombre' => 'Cartera',
                'rol_descripcion' => null
            ],
            [
                'rol_id' => '11',
                'rol_codigo' => 'INV',
                'rol_nombre' => 'Invitado',
                'rol_descripcion' => 'Tiene acceso básico al módulo.'
            ],
            [
                'rol_id' => '12',
                'rol_codigo' => 'SREV',
                'rol_nombre' => 'Super Revisor',
                'rol_descripcion' => 'No tiene las limitaciones de un reviisor normal.'
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class Semestres extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_semestres')->insert([
            'semestre_id' => '1',
            'semestre_codigo' => 'I',
            'semestre_nombre' => 'Primero',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '2',
            'semestre_codigo' => 'II',
            'semestre_nombre' => 'Segundo',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '3',
            'semestre_codigo' => 'III',
            'semestre_nombre' => 'Tercero',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '4',
            'semestre_codigo' => 'IV',
            'semestre_nombre' => 'Cuarto',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '5',
            'semestre_codigo' => 'V',
            'semestre_nombre' => 'Quinto',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '6',
            'semestre_codigo' => 'VI',
            'semestre_nombre' => 'Sexto',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '7',
            'semestre_codigo' => 'VII',
            'semestre_nombre' => 'Séptimo',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '8',
            'semestre_codigo' => 'VIII',
            'semestre_nombre' => 'Octavo',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '9',
            'semestre_codigo' => 'IX',
            'semestre_nombre' => 'Noveno',
            'semestre_descripcion' => null
        ]);

        DB::table('sia_semestres')->insert([
            'semestre_id' => '10',
            'semestre_codigo' => 'X',
            'semestre_nombre' => 'Décimo',
            'semestre_descripcion' => null
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class Areas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('sia_areas')->insert([
            'area_id' => '1',
            'area_codigo' => 'DEF',
            'area_nombre' => 'Default',
            'area_descripcion' => null
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class Usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sia_usuarios')->insert([
            'id' => '1',
            'email' => 'sia@unicatolica.edu.co',
            'password' => bcrypt('system2000'),
            'rol_sia' => 'Administrador',
            'enabled' => 1,
        ]);
    }
}

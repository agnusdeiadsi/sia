<?php

use Illuminate\Database\Seeder;

class EstadosCorreos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '1',
		  'estado_codigo' => 'PROC',
		  'estado_nombre' => 'Procesamiento',
		  'estado_descripcion' => 'La solicitud se encuentra en Procesamiento.'
		]);

		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '2',
		  'estado_codigo' => 'ACT',
		  'estado_nombre' => 'Activo',
		  'estado_descripcion' => 'La dirección de correo ha sido creada en el servidor o activada nuevamente.'
		]);

		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '3',
		  'estado_codigo' => 'RES',
		  'estado_nombre' => 'Restablecer Contraseña',
		  'estado_descripcion' => 'El usuario ha solicitado restablecer la contraseña del correo.'
		]);

		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '4',
		  'estado_codigo' => 'DES',
		  'estado_nombre' => 'Inactivo',
		  'estado_descripcion' => 'El correo se encuentra Inactivo en el servidor.'
		]);

		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '5',
		  'estado_codigo' => 'DESA',
		  'estado_nombre' => 'Sin actualizar datos',
		  'estado_descripcion' => 'El usuario no ha actualizado sus datos personales.'
		]);

		DB::connection('correos')->table('correos_estados')->insert([
		  'estado_id' => '6',
		  'estado_codigo' => 'ACTD',
		  'estado_nombre' => 'Actualizar datos',
		  'estado_descripcion' => 'El usuario ha actualizado sus datos personales y ahora debe actualizar la información en el servidor.'
		]);
    }
}

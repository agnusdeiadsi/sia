<?php

use Illuminate\Database\Seeder;

class Cuentas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('correos')->table('correos_cuentas')->insert([
          'cuenta_id' => '1',
          'cuenta_usuario' => 'sia',
          'cuenta_cuenta' => 'sia@unicatolica.edu.co',
          'tipo_id' => '1', //administrativo
          'estado_id' => '2', //Activo
      ]);
    }
}

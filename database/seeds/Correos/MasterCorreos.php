<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MasterCorreos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstadosCorreos::class);
        $this->call(TiposCorreos::class);
        $this->call(Cuentas::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class TiposCorreos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('correos')->table('correos_tipos')->insert([
        'tipo_id' => '1',
        'tipo_codigo' => 'ADM',
        'tipo_nombre' => 'Administrativo',
        'tipo_descripcion' => 'Correos para cargos administrativos.'
    	]);

    	DB::connection('correos')->table('correos_tipos')->insert([
        'tipo_id' => '2',
        'tipo_codigo' => 'DOC',
        'tipo_nombre' => 'Docencia',
        'tipo_descripcion' => 'Correos para docentes.'
    	]);

  		DB::connection('correos')->table('correos_tipos')->insert([
  		  'tipo_id' => '3',
  		  'tipo_codigo' => 'EST',
  		  'tipo_nombre' => 'Estudiantil',
  		  'tipo_descripcion' => 'Correos para estudiantes de laboratorio.'
  		]);

  		DB::connection('correos')->table('correos_tipos')->insert([
  		  'tipo_id' => '4',
  		  'tipo_codigo' => 'OTR',
  		  'tipo_nombre' => 'Otro',
  		  'tipo_descripcion' => null
  		]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Correos
        $this->call(MasterCorreos::class);

        //Sia
        $this->call(MasterSia::class);

        //Certificados
        $this->call(MasterCertificados::class);

        //e-RC
        $this->call(MasterErc::class);

        //Compras
        $this->call(MasterCompras::class);

        //Cancelaciones
        $this->call(MasterCancelaciones::class);
    }
}

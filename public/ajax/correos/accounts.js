/**
 * Descripcion documento aquí.
 *
 * @author Yeison Rodriguez
 * @version 2.0
 */

// Función para recoger los datos de PHP según el navegador.
function objetoAjax(){
    var xmlhttp=false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {

    try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
        xmlhttp = false;
    }
}

if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
      xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
//--------------------------------------------------------------------------

function generateTeacherAccount(firstname, middlename, lastname_1, lastname_2){
  $.ajax({
    url: './create',
    headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
    type: 'GET',
    data: {'firstname': firstname},
    dataType: 'json',

    beforeSend: function(){
            $('#respuesta').html('<div class="text-center"><span class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></span></div>');
    },
    success: function (response) {
            $("#respuesta").html(response);
    }
  });
}

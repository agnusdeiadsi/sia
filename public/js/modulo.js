// Get the Sidenav
var mySidenav = document.getElementById("mySidenav");

// Get the module nav
var mySidenavHeader = document.getElementById("mySidenavHeader");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

var myNavbar = document.getElementById("myNavbar");

// Toggle between showing and hiding the sidenav, and add overlay effect
function w3_open() {
  mySidenav.style.display = "block";
  overlayBg.style.display = "block";
  mySidenavHeader.style.display = "none";
  myNavbar.style.display = "none";
}

// Close the sidenav with the close button
function w3_close() {
  mySidenav.style.display = "none";
  overlayBg.style.display = "none";
  mySidenavHeader.style.display = "block";
  myNavbar.style.display = "block";
}

// Accordion
function accordion(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
      x.className += " w3-show";
  } else {
      x.className = x.className.replace(" w3-show", "");
  }
}

//Consulta las subareas del área seleccionada
function selectSubarea(filtro, area) {
  var xhttp;
  if (area == "") {
    document.getElementById("body-modulo").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("body-modulo").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "?filtro="+filtro+"&area="+area, true);
  xhttp.send();
}

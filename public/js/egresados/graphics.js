$(document).ready(function () {

	$('#select-laboral-filter').on('change', function () {
		
		$('#refrescar-laboral').show();
		var value = $('#select-laboral-filter').val();
		graficar_laboral(value);

	});

	$('#refrescar-laboral-btn').on('click', function () {

		var value = $('#select-laboral-filter').val();
		graficar_laboral(value);

	});

	$('#select-business-filter').on('change', function () {

		$('#refrescar-bussines').show();
		var value = $(this).val();
		graficar_emprendimiento(value);

	});

	$('#refrescar-bussines-btn').on('click', function () {

		var value = $('#select-business-filter').val();
		graficar_emprendimiento(value);

	})

});

//Funciones para graficar
var url = "/sia"

function graficar_laboral(value) {
	
	var program = new Array()
  	program.push($('#select-program').val())
	var period = $('#select-period').val();

	var since = $('#select-period-since').val();
	var until = $('#select-period-until').val();
	var year_graduate = $('#select-year-graduate').val();

	var selectTimePeriod = $('#select-period').val()
	var selectTimeUntil = $('#select-period-until').val()
	var selectTimeYear = $('#select-year-graduate').val()

	//Si la variable esta igualada a 1, significa que no se selecciono nada del select
	if(period == '')
	{
		period = 1;
	}

	if (since == '') 
	{
		since = 1;
		until = 1;
	}

	if (year_graduate == '') 
	{
		
		year_graduate = 'null';

	} else if(year_graduate == '1_year'){

		year_graduate = '1'

	} else if(year_graduate == '3_year'){

		year_graduate = '3'

	} else if(year_graduate == '5_year'){

		year_graduate = '5'

	}

	if (selectTimePeriod != '' || selectTimeUntil != null || selectTimeYear != '') 
	{
	
		$.post(url+'/public/api/program/'+program+'/nameProgram', function (Htmlprogram) 
		{
			
			if (value == 'graphic_laboral') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_situacion', function (data) 
				{	
					
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var programas = '';
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}
			
						$.each(data, function(index, elem){
							array.push( elem.situacionlaboral_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.situacionlaboral_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Situacion Laboral'
								    },
								    credits: {

								        enabled: false

								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                //var x = Marh.round(pcnt);
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Situacion Laboral',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});

			} else if (value == 'graphic_relacion') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_relacion', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						
						$.each(data, function(index, elem){
							array.push( elem.relacionprograma_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.relacionprograma_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Relacion con el Programa'
								    },
									credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Relacion Programa',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});

			} else if (value == 'graphic_cargo') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_cargo', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}
						
						$.each(data, function(index, elem){
							array.push( elem.nivelcargo_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.nivelcargo_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Cargo'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Cargo',
								        data: totales,
								        color: '#DCAC34'
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_duracion') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_duracion', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}
						
						$.each(data, function(index, elem){
							array.push( elem.duracionempresa_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.duracionempresa_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Duracion en la Empresa'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Duracion en la Empresa',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_contrato') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_contrato', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.contrato_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.contrato_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tipo de Contrato'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Tipo de Contrato',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)

					}
					});
			
			} else if (value == 'graphic_rangoSalarial') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_salario', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.rangosalario_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.rangosalario_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Salario Devengado()'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Salario Devengado()',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_ocupacion') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/ocupacion', function (data) 
				{if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.ocupacion_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.ocupacion_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Ocupacion Actual'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+'|'+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Ocupacion Actual',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_sectorEmpresa') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_sector', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}
						
						$.each(data, function(index, elem){
							array.push( elem.sectorempresa_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.sectorempresa_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Sector de la Empresa'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Sector de la Empresa',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_vinculacion') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_vinculacion', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.vinculacion_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.vinculacion_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tipo de Vinculacion'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Tipo de Vinculacion',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_area') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_area', function (data) 
				{

					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.areapertenece_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.areapertenece_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Area a la que Pertenece'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Cantidad: ',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			} else if (value == 'graphic_tipoEmpresa') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/laboral_tipoEmpresa', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.tipoempresa_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.tipoempresa_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head').html(html_table_head + '<th>Total</th>');
						$('#content-table').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-laboral').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tipo de Empresa'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Cantidad: ',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head').html(html_table_head + '<th>Tabla</th>')
						$('#content-table').html("")
						$('#container-laboral').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
					});
			
			}  	
		})
	
	} else {

		$('#table-head').html('<th>Tabla</th>')
		$('#content-table').html("")
		$('#container-laboral').html("")

		swal(
            '',
            'Asegurate de seleccionar un tiempo (Perido, tiempo de graduado)',
            'warning'
  			)
	}
}

function graficar_emprendimiento(value) {

	var program = new Array()
  	program.push($('#select-program').val())
	var period = $('#select-period').val();

	var since = $('#select-period-since').val();
	var until = $('#select-period-until').val();
	var year_graduate = $('#select-year-graduate').val();

	var selectTimePeriod = $('#select-period').val()
	var selectTimeUntil = $('#select-period-until').val()
	var selectTimeYear = $('#select-year-graduate').val()

	//var query_select = '';

	//Si la variable esta igualada a 1, significa que no se selecciono nada del select
	if(period == '')
	{
		period = 1;
	}

	if (since == '') 
	{
		since = 1;
		until = 1;
	}

	if (year_graduate == '') 
	{
		
		year_graduate = 'null';

	} else if(year_graduate == '_year'){

		year_graduate = '1'

	} else if(year_graduate == '3_year'){

		year_graduate = '3'

	} else if(year_graduate == '5_year'){

		year_graduate = '5'

	}
	
	if (selectTimePeriod != '' || selectTimeUntil != null || selectTimeYear != '') 
	{
		$.post(url+'/public/api/program/'+program+'/nameProgram', function (Htmlprogram) 
		{
			if(value == 'graphic_sectorEmpresa_Bu') {
				

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/business_sector', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.sectorempresa_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.sectorempresa_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head-business').html(html_table_head + '<th>Total</th>');
						$('#content-table-business').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-business').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tamaño de la empresa '
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Sector de la Empresas',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head-business').html(html_table_head + '<th>Tabla</th>')
						$('#content-table-business').html("")
						$('#container-business').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
				});

			} else if(value == 'graphic_tamanoEmpresa') {

				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/business_size', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}

						$.each(data, function(index, elem){
							array.push( elem.tamanoempresa_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.tamanoempresa_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						})
						
						$('#table-head-business').html(html_table_head + '<th>Total</th>');
						$('#content-table-business').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-business').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tamaño de la empresa '
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   	text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Tamaño de la Empresa',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});
					} else {
						$('#table-head-business').html(html_table_head + '<th>Tabla</th>')
						$('#content-table-business').html("")
						$('#container-business').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
				});
			
			} else if(value == 'graphic_ciudadEmpresa') {
				
				$.post(url+'/public/api/graphics/'+program+'/'+period+'/'+since+'/'+until+'/'+year_graduate+'/business_city', function (data) 
				{
					if (data.length != 0) 
					{

						var html_table_head = '';
						var html_table_body = '';
						var total = 0;
						var array = [];
						var periodo = [];
						var totales = [];
						
						if (period == 'all') 
						{
							periodo.push('Todos los Periodos');
						
						} else if(period == 1 && year_graduate == 'null') {

							periodo.push('Desde: '+since+' Hasta: '+until);

						} else if(period == 1 && year_graduate != 'null') {

							periodo.push(year_graduate+" año de haberse graduado");

						} else {

							periodo.push(data[0].grado_periodo);

						}
						
						$.each(data, function(index, elem){
							array.push( elem.ciudad_nombre );
							totales.push( parseInt(elem.total) );
							html_table_head += '<th>'+ elem.ciudad_nombre +'</th>';
							html_table_body += '<td>'+elem.total+'</td>';			
							total += parseInt(elem.total);	
						});
						
						$('#table-head-business').html(html_table_head + '<th>Total</th>');
						$('#content-table-business').html(html_table_body + '<td>'+total+'</td>');
						
						
						$('#container-business').highcharts({
					        	
								    chart: {
								        type: 'column'
								    },
								    title: {
								        text: 'Tamaño de la empresa'
								    },
								    credits: {

								        enabled: false
								        
								    },
								    subtitle: {
								    	
								 	   text: 'Programa: '+Htmlprogram+' | '+'Periodo: '+periodo
								 
								    },
								    xAxis: {
								        categories: array,
								        crosshair: true
								    },

								    yAxis: {
								        min: 0,
								        title: {
								            text: 'Porcentual (%)'
								        },
								        labels: {
								            formatter:function() {
								                var pcnt = (this.value / total) * 100;
								                return Highcharts.numberFormat(pcnt,1,',') + '%';
								            }
								        }
								    },

								    tooltip: {
								        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								            '<td style="padding:0"><b>{point.y}</b></td></tr>',
								        footerFormat: '</table>',
								        shared: true,
								        useHTML: true
								    },
								        plotOptions: {
										        series: {
										            borderWidth: 0,
										            dataLabels: {
										                enabled: true,
										                // format: '{point.y:.1f}%'
										                formatter:function() {
										                    var pcnt = (this.y / total ) * 100;
										                    return Highcharts.numberFormat(pcnt,1,',') + '%';
										                }
										            }
										        }
										    },

								    series: [{
								        name: 'Ciudades',
								        data: totales,
								        color: '#DCAC34'
								        
								    }]	 

					    	});

					}else {
						$('#table-head-business').html(html_table_head + '<th>Tabla</th>')
						$('#content-table-business').html("")
						$('#container-business').html("")

						swal(
				            '',
				            '¡No existen datos!',
				            'warning'
		          			)
					}
				});

			}
		})
	} else {
		
			$('#table-head-business').html('<th>Tabla</th>')
			$('#content-table-business').html("")
			$('#container-business').html("")

		swal(
            '',
            'Asegurate de seleccionar un tiempo (Perido, tiempo de graduado)',
            'warning'
  			)
	}
}

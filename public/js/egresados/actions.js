
var url = "/sia"

$(document).ready( function () {


  $('#finish').attr('disabled')
  //$("#div-situacion").show();

  //Llamado a funciones
  //$('#agregar').on('click', agregarFilaEstudios);

  $('#select-facu').on('change', onSelectProgram);

	$('#select-program').on('change', onSelectPeriod);

  $('#situacionlaboral').on('change', showLaboralForm);

  $('#send-import').on('click', showMessageLoad);

  $('#submit-send').on('click', showMessageSend);

  $('.show-form-em').on('click', showEmprenForm);

  $('#departamento').change(function () {

    $.post(url+'/public/api/'+$(this).val()+'/departamento', function (data) 
    {
      
      var html_select = ''
      var html_cb = ''
      
      html_select = '<option value="" hidden="">Seleccione</option>';
      /*html_cb += '<li class="ms-select-all">'
      html_cb += '<label>'
      html_cb += '<input type="checkbox" data-name="selectAllselect_tamaño_empresa[]">[Seleccionar Todos]'
      html_cb += '</label>'
      html_Cb += '</li>'*/

      for (var i = 0; i < data.length; i++) 
      {

        /*html_cb += '<li class = "" style = "false">'
        html_cb += '<label class = "">'
        html_cb += '<input type="checkbox" data-name="selectItemselect_tamaño_empresa[]" value ='+data[i].ciudad_id+'>'
        html_cb += '<span>'+data[i].ciudad_nombre+'</span>'
        html_cb += '</label>'
        html_cb += '</li>'*/

        html_select += '<option value="'+data[i].ciudad_id+'">'+data[i].ciudad_nombre+'</option>'
      
      }

      //$('div.city div.ms-drop.bottom ul').html(html_cb)

      $('#select_Ciudad_Bu').html(html_select)

    })

    /*
      for (var i = 0; i < data.length; i++) 
      {

        html_select += '<li class = "" style = "false">'
        html_select += '<label class = "">'
        html_select += '<input type="checkbox" data-name="selectItemselect_tamaño_empresa[]" value ='+data[i].ciudad_id+'>'
        html_select += '<span>'+data[i].ciudad_nombre+'</span>'
        html_select += '</label>'
        html_select += '</li>'
      
      }
      console.log(html_select)
      $('div.ms-drop.bottom:ul').html(html_select)*/
  });

  $('input').change(function(){

      getValue_Filters() 
      validateMaxFilter()

  });

  $('.city').on('change', function () 
  {
      getValue_Filters() 
      validateMaxFilter()
  })

  $('#finish').on('click', function () {

    var selectTimePeriod = $('#select-period').val()
    var selectTimeUntil = $('#select-period-until').val()
    var selectTimeYear = $('#select-year-graduate').val()

    if (selectTimePeriod != '' || selectTimeUntil != null || selectTimeYear != '') 
    {
      
      validation_filters();

    } else {

      swal(
            '',
            'Asegurate de seleccionar un tiempo (Perido, tiempo de graduado)',
            'warning'
        )
      
    }
    

  });

  $('#select-period').on('change', function() {
       $('#select-tipo-filtro').show()
       $('#select-period-since').prop('selectedIndex',0)
       $('#select-year-graduate').prop('selectedIndex',0)
       $('#select-period-until').html("")
  })

  $('#select-year-graduate').on('change', function() {
       $('#select-tipo-filtro').show()
       $('#select-period-since').prop('selectedIndex',0)
       $('#select-period').prop('selectedIndex',0)
       $('#select-period-until').html("")
  })

  $('#select-period-since').on('change', function() {
       $('#select-period').prop('selectedIndex',0)
       $('#select-year-graduate').prop('selectedIndex',0)
  })

  $('#select-period-until').on('change', function() {
       $('#select-tipo-filtro').show()
  })

  $('#select-type-filter').on('change', activate_section_graphic)

  $.post(url+'/public/api/facultad', function (data) 
  {
   	
    //Se crea una etiqueta option que se le concatenara el resultado de la cunsulta
    var html_select = '<option value="" hidden="">Seleccione</option>';
  
    html_select += "<option value='all'>Todos los Programas</option>";

    for (var i = 0; i < data.length; i++) {

      html_select += '<option value="'+data[i].facultad_id+'">'+data[i].facultad_nombre+'</option>';

    }
    $('#select-facu').html(html_select);

  });  

  $('.input-number').on('input', function () { 
    this.value = this.value.replace(/[^0-9]/g,'')
  })

});


//FUNTIONS

function onSelectProgram() {
  
  $('#select-period-since').prop('selectedIndex',0)
  $('#select-period-until').html("")
  $('#select-year-graduate').prop('selectedIndex',0)
  $('#select-period').prop('selectedIndex',0)
  $('#select-year-graduate').prop('selectedIndex',0)

  var facultad = $(this).val()

  $.post(url+'/public/api/facultad/'+facultad+'/programa', function (data) {

    if (data != 0) {

    //Se crea una etiqueta option que se le concatenara el resultado de la cunsulta
    var html_select = '<option value="" hidden="">Seleccione</option>';
    //var html_all = '<option value="all">Todos los Programas</option>';

      html_select += "<option value='all'>Todos los Programas</option>";

      for (var i = 0; i < data.length; i++) {

        html_select += '<option value="'+data[i].programa_id+'">'+data[i].programa_nombre+' - '+data[i].convenio_nombre+'</option>';

      }

    //Si la respuesta de la consulta no trae nada...
    } else {
      //Se crea una variable indicando que no existen registro
      var html_select = '<option value="" hidden="">No existen programas</option>';
    }
    $('#select-program').html(html_select);

  });  
}
//Funcion que sirve para cargar los periodos correspondientes al programa seleccionado
function onSelectPeriod() {

  var arrayProgram = new Array()
  arrayProgram.push($(this).val())
  $('#select-period-until').html("")

  $('#year_filters').show()
  //Se realiza la consulta
  $.post(url+'/public/api/programa/'+arrayProgram+'/periodo', function (data) {
     
    //Se pregunta si el resultado de la consulta traer consigo algo, si es asi
    if (data != 0) {
      
      //Se crea una variable que se le concatenara la respuesta de la consutal
      var html_select = '<option value="" hidden="">Seleccione</option>';
      var html_select_since = '<option value="" hidden="">Seleccione</option>';

      html_select += '<option value="all">Todos los Periodos</option>';

      for (var i = 0; i < data.length; i++) 
      {

        html_select += '<option value="'+data[i].grado_periodo+'">'+data[i].grado_periodo+'</option>';
        html_select_since += '<option value="'+data[i].grado_periodo+'">'+data[i].grado_periodo+'</option>';

      }
    //Si la respuesta de la consulta no trae nada...
    } else {
      //Se crea una variable indicando que no existen registro
      var html_select = '<option value="" hidden="">No existen periodos</option>';
      var html_select_since = '<option value="" hidden="">No existen periodos</option>';
    }
    
    //Se reemplaza el contenido de #select-period
    $('#select-period').html(html_select);
    $('#select-period-since').html(html_select_since);

      $('#select-period-since').on('change', function(){

        var period = $(this).val()

        var html_select_until = '<option value="" hidden="">Seleccione</option>';

        for (var i = 0; i < data.length; i++) {
          
          if (data[i].grado_periodo > period) 
          {

            html_select_until += '<option value="'+data[i].grado_periodo+'">'+data[i].grado_periodo+'</option>';

          }
        }

        $('#select-period-until').html(html_select_until);

      });

  });

}

function showEmprenForm()
{
    
  var value = $(this).val();

  if (value == 'si') {
  
    $('#form-emprendimiento').show('slow');
    $('.habilitar_em').removeAttr('disabled');
    $('.habilitar_em').prop('required',true);

  } else {

    $('#form-emprendimiento').hide('slow');  
    $('.habilitar_em').attr('disabled');
    $('.habilitar_em').removeAttr('required');

  }


}

function showLaboralForm()
{
  
  if ($(this).val() != 2 && $(this).val() != '') 
  {
    $('#form-laboral').show('slow');
    $('.habilitar').removeAttr('disabled');
    $('.habilitar').prop('required',true);
  } else {
    $('#form-laboral').hide('slow');
    $('.habilitar').attr('disabled');
    $('.habilitar').removeAttr('required');
  }
  
}

function showMessageSend() 
{
  /*swal({
  type: 'info',
  title: '¡Enviando correos!',
  text: "Por favor espera...",
  showConfirmButton: false,
  })*/

  //$(".loader").fadeOut("slow");
}

function showMessageLoad()
{
/*
  swal({
  type: 'info',
  title: 'Cargando',
  text: "Por favor espera...",
  showConfirmButton: false,
  })
*/
}

function activate_select_type_filters() 
{

  $('#select-tipo-filtro').show();

}

function activate_section_graphic() 
{
  var value = $(this).val();

  $('.section-graphic-filters').show();

  if (value == 'laboral') 
  {
     $('#laboral').show();
     $('#grado').hide();
     $('#emprendimiento').hide();
     $('#logros').hide();

  } else if (value == 'grado') {
    $('#grado').show();
    $('#laboral').hide();
    $('#emprendimiento').hide();
    $('#logros').hide();
  
  } else if (value == 'emprendimiento') {
    $('#emprendimiento').show();
    $('#grado').hide();
    $('#laboral').hide();
    $('#logros').hide();

  } else if (value == 'logros') {
    $('#logros').show();
    $('#emprendimiento').hide();
    $('#grado').hide();
    $('#laboral').hide();

  } 
 

}

var array_filters = new Array();
var filtros_laboral = [];
var filtros_business = [];
var filtros_achievements = []; 

$('#reset').click(function () {

    /*$("select.select").multipleSelect();
    $("select.select").multipleSelect("uncheckAll");*/
    array_filters.length = 0;
    filtros_laboral.length = [];
    filtros_business.length = [];
    filtros_achievements.lenth = [];
    $('input').prop('checked', false)
    $('div.multiple-class :button.ms-choice span').addClass('placeholder')
    $('div.multiple-class :button.ms-choice span').html('Seleccionar una opcion')
    $('div.multiple-class div.ms-drop.bottom ul li').removeClass('selected')
    
})

function getValue_Filters() {

  var array_situacion = {};
  var array_duracion = {};
  var array_cargo = {};
  var array_contrato = {};
  var array_sector = {};
  var array_rangoSalarial = {};
  var array_relacion = {};
  var array_vinculacion = {};
  var array_ocupacion = {};
  var array_area = {};
  var array_tipoEmpresa = {};
  var array_sectorBu = {};
  var array_ciudad = {};
  var array_tamano = {};
  var array_merito = {};

  //Contiene todos los filtros seleccionados hasta que se desee terminar
  array_filters.length = 0;
  filtros_laboral.length = 0;
  filtros_business.length = 0;
  filtros_achievements.lenth = 0;

   //Recoremos el select para identificar el value de la opcion seleccionada
   $.each($("#select_situacion_laboral option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        //Si el arreglo esta vacio
        if(!Array.isArray(array_situacion[url+"/public/api/get/name"])){
          //Se crea el arreglo que tiene como value el nombre del value que se selecciono
          array_situacion[url+"/public/api/get/name"] = new Array()
          //Se agrega el elemento
          array_situacion[url+"/public/api/get/name"].push($(elem).val())
        //Si ya esta creado el arreglo..
        } else {
          //Se agrega el elemento
          array_situacion[url+"/public/api/get/name"].push($(elem).val())
        }
        //Se agrega el nuevo filtro
        array_filters.push(array_situacion);
   })

   /*
      Lo anterior aplica para a que le corresponda
   */

   $.each($("#select_duracion option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_duracion[url+"/public/api/get/duracion"])){
          array_duracion[url+"/public/api/get/duracion"] = new Array()
          array_duracion[url+"/public/api/get/duracion"].push($(elem).val())
        } else {
          array_duracion[url+"/public/api/get/duracion"].push($(elem).val())
        }

        array_filters.push(array_duracion);
   })

   $.each($("#select_cargo option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_cargo[url+"/public/api/get/cargo"])){
          array_cargo[url+"/public/api/get/cargo"] = new Array()
          array_cargo[url+"/public/api/get/cargo"].push($(elem).val())
        } else {
          array_cargo[url+"/public/api/get/cargo"].push($(elem).val())
        }

        array_filters.push(array_cargo);
   })

   $.each($("#select_contrato option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_contrato[url+"/public/api/get/contrato"])){
          array_contrato[url+"/public/api/get/contrato"] = new Array()
          array_contrato[url+"/public/api/get/contrato"].push($(elem).val())
        } else {
          array_contrato[url+"/public/api/get/contrato"].push($(elem).val())
        }

        array_filters.push(array_contrato);
   })

   $.each($("#select_sectorEmpresa option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_sector[url+"/public/api/get/sector"])){
          array_sector[url+"/public/api/get/sector"] = new Array()
          array_sector[url+"/public/api/get/sector"].push($(elem).val())
        } else {
          array_sector[url+"/public/api/get/sector"].push($(elem).val())
        }

        array_filters.push(array_sector);
   })

   $.each($("#select_rangoSalarial option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_rangoSalarial[url+"/public/api/get/salario"])){
          array_rangoSalarial[url+"/public/api/get/salario"] = new Array()
          array_rangoSalarial[url+"/public/api/get/salario"].push($(elem).val())
        } else {
          array_rangoSalarial[url+"/public/api/get/salario"].push($(elem).val())
        }

        array_filters.push(array_rangoSalarial);
   })

   $.each($("#select_relacionPrograma option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_relacion[url+"/public/api/get/relacion"])){
          array_relacion[url+"/public/api/get/relacion"] = new Array()
          array_relacion[url+"/public/api/get/relacion"].push($(elem).val())
        } else {
          array_relacion[url+"/public/api/get/relacion"].push($(elem).val())
        }

        array_filters.push(array_relacion);
   })

   $.each($("#select_tipoVinculacion option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_vinculacion[url+"/public/api/get/vinculacion"])){
          array_vinculacion[url+"/public/api/get/vinculacion"] = new Array()
          array_vinculacion[url+"/public/api/get/vinculacion"].push($(elem).val())
        } else {
          array_vinculacion[url+"/public/api/get/vinculacion"].push($(elem).val())
        }

        array_filters.push(array_vinculacion);
   })

   $.each($("#select_ocupacionActual option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_ocupacion[url+"/public/api/get/ocupacion"])){
          array_ocupacion[url+"/public/api/get/ocupacion"] = new Array()
          array_ocupacion[url+"/public/api/get/ocupacion"].push($(elem).val())
        } else {
          array_ocupacion[url+"/public/api/get/ocupacion"].push($(elem).val())
        }

        array_filters.push(array_ocupacion);
   })

   $.each($("#select_areaPertenece option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_area[url+"/public/api/get/areaPertenece"])){
          array_area[url+"/public/api/get/areaPertenece"] = new Array()
          array_area[url+"/public/api/get/areaPertenece"].push($(elem).val())
        } else {
          array_area[url+"/public/api/get/areaPertenece"].push($(elem).val())
        }

        array_filters.push(array_area);
   })

   $.each($("#select_tipoEmpresa option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_tipoEmpresa[url+"/public/api/get/tipoEmpresa"])){
          array_tipoEmpresa[url+"/public/api/get/tipoEmpresa"] = new Array()
          array_tipoEmpresa[url+"/public/api/get/tipoEmpresa"].push($(elem).val())
        } else {
          array_tipoEmpresa[url+"/public/api/get/tipoEmpresa"].push($(elem).val())
        }

        array_filters.push(array_tipoEmpresa);
   })

   $.each($("#select_sectorEmpresa_Bu option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_sectorBu[url+"/public/api/get/sector"])){
          array_sectorBu[url+"/public/api/get/sector"] = new Array()
          array_sectorBu[url+"/public/api/get/sector"].push($(elem).val())
        } else {
          array_sectorBu[url+"/public/api/get/sector"].push($(elem).val())
        }

        array_filters.push(array_sectorBu);
   })

   $.each($("#select_Ciudad_Bu option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_ciudad[url+"/public/api/get/ciudad"])){
          array_ciudad[url+"/public/api/get/ciudad"] = new Array()
          array_ciudad[url+"/public/api/get/ciudad"].push($(elem).val())
        } else {
          array_ciudad[url+"/public/api/get/ciudad"].push($(elem).val())
        }

         array_filters.push(array_ciudad);
   })

   $.each($("#select_tamaño_empresa option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_tamano[url+"/public/api/get/tamanoEmpresa"])){
          array_tamano[url+"/public/api/get/tamanoEmpresa"] = new Array()
          array_tamano[url+"/public/api/get/tamanoEmpresa"].push($(elem).val())
        } else {
          array_tamano[url+"/public/api/get/tamanoEmpresa"].push($(elem).val())
        }

         array_filters.push(array_tamano);
   })

   $.each($("#select_merito option:selected"), function(index, elem){
      if($(elem).val() != "on" )
        if(!Array.isArray(array_merito[url+"/public/api/get/merito"])){
          array_merito[url+"/public/api/get/merito"] = new Array()
          array_merito[url+"/public/api/get/merito"].push($(elem).val())
        } else {
          array_merito[url+"/public/api/get/merito"].push($(elem).val())
        }

        array_filters.push(array_merito);
   })

   //Se recorre el arreglo con su respectivo index(ruta) y valor(llave primaria en la BD)
   $.each(array_situacion, function(index, val){

    data = JSON.stringify( val );

    //Se realiza la consulta por medio de un metodo POST
    $.post(index, {datos: data}, function(info){
      //Se parsea el resultado de la consulta y se agrega a un arreglo
     
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   /*
      Lo anterior aplica para lo que le corresponda
   */

   $.each(array_duracion, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_cargo, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_contrato, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_sector, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_rangoSalarial, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_relacion, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_vinculacion, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_ocupacion, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_area, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_tipoEmpresa, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_laboral.push( $.parseJSON(info));
      //alert(filtros_laboral);
    })
   })

   $.each(array_sectorBu, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_business.push( $.parseJSON(info));
    })
   })

   $.each(array_ciudad, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_business.push( $.parseJSON(info));
    })
   })

   $.each(array_tamano, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_business.push( $.parseJSON(info));
    })
   })

   $.each(array_merito, function(index, val){
    data = JSON.stringify( val );
    $.post(index, {datos: data}, function(info){
      filtros_achievements.push( $.parseJSON(info));
    })
   })

}

function validateMaxFilter() 
{
  if (array_filters.length > 4) 
  {

    swal(
      '',
      '¡La cantidad maxima permitida de filtros son 4!',
      'warning'
    )
    
    $('#finish').attr("disabled", "disabled")

  } else {

    $('#finish').removeAttr('disabled')

  }

}


function validation_filters() 
{
  console.log(array_filters)
  //Se pregunta si en alguno de los arreglos de la funcion getValue_Filters es diferente a cero, si es asi...
  if (array_filters.length != 0 ) 
  {

    //Se pregunta si cantidad de filtros seleccionados es menor o igual a 4, si todo se cumple...
    if (array_filters.length <= 4) 
    {
        //Muestra la seccion con los filtros seleccionado, esconde la seccion de los filtros, esconde el boton de finilizar
        $("#show-filters").show();
        $("#secondary-filters").hide();
        $("#buttom-finish").hide();
        //Si el arreglo es diferente de 0...
        if (filtros_laboral.length != 0) 
        {

          //Se muesta el div que tendra el los filtros seleccionados
          $("#div-situacion").show();
          $("#content-filters-laboral").html("<i>"+filtros_laboral+"</i>");
        
        } else {

          $("#div-situacion").hide();
          $("#content-filters-laboral").html("");

        } 

        /*
          Lo anterior aplica para lo que le corresponda
        */

        if (filtros_business.length != 0) 
        {
        
          $("#div-emprendimiento").show();
          $("#content-filters-emprendimiento").html("<i>"+filtros_business+"</i>");
        
        } else {

          $("#div-emprendimiento").hide();
          $("#content-filters-emprendimiento").html("");

        } 

        if (filtros_achievements.length != 0) 
        {
        
          $("#div-logros").show();
          $("#content-filters-logros").html("<i>"+filtros_achievements+"</i>");
        
        } else {

          $("#div-logros").hide();
          $("#content-filters-logros").html("");

        } 

    } else {
      $('#dialog-confirm').show();
    }

  } else {
      swal(
            '¡AVISO!',
            '¡Debes seleccionar por lo menos un filtro!',
            'warning'
          )
  }

}

/*Agregar fila a la table insumos*/
function agregarFilaEstudios()
{

//duplica la fila base, remueve la clase fila-base y la agrega al cuerpo de la tabla #tabla
$(".fila-estudios:first-child").clone().removeClass('fila-estudios').addClass('fila').appendTo("#tabla-estudios tbody");
$(".fila:last-child").remove();

//agregar el atributo requerido a cada input de la fila agregada y remueve el atributo disabled
$(".nivel:first-child").attr('required', 'required');
$(".nivel:first-child").removeAttr('disabled');

$(".institucion:first-child").attr('required', 'required');
$(".institucion:first-child").removeAttr('disabled');

$(".titulo:first-child").attr('required', 'required');
$(".titulo:first-child").removeAttr('disabled');

$(".inicio:first-child").attr('required', 'required');
$(".inicio:first-child").removeAttr('disabled');

$(".fin:first-child").attr('required', 'required');
$(".fin:first-child").removeAttr('disabled');


//inhabilita todos los input de la fila estudios
$(".fila-estudios:first-child input").removeAttr('required');
$(".fila-estudios:first-child input").attr('disabled', 'disabled');

$(".fila-estudios:first-child select").removeAttr('required');
$(".fila-estudios:first-child select").attr('disabled', 'disabled');
}

/*Eliminar fila de la table insumos*/
$(document).on("click",".eliminar-estudio",function(){
var parent = $(this).parents().get(0);
$(parent).remove();
});
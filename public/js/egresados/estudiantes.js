$(document).ready( function () {

  $('.show-form-em').on('change', showEmprenForm)

  $('#situacionlaboral').on('change', showLaboralForm)

  $('.show-form-es').on('click', showEmprenEs)

  /*
  * Llamado de funciones para la encuesta
  */

  $('#censal_patrocino').on('change', showNameCompany)

  $('#finacial-patrocinio').on('click', showFinanForm)

  $('#cs-text').on('click', cleanRadio)

  $('.cs').click('click', cleanText)

  $('#finacial-beca').on('click', showBecaForm)

  $('.button-sections-survey').on('click', enable)

  //$('input.input-checkbox').change(validate_cb) 

  //$('.cs_required').prop('required', true)

  $('#empresa_idea').on('change', showIdeaCompanyForm)

  $('#proceso-formacion-trabajo').on('change', showSituacionLaboral_1)

  $('#busca-trabajo').on('change', showSituacionLaboral_2)

  $('#razon-no-estudiar').on('change', showFormRazonOther)

  $('#recomendar').on('change', showFormRazonNoRecomendar)

  $('#aca-programa-select').on('change', getConvenio)

  /*$("#form_name").validate({
        ignore: [],
        onkeyup: false,
        rules: {            
        },      
        highlight:false,
    });*/

  $('.input-number').on('input', function () { 
    this.value = this.value.replace(/[^0-9]/g,'')
  })

  $('.input--only-text').on('input', function () { 
    this.value = this.value.replace(/[^a-zA-Záéíóúüñ]+/g,"")
  })

})

var count_cb = 0
var cb = null

/*function validate_cb() 
{

    if (this.checked) 
    {

      var cb_c = $('input.input-checkbox:checked')

      for (var i = 0; i < cb_c.length; i++) 
      {

        cb = $('input[name="'+cb_c[i].name+'"].input-checkbox')
        $('input[name="'+cb_c[i].name+'"].input-checkbox').removeAttr('required')

      }

      count_cb++

    } else {

      count_cb--

    }

    if (count_cb == 0) 
    {

      for (var j = 0; j < cb.length; j++) 
      {

        $('input[name="'+cb[j].name+'"].input-checkbox').prop('required', true)

      }

    }

}*/

var url = "/sia"

function getConvenio() 
{

  var program = $(this).val()

  $.post(url+'/public/api/estudiantes/'+program+'/convenio', function (data) 
  {

    $('#aca-convenio-text').val(data[0].convenio_nombre)
    $('#aca-convenio-id').val(data[0].convenio_id)

  })
}
    
function enable() 
{
  count_cb = 0;
  var count_null = 0
  var count_null_cb = 0
  var count_email = 0
  
  var message_email = '';
  var message_null = '';
  
  //Se toman el atributo clases del buton que nos da la accion para ingresar a esta funcion (button-sections-survey)

  //Se trae todo el atributo clases del boton que se esta presionando
  var clases = $(this).attr("class").split(' ')
  /*
  * Nota: Se necesita dividir el string de clases para podernos quedar con la posicion uno (0) de la cadena de carracteres para
          hacer dinamica la funcion
  */

  //Lo que se hace es declarar una variable la cual contendra todos los inputs de la division, gracias al id que se esta generando (la linea anterior)
  var div_input = $('div#'+clases[0]+' :input')

  //variable quen contendra los checkbox chekeados de la divsion
  var div_input_cb_c = $('div#'+clases[0]+' :input.input-checkbox:checked')

  //variable que contendra los checkbox de la divsion
  var div_input_cb = $('div#'+clases[0]+' :input.input-checkbox')

  //variable que contendra los radio de la divsion
  var div_input_r = $('div#'+clases[0]+' :input.input-radio')

  //variable que contendra los input tipo email de la division
  var div_input_email = $('div#'+clases[0]+' :input.input-email')

  //variable que contendra los input tipo select de la division
  var div_input_select = $('div#'+clases[0]+' :input.input-select')

  /*for (var i = 0; i < div_input_cb.length; i++) 
  {

    if (div_input_cb[i].checked == true) 
    {

      $('input[name="'+div_input_cb[i].name+'"].input-checkbox').removeAttr('required')

      break

    } else {

      $('input[name="'+div_input_cb[i].name+'"].input-checkbox').prop('required', true)

      break

    }

  }*/

  if (div_input_email.length > 0) 
  {
    //Se pregunta si el input tipo email tiene los estandares adecuados para un email-> @, sin Ñ y con .com
    if(/^[A-Za-z\.][A-Za-z0-9_\.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/.test($(div_input_email).val())==false)
    {

      message_email = 'Correo electronico se encuentra mal digitado';
      //de ser verdadero, tenesmos un campo mal dijitado
      count_null++

    }
  }
  
  if (div_input_cb.length > 0) 
  {
    //console.log(div_input_cb)
    var cb_array = []

    var j = 0

    cb_array.push(div_input_cb[0].name)

    for (var i = 0; i < div_input_cb.length; i++) 
    {
      
      if (div_input_cb[i].name != cb_array[j]) 
      {

        cb_array.push(div_input_cb[i].name)
        j++

      }

    }

    for (var j = 0; j < div_input_cb.length; j++) 
    {
      for (var i = 0; i < cb_array.length; i++) 
      {
      
        if (div_input_cb[j].checked == true && div_input_cb[j].name == cb_array[i]) {

          $('input[name="'+cb_array[i]+'"].input-checkbox').removeAttr('required')

        } 
        
      }

    }

    /*for (var i = 0; i < cb_array.length; i++) 
    {

      if ($('input[name="'+cb_array[i]+'"].input-checkbox').prop('checked', false))
      {
        alert("Hola")
      }

    }*/

    //console.log(cb_array)
    
  }
  
  /**/

  //se pregunta la dimencion de la variable que tiene los checkbox chequeados es mayor a 0

  /*
  * Nota: lo mismo sucede cono los radio
  */
  if (div_input_cb_c.length > 0) 
  {

    //de ser verdadero, se le quita el atributo required
    $('div#input-censal:input#cs-text').removeAttr('required')

  } 

  if (div_input_r.length > 0) 
  {
    
    $('div#'+clases[0]+' :input.input-radio').removeAttr('required')

  } 

  /*
  * Nota: El id de la division se pone en la clase del boton y se recupera con la primera linea de codigo
  */

  //recoremos el objeto que nos contiene todos los inputs
  for (var i = 0; i < div_input.length; i++) 
  {
    
    //Preguntamos si algun input esta vacio y tiene el atributo required
    if (div_input[i].value == '' && div_input[i].required == true) 
    {
      
      //De ser verdadero, se autoincrementa la siguiente variable, la cual identifica cuantos campos en blanco existen en la division
      count_null++
    
    }

  }

  //preguntamos si la variable que tiene los checkbox es mayor a 0
  if (div_input_cb.length > 0) 
  {
    //de ser verdarero significa que en la division hay checkbox, por lo tanto recoremos ese objeto
    for (var i = 0; i < div_input_cb.length; i++) 
    {
      //preguntamos si la propiedad checked es false y si la propiedad required es true
      if (div_input_cb[i].checked == false && div_input_cb[i].required == true) 
      {
        //De ser verdadero, se autoincrementa la siguiente variable, la cual identifica cuantos campos en blanco existen en la division
        count_null++

      }

    }

  } 

  /*
  * Nota: Lo anterior aplica para los radio 
  */

  if (div_input_r.length > 0) 
  {

    for (var i = 0; i < div_input_r.length; i++) 
    {
     
      if (div_input_r[i].checked == false && div_input_r[i].required == true) 
      {
        
        count_null++

      }

    }

  }

  if (count_null != 0) 
  {
    message_null = 'Campo vacio'
  }

  //Preguntamos si la variable es diferente de 0
  if (count_null != 0 || count_email != 0) 
  {

    var message_error = []

    message_error.push('<br>'+message_null+'<br>'+message_email)

    //De ser asi, significa que hay un campo en la division el cual esta vacio
    swal({
    html:true,
    type: 'warning',
    title: '¡Aviso!',
    text: "Asegurate de completar todos los campos del formulario correctamente <br><br><b>Error:</b><br>"+message_error,
    showConfirmButton: true,

    })

  } else {

    //De no ser verdadero, se nos habilita la siguiente seccion de la encuesta
    var id_next_section = $(this).attr("id")

    $('div#'+id_next_section).show()
    $('label#'+id_next_section).addClass('w3-disabled')
    //$('div#'+clases[0]+' :input').attr("readonly","readonly")
    $('#click-next-'+clases[0]).show('Por favor da click abajo')

    //$('div#'+clases[0]+' :input.input-select').attr("readonly","readonly");

  }

  if ($(this).attr("id") == 'finish') 
  {
    
    $('#save-form').removeAttr('disabled')

  }

}

function showEmprenEs() 
{
  var value = $(this).val()

  if (value == 'si') 
  {

    $('#form-estudio').show('slow')
    $('.habilitar_es').removeAttr('disabled')
    $('.habilitar_es').prop('required', true)

  } else {

    $('#form-estudio').hide('slow')
    $('.habilitar_es').attr('disabled')
    $('.habilitar_es').removeAttr('required')
    $('.habilitar_es').val('')

  }
}

function showEmprenForm()
{
    
  var value = $(this).val()

  if (value == 'si') {
  
    $('#form-emprendimiento').show('slow')
    $('#form-emprendimiento-plan').hide('slow')
    $('#empresa_idea').removeAttr('required')
    $('.habilitar_em').removeAttr('disabled')
    $('.habilitar_em').prop('required', true)

  } else {

    $('#form-emprendimiento').hide('slow')
    $('#form-emprendimiento-plan').show('slow')
    $('#empresa_idea').prop('required', true)
    $('.habilitar_em').attr('disabled')
    $('.habilitar_em').removeAttr('required')
    $('.habilitar_em').val('')

  }


}

function showLaboralForm()
{
  
  if ($(this).val() != 2 && $(this).val() != '') 
  {
    $('#form-laboral').show('slow')
    $('.habilitar').removeAttr('disabled')
    $('.habilitar').prop('required',true)
  } else {
    $('#form-laboral').hide('slow')
    $('.habilitar').attr('disabled')
    $('.habilitar').removeAttr('required')
  }
  
}

/*
*Funciones para la encuesta
*/

function showNameCompany() {
  
  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#divNameCompany').show('slide')
    $('#nameCompany').removeAttr('disabled')
    $('#nameCompany').prop('required',true)
  
  } else {

    $('#divNameCompany').hide('slide')
    $('#nameCompany').attr('disabled')
    $('#nameCompany').removeAttr('required')

  } 

}

function cleanRadio() 
{
  
  $('.cs').prop('checked', false)
  $('.cs').removeAttr('required')

}

function cleanText() 
{

    if (this.checked) 
    {

      $('#cs-text').val('')
      $('#cs-text').removeAttr('required')

    } else {

      $('#cs-text').prop('required', true)

    }


}

function showFinanForm() 
{
  
  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#divFormFinEnti').show('slide')
    $('.finanInput').removeAttr('disabled')
    $('.finanInput').prop('required',true)
  
  } else {

    $('#divFormFinEnti').hide('slide')
    $('.finanInput').attr('disabled')
    $('.finanInput').removeAttr('required')

  }
} 

function showBecaForm() 
{
  
  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#divFormBecaEnti').show('slide')
    $('.becaInput').removeAttr('disabled')
    $('.becaInput').prop('required',true)
  
  } else {

    $('#divFormBecaEnti').hide('slide')
    $('.becaInput').attr('disabled')
    $('.becaInput').removeAttr('required')

  } 

}

function showIdeaCompanyForm() 
{

  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#form-idea-company').show('slide')
    $('.input-idea-company').removeAttr('disabled')
    $('.input-idea-company').prop('required',true)
  
  } else {

    $('#form-idea-company').hide('slide')
    $('.input-idea-company').attr('disabled')
    $('.input-idea-company').removeAttr('required')

  } 
  
}

function showSituacionLaboral_1() 
{

  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#form-situacion-laboral-3').show('slide')
    $('#situacionlaboral').removeAttr('disabled')
    $('#situacionlaboral').prop('required',true)
    
    $('#form-situacion-laboral-2').hide('slide')
    $('.input-busca-trabajo').removeAttr('required')
    

    $('#busca-trabajo').attr('disabled')
    $('#busca-trabajo').removeAttr('required')
    $('#busca-trabajo').prop('selectedIndex', 0)

    $('#form-situacion-laboral-2-1').hide('slide')
  
  } else {

    $('#form-situacion-laboral-2').show('slide')
    $('#busca-trabajo').removeAttr('disabled')
    $('#busca-trabajo').prop('required',true)

    $('#form-situacion-laboral-3').hide('slide')
    $('#situacionlaboral').attr('disabled')
    $('#situacionlaboral').removeAttr('required')

    $('#form-laboral').hide('slide')
    $('#situacionlaboral').prop('selectedIndex',0)
    $('.habilitar').attr('disabled')
    $('.habilitar').removeAttr('required')


  } 

}

function showSituacionLaboral_2() 
{

  var value = $(this).val()

  if (value == 'si') 
  {
  
    $('#form-situacion-laboral-2-1').show('slide')
    $('.input-busca-trabajo').removeAttr('disabled')
    $('.input-busca-trabajo').prop('required',true)

    $('#situacionlaboral').attr('disabled')
    $('#situacionlaboral').removeAttr('required')

    $('#form-situacion-laboral-3').hide('slide')
  
  } else {

    $('#form-situacion-laboral-2-1').hide('slide')

    $('#form-situacion-laboral-3').show('slide')
    $('#situacionlaboral').removeAttr('disabled')
    $('#situacionlaboral').prop('required',true)

    $('.input-busca-trabajo').attr('disabled')
    $('.input-busca-trabajo').removeAttr('required')

  } 

}

function showFormRazonOther() 
{
  
  var value = $(this).val()

  if (value == 'otra') 
  {

    $('#form-other-study').show('slide')
    $('#textarea-razon').removeAttr('disabled')
    $('#textarea-razon').prop('required',true)

  } else {

    $('#form-other-study').hide('slide')
    $('#textarea-razon').attr('disabled')
    $('#textarea-razon').removeAttr('required')

  }

}

function showFormRazonNoRecomendar() 
{
  var value = $(this).val()

  if (value == 'no') 
  {

    $('#form-razon-recomendar').show('slide')
    $('#razon-no-recomendar').removeAttr('disabled')
    $('#razon-no-recomendar').prop('required',true)

  } else {

    $('#form-razon-recomendar').hide('slide')
    $('#razon-no-recomendar').attr('disabled')
    $('#razon-no-recomendar').removeAttr('required')

  }
}
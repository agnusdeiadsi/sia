$(document).ready( function () {
  
  var count = 0
  var section_back = null
  var id_section = null

  $('#select-period').on('change', function() {
       $('#menu-laboral').show() 
       $('#select-period-until').removeAttr('required')  
  })



  $('#select-year-graduate').on('change', function() {
   	   $('#menu-laboral').show() 
       $('#select-period-until').removeAttr('required') 
  })

  $('#select-period-since').on('change', function() {
       $('#select-period-until').prop('required', true)
  })

  $('#select-period-until').on('change', function() {
       $('#menu-laboral').show()
  })

  $('.fppEnable').on('change', function () {
    
      $('#fppDownload').removeAttr('disabled')

  })


  //Funcion que sirve para la ocultar y mostar las diferentes secciones de los filtros->laboral, logros, emprendimento
  $('.show-section').on('click', function() {

      $("#show-filters").hide();

      id_section = $(this).attr("id")

      if (count == 0) 
      {

        section_back = id_section
        $('.'+id_section).show()
        $('#'+id_section).addClass('selected-menu-filters')
        $('#'+id_section).removeClass('w3-teal')
        $('#buttom-finish').show()
        count++

      } else if(count > 0) {
        
        $("#secondary-filters").show();
        $('#buttom-finish').show()
        $('#'+section_back).addClass('w3-teal')
        $('#'+id_section).addClass('selected-menu-filters')
        $('#'+id_section).removeClass('w3-teal')

        $('.'+section_back).hide()
        $('.'+id_section).show()
        section_back = id_section

      }


  })

  $('#buttom-finish').on('click', function () {
    
    $('#'+id_section).addClass('w3-teal')
    $('#'+id_section).removeClass('selected-menu-filters')

  })
  
})


//Funciones



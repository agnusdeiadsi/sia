/*Agregar fila a la table insumos*/
function agregarFila(){
	$("#adjunto").removeAttr("disabled");
	$("#adjunto").attr("required", "required");

	//quita duplica la fila base, quita la clase de la fila y las clases de los elementos de misma y añade la nueva fila al cuerpo de la tabla
	$(".fila-base:first-child").clone().removeClass('fila-base cadjunto').appendTo("#tabla tbody");

	//añade el atributo disabled a los elementos de la fila base
	$(".cadjunto:first").attr("disabled", "disabled");
	$(".cadjunto:first").removeAttr("required");
}

/*Eliminar fila de la table insumos*/
$(document).on("click",".eliminar",function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});


function mayorEdad()
{
	chk = document.getElementById('chk_mayor').checked;
	
	if(chk == true)
	{
		$('#registro_civil').attr('disabled', 'disabled');
		$('#registro_civil').removeAttr("required");

		$('#documento_acudiente').attr('disabled', 'disabled');
		$('#documento_acudiente').removeAttr("required");
	}
	else
	{
		$('#registro_civil').removeAttr('disabled');
		$('#registro_civil').attr('required', 'required');

		$('#documento_acudiente').removeAttr('disabled');
		$('#documento_acudiente').attr('required', 'required');
	}
}

function contarTextarea(cadena_value){
	var cadena = cadena_value;
	var num = cadena.length;

	document.getElementById("caracteres").innerHTML= "<b>Caracteres: "+num+"</b>";
}

//cargar modelo certificado popover
function cargarCertificado(param)
{
	var image = param;
	var modelo = '<img src="../../../files/certificados/modelos/'+image+'.png" class="w3-image">';

	$('#modelo_certificado').html(modelo);
}

function displayNewInputs(tipo) {
	//variables
	var inputPeriodo = document.getElementsByClassName('periodo');
	var inputPension = document.getElementsByClassName('pension');
	var inputDeclaracion = document.getElementsByClassName('declaracion');
	var inputMaterias = document.getElementsByClassName('materias');
	var inputsCarnet = document.getElementsByClassName('carnet');

	if(tipo == 1 || tipo == 2 || tipo == 4) //Estudio, Notas, Horario
	{
		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='block';
			inputPeriodo[i].required='true';
			inputPeriodo[i].disabled=false;
		}

		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='none';
			inputPension[i].required='false';
			inputPension[i].disabled=true;
		}

		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='none';
			inputDeclaracion[i].required='false';
			inputDeclaracion[i].disabled=true;
		}

		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='none';
			inputMaterias[i].required='false';
			inputMaterias[i].disabled=true;
		}

		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='none';
			inputsCarnet[i].required='false';
			inputsCarnet[i].disabled=true;
		}
	}
	else if(tipo == 8) //pensión y cesantías
	{
		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='block';
			inputPension[i].required='true';
			inputPension[i].disabled=false;
		}

		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='none';
			inputPeriodo[i].required='false';
			inputPeriodo[i].disabled=true;
		}

		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='none';
			inputDeclaracion[i].required='false';
			inputDeclaracion[i].disabled=true;
		}

		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='none';
			inputMaterias[i].required='false';
			inputMaterias[i].disabled=true;
		}

		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='none';
			inputsCarnet[i].required='false';
			inputsCarnet[i].disabled=true;
		}
	}
	else if(tipo == 14) //notas materias
	{
		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='block';
			inputMaterias[i].required='true';
			inputMaterias[i].disabled=false;
		}

		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='none';
			inputPeriodo[i].required='false';
			inputPeriodo[i].disabled=true;
		}

		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='none';
			inputPension[i].required='false';
			inputPension[i].disabled=true;
		}

		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='none';
			inputDeclaracion[i].required='false';
			inputDeclaracion[i].disabled=true;
		}

		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='none';
			inputsCarnet[i].required='false';
			inputsCarnet[i].disabled=true;
		}
	}
	else if(tipo == 16) //carnet estudiantil
	{
		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='block';
			inputsCarnet[i].required='true';
			inputsCarnet[i].disabled=false;
		}

		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='none';
			inputPeriodo[i].required='false';
			inputPeriodo[i].disabled=true;
		}

		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='none';
			inputPension[i].required='false';
			inputPension[i].disabled=true;
		}

		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='none';
			inputDeclaracion[i].required='false';
			inputDeclaracion[i].disabled=true;
		}

		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='none';
			inputMaterias[i].required='false';
			inputMaterias[i].disabled=true;
		}
	}
	else if(tipo == 17) //declaracion de renta
	{
		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='block';
			inputDeclaracion[i].required='true';
			inputDeclaracion[i].disabled=false;
		}

		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='none';
			inputPeriodo[i].required='false';
			inputPeriodo[i].disabled=true;
		}

		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='none';
			inputPension[i].required='false';
			inputPension[i].disabled=true;
		}

		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='none';
			inputMaterias[i].required='false';
			inputMaterias[i].disabled=true;
		}

		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='none';
			inputsCarnet[i].required='false';
			inputsCarnet[i].disabled=true;
		}
	}
	else
	{
		for (var i = 0; i < inputPeriodo.length; i++) {
			inputPeriodo[i].style.display='none';
			inputPeriodo[i].required='false';
			inputPeriodo[i].disabled=true;
		}

		for (var i = 0; i < inputPension.length; i++) {
			inputPension[i].style.display='none';
			inputPension[i].required='false';
			inputPension[i].disabled=true;
		}

		for (var i = 0; i < inputDeclaracion.length; i++) {
			inputDeclaracion[i].style.display='none';
			inputDeclaracion[i].required='false';
			inputDeclaracion[i].disabled=true;
		}

		for (var i = 0; i < inputMaterias.length; i++) {
			inputMaterias[i].style.display='none';
			inputMaterias[i].required='false';
			inputMaterias[i].disabled=true;
		}

		for (var i = 0; i < inputsCarnet.length; i++) {
			inputsCarnet[i].style.display='none';
			inputsCarnet[i].required='false';
			inputsCarnet[i].disabled=true;
		}
	}

	/*if(inputPago === true) {
		for (var i = 0; i < inputValor.length; i++) {
			inputValor[i].style.display='block';
			inputValor[i].required='true';
			inputValor[i].disabled=false;
		}

		for (var i = 0; i < hideInput.length; i++) {
			hideInput[i].style.display='none'; // visibles: no
			hideInput[i].required=false; //requerido: no
			hideInput[i].disabled=false; //habilitados: si
		}
	}
	else {
		for (var i = 0; i < inputValor.length; i++) {
			inputValor[i].style.display='none';
			inputValor[i].required=false;
			inputValor[i].disabled=true;
		}

		for (var i = 0; i < hideInput.length; i++) {
			hideInput[i].required = false;
			hideInput[i].disabled=true;
		}
	}*/
}



// Script acepto condiciones
$(document).ready(function() {
	$('#acepto').click(function(){
		$('#condiciones').toggle('fast');
		$('#alert_instrucciones').toggle('fast');
	});
});
$(document).ready(function() {
	$('#btn_entiendo').click(function(){
		$('#dv_formulario').show('fast');
		$('#dv_alerta').hide();
		});
});
$(document).ready(function(){
	$('#periodo').popover();
});


//cargar Inputs del Certificado

$(document).ready(function(){
	$('#estudio_ra').click(function(){
		$('#input_extras').load('selectTagPeriodos.php');
	});
});
$(document).ready(function(){
	$('#notas_ra').click(function(){
		$('#input_extras').load('selectTagPeriodos.php');
	});
});
$(document).ready(function(){
	$('#notas2_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#horario_ra').click(function(){
		$('#input_extras').load('selectTagPeriodos.php');
	});
});
$(document).ready(function(){
	$('#horario2_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#conducta_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#termino_asignatura_ra').click(function(){
		$('#input_extras').empty();
	});
});

/**/
$(document).ready(function(){
	$('#financiero_fpc_ra').click(function(){
		$('#input_extras').html('<div class="form-group"><label>Valor ($)*</label><input type="number" class="form-control" name="valor" maxlength="10" form="form_solicitud_certificado" placeholder="Digite el valor a certificar" required></div><div class="form-group"><label>Entidad Solcitante*</label><input type="text" class="form-control" name="entidad" maxlength="50" form="form_solicitud_certificado" placeholder="Nombre de la Entidad que Solcita" required></div>');
	});
});
$(document).ready(function(){
	$('#financiero_ps_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#finaciero_declaracion_ra').click(function(){
		$('#input_extras').html('<div class="form-group"><label>Año Declaración*</label><input type="number" class="form-control" name="anio_declaracion" maxlength="4" form="form_solicitud_certificado" placeholder="Año declaración" required></div>');
	});
});
$(document).ready(function(){
	$('#ciclo_prop_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#ciclo_prof_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#doble_tit_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#egresado_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#contenido_prog_car_ra').click(function(){
		$('#input_extras').empty();
	});
});
$(document).ready(function(){
	$('#contenido_prog_mat_ra').click(function(){
		$('#input_extras').html('<div class="form-group"><label>Especifíque la(s) Materia(s)*</label><textarea class="form-control" name="materias" rows="5" maxlength="100" form="form_solicitud_certificado" placeholder="Especifíque la(s) Materia(s). 100 caracteres" required></textarea></div>');
	});
});
$(document).ready(function(){
	$('#carnet_ra').click(function(){
		$('#input_extras').load('inputsCarnet.php');
	});
});
	   


// Script .popover formato de los certificados
$(document).ready(function(){
	var certficado_estudio = '<img src="../../../files/certificados/modelos/01_estudio.png" class="w3-image">';
	$('#estudio').popover({content:certficado_estudio, html:true});
	
	var certficado_notas = '<img src="../../media/modelos/02_nota_semestre.png">';
	$('#notas').popover({content:certficado_notas, html:true}); 
	
	var certficado_notas2 = '<img src="../../media/modelos/03_nota_semestres.png">';
	$('#notas2').popover({content:certficado_notas2, html:true});
	
	var certficado_horario = '<img src="../../media/modelos/04_horario.png">';
	$('#horario').popover({content:certficado_horario, html:true});
	
	var certficado_horario2 = '<img src="../../media/modelos/05_hora_carrera.png">';
	$('#horario2').popover({content:certficado_horario2, html:true});
	
	var certficado_conducta = '<img src="../../media/modelos/06_conducta.png">';
	$('#conducta').popover({content:certficado_conducta, html:true});
	
	var certficado_termino_asig = '<img src="../../media/modelos/07_term_asignatura.png">';
	$('#termino_asignatura').popover({content:certficado_termino_asig, html:true});
	
	var certficado_financiero_fpc = '<img src="../../media/modelos/08_fina_cesantias.png">';
	$('#financiero_fpc').popover({content:certficado_financiero_fpc, html:true});
	
	var certficado_financiero_ps = '<img src="../../media/modelos/09_fina_paz_y_salvo.png">';
	$('#financiero_ps').popover({content:certficado_financiero_ps, html:true});
	
	var certficado_ciclo_prop = '<img src="../../media/modelos/10_cicl_propedeuticos.png">';
	$('#ciclo_prop').popover({content:certficado_ciclo_prop, html:true});
	
	var certficado_ciclo_prof = '<img src="../../media/modelos/11_cicl_profesional.png">';
	$('#ciclo_prof').popover({content:certficado_ciclo_prof, html:true});
	
	var certficado_doble_tit = '<img src="../../media/modelos/12_dobl_titulacion.png">';
	$('#doble_tit').popover({content:certficado_doble_tit, html:true});
	
	var certficado_egresado = '<img src="../../media/modelos/13_egre_grado.png">';
	$('#egresado').popover({content:certficado_egresado, html:true});
	
	var codigo_liquidacion = '<img src="../../media/modelos/codigo_liquidacion.png">';
	$('#liquidacion').popover({content:codigo_liquidacion, html:true});
});
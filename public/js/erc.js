/*
* Funcion para verificar los valores de cada conceto de caja  y para calcular el valor total recibo de caja
*/
function totalAPagar(){
	var matricula = parseFloat(document.getElementById('matricula').value);
	var carnet = parseFloat(document.getElementById('carnet').value);
	var precooperativa = parseFloat(document.getElementById('precooperativa').value);
	var curso_verano = parseFloat(document.getElementById('curso_verano').value);
	var certificados = parseFloat(document.getElementById('certificados').value);
	var derechos_grado = parseFloat(document.getElementById('derechos_grado').value);
	var habilitacion = parseFloat(document.getElementById('habilitacion').value);
	var seguro_estudiantil = parseFloat(document.getElementById('seguro_estudiantil').value);
	var estampilla = parseFloat(document.getElementById('estampilla').value);
	var inscripcion = parseFloat(document.getElementById('inscripcion').value);
	var homologacion = parseFloat(document.getElementById('homologacion').value);
	var otros_conceptos = parseFloat(document.getElementById('otros_conceptos').value);

	//valida que el campo sea un numero
	if (isNaN(matricula) || matricula<0){
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('matricula').focus();
	}else if (isNaN(carnet) || carnet<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('carnet').focus();
	}else if (isNaN(precooperativa) || precooperativa<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('precooperativa').focus();
	}else if (isNaN(curso_verano) || curso_verano<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('curso_verano').focus();
	}else if (isNaN(certificados) || certificados<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('certificados').focus();
	}else if (isNaN(derechos_grado) || derechos_grado<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('derechos_grado').focus();
	}else if (isNaN(habilitacion) || habilitacion<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('habilitacion').focus();
	}else if (isNaN(seguro_estudiantil) || seguro_estudiantil<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('seguro_estudiantil').focus();
	}else if (isNaN(estampilla) || estampilla<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('estampilla').focus();
  	}else if (isNaN(inscripcion) || inscripcion<0) {
  		document.getElementById('alerta').hidden=false;
  		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('inscripcion').focus();
	}else if (isNaN(homologacion) || homologacion<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('homologacion').focus();
	}else if (isNaN(otros_conceptos) || otros_conceptos<0) {
		document.getElementById('alerta').hidden=false;
		document.getElementById('guardar_formulario').disabled=true;
		document.getElementById('otros_conceptos').focus();
	}else{
		document.getElementById('alerta').hidden=true;
		document.getElementById('guardar_formulario').disabled=false;
	}

	//total a pagar
	var total;
	total = (matricula+carnet+precooperativa+curso_verano+certificados+derechos_grado+habilitacion+seguro_estudiantil
		+estampilla+inscripcion+homologacion+otros_conceptos);
	var respuesta = document.getElementById('total').value=total;
	return respuesta;
}


/*
* Funcion que despliega los INPUT de cada uno de las modalidades de pago de un recibo
*/
function displayPago(id) {
	var inputPago = document.getElementById('checkbox_'+id).checked;
	var inputValor = document.getElementsByClassName('valor_'+id);
	var hideInput = document.getElementsByClassName('hide_'+id);
	//window.alert(inputPago);
	if(inputPago === true) {
		for (var i = 0; i < inputValor.length; i++) {
			inputValor[i].style.display='block';
			inputValor[i].required='true';
			inputValor[i].disabled=false;
		}

		for (var i = 0; i < hideInput.length; i++) {
			hideInput[i].style.display='none'; // visibles: no
			hideInput[i].required=false; //requerido: no
			hideInput[i].disabled=false; //habilitados: si
		}
	}
	else {
		for (var i = 0; i < inputValor.length; i++) {
			inputValor[i].style.display='none';
			inputValor[i].required=false;
			inputValor[i].disabled=true;
		}

		for (var i = 0; i < hideInput.length; i++) {
			hideInput[i].required = false;
			hideInput[i].disabled=true;
		}
	}
}

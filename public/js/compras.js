/*Agregar fila a la table insumos*/
function agregarFilaInsumos(){
	//duplica la fila base, remueve la clase fila-base y la agrega al cuerpo de la tabla #tabla
	$(".fila-insumo:first-child").clone().removeClass('fila-insumo').addClass('fila').appendTo("#tabla-insumos tbody");
	$(".fila:last-child").remove();

	//agregar el atributo requerido a cada input de la fila agregada y remueve el atributo disabled
	$(".referencia:first-child").attr('required', 'required');
	$(".referencia:first-child").removeAttr('disabled');

	$(".medida:first-child").attr('required', 'required');
	$(".medida:first-child").removeAttr('disabled');

	$(".cantidad:first-child").attr('required', 'required');
	$(".cantidad:first-child").removeAttr('disabled');

	$(".caracteristica:first-child").attr('required', 'required');
	$(".caracteristica:first-child").removeAttr('disabled');

	//$(".costo:first-child").attr('required', 'required');
	$(".costo:first-child").removeAttr('disabled');

	//inhabilita todos los input y select de la fila base
	$(".fila-insumo:first-child input").removeAttr('required');
	$(".fila-insumo:first-child input").attr('disabled', 'disabled');

	$(".fila-insumo:first-child select").removeAttr('required');
	$(".fila-insumo:first-child select").attr('disabled', 'disabled');
}

/*Eliminar fila de la table insumos*/
$(document).on("click",".eliminar-insumo",function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});


/*Agregar fila a la table centros de costos*/
function agregarCentroCostos(){
	$("#centrocostos_id").val($("#centrocostos_select").find('option:selected').val()).removeAttr("disabled");
	$("#centrocostos_nombre").val($("#centrocostos_select").find('option:selected').text());
	$(".fila-centro:first-child").clone().removeClass('fila-centro input-centro').addClass('nueva-fila-centro').appendTo("#tabla-centro tbody");
	$(".nueva-fila-centro:last-child").remove();
	$(".input-centro:first").val('').attr("disabled", "disabled");
}

/*Eliminar fila de la table centros de costos*/
$(document).on("click",".eliminar-centro", function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});

/*Agregar fila a la table centros de operacion*/
function agregarCentroOperacion(){
	$("#centrooperacion_id").val($("#centrooperacion_select").find('option:selected').val()).removeAttr("disabled");
	$("#centrooperacion_nombre").val($("#centrooperacion_select").find('option:selected').text());
	$(".fila-centro-operacion:first-child").clone().removeClass('fila-centro-operacion').addClass('nueva-fila-centro-operacion').appendTo("#tabla-centro-operacion tbody");
	$(".nueva-fila-centro-operacion:last-child").remove();
	$(".input-centro-operacion:first").val('').attr("disabled", "disabled");
}

/*Eliminar fila de la table centros de costos*/
$(document).on("click",".eliminar-centro-operacion", function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});

/*Eliminar fila de la table centros de costos*/
$(document).on("click",".eliminar-centro", function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});

/*Agregar fila a la table proyectos*/
function agregarProyecto(){
	$("#proyecto_id").val($("#proyecto_select").find('option:selected').val()).removeAttr("disabled");
	$("#proyecto_nombre").val($("#proyecto_select").find('option:selected').text());
	$(".fila-proyecto:first-child").clone().removeClass('fila-proyecto').addClass('nueva-fila-proyecto').appendTo("#tabla-proyectos tbody");
	$(".nueva-fila-proyecto:last-child").remove();
	$(".input-proyecto:first").val('').attr("disabled", "disabled");
}

/*Eliminar fila de la table proyectos*/
$(document).on("click",".eliminar-proyecto", function(){
	var parent = $(this).parents().get(0);
	$(parent).remove();
});


/*function validarcentros(){
	var centro_costos = $('input#centrocostos_id').val();
	var centro_operacion = $('input#centrooperacion_id').val();

	if(centro_costos == null || centro_costos == '' || centro_operacion == null || centro_operacion == ''){
		//alert('No hay seleccionado');
		document.getElementById('submit_btn').disabled=true;
	}else{
		alert('Seleccionado');
		document.getElementById('submit_btn').disabled=false;
	}

}*/

function verSelecciones(){
	var array = new Array();

	var centro_costos = $('select#cen_costos option:selected').each(function(){
		array.push($(this).val());
	});

	$.each(function(){
		$('#centros_costos').html(array.val());
	});
}

<?php

Route::get('/browse', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('browse')->user();

    return view('browse.home');
});

<?php
 use App\Sistema;
 use App\Area;
 use App\Subarea;
 use App\Proyecto;
 use App\CentroCostos;
 use App\CentroOperacion;
 use App\Model\Compras\Solicitud;
  use App\Model\Certificados\Solicitud as SolicitudCertificado;

 //Mail Classes
 use App\Mail\Compras\NotificacionNuevaSolicitud;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*
|--------------------------------------------------------------------------
| Browse prefix
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'browse'], function () {
  Route::get('/', function () {
    return view('browse.auth.login');
  });

	Route::get('/home', 'BrowseController@index')->name('browse.home');

  //login and password reset
  Route::get('/login', 'BrowseAuth\LoginController@showLoginForm');
  Route::post('/login', 'BrowseAuth\LoginController@login')->name('login');
  Route::post('/logout', 'BrowseAuth\LoginController@logout');

  Route::get('/register', 'BrowseAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'BrowseAuth\RegisterController@register');

  Route::post('/password/email', 'BrowseAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'BrowseAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'BrowseAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'BrowseAuth\ResetPasswordController@showResetForm');

  /*
  |--------------------------------------------------------------------------
  | administracion del sitio
  |--------------------------------------------------------------------------
  */
  //main management panel menu
  Route::get('/site/home', 'SiteController@index')->name('browse.site.home');

  /**
  * Systems
  *
  *
  */
  //show systems list
  Route::get('/site/sistemas/index', 'SistemasController@index')->name('browse.site.sistemas.index');

  //let read the system manual on page
  Route::get('/site/sistemas/{sistema}/manual/read', 'SistemasController@read')->name('browse.site.sistemas.manual.readmanual');

  //let download the system manual
  Route::get('/site/sistemas/download/manual/{sistema}', function (Sistema $sistema) {
    $fichero = 'files/manuales/'.$sistema->sistema_manual;
    return Response::download($fichero);
  });

  //create a new system
  Route::get('/site/sistemas/create', 'SistemasController@create')->name('browse.site.sistemas.create');
  Route::post('/site/sistemas/store', 'SistemasController@store')->name('browse.site.sistemas.store');

  //edit a system
  Route::get('/site/sistemas/{sistema}/edit', 'SistemasController@edit')->name('browse.site.sistemas.edit');
  Route::put('/site/sistemas/{sistema}/update', 'SistemasController@update')->name('browse.site.sistemas.update');

  //Enable system
  Route::get('/site/sistemas/{sistema}/enable', 'SistemasController@enable')->name('browse.site.sistemas.enable');

  //Disable system
  Route::get('/site/sistemas/{sistema}/disable', 'SistemasController@disable')->name('browse.site.sistemas.disable');

  
  /**
  * Users
  *
  *
  */
  //user profile
  Route::get('/site/usuarios/perfil', 'UsuariosController@profile')->name('browse.site.usuarios.perfil');
  Route::put('/site/usuarios/perfil/update', 'UsuariosController@updateProfile')->name('browse.site.usuarios.perfil.update');
  Route::put('/site/usuarios/perfil/password', 'UsuariosController@password')->name('browse.site.usuarios.perfil.updatepassword');

  //show the activated users list
  Route::get('/site/usuarios', 'UsuariosController@index')->name('browse.site.usuarios.index');

  //let activate an email account as a SIA user
  Route::get('/site/usuarios/{cuenta}/activate', 'UsuariosController@activate')->name('browse.site.usuarios.activate');
  Route::post('/site/usuarios/activate/store', 'UsuariosController@storeActivation')->name('browse.site.usuarios.storeactivation');

  //update an user role at SIA
  Route::get('/site/usuarios/{usuario}/edit', 'UsuariosController@edit')->name('browse.site.usuarios.edit');
  Route::put('/site/usuarios/{usuario}/update', 'UsuariosController@update')->name('browse.site.usuarios.update');
  
  //let create and update user permissions
  Route::get('/site/usuarios/{usuario}/permisos', 'UsuariosController@permisos')->name('browse.site.usuarios.permisos');
  Route::put('/site/usuarios/{usuario}/updatepermisos', 'UsuariosController@updatepermisos')->name('browse.site.usuarios.updatepermisos');

  //let deactivate an user
  Route::get('/site/usuarios/{usuario}/destroy', 'UsuariosController@destroy')->name('browse.site.usuarios.destroy');

  //show user's entrances
  Route::get('/site/usuarios/{usuario}/entrances', 'UsuariosController@entrances')->name('browse.site.usuarios.entrances');

  /*
  * Roles
  *
  *
  */
  //show roles list
  Route::get('/site/roles/index', 'RolesController@index')->name('browse.site.roles.index');
  //create and store a new role
  Route::get('/site/roles/create', 'RolesController@create')->name('browse.site.roles.create');
  Route::post('/site/roles/create', 'RolesController@store')->name('browse.site.roles.store');
  //edit and update a role
  Route::get('/site/roles/edit/{rol}', 'RolesController@edit')->name('browse.site.roles.edit');
  Route::put('/site/roles/update/{rol}', 'RolesController@update')->name('browse.site.roles.update');
  //destroy a role from DB
  Route::get('/site/roles/destroy/{rol}', 'RolesController@destroy')->name('browse.site.roles.destroy');

  /*
  * Banner
  *
  *
  */

  Route::get('/site/banner/index', 'Banner\BannerController@fetchMatriculados')->name('browse.site.banner.index');

  /**
  * Contratos
  *
  *
  */
  Route::get('/site/contratos/index', 'ContratoController@index')->name('browse.site.contratos.index');

  Route::get('/site/contratos/create', 'ContratoController@create')->name('browse.site.contratos.create');

  Route::post('/site/contratos/store', 'ContratoController@store')->name('browse.site.contratos.store');

  Route::get('/site/contratos/{contrato}/edit', 'ContratoController@edit')->name('browse.site.contratos.edit');

  Route::put('/site/contratos/{contrato}/update', 'ContratoController@update')->name('browse.site.contratos.update');

  /**
  * Sedes
  *
  *
  */
  Route::get('/site/sedes/index', 'SedeController@index')->name('browse.site.sedes.index');

  Route::get('/site/sedes/create', 'SedeController@create')->name('browse.site.sedes.create');

  Route::post('/site/sedes/store', 'SedeController@store')->name('browse.site.sedes.store');

  Route::get('/site/sedes/{sede}/edit', 'SedeController@edit')->name('browse.site.sedes.edit');

  Route::put('/site/sedes/{sede}/update', 'SedeController@update')->name('browse.site.sedes.update');

  /**
  * Facultades
  *
  *
  */
  Route::get('/site/facultades/index', 'FacultadController@index')->name('browse.site.facultades.index');

  Route::get('/site/facultades/create', 'FacultadController@create')->name('browse.site.facultades.create');

  Route::post('/site/facultades/store', 'FacultadController@store')->name('browse.site.facultades.store');

  Route::get('/site/facultades/{facultad}/edit', 'FacultadController@edit')->name('browse.site.facultades.edit');

  Route::put('/site/facultades/{facultad}/update', 'FacultadController@update')->name('browse.site.facultades.update');

  /**
  * Programas
  *
  *
  */
  Route::get('/site/programas/index', 'ProgramaController@index')->name('browse.site.programas.index');

  Route::get('/site/programas/create', 'ProgramaController@create')->name('browse.site.programas.create');

  Route::post('/site/programas/store', 'ProgramaController@store')->name('browse.site.programas.store');

  Route::get('/site/programas/{programa}/edit', 'ProgramaController@edit')->name('browse.site.programas.edit');

  Route::put('/site/programas/{programa}/update', 'ProgramaController@update')->name('browse.site.programas.update');

  /*
  |--------------------------------------------------------------------------
  | systems menu
  |--------------------------------------------------------------------------
  */
  //routes of the main systems menus
  Route::get('/certificados/{sistema}', 'BrowseController@modulo')->name('certificados');
  Route::get('/compras/{sistema}', 'BrowseController@modulo')->name('compras');
  Route::get('/erc/{sistema}', 'BrowseController@modulo')->name('erc');
  Route::get('/correos/{sistema}', 'BrowseController@modulo')->name('correos');
  Route::get('/directorio/{sistema}', 'BrowseController@modulo')->name('directorio');
  Route::get('/micrositio/{sistema}', 'BrowseController@modulo')->name('micrositio');
  Route::get('/vb/{sistema}', 'BrowseController@modulo')->name('vb');
  Route::get('/contratos/{sistema}', 'BrowseController@modulo')->name('contratos');
  Route::get('/egresados/{sistema}', 'BrowseController@modulo')->name('egresados');
  Route::get('/cancelaciones/{sistema}', 'BrowseController@modulo')->name('cancelaciones');
  Route::get('/grados/{sistema}', 'BrowseController@modulo')->name('grados');

  /*
  |--------------------------------------------------------------------------
  | Certificados Routes
  |--------------------------------------------------------------------------
  */
  //reload module
  Route::get('/certificados/home/{sistema}', 'BrowseController@menu')->name('browse.certificados.menu');

  //show all requests to process them according to their state
  Route::get('/certificados/{sistema}/solicitudes', 'Browse\Certificados\SolicitudesController@index')->name('browse.certificados.solicitudes');

  Route::get('/certificados/{sistema}/solicitudes/{solicitud}/show', 'Browse\Certificados\SolicitudesController@show')->name('browse.certificados.solicitudes.show');

  Route::get('/certificados/{sistema}/solicitudes/{solicitud}/process', 'Browse\Certificados\SolicitudesController@process')->name('browse.certificados.solicitudes.process');

  //download student card list requests in pdf format
  Route::get('/certificados/{sistema}/solicitudes/download/list', function (Sistema $sistema)
  {
    $solicitudes = SolicitudCertificado::orderBy('created_at', 'DESC')->where([['tipo_id', '=', 16], ['estado_id', '=', 1]])->get();

    $usuario_imprime = \Auth::user()->email;
    $pdf = PDF::loadView('browse.certificados.solicitudes.downloadlist', [
      'solicitudes' => $solicitudes,
    ]);
    return $pdf->setPaper('letter', 'landscape')->download('solicitudes_carnets_'.time().'.pdf');
  })->name('browse.certificados.solicitudes.downloadlist');

  Route::get('/certificados/solicitudes/download/photo/{solicitud}', function (SolicitudCertificado $solicitud) {
    $fichero = 'files/certificados/carnets/'.$solicitud->solicitud_carnet_foto;
    return Response::download($fichero);
  });

  //print a request
  Route::get('/certificados/{sistema}/solicitudes/print/{solicitud}', 'Browse\Certificados\SolicitudesController@print')->name('browse.certificados.solicitudes.print');


  /*
  |--------------------------------------------------------------------------
  | Compras Routes
  |--------------------------------------------------------------------------
  */
  //reload module
  Route::get('/compras/home/{sistema}', 'BrowseController@menu')->name('browse.compras.menu');

  //show all requests to process them according to their state
  Route::get('/compras/solicitudes/process/{sistema}', 'Browse\compras\SolicitudesController@index')->name('browse.compras.solicitudes.index');

  //let create and store a new request
  Route::get('/compras/{sistema}/solicitudes/create', 'Browse\compras\SolicitudesController@create')->name('browse.compras.solicitudes.create');
  Route::post('/compras/{sistema}/solicitudes/store', 'Browse\compras\SolicitudesController@store')->name('browse.compras.solicitudes.store');

  //show all requests of an user
  Route::get('/compras/{sistema}/solicitudes/myrequests', 'Browse\compras\SolicitudesController@my')->name('browse.compras.solicitudes.myrequests');

  //show a request
  Route::get('/compras/{sistema}/solicitudes/show/{solicitud}', 'Browse\compras\SolicitudesController@show')->name('browse.compras.solicitudes.show');

  //edit and update a request
  Route::get('/compras/{sistema}/solicitudes/edit/{solicitud}', 'Browse\compras\SolicitudesController@edit')->name('browse.compras.solicitudes.edit');
  Route::put('/compras/{sistema}/solicitudes/update/{solicitud}', 'Browse\compras\SolicitudesController@update')->name('browse.compras.solicitudes.update');

  //send mail of request
  Route::get('/compras/{sistema}/solicitudes/mail/{solicitud}', function(Sistema $sistema, Solicitud $solicitud) {
    Mail::to($solicitud->solicitud_email_solicitante)
    ->cc(['sia@unicatolica.edu.co'])
    ->send(new NotificacionNuevaSolicitud($sistema, $solicitud));
  })->name('browse.compras.emails.new');


  //download a request in pdf format
  Route::get('/compras/{sistema}/solicitudes/download/{solicitud}', function (Sistema $sistema, Solicitud $solicitud)
  {
    $sistemas = Sistema::orderBy('sistema_nombre', 'ASC')->get();

    $areas = Area::get();
    $subareas = Subarea::get();
    $centros_costos = CentroCostos::get();
    $centros_operacion = CentroOperacion::get();
    $proyectos = Proyecto::get();

    $usuario_imprime = \Auth::user()->email;
    $pdf = PDF::loadView('browse.compras.solicitudes.download', [
      'solicitud' => $solicitud,
      'sistema' => $sistema,
      'areas' => $areas,
      'subareas' => $subareas,
      'centros_costos' => $centros_costos,
      'centros_operacion' => $centros_operacion,
      'proyectos' => $proyectos,
    ]);
    return $pdf->download($solicitud->solicitud_id.'.pdf');
  })->name('browse.compras.solicitudes.download');

  //Save a request in PDF format after archiving or rejecting.
  Route::get('/compras/{sistema}/solicitudes/{solicitud}/store/pdf', 'Browse\compras\SolicitudesController@storePdf')->name('browse.compras.solicitudes.store.pdf');

  //let download the PDF file of request that has been archived o rejected
  Route::get('/compras/{sistema}/solicitudes/{solicitud}/download/pdf', function (Sistema $sistema, Solicitud $solicitud) {
    $fichero = 'files/compras/solicitudes/'.$solicitud->solicitud_id.'.pdf';
    if(!file_exists($fichero))
    {
      Alert::error('<p>La solicitud no se encuentra en formato PDF.</p>', 'Error')->html()->persistent();
      
      return redirect()->back();
    }
    else
    {
      return Response::download($fichero);
    }
  })->name('browse.compras.solicitudes.download.pdf');


  Route::get('/compras/{sistema}/solicitudes/myrequests/editar/{solicitud}', 'Browse\compras\SolicitudesController@edit')->name('browse.compras.solicitudes.editar');

  Route::get('/compras/{sistema}/solicitudes/myrequests/firmar/{usuario}/{solicitud}', 'Browse\compras\SolicitudesController@firmar')->name('browse.compras.solicitudes.firmar');

  //let download the request budget
  Route::get('/compras/{sistema}/solicitudes/{solicitud}/budget/download', function ($sistema, Solicitud $solicitud) {
    $fichero = 'files/compras/presupuestos/'.$solicitud->solicitud_presupuesto;
    return Response::download($fichero);
  })->name('browse.compras.budget.download');

  //comentarios
  Route::post('/compras/{sistema}/solicitudes/comentarios/store/{solicitud}', 'Browse\compras\ComentariosController@store')->name('browse.compras.solicitudes.comentarios.store');


  /**
  * 
  * Managing requests
  *
  */

  //evaluate a request
  Route::get('/compras/solicitudes/evaluate/{sistema}/{solicitud}', 'Browse\compras\SolicitudesController@evaluate')->name('browse.compras.solicitudes.evaluate');
  Route::post('/compras/solicitudes/archive/{sistema}/{solicitud}', 'Browse\compras\SolicitudesController@archive')->name('browse.compras.solicitudes.archive');

  //sign a request
  Route::get('/compras/solicitudes/sign/{sistema}/{solicitud}', 'Browse\compras\SolicitudesController@sign')->name('browse.compras.solicitudes.sign');
  Route::get('/compras/solicitudes/updatesign/{sistema}/{solicitud}', 'Browse\compras\SolicitudesController@updateSign')->name('browse.compras.solicitudes.updatesign');
  Route::get('/compras/solicitudes/check/{sistema}/{solicitud}', 'Browse\compras\SolicitudesController@check')->name('browse.compras.solicitudes.check');

  //deny a request
  Route::post('/compras/{sistema}/solicitudes/deny/{solicitud}', 'Browse\compras\SolicitudesController@deny')->name('browse.compras.solicitudes.deny');

  //reject a request
  Route::get('/compras/{sistema}/solicitudes/reject/{solicitud}', 'Browse\compras\SolicitudesController@reject')->name('browse.compras.solicitudes.reject');



  //display all supplies of request to process them later
  Route::get('/compras/{sistema}/solicitudes/insumos/{solicitud}', 'Browse\compras\InsumosController@index')->name('browse.compras.solicitudes.insumos.index');

  //process a supply
  Route::get('/compras/{sistema}/solicitudes/insumos/{solicitud}/{insumo}/{estado}', 'Browse\compras\InsumosController@update')->name('browse.compras.solicitudes.insumos.update');

  //download supplies
  Route::get('/compras/{sistema}/solicitudes/{solicitud}/insumos/download/xls', 'Browse\compras\InsumosController@downloadXls')->name('browse.compras.solicitudes.insumos.downloadxls');



  //Permisisions view
  Route::get('/compras/{sistema}/autorizaciones', 'Browse\compras\AutorizacionesController@index')->name('browse.compras.autorizaciones');

  //Let edit and update permissions
  Route::get('/compras/{sistema}/autorizaciones/edit/{usuario}', 'Browse\compras\AutorizacionesController@edit')->name('browse.compras.autorizaciones.edit');
  Route::put('/compras/{sistema}/autorizaciones/update/{usuario}', 'Browse\compras\AutorizacionesController@update')->name('compras.autorizaciones.update');

  //revisiones
  //Let edit and update revisions
  Route::get('/compras/{sistema}/revisiones/edit/{usuario}', 'Browse\compras\RevisionesController@edit')->name('browse.compras.revisiones.edit');
  Route::put('/compras/{sistema}/revisiones/update/{usuario}', 'Browse\compras\RevisionesController@update')->name('browse.compras.revisiones.update');

  /*
  |--------------------------------------------------------------------------
  | eRC Routes
  |--------------------------------------------------------------------------
  */
  //reload module
  Route::get('/erc/home/{sistema}', 'BrowseController@menu')->name('browse.erc.menu');

  Route::get('/erc/recibos/historial/{sistema}', 'Browse\erc\RecibosController@index')->name('erc.recibos.historial');

  Route::get('/erc/recibos/cartera/{sistema}', 'Browse\erc\RecibosController@cartera')->name('erc.recibos.cartera');

  Route::get('/erc/recibos/cartera/contabilizar/{sistema}/{recibo}', 'Browse\erc\RecibosController@contabilizar')->name('erc.recibos.contabilizar');

  Route::get('/erc/recibos/show/{sistema}/{recibo}', 'Browse\erc\RecibosController@show')->name('erc.recibos.show');

  Route::get('/erc/recibos/create/{sistema}', 'Browse\erc\RecibosController@create')->name('erc.recibos.create');

  Route::post('/erc/recibos/store/{sistema}', 'Browse\erc\RecibosController@store')->name('erc.recibos.store');

  Route::get('/erc/recibos/edit/{sistema}/{recibo}', 'Browse\erc\RecibosController@edit')->name('erc.recibos.edit');

  Route::put('/erc/recibos/update/{sistema}/{recibo}', 'Browse\erc\RecibosController@update')->name('erc.recibos.update');

  //download recibo de caja pdf format
  Route::get('/erc/recibos/download/{sistema}/{recibo}', function ($sistema, $recibo) {
    $recibo = App\Model\Erc\Recibo::find($recibo);
    $recibo_centro = $recibo->centroOperacion;
    $recibo_concepto = $recibo->concepto;
    $recibo_pago = $recibo->pagos;

    $usuario_imprime = \Auth::user()->email;
    $pdf = PDF::loadView('browse.erc.recibos.download', ['recibo' => $recibo, 'usuario' => $usuario_imprime]);
    return $pdf->download('erc_'.$recibo->centroOperacion->centrooperacion_codigo.'_'.$recibo->recibo_consecutivo_centro.'.pdf');
  });

  /*
  |--------------------------------------------------------------------------
  | Correos Routes
  |--------------------------------------------------------------------------
  */
  //reload module
  Route::get('/correos/home/{sistema}', 'BrowseController@menu')->name('browse.correos.menu');

  //let create and store data for a new account
  Route::get('/correos/cuentas/create/{sistema}', 'Browse\correos\CuentasController@create')->name('browse.correos.cuentas.create');
  Route::post('/correos/cuentas/store/{sistema}', 'Browse\correos\CuentasController@store')->name('browse.correos.cuentas.store');
  Route::post('/correos/cuentas/storeadmin/{sistema}', 'Browse\correos\CuentasController@storeAdmin')->name('browse.correos.cuentas.storeadmin');

  //let users update their profiles data
  Route::get('/correos/cuentas/account/{sistema}', 'Browse\correos\CuentasController@account')->name('browse.correos.cuentas.account');
  Route::put('/correos/cuentas/updateaccount/{sistema}/{id}', 'Browse\correos\CuentasController@updateAccount')->name('browse.correos.cuentas.updateaccount');  

  /*
  * Management
  */

  //show accounts list
  Route::get('/correos/cuentas/{sistema}/index/', 'Browse\correos\CuentasController@index')->name('browse.correos.cuentas.index');

  //show requests
  Route::get('/correos/cuentas/{sistema}/requests/', 'Browse\correos\CuentasController@requests')->name('browse.correos.cuentas.requests');

  //let process an account request
  Route::get('/correos/cuentas/process/{sistema}/{cuenta}/{estado}', 'Browse\correos\CuentasController@process')->name('browse.correos.cuentas.process');
  Route::get('/correos/cuentas/processrequest/{sistema}/{cuenta}/{estado}', 'Browse\correos\CuentasController@processRequest')->name('browse.correos.cuentas.processrequest');

  //let edit and update an account
  Route::get('/correos/cuentas/edit/{sistema}/{cuenta}', 'Browse\correos\CuentasController@edit')->name('browse.correos.cuentas.edit');
  Route::put('/correos/cuentas/update/{sistema}/{cuenta}', 'Browse\correos\CuentasController@update')->name('browse.correos.cuentas.update');

  //ruta para eliminar un registro
  Route::get('/correos/cuentas/{cuenta}/destroy', 'Browse\correos\CuentasController@destroy')->name('browse.correos.cuentas.destroy');

  /*
  |--------------------------------------------------------------------------
  | Directorio Routes
  |--------------------------------------------------------------------------
  */

  //reload module
  Route::get('/directorio/{sistema}/home', 'BrowseController@menu')->name('browse.directorio.menu');

  //show directory
  Route::get('/directorio/{sistema}/extensiones/directory', 'Browse\Directorio\ExtensionesController@directory')->name('browse.directorio.extensiones.directory');

  //show all extensions
  Route::get('/directorio/{sistema}/extensiones/index', 'Browse\Directorio\ExtensionesController@index')->name('browse.directorio.extensiones.index');

  //show creation form
  Route::get('/directorio/{sistema}/extensiones/create', 'Browse\Directorio\ExtensionesController@create')->name('browse.directorio.extensiones.create');
  Route::post('/directorio/{sistema}/extensiones/store', 'Browse\Directorio\ExtensionesController@store')->name('browse.directorio.extensiones.store');

  //show edit form
  Route::get('/directorio/{sistema}/extensiones/edit/{extension}', 'Browse\Directorio\ExtensionesController@edit')->name('browse.directorio.extensiones.edit');
  Route::put('/directorio/{sistema}/extensiones/update/{extension}', 'Browse\Directorio\ExtensionesController@update')->name('browse.directorio.extensiones.update');

 /*
  |--------------------------------------------------------------------------
  | Egresados Routes
  |--------------------------------------------------------------------------
  */

  //reload module
  Route::get('/egresados/home/{sistema}', 'BrowseController@menu')->name('browse.egresados.menu');

  Route::get('/egresados/{sistema}/reportes/formData', 'Browse\Egresados\EgresadoController@index_formData')->name('browse.egresados.reportes.formData');
  //Route::get('/egresados/{sistema}/prueba', 'Browse\Egresados\EgresadoController@prueba')->name('prueba');
  //Formulario con los filtros | Wizard
  Route::get('/egresados/{sistema}/reportes/wizard', 'Browse\Egresados\EgresadoController@generate_data')->name('form-Wizard');
  Route::get('/egresados/{sistema}/graphics', 'Browse\Egresados\EgresadoController@graphics')->name('graphics');
  //Formulario para graficar
  Route::get('/egresados/{sistema}/reportes/graphics', 'Browse\Egresados\EgresadoController@form_graphics')->name('browse.egresados.reportes.graficos');
  //Formulario para actualizar
  Route::get('/egresados/{sistema}/formulario/form', 'Browse\Egresados\EgresadoController@form')->name('browse.egresados.formulario.form');
  Route::get('/egresados/{sistema}/formulario/edit', 'Browse\Egresados\EgresadoController@edit')
              ->name('browse.egresados.formulario.edit'); 
  Route::get('/egresados/{sistema}/{egresado}/formulario/edit-table', 'Browse\Egresados\EgresadoController@edit_table')
              ->name('browse.egresados.formulario.edit-table'); 
  //Actualizar los datos
  Route::post('/egresados/{sistema}/{key}/formulario/update', 'Browse\Egresados\EgresadoController@update')
              ->name('browse.egresados.update'); 
  //Importar Archivo
  Route::get('/egresados/{sistema}/import', 'Browse\Egresados\EgresadoController@import')->name('browse.egresados.import');
  Route::post('/egresados/{sistema}/import/upload-students', 'Browse\Egresados\EgresadoController@uploadXls')->name('uploadxls');

  //enviar encuesta
  Route::get('/egresados/{sistema}/index-send', 'Browse\Egresados\EgresadoController@index_send_email')->name('browse.egresados.index-send');
  Route::get('/egresados/{sistema}/send', 'Browse\Egresados\EgresadoController@send_email')->name('browse.egresados.send');

  //Grado
  Route::get('/egresados/{sistema}/grado', 'Browse\Egresados\EgresadoController@create_grado')->name('browse.egresados.grado');
   Route::post('/egresados/{sistema}/import/upload-grade', 'Browse\Egresados\EgresadoController@uploadXls_grade')->name('uploadxls-grade');

  /*
  |--------------------------------------------------------------------------
  | Cancelaciones Routes
  |--------------------------------------------------------------------------
  */
  //reload module
  Route::get('/cancelaciones/home/{sistema}', 'BrowseController@menu')->name('browse.cancelaciones.menu');

  //Solicitudes
  Route::get('/cancelaciones/{sistema}/solicitudes/index', 'Browse\Cancelaciones\SolicitudController@index')->name('browse.cancelaciones.solicitudes.index');

  Route::get('/cancelaciones/{sistema}/solicitudes/show/{solicitud}', 'Browse\Cancelaciones\SolicitudController@show')->name('browse.cancelaciones.solicitudes.show');

  Route::post('/cancelaciones/{sistema}/solicitudes/process/{solicitud}', 'Browse\Cancelaciones\SolicitudController@process')->name('browse.cancelaciones.solicitudes.process');

  Route::get('/cancelaciones/{sistema}/solicitudes/respuesta/{respuesta}', 'Browse\Cancelaciones\RespuestaController@sign')->name('browse.cancelaciones.respuesta.sign');

  //Apelaciones
  Route::get('/cancelaciones/{sistema}/apelaciones/index', 'Browse\Cancelaciones\ApelacionController@index')->name('browse.cancelaciones.apelaciones.index');

  Route::get('/cancelaciones/{sistema}/apelaciones/show/{apelacion}', 'Browse\Cancelaciones\ApelacionController@show')->name('browse.cancelaciones.apelaciones.show');

  Route::post('/cancelaciones/{sistema}/apelaciones/process/{apelacion}', 'Browse\Cancelaciones\ApelacionController@process')->name('browse.cancelaciones.apelaciones.process');

  //adjuntos
  Route::get('/cancelaciones/{sistema}/adjuntos/download/{adjunto}', 'Browse\Cancelaciones\AdjuntoController@download')->name('browse.cancelaciones.adjuntos.download');

  //soportes
  Route::post('/cancelaciones/{sistema}/soporte/store/{solicitud}', 'Browse\Cancelaciones\SoporteController@store')->name('browse.cancelaciones.soportes.store');
  Route::get('/cancelaciones/{sistema}/soporte/download/{soporte}', 'Browse\Cancelaciones\SoporteController@download')->name('browse.cancelaciones.soportes.download');

  //respuestas
  Route::get('/cancelaciones/{sistema}/respuestas/download/{respuesta}', 'Browse\Cancelaciones\RespuestaController@download')->name('browse.cancelaciones.respuestas.download');

  //autorizaciones
  Route::get('/cancelaciones/{sistema}/autorizaciones/index', 'Browse\Cancelaciones\AutorizacionController@index')->name('browse.cancelaciones.autorizaciones.index');

  Route::get('/cancelaciones/{sistema}/autorizaciones/create', 'Browse\Cancelaciones\AutorizacionController@create')->name('browse.cancelaciones.autorizaciones.create');

  Route::post('/cancelaciones/{sistema}/autorizaciones/store', 'Browse\Cancelaciones\AutorizacionController@store')->name('browse.cancelaciones.autorizaciones.store');

  Route::get('/cancelaciones/{sistema}/autorizaciones/edit/{autorizacion}', 'Browse\Cancelaciones\AutorizacionController@edit')->name('browse.cancelaciones.autorizaciones.edit');

  Route::post('/cancelaciones/{sistema}/autorizaciones/update/{autorizacion}', 'Browse\Cancelaciones\AutorizacionController@update')->name('browse.cancelaciones.autorizaciones.update');

  //Informes
  Route::get('/cancelaciones/{sistema}/informes/index', 'Browse\Cancelaciones\InformeController@index')->name('browse.cancelaciones.informes.index');

  Route::get('/cancelaciones/{sistema}/informes/requests', 'Browse\Cancelaciones\InformeController@requests')->name('browse.cancelaciones.informes.requests');

  Route::get('/cancelaciones/{sistema}/informes/requests/bar-chart', 'Browse\Cancelaciones\InformeController@requestsFilter')->name('browse.cancelaciones.informes.requests.barchart');

  /*
  |--------------------------------------------------------------------------
  | Grados
  |--------------------------------------------------------------------------
  */

  /*
  * Rutas secretaria de la facultad
  */
  Route::get('/grados/home/{sistema}', 'BrowseController@menu')->name('browse.grados.menu');
  Route::get('/grados/sefa/index/{sistema}', 'Browse\Grados\RoleController@sefaIndex')->name('browse.grados.sefa.index');
  Route::post('/grados/sefa/postulante/{sistema}', 'Browse\Grados\RoleController@sefaSearch')
        ->name('browse.grados.sefa.search');
  /*Route::post('/grados/sefa/postulante/{sistema}/estado', 'Browse\Grados\RoleController@estado')
        ->name('browse.grados.sefa.estado');*/
  Route::get('/grados/sefa/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@sefaSearchTable')
        ->name('browse.grados.sefa.search-table');

  /*
  * Rutas Biblioteca
  */
  Route::get('/grados/biblioteca/index/{sistema}', 'Browse\Grados\RoleController@bibliotecaIndex')->name('browse.grados.biblioteca.index');
  Route::get('/grados/biblioteca/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@bibliotecaSearchTable')
        ->name('browse.grados.biblioteca.search-table');
  Route::post('/grados/biblioteca/index/{sistema}/pospro', 'Browse\Grados\RoleController@bibliotecaPosproIndex')->name('browse.grados.biblioteca.pospro');
  /*Route::post('/grados/biblioteca/postulante/{sistema}/estado', 'Browse\Grados\RoleController@estado')
        ->name('browse.grados.biblioteca.estado');*/

  /*
  * Egresados
  */
  Route::get('/grados/egresados/index/{sistema}', 'Browse\Grados\RoleController@egresadosIndex')->name('browse.grados.egresados.index');
  Route::get('/grados/egresados/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@egresadosSearchTable')
        ->name('browse.grados.egresados.search-table');
  Route::post('/grados/egresados/index/{sistema}/pospro', 'Browse\Grados\RoleController@egresadosPosproIndex')->name('browse.grados.egresados.pospro');
  /*Route::post('/grados/egresados/postulante/{sistema}/estado', 'Browse\Grados\RoleController@estado')
        ->name('browse.grados.egresados.estado');*/

  /*
  * Director Programa
  */
  Route::get('/grados/director/baseindex/{sistema}', 'Browse\Grados\RoleController@directorBaseIndex')->name('browse.grados.director.baseindex');
  /*
  * Pospro = postulacion programa
  */
  Route::post('/grados/director/index/{sistema}', 'Browse\Grados\RoleController@directorIndex')->name('browse.grados.director.pospro');
  Route::get('/grados/director/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@directorSearchTable')
        ->name('browse.grados.dirPrograma.search-table');

  /*
  * Reaca = Registro Academico
  */
  Route::get('/grados/reaca/index/{sistema}', 'Browse\Grados\RoleController@reacaIndex')
        ->name('browse.grados.reaca.index');
  Route::get('/grados/reaca/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@reacaSearchTable')
        ->name('browse.grados.reaca.search-table');
  Route::post('/grados/reaca/index/{sistema}/pospro', 'Browse\Grados\RoleController@reacaPosproIndex')->name('browse.grados.reaca.pospro');

  /*
  * Cartera

    Carfin = Cartera financiero
  */

  Route::get('/grados/cartera/index/financiero/{sistema}', 'Browse\Grados\RoleController@carFinanIndex')
        ->name('browse.grados.cartera.indexFinan');
  Route::get('/grados/cartera/postulante/{sistema}/{postulante}', 'Browse\Grados\RoleController@carfinanSearchTable')
        ->name('browse.grados.carfin.search-table');

  /*
  * General
  */
  Route::post('/grados/postulacion/postulante/{sistema}/estado', 'Browse\Grados\RoleController@estado')
        ->name('browse.grados.postulacion.estado');
//end Browse prefix
});


/*
* PREFIX: ESTUDIANTES
*
*/
Route::group(['prefix' => 'estudiantes'], function () {
  //login and password reset
  Route::get('/', 'EstudianteAuth\LoginController@showLoginForm');
  Route::get('/login', 'EstudianteAuth\LoginController@showLoginForm');
  Route::post('/login', 'EstudianteAuth\LoginController@login')->name('estudiantes.login');
  Route::post('/logout', 'EstudianteAuth\LoginController@logout');

  Route::get('/register', 'EstudianteAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'EstudianteAuth\RegisterController@register');

  Route::post('/password/email', 'EstudianteAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'EstudianteAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'EstudianteAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'EstudianteAuth\ResetPasswordController@showResetForm');

  //home
  Route::get('/home', 'EstudianteController@index')->name('estudiantes.home');

  //contacto
  Route::get('/contacto', 'EstudianteController@contact')->name('estudiantes.contact');

  //perfil
  Route::get('/perfil', 'EstudianteController@profile')->name('estudiantes.profile');

  //certificados routes
  Route::get('/certificados/{sistema}', 'EstudianteController@certificados')->name('estudiantes.certificados');
  Route::get('/certificados/{sistema}/create', 'Estudiantes\Certificados\SolicitudesController@create')->name('estudiantes.certificados.solicitudes.create');
  Route::post('/certificados/{sistema}/store', 'Estudiantes\Certificados\SolicitudesController@store')->name('estudiantes.certificados.solicitudes.store');

  Route::get('/certificados/{sistema}/search', 'Estudiantes\Certificados\SolicitudesController@search')->name('estudiantes.certificados.solicitudes.search');

  //let download
  Route::get('/certificados/solicitudes/download/receipt/{sistema}/{solicitud}', function ($sistema, SolicitudCertificado $solicitud) {
    $fichero = 'files/certificados/comprobantes/'.$solicitud->solicitud_comprobante_liquidacion;
    return Response::download($fichero);
  });

  Route::get('/certificados/solicitudes/download/photo/{sistema}/{solicitud}', function ($sistema, SolicitudCertificado $solicitud) {
    $fichero = 'files/certificados/carnets/'.$solicitud->solicitud_carnet_foto;
    return Response::download($fichero);
  });

  Route::get('/certificados/solicitudes/download/document/{sistema}/{solicitud}', function ($sistema, SolicitudCertificado $solicitud) {
    $fichero = 'files/certificados/carnets/'.$solicitud->solicitud_carnet_documento;
    return Response::download($fichero);
  });

  /*
  * Directorio routes
  *
  */
  Route::get('/directorio/{sistema}/extensiones', 'Estudiantes\Directorio\ExtensionesController@index')->name('estudiantes.directorio');

  /*
  * Cancelaciones routes
  *
  */
  Route::get('/cancelaciones/{sistema}', 'EstudianteController@cancelaciones')->name('estudiantes.cancelaciones');

  //create and store routes
  Route::get('/cancelaciones/{sistema}/solicitudes/create', 'Estudiantes\Cancelaciones\SolicitudController@create')->name('estudiantes.cancelaciones.solicitudes.create');
  Route::post('/cancelaciones/{sistema}/solicitudes/store', 'Estudiantes\Cancelaciones\SolicitudController@store')->name('estudiantes.cancelaciones.solicitudes.store');

  //search
  Route::get('/cancelaciones/{sistema}/solicitudes/search', 'Estudiantes\Cancelaciones\SolicitudController@search')->name('estudiantes.cancelaciones.solicitudes.search');

  Route::get('/cancelaciones/{sistema}/solicitudes/show/{solicitud}', 'Estudiantes\Cancelaciones\SolicitudController@show')->name('estudiantes.cancelaciones.solicitudes.show');

  //adjuntos
  Route::get('/cancelaciones/{sistema}/adjunto/download/{adjunto}', 'Estudiantes\Cancelaciones\AdjuntoController@download')->name('estudiantes.cancelaciones.adjuntos.download');

  //respuestas
  Route::get('/cancelaciones/{sistema}/respuestas/download/{respuesta}', 'Estudiantes\Cancelaciones\RespuestaController@download')->name('estudiantes.cancelaciones.respuestas.download');


  //apelaciones

  //create & store
  Route::get('/cancelaciones/{sistema}/solicitudes/{solicitud}/apelaciones/create', 'Estudiantes\Cancelaciones\ApelacionController@create')->name('estudiantes.cancelaciones.apelaciones.create');

  Route::post('/cancelaciones/{sistema}/solicitudes/{solicitud}/apelaciones/store', 'Estudiantes\Cancelaciones\ApelacionController@store')->name('estudiantes.cancelaciones.apelaciones.store');

  //show
  Route::get('/cancelaciones/{sistema}/apelaciones/show/{apelacion}', 'Estudiantes\Cancelaciones\ApelacionController@show')->name('estudiantes.cancelaciones.apelaciones.show');

  //egresados routes
  Route::get('/egresados/{sistema}', 'Estudiantes\Egresados\FormularioController@showMenu')->name('estudiantes.egresados');

  Route::get('/egresados/{sistema}/{pidm}/formulario', 'Estudiantes\Egresados\FormularioController@edit')
        ->name('estudiantes.egresados.formulario');

  Route::get('/egresados/{sistema}/{pidm}/encuesta', 'Estudiantes\Egresados\FormularioController@showSurvey')
        ->name('estudiantes.egresados.encuesta');
        //pdf
  Route::get('/egresados/{pidm}/pdf', 'Estudiantes\Egresados\FormularioController@pdf')->name('estudiantes.egresados.pdf');

  Route::post('/egresados/{sistema}/{key}/update', 'Estudiantes\Egresados\FormularioController@update')->name('estudiantes.egresados.update');
   Route::post('/egresados/{sistema}/{key}/create', 'Estudiantes\Egresados\FormularioController@create')->name('estudiantes.egresados.create');

  /*
  |--------------------------------------------------------------------------
  | Grados
  |--------------------------------------------------------------------------
  */

  Route::get('/grados/{sistema}', 'Estudiantes\Grado\SolicitudGradoController@menu')->name('estudiantes.grados');
  Route::get('/grados/{sistema}/{pidm}/create', 'Estudiantes\Grado\SolicitudGradoController@create')->name('estudiantes.grados.create');
  Route::get('/grados/{sistema}/{pidm}/docs', 'Estudiantes\Grado\SolicitudGradoController@docs')->name('estudiantes.grados.docs');

  Route::post('/grados/postulacion/store/{sistema}/{matriculado}', 'Estudiantes\grado\SolicitudGradoController@store')->name('estudiantes.grados.postulacion.store');

  Route::post('/grados/postulacion/store/{sistema}/{matriculado}/archivos', 'Estudiantes\grado\SolicitudGradoController@storeArchive')->name('estudiantes.grados.archivos.store');
});

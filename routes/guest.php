<?php

Route::get('/guest', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('guest')->user();

    return view('guest.home');
});


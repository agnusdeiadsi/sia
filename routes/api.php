<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Cargar select's
Route::post('/programa/{id}/periodo', 'Browse\Egresados\WebServiceController@loadPeriod');

Route::post('/facultad/{facultad}/programa', 'Browse\Egresados\WebServiceController@loadProgram');

Route::post('/facultad', 'Browse\Egresados\WebServiceController@loadFacultad');

Route::post('/periodos', 'Browse\Egresados\WebServiceController@loadPeriodRange');

Route::post('/{departamento}/departamento', 'Browse\Egresados\WebServiceController@loadCity');

//Graficar laboral

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_situacion', 																									'Browse\Egresados\WebServiceController@getPaint_situacion');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_relacion', 																										'Browse\Egresados\WebServiceController@getPaint_relacion');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_cargo', 'Browse\Egresados\WebServiceController@getPaint_cargo');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_duracion', 																										'Browse\Egresados\WebServiceController@getPaint_duracion');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_contrato', 																										'Browse\Egresados\WebServiceController@getPaint_contrato');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_salario', 																										'Browse\Egresados\WebServiceController@getPaint_salario');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/ocupacion', 																												'Browse\Egresados\WebServiceController@getPaint_ocupacion');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_sector', 																											'Browse\Egresados\WebServiceController@getPaint_sector');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_vinculacion', 																									'Browse\Egresados\WebServiceController@getPaint_vinculacion');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_area', 'Browse\Egresados\WebServiceController@getPaint_area');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/laboral_tipoEmpresa', 																										'Browse\Egresados\WebServiceController@getPaint_tipoEmpresa');

//Graficar emprendimiento

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/business_city', 																											'Browse\Egresados\WebServiceController@getPaint_business_city');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/business_size', 																											'Browse\Egresados\WebServiceController@getPaint_business_size');

Route::post('/graphics/{program}/{period}/{since}/{until}/{year_graduate}/business_sector', 																										'Browse\Egresados\WebServiceController@getPaint_business_sector');

//Reconocer el nombre

Route::post('/get/{laboral}/name', 'Browse\Egresados\WebServiceController@name_laboral');

Route::post('/get/name', 'Browse\Egresados\WebServiceController@name_laboral');

Route::post('/get/duracion', 'Browse\Egresados\WebServiceController@name_duracion');

Route::post('/get/cargo', 'Browse\Egresados\WebServiceController@name_cargo');

Route::post('/get/contrato', 'Browse\Egresados\WebServiceController@name_contrato');

Route::post('/get/sector', 'Browse\Egresados\WebServiceController@name_sector');

Route::post('/get/salario', 'Browse\Egresados\WebServiceController@name_salario');

Route::post('/get/relacion', 'Browse\Egresados\WebServiceController@name_relacion');

Route::post('/get/vinculacion', 'Browse\Egresados\WebServiceController@name_vinculacion');

Route::post('/get/ocupacion', 'Browse\Egresados\WebServiceController@name_ocupacion');

Route::post('/get/areaPertenece', 'Browse\Egresados\WebServiceController@name_area');

Route::post('/get/tipoEmpresa', 'Browse\Egresados\WebServiceController@name_tipoEmpresa');

Route::post('/get/ciudad', 'Browse\Egresados\WebServiceController@name_ciudad');

Route::post('/get/tamanoEmpresa', 'Browse\Egresados\WebServiceController@name_tamanoEmpresa');

Route::post('/get/merito', 'Browse\Egresados\WebServiceController@name_merito');

Route::post('/program/{program}/nameProgram', 'Browse\Egresados\WebServiceController@nameProgram');


//Estudiates-Egresados

Route::post('estudiantes/{program}/convenio', 'Browse\Egresados\WebServiceController@getConvenio');

/*
*
* Estudiantes-grados
* 
*/

Route::post('estudiantes/{matriculado}/{programa}/solicitud', 
												'Estudiantes\Grado\ServiceController@getSolicitud');

/*
*
*	Grados
*
*/

Route::post('grados/programaGeneral', 'Browse\Grados\serviceController@loadProgramGeneral');
Route::post('grados/{programa}/programaDirector', 'Browse\Grados\serviceController@loadProgramDirector');

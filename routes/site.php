<?php

Route::get('/site', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('site')->user();

    //dd($users);

    return view('site.home');
});

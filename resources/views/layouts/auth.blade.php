<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />

    <!-- w3-css -->
    <link rel="stylesheet" href="{{ asset('w3css/w3.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/w3-theme-blue-grey.css') }}">
    <link rel='stylesheet' href="{{ asset('w3css/css.css?family=Open+Sans') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        /* my style */
        @media (min-width: 1400px){
            body, .navbar{
            background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
            background-size: cover;
            }

            .navbar{
                text-decoration: none;
            }

            .navbar li:hover{
                color: inherit;
            }

            .logo{
                width: 400px;
            }

            .content{
                margin-top: 3em;
            }
        }

        @media (max-width: 1400px){
            body{
            background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
            background-size: cover;
            }

            .navbar-smallscreen{
              background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
            }

            .logo{
                width: 250px;
            }

            .content{
                margin-top: 3em;
            }
        }

        @media (max-width: 320px){
            body{
            background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
            background-size: cover;
            }

            .navbar-smallscreen{
              background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
            }

            .navbar{
              background-color: red;
            }

            .logo{
                width: 180px;
            }

            .content{
                margin-top: 3em;
            }
        }
        .logo-navbar{
            width: 10%;
            padding: 0;
            margin: 0;
        }
    </style>
</head>
<body>
    <div id="app" class="w3-center flex-center position-ref full-height">
        @if (Auth::guest())
            <div class="content w3-animate-opacity">
                <div class="title m-b-md">
                    <img src="{{ asset('images/sia.png') }}" class="w3-image logo" alt="logo">
                </div>

                {{--muestra el formulario para el inicio de sesion--}}
                @yield('content')
                
                <div class="w3-padding" style="position: absolute; bottom: 0; left: 0; right: 0;">
                    <p style="margin: auto 0; text-align: center;"><b>Fundación Universitaria Católica Lumen Gentium</b><br>
                    Sistema Integrado de Aplicaciones - SIA<br>
                    {{ date('Y') }}</p>
                </div>
            </div>
        @endif
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

@extends('estudiantes.layout.default')

@section('title-estudiantes')
  {{$sistema->sistema_nombre}} | Formulario
@endsection

@section('content-estudiantes')

	<style type="text/css">
		
		.label-file
		{

			background-color: #002e45;
			color: #fff;
			text-align: center;
			display: block;
			border-radius: 4px;
			padding-bottom: 40px;
			padding-top: 40px;	
		
		}

		.label-teal
		{

			background-color: teal;
			color: #fff;
			text-align: center;
			display: block;
			border-radius: 4px;
			padding-bottom: 40px;
			padding-top: 40px;	
		
		}

		.label-file:hover{

			background-color: #EDA33A !important;
		}

		/*#label-file:hover
		{
			background-color: grey;
			color: black;
		}*/

		#file-cedula, 
		#file-sustentacion, 
		#file-actaBachiller, 
		#file-pruebasSaber, 
		#file-certificadoEncuesta, 
		#file-actaTecTec
		{
			
			display: none;
		
		}

	</style>
	
	@php 
		
		/*$Depar_expedicion = App\Ciudad::find($matriculado->identificaciones->first()->identificacion_lugar_expedicion);
		$Depar_ubicacion = App\Ciudad::find($matriculado->ciudad_residencia);

		$solicitud = App\Model\solicitudes\Solicitud::where([
					'matriculado_pidm' => $matriculado->matriculado_pidm,
					'tipoSolicitud_id' => 1,
					'programa_id' => $programa->programa_id])//Solicitud grado
					->orderBy('created_at', 'DESC')->first();

		$academico = null;
		$_periodo = null;
		$año = null;
		$generic_ = '/sia/public/storage/';
		$foto = 'default-profile.jpg';
		$ruta = $generic_.$foto;

		$documento_data = App\Identificacion::where('matriculado_pidm', $matriculado->matriculado_pidm)
											->orderBy('created_at', 'DESC')->first();

		if ($solicitud != null) 
		{

			$academico = App\Academico::where(['matriculado_pidm' => $matriculado->matriculado_pidm,
															'programa_id' => $programa->programa_id])
										->orderBy('created_at', 'DESC');

			$finalizacionAcademico = $academico->first()->academico_fecha_finalizacion;	

			$periodo = explode('-', $finalizacionAcademico);

			$año = $periodo[0];
			$_periodo = $periodo[1];

			$student_photos = App\Model\Solicitudes\SolicitudArchivos::where(['solicitud_id' => $solicitud->solicitud_id,
																		'solicitudArchivos_ruta' => 
																		'/'.$documento_data->identificacion_numero.'-'.$solicitud->solicitud_id.'/perfil'])
																->orderBy('created_at', 'DESC')->get();

			if($student_photos->count() != 0)
			{
			
				$student_photos = $student_photos->first()->solicitudArchivos_nombre
									.'.'.
								  $student_photos->first()->solicitudArchivos_tipo;

				$ruta = $generic_.$documento_data->identificacion_numero.
														'-'.$solicitud->solicitud_id."/perfil/".$student_photos;

			}

			//dd($solicitud->archivos);

		}
	*/	
		/*$solicitud = App\Model\solicitudes\Solicitud::where([
					'matriculado_pidm' => $matriculado->matriculado_pidm,
					'tipoSolicitud_id' => 1,
					'programa_id' => $programa->programa_id])
					->orderBy('created_at', 'DESC')->first();*/

	@endphp

	{{ csrf_field() }}

	<div class="w3-container"><br>
	  <ol class="breadcrumb breadcrumb-arrow">
	    <li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
	    <li><a href="{{ route('estudiantes.grados', $sistema) }}">Grados</a></li>
	    <li class="active"><span>Formulario postulación a grado</span></li>
	  </ol>
	</div>

	{{--dd($matriculado->identificaciones)--}}
	<div class="container">

		@if($solicitud->count() > 0)
			@php
				$fecha = explode(' ', $solicitud->created_at);
			@endphp
			<div class="w3-raw alert-success" style="height: 80px; border-radius: 4px;">
				<div class="col-sm-12 text-center">
					<label style="margin-top: 15px">
						<h3>
							Su solicitud a grado para el programa academico <i><b>{{$programa->programa_nombre}}</b></i> ha sido creada el {{$fecha[0]}}
						</h3>
					</label>
				</div>
			</div><br>
		@endif

		{{--Alerta de errores--}}
	    @if(count($errors) > 0)
		    <div class="w3-panel w3-pale-red w3-leftbar w3-border-red" style="border-radius: 4px">
		    	<p>
		    		@foreach($errors->all() as $error)
		    			<li>
		    				{{ $error }}
		    			</li>
		    		@endforeach
		    	</p>
		    </div>
	    @endif

    	@yield('content-form')

	</div>

	<div class="loader" hidden=""></div>

	<script type="text/javascript">

		var label;

		$('.label-file').hover(function () {
			$('.label-file').css('cursor', 'pointer')
		/*
			label = $(this).html()

			$( this ).html("")
			$( this ).append( $( "<span class='fa fa-plus' style='border-radius: 4px;padding-bottom: 10px;padding-top: 10px;'></span>" ) )			 

		}, function () {
			
			$( this ).find( $( "i" ) ).removeClass('fa-plus');
			$( this ).html(label)*/

		})

		$('.bpostulacion').click(function () 
		{
			$('#dialog-confirm').show()
		})

		$('#file-cedula').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-cedula').text(files[0].name)

			}

			//$('#label_file_span').text(files[name])
			/*if (files.length <= 6) 
			{

				if (files.length == 0) {

					$('#label_file_span').text("Seleccionar archivos")

				} else if (file.length == 1)	{
					
					$('#label_file_span').text(files.length + " archivo listo para subir")
				
				} else {

					$('#label_file_span').text(files.length + " archivos listos para subir")

				}

			} else {

				swal(

		            '',
		            'Solo se permiten subir 6 archivos simultaneamente',
		            'warning'

	        	)

			}*/
		})

		$('#file-sustentacion').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-sustentacion').text(files[0].name)

			}

		})

		$('#file-actaBachiller').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-actaBachiller').text(files[0].name)

			}

		})

		$('#file-pruebasSaber').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-pruebasSaber').text(files[0].name)

			}

		})

		$('#file-certificadoEncuesta').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-certificadoEncuesta').text(files[0].name)

			}

		})

		$('#file-actaTecTec').on('change', function () 
		{
			var files = $(this)[0].files

			if(files.length != 0)
			{

				$('#label-file-span-actaTecTec').text(files[0].name)

			}

		})

	    $('.form').submit(function () {
            $('.loader').show()
        })

		/*function openTab(evt, cityName) 
		{
		    var i, x, tablinks;
		    x = document.getElementsByClassName("tab");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablink");
		    for (i = 0; i < x.length; i++) {
		       tablinks[i].className = tablinks[i].className.replace(" w3-border-green", "");
		    }
		    document.getElementById(cityName).style.display = "block";
		    evt.currentTarget.firstElementChild.className += " w3-border-green";
		}*/

		$('#departamento_expedicion').change(function () 
		{

		    $.post(url+'/public/api/'+$(this).val()+'/departamento', function (data) {
		      
		      var html_select = ''
		      var html_cb = ''

		      for (var i = 0; i < data.length; i++) 
		      {

		        html_select += '<option value="'+data[i].ciudad_id+'">'+data[i].ciudad_nombre+'</option>'
		      
		      }

		      $('#ciudad_expedicion').html(html_select)

		    })

		});

		$('#departamento_ubicacion').change(function () 
		{

		    $.post(url+'/public/api/'+$(this).val()+'/departamento', function (data) {
		      
		      var html_select = ''
		      var html_cb = ''

		      for (var i = 0; i < data.length; i++) 
		      {

		        html_select += '<option value="'+data[i].ciudad_id+'">'+data[i].ciudad_nombre+'</option>'
		      
		      }

		      $('#ciudad_ubicacion').html(html_select)

		    })

		});

	</script>

@endsection
@extends('estudiantes.grados.postulacion.base')

@section('content-form')
	
	<div class="w3-row w3-bar w3-white">
    	<a href="{{ route('estudiantes.'.$sistema->sistema_nombre_corto.'.create', 
        			[$sistema, 
        				Auth::guard('estudiante')->user()->matriculado_pidm]) }}" class="w3-text-grey">
    		<div class="w3-col s6 tablink w3-bottombar w3-border-{{$sistema->sistema_colorclass}} w3-hover-light-grey w3-padding">Datos base</div>
    	</a>
        
        <a href="{{ route('estudiantes.'.$sistema->sistema_nombre_corto.'.docs', 
        			[$sistema, 
        				Auth::guard('estudiante')->user()->matriculado_pidm]) }}" class="w3-text-grey">
            <div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding">Archivos</div>
        </a>
	</div>
	
	@php 
		
		$Depar_expedicion = App\Ciudad::find($matriculado->identificaciones->first()->identificacion_lugar_expedicion);
		$Depar_ubicacion = App\Ciudad::find($matriculado->ciudad_residencia);

		$solicitud = App\Model\solicitudes\Solicitud::where([
					'matriculado_pidm' => $matriculado->matriculado_pidm,
					'tipoSolicitud_id' => 1,
					'programa_id' => $programa->programa_id])//Solicitud grado
					->orderBy('created_at', 'DESC')->first();

		$academico = null;
		$_periodo = null;
		$año = null;
		$generic_ = '/sia/public/storage/';
		$foto = 'default-profile.jpg';
		$ruta = $generic_.$foto;

		$documento_data = App\Identificacion::where('matriculado_pidm', $matriculado->matriculado_pidm)
											->orderBy('created_at', 'DESC')->first();

		if ($solicitud != null) 
		{

			$academico = App\Academico::where(['matriculado_pidm' => $matriculado->matriculado_pidm,
															'programa_id' => $programa->programa_id])
										->orderBy('created_at', 'DESC');

			$finalizacionAcademico = $academico->first()->academico_fecha_finalizacion;	

			$periodo = explode('-', $finalizacionAcademico);

			$año = $periodo[0];
			$_periodo = $periodo[1];

			$student_photos = App\Model\Solicitudes\SolicitudArchivos::where(['solicitud_id' => $solicitud->solicitud_id,
																		'solicitudArchivos_ruta' => 
																		'/'.$documento_data->identificacion_numero.'-'.$solicitud->solicitud_id.'/perfil'])
																->orderBy('created_at', 'DESC')->get();

			if($student_photos->count() != 0)
			{
			
				$student_photos = $student_photos->first()->solicitudArchivos_nombre
									.'.'.
								  $student_photos->first()->solicitudArchivos_tipo;

				$ruta = $generic_.$documento_data->identificacion_numero.
														'-'.$solicitud->solicitud_id."/perfil/".$student_photos;

			}

			//dd($solicitud->archivos);

		}
		

	@endphp

	<div id="datosBase" class="w3-container tab">
    	<br>
		<div class="w3-row" style="background-color: #d7dde5; border-radius: 4px;">
			<div class="col-sm-12">
				<h4 class="w3-text-black 
							w3-panel 
							{{--w3-pale-{{$sistema->sistema_colorclass}} --}}
							w3-leftbar 
							w3-border-{{$sistema->sistema_colorclass}}">
							<b>1. Informacion personal del estudiante</b></h4>
			</div>
		</div>
		<br>
		{!!Form::open(['route' => ['estudiantes.grados.postulacion.store', 
									$sistema, 
									$matriculado], 
									'method' => 'post',
									'enctype' => 'multipart/form-data',
									'class' => 'form'])!!}
			
			<div class="w3-row">
				<div class="col-12 w3-center">
					{{--<label class="w3-text-black">
						Foto
						<span style="color: red">
							*
						</span>
					</label>--}}
					<img width="200px" 
					class="img-rounded"
					src="{{$ruta}}">

					{{--<a href="{{$ruta}}" target="_black">IMG</a>--}}
					{!!Form::hidden('solicitud', $solicitud != null ? $solicitud->solicitud_id : '' )!!}
					{{--<img src="/sia/storage/app/public/no_foto.png">	--}}
				</div>
			</div>
			<br>
			<div class="row">
				<center>
				{!!Form::file('foto')!!}
				</center>
			</div>
			<br>

			<div class="w3-row">

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					
					<label class="w3-text-black">
						Primer apellido
						<span style="color: red">
							*
						</span>
					</label>

					{!!Form::text('primer_apellido', 
									$matriculado->matriculado_primer_apellido, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
												w3-text-grey 
												input-text
												input--only-text',
									'required'])!!}
				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Segundo apellido
					</label>

					{!!Form::text('segundo_apellido', 
									$matriculado->matriculado_segundo_apellido, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
												w3-text-grey 
												input-text
												input--only-text'])!!}
				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Primer nombre
						<span style="color: red">
							*
						</span>
					</label>

					{!!Form::text('primer_nombre', 
									$matriculado->matriculado_primer_nombre,
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
												w3-text-grey 
												input-text
												input--only-text',
									'required'])!!}
				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Segundo nombre
					</label>

					{!!Form::text('segundo_nombre', 
									$matriculado->matriculado_segundo_nombre,
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'
												w3-text-grey 
												input-text
												input--only-text'])!!}
				</div>

			</div>

			<br>
			<div class="w3-row">

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Dirección
						<span style="color: red">
							*
						</span>
					</label>

					{!!Form::text('direccion', 
									$matriculado->matriculado_direccion, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
												w3-text-grey 
												input-text',
									'required'])!!}
				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Barrio
						<span style="color: red">
							*
						</span>
					</label>

					{!!Form::text('barrio', 
									$matriculado->matriculado_barrio, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
												w3-text-grey 
												input-text
												input--only-text',
									'required'])!!}
				</div>

				@if($matriculado->ciudad_residencia == null)
					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">

						<label class="w3-text-black">Departamento
							<span style="color: red">*</span>
						</label>
						
						{!! Form::select('departamento', 
										$departamento->pluck('departamento_nombre', 'departamento_id'), 
										null, 
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 
										'placeholder' => 'Seleccione', 
										'id' => 'departamento_ubicacion',
										'required']) !!}

					</div>

					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Ciudad
							<span style="color: red">*</span>
						</label>
						
						{!!Form::select('ciudad', 
										[],
										null,
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
										'required',
										'id' => 'ciudad_ubicacion'])!!}
					</div>

				@else
							
					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Departamento
							<span style="color: red">*</span>
						</label>
					
						{!! Form::select('departamento', 
										$departamento->pluck('departamento_nombre', 'departamento_id'), 
										$Depar_ubicacion->departamento_id, 
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 
										'placeholder' => 'Seleccione', 
										'id' => 'departamento_ubicacion',
										'required']) !!}

					</div>

					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Ciudad
							<span style="color: red">*</span>
						</label>

						{!!Form::select('ciudad', 
										$ciudad->pluck('ciudad_nombre', 'ciudad_id'),
										$matriculado->ciudad_residencia,
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
										'required',
										'id' => 'ciudad_ubicacion'])!!}
					</div>

				@endif
				

			</div>

			<br>
			<div class="w3-row">

				<div class="col-sm-3">
					<label class="w3-text-black">
						Telefono celular
						<span style="color: red">
							*
						</span>			
					</label>
					{!!Form::text('celular', 
									$matriculado->matriculado_celular, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text input-number',
									'required'])!!}
				</div>

				<div class="col-sm-3">
					<label class="w3-text-black">
						Telefono fijo		
					</label>
					{!!Form::text('fijo', 
									$matriculado->matriculado_telefono, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text input-number'])!!}
				</div>

				<div class="col-sm-6">
					<label class="w3-text-black">
						Correo electronico
						<span style="color: red">
							*
						</span>			
					</label>
					{!!Form::email('email', 
									$matriculado->matriculado_email, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text ',
									'required'])!!}
				</div>
			
			</div>

			<hr>
			<div class="w3-row">
				
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">	
					<label class="w3-text-black">
						Tipo documento
						<span style="color: red">
							*
						</span>
					</label>

					<br>
					
					{!!Form::select('tipo_documento', 
									['C.C' => 'C.C', 'T.I' => 'T.I', 'C.E' => 'C.E'],
									$documento_data->identificacion_tipo,
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'required',
									'id' => 'ciudad_ubicacion'])!!}
					{{--<input type="radio" 
							name="tipo_documento" 
							value="CC" 
							{{ $documento_data->identificacion_tipo == "CC" ? 'checked' : '' }} 
							class="w3-radio"
							required>

					<label class="w3-text-black" style="margin-right: 10px">
						C.C
					</label>

					<input type="radio" 
							name="tipo_documento"
							value="TI" 
							{{ $documento_data->identificacion_tipo == "TI" ? 'checked' : '' }}
							class="w3-radio">

					<label class="w3-text-black" style="margin-right: 10px">
						T.I
					</label>

					<input type="radio" 
							name="tipo_documento" 
							value="CE" 
							{{ $documento_data->identificacion_tipo == "CE" ? 'checked' : '' }}
							class="w3-radio">

					<label class="w3-text-black" style="margin-right: 10px">
						C.E
					</label>--}}
				
				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-black">
						Número de documento
						{{--<span style="color: red">
							*
						</span>--}}
					</label>
					
					{!!Form::text('numero_documento', 
									$matriculado->identificaciones->first()->identificacion_numero,
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'readonly'])!!}
				</div>

				@if($matriculado->identificaciones->first()->identificacion_lugar_expedicion == null)
							
					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Departamento de expedición
							<span style="color: red">*</span>
						</label>
						{!! Form::select('depar_lugar_expedicion', 
										$departamento->pluck('departamento_nombre', 'departamento_id'), 
										null, 
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 
										'placeholder' => 'Seleccione', 
										'id' => 'departamento_expedicion',
										'required']) !!}

					</div>

					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Ciudad de expedición
							<span style="color: red">*</span>
						</label>
						{!!Form::select('lugar_expedicion', 
										[],
										null,
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
										'required',
										'id' => 'ciudad_expedicion',
										'placeholder' => 'Seleccione'])!!}
					</div>

				@else
							
					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<label class="w3-text-black">Departamento de expedición
							<span style="color: red">*</span>
						</label>
						{!! Form::select('depar_lugar_expedicion', 
										$departamento->pluck('departamento_nombre', 'departamento_id'), 
										$Depar_expedicion->departamento_id, 
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 
										'placeholder' => 'Seleccione', 
										'id' => 'departamento_expedicion',
										'required']) !!}

					</div>

					<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3"> 
						<label class="w3-text-black">Ciudad de expedición
							<span style="color: red">*</span>
						</label>
						{!!Form::select('lugar_expedicion', 
										$ciudad->pluck('ciudad_nombre', 'ciudad_id'),
										$matriculado->identificaciones->first()->lugar_expedicion_id,
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
										'required',
										'id' => 'ciudad_expedicion'])!!}
					</div>

				@endif

			</div>

			<br>
			<div class="w3-row" style="background-color: #d7dde5; border-radius: 4px;">
				<div class="col-sm-12">
					<h4 class="w3-text-black 
								w3-panel 
								{{--w3-pale-{{$sistema->sistema_colorclass}}--}}
								w3-leftbar 
								w3-border-{{$sistema->sistema_colorclass}}"><b>2. Informacion academica</b></h4>
				</div>
			</div>
			<br>

			<div class="w3-row">

				<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
					<label class="w3-text-black">
						Programa
					</label>

					{!!Form::text('programa', 
									$programa->programa_nombre, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'readonly'])!!}

					{!!Form::hidden('programa_id', 
									$programa->programa_id, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'readonly'])!!}
				</div>

				<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
					<label class="w3-text-black">
						Facultad
					</label>

					{!!Form::text('facultad', 
									$programa->facultad->facultad_nombre, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'readonly'])!!}
				</div>

			</div>

			<br>
			<div class="w3-row">


				<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">

					<label class="w3-text-black">
						Codigo ID			
					</label>

					{!!Form::text('codigo_id', 
									$Bmatriculado->pers_id, 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'readonly'])!!}
				</div>

				<div class="col-2 col-sm-6 col-md-6 col-lg-2 col-xl-2">
					<label class="w3-text-black">
						Sede finalizacion
						<span style="color: red">
							*
						</span>
					</label>

					{!!Form::select('sede',
									$sede->pluck('sede_nombre', 'sede_id'), 
									$academico != null ? $academico->first()->sede_id : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									'placeholder' => 'Seleccione',
									'required'])!!}
				</div>

				<div class="col-4 col-sm-6 col-md-6 col-lg-4 col-xl-4">

					<div class="row">
						<center>
							<label class="w3-text-black">
								Periodo de finalización
								<span style="color: red">
									*
								</span>
							</label>
						</center>
					</div>

					<div class="row">
						<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
							
							{!!Form::text('finalizacionEstudio', 
											$año, 
											['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text input-number',
											'placeholder' => 'Año',
											'required'])!!}
						</div>

						<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
							{!!Form::select('finalizacionPeriodo',
										['1' => '1', '2' => '2'], 
										$_periodo, 
										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
										'placeholder' => 'Periodo',
										'required'])!!}
						</div>
					</div>

				</div>

			</div>

			<br>

			<div id="dialog-confirm" class="w3-modal">
			    <div class="w3-modal-content w3-animate-zoom">
			      <div class="w3-container">
			        <span onclick="document.getElementById('dialog-confirm').style.display='none'" class="w3-button w3-display-topright">&times;</span>
			        <br>
			        <p>
			        	Acepta terminos y condiciones
			        </p>
			        <hr>
			        <center>
			            <p>
			                <b>
			                    ¿ACEPTA?
			                </b>
			            </p>
			            <button name="action" class="w3-btn w3-{{ $sistema->sistema_colorclass }}" onclick="document.getElementById('dialog-confirm').style.display='none'">
			                Si
			            </button>
			            <a class="w3-btn" onclick="document.getElementById('dialog-confirm').style.display='none'" type="reset" style="background-color: #DCAC34; color: white;">
			                NO
			            </a>
			        </center>
			        <hr>
			      </div>
			    </div>
		    </div> 

		    @if($solicitud != null)
		    	@if($solicitud->programa_id == $programa->programa_id)
				    <div class="w3-row">
						<div class="col-sm-12 w3-center">
							<button type="submit" class="w3-btn w3-text-white w3-{{$sistema->sistema_colorclass}}">
								<span class="fa fa-check fa-fw">
									
								</span>
								Enviar
							</button>
						</div>	
					</div>
				@endif
			@endif
		{!!Form::close()!!}

		@if($solicitud == null)
			<div class="w3-row">
				<div class="col-sm-12 w3-center">
					<button type="submit" class="w3-btn w3-text-white bpostulacion w3-{{$sistema->sistema_colorclass}}">
						<span class="fa fa-check fa-fw">
							
						</span>
						Crear Solicitud
					</button>
				</div>	
			</div>
		@else
			@if($solicitud->programa_id != $programa->programa_id)
				<div class="w3-row">
					<div class="col-sm-12 w3-center">
						<button type="submit" class="w3-btn w3-text-white bpostulacion w3-{{$sistema->sistema_colorclass}}">
							<span class="fa fa-check fa-fw">
								
							</span>
							Crear Solicitud
						</button>
					</div>	
				</div>
			@endif
		@endif

	</div>

@endsection
@extends('estudiantes.grados.postulacion.base')

@section('content-form')
	
	<div class="w3-row w3-bar w3-white">
    	<a href="{{ route('estudiantes.'.$sistema->sistema_nombre_corto.'.create', 
        			[$sistema, 
        				Auth::guard('estudiante')->user()->matriculado_pidm]) }}" class="w3-text-grey">
    		<div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding">Datos base</div>
    	</a>
        
        <a href="{{ route('estudiantes.'.$sistema->sistema_nombre_corto.'.docs', 
        			[$sistema, 
        				Auth::guard('estudiante')->user()->matriculado_pidm]) }}" class="w3-text-grey">
            <div class="w3-col s6 tablink w3-bottombar w3-border-{{$sistema->sistema_colorclass}} w3-hover-light-grey w3-padding">Archivos</div>
        </a>
	</div>

	@php
		$solicitud = App\Model\solicitudes\Solicitud::where([
					'matriculado_pidm' => $matriculado->matriculado_pidm,
					'tipoSolicitud_id' => 1,
					'programa_id' => $programa->programa_id])
					->orderBy('created_at', 'DESC')->first();

	@endphp

	<div id="archivos" class="w3-container tab">
		<br>
		{!!Form::open(['route' => ['estudiantes.grados.archivos.store', 
									$sistema, 
									$matriculado], 
									'method' => 'post',
									'files' => true,
									'enctype' => 'multipart/form-data',
									'class' => 'form'])!!}
		

			<div class="w3-row" style="background-color: #d7dde5; border-radius: 4px;">
				<div class="col-sm-12">
					<h4 class="w3-text-black 
								w3-panel 
								{{--w3-pale-{{$sistema->sistema_colorclass}}--}}
								w3-leftbar 
								w3-border-{{$sistema->sistema_colorclass}}">
								<b>3. Documuentos solicitados</b></h4>
				</div>
			</div>
			<br>
			<div class="w3-row">
				<div class="col-sm-12">
					<label>
						Los formatos permitidos: JPEG, PDF, PNG		
					</label>
				</div>
			</div>
			<br>
			<div class="w3-row">
				<div class="col-md-8">
					{{--<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<label class="w3-text-black">
							Por favor adjuntar los siguientes documentos:
						</label><br>
						1. Cedula <br>
						2. Certificado de presentación pruebas Saber pro (ECAES) <br>
						3. Acta de sustentacion de trabajo de grado <br>
						4. Acta de grado de bachiller <br>
						5. Resultado de la prueba saber 11 (ICFES) <br>
						6. Encuesta de graduados del Observatorio Laboral del MEN <br>
						7. Encuesta momento de grado <br>
						8. Acta de grado de Tecnico o Tecnologo (Siclos propedeuticos) <br>
					</div>
					<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<center>
							<img src="/sia/public/images/folder-upload.png">
						</center>
						<br>
						<label for="file" id="label-file" class="w3-btn w3-{{$sistema->sistema_colorclass}}">
							<i class="fa fa-upload fa-fw"></i>
							<span id="label_file_span">Seleccionar archivos</span>
						</label>
						<a class="w3-bar-item" data-toggle="tooltip" title="Adjunta los documentos solicitados">
	                        <span class="fa fa-question-circle fa-fw">
	                        </span>
            			</a>
						{!!Form::file('archivo[]', 
									['multiple', 
									'class' => 'form-control-file',
									'id' => 'file'])!!}
					</div>--}}

					<div class="w3-row">
						
						<div class="col-lg-4">
							<label for="file-cedula" id="cedula" class="label-file w3-grey">
								<i class="fa fa-upload fa-fw"></i>
								<br>
								<span id="label-file-span-cedula">Cedula</span>
								<br>
							</label>
							{!!Form::file('cedula'/*, ['required']*/, 
											['id' => 'file-cedula', 
											'class' => 'file'])!!}
						</div>

					
						<div class="col-lg-4">
							<label for="file-sustentacion" id="sustentacion" class="label-file w3-grey">
								<i class="fa fa-upload fa-fw"></i>
								<br>
									<span id="label-file-span-sustentacion">
										Acta de sustentación 
									</span>
								<br>
							</label> 
							{!!Form::file('sustencion'/*, ['required']*/, 
											['id' => 'file-sustentacion', 
											'class' => 'file'])!!}
						</div>
					

					
						<div class="col-lg-4">
							<label for="file-actaBachiller" id="actaBachiller" class="label-file w3-grey">
								<i class="fa fa-upload fa-fw"></i><br>
									<span id="label-file-span-actaBachiller">
										Acta de grado de bachiller
									</span>
								<br>
							</label>
							{!!Form::file('actaBachiller'/*, ['required']*/, [
											'id' => 'file-actaBachiller', 
											'class' => 'file'])!!}
						</div>
					</div>

					<div class="w3-row">
						<div class="col-lg-4" style="margin-top: 20px">
							<label for="file-pruebasSaber" id="pruebasSaber" class="label-file w3-grey" title="Programas en convenio">
								<i class="fa fa-upload fa-fw"></i><br>
									<span id="label-file-span-pruebasSaber">
										Resultado pruebas saber 11
									</span>
								<a class="w3-bar-item" data-toggle="tooltip">
			                        <span class="fa fa-question-circle fa-fw">
			                        </span>
                    			</a><br>
							</label>
							{!!Form::file('pruebasSaber', 
											['id' => 'file-pruebasSaber', 
											'class' => 'file'])!!}
						</div>
					
						<div class="col-lg-4" style="margin-top: 20px">
							<label for="file-certificadoEncuesta" 
									id="certificadoEncuesta" 
									class="label-file w3-grey">
								<i class="fa fa-upload fa-fw"></i><br>
								<span id="label-file-span-certificadoEncuesta">
									Certificado encuesta momento de grado
								</span> 
								<br>
							</label>
							{!!Form::file('certificadoEncuesta'/*, ['required']*/, 
										['id' => 'file-certificadoEncuesta', 
										'class' => 'file'])!!}
						</div>
											
						<div class="col-lg-4" style="margin-top: 20px">
							<label for="file-actaTecTec" id="actaTecTec" class="label-file w3-grey" title="Programas por ciclos propedeuticos">
								<i class="fa fa-upload fa-fw"></i><br>
								<span id="label-file-span-actaTecTec">
									Acta de grado tecnico o tecnologo
								</span>
								<a class="w3-bar-item" data-toggle="tooltip" >
			                        <span class="fa fa-question-circle fa-fw">
			                        </span>
                    			</a>
							</label>
							{!!Form::file('actaTecTec', 
											['id' => 'file-actaTecTec', 
											'class' => 'file'])!!}
						</div>
					</div>
					<br>
					<div class="w3-row">
						@if($solicitud != null)
							{{--Tiene solicitud de tipo grado--}}
							@if($solicitud->programa_id == $programa->programa_id)
							{{--El programa en banner es igual al programa de la ultima solicitud de grado--}}
								<div class="w3-row">
									<div class="col-sm-2 w3-right">
										<button type="submit" class="w3-btn w3-text-white w3-{{$sistema->sistema_colorclass}}">
											<span class="fa fa-check fa-fw">
												
											</span>
											Enviar
										</button>
									</div>
								</div>
							@else
								{{--El programa en banner es diferente al programa de la ultima solicitud de grado--}}
								<div class="col-sm-2 w3-right">
									<button type="submit" class="w3-btn w3-text-white w3-{{$sistema->sistema_colorclass}}" disabled="">
										<span class="fa fa-check fa-fw">
											
										</span>
										Enviar
									</button>
									<a class="w3-bar-item" data-toggle="tooltip" title="Para adjuntar los archivos solicitados debes diligenciar el formulario de postulacion a grado">
				                        <span class="fa fa-question-circle fa-fw">
				                        </span>
		                			</a>
								</div>
							@endif
						@else
							{{--No tiene solicitudes de tipo grado--}}
							<div class="w3-row">
								<div class="col-sm-2 w3-right">
									<button type="submit" class="w3-btn w3-text-white w3-{{$sistema->sistema_colorclass}}" disabled="">
										<span class="fa fa-check fa-fw">
											
										</span>
										Enviar
									</button>
									<a class="w3-bar-item" data-toggle="tooltip" title="Para adjuntar los archivos solicitados debes diligenciar el formulario de postulacion a grado">
				                        <span class="fa fa-question-circle fa-fw">
				                        </span>
		                			</a>
								</div>
							</div>
						@endif
					</div>

				</div>

				
				@foreach($doc as $documento => $value)
					<div class="col-md-4">
						<div class="col-sm-12 text-center w3-{{$value}}" style="border-radius: 4px; margin-top: 5px">
							<label style="margin-top: 10px">
								<p>
									{{$documento}}
								</p>
							</label>
						</div>
					</div>
				@endforeach

				{{--@if(isset($solicitud) && $solicitud->count() > 0)
					foreach
					<div class="col-md-4">
						<div class="col-sm-12 text-center alert-success" style="border-radius: 4px">
							<label style="margin-top: 15px">
								<p>
									La cedula <br>
									ha sido insertada
								</p>
							</label>
						</div>
					</div>

					<div class="col-md-4">
						<div class="col-sm-12 text-center alert-danger" style="border-radius: 4px">
							<label style="margin-top: 15px">
								<p>
									La cedula no ha sido insertada
								</p>
							</label>
						</div>
					</div>
					
				@endif
				@if($cedula)
					<div class="col-md-8">
						<div class="col-sm-12 text-center alert-success" style="border-radius: 4px">
							<label style="margin-top: 15px">
								<p>
									La cedula ha sido insertada
								</p>
							</label>
						</div>
					</div>
				@endif--}}

			</div>
			<br>

		{!!Form::close()!!}

	</div>

@endsection
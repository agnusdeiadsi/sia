@extends('estudiantes.layout.default')

@section('title-estudiantes')
SIA | Home
@endsection

@section('content-estudiantes')
<style type="text/css">
  #descripcion, #titulo {
    width:95%;
    overflow:hidden;
    white-space:nowrap;
    text-overflow: ellipsis;
  }
</style>
@php
  $pidm = Auth::guard('estudiante')->user()->matriculado_pidm;
  $matriculado = \App\Matriculado::find($pidm);
@endphp
<div class="w3-row w3-hide-small w3-hide-medium">
  <div class="w3-col s12 m12 l12 w3-center">
    <h1><img src="{{ asset('images/servicios-titulo.png') }}" width="100%"></h1>
  </div>
</div>
<br>
<div class="w3-container">
  {{--dd($sistemas)--}}
  @foreach($sistemas->chunk(3) as $sistemas_chunk)
    {{-- menu for large screens --}}
    <div class="w3-row-padding w3-hide-small w3-hide-medium">
      @foreach($sistemas_chunk as $sistema)
        <div class="w3-col s4 w3-hover-opacity">
          <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto, [$sistema, $matriculado])}}">
            <div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
              <div class="w3-col m3 l3 w3-center">
                  <div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-bottom-left-radius: 15px;">
                    <img src="{{ asset('images/icon/'.$sistema->sistema_icon) }}" alt="{{$sistema->sistema_nombre_corto}}" class="w3-image">
                  </div>
              </div>
              <div class="w3-col m9 l9">
                <div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}" style="border-top-right-radius: 15px;">
                  <div class="w3-col m12 l12" style="padding:0px 5px 0px 15px;">
                    <h4 id="titulo"><b>{{$sistema->sistema_nombre}}</b></h4>
                  </div>
                </div>
                <div class="w3-row w3-text-dark-grey">
                  <div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
                    <p style="line-height: 1;" id="descripcion">{{$sistema->sistema_descripcion}}</p>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      @endforeach
    </div>
    <br>
    <!--Menu for small/medium screens -->
    @foreach($sistemas_chunk as $sistema)
      {{-- small --}}
      <div class="w3-row w3-hide-medium w3-hide-large">
        <div class="w3-col m12 w3-hover-opacity">
          <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto, [$sistema, $matriculado])}}">
            <div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
              <div class="w3-col m3 l3 w3-center">
                  <div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-top-right-radius: 15px;">
                    <img src="{{ asset('images/icon/'.$sistema->sistema_icon) }}" alt="{{$sistema->sistema_nombre_corto}}" class="w3-image">
                  </div>
              </div>
              <div class="w3-col m9 l9">
                <div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}">
                  <div class="w3-col m12 l12" style="padding:0px 5px 0px 15px;">
                    <h4><b>{{$sistema->sistema_nombre}}</b></h4>
                  </div>
                </div>
                <div class="w3-row w3-text-dark-grey">
                  <div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
                    <p id="contenido">{{$sistema->sistema_descripcion}}</p>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <br class="w3-hide-medium w3-hide-large">

      {{-- medium --}}
      <div class="w3-row w3-hide-small w3-hide-large">
        <div class="w3-col m12 w3-hover-opacity">
          <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto, [$sistema, $matriculado])}}">
            <div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
              <div class="w3-col m3 l3 w3-center">
                  <div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-bottom-left-radius: 15px;">
                    <img src="{{ asset('images/icon/'.$sistema->sistema_icon) }}" alt="{{$sistema->sistema_nombre_corto}}" class="w3-image">
                  </div>
              </div>
              <div class="w3-col m9 l9">
                <div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}" style="border-top-right-radius: 15px;">
                  <div class="w3-col m12 l12" style="padding:0px 5px 0px 15px;">
                    <h4><b>{{$sistema->sistema_nombre}}</b></h4>
                  </div>
                </div>
                <div class="w3-row w3-text-dark-grey">
                  <div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
                    <p id="contenido">{{$sistema->sistema_descripcion}}</p>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <br class="w3-hide-small w3-hide-large">
    @endforeach
  @endforeach
  <div class="w3-center w3-padding-32">
      <div class="w3-bar">
          {{$sistemas->render()}}
      </div>
  </div>
</div>
@endsection

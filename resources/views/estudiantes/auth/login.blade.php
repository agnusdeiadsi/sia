@extends('estudiantes.layout.auth')

@section('title-estudiantes')
    Estudiantes | Login
@endsection

@section('content-login')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-default">
                {{--<div class="panel-heading">Login</div>--}}
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/estudiantes/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('identificacion_numero') ? ' has-error' : '' }}">

                            <div class="col-md-offset-2 col-md-8">
                                <label for="email" class="control-label w3-left">Código Estudiante</label>
                                <input id="identificacion_numero" type="text" class="w3-input w3-text-grey" name="identificacion_numero" value="{{ old('identificacion_numero') }}" placeholder="Código Estudiante" autofocus>

                                @if ($errors->has('identificacion_numero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('identificacion_numero') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!--muestra mensajes de flash-->
                        @include('flash::message')

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Recordarme
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="w3-btn w3-white w3-border">
                                    <span class="fa fa-sign-in fa-fw"></span> Entrar
                                </button>

                                {{--<a class="btn btn-link" href="{{ url('/guest/password/reset') }}">
                                    Forgot Your Password?
                                </a>--}}
                            </div>
                        </div>
                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/estudiantes/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

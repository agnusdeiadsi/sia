<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title-estudiantes')</title>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
<meta name="theme-color" content="#ffffff">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/cancelaciones.css') }}" rel="stylesheet">

<link href="{{ asset('w3css/w3-v4.css') }}" rel="stylesheet">
<link href="{{ asset('css/egresados.css') }}" rel="stylesheet">
<link href="{{ asset('css/grados.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-camo.css') }}">
<link href="{{ asset('w3css/colors/w3-colors-food.css') }}"" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-highway.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-safety.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-signal.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-vivid.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-2017.css') }}" rel="stylesheet">
<link href="{{ asset('w3css/colors/w3-colors-windows.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/egresados/accordion.css')}}">

<link href="{{ asset('breadcrumbs/breadcrumbs.css') }}" rel="stylesheet">
<link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">

<!-- Scripts -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>

<!-- If using flash()->important() or flash()->overlay(), you'll need to pull in the JS for Twitter Bootstrap. -->
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

{{-- Sweetalert --}}
<link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.min.css') }}">
<script type="text/javascript" src="{{ asset('sweetalert/js/sweetalert-1-1-3.min.js') }}"></script>
</head>
<style>
  body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif;}
  a.w3-bar-item {text-decoration: none; color: inherit;}
  a.w3-bar-item:link {color: inherit;}
  a.w3-btn {text-decoration: none; color: inherit;}
  a.w3-btn:link {color: inherit;}
</style>
<body>
<!-- logo sia -->
<div class="w3-hide-small w3-hide-medium" style="position: absolute; z-index:1;">
    <center>
        <a class="w3-text-white" href="{{url('/estudiantes/home')}}" style="text-decoration: none;">
            <img src="{{ asset('images/sia-superior-home.png') }}" class="w3-image" width="320">
        </a>
    </center>
</div>

<!-- !PAGE CONTENT! -->
<div class="w3-main w3-animate-top">
  <!-- Header for large screens -->
  <header class="w3-hide-small w3-hide-medium">
      <a href="#">
          <img class="w3-right w3-margin w3-hide-large" src="{{asset('images/sia.png')}}" style="width:65px;"/>
      </a>
      <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()">
          <i class="fa fa-bars">
          </i>
      </span>
      <div class="w3-container" style="background-color: #e8e8e6;">
          @php
              /*dd(Auth::guard('estudiante')->user());
              $pidm = Auth::guard('estudiante')->user()->matriculado_pidm;
              $matriculado = \App\Matriculado::find($pidm);
                              
              $auth = explode(' ', $matriculado->matriculado_nombres);

              if(count($auth) == 2)
              {
                  $authname = $auth[1];
                  $authlastname = $auth[0];
              }
              elseif(count($auth) == 3)
              {
                  $authname = $auth[2];
                  $authlastname = $auth[0];
              }
              elseif(count($auth) == 4)
              {
                  $authname = $auth[2];
                  $authlastname = $auth[0];
              }
              */
          @endphp
          <div class="w3-text-2017-navy-peony" style="position: absolute; z-index: 1; width: 100%; margin-top: 30px;">
              <h3 class="w3-right" style="margin-right: 100px;">Bienvenido <b>{{ Auth::guard('estudiante')->user()->persona->pers_nom1 }}</b></h3>
              {{--<h3 class="w3-right" style="margin-right: 100px;">Bienvenido <b>{{$authname}}</b></h3>--}}
          </div>

          <div class="w3-row" style="margin-top: 10px; position: relative; z-index: 2;">
            <div class="w3-col m12 l12 w3-margin-top">
              <div class="w3-dropdown-click w3-hover-none w3-right">
                  <a class="w3-text-2017-navy-peony w3-hover-opacity" href="javascript:void(0);" onclick="myFunction('largeiconmenu')">
                      <span class="fa fa-user-circle fa-fw fa-3x">
                      </span>
                  </a>
                  <div class="w3-dropdown-content w3-bar-block w3-border w3-2017-navy-peony" id="largeiconmenu" style="right: 0; z-index: 4; width: 240px;">
                      <a class="w3-bar-item w3-button" href="{{ route('estudiantes.profile') }}" onclick="myFunction('largeiconmenu')">
                          <i class="fa fa-user fa-fw w3-margin-right">
                          </i>
                          Perfil
                      </a>
                      @php
                        $directorio = \App\Sistema::where('sistema_nombre_corto', '=', 'directorio')->first();
                      @endphp
                      <a class="w3-bar-item w3-button" href="{{route('estudiantes.'.$directorio->sistema_nombre_corto, $directorio)}}" onclick="myFunction('largeiconmenu')">
                          <i class="fa fa-address-book fa-fw w3-margin-right">
                          </i>
                          Directorio
                      </a>
                      {{--@if(Auth::user()->rol_sia == 'Administrador')
                      <a class="w3-bar-item w3-button" href="#contact" onclick="myFunction('largeiconmenu')">
                          <i class="fa fa-envelope fa-fw w3-margin-right">
                          </i>
                          Contacto
                      </a>
                      @endif--}}
                      <a class="w3-bar-item w3-button w3-padding" href="{{ url('/estudiantes/logout') }}" onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                          <i class="fa fa-sign-out fa-fw w3-margin-right">
                          </i>
                          Cerrar Sesión
                      </a>
                      <form action="{{ url('/estudiantes/logout') }}" id="logout-form" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </div>
              </div>
              <!-- scope to search for services -->
              {{-- {!! Form::open(['route' => ['browse.home'], 'method' => 'get']) !!}
              <button class="w3-bar-item w3-button w3-medium w3-2017-navy-peony w3-mobile w3-right" type="submit">
                  <span class="fa fa-search">
                  </span>
              </button>
              <input class="w3-bar-item w3-input w3-medium w3-white w3-mobile w3-right w3-col m2 l2 w3-border-2017-navy-peony" maxlength="20" name="buscar" placeholder="Buscar" required="" type="text"/>
                  {!! Form::close() !!} --}}
            </div>
          </div>
          <br>
          <br>
      </div>
  </header>

  <!-- Header for small/medium screens-->
  <header class="w3-hide-large">
    <div class="w3-row-padding w3-2017-navy-peony">
      <div class="w3-col s8">
        <a href="{{ url('/estudiantes/home') }}">
          <img class="w3-margin w3-hide-large" src="{{asset('images/sia-small.png')}}" style="width:65px;"/>
        </a>
      </div>
      <div class="w3-col s4 w3-margin-top text-right">
        <div class="w3-dropdown-click w3-hover-none w3-right">
            <a class="w3-text-white w3-hover-opacity" href="javascript:void(0);" onclick="myFunction('shorticonmenu')">
                <span class="fa fa-user-circle fa-fw fa-2x">
                </span>
            </a>
            <div class="w3-dropdown-content w3-bar-block w3-border" id="shorticonmenu" style="right: 0; z-index: 4; width: 240px;">
                <a class="w3-bar-item w3-button" href="{{ route('estudiantes.profile') }}" onclick="myFunction('shorticonmenu')">
                    <i class="fa fa-user fa-fw w3-margin-right">
                    </i>
                    Perfil
                </a>
                @php
                  $directorio = \App\Sistema::where('sistema_nombre_corto', '=', 'directorio')->first();
                @endphp
                <a class="w3-bar-item w3-button" href="{{route('estudiantes.'.$directorio->sistema_nombre_corto, $directorio)}}" onclick="myFunction('shorticonmenu')">
                    <i class="fa fa-address-book fa-fw w3-margin-right">
                    </i>
                    Directorio
                </a>
                {{--@if(Auth::user()->rol_sia == 'Administrador')
                <a class="w3-bar-item w3-button" href="#contact" onclick="myFunction('shorticonmenu')">
                    <i class="fa fa-envelope fa-fw w3-margin-right">
                    </i>
                    Contacto
                </a>
                @endif--}}
                <a class="w3-bar-item w3-button w3-padding" href="{{ url('/estudiantes/logout') }}" onclick="event.preventDefault();
       document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out fa-fw w3-margin-right">
                    </i>
                    Cerrar Sesión
                </a>
                <form action="{{ url('/estudiantes/logout') }}" id="logout-form" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <!-- scope to search for services -->
        {{-- {!! Form::open(['route' => ['browse.home'], 'method' => 'get']) !!}
        <button class="w3-bar-item w3-button w3-medium w3-2017-navy-peony w3-mobile w3-right" type="submit">
            <span class="fa fa-search">
            </span>
        </button>
        <input class="w3-bar-item w3-input w3-medium w3-white w3-mobile w3-right w3-col m2 l2 w3-border-2017-navy-peony" maxlength="20" name="buscar" placeholder="Buscar" required="" type="text"/>
            {!! Form::close() !!} --}}
      </div>
    </div>
  </header>

  <div class="w3-container">
    {{--muestra mensajes de flash--}}
    @include('flash::message')

    <!-- muestra mensajes de sweet alert -->
    @include('sweet::alert')

    {{--Alerta de errores--}}
    {{--@if(count($errors) > 0)
    <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
      <p>
      @foreach($errors->all() as $error)
        <li>
          {{ $error }}
        </li>
      @endforeach
      </p>
    </div>
    @endif--}}
    {{--Fin alerta de errores--}}
  </div>
  <br>
  {{-- servicios --}}
  <div class="w3-container">
    @yield('content-estudiantes')
  </div>
  <br>

  <!-- Footer -->
  <!-- Footer for large screens -->
    <div class="w3-hide-medium w3-hide-small" style="position: relative; bottom: -25vh; width: 100%;">
        <footer class="w3-container w3-padding" style="background-color: #006894; background-image: url({{ asset('images/bg-footer.png') }});">
            <div class="w3-row-padding">
                <div class="w3-col s12">
                    <center>
                        <img class="w3-image" src="{{ asset('images/logo-blanco.png') }}"/>
                    </center>
                </div>
            </div>
        </footer>
        <div class="w3-2017-navy-peony w3-center w3-padding">
            <p>
                Área TIC - Vicerrectoría Académica | Fundación Universitaria Católica Lumen Gentium | Desarrollado por Agnusdei ADSI | Template Powered by
                <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">
                    w3.css
                </a>
                | {{ date('Y') }}
            </p>
            <p>
                Contáctenos Campus Pance, Cali, Valle / soporte@apps.unicatolica.edu.co / Teléfono: (+57) 2 3120038 ext. 1052
            </p>
        </div>
    </div>

    <!-- Footer for small screens -->
    <div class="w3-hide-large">
        <footer class="w3-container w3-padding" style="background-color: #006894; background-image: url({{ asset('images/bg-footer.png') }});">
            <div class="w3-row-padding">
                <div class="w3-col s12">
                    <center>
                        <img class="w3-image" src="{{ asset('images/logo-blanco.png') }}"/>
                    </center>
                </div>
            </div>
        </footer>
        <div class="w3-2017-navy-peony w3-center w3-padding">
            <p>
                Área TIC - Vicerrectoría Académica | Fundación Universitaria Católica Lumen Gentium | Desarrollado por Agnusdei ADSI | Template Powered by
                <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">
                    w3.css
                </a>
                | {{ date('Y') }}
            </p>
            <p>
                Contáctenos Campus Pance, Cali, Valle / soporte@apps.unicatolica.edu.co / Teléfono: (+57) 2 3120038 ext. 1052
            </p>
        </div>
    </div>
<!-- End page content -->
</div>

<script>
// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

function myFunction(menu) {
    var x = document.getElementById(menu);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}

$('#flash-overlay-modal').modal();
</script>
<script>
  $( function() {
    $( "#accordion" ).accordion({
      //collapsible: true
      heightStyle: "content"
      
    });
  } );
</script>
<script src="{{asset('js/cancelaciones.js')}}"></script>
<script src="{{asset('js/certificados.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/egresados/jquery-ui.js')}}"></script>
{{--<script src="{{asset('js/egresados/actions.js')}}"></script>--}}
<script src="{{asset('js/egresados/estudiantes.js')}}"></script>
<script src="{{asset('js/egresados/accordion.js')}}"></script>
<script src="{{asset('js/egresados/jquery.validate.js')}}"></script>
</body>
</html>
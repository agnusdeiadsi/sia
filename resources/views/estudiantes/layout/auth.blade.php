<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title-estudiantes')</title>

    <!-- Styles -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/w3-v4.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('w3css/colors/w3-colors-camo.css') }}">
    <link href="{{ asset('w3css/colors/w3-colors-food.css') }}"" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-highway.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-safety.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-signal.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-vivid.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-2017.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-windows.css') }}" rel="stylesheet">
    <link href="{{ asset('breadcrumbs/breadcrumbs.css') }}" rel="stylesheet">

    {{-- Sweetalert --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.min.css') }}">
    <script type="text/javascript" src="{{ asset('sweetalert/js/sweetalert-1-1-3.min.js') }}"></script>
    
    <style type="text/css">
        @media (min-width: 1400px){
            body{
                background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
                background-size: cover;
                background-repeat: no-repeat;
            }

            .logo{
               width: 300px; 
            }

            .content{
                margin-top: 3em;
            }
        }

        @media (max-width: 1400px){
            body{
                background-image: url('{{ asset('images/768x1024.png') }}'); color: white;
                background-size: cover;
                background-repeat: no-repeat;
            }

            .logo{
                width: 250px;   
            }

            .content{
                margin-top: 3em;
            }
        }

        @media (max-width: 320px){
            body{
                background-image: url('{{ asset('images/768x1024.png') }}'); color: white;
                background-size: cover;
                background-repeat: no-repeat;
            }

            .logo{
                width: 180px;
            }

            .content{
                margin-top: 3em;
            }
        }
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @if(!Auth::guard('estudiante')->check())
        <div id="app" class="w3-center flex-center position-ref full-height w3-margin">
            <div class="content w3-animate-opacity">
                <div class="title m-b-md">
                    <img src="{{ asset('images/sia.png') }}" class="w3-image logo" alt="logo">
                </div>
                <!-- muestra mensajes de sweet alert -->
                @include('sweet::alert')

                {{--muestra el formulario para el inicio de sesion--}}
                @yield('content-login')
                <br>
                <br>
                <div class="w3-padding w3-hide-small w3-hide-medium" style="position: absolute; bottom: 0; left: 0; right: 0;">
                    {{--<div class="title m-b-md">
                        <img src="{{ asset('images/logo-blanco.png') }}" class="w3-image" alt="logo">
                    </div>--}}
                    <p style="margin: auto 0; text-align: center;"><b>Fundación Universitaria Católica Lumen Gentium</b><br>
                    Sistema Integrado de Aplicaciones - SIA<br>
                    {{ date('Y') }}</p>
                </div>

                <div class="w3-padding w3-hide-large">
                    {{--<div class="title m-b-md">
                        <img src="{{ asset('images/logo-blanco.png') }}" class="w3-image" alt="logo">
                    </div>--}}
                    <p style="margin: auto 0; text-align: center;"><b>Fundación Universitaria Católica Lumen Gentium</b><br>
                    Sistema Integrado de Aplicaciones - SIA<br>
                    {{ date('Y') }}</p>
                </div>
            </div>
        </div>
    @else
        <script>
            window.location.href='/sia/public/estudiantes/home';
        </script>
    @endif

    <!-- Scripts -->
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>

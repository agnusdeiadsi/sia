@extends('estudiantes.layout.default')

@section('title-estudiantes')
	Editar Apelación {{$apelacion->apelacion_id}} | {{$sistema->sistema_nombre}}
@endsection

@section('content-estudiantes')
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li><a href="{{ route('estudiantes.cancelaciones', $sistema) }}">Cancelaciones</a></li>
			<li><a href="{{ route('estudiantes.cancelaciones.solicitudes.show', [$sistema, $apelacion->solicitud]) }}">Solicitud {{$apelacion->solicitud->solicitud_id}}</a></li>
			<li class="active"><span>Apelación</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="w3-text-{{$sistema->sistema_colorclass}}"><b>Apelación<br>Solicitud N° {{$apelacion->solicitud->solicitud_id}}</b></h2>
		</div>
	</div>
	<hr>

	{!! Form::open(['route' => ['estudiantes.cancelaciones.apelaciones.store', $sistema, $apelacion->solicitud], 'method' => 'post', 'files' => 'true']) !!}
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitud</b></h4>
        </div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('fecha', 'Fecha Radicación') }}
					{{ Form::text('fecha', $apelacion->solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha Radicación', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('solicitud_id', 'ID') }}
					{{ Form::text('solicitud_id', $apelacion->solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'ID Solicitud', 'readonly']) }}
				</p>
			</div>
		</div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('tipo_id', 'Motivo') }}
					{{ Form::text('tipo_id', $apelacion->solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Motivo', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('estado_id', 'Estado') }}
					{{ Form::text('estado_id', $apelacion->solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Estado', 'readonly']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Apelación</b></h4>
        </div>

        <div class="w3-row-padding">
        	<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('created_at', 'Fecha radicación') }}
					{{ Form::text('apelacion_id', $apelacion->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
				</p>
			</div>
			
			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('apelacion_id', 'ID Apelación') }}
					{{ Form::text('apelacion_id', $apelacion->apelacion_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('apelacion_estado_id', 'Estado') }}
					{{ Form::text('apelacion_id', $apelacion->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
				</p>
			</div>
		</div>

        <div class="w3-row-padding">
			<div class="w3-col s12">
				<p>
					{{ Form::label('apelacion_descripcion', 'Descripción*') }}
					{{ Form::textarea('apelacion_descripcion', $apelacion->apelacion_descripcion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b> Adjuntos</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12">
				<span class="help-block">Si deseas, puedes adjuntar algún documento como soporte de la apelación.</span>
				<div class="w3-responsive">
        			<table id="tabla" class="table table-condensed table-striped table-responsive">
        				<thead class="w3-{{$sistema->sistema_colorclass}}">
        					<th>Archivo</th>
        					<th></th>
        				</thead>
                		<tbody>
        					<tr class="fila-base w3-text-grey">
        						<td>
        							{{ Form::file('apelacion_adjunto[]', ['class' => 'w3-input cadjunto w3-border-'.$sistema->sistema_colorclass, 'id' => 'adjunto', 'disabled' => 'true']) }}
        						</td>
        						<td class="text-center eliminar">
			                      <span class="fa fa-minus-circle"></span>
			                    </td>
        					</tr>
        				</tbody>
        			</table>
        		</div>
        		<center><a href="#" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" onclick="agregarFila(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Respuesta</b></h4>
        </div>

        @if($apelacion->respuesta != null)
        	{{$apelacion->respuesta}}
        @endif

		<hr>
		<div class="w3-row-padding w3-center">
			<div class="w3-col s12">
				<a href="{{ route('estudiantes.cancelaciones.solicitudes.show', [$sistema, $apelacion->solicitud]) }}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-reply fa-fw"></span> Volver</a>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
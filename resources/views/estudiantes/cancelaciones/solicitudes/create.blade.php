@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}}
@endsection

@section('content-estudiantes')
	@php
		$matriculado = \App\Matriculado::find(Auth::guard('estudiante')->user()->matriculado_pidm);
	@endphp

	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li><a href="{{ route('estudiantes.cancelaciones', $sistema) }}">Cancelaciones</a></li>
			<li class="active"><span>Crear</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="w3-text-{{$sistema->sistema_colorclass}}"><b>Formulario de Reclamos<br>y Cancelaciones Financieras</b></h2>
		</div>
	</div>
	<hr>
	{!! Form::open(['route' => ['estudiantes.cancelaciones.solicitudes.store', $sistema], 'method' => 'post', 'files' => 'true']) !!}
		{{-- Datos del matriculado --}}
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Estudiante</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_nombres', 'Tipo Documento') }}
					{{ Form::text('matriculado_nombre', $matriculado->identificacion->first()->identificacion_tipo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_identificación', 'Documento') }}
					{{ Form::text('matriculado_nombre', $matriculado->identificacion->first()->identificacion_numero, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_codigo', 'Código') }}
					@if(count($matriculado->academico->first()) > 0)
						{{ Form::text('matriculado_codigo', $matriculado->academico->first()->academico_codigo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@else
						{{ Form::text('matriculado_codigo', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@endif

					{{ Form::hidden('matriculado_pidm', $matriculado->matriculado_pidm, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'PIDM', 'readonly']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_nombres', 'Nombres*') }}
					{{ Form::text('matriculado_nombre', $matriculado->matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('subarea_id', 'Programa*') }}
					{{ Form::select('subarea_id', $subareas->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('sede_id', 'Sede*') }}
					{{ Form::select('sede_id', $sedes->pluck('sede_nombre', 'sede_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_email', 'E-mail*') }}
					{{ Form::email('matriculado_email', $matriculado->matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'E-mail', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_telefono', 'Teléfono') }}
					{{ Form::text('matriculado_telefono', $matriculado->matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono', 'maxlength' => '10', 'pattern' => '[0-9]{7,10}',]) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_celular', 'Celular*') }}
					{{ Form::text('matriculado_celular', $matriculado->matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Celular', 'maxlength' => '10', 'pattern' => '[0-9]{10,10}', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m2 l2">
				<p>
					{{ Form::label('chk_mayor', 'Certifico que soy mayor de edad (18+)') }}
					{{ Form::checkbox('chk_mayor', 1, false, ['onclick' => 'mayorEdad();', 'id' => 'chk_mayor']) }}
				</p>
			</div>

			<div class="w3-col s12 m5 l5">
				<p>
					{{ Form::label('registro_civil', 'Registro Civil') }}
					{{ Form::file('registro_civil', ['class' => 'w3-input adjunto_menor w3-border-'.$sistema->sistema_colorclass, 'id' => 'registro_civil', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m5 l5">
				<p>
					{{ Form::label('documento_acudiente', 'Documento  Acudiente') }}
					{{ Form::file('documento_acudiente', ['class' => 'w3-input adjunto_menor w3-border-'.$sistema->sistema_colorclass, 'id' => 'documento_acudiente', 'required']) }}
				</p>
			</div>
		</div>


		{{-- Datos de la Solicitud --}}
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitud</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('tipo_id', 'Motivo*') }}
					{{ Form::select('tipo_id', $tipos->pluck('tipo_nombre', 'tipo_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12">
				<p>
					{{ Form::label('solicitud_descripcion', 'Descripción*') }}
					{{ Form::textarea('solicitud_descripcion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b> Adjuntos</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12">
				<span class="help-block">Aquí debes adjuntar todos los documentos comprobantes de tu solicitud; tales como: excusas médicas, facturas, entre otros. Los archivos deben estar en formato PDF cuyo tamaño no debe superar los 5120 Bytes (5MB).</span>
				<div class="w3-responsive">
        			<table id="tabla" class="table table-condensed table-striped table-responsive">
        				<thead class="w3-{{$sistema->sistema_colorclass}}">
        					<th>Archivo</th>
        					<th></th>
        				</thead>
                		<tbody>
        					<tr class="fila-base w3-text-grey">
        						<td>
        							{{ Form::file('solicitud_adjunto[]', ['class' => 'w3-input cadjunto w3-border-'.$sistema->sistema_colorclass, 'id' => 'adjunto', 'disabled' => 'true']) }}
        						</td>
        						<td class="text-center eliminar">
			                      <span class="fa fa-minus-circle"></span>
			                    </td>
        					</tr>
        				</tbody>
        			</table>
        		</div>
        		<center><a href="#" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" onclick="agregarFila(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
			</div>
		</div>
		<hr>
		<div class="w3-row-padding w3-center">
			<div class="w3-col s12">
				<button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
				<a href="{{ route('estudiantes.cancelaciones', [$sistema, $matriculado]) }}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}} | Crear Solicitud
@endsection

@section('content-estudiantes')
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li><a href="{{ route('estudiantes.cancelaciones', $sistema) }}">Cancelaciones</a></li>
			<li class="active"><span>Solicitud {{$solicitud->solicitud_id}}</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="w3-text-{{$sistema->sistema_colorclass}}"><b>Reclamos y Cancelaciones Financieras<br>Solicitud N° {{ $solicitud->solicitud_id }}</b></h2>
		</div>
	</div>
	<hr>
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitante</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_nombres', 'Tipo Documento') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->identificacion->first()->identificacion_tipo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_identificación', 'Documento') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->identificacion->first()->identificacion_numero, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_codigo', 'Código') }}
					@if(count($solicitud->matriculado->academico) > 0)
						{{ Form::text('matriculado_codigo', $solicitud->matriculado->academico->first()->academico_codigo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@else
						{{ Form::text('matriculado_codigo', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@endif

					{{ Form::hidden('matriculado_pidm', $solicitud->matriculado->matriculado_pidm, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'PIDM', 'readonly']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_nombres', 'Nombres*') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('programa_id', 'Programa*') }}
					@if(count( $solicitud->matriculado->academico) > 0)
						{{ Form::text('programa_id', $solicitud->matriculado->academico->first()->programa->subarea_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Programa', 'readonly', 'required']) }}
					@else
						{{ Form::text('programa_id', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Programa', 'readonly', 'required']) }}
					@endif
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('sede_id', 'Sede*') }}
					@if(count($solicitud->matriculado->academico) > 0)
						{{ Form::text('sede_id', $solicitud->matriculado->academico->first()->sede->sede_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Sede', 'readonly', 'required']) }}
					@else
						{{ Form::text('sede_id', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Sede', 'readonly', 'required']) }}
					@endif
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_email', 'E-mail*') }}
					{{ Form::text('matriculado_email', $solicitud->matriculado->matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'E-mail', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_telefono', 'Teléfono*') }}
					{{ Form::text('telefono', $solicitud->matriculado->matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_celular', 'Celular*') }}
					{{ Form::text('matriculado_celular', $solicitud->matriculado->matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Celular', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitud</b></h4>
        </div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('fecha', 'Fecha Radicación') }}
					{{ Form::text('fecha', $solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha Radicación', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('solicitud_id', 'ID') }}
					{{ Form::text('solicitud_id', $solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'ID Solicitud', 'readonly']) }}
				</p>
			</div>
		</div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('tipo_id', 'Motivo') }}
					{{ Form::text('tipo_id', $solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Motivo', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('estado_id', 'Estado') }}
					{{ Form::text('estado_id', $solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Estado', 'readonly']) }}
				</p>
			</div>
		</div>		

		<div class="w3-row-padding">
			<div class="w3-col s12">
				<p>
					{{ Form::label('solicitud_descripcion', 'Descripción*') }}
					{{ Form::textarea('solicitud_descripcion', $solicitud->solicitud_descripcion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Adjuntos</b></h4>
        </div>
		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-responsive">
        			<table class="w3-table-all">
                		<tbody>
			        		@if($solicitud->adjuntos->count() > 0)
			        			@foreach($solicitud->adjuntos as $adjunto)
		        					<tr class="w3-text-grey">
		        						<td>{{ $adjunto->adjunto_nombre }}</td>
		        						<td>
		        							<a href="{{ route('estudiantes.cancelaciones.adjuntos.download', [$sistema, $adjunto]) }}"><span class="fa fa-download fa-fw"></span> Descargar</a>
		        						</td>
		        					</tr>
			        			@endforeach
			        		@else
			        			<tr>
			        				<td>
			        					<i>La solicitud no tiene archivos adjuntos.</i>
			        				</td>
			        			</tr>
        					@endif
        				</tbody>
        			</table>
        		</div>
			</div>
		</div>

		@if($solicitud->estado_id == 3)
			{{--Respuesta--}}
			<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
	        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Respuesta</b></h4>
	        </div>
	        @if($solicitud->respuesta != null)
	        	<a href="{{ route('estudiantes.cancelaciones.respuestas.download', [$sistema, $solicitud->respuesta]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-download fa-fw" title="Descargar"></span> Descargar</a>
	        @endif
	    @endif
	    <hr>

	    <div class="row w3-center">
			<div class="col-sm-12">
				@if($solicitud->estado_id == 3)
					<a href="{{ route('estudiantes.cancelaciones.apelaciones.create', [$sistema, $solicitud]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-bar-item"><span class="fa fa-repeat fa-fw"></span> Apelar</a>
				@endif
				<a href="{{ route('estudiantes.cancelaciones', $sistema) }}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-remove fa-fw"></span> Volver</a>
			</div>
		</div>	
@endsection

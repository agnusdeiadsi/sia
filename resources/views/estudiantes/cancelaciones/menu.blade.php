@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}}
@endsection

@section('content-estudiantes')
	<!-- Menú -->
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li class="active"><span>Cancelaciones</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-row-padding w3-margin">
        <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.solicitudes.create', [$sistema, $matriculado])}}">
          <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
            <h1><span class="fa fa-check-square fa-fw"></span>Crear Solicitud</h1>
          </div>
        </a>

        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display='block'" class="w3-hover-opacity">
          <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
            <h1><span class="fa fa-search fa-fw"></span>Consultar Solicitudes</h1>
          </div>
        </a>
    </div>

    <div class="w3-container">
		<div id="id01" class="w3-modal">
			<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

			  <div class="w3-center"><br>
			    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
			    <h3><b>Consultar Solicitudes</b></h3>
			  </div>

			  <form class="w3-container" action="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.solicitudes.search', $sistema)}}" method="get">
			    <div class="w3-section">
			      <label><b>ID Solicitud</b></label>
			      <input class="w3-input w3-border w3-margin-bottom" type="text" maxlength="20" placeholder="ID Solicitud" name="solicitud_id" required>
			      <button class="w3-button w3-block w3-{{$sistema->sistema_colorclass}} w3-section w3-padding" type="submit">Buscar</button>
			    </div>
			  </form>
			</div>
		</div>
	</div>
@endsection
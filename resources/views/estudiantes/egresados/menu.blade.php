@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}}
@endsection

@section('content-estudiantes')
@php
    $pidm = Auth::guard('estudiante')->user()->matriculado_pidm;
    //$matriculado = \App\Matriculado::find($pidm); 
    //$grado = \App\Model\Egresados\Grado::GetStudent($pidm);

    //dd($grado);
 @endphp

<div class="w3-container"><br>
  <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
    <li class="active"><span>Egresados</span></li>
  </ol>
</div>

 <div class="w3-row-padding w3-margin">

    <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.encuesta', [$sistema, $pidm])}}" class="w3-hover-opacity">
      <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
        <h2><span class="fa fa-pencil-square fa-fw"></span>Encuesta momento de grado</h2>
      </div>
    </a>

    <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.formulario', [$sistema, $pidm])}}">
      <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
        <h2><span class="fa fa-edit fa-fw"></span>Formulario de actualizacion</h2>
      </div>
    </a>
    
</div>

@endsection
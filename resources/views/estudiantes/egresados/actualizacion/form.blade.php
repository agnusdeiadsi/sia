@extends('estudiantes.layout.default')

@section('title-estudiantes')
  {{$sistema->sistema_nombre}} | Formulario
@endsection

@section('content-estudiantes')

<div class="w3-container"><br>
  <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
    <li><a href="{{ route('estudiantes.egresados', $sistema) }}">Egresados</a></li>
    <li class="active"><span>Formulario de actualizacion</span></li>
  </ol>
</div>
@php
  $identificacion = $egresado->identificaciones()->orderBy('created_at', 'DESC')->get();
  //dd($identificacion);
@endphp
{{--dd($estudiante_info)--}}
<div class="w3-container">
@if(!empty($egresado))
  @if($egresado->referencia->count() > 0 /*&& $egresado->laboral->first()->situacionlaboral_id != ''*/)
  
    {!! Form::open(['route' => ['estudiantes.egresados.update', $sistema, $egresado->matriculado_pidm], 'method' => 'post']) !!}
  
      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h3><b>1. Informacion personal</b></h3>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <p>
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero identificacion<span style="color: red">*</span></label>
               
                {!! Form::text('numero', $identificacion->first()->identificacion_numero, ['class' => 'w3-input w3-text-grey', 'disabled']) !!}
          </p>
        </div>
      </div>
      
        <div class="row">
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Primer nombre<span style="color: red">*</span></label>
                 
                  {!! Form::text('first_name', 
                                  $egresado->matriculado_primer_nombre, 
                                  ['class' => 'w3-input w3-text-grey input-text', 
                                  'required']) !!}
              </p>
          </div>
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Segundo nombre</label>
                 
                  {!! Form::text('second_name', 
                                  $egresado->matriculado_segundo_nombre, 
                                  ['class' => 'w3-input w3-text-grey input-text']) !!}
              </p>
          </div>
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Primer apellido<span style="color: red">*</span></label>
                 
                  {!! Form::text('last_name_first', 
                                  $egresado->matriculado_primer_apellido, 
                                  ['class' => 'w3-input w3-text-grey input-text', 
                                  'required']) !!}
              </p>
          </div>

          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Segundo apellido<span style="color: red">*</span></label>
                 
                  {!! Form::text('last_name_second', 
                                  $egresado->matriculado_segundo_apellido, 
                                  ['class' => 'w3-input w3-text-grey input-text']) !!}
              </p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Telfono Fijo<span style="color: red">*</span></label>
                  {!! Form::number('tel', 
                                    $egresado->matriculado_telefono, 
                                    ['class' => 'w3-input w3-text-grey', 
                                    'required' => 'true']) !!}
              </p>
          </div>
          <div class="col-sm-3">
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono Celular<span style="color: red">*</span></label>
                  {!! Form::number('cel', 
                                    $egresado->matriculado_celular, 
                                    ['class' => 'w3-input w3-text-grey', 
                                    'required' => 'true']) !!}
              </p>
          </div>
          <div class="col-sm-3">
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Correo<span style="color: red">*</span></label>
                  {!! Form::text('email', 
                                  $egresado->matriculado_email, 
                                  ['class' => 'w3-input w3-text-grey', 
                                  'required' => 'true']) !!}
              </p>
          </div>

          <div class="col-sm-3">
              <p>
                 <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad de residencia<span style="color: red">*</span></label>
                 {!! Form::select('ciudad_residencia', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar Uno']) !!}
              </p>
          </div>
      </div>

      <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>2. Informacion laboral</b></h3>
            </div>
          </div>
        </div>

      <div>
        @if($egresado->laboral->count() > 0)
          <br>
          <div class="row">
            <div class="col-sm-12"> 
              <table class="table table-condensed table-striped table-responsive">
                <thead class="w3-{{$sistema->sistema_colorclass}}">
                  <th>
                    <center>
                      Situacion laboral
                    </center>
                  </th>
                  <th>
                    <center>
                      Nombre empresa
                    </center>
                  </th>
                  <th>
                    <center>
                      Cargo
                    </center>
                    
                  </th>
                  <th>
                    <center>
                      Duracion
                    </center>
                    
                  </th>
                  <th>
                    <center>
                      Contrato
                    </center>                
                  </th>
                  <th>
                    <center>
                      Vinculacion
                    </center>
                  </th>
                  <th>
                    <center>
                      Salario
                    </center>
                  </th>
                  
                </thead>
                <tbody>

                @if($egresado->laboral->count() > 0)
                  @foreach($egresado->laboral as $laboral) 
                    <tr>
                      <td>
                       <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->situacionlaboral->first()->situacionlaboral_nombre}}" >
                      </td>
                    @if($laboral->laboral_empresa != null)
                        <td>
                         <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->laboral_empresa}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->cargos->first()->nivelcargo_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->duraciones->first()->duracionempresa_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->contratos->first()->contrato_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->vinculaciones->first()->vinculacion_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->salarios->first()->rangosalario_nombre}}" >
                        </td>
                      @endif
                    </tr>
                  @endforeach
                @else
                    <tr>
                      <td>
                        <i>No existen resultados</i>
                      </td>
                    </tr>
                @endif
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <label class="w3-text-{{$sistema->sistema_colorclass}}">Situacion Laboral<span style="color: red">*</span></label>
               {!! Form::select('laboral_situacion', $situacionLaboral->pluck('situacionlaboral_nombre', 'situacionlaboral_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'id' => 'situacionlaboral']) !!}
            </div>
          </div>
          <br>
          <div id="form-laboral" hidden="">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                <h4><b>2.1. Datos de la empresa</b></h4>    
              </div>
            </div>
          </div>
            <div class="row">
              <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                     {!! Form::text('name_empresa', null, ['class' => 'w3-input w3-text-grey habilitar', 'maxlength' => '50', 'placeholder' => 'Nombre Empresa', 'id' => 'codigo', 'disabled']) !!}
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Tipo Empresa<span style="color: red">*</span></label>
               {!! Form::select('tipoEmpresa', $tipoEmpresa->pluck('tipoempresa_nombre', 'tipoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div>
                <div class="col-sm-2">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                 {!! Form::select('ciudad', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>2.2. Datos Laborales</b></h4>    
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Area<span style="color: red">*</span></label>
               {!! Form::select('area', $areaPertenece->pluck('areapertenece_nombre', 'areapertenece_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Cargo<span style="color: red">*</span></label>
               {!! Form::select('cargo', $cargo->pluck('nivelcargo_nombre', 'nivelcargo_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Relacion con el Programa<span style="color: red">*</span></label>
               {!! Form::select('relacionPrograma', $relacionPrograma->pluck('relacionprograma_nombre', 'relacionprograma_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Ocupacion<span style="color: red">*</span></label>
               {!! Form::select('ocupacion', $ocupacion->pluck('ocupacion_nombre', 'ocupacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Tiempo<span style="color: red">*</span></label>
               {!! Form::select('duracionEmpresa', $duracionEmpresa->pluck('duracionempresa_nombre', 'duracionempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
             {{-- <div class="col-sm-2">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Pais<span style="color: red">*</span></label>
               {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar']) !!}
              </div>--}}
            </div>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>2.3. Contratacion</b></h4>    
                </div>
              </div>
            </div>
            <div class="row">
               <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Vinculacion<span style="color: red">*</span></label>
               {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Contrato<span style="color: red">*</span></label>
               {!! Form::select('contrato', $contrato->pluck('contrato_nombre', 'contrato_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Rango Salarial<span style="color: red">*</span></label>
               {!! Form::select('salario', $rangoSalarial->pluck('rangosalario_nombre', 'rangosalario_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
            </div>
          </div>
        {{--@else
        <div>
          <div class="row">
            <div class="col-sm-12"> 
                  <p>
                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa donde Actualmente Labora<span style="color: red">*</span></label>
                      {!! Form::text('name_empresa', null, ['class' => 'w3-input w3-text-grey', 'maxlength' => '50', 'placeholder' => 'Nombre Empresa', 'id' => 'codigo', 'required' => 'true']) !!}
                  </p>
            </div>
          </div>
        </div>--}}
        @endif
        <hr>
        <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>3. Informacion academica</b></h3>
            </div>
          </div>
        </div>
        <div>
        <div class="row">
          <div class="col-sm-12">
            <div class="w3-responsive">
              <table class="table table-condensed table-striped table-responsive">
                <thead class="w3-{{$sistema->sistema_colorclass}}">
                  <th>Nivel Academico</th>
                  <th>Nombre Institucion</th>
                  <th>Titulo Obtenido</th>
                  <th>Inicio</th>
                  <th>Fin</th>

                </thead>
                <tbody>
                @if($egresado->estudios->count() > 0)
                  @foreach($egresado->estudios as $data)
                    <tr class="w3-text-grey">
                      
                      <td>
                        <input type="hidden" value="{{$data->nivelAcademico->nivelacademico_id}}">
                        <input type="text" name="estudio_nivel_" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm nivel" maxlength="100" readonly="" value="{{$data->nivelAcademico->nivelacademico_nombre}}" >
                      </td>
                      <td>
                        <input type="text" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm institucion" maxlength="100"  readonly="" value="{{$data->estudio_institucion}}" >
                      </td>
                      <td>
                        <input type="text" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm titulo" maxlength="50"  readonly="" value="{{$data->estudio_titulo}}">
                      </td>
                      <td>
                        <input type="text" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm inicio" maxlength="100" readonly="" value="{{$data->estudio_inicio}}">
                      </td>
                      <td>
                        <input type="text" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm fin" maxlength="20" readonly="" value="{{$data->estudio_fin}}">
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td>
                      <i>No existen resultados</i>
                    </td>
                  </tr>
                @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><br>

    <div class="row">
        <div class="col-sm-12">
          <label><h4>¿Ha realizado otros estudios?</h4></label><br>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12"  style="margin-bottom: 10px">
           <label class="w3-text-{{$sistema->sistema_colorclass}}">Si</label>          
              {{ Form::radio('estudio', 'si', false, ['class' => 'show-form-es']) }}
          <label class="w3-text-{{$sistema->sistema_colorclass}}"  style="margin-left: 20px">No</label>
              {{ Form::radio('estudio', 'no', true, ['class' => 'show-form-es']) }}
        </div>
      </div>
      <br>
      <div id="form-estudio" hidden="">
        <div class="row">
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Nivel academico<span style="color: red">*</span></label>
            {!! Form::select('estudio_nivel_estudiante', $nivel->pluck('nivelacademico_nombre', 'nivelacademico_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_es', 'placeholder' => 'Seleccionar Uno', 'disabled' => 'true']) !!}
          </div>
          <div class="col-sm-4">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la institucion<span style="color: red">*</span></label>
            <input type="text" name="estudio_nombreInstitucion_estudiante"  class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" maxlength="100" placeholder="Nombre de la institucion" disabled="true">
          </div>
          <div class="col-sm-4">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Titulo obtenido<span style="color: red">*</span></label>
            <input type="text" name="estudio_titulo_estudiante" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" maxlength="50" placeholder="Titulo Obtenido" disabled="true">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha inicio<span style="color: red">*</span></label>
            <input type="date" name="estudio_inicio_estudiante" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" disabled="true">
          </div>
          <div class="col-sm-4">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha fin<span style="color: red">*</span></label>
            <input type="date" name="estudio_fin_estudiante" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" disabled="true">
          </div>
        </div>
       
      </div>

      <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>4. Informacion de emprendimiento</b></h3>
            </div>
          </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <table class="table table-condensed table-striped table-responsive">
            <thead class="w3-{{$sistema->sistema_colorclass}}">
              <td>
                Nombre empresa
              </td>
              <td>
                Direccion
              </td>
              <td>
                Telefono empresa
              </td>
              <td>
                Ciudad
              </td>
              <td>
                Sector
              </td>
              <td>
                Tamaño
              </td>
            </thead>
            <tbody>
              @if($egresado->emprendimiento->count() > 0)

                @foreach($egresado->emprendimiento as $emprendimiento)  
                  <tr>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_empresa}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_direccion}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_telefono}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->ciudades->first()->ciudad_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->sectoresempresas->first()->sectorempresa_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->tamanoempresas->first()->tamanoempresa_nombre}}">
                    </td>
                  </tr>
                @endforeach
              @else
                  <tr>
                    <td>
                      <i>No existen resultados</i>
                    </td>
                  </tr>
              @endif
              
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <label><h4>¿Tiene Empresa?</h4></label>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          
            <label class="w3-text-{{$sistema->sistema_colorclass}}" >Si tiene</label>          
            {{ Form::radio('empresa', 'si', false, ['class' => 'show-form-em']) }}
            <label class="w3-text-{{$sistema->sistema_colorclass}}" style="margin-left: 20px">No tiene</label>
            {{ Form::radio('empresa', 'no', true, ['class' => 'show-form-em']) }}
        </div>
      </div>
        
      </div>
      <br>
      <div id="form-emprendimiento" hidden="">
        <div class="row">
          <div class="col-sm-4">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                       {!! Form::text('nombre_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Nombre Empresa', 'disabled']) !!}
          </div>
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Direccion de la empresa<span style="color: red">*</span></label>
                       {!! Form::text('direccion_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Dirrecion Empresa', 'disabled']) !!}
          </div>
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono de la empresa<span style="color: red">*</span></label>
                       {!! Form::number('telefono_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Telefono Empresa', 'disabled']) !!}
          </div>
        </div>
        <br>
        <div class="row">
              <div class="col-sm-2">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                   {!! Form::select('ciudad_em', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>

              <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Sector empresa<span style="color: red">*</span></label>
                   {!! Form::select('sector_em', $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>

              <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Tamaño empresa<span style="color: red">*</span></label>
                   {!! Form::select('tamano_em', $tamanoEmpresa->pluck('tamanoempresa_nombre', 'tamanoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
        </div>  
      </div><br>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h4><b>5. Reconocimiento</b></h4>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <table class="table table-condensed table-striped table-responsive">
              <thead class="w3-{{$sistema->sistema_colorclass}}">
              <td>
                Nombre de reconocimiento
              </td>
              <td>
                Nombre de la organizacion
              </td>
              <td>
                Fecha de reconocimiento
              </td>
            </thead>
            <tbody>
              @if($egresado->reconocimientos->count() > 0)
                @foreach($egresado->reconocimientos as $reconocimiento)
                  <tr>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_organizacion}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_fecha}}">
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td>
                    <i>No existen resultados</i>
                  </td>
                </tr>
              @endif
            
            </tbody>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">  
          <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre reconocimiento<span style="color: red">*</span></label>
                   {!! Form::text('reconocimiento_nombre', null, ['class' => 'w3-input w3-text-grey', 'placeholder' => 'Nombre reconocimiento']) !!}
        </div>
        <div class="col-sm-4">
          <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                   {!! Form::text('reconocimiento_organizacion', null, ['class' => 'w3-input w3-text-grey', 'placeholder' => 'Nombre de la epresa']) !!}
        </div>
        <div class="col-sm-4">
          <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha de reconocimiento<span style="color: red">*</span></label>
          <input type="date" name="reconocimiento_fecha" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm inicio">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-12">
          <button class="w3-btn w3-{{$sistema->sistema_colorclass}}">
            <span class="fa fa-floppy-o fa-fw"></span>
              Actualizar
          </button>
        </div>
      </div>

    {!!Form::close()!!}
  @else

    <div class="w3-container">
        <div class="row">
          <div class="w3-center">

            <h3 class="w3-text-grey">
              El formulario de actualización de datos no estará disponible hasta que no realice la encuesta “Momento de Grado”.
            </h3>
            <a class="w3-btn w3-{{$sistema->sistema_colorclass}}" href="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.encuesta', [$sistema, $egresado->matriculado_pidm])}}">
              Realizar encuesta
            </a>

          </div>
        </div>
    </div>

  @endif

@endif
<div>
@endsection
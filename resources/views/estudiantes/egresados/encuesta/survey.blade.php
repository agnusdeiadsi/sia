@extends('estudiantes.layout.default')

@section('title-estudiantes')
  {{$sistema->sistema_nombre}} | Encuesta
@endsection

@section('content-estudiantes')

<div class="w3-container"><br>
  <ol class="breadcrumb breadcrumb-arrow">
    <li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
    <li><a href="{{ route('estudiantes.egresados', $sistema) }}">Egresados</a></li>
    <li class="active"><span>Encuesta momento de grado</span></li>
  </ol>
</div>
@php
  $identificacion = $egresado->identificaciones()->orderBy('created_at', 'DESC')->get();
  //dd($identificacion);
@endphp
<div class="w3-container">
@if(isset($egresado))
  @if($num == 0) {{--Para saber si ha pasado mas de 1 año de haber hecho la encuesta--}}

    {{--dd($identificacio)--}}
      {!! Form::open(['route' => ['estudiantes.egresados.create', $sistema, $egresado->matriculado_pidm], 'method' => 'post', 'id' => 'form_name']) !!}

      <div id="accordion">
        <div class="w3-panel w3-leftbar- w3-border-{{$sistema->sistema_colorclass}} w3-{{$sistema->sistema_colorclass}}">
            <b>1. Personal</b>
        </div>

        <div id="input-personal">
          <div class="row">
              <div class="col-sm-3">
                <p>
                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero identificacion<span style="color: red">*</span></label>
                     
                      {!! Form::text('numero', $identificacion->first()->identificacion_numero, ['class' => 'w3-input w3-text-grey input-text', 'readonly', 'required']) !!}
                </p>
              </div>
          </div>
                              
          <div class="row">
            <div class="col-sm-3"> 
                <p>
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Primer nombre<span style="color: red">*</span></label>
                   
                    {!! Form::text('first_name', 
                                    $egresado->matriculado_primer_nombre, 
                                    ['class' => 'w3-input w3-text-grey input-text', 
                                    'required']) !!}
                </p>
            </div>
            <div class="col-sm-3"> 
                <p>
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Segundo nombre</label>
                   
                    {!! Form::text('second_name', 
                                    $egresado->matriculado_segundo_nombre, 
                                    ['class' => 'w3-input w3-text-grey input-text']) !!}
                </p>
            </div>
            <div class="col-sm-3"> 
                <p>
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Apellidos<span style="color: red">*</span></label>
                   
                    {!! Form::text('last_name_first', 
                                    $egresado->matriculado_primer_apellido, 
                                    ['class' => 'w3-input w3-text-grey input-text', 
                                    'required']) !!}
                </p>
            </div>
            <div class="col-sm-3"> 
                <p>
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Apellidos</label>
                   
                    {!! Form::text('last_name_second', 
                                    $egresado->matriculado_segundo_apellido, 
                                    ['class' => 'w3-input w3-text-grey input-text']) !!}
                </p>
            </div>
          </div>
              
          <div class="row">   
             <div class="col-sm-3">
                  <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Telfono Fijo<span style="color: red">*</span></label>
                      {!! Form::number('tel', $estudiante_infoC->first()->telp_telres, ['class' => 'w3-input w3-text-grey input-text', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Telfono Fijo<span style="color: red">*</span></label>
                      {!! Form::text('tel', 
                                      $egresado->matriculado_telefono, 
                                      ['class' => 'w3-input w3-text-grey input-text input-number',  
                                      'required']) !!}
                  </p>
              </div>
              <div class="col-sm-3">
                  <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono Celular<span style="color: red">*</span></label>
                      {!! Form::number('cel',  $estudiante_infoC->first()->telp_telcel, ['class' => 'w3-input w3-text-grey input-text', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono Celular<span style="color: red">*</span></label>
                      {!! Form::text('cel', 
                                      $egresado->matriculado_celular, 
                                      ['class' => 'w3-input w3-text-grey input-text input-number',  
                                      'required']) !!}
                  </p>
              </div>
              <div class="col-sm-3">
                  <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Correo<span style="color: red">*</span></label>
                      {!! Form::text('email', $estudiante_infoE->first()->emal_altr, ['class' => 'w3-input w3-text-grey input-text', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Correo<span style="color: red">*</span></label>
                      {!! Form::Text('email', 
                                      $egresado->matriculado_email, 
                                      ['class' => 'w3-input w3-text-grey input-email', 
                                      'required']) !!}
                  </p>
              </div>

              <div class="col-sm-3">
                  <p>
                     {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad de residencia<span style="color: red">*</span></label>
                     {!! Form::Text('ciudad_residencia', $estudiante_infoR->first()->ubip_ciudad, ['class' => 'w3-input w3-text-grey input-text', 'readonly']) !!}--}}

                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad de residencia<span style="color: red">*</span></label>
                     {!! Form::Select('ciudad_residencia', 
                                      $ciudad->pluck('ciudad_nombre', 'ciudad_id'), 
                                      $egresado->ciudad_id, 
                                      ['class' => 'w3-select w3-text-grey input-select', 
                                      'placeholder' => ' Seleccionar',
                                      'required']) !!}
                  </p>
              </div>
            </div> 
              
              <br>
            <div class="row">
              <div class="col-sm-5">
                <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Direccion de residencia<span style="color: red">*</span></label>
                     
                      {!! Form::text('direccion_estudiante', $estudiante_infoR->first()->ubip_direccion, ['class' => 'w3-input w3-text-grey input-text', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Direccion de residencia<span style="color: red">*</span></label>
                     
                      {!! Form::text('direccion_estudiante', 
                                      $egresado->matriculado_direccion, 
                                      ['class' => 'w3-input w3-text-grey input-text', 
                                      'required']) !!}
                  </p>
              </div>
              <div class="col-sm-2">
                <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Estrato<span style="color: red">*</span></label>
                     
                      {!! Form::text('estrato', $estudiante_info->first()->pers_estrato, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Estrato<span style="color: red">*</span></label>
                     
                      {!! Form::Select('estrato', $estrato, $egresado->matriculado_estrato, ['class' => 'w3-select w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
                  </p>
              </div>
              <div class="col-sm-2">
                <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Genero<span style="color: red">*</span></label>
                     
                       {!! Form::text('genero', $estudiante_infoP->first()->biop_desc_genero, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text', 'readonly']) !!}--}}

                       <label class="w3-text-{{$sistema->sistema_colorclass}}">Genero<span style="color: red">*</span></label>
                     
                       {!! Form::Select('genero', 
                                        $genero, 
                                        $egresado->matriculado_genero, 
                                        ['class' => 'w3-select w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                        'placeholder' => 'Seleccionar', 
                                        'required']) !!}
                  </p>
              </div>
              <div class="col-sm-3">
                <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Estado civil<span style="color: red">*</span></label>
                     
                       {!! Form::text('estado_civil', $estudiante_infoP->first()->biop_desc_estadocivil, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text', 'readonly']) !!}--}}

                       <label class="w3-text-{{$sistema->sistema_colorclass}}">Estado civil<span style="color: red">*</span></label>
                     
                       {!! Form::Select('estado_civil', $estadosCiviles->pluck('estadocivil_nombre', 'estadocivil_id'), $egresado->estadocivil_id, ['class' => 'w3-select w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
                  </p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-3">
                <p>
                      {{--<label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha de nacimiento<span style="color: red">*</span></label>
                     
                      {!! Form::date('fecha_nacimiento', $fecha[0], ['class' => 'w3-input w3-text-grey', 'readonly']) !!}--}}

                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha de nacimiento<span style="color: red">*</span></label>
                     
                      {!! Form::date('fecha_nacimiento', 
                                      $egresado->matriculado_fecha_nacimineto, 
                                      ['class' => 'w3-input w3-text-grey', 
                                      'required']) !!}
                  </p>
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-personal w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-academico">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-personal" class="w3-center" hidden="">
              <label class="w3-text-{{$sistema->sistema_colorclass}}">
                Por favor da click abajo
                <span class="fa fa-level-down"></span>
              </label>
            </div>

        </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-academico">
            <b>2. Academico</b>
          </div>

          <div id="input-academico">
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Codigo ID<span style="color: red">*</span></label>
                {!! Form::text('codigo_estudiante', $Begresado->pers_id, ['class' => 'w3-input w3-text-grey', 'readonly']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Sede<span style="color: red">*</span></label>
                {!! Form::select('sede_academico', $sedes->pluck('sede_nombre', 'sede_id'), null, ['class' => 'w3-input w3-text-grey input-text', 'required' => 'true', 'placeholder' => 'Seleccionar']) !!}
              </div>
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-8">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Programa<span style="color: red">*</span></label>
                {!! Form::Select('Programa_academico', $programas->pluck('programa_nombre', 'programa_id'), null, ['class' => 'w3-select w3-text-grey input-text', 'placeholder' => 'Seleccionar', 'id' => 'aca-programa-select', 'required']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Convenio</label>
                {{--{!! Form::Select('Convenio_academico', [], null, ['class' => 'w3-input w3-text-grey input-select', 'id' => 'aca-convenio']) !!}--}}

                {!! Form::hidden('Convenio_academico', null, ['id' => 'aca-convenio-id']) !!}

                {!! Form::text('Convenio_academico_text', null, ['class' => 'w3-input w3-text-grey habilitar input-text', 'id' => 'aca-convenio-text', 'readonly']) !!}
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-academico w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-censal">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-academico" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-censal">
            <b>3. Censal</b>
          </div>
          <div id="input-censal">
            <div class="row">
              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Pertenece a una población con necesidades educativas especiales?
                <span style="color: red">*</span></label>
                {!! Form::select('censal_necesidades_edu', $sino, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </div>
              <div class="col-sm-2">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Discapacidad</label>
                {!! Form::select('censal_discapacidad', $discapacidad, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Patrocino sus estudios alguna organización?
                <span style="color: red">*</span></label>
                {!! Form::select('censal_patrocino', $sino, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'id' => 'censal_patrocino', 'required' => 'true']) !!}
              </div>
            </div>

            <div class="row" id="divNameCompany" hidden="" style="margin-top: 10px">
                <div class="col-sm-12">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa
                  <span style="color: red">*</span></label>
                  {!! Form::text('censal_name_empresa', null, ['class' => 'w3-input w3-text-grey input-text', 'placeholder' => 'Nombre organizacion', 'id' => 'nameCompany', 'disabled']) !!}
                </div>
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Forma parte de algún tipo de comunidad?</label>
                <span style="color: red">*</span></label>
              </div>
              <div class="col-sm-12" style="margin-top: 10px">
                @foreach($comunidad_asociacion as $data => $key)
                  {{ Form::checkbox('cs_comunidad[]', 
                                    $data, 
                                    false,
                                    ['class' => 'cs input-checkbox', 
                                    'required',
                                    'id' => 'cs_comunidad']) }}
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">{{$key}}</label>
                  <br>
                @endforeach
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Otro, cual?</label>
                  {{Form::Text('cs', null, ['class' => 'w3-input w3-text-grey cs_required input-text', 
                                            'required' => 'true', 
                                            'placeholder' => 'Nombre de la comunidad asociativa', 'id' => 'cs-text', 'required' => 'true'])}}
                
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-censal w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-familiar">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-censal" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-familiar">
            <b>4. Familiar</b>
          </div>
          <div id="input-familiar">
            <div class="row">
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero integrantes en su familia</label>
                <span style="color: red">*</span></label>
                {!! Form::select('famila_intigrantes', $familia_select, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
              </div>
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero de hijos</label>
                <span style="color: red">*</span></label>
                {!! Form::select('famila_hijos', $familia_select, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
              </div>
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero de hermanos</label>
                <span style="color: red">*</span></label>
                {!! Form::select('famila_hermanos', $familia_select, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
              </div>
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Hermanos con estudios superiores</label>
                <span style="color: red">*</span></label>
                {!! Form::select('famila_hermanos_superiores', $familia_select, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar', 'required']) !!}
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-familiar w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-financiacion">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-familiar" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-financiacion">
            <b>5. Financiacion</b>
          </div>

          <div id="input-finaciacion">
            <div class="row">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Durante su proceso de formación, usted financio sus estudios con alguna entidad?
                </label>
                <span style="color: red">*</span></label>
              </div>
              <div class="col-sm-2">
                {!! Form::select('financiacion_patrocino', $sino, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar' , 'id' => 'finacial-patrocinio', 'required' => 'true']) !!}
              </div>
            </div>
            <div id="divFormFinEnti" hidden>
              <div class="row" style="margin-top: 15px">
                <div class="col-sm-12">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">
                    ¿De cuál de estas entidades recibió un crédito educativo durante algún(os) semestre(s) de su carrera?
                  </label>
                  <span style="color: red">*</span></label>
                </div>
                <div class="col-sm-6" style="margin-top: 15px">
                  @foreach($entidades_finan as $entidad => $key)
                    {{ Form::checkbox('cs_finan[]', $entidad, false, ['class' => 'finanInput input-checkbox', 'required' => 'true']) }}
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">{{$key}}</label>
                    <br>
                  @endforeach
                </div>
              </div>
            </div>

            <div class="row" style="margin-top: 15px">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Durante su proceso de formación, usted recibió algún tipo de beca?
                </label>
                <span style="color: red">*</span></label>
              </div>

              <div class="col-sm-2">
                {!! Form::select('financiacion_beca', $sino, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 'placeholder' => 'Seleccionar' , 'id' => 'finacial-beca', 'required' => 'true']) !!}
              </div>
            </div>
            <div id="divFormBecaEnti" hidden>
              <div class="row" style="margin-top: 15px">
                <div class="col-sm-12">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">
                    ¿Con que entidad, empresa o institución obtuvo la beca?
                  </label>
                  <span style="color: red">*</span></label>
                </div>
                <div class="col-sm-6" style="margin-top: 15px">
                  @foreach($entidades_beca as $entidad => $key)
                    {{ Form::checkbox('cs_beca[]', $entidad, false, ['class' => 'becaInput input-checkbox', 'required' => 'true']) }}
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">{{$key}}</label>
                    <br>
                  @endforeach
                </div>
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-finaciacion w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-competencias">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-finaciacion" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-competencias">
            <b>6. Competencias</b>
          </div>

          <div id="input-competencias">

            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>6.1. Competencias</b></h4>    
                </div>
              </div>
            </div>
            

            <div class="row">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}"> 
                  Por favor califique su nivel de satisfacción con relación a la formación en las siguientes competencias:
                </label>
              </div>
            </div>

            <div class="row" style="margin-top: 15px">             
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Cultura de Convivencia<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_culcon', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                   
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Liderazgo<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_lider', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Trabajo en equipo<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_traequi', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                   
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Manejo del discurso<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_manedisc', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Capacidad de analisis y critica<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_capanacri', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                   
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Comunicación verbal y no verbal<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_comuvernover', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Adaptabilidad al cambio<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_adapca', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Creatividad e innovación<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_creinno', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Toma de decisiones<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_todeci', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Investigación<span style="color: red">*</span></label>
                 {!! Form::select('satisfacion_inves', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="row" style="margin-top: 20px">
              <label class="w3-text-{{$sistema->sistema_colorclass}}">
                ¿Cómo influyó la universidad en la mejora de sus competencias en el idioma extranjero inglés?
                <span style="color: red">*</span>
              </label>
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  A nivel de habla
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('satisfacion_inhabla', 
                                  $satisfacion_idioma, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  A nivel de escucha
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('satisfacion_inescucha', 
                                  $satisfacion_idioma, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  A nivel de lectura
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('satisfacion_inlectura', 
                                  $satisfacion_idioma, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  A nivel de escritura
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('satisfacion_inescritura', 
                                  $satisfacion_idioma, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>6.2. Plan de vida</b></h4>    
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Qué ha pensado hacer a largo plazo?
                  <span style="color: red">*</span>
                </label>
              </div>  
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-5">

                @foreach($planes_vida as $plan_vida => $key)
                  {{ Form::checkbox('cs_plan[]', $plan_vida, false, ['required' => 'true', 'class' => 'input-checkbox']) }}
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">
                    {{$key}}
                  </label>
                  <br>
                @endforeach
                
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>6.3. Emprendimiento</b></h4>    
                </div>
              </div>

               <div class="row">
              <div class="col-sm-2">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Tiene Empresa?</label><span style="color: red">*</span><br>
                   {!! Form::select('empresa', 
                      $sino, 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey show-form-em input-select', 
                      'placeholder' => 'Seleccionar' , 
                      'id' => 'empresa', 
                      'required' => 'true']) !!}
                </div>
              </div>
                
              <div id="form-emprendimiento" hidden>
                <div class="row" style="margin-top: 10px;">
                  <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                               {!! Form::text('nombre_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em input-text', 'placeholder' => 'Nombre Empresa', 'disabled']) !!}
                  </div>
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Direccion de la empresa<span style="color: red">*</span></label>
                               {!! Form::text('direccion_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em input-text','placeholder' => 'Dirrecion Empresa', 'disabled']) !!}
                  </div>
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono de la empresa<span style="color: red">*</span></label>
                               {!! Form::text('telefono_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em input-text input-number', 'placeholder' => 'Telefono Empresa', 'disabled']) !!}
                  </div>
                </div>
                
                <div class="row" style="margin-top: 10px;">
                    <div class="col-sm-2">
                          <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                         {!! Form::select('ciudad_em', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em input-select', 'placeholder' => 'Seleccionar', 'disabled']) !!}
                    </div>

                    <div class="col-sm-4">
                          <label class="w3-text-{{$sistema->sistema_colorclass}}">Sector empresa<span style="color: red">*</span></label>
                         {!! Form::select('sector_em', $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em input-select', 'placeholder' => 'Seleccionar', 'disabled']) !!}
                    </div>

                    <div class="col-sm-4">
                          <label class="w3-text-{{$sistema->sistema_colorclass}}">Tamaño empresa<span style="color: red">*</span></label>
                         {!! Form::select('tamano_em', $tamanoEmpresa->pluck('tamanoempresa_nombre', 'tamanoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em input-select', 'placeholder' => 'Seleccionar', 'disabled']) !!}
                    </div>
                </div>  
              </div>

              <div id="form-emprendimiento-plan" hidden="" style="margin-top: 10px">

                <div class="row">
                  <div class="col-sm-12">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">
                      Dentro de su plan de vida, tiene como proyecto Crear una Empresa
                    </label>
                    <span style="color: red">*</span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-2">
                     {!! Form::select('empresa_idea', 
                                  $sino, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' , 
                                  'id' => 'empresa_idea', 
                                  'required']) !!}
                  </div>
                </div>
              </div>

              <div id="form-idea-company" hidden="" style="margin-top: 10px">

                <div class="row">
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">
                      Idea de negocio
                    </label>
                    <span style="color: red">*</span>
                    {!! Form::Text('idea_company', 
                                    null, 
                                    ['class' => 'w3-input w3-text-grey input-idea-company input-text', 
                                    'placeholder' => 'Idea de negocio']) !!}

                  </div>
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">
                      Sector de empresa
                    </label>
                    <span style="color: red">*</span>
                    {!! Form::Select('idea_company_sector', 
                                    $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'),
                                    null, 
                                    ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                    'placeholder' => 'Seleccionar']) !!}

                  </div>
                </div>
              </div>

              <div class="row" style="margin-top: 20px">
                <div class="col-sm-12">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">
                      ¿Cuál considera que es la principal dificultad en la creación de empresa?
                    </label>
                    <span style="color: red">*</span>
                </div>
              </div>

              <div class="row" style="margin-top: 10px">
                <div class="col-sm-12">
                  @foreach($dificultades as $dificultad => $key)
                    {{ Form::radio('dificultad_crear_empresa', $key, false, ['class' => 'input-radio', 'required']) }}
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">
                      {{$dificultad}}
                    </label>
                    <br>
                  @endforeach
                </div>
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-competencias w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-laboral">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-competencias" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>
          


        <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-laboral"> 
          <b>7. Laboral</b>
        </div>

        <div id="input-laboral">

          <div class="row">
            <div class="col-sm-6">
              
              <label class="w3-text-{{$sistema->sistema_colorclass}}">
                ¿Durante su proceso de formación usted trabajo?
                <span style="color: red">*</span>
              </label>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-2">
              {!! Form::select('proceso_formacion_trabajo', 
                      $sino, 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select', 
                      'placeholder' => 'Seleccionar' , 
                      'id' => 'proceso-formacion-trabajo', 
                      'required' => 'true']) !!}
            </div>
          </div>

          <div id="form-situacion-laboral-2" hidden style="margin-top: 10px">
            <div class="row">
              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Busca trabajo actualmente?
                  <span style="color: red">*</span>
                </label>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-2">
                {!! Form::select('busca_trabajo', 
                        $sino, 
                        null, 
                        ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select', 
                        'placeholder' => 'Seleccionar' , 
                        'id' => 'busca-trabajo', 
                        'disabled']) !!}
              </div>
            </div>
          </div>

          <div id="form-situacion-laboral-2-1" hidden="" style="margin-top: 10px">
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Busca trabajo por primera vez?
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('busca_trabajo_primeVez', 
                      $sino, 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select input-busca-trabajo', 
                      'placeholder' => 'Seleccionar' ,  
                      'disabled']) !!}
              </div>

              <div class="col-sm-7">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cuál considera la principal dificultad a la hora de conseguir el trabajo que busca?
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('dificultad_trabajo', 
                      ['no_trabajo_disponible' => 'No hay trabajo disponible en la ciudad donde vive',
                       'no_saber_buscar' => 'No sabe como buscarlo',
                       'no_encuetra_trabajo_profesion' => 'No encuentra el trabajo apropiado en su oficio o profesion',
                       'carece_experiencia' => 'Carece de la experiencia necesaria',
                       'muy_joven' => 'Los empleadores lo ven muy joven',
                       'carece_competencias' => 'Carece de las competencias requeridas',
                       'salario_bajo' => 'El salario que le ofrecen es muy bajo',
                       'otro' => 'Otro'], 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select input-busca-trabajo', 
                      'placeholder' => 'Seleccionar' , 
                      'id' => 'busca-trabajo', 
                      'disabled']) !!}
              </div>

            </div>

            <div class="row" style="margin-top: 10px">

              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cuantos meses ha estado buscando trabajo?
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('buscando_trabajo', 
                      ['1' => 'Un mes',
                       '2' => 'Dos meses',
                       '3' => 'Tres meses',
                       '4' => 'Cuatro meses',
                       '5' => 'Cinco meses',
                       '6' => 'Seis meses',
                       '7' => 'Siete meses',
                       '8' => 'Ocho meses',
                       '9' => 'Nueve meses',
                       '10' => 'Diez meses',
                       '11' => 'Once meses',
                       '12' => 'Doce meses',
                       'no_aplica' => 'No aplica',], 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select input-busca-trabajo', 
                      'placeholder' => 'Seleccionar' , 
                      'id' => 'busca-trabajo', 
                      'disabled']) !!}
              </div>

              <div class="col-sm-7">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cuál es el canal de búsqueda de empleo que considera podría ser el más efectivo?
                  <span style="color: red">*</span>
                </label>
                {!! Form::select('canalbusqueda_trabajo', 
                      ['medios_comunicacion' => 'Medios de comunicacion',
                       'bolsa_empleo_universidad' => 'Bolsa de empleo de la universidad',
                       'otra_bolsa_empleo' => 'Otras bolsas de empleo',
                       'redes_sociales' => 'Redes sociales',
                       'otro' => 'Otro'], 
                      null, 
                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey  input-select input-busca-trabajo', 
                      'placeholder' => 'Seleccionar' , 
                      'id' => 'busca-trabajo', 
                      'disabled']) !!}
              </div>

            </div>
          </div>

          <div id="form-situacion-laboral-3" hidden style="margin-top: 10px">
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Situacion Laboral<span style="color: red">*</span></label>
                 {!! Form::select('laboral_situacion', $situacionLaboral->pluck('situacionlaboral_nombre', 'situacionlaboral_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey situacion-laboral-form input-select', 'placeholder' => 'Seleccionar', 'id' => 'situacionlaboral', 
                        'required' => 'true']) !!}
              </div>
            </div>  
          </div>
          
            <br>
            <div id="form-laboral" hidden="">

              <div class="row">
                <div class="col-sm-12">
                  <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                    <h4><b>Datos de la empresa</b></h4>    
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                       <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                       {!! Form::text('name_empresa', null, ['class' => 'w3-input w3-text-grey habilitar input-text', 'placeholder' => 'Nombre Empresa', 'id' => 'codigo', 'disabled', 
                        'required' => 'true']) !!}
                </div>
              
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Tipo Empresa<span style="color: red">*</span></label>
                 {!! Form::select('tipoEmpresa', $tipoEmpresa->pluck('tipoempresa_nombre', 'tipoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div>
                  <div class="col-sm-2">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                   {!! Form::select('ciudad', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                  </div>
                </div>
              </div>

             <div class="row">
                <div class="col-sm-12">
                  <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                    <h4><b>Datos laborales</b></h4>    
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Area<span style="color: red">*</span></label>
                 {!! Form::select('area', $areaPertenece->pluck('areapertenece_nombre', 'areapertenece_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Cargo<span style="color: red">*</span></label>
                 {!! Form::select('cargo', $cargo->pluck('nivelcargo_nombre', 'nivelcargo_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Relacion con el Programa<span style="color: red">*</span></label>
                 {!! Form::select('relacionPrograma', $relacionPrograma->pluck('relacionprograma_nombre', 'relacionprograma_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
              </div>

              <div class="row" style="margin-top: 10px;">
                <div class="col-sm-6">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Ocupacion<span style="color: red">*</span></label>
                 {!! Form::select('ocupacion', $ocupacion->pluck('ocupacion_nombre', 'ocupacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Tiempo<span style="color: red">*</span></label>
                 {!! Form::select('duracionEmpresa', $duracionEmpresa->pluck('duracionempresa_nombre', 'duracionempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
              </div>
               {{-- <div class="col-sm-2">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Pais<span style="color: red">*</span></label>
                 {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar']) !!}
                </div>--}}

              <br>
              <div class="row">
                <div class="col-sm-12">
                  <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                    <h4><b>Contratacion</b></h4>    
                  </div>
                </div>
              </div>
              <div class="row">
                 <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Vinculacion<span style="color: red">*</span></label>
                 {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Contrato<span style="color: red">*</span></label>
                 {!! Form::select('contrato', $contrato->pluck('contrato_nombre', 'contrato_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder input-select' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
                <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Rango Salarial<span style="color: red">*</span></label>
                 {!! Form::select('salario', $rangoSalarial->pluck('rangosalario_nombre', 'rangosalario_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar input-select', 'placeholder' => 'Seleccionar', 'disabled', 
                        'required' => 'true']) !!}
                </div>
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-laboral w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-identidad">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-laboral" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

        </div>
          
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-identidad">
            <b>8. Nivel Indentidad</b>
          </div>

          <div id="input-identidad">
            <div class="row">
              <div class="col-sm-9">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                    ¿Si tuviera la oportunidad de cursar de nuevo sus estudios de pregrado y/o posgrados volvería a estudiar aquí?
                </label>
                <span style="color: red">*</span>
              </div>
              <div class="col-sm-2">
                {!! Form::select('volveria_estudiar', 
                                  $sino, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar', 
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="row" style="margin-top: 10px">

              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cuál sería la principal razón para querer volver a esta institución?
                </label>
                <span style="color: red">*</span>
                {!! Form::select('razon_no_estudiar', 
                                  ['calidad_formacion' => 'Calidad de la formacion',
                                    'calidad_profesores' => 'Calidad de los profesores',
                                   'reconocimineto_institucion' => 'Reconocimineto de la institucion',
                                   'fundamentacion_empresa' => 'Fundamentacion para crear empresa',
                                   'recursos_apoyo' => 'Los recursos de apoyo al proceso de formacion',
                                   'posibilidad_encontrar_empleo' => 'Posibilidad de encontrar empleo rapidamente',
                                   'otra' => 'Otras',
                                   'no_aplica' => 'No Aplica'], 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar', 
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cual sería la principal razón para <b>NO</b> querer volver a esta institucion?
                </label>
                <span style="color: red">*</span>
                {!! Form::select('razon_si_estudiar', 
                                  ['calidad_formacion' => 'Calidad de la formacion',
                                   'calidad_profesores' => 'Calidad de los profesores',
                                   'fundamentacion_empresa' => 'Fundamentacion para crear empresa',
                                   'recursos_apoyo' => 'Los recursos de apoyo al proceso de formacion',
                                   'posibilidad_encontrar_empleo' => 'Posibilidad de encontrar empleo rapidamente',
                                   'otra' => 'Otras',
                                   'no_aplica' => 'No Aplica'], 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select',
                                   'id' => 'razon-no-estudiar',
                                   'placeholder' => 'Seleccionar', 
                                   'required' => 'true']) !!}
              </div>
            </div>

            <div id="form-other-study" style="margin-top: 10px" hidden="">

              <label class="w3-text-{{$sistema->sistema_colorclass}}">Razon</label><span style="color: red">*</span>
              <div class="row">
                <div class="col-sm-12">
                  {{ Form::textarea('razon_other_study', 
                                     null, 
                                     ['class' => 'w3-input w3-border-2017-navy-peony input-textarea',
                                      'disabled',
                                      'id' => 'textarea-razon']) }}
                </div>
              </div>
              
            </div>

            <div class="row" style="margin-top: 15px">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿En el futuro, le gustaría cursar otros estudios en esta institución?
                </label>
                <span style="color: red">*</span>
              </div>
              <div class="col-sm-12" style="margin-top: 10px">
                @foreach($cursos as $curso => $key)
                  {{ Form::checkbox('otros_estudios[]', $curso, false,['required' => 'true', 'class' => 'input-checkbox']) }}
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">{{$key}}</label>
                  <br>
                @endforeach
              </div>
            </div>

            <div class="row" style="margin-top: 15px">
              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Usted recomendaría a un familiar o amigo estudiar en UNICATÓLICA?
                </label>
              </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                  {!! Form::select('recomendar', 
                                 $sino, 
                                 null, 
                                 ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                 'placeholder' => 'Seleccionar',
                                 'id' => 'recomendar', 
                                 'required' => 'true']) !!}
                </div>
            </div>

            <div id="form-razon-recomendar" style="margin-top: 10px" hidden="">

              <label class="w3-text-{{$sistema->sistema_colorclass}}">Razon</label><span style="color: red">*</span>
              <div class="row">
                <div class="col-sm-12">
                  {{ Form::textarea('razon_no_recomendar', 
                                     null, 
                                     ['class' => 'w3-input w3-border-2017-navy-peony input-textarea',
                                      'disabled',
                                      'id' => 'razon-no-recomendar']) }}
                </div>
              </div>
              
            </div>

            <div class="row" style="margin-top: 10px">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  ¿Cuáles areas de apoyo aportaron a su nivel de identidad universitaria?
                </label>
                <span style="color: red">*</span>
              </div>

              <div class="col-sm-12" style="margin-top: 10px">
                @foreach($apoyos as $apoyo => $key)
                  {{ Form::checkbox('eapoyo[]', $key, false,['required' => 'true', 'class' => 'input-checkbox']) }}
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">{{$apoyo}}</label>
                  <br>
                @endforeach
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-identidad w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-satisfacion">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-identidad" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-satisfacion">
            <b>9. Satisfacion</b>
          </div>

          <div id="input-satisfacion">
            <div class="row">
              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Salones de clase
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_salones', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Laboratorios y talleres
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_talleres', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Espacios para estudiar
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_espacios', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Ayudas audiovisuales
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_audiovisual', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="row" style="margin-top: 10px">

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Aulas de informática
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_aulas_informatica', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Espacios práctica deportiva
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_practica_deportiva', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Redes sociales
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_redes_sociales', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Biblioteca
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_biblioteca', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

            </div>

            <div class="row" style="margin-top: 10px">

              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Medios de comunicación institucionales
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_medio_comunicacion', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Espacios para realizar actividades artísticas/culturales
                </label>
                <span style="color: red">*</span>

                {!! Form::select('satisfacion_actividades_artisticas', 
                                  $satisfacion_competencias, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar' ,  
                                  'required' => 'true']) !!}
              </div>

            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-satisfacion w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-estudios">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-satisfacion" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-estudios">
            <b>10. Otros estudios</b>
          </div>

          <div id="input-estudios">
            <div class="row">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">¿Ha realizado otros estudios?</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2"  style="margin-bottom: 10px">
                  
                 {!! Form::select('estudio', 
                                   $sino, 
                                   null, 
                                   ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey show-form-es input-select', 
                                   'placeholder' => 'Seleccionar', 
                                   'required' => 'true']) !!}         
              </div>
            </div>
              <div id="form-estudio" hidden="">
                <div class="row" style="margin-bottom: 10px">
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Nivel academico<span style="color: red">*</span></label>
                    {!! Form::select('estudio_nivel_estudiante', $nivel->pluck('nivelacademico_nombre', 'nivelacademico_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_es input-select', 'placeholder' => 'Seleccionar Uno', 'disabled' => 'true']) !!}
                  </div>
                  <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la institucion<span style="color: red">*</span></label>
                    <input type="text" name="estudio_nombreInstitucion_estudiante"  class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" maxlength="100" placeholder="Nombre de la institucion" disabled="true">
                  </div>
                  <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Titulo obtenido<span style="color: red">*</span></label>
                    <input type="text" name="estudio_titulo_estudiante" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" maxlength="50" placeholder="Titulo Obtenido" disabled="true">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha inicio<span style="color: red">*</span></label>
                    <input type="date" name="estudio_inicio_estudiante" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" disabled="true">
                  </div>
                  <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha fin<span style="color: red">*</span></label>
                    <input type="date" name="estudio_fin_estudiante" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm habilitar_es" disabled="true">
                  </div>
                </div>
              </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-estudios w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-reconocimientos">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-estudios" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-reconocimientos">
            <b>11. Reconocimientos</b>
          </div>

          <div id="input-reconocimientos">
            <div class="row">
              <div class="col-sm-4">  
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre reconocimiento</label>
                         {!! Form::text('reconocimiento_nombre', null, ['class' => 'w3-input w3-text-grey input-text', 'placeholder' => 'Nombre reconocimiento']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa</label>
                         {!! Form::text('reconocimiento_organizacion', null, ['class' => 'w3-input w3-text-grey input-text', 'placeholder' => 'Nombre de la epresa']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha de reconocimiento</label>
                <input type="date" name="reconocimiento_fecha" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm inicio">
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-reconocimientos w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="section-seguimiento">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            <div id="click-next-input-reconocimientos" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>

          </div>

          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}" style="display: none" id="section-seguimiento">
            <b>12. Datos seguimiento</b>
          </div>

          <div id="input-seguimiento">
            <div class="row">
              <div class="col-sm-12">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  A continuación le solicitamos su colaboración para que nos suministre los datos de un familiar o amigo, que solo se utilizaran en el evento en que se presenten dificultades para lograr el contacto con usted. Los datos serán mantenidos bajo promesa de confidencialidad
                </label>
              </div>
            </div>
            <div class="row" style="margin-top: 15px">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Nombres y apellidos
                </label>
                <span style="color: red">*</span>

                {!! Form::text('referencia_nombre', 
                                null, 
                                ['class' => 'w3-input w3-text-grey input-text', 
                                'placeholder' => 'Nombres y apellidos',  
                                'required' => 'true']) !!}
              </div>

              <div class="col-sm-2">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Parentesco
                </label>
                <span style="color: red">*</span>

                {!! Form::select('referencia_parentesco', 
                                  $parentesco, 
                                  null, 
                                  ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-select', 
                                  'placeholder' => 'Seleccionar',  
                                  'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Teléfono fijo
                </label>
                <span style="color: red">*</span>

                {!! Form::text('referencia_telefono', 
                                null, 
                                ['class' => 'w3-input w3-text-grey input-text input-number', 
                                'placeholder' => 'Telefono fijo',  
                                'required' => 'true']) !!}
              </div>

              <div class="col-sm-3">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">
                    Celular
                  </label>
                  <span style="color: red">*</span>

                  {!! Form::text('referencia_celular', 
                                  null, 
                                  ['class' => 'w3-input w3-text-grey input-text input-number', 
                                  'placeholder' => 'Celular',  
                                  'required' => 'true']) !!}
              </div>
            </div>

            <div class="w3-center" style="margin-top: 20px">
            
              <label class="input-seguimiento w3-btn w3-{{$sistema->sistema_colorclass}} button-sections-survey" id="finish">

                <span class="fa fa-check fa-fw"></span>
                  Siguiente
              </label>

            </div>

            {{--<div id="click-next-input-seguimiento" class="w3-center" hidden="">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">
                  Por favor da click abajo
                  <span class="fa fa-level-down"></span>
                </label>
            </div>--}}

          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-sm-12">
            <div class="w3-right">
              
              <button class="w3-btn w3-{{$sistema->sistema_colorclass}}" 
                            disabled="" 
                            title="Asegurate de completar todos los campos de la encuesta"
                            id="save-form">

                <span class="fa fa-save fa-fw"></span>
                  Guardar
              </button>
            </div>
          </div>
          
        </div>

    {!!Form::close()!!}
  @else 

    <div class="w3-container">
      <div class="row">
        <div class="w3-center">

          <h3 class="w3-text-grey">La encuesta ha sido realizada satisfactoriamente</h3>
          <a class="w3-btn w3-{{$sistema->sistema_colorclass}}" href="{{ route('estudiantes.egresados.pdf', $egresado->matriculado_pidm) }}">
            Descargar Certificado
          </a> 

        </div>
      </div>
    </div>
    

  @endif
@endif
</div>
@endsection
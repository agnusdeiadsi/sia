{{--dd($matriculado->academico->first()->programa_id)--}}
<img src="images/logo_lastest.png">
<style>

	table 
	{
	    border-collapse: collapse;
	    width: 100%;
	}

	th, td 
	{
	    padding: 8px;
	    text-align: left;
	    border-bottom: 1px solid #ddd;
	}

	body
	{
		width: 100%;
		height:225px;
		filter:alpha(opacity=70);
		-moz-opacity:.700;
		opacity:.70;
		/*background-image: url('../images/Logo-Color-Vertical.png')*/
	}
</style>
<body>
	<table>
		<thead>
			<tr>
				<th>
					<b>Cedula</b>
				</th>
				<th>
					<b>Nombres</b>
				</th>
				<th>
					<b>Apellidos</b>
				</th>
				<th>
					<b>Telefono</b>
				</th>
				<th>
					<b>Celular</b>
				</th>
				<th>
					<b>Programa</b>
				</th>
				<th>
					<b>Sede</b>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					{{$matriculado->identificaciones->first()->identificacion_numero}}
				</td>
				<td>
					{{$matriculado->matriculado_primer_nombre.' '.$matriculado->matriculado_segundo_nombre}}
				</td>
				<td>
					{{$matriculado->matriculado_primer_apellido.' '.$matriculado->matriculado_segundo_apellido}}
				</td>
				<td>
					{{$matriculado->matriculado_telefono}}
				</td>
				<td>
					{{$matriculado->matriculado_celular}}
				</td>
				<td>
					{{$programa->first()->programa_nombre}}
				</td>
				<td>
					
				</td>
			</tr>
		</tbody>
	</table>

	<h1 align="center">La encuesta ha sido realizada satisfactoriamente, agradecemos tu participación</h1>
</body>


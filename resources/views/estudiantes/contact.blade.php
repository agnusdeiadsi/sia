@extends('estudiantes.layout.default')

@section('title-estudiantes')
  Contacto
@endsection

@section('content-estudiantes')
    <div class="w3-container">
      <div class="w3-section w3-bottombar w3-padding-16">
        <h1><b><span class="fa fa-envelope fa-fw"></span> Contacto</b></h1>
      </div>
    </div>

    <!-- Contact Section -->
    <div class="w3-container w3-padding-large">
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
        <p>soportesia@unicatolica.edu.co</p>
      </div>
      <div class="w3-third w3-teal">
        <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
        <p>Pance, Cali, Valle</p>
      </div>
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
        <p>+57 2 3120038 ext 1052</p>
      </div>
    </div>
    <hr class="w3-opacity">
    <form action="/action_page.php" target="_blank">
      <div class="w3-section">
        <label>Nombres</label>
        <input class="w3-input w3-border" type="text" name="Name" value="{{Auth::guard('estudiante')->user()->matriculado_nombres}}" required>
      </div>
      <div class="w3-section">
        <label>Email</label>
        <input class="w3-input w3-border" type="text" name="Email" value="{{Auth::guard('estudiante')->user()->matriculado_email}}" required>
      </div>
      <div class="w3-section">
        <label>Mensaje</label>
        <textarea class="w3-input w3-border" type="text" name="Message" maxlength="2000" rows="10" placeholder="Mensaje (2.000 caracteres)" required></textarea>
      </div>
      <button type="submit" class="w3-button w3-2017-navy-peony w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Enviar</button>
    </form>
    </div>
    <br>
@endsection
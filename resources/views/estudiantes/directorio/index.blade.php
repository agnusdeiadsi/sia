@extends('estudiantes.layout.default')

@section('title-estudiantes')
  {{$sistema->sistema_nombre}} | Extensiones
@endsection

@section('content-estudiantes')
  
  <!-- breadcrumbs -->
  <div class="w3-container"><br>
    <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
      <li><a href="{{ url()->previous() }}">{{$sistema->sistema_nombre}}</a></li>
      <li class="active"><span>Extensiones</span></li>
    </ol>
  </div>
  <!-- fin breadcrumbs -->

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    <div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['estudiantes.directorio', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small w3-hide-medium">
      {!! Form::open(['route' => ['estudiantes.directorio', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>

  <div class="w3-responsive">
    <table class="w3-table-all w3-hoverable">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>Extensión</th>
          <th>Nombres</th>
          <th>Email</th>
          <th>Área</th>
          <th>Subarea</th>
        </tr>
      </thead>
      <tbody>
        @if($extensiones->count() == 0)
          <tr>
            <td colspan="7"><i>No hay registros</i></td>
          </tr>
        @else
          @foreach($extensiones as $extension)
            <tr>
              <td>
                {{$extension->extension_extension}}
              </td>
              <td>
                {{$extension->cuenta_primer_nombre.' '.$extension->cuenta_segundo_nombre.' '.$extension->cuenta_primer_apellido.' '.$extension->cuenta_segundo_apellido}}
              </td>
              <td>
                {{$extension->cuenta_cuenta}}
              </td>
              <td>
                {{$extension->area_nombre}}
              </td>
              <td>
                {{$extension->subarea_nombre}}
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
  
  <iframe name="open_call" id="open_call" hidden></iframe>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>
@endsection
@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}}
@endsection

@section('content-estudiantes')
	@php
		extract($_REQUEST);
	@endphp

	@if($solicitud != null)
		<!-- breadcrumbs -->
		<div class="w3-container"><br>
			<ol class="breadcrumb breadcrumb-arrow">
				<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
				<li><a href="{{ url()->previous() }}">Certificados</a></li>
				<li class="active"><span>Consultar Solicitud</span></li>
			</ol>
		</div>
		<!-- fin breadcrumbs -->

		<div class="row">
			<div class="col-sm-12 text-center">
				<h2><b>Solicitud de<br>Certificados Académicos y Financieros<br>N° {{$solicitud->solicitud_id}}</b></h2>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos de la Solicitud</b></h4>
        		</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>ID Solicitud <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="ID de la Solicitud."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Fecha Radicado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fecha en la que se realizó la solicitud."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Estado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="{{$solicitud->estado->estado_descripcion}}"><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Liquidación Banner <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código de la factura de pago de Banner."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->solicitud_codigo_liquidacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Comprobante Liquidación <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Recibo escaneado en formato PDF y no mayor a 2 Mb (2048 kilobytes)."><span class="fa fa-question-circle fa-fw"></span></a></label>
					@if($solicitud->solicitud_comprobante_liquidacion != null)
						<br><a href="{{url('estudiantes/certificados/solicitudes/download/receipt', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar</a>
					@else
						<br><i class="w3-grey w3-padding">La solicitud no tiene comprobante.</i>
					@endif
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label>Formulario <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código del Formulario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_formulario', $solicitud->solicitud_codigo_formulario, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
				<label>Versión <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Versión del Formulario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_version_formulario', $solicitud->solicitud_version_formulario, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Versión Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Datos del Estudiante</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Documento <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de documento del usuario: CC, TI, PS, CE."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_tipo_documento', $solicitud->solicitud_matriculado_tipo_documento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Tipo documento', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Documento <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Número del documento de identidad."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_documento', $solicitud->solicitud_matriculado_documento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Documento', 'maxlength' => '20', 'pattern' => '[0-9]{7,20}', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Código* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código del usuario registrado en Banner."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('matriculado_id', $solicitud->solicitud_codigo_liquidacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código Estudiante', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<label>Nombres* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Nombres y apellidos del usuario registrados."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_nombres', $solicitud->solicitud_matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'required', 'readonly'])}}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Fecha Nacimiento* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fecha de nacimiento del usuario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::date('solicitud_matriculado_fecha_nacimiento', $solicitud->solicitud_matriculado_fecha_nacimiento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha de Nacimiento', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Correo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Correo electrónico del usuario donde se enviará los datos de la solicitud creada."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::email('solicitud_matriculado_email', $solicitud->solicitud_matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Email', 'maxlength' => '100', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Teléfono Fijo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Teléfono fijo, o celular en el caso de no contar con uno. No usar espacios. De 7 a 10 dígitos."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_telefono', $solicitud->solicitud_matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono Fijo', 'maxlength' => '10', 'pattern' => '[0-9]{7,10}', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Celular <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Teléfono celular (Opcional). No usar espacios entre los números. 10 dígitos."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_celular', $solicitud->solicitud_matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono Celular', 'pattern' => '[0-9]{10,10}', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Programa* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Programa en el que el estudiante está matriculado y/o desea el certifiado sea realizado."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_programa', $solicitud->solicitud_matriculado_programa, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Semestre (Actual)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Semestre que se certificará."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_semestre', $solicitud->semestre->semestre_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Estudiante <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de estudiante según Banner Student."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('tipoestudiante_id', $solicitud->solicitud_matriculado_tipo_estudiante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>3. Información del Certificado</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Certificado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de certificado que se realizará. Seleccione el tipo de certificado y haga clic en el icono del ojo para ver el modelo."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('tipo_nombre', $solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}

					{{Form::hidden('tipo_id', $solicitud->tipo->tipo_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'id' => 'tipo_id', 'onchange' => 'cargarCertificado(this.value); displayNewInputs(this.value);', 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>

			<div class="col-sm-4">
				{{--New inputs--}}
				<div class="row periodo" style="display: none;">
					<div class="col-sm-12">
						<label class="periodo" style="display: none;">Periodo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Periodo del certificado. Ejemplo: 201610."><span class="fa fa-question-circle fa-fw"></span></a></label>
						{{Form::text('periodo_id', $solicitud->periodo_id, ['class' => 'w3-input periodo', 'maxlength' => '6', 'pattern' => '[0-9]{4,6}', 'placeholder' => 'Periodo', 'readonly', 'disabled', 'style' => 'display:none;'])}}
					</div>
				</div>

				<div class="row pension" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Valor ($)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Valor en COP$"><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input w3-border-{{$sistema->sistema_colorclass}} pension" name="solicitud_pension_valor" maxlength="10" placeholder="Digite el valor a certificar" value="{{$solicitud->solicitud_pension_valor}}" readonly disabled style="display: none;">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Entidad* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Nombre completo de la entidad a quien va dirigido el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} pension" name="solicitud_pension_entidad" maxlength="100" placeholder="Nombre Entidad Solicitante" value="{{$solicitud->solicitud_pension_entidad}}" readonly disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row declaracion" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="declaracion" style="display: none;">Año* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Año para el cual se va a realizar la declaración de renta."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input w3-border-{{$sistema->sistema_colorclass}} declaracion" name="solicitud_anio_declaracion" maxlength="4" placeholder="Año declaración" value="{{$solicitud->solicitud_anio_declaracion}}" readonly disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row materias" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="materias" style="display: none;">Especifíque la(s) Materia(s)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Liste los nombres de las materias que debe mencionar el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<textarea class="w3-input w3-border-{{$sistema->sistema_colorclass}} materias" name="materia_nombre[]" rows="10" maxlength="100" placeholder="Especifíque la(s) Materia(s). (1000 caracteres)" readonly disabled style="display: none;">@foreach($solicitud->materias as $materia){{$materia->materia_nombre}}@endforeach
							</textarea>
						</div>
					</div>
				</div>

				<div class="row carnet" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Foto* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fotografía 3x4, fondo blanco. Subir en formato jpg o png, no mayor a 5 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							@if($solicitud->solicitud_carnet_foto != null)
								<a href="{{url('estudiantes/certificados/solicitudes/download/photo', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-hover-text-white w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar Foto</a>
							@endif
						</div>
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Fotocopia Documento* (150%) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Documento de identidad escaneado en formato PDF, no mayor a 2 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							@if($solicitud->solicitud_carnet_documento != null)
								<a href="{{url('estudiantes/certificados/solicitudes/download/document', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-hover-text-white w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar Documento</a>
							@endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
						            <div class="form-group">
						            	<label class="carnet" style="display: none;">URL Denuncio por Pérdida del Carnet* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="URL del denuncio en la página de la policia https://webrp.policia.gov.co:444/publico/constancia.aspx. El link debe estar acortado por: https://goo.gl/."><span class="fa fa-question-circle fa-fw"></span></a></label>
						            	<input class="w3-input carnet" name="solicitud_carnet_denuncio" id="denuncio_url" maxlength="50" placeholder="URL del denuncio del carnet. 20 caracteres" value="{{$solicitud->solicitud_carnet_denuncio}}" readonly disabled style="display: none;"><div id="denuncio_caracteres"></div>
						            </div>
						        </div>
							</div>
						</div>
					</div>					
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Sede reclamo <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Sede donde se reclamará el certificado una vez haya sido realizado."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('sede_id', $solicitud->sede->sede_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label>Observaciones <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Observaciones adicionales a los campos predeterminados del formulario de solicitud."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::textarea('solicitud_observaciones', $solicitud->solicitud_observaciones, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Obervaciones (2.000 caracteres)', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>4. Movimientos de la Solicitud</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				@if($solicitud->usuarioimprime != null)
				<div class="form-group">
					<label>Impreso por <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Impreso en {{$solicitud->solicitud_fecha_impresion}}."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_usuario_imprime', $solicitud->usuarioimprime->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Impreso por', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
				@endif
			</div>
			<div class="col-sm-4">
				@if($solicitud->usuariorecibe != null)
				<div class="form-group">
					<label>Recibido por (Sede) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Recibido en Sede en {{$solicitud->solicitud_fecha_recibido}}."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_usuario_recibe', $solicitud->usuariorecibe->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
				@endif
			</div>
			<div class="col-sm-4">
				@if($solicitud->usuarioentrega != null)
				<div class="form-group">
					<label>Entregado por <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Entregado en {{$solicitud->solicitud_fecha_entrega}}"><span class="fa fa-question-circle fa-fw"></span></a></label><a href="javascript:void(0);" class="w3-bar-item" data-toggle="modal" data-target="#observaciones_entrega"><span class="fa fa-plus-square fa-fw"></span>Info</a>
					@if($solicitud->estado_id == 4)
						{{Form::text('solicitud_usuario_entrega', $solicitud->usuarioentrega->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
					@endif
				</div>
				@endif
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-rightbar w3-border-{{$sistema->sistema_colorclass}} text-justify">
			<p>
			Ten en cuenta las siguientes recomendaciones a la hora de solicitar cualquier trámite en Registro Académico:
				<ul>
					<li>
						Si no puedes solicitar o reclamar certificados personalmente, debes presentar una carta de autorización debidamente firmada y copia del documento de identidad de la persona que autoriza.
					</li> 
					<li>
						Para reclamar cualquier factura, debes identificarte como estudiante de UNICATÓLICA a través del carnet (actualizado).
					</li>
				</ul>
			</p>
		</div>

		<div class="w3-center">
			<a href="{{route('estudiantes.certificados', $sistema)}}" class="w3-bar-item w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>
		</div>
			<!-- Modal -->
			<div id="observaciones_entrega" class="modal fade w3-animate-zoom" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"><span class="fa fa-files-o fa-fw"></span> Detalles de la Entrega</h4>
			      </div>
			      <div class="modal-body">
			      	<p>
			      		<label>Documento Reclamante*</label>
			        	{!! Form::text('solicitud_reclamante_documento', $solicitud->solicitud_reclamante_documento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'maxlength' => '20', 'placeholder' => 'Documento del reclamante', 'required', 'readonly']) !!}
			       	</p>

			       	<p>
			       		<label>Nombres Reclamante*</label>
			        	{!! Form::text('solicitud_reclamante_nombres', $solicitud->solicitud_reclamante_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'maxlength' => '100', 'placeholder' => 'Nombres del reclamante', 'required', 'readonly']) !!}
			        </p>

			        <p>
			       		<label>¿Carta de Autorización?*</label><br>
			        	<b>{{$solicitud->solicitud_reclamante_carta}}</b>
			        </p>

			        <p>
			       		<label>Observaciones</label>
			        	{!! Form::textarea('solicitud_observaciones_entrega', $solicitud->solicitud_observaciones_entrega, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'maxlength' => '255', 'placeholder' => 'Observaciones (255 caracteres)', 'readonly']) !!}
			        </p>
			        {!! Form::hidden('estado', 4, ['required']) !!}
			        </p>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
			      </div>
			    </div>

			  </div>
			</div>
	<br>
	@else
	<!-- Menú -->
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li class="active"><span>Certificados</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-row-padding w3-margin">
        <a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto.'.solicitudes.create', $sistema)}}">
          <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
            <h1><span class="fa fa-check-square fa-fw"></span>Crear Solicitud</h1>
          </div>
        </a>

        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display='block'" class="w3-hover-opacity">
          <div class="w3-half w3-container w3-padding-32 w3-margin-bottom w3-center w3-{{$sistema->sistema_colorclass}} w3-hover-opacity" style="height: 150px;">
            <h1><span class="fa fa-search fa-fw"></span>Consultar Solicitudes</h1>
          </div>
        </a>
    </div>

    <div class="w3-container">
		<div id="id01" class="w3-modal">
			<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

			  <div class="w3-center"><br>
			    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
			    <h3><b>Consultar Solicitudes</b></h3>
			  </div>

			  <form class="w3-container" action="{{route('estudiantes.'.$sistema->sistema_nombre_corto, $sistema)}}" method="get">
			    <div class="w3-section">
			      <label><b>Código Liquidación / Solicitud ID</b></label>
			      <input class="w3-input w3-border w3-margin-bottom" type="text" maxlength="20" placeholder="Código Liquidación o Solicitud ID" name="busqueda" required>
			      <button class="w3-button w3-block w3-{{$sistema->sistema_colorclass}} w3-section w3-padding" type="submit">Buscar</button>
			    </div>
			  </form>
			</div>
		</div>
	</div>
	@endif
@endsection
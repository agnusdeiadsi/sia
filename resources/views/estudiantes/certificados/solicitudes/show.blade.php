@extends('guest.layout.guest')

@section('title')
  Menú {{$sistema->sistema_nombre}}
@endsection

@section('content-guest')
	@php
		extract($_REQUEST);
	@endphp

	@if($solicitud != null)
		<!-- breadcrumbs -->
		<div class="w3-container"><br>
			<ol class="breadcrumb breadcrumb-arrow">
				<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
				<li><a href="{{ url()->previous() }}">Certificados</a></li>
				<li class="active"><span>Consultar Solicitud</span></li>
			</ol>
		</div>
		<!-- fin breadcrumbs -->

		<div class="row">
			<div class="col-sm-12 text-center">
				<h2><b>Solicitud<br>N° {{$solicitud->solicitud_id}}</b></h2>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos de la Solicitud</b></h4>
        		</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>ID Solicitud <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Fecha Radicado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Estado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="{{$solicitud->estado->estado_descripcion}}"><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				@if($solicitud->usuarioimprime != null)
				<div class="form-group">
					<label>Impreso por <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Impreso en {{$solicitud->solicitud_fecha_impresion}}."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->usuarioimprime->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
				@endif
			</div>
			<div class="col-sm-4">
				@if($solicitud->usuariorecibe != null)
				<div class="form-group">
					<label>Recibido por (Sede) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Recibido en Sede en {{$solicitud->solicitud_fecha_recibido}}."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->usuariorecibe->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
				@endif
			</div>
			<div class="col-sm-4">
				@if($solicitud->usuarioentrega != null)
				<div class="form-group">
					<label>Entregado por <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Entregado en {{$solicitud->solicitud_fecha_entrega}}"><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->usuarioentrega->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
				@endif
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Liquidación Banner <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', $solicitud->solicitud_codigo_liquidacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Comprobante Liquidación <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					@if($solicitud->solicitud_comprobante_liquidacion != null)
						<br><a href="{{url('estudiantes/certificados/solicitudes/download/receipt', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-hover-text-white w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar</a>
					@else
						<br><i class="w3-grey w3-padding">La solicitud no tiene comprobante.</i>
					@endif
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label>Formulario <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_formulario', $solicitud->solicitud_codigo_formulario, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
				<label>Versión <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_version_formulario', $solicitud->solicitud_version_formulario, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Versión Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos del Estudiante</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Documento <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_tipo_documento', $solicitud->solicitud_matriculado_tipo_documento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Tipo documento', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Documento <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_documento', $solicitud->solicitud_matriculado_documento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Documento', 'maxlength' => '20', 'pattern' => '[0-9]{7,20}', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Código* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('matriculado_id', $solicitud->solicitud_codigo_liquidacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código Estudiante', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<label>Nombres* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_nombres', $solicitud->solicitud_matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Nombres', 'required', 'readonly'])}}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Fecha Nacimiento* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::date('solicitud_matriculado_fecha_nacimiento', $solicitud->solicitud_matriculado_fecha_nacimiento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Fecha de Nacimiento', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required', 'readonly'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Correo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::email('solicitud_matriculado_email', $solicitud->solicitud_matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Email', 'maxlength' => '100', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Teléfono Fijo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_telefono', $solicitud->solicitud_matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Teléfono Fijo', 'maxlength' => '10', 'pattern' => '[0-9]{7,10}', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Celular <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_celular', $solicitud->solicitud_matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Teléfono Celular', 'pattern' => '[0-9]{10,10}', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Programa* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_programa', $solicitud->solicitud_matriculado_programa, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Semestre (Actual)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_semestre', $solicitud->semestre->semestre_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Estudiante <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('tipoestudiante_id', $solicitud->solicitud_matriculado_tipo_estudiante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Información del Certificado</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Certificado <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de certificado que se realizará. Seleccione el tipo de certificado y haga clic en el icono del ojo para ver el modelo."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('tipo_nombre', $solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}

					{{Form::hidden('tipo_id', $solicitud->tipo->tipo_id, ['class' => 'w3-input', 'id' => 'tipo_id', 'onchange' => 'cargarCertificado(this.value); displayNewInputs(this.value);', 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>

			<div class="col-sm-4">
				{{--New inputs--}}
				<div class="row periodo" style="display: none;">
					<div class="col-sm-12">
						<label class="periodo" style="display: none;">Periodo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Periodo del certificado. Ejemplo: 201610."><span class="fa fa-question-circle fa-fw"></span></a></label>
						{{Form::text('periodo_id', null, ['class' => 'w3-input periodo', 'maxlength' => '6', 'pattern' => '[0-9]{4,6}', 'placeholder' => 'Periodo', 'disabled', 'style' => 'display:none;'])}}
					</div>
				</div>

				<div class="row pension" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Valor ($)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Valor en COP$"><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input pension" name="solicitud_pension_valor" maxlength="10" placeholder="Digite el valor a certificar" disabled style="display: none;">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Entidad* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Nombre completo de la entidad a quien va dirigido el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="text" class="w3-input pension" name="solicitud_pension_entidad" maxlength="100" placeholder="Nombre Entidad Solicitante" disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row declaracion" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="declaracion" style="display: none;">Año* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Año para el cual se va a realizar la declaración de renta."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input declaracion" name="solicitud_anio_declaracion" maxlength="4" placeholder="Año declaración" disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row materias" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="materias" style="display: none;">Especifíque la(s) Materia(s)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Liste los nombres de las materias que debe mencionar el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<textarea class="form-control materias" name="materia_nombre[]" rows="10" maxlength="100" placeholder="Especifíque la(s) Materia(s). 1.000 caracteres" disabled style="display: none;"></textarea>
						</div>
					</div>
				</div>

				<div class="row carnet" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Foto* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fotografía 3x4, fondo blanco. Subir en formato jpg o png, no mayor a 5 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							@if($solicitud->solicitud_carnet_foto != null)
								<a href="{{url('estudiantes/certificados/solicitudes/download/photo', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-hover-text-white w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar Foto</a>
							@endif
						</div>
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Fotocopia Documento* (150%) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Documento de identidad escaneado en formato PDF, no mayor a 2 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							@if($solicitud->solicitud_carnet_documento != null)
								<a href="{{url('estudiantes/certificados/solicitudes/download/document', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-hover-text-white w3-col sm-12" style="text-decoration: none;"><span class="fa fa-download fa-fw"></span> Descargar Documento</a>
							@endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
						            <div class="form-group">
						            	<label class="carnet" style="display: none;">URL Denuncio por Pérdida del Carnet* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="URL del denuncio en la página de la policia https://webrp.policia.gov.co:444/publico/constancia.aspx. El link debe estar acortado por: https://goo.gl/."><span class="fa fa-question-circle fa-fw"></span></a></label>
						            	<input class="w3-input carnet" name="solicitud_carnet_denuncio" id="denuncio_url" maxlength="50" placeholder="URL del denuncio del carnet. 20 caracteres" value="{{$solicitud->solicitud_carnet_denuncio}}" disabled style="display: none;"><div id="denuncio_caracteres"></div>
						            </div>
						        </div>
							</div>
						</div>
					</div>					
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Sede reclamo <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('sede_id', $solicitud->sede->sede_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Seleccionar', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label>Observaciones <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::textarea('solicitud_observaciones', $solicitud->solicitud_observaciones, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Obervaciones (2.000 caracteres)', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-rightbar w3-border-{{$sistema->sistema_colorclass}} text-center">
		TENGA EN CUENTA LAS SIGUIENTES RECOMENDACIONES A LA HORA DE SOLICITAR CUALQUIER TRÁMITE EN REGISTRO ACADÉMICO:<br>
		<br>
		- SI NO PUEDE SOLICITAR O RECLAMAR CERTIFICADOS PERSONALMENTE, DEBE PRESENTAR LOS SIGUIENTES DOCUMENTOS: CARTA DE AUTORIZACIÓN DEBIDAMENTE FIRMADA Y COPIA DEL DOCUMENTO DE IDENTIDAD  DE LA PERSONA QUE AUTORIZA.
		- PARA RECLAMAR  CUALQUIER FACTURA, DEBE IDENTIFICARSE COMO ESTUDIANTE DE UNICATÓLICA A TRAVÉS DEL CARNÉ (ACTUALIZADO).
		</div>

		<div class="w3-center">
			<a href="{{route('estudiantes.'.$sistema->sistema_nombre_corto, $sistema)}}" class="w3-btn w3-red w3-bar-item">Volver</a>
		</div>
		<br>
@endsection
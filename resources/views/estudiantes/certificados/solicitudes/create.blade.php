@extends('estudiantes.layout.default')

@section('title-estudiantes')
	{{$sistema->sistema_nombre}} | Crear Solicitud
@endsection

@section('content-estudiantes')
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li><a href="{{ route('estudiantes.certificados', $sistema) }}">Certificados</a></li>
			<li class="active"><span>Crear Solicitud</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2><b>Formulario Solicitud de <br>Certificados y Carnets</b></h2>
		</div>
	</div>
	<hr>
	{!!Form::open(['route' => ['estudiantes.certificados.solicitudes.store', $sistema], 'method' => 'POST', 'files' => 'true'])!!}
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Liquidación Banner* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código de la factura de pago de Banner."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_liquidacion', null, ['class' => 'w3-input w3-border-bottom w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código de liquidación Banner', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Comprobante Liquidación* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Recibo escaneado en formato PDF y no mayor a 2 Mb (2048 kilobytes)."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::file('solicitud_comprobante_liquidacion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'required'])}}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label>Formulario <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código del Formulario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_codigo_formulario', 'RA-6.1-4.9-4-001', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Código Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
				<label>Versión <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Versión del Formulario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_version_formulario', '01', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Versión Formulario', 'maxlength' => '20', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos del Estudiante</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Documento* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de documento del usuario: CC, TI, PS, CE."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_tipo_documento', Auth::guard('estudiante')->user()->matriculado_tipo_identificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-disabled', 'placeholder' => 'Tipo documento', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Documento* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Número del documento de identidad."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_documento', Auth::guard('estudiante')->user()->matriculado_identificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Documento', 'maxlength' => '20', 'pattern' => '[0-9]{7,20}', 'required', 'readonly'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Código* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Código del usuario registrado en Banner."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('matriculado_id', Auth::guard('estudiante')->user()->matriculado_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código Estudiante', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'required', 'readonly'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<label>Nombres* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Nombres y apellidos del usuario registrados."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_nombres', Auth::guard('estudiante')->user()->matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'required'])}}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Fecha Nacimiento* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fecha de nacimiento del usuario."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::date('solicitud_matriculado_fecha_nacimiento', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha de Nacimiento', 'maxlength' => '20', 'pattern' => '[0-9]{10,}', 'autofocus', 'required'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Correo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Correo electrónico del usuario donde se enviará los datos de la solicitud creada."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::email('solicitud_matriculado_email', Auth::guard('estudiante')->user()->matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Email', 'maxlength' => '100', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Teléfono Fijo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Teléfono fijo, o celular en el caso de no contar con uno. No usar espacios. De 7 a 10 dígitos."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_telefono', Auth::guard('estudiante')->user()->matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono Fijo', 'maxlength' => '10', 'pattern' => '[0-9]{7,10}', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Celular <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Teléfono celular (Opcional). No usar espacios entre los números. 10 dígitos."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::text('solicitud_matriculado_celular', Auth::guard('estudiante')->user()->matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono Celular', 'pattern' => '[0-9]{10,10}'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Programa* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Programa en el que el estudiante está matriculado y/o desea el certifiado sea realizado."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::select('solicitud_matriculado_programa', $programas, Auth::guard('estudiante')->user()->matriculado_alfa, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Semestre* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Semestre que se certificará."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::select('solicitud_matriculado_semestre', $semestres, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Estudiante* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de estudiante según Banner Student."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::select('solicitud_matriculado_tipo', ['1' => 'Activo'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
				    <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Información del Certificado</b></h4>
        		</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Tipo Certificado* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Tipo de certificado que se realizará. Seleccione el tipo de certificado y haga clic en el icono del ojo para ver el modelo."><span class="fa fa-question-circle fa-fw"></span></a><a class="w3-bar-item" onclick="document.getElementById('cargar_modelo').style.display='block'" title="Modelo del Certificado"><span class="fa fa-eye fa-fw"></span></a></label>
					{{Form::select('tipo_id', $tipos, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'id' => 'tipo_id', 'onchange' => 'cargarCertificado(this.value); displayNewInputs(this.value);', 'placeholder' => 'Seleccionar', 'required'])}}</a>
				</div>
			</div>

			<div class="col-sm-4">
				{{--New inputs--}}
				<div class="row periodo" style="display: none;">
					<div class="col-sm-12">
						<label class="periodo" style="display: none;">Periodo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Periodo del certificado. Ejemplo: 201610."><span class="fa fa-question-circle fa-fw"></span></a></label>
						{{Form::text('periodo_id', null, ['class' => 'w3-input periodo', 'maxlength' => '6', 'pattern' => '[0-9]{4,6}', 'placeholder' => 'Periodo', 'disabled', 'style' => 'display:none;'])}}
					</div>
				</div>

				<div class="row pension" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Valor ($)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Valor en COP$"><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input pension" name="solicitud_pension_valor" maxlength="10" placeholder="Digite el valor a certificar" value="{{ old('solicitud_pension_valor') }}" disabled style="display: none;">
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label class="pension" style="display: none;">Entidad* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Nombre completo de la entidad a quien va dirigido el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="text" class="w3-input pension" name="solicitud_pension_entidad" maxlength="100" placeholder="Nombre Entidad Solicitante" value="{{ old('solicitud_pension_entidad') }}" disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row declaracion" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="declaracion" style="display: none;">Año* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Año para el cual se va a realizar la declaración de renta."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="number" class="w3-input declaracion" name="solicitud_anio_declaracion" maxlength="4" placeholder="Año declaración" value="{{ old('solicitud_anio_declaracion') }}" disabled style="display: none;">
						</div>
					</div>
				</div>

				<div class="row materias" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="materias" style="display: none;">Especifíque la(s) Materia(s)* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Liste los nombres de las materias que debe mencionar el certificado."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<textarea class="form-control materias" name="materia_nombre" rows="10" maxlength="100" placeholder="Especifíque la(s) Materia(s). 1.000 caracteres" disabled style="display: none;">{{ old('materia_nombre') }}</textarea>
						</div>
					</div>
				</div>

				<div class="row carnet" style="display: none;">
					<div class="col-sm-12">
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Foto* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Fotografía 3x4, fondo blanco. Subir en formato jpg o png, no mayor a 5 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="file" class="w3-input carnet" name="solicitud_carnet_foto" disabled style="display: none;">
						</div>
						<div class="form-group">
							<label class="label-control carnet" style="display: none;">Fotocopia Documento* (150%) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Documento de identidad escaneado en formato PDF, no mayor a 2 Mb."><span class="fa fa-question-circle fa-fw"></span></a></label>
							<input type="file" class="w3-input carnet" name="solicitud_carnet_documento" disabled style="display: none;">
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
						            <div class="form-group">
						            	<label class="carnet" style="display: none;">URL Denuncio por Pérdida del Carnet* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="URL del denuncio en la página de la policia https://webrp.policia.gov.co:444/publico/constancia.aspx. El link debe estar acortado por: https://goo.gl/."><span class="fa fa-question-circle fa-fw"></span></a></label>
						            	<input class="w3-input carnet" name="solicitud_carnet_denuncio" maxlength="50" placeholder="URL del denuncio del carnet. 20 caracteres" disabled style="display: none;">
						            </div>
						        </div>
							</div>
						</div>
					</div>					
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label>Sede reclamo* <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Sede donde se reclamará el certificado una vez haya sido realizado."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::select('sede_id', $sedes, null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Seleccionar', 'required'])}}
				</div>
			</div>
		</div>

		{{--Modal modelo certificado--}}
		<div class="w3-container">
			<div id="cargar_modelo" class="w3-modal">
				<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
					<div class="w3-center"><br>
						<span onclick="document.getElementById('cargar_modelo').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Cerrar">&times;</span>
						<h4><b>Modelo Certificado</b></h4>
					</div>
				    <div id="modelo_certificado" class="w3-padding"></div>
				</div>
			</div>
		</div>

		

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label>Observaciones <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Observaciones adicionales a los campos predeterminados del formulario de solicitud."><span class="fa fa-question-circle fa-fw"></span></a></label>
					{{Form::textarea('solicitud_observaciones', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'id' => 'observaciones', 'onkeyup' => 'contarTextarea(observaciones.value)', 'placeholder' => 'Observaciones (2000 caracteres)'])}}
					<div id="caracteres"></div>
				</div>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-rightbar w3-border-{{$sistema->sistema_colorclass}} text-justify">
			<p>
			Ten en cuenta las siguientes recomendaciones a la hora de solicitar cualquier trámite en Registro Académico:
				<ul>
					<li>
						Si no puedes solicitar o reclamar certificados personalmente, debes presentar una carta de autorización debidamente firmada y copia del documento de identidad de la persona que autoriza.
					</li> 
					<li>
						Para reclamar cualquier factura, debes identificarte como estudiante de UNICATÓLICA a través del carnet (actualizado).
					</li>
				</ul>
			</p>
		</div>

		<div class="w3-center">
			<button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
			<a href="{{route('estudiantes.certificados', $sistema)}}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
		</div>
	{!!Form::close()!!}
	<br>
@endsection
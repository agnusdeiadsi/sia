@extends('estudiantes.layout.default')

@section('title-estudiantes')
  Mi Perfil
@endsection

@section('content-estudiantes')
    <div class="w3-container">
      <div class="w3-section w3-bottombar w3-padding-16">
        <h1><b><span class="fa fa-user fa-fw"></span> Perfil</b></h1>
      </div>
    </div>

    <!-- profile Section -->
    <div class="w3-container w3-padding-large">
    <div class="w3-light-blue w3-padding">
      <label>Información tomada del periodo {{Auth::guard('estudiante')->user()->matriculado_periodo}}</label>
    </div>
    <form action="/action_page.php" target="_blank">
      <div class="w3-section">
        <label>Código</label>
        <input class="w3-input w3-border" type="text" name="Name" value="{{Auth::guard('estudiante')->user()->matriculado_id}}" required>
      </div>
      <div class="w3-section">
        <label>Nombres</label>
        <input class="w3-input w3-border" type="text" name="Name" value="{{Auth::guard('estudiante')->user()->matriculado_nombres}}" required>
      </div>
      <div class="w3-section">
        <label>Email</label>
        <input class="w3-input w3-border" type="text" name="Email" value="{{Auth::guard('estudiante')->user()->matriculado_email}}" required>
      </div>
    </form>
    </div>
    <br>
@endsection
<div id="flash-overlay-modal" class="modal fade {{ $modalClass or '' }}">
    <div class="modal-dialog">
        <div class="modal-content">
            @if(isset($sistema))
                <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
            @else
                <div class="modal-header w3-2017-navy-peony">    
            @endif
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">{{ $title }}</h4>
            </div>

            @if(isset($sistema))
                <div class="modal-body w3-text-{{$sistema->sistema_colorclass}}">
            @else
                <div class="modal-body w3-text-2017-navy-peony">
            @endif
                <p>{!! $body !!}</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@component('mail::message')
<b></b>

The body of your message.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

cordialmente,
<br>
<table cellspacing="10">
	<tr>
		<td style="border-right: 1px solid black;">
			<img src="{{ asset('images/Logo-Color-Vertical.png') }}" width="200"/>
		</td>
		<td>
			<p style="text-align: justify;">
			Sistema Integrado de Aplicaciones<br>
			Fundación Universitaria Católica Lumen Gentium<br>
			noresponder@unicatolica.edu.co<br>
			Tel: +57 2 555 27 67 / 312 0038 ext. 1052 - Campus Pance<br>
			Cali - Colombia<br>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<p style="font-size: 8pt; text-align: justify;">"Prueba Electrónica: al recibir el acuse de recibo por parte del destinatario o dependencia, se entenderá como aceptado y se recepcionará como documento prueba de la entrega del usuario (Ley 527 del 18/08/1999) - Reconocimiento jurídico de los mensajes de datos en forma electrónica a través de las redes telemáticas". 
Aviso Legal: este mensaje (incluyendo sus anexos), está destinado únicamente para el uso del individuo o entidad a la cual se ha direccionado y puede contener información que no es de carácter público, de uso privilegiado o de carácter confidencial. Si usted no es el destinatario intencional se le informa que cualquier uso, difusión, distribución o copiado de esta comunicación está terminantemente prohibido, tal como lo establece la Ley 1273 de enero de 2009. Si usted ha recibido esta comunicación por error, notifíquenos inmediatamente y elimine este mensaje. Este mensaje y sus anexos han sido revisados con software antivirus, para evitar que contenga código malicioso que pueda afectar sistemas de cómputo, sin embargo es responsabilidad del destinatario confirmar este hecho en el momento de su recepción.</p>
		</td>
	</tr>
</table>
@endcomponent
@extends('browse.cancelaciones.menu')

@section('title-modulo')
	Autorizaciones | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
	@if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
		<div class="row w3-text-{{$sistema->sistema_colorclass}}">
			<div class="col-sm-12">
			  <h2><span class="fa fa-connectdevelop fa-fw"></span> Autorizaciones</h2>
			</div>
		</div>
		<hr>

		<div class="row">
			<div class="col-sm-12">
				<a href="{{ route('browse.cancelaciones.autorizaciones.create', $sistema) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-right"><span class="fa fa-plus-square"></span> Crear</a>
			</div>
		</div>
		<br>

		<div class="w3-responsive">
			<table class="w3-table-all">
				<thead>
					<tr class="w3-{{$sistema->sistema_colorclass}}">
						<th>Usuario</th>
						<th>Autoriza</th>
						<th>Cuando</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@if($autorizaciones->count() > 0)
						@foreach($autorizaciones as $autoriacion)
							<tr>
								<td>
									@php
										$usuario = \App\Usuario::find($autoriacion->usuario_id);
									@endphp
									{{ $usuario->email}}
								</td>
								<td>
									@php
										$tipo = \App\Model\Cancelaciones\Tipo::find($autoriacion->tipo_id);
									@endphp
									{{ $tipo->tipo_nombre}}
								</td>
								<td>
									@php
										$estado = \App\Model\Cancelaciones\Estado::find($autoriacion->estado_id);
									@endphp
									{{ $estado->estado_nombre}}
								</td>
								<td>
									<a href=""><span class="fa fa-pencil fa-fw"></span></a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="4"><i>No se han definido las autorizaciones aún.</i></td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	@else
		<div class="w3-content">
      		<div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        		<p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      		</div>
    	</div>
	@endif
@endsection
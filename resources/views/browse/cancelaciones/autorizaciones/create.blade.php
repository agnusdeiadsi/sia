@extends('browse.cancelaciones.menu')

@section('title-modulo')
	Crear | Autorizaciones
@endsection

@section('content-modulo')
	<div class="w3-row-padding w3-text-{{$sistema->sistema_colorclass}}">
		<div class="w3-col s12 m6 l6">
			<h2><span class="fa fa-flag fa-fw"></span>Definir Autorización</h2>
		</div>
	</div>

	{{ Form::open(['route' => ['browse.cancelaciones.autorizaciones.store', $sistema], 'method' => 'post']) }}
		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<div class="form-group">
					{{ Form::label('usuario_id', 'El usuario*') }}
					{{ Form::select('usuario_id', $usuarios->pluck('email', 'id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'', 'placeholder' => 'Seleccionar', 'required']) }}
				</div>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<div class="form-group">
					{{ Form::label('tipo_id', 'Autoriza tipo de solicitud*') }}
					{{ Form::select('tipo_id', $tipos->pluck('tipo_nombre', 'tipo_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'', 'placeholder' => 'Seleccionar', 'required']) }}
				</div>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<div class="form-group">
					{{ Form::label('estado_id', 'Cuando el estado es*') }}
					{{ Form::select('estado_id', $estados->pluck('estado_nombre', 'estado_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'', 'placeholder' => 'Seleccionar', 'required']) }}
				</div>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
				<a href="{{ route('browse.cancelaciones.autorizaciones.index', $sistema) }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
			</div>
		</div>
	{{ Form::close() }}
@endsection
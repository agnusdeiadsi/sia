@extends('browse.cancelaciones.menu')

@section('title-modulo')
	Solicitud {{ $solicitud->solicitud_id }} | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
	@php
    	extract($_REQUEST);
  	@endphp
	{{--dd($solicitud->tipo)--}}
	<!-- Menú -->
	<!-- breadcrumbs -->
	{{--<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('estudiantes/home') }}">Inicio</a></li>
			<li><a href="{{ route('estudiantes.cancelaciones', [$sistema, $matriculado]) }}">Cancelaciones</a></li>
			<li class="active"><span>Crear</span></li>
		</ol>
	</div>--}}
	<!-- fin breadcrumbs -->

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="w3-text-{{$sistema->sistema_colorclass}}"><b>Reclamos y Cancelaciones Financieras<br>Solicitud N° {{ $solicitud->solicitud_id }}</b></h2>
		</div>
	</div>
	<hr>
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitante</b></h4>
        </div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_nombres', 'Tipo Documento') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->identificacion->first()->identificacion_tipo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_identificación', 'Documento') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->identificacion->first()->identificacion_numero, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m4 l4">
				<p>
					{{ Form::label('matriculado_codigo', 'Código') }}
					@if(count($solicitud->matriculado->academico) > 0)
						{{ Form::text('matriculado_codigo', $solicitud->matriculado->academico->first()->academico_codigo, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@else
						{{ Form::text('matriculado_codigo', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Código', 'readonly']) }}
					@endif

					{{ Form::hidden('matriculado_pidm', $solicitud->matriculado->matriculado_pidm, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'PIDM', 'readonly']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_nombres', 'Nombres*') }}
					{{ Form::text('matriculado_nombre', $solicitud->matriculado->matriculado_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nombres', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('programa_id', 'Programa*') }}
					{{ Form::text('programa_id', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Programa', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('sede_id', 'Sede*') }}
					{{ Form::text('sede_id', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Sede', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('matriculado_email', 'E-mail*') }}
					{{ Form::text('matriculado_email', $solicitud->matriculado->matriculado_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'E-mail', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_telefono', 'Teléfono*') }}
					{{ Form::text('telefono', $solicitud->matriculado->matriculado_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Teléfono', 'readonly', 'required']) }}
				</p>
			</div>

			<div class="w3-col s12 m3 l3">
				<p>
					{{ Form::label('matriculado_celular', 'Celular*') }}
					{{ Form::text('matriculado_celular', $solicitud->matriculado->matriculado_celular, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Celular', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitud</b></h4>
        </div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('fecha', 'Fecha Radicación') }}
					{{ Form::text('fecha', $solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha Radicación', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('solicitud_id', 'ID') }}
					{{ Form::text('solicitud_id', $solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'ID Solicitud', 'readonly']) }}
				</p>
			</div>
		</div>

        <div class="w3-row-padding">
			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('tipo_id', 'Motivo') }}
					{{ Form::text('tipo_id', $solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Motivo', 'readonly']) }}
				</p>
			</div>

			<div class="w3-col s12 m6 l6">
				<p>
					{{ Form::label('estado_id', 'Estado') }}
					{{ Form::text('estado_id', $solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Estado', 'readonly']) }}
				</p>
			</div>
		</div>		

		<div class="w3-row-padding">
			<div class="w3-col s12">
				<p>
					{{ Form::label('solicitud_descripcion', 'Descripción*') }}
					{{ Form::textarea('solicitud_descripcion', $solicitud->solicitud_descripcion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'readonly', 'required']) }}
				</p>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Adjuntos</b></h4>
        </div>
		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-responsive">
        			<table class="w3-table-all">
                		<tbody>
			        		@if($solicitud->adjuntos->count() > 0)
			        			@foreach($solicitud->adjuntos as $adjunto)
		        					<tr class="w3-text-grey">
		        						<td>{{ $adjunto->adjunto_nombre }}</td>
		        						<td>
		        							<a href="{{ route('browse.cancelaciones.adjuntos.download', [$sistema, $adjunto]) }}"><span class="fa fa-download fa-fw"></span> Descargar</a>
		        						</td>
		        					</tr>
			        			@endforeach
			        		@else
			        			<tr>
			        				<td>
			        					<i>La solicitud no tiene archivos adjuntos.</i>
			        				</td>
			        			</tr>
        					@endif
        				</tbody>
        			</table>
        		</div>
			</div>
		</div>

		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Soportes Áreas</b></h4>
        </div>
		<div class="row">
			<div class="col-sm-12">
        		<div class="w3-responsive">
        			<table class="w3-table-all">
        				<thead>
        					<tr class="w3-{{ $sistema->sistema_colorclass}}">
        						<th>Archivo</th>
        						<th>Subido por</th>
        						<th>Fecha</th>
        						<th></th>
        					</tr>
        				</thead>
                		<tbody>
			        		@if($solicitud->soportes->count() > 0)
			        			@foreach($solicitud->soportes as $soporte)
		        					<tr class="w3-text-grey">
		        						<td>{{ $soporte->soporte_nombre }}</td>
		        						<td>{{ $soporte->usuario->cuenta->cuenta_primer_nombre.' '.$soporte->usuario->cuenta->cuenta_primer_apellido }} <a href="javascript:void(0)" onclick="document.getElementById('perfil_{{$soporte->usuario->id}}').style.display='block'"><span class="fa fa-plus fa-fw"></span> Info</a></td>
		        						<td>{{ $soporte->created_at }}</td>
		        						<td>
		        							<a href="{{ route('browse.cancelaciones.soportes.download', [$sistema, $soporte]) }}"><span class="fa fa-download fa-fw" title="Descargar"></span></a>
		        							<a href="{{ route('browse.cancelaciones.soportes.download', [$sistema, $soporte]) }}"><span class="fa fa-eye fa-fw" title="Ver"></span></a>
		        						</td>
		        					</tr>

		        					{{-- MODAL PERFIL USUARIO --}}
		        					<div id="perfil_{{$soporte->usuario->id}}" class="w3-modal w3-animate-zoom">
								        <div class="w3-modal-content">
								          <div class="w3-container w3-padding">
								            <span onclick="document.getElementById('perfil_{{$soporte->usuario->id}}').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
								            <div class="w3-row-padding w3-{{$sistema->sistema_colorclass}}">
								              <div class="w3-col s12 w3-center">
								                <h4><b><span class="fa fa-user fa-fw"></span>Perfil</b></h4>
								              </div>
								            </div><br>  
								            <div class="w3-row-padding">
								              <div class="w3-col s4">
								                <center>
								                  @if($soporte->usuario->cuenta->cuenta_genero == 'M')
								                    <img src="{{asset('images/avatar/img_avatar3.png')}}" class="w3-image" alt="avatar_m">
								                  @elseif($soporte->usuario->cuenta->cuenta_genero == 'F')
								                    <img src="{{asset('images/avatar/img_avatar4.png')}}" class="w3-image" alt="avatar_f">
								                  @endif
								                </center>
								              </div>
								              <div class="w3-col s8">
								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Nombres</b>: {{$soporte->usuario->cuenta->cuenta_primer_nombre.' '.$soporte->usuario->cuenta->cuenta_segundo_nombre.' '.$soporte->usuario->cuenta->cuenta_primer_apellido.' '.$soporte->usuario->cuenta->cuenta_segundo_apellido}}
								                    </div>    
								                  </div>

								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Área</b>: {{$soporte->usuario->cuenta->subarea->subarea_nombre}}
								                    </div>    
								                  </div>

								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Cuenta</b>: {{$soporte->usuario->cuenta->cuenta_cuenta}} <b>Tipo</b>: {{$soporte->usuario->cuenta->tipo->tipo_nombre}}
								                    </div>    
								                  </div>
								                  @php
								                  	$myaccount = \App\Model\Correos\Cuenta::find(\Auth::user()->id);
								                  @endphp
								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Teléfono</b>: (+57) (2) 3120038 <b>Extensión</b>: {{$soporte->usuario->cuenta->extension->extension_extension}} <a href="http://10.0.0.3/ippbx/index.php?r=peers/dircall&destino={{$soporte->usuario->cuenta->extension->extension_extension}}&origen={{$myaccount->extension->extension_extension}}" title="Llamar" target="open_call"><span class="fa fa-phone fa-fw"></span>Llamar</a>
								                      <iframe name="open_call" id="open_call" hidden></iframe>
								                    </div>    
								                  </div>

								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Rol SIA</b>: {{$soporte->usuario->rol_sia}}
								                    </div>    
								                  </div>

								                  <div class="w3-row-padding">
								                    <div class="w3-col s12">
								                      <b>Rol Módulo</b>: {{$soporte->usuario->rol_modulo}}
								                    </div>    
								                  </div>
								              </div>
								            </div><br>
								          </div>
								        </div>--}}
								      </div>
			        			@endforeach
			        		@else
			        			<tr>
			        				<td colspan="4">
			        					<i>La solicitud no tiene archivos de soporte por parte de las áreas involucradas.</i>
			        				</td>
			        			</tr>
        					@endif
        				</tbody>
        			</table>
        		</div>
			</div>
		</div>

		{{--Respuesta--}}
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Respuesta</b></h4>
        </div>
        @if($solicitud->respuesta != null)
        	<a href="" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-download fa-fw" title="Descargar"></span> Descargar</a>
        @endif

        {{--Firmas--}}
        <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
        	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Fimas o Aprobaciones</b></h4>
        </div>
        @if($solicitud->respuesta != null)
        	<div class="w3-row-padding">
        		<div class="w3-col s12 m6 l6">
	        		@if($solicitud->respuesta->respuesta_firma_uno != null)
	        			<b>{{$solicitud->respuesta->usuarioFimarUno->cuenta->cuenta_primer_nombre.' '.$solicitud->respuesta->usuarioFimarUno->cuenta->cuenta_primer_apellido}}</b><br>
	        			{{$solicitud->respuesta->usuarioFimarUno->email}}
	        		@endif
	        	</div>

	        	<div class="w3-col s12 m6 l6">
	        		@if($solicitud->respuesta->respuesta_firma_dos != null)
	        			<b>{{$solicitud->respuesta->usuarioFimarDos->cuenta->cuenta_primer_nombre.' '.$solicitud->respuesta->usuarioFimarUno->cuenta->cuenta_primer_apellido}}</b><br>
	        			{{$solicitud->respuesta->usuarioFimarDos->email}}
	        		@endif
	        	</div>
        	</div>
        @endif

		<hr>
		<div class="row w3-center">
			<div class="col-sm-12">
				@if($solicitud->estado_id == 1)
					@if(\Auth::user()->rol_sia == 'Administrador' || \Auth::user()->rol_modulo == 'Manager')
						<a href="javascript:void(0)" class="w3-btn w3-{{ $sistema->sistema_colorclass}} w3-bar-item" onclick="document.getElementById('modal_procesar_solicitud').style.display='block'"><span class="fa fa-refresh fa-fw"></span> Procesar</a>
					@endif

					@if(\Auth::user()->rol_sia == 'Administrador' || \Auth::user()->rol_modulo == 'Revisor')
						<a href="javascript:void(0)" class="w3-btn w3-{{ $sistema->sistema_colorclass}} w3-bar-item" onclick="document.getElementById('modal_soportes_solicitud').style.display='block'"><span class="fa fa-thumbs-up fa-fw"></span> Vo.Bo</a>
					@endif
				@elseif($solicitud->estado_id == 2)
					<a href="{{ route('browse.cancelaciones.respuesta.sign', [$sistema, $solicitud->respuesta, 'filtro' => $filtro]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-pencil fa-fw"></span> Aprobar</a>
				@endif
				
				<a href="{{ route('browse.cancelaciones.solicitudes.index', [$sistema, 'filtro' => $filtro]) }}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-remove fa-fw"></span> Volver</a>
			</div>
		</div>

	{{-- MODAL SOPORTES --}}
	<div id="modal_soportes_solicitud" class="w3-modal w3-animate-zoom">
		<div class="w3-modal-content">
		  	<div class="w3-container w3-padding">
		    <span onclick="document.getElementById('modal_soportes_solicitud').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
		    <div class="w3-row-padding w3-{{ $sistema->sistema_colorclass }}">
		      <div class="w3-col s12 w3-center">
		        <h4><b><span class="fa fa-files-o fa-fw"></span> Soportes</b></h4>
		      </div>
		    </div><br>
		    {{ Form::open(['route' => ['browse.cancelaciones.soportes.store', $sistema, $solicitud], 'method' => 'post', 'files' => 'true']) }}
				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('soporte', 'Soporte*') }}
							{{ Form::file('soporte', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nota interna', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('soporte_descripcion', 'Descripción*') }}
							{{ Form::textarea('soporte_descripcion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
					<div class="w3-col s12 w3-center">
						<button type="submit" class="w3-btn w3-{{ $sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
						<button type="reset" class="w3-btn w3-red" onclick="document.getElementById('modal_soportes_solicitud').style.display='none'"><span class="fa fa-ban fa-fw"></span> Cancelar</button>
					</div>
			    </div><br>
			{{ Form::close() }}
			</div>
		</div>
	</div>
	{{-- FIN MODAL SOPORTES --}}


	{{-- MODAL PROCESAR SOLICITUD --}}
	<div id="modal_procesar_solicitud" class="w3-modal w3-animate-zoom">
		<div class="w3-modal-content">
		  	<div class="w3-container w3-padding">
		    <span onclick="document.getElementById('modal_procesar_solicitud').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
		    <div class="w3-row-padding w3-{{ $sistema->sistema_colorclass }}">
		      <div class="w3-col s12 w3-center">
		        <h4><b><span class="fa fa-refresh fa-fw"></span> Procesar</b></h4>
		      </div>
		    </div><br>
		    {{ Form::open(['route' => ['browse.cancelaciones.solicitudes.process', $sistema, $solicitud, 'filtro' => $filtro], 'method' => 'post', 'files' => 'true']) }}  
			    <div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_ni', 'N° Nota Interna*') }}
							{{ Form::text('respuesta_ni', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Nota interna', 'maxlength' => '10', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_valor', 'Valor ($)*') }}
							{{ Form::text('respuesta_valor', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Valor', 'maxlength' => '20', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_dc', 'Respuesta PDF*') }}
							{{ Form::file('respuesta_dc', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Documento PDF', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_observacion', 'Observación*') }}
							{{ Form::textarea('respuesta_observacion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Observación (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
					<div class="w3-col s12 w3-center">
						<button type="submit" class="w3-btn w3-{{ $sistema->sistema_colorclass}}"><span class="fa fa-refresh fa-fw"></span> Procesar</button>
						<button type="reset" class="w3-btn w3-red" onclick="document.getElementById('modal_procesar_solicitud').style.display='none'"><span class="fa fa-ban fa-fw"></span> Cancelar</button>
					</div>
			    </div><br>
			{{ Form::close() }}
		  	</div>
		</div>
	</div>
	{{-- FIN MODAL PROCESAR SOLICITUD --}}
@endsection
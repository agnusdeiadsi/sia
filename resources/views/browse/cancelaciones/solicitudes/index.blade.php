@extends('browse.cancelaciones.menu')

@section('title-modulo')
  Solicitudes | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Revisor' || Auth::user()->rol_modulo == 'Autorizante')
  
  <div class="w3-row w3-text-{{$sistema->sistema_colorclass}}">
    <div class="w3-col s12">
      <h2><span class="fa fa-flag fa-fw"></span>Solicitudes</h2>
    </div>
  </div>

  <div class="w3-row w3-bar w3-white">
    <div class="w3-col s12 m4 l4 tablink w3-bottombar" id="1" onclick="openTab(event,'procesamiento')">
      <a href="{{ route('browse.cancelaciones.solicitudes.index', [$sistema, 'filtro' => '1']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;"> Procesamiento
        {{--<span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
          {{$solicitudes->where('estado_id', 1)->count()}}
        </span>--}}
      </a>
    </div>

    <div class="w3-col s12 m4 l4 tablink w3-bottombar" id="2" onclick="openTab(event,'firmar')">
      <a href="{{ route('browse.cancelaciones.solicitudes.index', [$sistema, 'filtro' => '2']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;"> Firmar
        {{--<span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
          {{$solicitudes->where('estado_id', 2)->count()}}
        </span>--}}
      </a>
    </div>

    <div class="w3-col s12 m4 l4 tablink w3-bottombar" id="3" onclick="openTab(event,'historial')">
      <a href="{{ route('browse.cancelaciones.solicitudes.index', [$sistema, 'filtro' => '3']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;"> Procesadas</a>
    </div>
  </div>

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    {{--<div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['erc.recibos.historial', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['erc.recibos.historial', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>--}}
  </div>

  <div id="procesamiento" class="w3-container city" style="display:none;">
    <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Radicado</th>
              <th>ID Solicitud</th>
              <th>Codigo Estudiante</th>
              <th>Nombres</th>
              <th>Tipo Solicitud</th>
              <th>Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_solicitudes = 0;
            @endphp
            @foreach($solicitudes->where('estado_id', 1) as $solicitud_)
              @php
                $solicitud = \App\Model\Cancelaciones\Solicitud::find($solicitud_->solicitud_id);
              @endphp
                <tr>
                  <td>{{ $solicitud->created_at }}</td>
                  <td>{{ $solicitud->solicitud_id }}</td>
                  @if(count($solicitud->matriculado->academico) > 0)
                    <td>{{ $solicitud->matriculado->academico->first()->academico_codigo }}</td>
                  @else
                    <td></td>
                  @endif
                  <td>{{ $solicitud->matriculado->matriculado_nombres }}</td>
                  <td>{{ $solicitud->tipo->tipo_nombre }}</td>
                  <td>{{ $solicitud->estado->estado_nombre }}</td>
                  <td>
                    <a href="{{ route('browse.cancelaciones.solicitudes.show', [$sistema, $solicitud, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                    <a href="" class="w3-btn w3-white w3-border" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
                  </td>
                </tr>
                @php
                  $total_solicitudes++;
                @endphp
            @endforeach

            @if($total_solicitudes==0)
              <tr>
                <td colspan="7"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>

      <div class="w3-center">
        <div class="w3-bar">
          {!! $solicitudes->appends(['filtro' => 1])->render() !!}
        </div>
      </div>
    </p>
  </div>

  <div id="firmar" class="w3-container city" style="display:none;">
    <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Radicado</th>
              <th>ID Solicitud</th>
              <th>Codigo Estudiante</th>
              <th>Nombres</th>
              <th>Tipo Solicitud</th>
              <th>Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_solicitudes = 0;
            @endphp
            @foreach($solicitudes->where('estado_id', 2) as $solicitud)
              @php
                $solicitud = \App\Model\Cancelaciones\Solicitud::find($solicitud->solicitud_id);
              @endphp
                @if($solicitud->respuesta->respuesta_firma_uno == null && $solicitud->respuesta->respuesta_firma_dos == null || $solicitud->respuesta->respuesta_firma_uno != \Auth::user()->id && $solicitud->respuesta->respuesta_firma_dos != \Auth::user()->id)
                  <tr>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    @if(count($solicitud->matriculado->academico) > 0)
                      <td>{{ $solicitud->matriculado->academico->first()->academico_codigo }}</td>
                    @else
                      <td></td>
                    @endif
                    <td>{{ $solicitud->matriculado->matriculado_nombres }}</td>
                    <td>{{ $solicitud->tipo->tipo_nombre }}</td>
                    <td>{{ $solicitud->estado->estado_nombre }}</td>
                    <td>
                      <a href="{{ route('browse.cancelaciones.solicitudes.show', [$sistema, $solicitud, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                      <a href="" class="w3-btn w3-white w3-border" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_solicitudes++;
                  @endphp
              @endif
            @endforeach

            @if($total_solicitudes==0)
              <tr>
                <td colspan="7"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>

      <div class="w3-center">
        <div class="w3-bar">
          {!! $solicitudes->appends(['filtro' => 2])->render() !!}
        </div>
      </div>
    </p>
  </div>

  {{-- HISTORIAL --}}
  <div id="historial" class="w3-container city" style="display:none;">
    <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Radicado</th>
              <th>ID Solicitud</th>
              <th>Codigo Estudiante</th>
              <th>Nombres</th>
              <th>Tipo Solicitud</th>
              <th>Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_solicitudes = 0;
            @endphp
            @foreach($solicitudes->where('estado_id', 3) as $solicitud)
              @php
                $solicitud = \App\Model\Cancelaciones\Solicitud::find($solicitud->solicitud_id);
              @endphp
              <tr>
                <td>{{ $solicitud->created_at }}</td>
                <td>{{ $solicitud->solicitud_id }}</td>
                @if(count($solicitud->matriculado->academico) > 0)
                  <td>{{ $solicitud->matriculado->academico->first()->academico_codigo }}</td>
                @else
                  <td></td>
                @endif
                <td>{{ $solicitud->matriculado->matriculado_nombres }}</td>
                <td>{{ $solicitud->tipo->tipo_nombre }}</td>
                <td>{{ $solicitud->estado->estado_nombre }}</td>
                <td>
                  <a href="{{ route('browse.cancelaciones.solicitudes.show', [$sistema, $solicitud, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                  <a href="" class="w3-btn w3-white w3-border" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
                </td>
              </tr>
              @php
                $total_solicitudes++;
              @endphp
            @endforeach

            @if($total_solicitudes==0)
              <tr>
                <td colspan="7"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>

      <div class="w3-center">
        <div class="w3-bar">
          {!! $solicitudes->appends(['filtro' => 3])->render() !!}
        </div>
      </div>
    </p>
  </div>
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
  <script>
    document.getElementById({{$filtro}}).click();

    function openTab(evt, tabName) {
      var i, x, tablinks;
      x = document.getElementsByClassName("city");
      for (i = 0; i < x.length; i++) {
          x[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < x.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" w3-border-{{$sistema->sistema_colorclass}}", "");
      }
      document.getElementById(tabName).style.display = "block";
      evt.currentTarget.className += " w3-border-{{$sistema->sistema_colorclass}}";
    }
  </script>
@endsection

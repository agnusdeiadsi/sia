@extends('browse.cancelaciones.menu')

@section('title-modulo')
  Informes | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
	<div class="w3-row w3-text-{{$sistema->sistema_colorclass}}">
    	<div class="w3-col s12">
      		<h2><span class="fa fa-bar-chart fa-fw"></span>Informes</h2>
    	</div>
  	</div>
  	<hr>
  	
  	<div class="w3-row-padding">
  		<div class="w3-col s12 m3 l3">
        <a href="{{ route('browse.cancelaciones.informes.requests', $sistema) }}" class="w3-bar-item">
    			<div class="w3-card w3-hover-shadow">
    				<div class="w3-container w3-center">
    					<span class="fa fa-bar-chart fa-5x"></span>
  					<p>Solicitudes por periodo</p>
  				</div>
    				<footer class="w3-container w3-blue">
  					<h5>Footer</h5>
  				</footer>
    			</div>
        </a>
  		</div>
  	</div>
@endsection
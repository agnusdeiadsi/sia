@extends('browse.cancelaciones.informes.requests')

@section('informe-chart')
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<style type="text/css">
#container, #sliders {
    min-width: 500px; 
    max-width: 800px;
    margin: 0 auto;
}
#container {
    height: 450px; 
}
		</style>
		<script type="text/javascript">
$(function () {
    // Set up the chart
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            type: 'column',
            margin: 75,
            /*options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 50,
                viewDistance: 25
            }*/
        },
        title: {
            text: 'Total Solicitudes por Estado'
        },
        subtitle: {
            text: 'Periodo '+
            @if($datos->count() != null)
                {!! $datos->first()->periodo !!}
            @endif
        },
        credits: {
        	enabled: false
    	},
        xAxis: {
        	title: {
    			text: 'Estado',
    		},

        	categories: [
                @if($datos->count() != null)
            		@foreach ($datos as $dato)
    	        		{!! $estados->where('estado_id', $dato->estado)->pluck('estado_nombre') !!},
    	        	@endforeach
                @else
                    null,
                @endif
        	]
    	},
    	yAxis:{
    		title: {
    			text: 'Total',
    		}
    	},
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [{
        	name: 'Solicitudes',
        	data: [
                @if($datos->count() != null)
    	            @foreach ($datos as $data)
    	        		{!! $data->total !!},
    	        	@endforeach
                @else
                    null,
                @endif
	        ],

            dataLabels: {
                enabled: true,
            }
        }],
    });

    function showValues() {
        $('#R0-value').html(chart.options.chart.options3d.alpha);
        $('#R1-value').html(chart.options.chart.options3d.beta);
    }

    // Activate the sliders
    $('#R0').on('change', function () {
        chart.options.chart.options3d.alpha = this.value;
        showValues();
        chart.redraw(false);
    });
    $('#R1').on('change', function () {
        chart.options.chart.options3d.beta = this.value;
        showValues();
        chart.redraw(false);
    });

    showValues();
});
</script>

<div class="w3-responsive">
	<div id="container"></div>
</div>
@endsection
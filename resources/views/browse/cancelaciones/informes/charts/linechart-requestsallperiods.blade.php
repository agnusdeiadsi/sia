@extends('browse.cancelaciones.informes.requests')

@section('informe-chart')
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Histórico solicitudes',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [
            	@foreach ($periodos as $categoria)
            		{!! $categoria->periodo !!},
            	@endforeach
            ],
            title: {
                text: 'Periodos'
            },
        },
        yAxis: {
            title: {
                text: 'Total'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [{
            name: 'Solicitudes',
            data: [
            	@foreach ($periodos as $dato)
            		{!! $dato->total !!},
            	@endforeach
            ],

            dataLabels: {
                enabled: true,
            }
        }]
    });
});
</script>
<div class="w3-responsive">
	<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
@endsection
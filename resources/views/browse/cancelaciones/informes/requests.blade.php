@extends('browse.cancelaciones.menu')

@section('title-modulo')
  Informe Solicitudes | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
	@php
		extract($_REQUEST);
	@endphp

	<div class="w3-row-padding">
		{{ Form::open(['route' => ['browse.cancelaciones.informes.requests.barchart', $sistema], 'method' => 'get']) }}

		@if(isset($periodo))
			<div class="w3-col s12 m4 l4">
				{{ Form::label('periodo', 'Periodo') }}
				{{ Form::select('periodo', $periodos->pluck('periodo', 'periodo'), $periodo, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
			</div>
		@else
			<div class="w3-col s12 m4 l4">
				{{ Form::label('periodo', 'Periodo') }}
				{{ Form::select('periodo', $periodos->pluck('periodo', 'periodo'), null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
			</div>
		@endif

		@if(isset($tipo))
			<div class="w3-col s12 m4 l4">
				{{ Form::label('tipo', 'Tipo') }}
				{{ Form::select('tipo', $tipos->pluck('tipo_nombre', 'tipo_id'), $tipo, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
			</div>
		@else
			<div class="w3-col s12 m4 l4">
				{{ Form::label('tipo', 'Tipo') }}
				{{ Form::select('tipo', $tipos->pluck('tipo_nombre', 'tipo_id'), null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
			</div>
		@endif

		<div class="w3-col s12 m4 l4">
			<button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-filter fa-fw"></span></button>
			<a href="{{ route('browse.cancelaciones.informes.requests', $sistema) }}" class="w3-btn w3-red"><span class="fa fa-ban fa-fw"></span></a>
		</div>
		{{ Form::close() }}
	</div>
	<br>
	<br>
  	@yield('informe-chart')
@endsection
@extends('browse.cancelaciones.menu')

@section('title-modulo')
	Apelación {{$apelacion->apelacion_id}} | {{$sistema->sistema_nombre}}
@endsection

@section('content-modulo')
	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="w3-text-{{$sistema->sistema_colorclass}}"><b>Apelación<br>Solicitud N° {{$apelacion->solicitud->solicitud_id}}</b></h2>
		</div>
	</div>
	<hr>

	<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
    	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Solicitud</b></h4>
    </div>

    <div class="w3-row-padding">
		<div class="w3-col s12 m6 l6">
			<p>
				{{ Form::label('fecha', 'Fecha Radicación') }}
				{{ Form::text('fecha', $apelacion->solicitud->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Fecha Radicación', 'readonly']) }}
			</p>
		</div>

		<div class="w3-col s12 m6 l6">
			<p>
				{{ Form::label('solicitud_id', 'ID') }}
				{{ Form::text('solicitud_id', $apelacion->solicitud->solicitud_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'ID Solicitud', 'readonly']) }}
			</p>
		</div>
	</div>

    <div class="w3-row-padding">
		<div class="w3-col s12 m6 l6">
			<p>
				{{ Form::label('tipo_id', 'Motivo') }}
				{{ Form::text('tipo_id', $apelacion->solicitud->tipo->tipo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Motivo', 'readonly']) }}
			</p>
		</div>

		<div class="w3-col s12 m6 l6">
			<p>
				{{ Form::label('estado_id', 'Estado') }}
				{{ Form::text('estado_id', $apelacion->solicitud->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Estado', 'readonly']) }}
			</p>
		</div>
	</div>

	<div class="w3-row-padding">
		<div class="w3-col s12">
			<p>
				<a href="javascript:void(0)" class="w3-bar-item" onclick="document.getElementById('modal_ver_solicitud').style.display='block'"><span class="fa fa-eye fa-fw"></span> Mostrar más</a>
			</p>
		</div>
	</div>

	<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
    	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Datos Apelación</b></h4>
    </div>

    <div class="w3-row-padding">
    	<div class="w3-col s12 m4 l4">
			<p>
				{{ Form::label('created_at', 'Fecha radicación') }}
				{{ Form::text('apelacion_id', $apelacion->created_at, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
			</p>
		</div>

		<div class="w3-col s12 m4 l4">
			<p>
				{{ Form::label('apelacion_id', 'ID Apelación') }}
				{{ Form::text('apelacion_id', $apelacion->apelacion_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
			</p>
		</div>

		<div class="w3-col s12 m4 l4">
			<p>
				{{ Form::label('apelacion_estado_id', 'Estado') }}
				{{ Form::text('apelacion_id', $apelacion->estado->estado_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'readonly']) }}
			</p>
		</div>
	</div>

    <div class="w3-row-padding">
		<div class="w3-col s12">
			<p>
				{{ Form::label('apelacion_descripcion', 'Descripción*') }}
				{{ Form::textarea('apelacion_descripcion', $apelacion->apelacion_descripcion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Descripción (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
			</p>
		</div>
	</div>

	<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
    	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b> Adjuntos</b></h4>
    </div>

	<div class="w3-row-padding">
		<div class="w3-col s12">
			@if($apelacion->adjuntos != null)
				@foreach($apelacion->adjuntos as $adjunto)
					<a href="{{ route('browse.cancelaciones.adjuntos.download', [$sistema, $adjunto]) }}"><span class="fa fa-download fa-fw" title="Descargar"></span></a>
				@endforeach
			@else
				<i>La apelación no tiene documentos adjuntos.</i>
			@endif
		</div>
	</div>

	@if($apelacion->respuesta != null)
		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
	    	<h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>Respuesta</b></h4>
	    </div>

    	<a href="{{ route('browse.cancelaciones.respuestas.download', [$sistema, $apelacion->respuesta]) }}" class="w3-bar-item"><span class="fa fa-download fa-fw" title="Descargar"></span> Descargar</a>
    @endif
	<hr>

	<div class="w3-row-padding w3-center">
		<div class="w3-col s12">
			<a href="javascript:void(0);" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-bar-item" onclick="document.getElementById('modal_procesar_apelacion').style.display='block'"><span class="fa fa-refresh fa-fw"></span> Procesar</a>
			<a href="{{ route('browse.cancelaciones.apelaciones.index', $sistema) }}" class="w3-btn w3-red w3-bar-item"><span class="fa fa-reply fa-fw"></span> Volver</a>
		</div>
	</div>

	{{-- MODAL PROCESAR APELACION --}}
	<div id="modal_procesar_apelacion" class="w3-modal w3-animate-zoom">
		<div class="w3-modal-content">
		  	<div class="w3-container w3-padding">
		    <span onclick="document.getElementById('modal_procesar_apelacion').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
		    <div class="w3-row-padding w3-{{ $sistema->sistema_colorclass }}">
		      <div class="w3-col s12 w3-center">
		        <h4><b><span class="fa fa-refresh fa-fw"></span> Procesar</b></h4>
		      </div>
		    </div><br>
		    {{ Form::open(['route' => ['browse.cancelaciones.apelaciones.process', $sistema, $apelacion], 'method' => 'post', 'files' => 'true']) }}  
				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_dc', 'Respuesta PDF*') }}
							{{ Form::file('respuesta_dc', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Documento PDF', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
			      	<div class="w3-col s12">
				        <p>
							{{ Form::label('respuesta_observacion', 'Observación*') }}
							{{ Form::textarea('respuesta_observacion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Observación (2000 caracteres)', 'maxlength' => '2000', 'required']) }}
						</p>
					</div>
				</div>

				<div class="w3-row-padding">
					<div class="w3-col s12 w3-center">
						<button type="submit" class="w3-btn w3-{{ $sistema->sistema_colorclass}}"><span class="fa fa-refresh fa-fw"></span> Procesar</button>
						<button type="reset" class="w3-btn w3-red" onclick="document.getElementById('modal_procesar_apelacion').style.display='none'"><span class="fa fa-ban fa-fw"></span> Cancelar</button>
					</div>
			    </div><br>
			{{ Form::close() }}
		  	</div>
		</div>
	</div>
	{{-- FIN MODAL PROCESAR APELACION --}}

	{{-- MODAL VER SOLICITUD --}}
	<div id="modal_ver_solicitud" class="w3-modal w3-animate-zoom">
		<div class="w3-modal-content">
		  	<div class="w3-container w3-padding">
			    <span onclick="document.getElementById('modal_ver_solicitud').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
			    <div class="w3-row-padding w3-{{ $sistema->sistema_colorclass }}">
			      <div class="w3-col s12 w3-center">
			        <h4><b><span class="fa fa-refresh fa-fw"></span> Solicitud {{$apelacion->solicitud->solicitud_id}}</b></h4>
			      </div>
			    </div><br>

		  	</div>
		</div>
	</div>
	{{-- FIN MODAL VER SOLICITUD --}}
@endsection
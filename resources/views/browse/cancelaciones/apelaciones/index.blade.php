@extends('browse.cancelaciones.menu')

@section('title-modulo')
  Solicitudes | {{ ucfirst($sistema->sistema_nombre_corto) }}
@endsection

@section('content-modulo')
  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')

  <div class="row w3-text-{{$sistema->sistema_colorclass}}">
    <div class="col-sm-12">
      <h2><span class="fa fa-flag fa-fw"></span>Apelaciones</h2>
    </div>
  </div>

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    {{--<div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['erc.recibos.historial', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['erc.recibos.historial', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>--}}
  </div>
  <hr>

  <div class="w3-responsive">
    <table class="w3-table w3-striped w3-hoverable">
      <thead>
        <tr class="w3-{{ $sistema->sistema_colorclass }}">
          <th>Fecha Radicado</th>
          <th>Apelación ID</th>
          <th>Codigo Estudiante</th>
          <th>Solicitud ID</th>
          <th>Estado</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="w3-text-grey">
        @php
          $total_apelaciones = 0;
        @endphp
        @if($apelaciones->count() > 0)
          @foreach($apelaciones as $apelacion)
            {{--dd($solicitud->matriculado->identificacion->first()->identificacion_numero)--}}
            <tr>
              <td>{{ $apelacion->created_at }}</td>
              <td>{{ $apelacion->apelacion_id }}</td>
              <td>{{ $apelacion->solicitud->solicitud_id }}</td>
              <td>{{ $apelacion->solicitud->matriculado->matriculado_nombres }}</td>
              <td>{{ $apelacion->estado->estado_nombre }}</td>
              <td>
                <a href="{{ route('browse.cancelaciones.apelaciones.show', [$sistema, $apelacion]) }}" class="w3-btn w3-white w3-border" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                <a href="" class="w3-btn w3-white w3-border" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
              </td>
            </tr>
            {{--<tr>
              <td>{{ $recibo->recibo_created_at }}</td>
              <td>{{ $recibo->centrooperacion_codigo." - ".$recibo->centrooperacion_nombre }}</td>
              <td>{{ $recibo->recibo_consecutivo_centro }}</td>
              <td>{{ $recibo->recibo_codigo_liquidacion }}</td>
              <td>{{ $recibo->email }}</td>
              <td>{{ $recibo->estado_nombre }}</td>
              <td>
                <!--<a href="{{ route('erc.recibos.show', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Ver"><i class="fa fa-eye"></i></a>-->
                <a href="{{ route('erc.recibos.edit', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Ver/Editar"><i class="fa fa-pencil fa-fw"></i></a>
                <a href="{{ url('browse/erc/recibos/download', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
              </td>
            </tr>--}}
            @php
              $total_apelaciones++;
            @endphp
          @endforeach
        @endif

        @if($total_apelaciones==0)
          <tr>
            <td colspan="6"><i>No hay registros.</i></td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $apelaciones->render() !!}
    </div>
  </div>
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="../../lib/css.css">
  <script type="text/javascript">
  </script>
  <style type="text/css">
    body, table{
      foabezado, #tabla_pie{
      font-siznt-family: calibri;
      font-size: 10.5px;
      border-spacing: 0;
      border-collapse: collapse;
    }
    #fila_total, #fila_conceptos{
      background-color: grey;
      font-size: 12px;
    }
    #tabla_ence: 12px;
    }

    .columna_etiqueta{
      text-align: left;
      vertical-align: text-top;
    }
  @page{
      margin: 5px 10px 0px 10px;
  }

  </style>
</head>

<body>
<!-- Nueva solicitud -->
<table width="100%" border="1" style="max-width: 2550px; padding: 5px; border-collapse: collapse; font-family: arial; font-size: 12px;">
  <thead>
    <tr>
      <td align="center" with="10%">
        <img src="{{asset('/images/Logo-Color-Vertical.png')}}" width="100" />
      </td>
      <td align="center" style="padding: 5px;" with="60%">
        <h3>REQUERIMIENTOS DE MATERIALES, INSUMOS O SERVICIOS</h3>
      </td>
      <td align="center" with="10%">
        <table>
          <tr>
            <td>
              <b>Código</b><br>6A-7.2.4.9.4
            </td>
            <td>
              <b>Versión</b><br>1.0
            </td>
          </tr>
          <tr>
            <td>
              <b>Fecha Vigencia</b>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="3" align="center">
        <h4>SOLICITUD N° {{$solicitud->solicitud_id}}</h4>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <br />
      </td>
    </tr>
  </thead>

  <tbody>
    <!-- datos del solicitante -->
    <tr>
      <td colspan="3" style="background: grey; padding: 10px;">
        <b>1. Datos del Solicitante</b>
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Nombres y Apellidos</b><br />
        {{$solicitud->solicitud_nombres_solicitante}}
      </td>
    </tr>

    <tr>
      <td style="padding: 5px;">
        <b>Email</b><br />
        {{$solicitud->solicitud_email_solicitante}}
      </td>
      <td style="padding: 5px;">
        <b>Telefono Fijo o Móvil</b><br />
        {{$solicitud->solicitud_telefono_solicitante}}
      </td>
      <td style="padding: 5px;">
        <b>Extensión</b><br />
        {{$solicitud->solicitud_extension_solicitante}}
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Jefe Inmediato</b><br />
        {{$solicitud->solicitud_nombres_responsable}}
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
        <br />
      </td>
    </tr>

    <!-- datos de la solicitud -->
    <tr>
      <td colspan="3" style="background: grey; padding: 10px;">
        <b>2. Datos de la Solitud</b>
      </td>
    </tr>

    <tr>
      <td style="padding: 5px;">
        <b>Área\Dependencia\Facultad</b><br />
        @foreach ($areas as $area)
          @if ($area->area_id == $solicitud->area_id)
            {{$area->area_nombre}}
          @endif
        @endforeach
      </td>
      <td style="padding: 5px;">
        <b>Subárea</b><br />
        @foreach ($subareas as $subarea)
          @if ($subarea->subarea_id == $solicitud->subarea_id)
            {{$subarea->subarea_nombre}}
          @endif
        @endforeach
      </td>
      <td style="padding: 5px;">
        <b>Tipo de Solicitud</b><br />
        @if ($solicitud->tipo_id == 1)
          Material o Insumo
        @elseif($solicitud->tipo_id == 2)
          Servicio
        @endif
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Centros de Operación</b><br /><br />
        <table border="1" style="border-style:dashed; border-collapse: collapse;" width="100%">
          <thead>
            <tr style="background: grey; border: 1px solid black; border-style:dashed;">
              <td style="padding: 5px;">
                <b>Código</b>
              </td>
              <td style="padding: 5px;">
                <b>Descripción Centro</b>
              </td>
            </tr>
          </thead>
          <tbody>
            @foreach ($solicitud->centrosOperacion as $centro_operacion)
              <tr style="border: 1px solid black; border-style:dashed;">
                <td style="padding: 5px;">
                  {{$centro_operacion->centrooperacion_codigo}}
                </td>
                <td style="padding: 5px;">
                  {{$centro_operacion->centrooperacion_nombre}}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
      </td>
    </tr>

    {{--<tr>
      <td colspan="3" style="padding: 5px;">
        <b>Proyecto</b><br />
        @foreach ($proyectos as $proyecto)
          @if ($proyecto->proyecto_id == $solicitud->proyecto_id)
            {{$proyecto->proyecto_nombre}}
          @endif
        @endforeach
      </td>
    </tr>--}}

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Centros de Costos</b><br /><br />
        <table border="1" style="border-style:dashed; border-collapse: collapse;" width="100%">
        	<thead>
            <tr style="background: grey; border: 1px solid black; border-style:dashed;">
              <td style="padding: 5px;">
                <b>Código</b>
              </td>
          		<td style="padding: 5px;">
                <b>Descripción Centro</b>
              </td>
            </tr>
        	</thead>
        	<tbody>
             @foreach ($solicitud->centrosCostos as $centro_costos)
               <tr style="border: 1px solid black; border-style:dashed;">
           			 <td style="padding: 5px;">
                   {{$centro_costos->centrocosto_codigo}}
                 </td>
           			 <td style="padding: 5px;">
                   {{$centro_costos->centrocosto_nombre}}
                 </td>
           		 </tr>
             @endforeach
          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Proyectos</b><br /><br />
        <table border="1" style="border-style:dashed; border-collapse: collapse;" width="100%">
          <thead>
            <tr style="background: grey; border: 1px solid black; border-style:dashed;">
              <td style="padding: 5px;">
                <b>Código</b>
              </td>
              <td style="padding: 5px;">
                <b>Descripción Proyecto</b>
              </td>
            </tr>
          </thead>
          <tbody>
            @foreach ($solicitud->proyectos as $proyecto)
              <tr style="border: 1px solid black; border-style:dashed;">
                <td style="padding: 5px;">
                  {{$proyecto->proyecto_codigo}}
                </td>
                <td style="padding: 5px;">
                  {{$proyecto->proyecto_nombre}}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <br />
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Insumos, Materiales o Servicios</b><br /><br />
        <table border="1" style="border-style:dashed; border-collapse: collapse;" width="100%">
          <thead>
            <tr style="background: grey; border: 1px solid black; border-style:dashed;">
              <th>Descripción Referencia</th>
              <th>Unidad de Medida</th>
              <th>Cantidad Solicitada</th>
              <th>Especificaciones y/o Características</th>
              <th>Costo Estimado</th>
              <th>Estado*</th>
            </tr>
          </thead>
          <tbody>
            @if(empty($solicitud->insumos->toArray()))
              <tr>
                <td colspan="5" style="padding: 5px;">
                  <i>La solicitud no tiene insumos.</i>
                </td>
              </tr>
            @else
              @foreach ($solicitud->insumos as $insumo)
                <tr style="border: 1px solid black; border-style:dashed;">
                  <td style="padding: 5px;">
                    {{$insumo->solicitudinsumo_referencia}}
                  </td>
                  <td style="padding: 5px;">
                    {{$insumo->solicitudinsumo_medida}}
                  </td>
                  <td style="padding: 5px;">
                    {{$insumo->solicitudinsumo_cantidad}}
                  </td>
                  <td style="padding: 5px;">
                    {{$insumo->solicitudinsumo_caracteristica}}
                  </td>
                  <td style="padding: 5px;">
                    {{$insumo->solicitudinsumo_costo}}
                  </td>
                  <td style="padding: 5px;">
                    @if($insumo->estado_id == 4)
                      A
                    @elseif($insumo->estado_id == 5)
                      NA
                    @elseif($insumo->estado_id == 1)
                      P
                    @else
                      P
                    @endif
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
        <b>Estado: A: Aprobado | NA: No Aprobado | P: Pendiente |</b>
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <b>Anexos</b><br />
        @if($solicitud->solicitud_presupuesto)
          <a href="{{url('browse/compras/solicitudes/download/budget', [$sistema, $solicitud])}}">Descargar {{$solicitud->solicitud_presupuesto}}</a>
        @else
          <i>La solictud no tiene anexos.</i>
        @endif
      </td>
    </tr>

    <tr>
      <td colspan="3">
        <br />
        <br />
      </td>
    </tr>

    <!-- datos de la solicitud -->
    <tr>
      <td colspan="3" style="background: grey; padding: 10px;">
        <b>3. Justificación</b>
      </td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px;">
        <p style="word-wrap: break-word;">{{$solicitud->solicitud_justificacion}}</p>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>

@extends('browse.compras.menu')

@section('title-modulo')
  {{ ucwords($sistema->sistema_nombre_corto) }} | Gestionar Solicitudes
@endsection

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  {{-- Valida si el usuario tiene uno de los roles y si el autorizante es igual al usuario de la sesion --}}
  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' && $autorizante == Auth::user()->id || Auth::user()->rol_modulo == 'Autorizante' && $autorizante == Auth::user()->id || Auth::user()->rol_modulo == 'Revisor' && $autorizante == Auth::user()->id || Auth::user()->rol_modulo == 'Super Revisor' && $autorizante == Auth::user()->id)
    
    <h2>Gestionar Solicitudes</h2>
    <hr>
    <div class="w3-row w3-bar w3-white">
      <div class="w3-col s4 tablink w3-bottombar" id="1" onclick="openTab(event,'procesamiento')">
        <a href="{{ route('browse.compras.solicitudes.index', [$sistema, 'filtro' => '1', 'autorizante' => Auth::user()->id, 'revisor' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Procesamiento
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            @if(isset($solicitudes_subareas))
              @php
                $sum1 = $solicitudes->where('estado_id', 1)->count();
                $sum2 = $solicitudes_subareas->where('estado_id', 1)->count();
                $sum = $sum1 + $sum2;
              @endphp
            @else
              @php
                $sum = $solicitudes->where('estado_id', 1)->count();
              @endphp
            @endif
            {{ $sum }}
          </span>
        </a>
      </div>

      <div class="w3-col s4 tablink w3-bottombar" id="2" onclick="openTab(event,'comite')">
        <a href="{{ route('browse.compras.solicitudes.index', [$sistema, 'filtro' => '2', 'autorizante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">En estudio
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            @if(isset($solicitudes_subareas))
              @php
                $sum1 = $solicitudes->where('estado_id', 2)->count();
                $sum2 = $solicitudes_subareas->where('estado_id', 2)->count();
                $sum = $sum1 + $sum2;
              @endphp
            @else
              @php
                $sum = $solicitudes->where('estado_id', 2)->count();
              @endphp
            @endif
            {{ $sum }}
          </span>
        </a>
      </div>

      <div class="w3-col s4 tablink w3-bottombar" id="6" onclick="openTab(event,'historial')">
        <a href="{{ route('browse.compras.solicitudes.index', [$sistema, 'filtro' => '6', 'autorizante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Historial</a>
      </div>
    </div>

    {{-- Tab Procesamiento --}}
    <div id="procesamiento" class="w3-container city" style="display:none;">
      <p>
        <div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Área</th>
                <th>Subárea</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              {{-- Imprime los registros de solicitudes cuyo estado sea En Procesamiento --}}
              @foreach($solicitudes->where('estado_id', '1') as $solicitud)
                {{-- Valida que el usuario tenga rol de Manager en el módulo --}}
                @if(Auth::user()->rol_modulo == "Manager")
                  @php
                    /*$a = empty($solicitud->area->autorizacionesUsuarios);
                    $b = empty($solicitud->subarea->autorizacionesUsuarios);
                    echo $a;

                    if($a == 1 && $b == 1)
                    {
                      //echo $solicitud->subarea->autorizacionesUsuarios;
                      echo "Están Vacía";
                    }
                    else
                    {
                      if(count($solicitud->subarea->autorizacionesUsuarios) > 0)
                      {
                        echo "Tiene usuario";
                      }
                      else
                      {
                        echo "No tiene usuario";
                      }
                      
                    }*/
                  @endphp
                  {{-- si la solicitud debe tener 3 firmas --}}
                  @if(!empty($solicitud->area->autorizacionesUsuarios) && !empty($solicitud->subarea->autorizacionesUsuarios))
                    {{-- Valida que la solicitud necesite un segundo autorizante --}}
                    @if(count($solicitud->subarea->autorizacionesUsuarios) > 0)
                      {{-- si la solicitud ya cuanta con dos firmas entonces mostrar --}}
                      @if($solicitud->solicitud_primera_firma != null && $solicitud->solicitud_segunda_firma != null)
                        <tr>
                          <td>{{ $solicitud->solicitud_id }}</td>
                          <td>{{ $solicitud->created_at }}</td>
                          <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                          <td>
                            @if($solicitud->tipo_id == 1)
                              Material o Insumo
                            @elseif($solicitud->tipo_id == 2)
                              Servicio
                            @endif
                          </td>
                          <td>
                            {{ $solicitud->area->area_nombre }}
                          </td>
                          <td>
                            @if($solicitud->subarea != null) 
                              {{ $solicitud->subarea->subarea_nombre }}
                            @endif
                          </td>
                          <td>
                            {{$solicitud->updated_at}}
                          </td>
                          <td>
                            <a href="{{ route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1']) }}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                            @if(Auth::user()->rol_sia == 'Administrador')
                              <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                            @endif
                            <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                          </td>
                        </tr>
                        @php
                          $total_procesamiento++;
                        @endphp
                      @endif
                    {{-- Si la solicitud no necesita un segundo autorizante --}}
                    @else
                      {{-- si la solicitud ya cuanta con una firmas entonces mostrar --}}
                      @if($solicitud->solicitud_primera_firma != null)
                        <tr>
                          <td>{{ $solicitud->solicitud_id }}</td>
                          <td>{{ $solicitud->created_at }}</td>
                          <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                          <td>
                            @if($solicitud->tipo_id == 1)
                              Material o Insumo
                            @elseif($solicitud->tipo_id == 2)
                              Servicio
                            @endif
                          </td>
                          <td>
                            {{ $solicitud->area->area_nombre }}
                          </td>
                          <td>
                            @if($solicitud->subarea != null) 
                              {{ $solicitud->subarea->subarea_nombre }}
                            @endif
                          </td>
                          <td>
                            {{$solicitud->updated_at}}
                          </td>
                          <td>
                            <a href="{{ route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1']) }}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                            @if(Auth::user()->rol_sia == 'Administrador')
                              <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                            @endif
                            <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                          </td>
                        </tr>
                        @php
                          $total_procesamiento++;
                        @endphp
                      @endif
                    @endif
                  {{-- si la solicitud debe tener 2 firmas --}}
                  @elseif(!empty($solicitud->area->autorizacionesUsuarios) && empty($solicitud->subarea->autorizacionesUsuarios))
                    {{-- Valida que la solicitud ya tenga la primera firma --}}
                    @if($solicitud->solicitud_primera_firma != null)
                      <tr>
                        <td>{{ $solicitud->solicitud_id }}</td>
                        <td>{{ $solicitud->created_at }}</td>
                        <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                        <td>
                          @if($solicitud->tipo_id == 1)
                            Material o Insumo
                          @elseif($solicitud->tipo_id == 2)
                            Servicio
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->area->area_nombre }}
                        </td>
                        <td>
                          @if($solicitud->subarea != null) 
                            {{ $solicitud->subarea->subarea_nombre }}
                          @endif
                        </td>                       
                        <td>
                          {{$solicitud->updated_at}}
                        </td>
                        <td>
                          <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                          @if(Auth::user()->rol_sia == 'Administrador')
                            <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                          @endif
                          <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                        </td>
                      </tr>
                      @php
                        $total_procesamiento++;
                      @endphp
                    @endif
                    {{-- Fin validación primera firma --}}
                  @endif
                  {{-- Fin, Valida que el usuario tenga rol de Manager en el módulo --}}

                {{-- Valida que el usuario sea revisor --}}
                @elseif(Auth::user()->rol_modulo == "Revisor")
                  {{-- si la solicitud debe ser revisada --}}
                  @php
                    $solicitud = App\Model\Compras\Solicitud::find($solicitud->solicitud_id);
                  @endphp
                  @if(!empty($solicitud->area->revisionesUsuarios) || !empty($solicitud->subarea->revisionesUsuarios))
                    @if($solicitud->solicitud_primera_revision != Auth::user()->id && $solicitud->solicitud_segunda_revision != Auth::user()->id && $solicitud->solicitud_tercera_revision != Auth::user()->id)
                      <tr>
                        <td>{{ $solicitud->solicitud_id }}</td>
                        <td>{{ $solicitud->created_at }}</td>
                        <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                        <td>
                          @if($solicitud->tipo_id == 1)
                            Material o Insumo
                          @elseif($solicitud->tipo_id == 2)
                            Servicio
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->area->area_nombre }}
                        </td>
                        <td>
                          @if($solicitud->subarea != null)
                            {{ $solicitud->subarea->subarea_nombre }}
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->updated_at  }}
                        </td>
                        <td>
                          <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                          @if(Auth::user()->rol_sia == 'Administrador')
                            <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                          @endif
                          <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                        </td>
                      </tr>
                      @php
                        $total_procesamiento++;
                      @endphp
                    @endif
                  @endif
                {{-- Valida que el usuario sea revisor --}}
                @elseif(Auth::user()->rol_modulo == "Super Revisor")
                  {{-- si la solicitud debe ser revisada --}}
                  @php
                    $solicitud = App\Model\Compras\Solicitud::find($solicitud->solicitud_id);
                  @endphp
                  @if($solicitud->solicitud_primera_revision != Auth::user()->id && $solicitud->solicitud_segunda_revision != Auth::user()->id && $solicitud->solicitud_tercera_revision != Auth::user()->id)
                    <tr>
                      <td>{{ $solicitud->solicitud_id }}</td>
                      <td>{{ $solicitud->created_at }}</td>
                      <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                      <td>
                        @if($solicitud->tipo_id == 1)
                          Material o Insumo
                        @elseif($solicitud->tipo_id == 2)
                          Servicio
                        @endif
                      </td>
                      <td>
                        {{ $solicitud->area->area_nombre }}
                      </td>
                      <td>
                        @if($solicitud->subarea != null)
                          {{ $solicitud->subarea->subarea_nombre }}
                        @endif
                      </td>
                      <td>
                        {{ $solicitud->updated_at  }}
                      </td>
                      <td>
                        <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                        @if(Auth::user()->rol_sia == 'Administrador')
                          <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                        @endif
                        <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                      </td>
                    </tr>
                    @php
                      $total_procesamiento++;
                    @endphp
                  @endif
                {{-- Valida que el usuario sea revisor --}}
                @elseif(Auth::user()->rol_modulo == "Autorizante")
                  {{-- si la solicitud debe ser revisada --}}
                  @php
                    $solicitud = App\Model\Compras\Solicitud::find($solicitud->solicitud_id);
                  @endphp
                  @if(!empty($solicitud->area->autorizacionesUsuarios) && !empty($solicitud->subarea->autorizacionesUsuarios))
                    @if($solicitud->solicitud_primera_firma != Auth::user()->id && $solicitud->solicitud_segunda_firma != Auth::user()->id)
                      <tr>
                        <td>{{ $solicitud->solicitud_id }}</td>
                        <td>{{ $solicitud->created_at }}</td>
                        <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                        <td>
                          @if($solicitud->tipo_id == 1)
                            Material o Insumo
                          @elseif($solicitud->tipo_id == 2)
                            Servicio
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->area->area_nombre }}
                        </td>
                        <td>
                          @if($solicitud->subarea != null)
                            {{ $solicitud->subarea->subarea_nombre }}
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->updated_at }}
                        </td>
                        <td>
                          <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                          @if(Auth::user()->rol_sia == 'Administrador')
                            <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                          @endif
                          <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                        </td>
                      </tr>
                      @php
                        $total_procesamiento++;
                      @endphp
                    @endif
                  @elseif(!empty($solicitud->area->autorizacionesUsuarios) && empty($solicitud->subarea->autorizacionesUsuarios))
                    @if($solicitud->solicitud_primera_firma != Auth::user()->id && $solicitud->solicitud_segunda_firma != Auth::user()->id)
                      <tr>
                        <td>{{ $solicitud->solicitud_id }}</td>
                        <td>{{ $solicitud->created_at }}</td>
                        <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                        <td>
                          @if($solicitud->tipo_id == 1)
                            Material o Insumo
                          @elseif($solicitud->tipo_id == 2)
                            Servicio
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->area->area_nombre }}
                        </td>
                        <td>
                          @if($solicitud->subarea != null)
                            {{ $solicitud->subarea->subarea_nombre }}
                          @endif
                        </td>
                        <td>
                          {{ $solicitud->updated_at }}
                        </td>
                        <td>
                          <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                          @if(Auth::user()->rol_sia == 'Administrador')
                            <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                          @endif
                          <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                        </td>
                      </tr>
                      @php
                        $total_procesamiento++;
                      @endphp
                    @endif
                  @endif
                @else
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{ $solicitud->area->area_nombre }}
                    </td>
                    <td>
                      @if($solicitud->subarea != null)
                        {{ $solicitud->subarea->subarea_nombre }}
                      @endif
                    </td>
                    <td>
                      {{ $solicitud->updated_at  }}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
                @endif
              @endforeach

              {{-- imprime las subareas --}}
              @if(isset($solicitudes_subareas) && !empty($solicitudes_subareas))
                @foreach($solicitudes_subareas->where('estado_id', 1) as $solicitud)
                    <tr>
                      <td>{{ $solicitud->solicitud_id }}</td>
                      <td>{{ $solicitud->created_at }}</td>
                      <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                      <td>
                        @if($solicitud->tipo_id == 1)
                          Material o Insumo
                        @elseif($solicitud->tipo_id == 2)
                          Servicio
                        @endif
                      </td>
                      <td>
                        {{$solicitud->updated_at}}
                      </td>
                      <td>
                        <a href="{{route('browse.compras.solicitudes.sign', [$sistema, $solicitud->solicitud_id, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Firmar"><span class="fa fa-pencil fa-fw"></span></a>
                        @if(Auth::user()->rol_sia == 'Administrador')
                          <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                        @endif
                        <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                      </td>
                    </tr>
                    @php
                      $total_procesamiento++;
                    @endphp
                @endforeach
              @endif  

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="8"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {{--!! $solicitudes->appends(['filtro' => '1', 'autorizante' => $autorizante])->render() !!--}}
          </div>
        </div>
      </p>
    </div>
    {{-- Fin Tab Procesamiento --}}

    {{-- Tab En estudio --}}
    <div id="comite" class="w3-container city" style="display:none">
      <p>
        <div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              @foreach($solicitudes->where('estado_id', 2) as $solicitud)
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{$solicitud->updated_at}}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.evaluate', [$sistema, $solicitud->solicitud_id, 'filtro' => '2'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Evaluar"><span class="fa fa-eye fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud->solicitud_id, 'filtro' => '2'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>
                      
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
              @endforeach

              @if(isset($solicitudes_subareas) && !empty($solicitudes_subareas))
                @foreach($solicitudes_subareas->where('estado_id', 2) as $solicitud)
                    <tr>
                      <td>{{ $solicitud->solicitud_id }}</td>
                      <td>{{ $solicitud->created_at }}</td>
                      <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                      <td>
                        @if($solicitud->tipo_id == 1)
                          Material o Insumo
                        @elseif($solicitud->tipo_id == 2)
                          Servicio
                        @endif
                      </td>
                      <td>
                        {{$solicitud->updated_at}}
                      </td>
                      <td>
                        <a href="{{route('browse.compras.solicitudes.evaluate', [$sistema, $solicitud->solicitud_id, 'filtro' => '2'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Evaluar"><span class="fa fa-eye fa-fw"></span></a>
                        @if(Auth::user()->rol_sia == 'Administrador')
                          <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                        @endif
                        <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                      </td>
                    </tr>
                    @php
                      $total_procesamiento++;
                    @endphp
                @endforeach
              @endif

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="6"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {{--!! $solicitudes->appends(['filtro' => '2', 'autorizante' => $autorizante])->render() !!--}}
          </div>
        </div>
      </p>
    </div>

    <div id="historial" class="w3-container city" style="display:none">
      <p>
        <div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              @foreach($solicitudes->whereIn('estado_id', [5,6]) as $solicitud)
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{$solicitud->updated_at}}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.show', [$sistema, $solicitud->solicitud_id, 'filtro' => '6'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud->solicitud_id, 'filtro' => '6'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>    
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      {{-- <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a> --}}
                      <a href="{{ route('browse.compras.solicitudes.download.pdf', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-file-pdf-o fa-fw"></i></a>
                      @if($solicitud->estado_id == 5)
                        <span class="fa fa-times-circle fa-fw w3-text-red"></span>
                      @elseif($solicitud->estado_id == 6)
                        <span class="fa fa-check-circle fa-fw w3-text-green"></span>
                      @endif
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
              @endforeach

              @if(isset($solicitudes_subareas) && !empty($solicitudes_subareas))
                @foreach($solicitudes_subareas->whereIn('estado_id', [5,6]) as $solicitud)
                    <tr>
                      <td>{{ $solicitud->solicitud_id }}</td>
                      <td>{{ $solicitud->created_at }}</td>
                      <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                      <td>
                        @if($solicitud->tipo_id == 1)
                          Material o Insumo
                        @elseif($solicitud->tipo_id == 2)
                          Servicio
                        @endif
                      </td>
                      <td>
                        {{$solicitud->updated_at}}
                      </td>
                      <td>
                        <a href="{{route('browse.compras.solicitudes.evaluate', [$sistema, $solicitud->solicitud_id, 'filtro' => '6'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Evaluar"><span class="fa fa-eye fa-fw"></span></a>
                        @if(Auth::user()->rol_sia == 'Administrador')
                          <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud->solicitud_id])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                        @endif
                        {{--<a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud->solicitud_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>--}}
                        <a href="{{ url('/compras/solicitudes/download/pdf', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-file-pdf-o fa-fw"></i></a>
                      </td>
                    </tr>
                    @php
                      $total_procesamiento++;
                    @endphp
                @endforeach
              @endif

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="6"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {{--!! $solicitudes->appends(['filtro' => '6', 'autorizante' => $autorizante])->render() !!--}}
          </div>
        </div>
      </p>
    </div>
@else
  @php
    Alert::error('Acceso denegado. Usuario no autorizado.')->html()->persistent('OK');
  @endphp
  <div class="w3-content">
    <div class="w3-panel w3-center">
      <h1><span class="fa fa-frown-o fa-5x"></span></h1>
      <h1>¡Oops!</h1>
    </div>
  </div>
@endif
<script>
document.getElementById({{$filtro}}).click();

function openTab(evt, tabName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-border-{{$sistema->sistema_colorclass}}", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " w3-border-{{$sistema->sistema_colorclass}}";
}
</script>
@endsection

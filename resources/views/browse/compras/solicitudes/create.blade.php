@extends('browse.compras.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Crear
@endsection

@section('content-modulo')
  @php
    extract($_REQUEST);

    if(!isset($area))
    {
      $area = null;
    }
  @endphp
  <div class="row">
  		<div class="col-sm-12">
  			<div class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} w3-text-grey">
          <span onclick="this.parentElement.style.display='none'" class="fa fa-remove w3-button w3-right w3-hover-none"></span>
  				<h3>Actualizar Datos</h3>
          <p>Si tus datos son incorrectos, ingresa al servicio <b>Correos Institucionales > <span class="fa fa-user fa-fw"></span> Mi Cuenta</b> para actualizar tu información e inténtalo nuevamente.</p>
  			</div>
  		</div>
  	</div>

    {!! Form::open(['route' => ['browse.compras.solicitudes.store', $sistema], 'method' => 'post', 'files' => 'true',]) !!}
      <div class="row w3-text-{{$sistema->sistema_colorclass}}">
      	<div class="col-sm-12 text-center">
      		<h2><b>Formulario Requerimiento de <br>Materiales, Insumos y Servicios</b></h2>
      	</div>
      </div>
      <br>
      <div class="row" style="display: none;">
      	<div class="col-sm-6">
      		<div class="form-group">
      			<label>Código</label>
      			<input type="text" name="solicitud_codigo_formulario" id="codigo_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="6A-7.2.4.9.4" readonly>
      		</div>
      	</div>
      	<div class="col-sm-6">
      		<div class="form-group">
      			<label>Versión</label>
      			<input type="text" name="solicitud_version_formulario" id="version_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="1.0" readonly>
      		</div>
      	</div>
      </div>

      <div class="row">
      	<div class="col-sm-12">
      		<div class="w3-panel w3-pale-red w3-leftbar w3-border-red w3-padding"><p><b>Nota:</b> los campos marcados con asterisco (*) son campos obligatorios.</p></div>
      		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
            <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos del Solicitante</b></h4>
          </div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Nombres<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_nombres_solicitante', $cuenta->cuenta_primer_apellido.' '.$cuenta->cuenta_segundo_apellido.' '.$cuenta->cuenta_primer_nombre.' '.$cuenta->cuenta_segundo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Nombres y Apellidos', 'required' => true]) !!}
      		</div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-6">
      		<div class="form-group">
      			<label>E-mail<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_email_solicitante', Auth::user()->email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '100', 'placeholder' => 'E-mail', 'required' => true, 'readonly']) !!}
      		</div>
      	</div>
      	<div class="col-sm-4">
      		<div class="form-group">
      			<label>Teléfono Fijo o Móvil<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_telefono_solicitante', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Teléfono', 'required' => true]) !!}
      		</div>
      	</div>
      	<div class="col-sm-2">
      		<div class="form-group">
      			<label>Extensión</label>
      			{!! Form::text('solicitud_extension_solicitante', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Extensión']) !!}
      		</div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Jefe Inmediato<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_nombres_responsable', old('solicitud_nombres_responsable'), ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'solicitud_nombres_responsable', 'maxlength' => '100', 'placeholder' => 'Jefe Inmediato', 'required']) !!}
      		</div>
      	</div>
      </div>
    	<hr>

    		<div class="row">
    			<div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
    				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Datos de la Solicitud</b></h4>
            </div>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Área\Dependencia\Facultad<span style="color: red;">*</span></label>
    					{!! Form::select('area_id', $areas->pluck('area_nombre', 'area_id'), $area, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'area', 'onchange' => 'selectSubarea(null, this.value)', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    				</div>
    			</div>
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Subárea<span style="color: red;">*</span></label>
    					<div id="cargar_subareas">
                @if(count($subareas->where('area_id', $area)) > 0)
    						{{ Form::select('subarea_id', $subareas->where('area_id', $area)->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) }}
                @else
                {{ Form::select('subarea_id', $subareas->where('area_id', $area)->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey w3-disabled', 'placeholder' => 'Seleccionar', 'disabled']) }}
                @endif
    					</div>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="form-group">
    					<label>Tipo Solicitud<span style="color: red;">*</span></label>
              {!! Form::select('tipo_id', ['1' => 'Material o Insumo', '2' => 'Servicio'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    				</div>
    			</div>
    		</div>
    		<hr>
    		<label>Centros de Operación (Sede)<span style="color: red;">*</span></label>
        <div class="row">
    			<div class="col-sm-12">
            <table id="tabla-centro-operacion" class="table table-condensed table-striped table-responsive">
            	<thead class="w3-{{$sistema->sistema_colorclass}}">
            		<th>Código</th>
            		<th>Descripción Centro</th>
            		<th></th>
            	</thead>
            	<tbody>
            		<tr class="fila-centro-operacion w3-text-grey">
            			<td><input type="text" name="centrooperacion_id[]" id="centrooperacion_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-centro-operacion" maxlength="100" placeholder="Código Centro" readonly disabled="true"></td>
            			<td><input type="text" name="centrooperacion_nombre[]" id="centrooperacion_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-centro-operacion" maxlength="100" placeholder="Descripción Centro" readonly></td>
            			<td class="text-center eliminar-centro-operacion"><span class="fa fa-minus-circle"></span></td>
            		</tr>
              </tbody>
              </table>
              <center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modelCentroOperacion" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
    			</div>
    		</div>

    		<!-- Modal -->
    		<div id="modelCentroOperacion" class="modal fade" role="dialog">
    		  <div class="modal-dialog">

    		    <!-- Modal content-->
    		    <div class="modal-content">
    		      <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
    		        <button type="button" class="close" data-dismiss="modal">&times;</button>
    		        <h4 class="modal-title">Centros de Operación</h4>
    		      </div>
    		      <div class="modal-body">
    		        <p>
                  {!! Form::select('centrooperacion_select', $centros_operacion, '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'centrooperacion_select', 'placeholder' => 'Seleccionar']) !!}
    		        </p>
    		      </div>
    		      <div class="modal-footer">
    		      	<button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarCentroOperacion(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
    		        <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
    		      </div>
    		    </div>

    		  </div>
    		</div>
    		<hr>

        <label>Centros de Costo<span style="color: red;">*</span></label>
        <span class="help-block"><b>Atención:</b> Si el Centro de Costo de la solicitud es compartido, por favor selecciona los demás centros de costo que apliquen.</span><br>
        <div class="row">
          <div class="col-sm-12">
            <table id="tabla-centro" class="table table-condensed table-striped table-responsive">
              <thead class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Descripción Centro</th>
                <th></th>
              </thead>
              <tbody>
                <tr class="fila-centro w3-text-grey">
                  <td><input type="text" name="centrocostos_id[]"  id="centrocostos_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="10" placeholder="Código Centro" readonly disabled="true"></td>
                  <td><input type="text" name="centrocostos_nombre[]" id="centrocostos_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="100" placeholder="Descripción Centro" readonly></td>
                  <td class="text-center eliminar-centro"><span class="fa fa-minus-circle"></span></td>
                </tr>
              </tbody>
              </table>
              <center><a href="#" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modalCentroCosto" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
          </div>
        </div>

        <!-- Modal -->
        <div id="modalCentroCosto" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Centros de Costos</h4>
              </div>
              <div class="modal-body">
                <p>
                  {!! Form::select('centrocostos_select', $centros_costos->pluck('centrocosto_codigo', 'centrocosto_id'), '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'centrocostos_select', 'placeholder' => 'Seleccionar']) !!}
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarCentroCostos(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
                <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
              </div>
            </div>

          </div>
        </div>
        <hr>

        <label>Proyectos<span style="color: red;">*</span></label>
        <div class="row">
          <div class="col-sm-12">
            <table id="tabla-proyectos" class="table table-condensed table-striped table-responsive">
              <thead class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Descripción Proyecto</th>
                <th></th>
              </thead>
              <tbody>
                <tr class="fila-proyecto w3-text-grey">
                  <td><input type="text" name="proyecto_id[]" id="proyecto_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="100" placeholder="Código Proyecto" readonly disabled="true"></td>
                  <td><input type="text" name="proyecto_nombre[]" id="proyecto_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-proyecto" maxlength="100" placeholder="Proyecto" readonly></td>
                  <td class="text-center eliminar-proyecto"><span class="fa fa-minus-circle"></span></td>
                </tr>
              </tbody>
              </table>
              <center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modelProyecto" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
          </div>
        </div>

        <!-- Modal -->
        <div id="modelProyecto" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Proyectos</h4>
              </div>
              <div class="modal-body">
                <p>
                  {!! Form::select('proyecto_select', $proyectos->pluck('proyecto_codigo', 'proyecto_id'), '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'proyecto_select', 'placeholder' => 'Seleccionar']) !!}
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarProyecto(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
                <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
              </div>
            </div>

          </div>
        </div>
    		<hr>

        <div class="row">
        	<div class="col-sm-12">
        		<label>Insumos, Materiales o Servicios</label>
        		<span class="help-block">
                <ul>
                  <li>En el campo de <b>Costo Estimado</b> no uses separadores de miles, millones u otra cantidad. Tampoco uses decimales, solo cifras redondeadas.</li>
                  <li>Para añadir más de un insumo, material o servicio, haz clic en el ícono (+).</li>
              </ul>
            </span>
        		<div class="w3-responsive">
        			<table id="tabla-insumos" class="w3-table-all">
        				<thead>
        					<tr class="w3-{{$sistema->sistema_colorclass}}">
                    <th></th>
          					<th>Descripción Referencia</th>
          					<th>Unidad de Medida</th>
          					<th>Cantidad Solicitada</th>
          					<th>Especificaciones y/o Características</th>
          					<th>Costo Estimado</th>
          					<th></th>
                  </tr>
        				</thead>
                <tbody>
        					<tr class="fila-insumo w3-text-grey">
        			      <td>-</td>
        						<td>
                      <input type="text" name="solicitudinsumo_referencia[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="Referencia" disabled>
                    </td>
        						<td>
                      {{--<input type="text" name="solicitudinsumo_medida[]" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm medida" maxlength="10" placeholder="Unidad de Medida" title="Und, Ton, Kg, Lb, gr, Lt, ml, mt, etc." disabled>--}}
                      {{ form::select('solicitudinsumo_medida[]', $medidas->pluck('umedida_nombre', 'umedida_codigo'), null, ['id' => 'uni_item', 'class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-sm medida', 'placeholder' => 'Seleccionar', 'disabled' => 'true']) }}
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_cantidad[]" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm cantidad" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}" disabled>
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_caracteristica[]" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm caracteristica" maxlength="100" placeholder="Especificaciones" disabled>
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_costo[]" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm costo" maxlength="20" placeholder="Costo $" disabled>
                    </td>
        						<td class="text-center eliminar-insumo">
                      <span class="fa fa-minus-circle"></span>
                    </td>
        					</tr>
        				</tbody>
        			</table>
        		</div>
            <br>
        		<center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" onclick="agregarFilaInsumos(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
        	</div>
        </div>
        <hr>

        <div class="row">
        	<div class="col-sm-12">
        		<div class="form-group">
        			<label>Anexos (opcional) <span class="fa fa-paperclip fa-fw"></span> (Máx. 50MB)</label>
              {!! Form::file('solicitud_presupuesto', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey']) !!}
        		</div>
        	</div>
        </div>
    		<hr>

        <div class="row">
        	<div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
    				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>3. Justificación</b></h4>
            </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<div class="form-group">
        			<label>Justificación<span style="color: red;">*</span></label>
              {!! Form::textarea('solicitud_justificacion', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'rows' => '6', 'maxlength' => '2000', 'placeholder' => 'Escribe la justificación de tu solicitud (2000 caracteres).', 'required']) !!}
        		</div>
        	</div>
        </div>

        <div class="w3-center">
    		    <button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-floppy-o"></span> Guardar</button> <button type="reset" id="limpiar" class="w3-btn w3-red"><span class="fa fa-close fa-fw"></span> Limpiar</button>
        </div>
    {!! Form::close() !!}
@endsection

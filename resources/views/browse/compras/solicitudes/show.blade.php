@extends('browse.compras.menu')

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  {!! Form::open(['route' => ['browse.compras.solicitudes.store', $sistema], 'method' => 'post', 'files' => 'true',]) !!}
    <div class="row w3-text-{{$sistema->sistema_colorclass}}">
    	<div class="col-sm-3 text-center">
    		<h3><img src="{{ asset('images/Unicatolica-color-azul-vertical.png') }}" alt="logo-unicatolica" width="150"></h3>
    	</div>
    	<div class="col-sm-6 text-center">
    		<h2><b>Formulario Requerimiento de <br>Materiales, Insumos y Servicios</b></h2>
    	</div>
    	<div class="col-sm-3 text-center w3-text-grey w3-hide-small">
    		<div class="row text-left">
    			<div class="col-sm-6"><b>Código</b><br>6A-7.2.4.9.4</div>
    			<div class="col-sm-6"><b>Versión</b><br>1.0</div>
    		</div>
    		<div class="row text-left">
    			<div class="col-sm-12"><b>Fecha Vigencia</b><br></div>
    		</div>
    	</div>
    </div>
    <br>
    <div class="row" style="display: none;">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Código</label>
    			<input type="text" name="solicitud_codigo_formulario" id="codigo_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="6A-7.2.4.9.4" readonly>
    		</div>
    	</div>
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Versión</label>
    			<input type="text" name="solicitud_version_formulario" id="version_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="1.0" readonly>
    		</div>
    	</div>
    </div>

    <div class="row">
    	<div class="col-sm-12">
    		<div class="w3-panel w3-pale-red w3-leftbar w3-border-red"><p><b>Nota:</b> los campos marcados con asterisco (*) son campos obligatorios.</p></div>
    		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
          <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos del Solicitante</b></h4>
        </div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
    		<div class="form-group">
    			<label>Nombres y Apellidos<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_nombres_solicitante', $solicitud->solicitud_nombres_solicitante, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Nombres y Apellidos', 'required' => 'true', 'disabled']) !!}
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>E-mail<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_email_solicitante', $solicitud->solicitud_email_solicitante, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'maxlength' => '100', 'placeholder' => 'E-mail', 'required' => 'true', 'disabled']) !!}
    		</div>
    	</div>
    	<div class="col-sm-4">
    		<div class="form-group">
    			<label>Teléfono Fijo o Móvil<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_telefono_solicitante', $solicitud->solicitud_telefono_solicitante, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Teléfono', 'required' => 'true', 'disabled']) !!}
    		</div>
    	</div>
    	<div class="col-sm-2">
    		<div class="form-group">
    			<label>Extensión</label>
    			{!! Form::text('solicitud_extension_solicitante', $solicitud->solicitud_extension_solicitante, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Extensión', 'disabled']) !!}
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
    		<div class="form-group">
    			<label>Jefe Inmediato<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_nombres_responsable', $solicitud->solicitud_nombres_responsable, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Jefe Inmediato', 'required' => 'true', 'disabled']) !!}
    		</div>
    	</div>
    </div>
  	<hr>

  		<div class="row">
  			<div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
  				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Datos de la Solicitud</b></h4>
          </div>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-sm-5">
  				<div class="form-group">
  					<label>Área\Dependencia\Facultad<span style="color: red;">*</span></label>
  					{!! Form::select('area_id', $areas, $solicitud->area_id, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) !!}
  				</div>
  			</div>
  			<div class="col-sm-5">
  				<div class="form-group">
  					<label>Subárea<span style="color: red;">*</span></label>
  					<div id="cargar_subareas">
  						{!! Form::select('subarea_id', $subareas, $solicitud->subarea_id, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) !!}
  					</div>
  				</div>
  			</div>
  			<div class="col-sm-2">
  				<div class="form-group">
  					<label>Tipo Solicitud<span style="color: red;">*</span></label>
            {!! Form::select('tipo_id', ['1' => 'Material o Insumo', '2' => 'Servicio'], $solicitud->tipo_id, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) !!}
  				</div>
  			</div>
  		</div>
  		<hr>
  		<label>Centros de Operación (Sede)<span style="color: red;">*</span></label>
      <div class="row">
  			<div class="col-sm-12">
          <table id="tabla-centro-operacion" class="table table-condensed table-striped table-responsive">
          	<thead class="w3-{{$sistema->sistema_colorclass}}">
          		<th>Código</th>
          		<th>Descripción Centro</th>
          	</thead>
          	<tbody>
              @foreach ($solicitud->centrosOperacion as $centro_operacion)
                <tr class="w3-text-grey">
            			<td>
                    <input type="text" name="centrooperacion_id[]" id="centrooperacion_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-centro-operacion" maxlength="100" placeholder="Código Centro" value="{{$centro_operacion->centrooperacion_codigo}}" readonly disabled>
                  </td>
            			<td>
                    <input type="text" name="centrooperacion_nombre[]" id="centrooperacion_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-centro-operacion" maxlength="100" placeholder="Descripción Centro" value="{{$centro_operacion->centrooperacion_nombre}}" readonly disabled>
                  </td>
            		</tr>
              @endforeach
            </tbody>
            </table>
  			</div>
  		</div>
  		<hr>

  		<label>Centros de Costo<span style="color: red;">*</span></label>
  		<span class="help-block"><b>Atención:</b> Si el Centro de Costo de la solicitud es compartido, por favor selecciona los demás centros de costo que apliquen.</span><br>
      <div class="row">
  			<div class="col-sm-12">
          <table id="tabla-centro" class="table table-condensed table-striped table-responsive">
          	<thead class="w3-{{$sistema->sistema_colorclass}}">
          		<th>Código</th>
          		<th>Descripción Centro</th>
          	</thead>
          	<tbody>
               @foreach ($solicitud->centrosCostos as $centro_costos)
                 <tr class="w3-text-grey">
             			 <td>
                     <input type="text" name="centrocostos_id[]"  id="centrocostos_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="10" placeholder="Código Centro" value="{{$centro_costos->centrocosto_codigo}}" readonly disabled>
                   </td>
             			 <td>
                     <input type="text" name="centrocostos_nombre[]" id="centrocostos_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="100" placeholder="Descripción Centro" value="{{$centro_costos->centrocosto_nombre}}" readonly disabled>
                   </td>
             		 </tr>
               @endforeach
            </tbody>
          </table>
  			</div>
  		</div>
      <hr>

      <label>Proyectos<span style="color: red;">*</span></label>
      <div class="row">
        <div class="col-sm-12">
          <table id="tabla-proyectos" class="table table-condensed table-striped table-responsive">
            <thead class="w3-{{$sistema->sistema_colorclass}}">
              <th>Código</th>
              <th>Descripción Proyecto</th>
            </thead>
            <tbody>
                @foreach ($solicitud->proyectos as $proyecto)
                 <tr class="w3-text-grey">
                   <td>
                     <input type="text" name="proyecto_id[]"  id="proyecto_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="20" placeholder="Código Centro" value="{{$proyecto->proyecto_codigo}}" readonly disabled>
                   </td>
                   <td>
                     <input type="text" name="proyecto_nombre[]" id="proyecto_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="100" placeholder="Descripción Centro" value="{{$proyecto->proyecto_nombre}}" readonly disabled>
                   </td>
                 </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <hr>

      <div class="row">
      	<div class="col-sm-12">
      		<label>Insumos, Materiales o Servicios</label>
      		<span class="help-block">
            <ul>
                <li>En el campo de <b>Costo Estimado</b> no uses separadores de miles, millones y otra cantidad, solo usa el punto (.) para indicar decimales. Ejemplo: 100235.16</li>
                <li>Para añadir más de un insumo, material o servicio, haz clic en el ícono (+).</li>
            </ul>
          </span>
      		<div class="w3-responsive">
      			<table id="tabla" class="table table-condensed table-striped table-responsive">
      				<thead class="w3-{{$sistema->sistema_colorclass}}">
      					<th></th>
      					<th>Descripción Referencia</th>
      					<th>Unidad de Medida</th>
      					<th>Cantidad Solicitada</th>
      					<th>Especificaciones y/o Características</th>
      					<th>Costo Estimado</th>
      				</thead>
              <tbody>
                @foreach ($solicitud->insumos as $insumo)
                  <tr class="w3-text-grey">
        						<td></td>
        						<td>
                      <input type="text" name="solicitudinsumo_referencia[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="Referencia" value="{{$insumo->solicitudinsumo_referencia}}" disabled>
                    </td>
        						<td>
                      {{-- <input type="text" name="solicitudinsumo_medida[]" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm medida" maxlength="10" placeholder="Unidad de Medida" title="Und, Ton, Kg, Lb, gr, Lt, ml, mt, etc." value="{{$insumo->solicitudinsumo_medida}}"> --}}
                      {{ form::select('solicitudinsumo_medida[]', $medidas->pluck('umedida_nombre', 'umedida_codigo'), $insumo->solicitudinsumo_medida, ['id' => 'uni_item', 'class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-sm medida', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) }}
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_cantidad[]" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm cantidad" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}" value="{{$insumo->solicitudinsumo_cantidad}}" disabled>
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_caracteristica[]" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm caracteristica" maxlength="100" placeholder="Especificaciones" value="{{$insumo->solicitudinsumo_caracteristica}}" disabled>
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_costo[]" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm costo" maxlength="20" placeholder="Costo $" value="{{$insumo->solicitudinsumo_costo}}" disabled>
                    </td>
        					</tr>
                @endforeach
      				</tbody>
      			</table>
      		</div>
      	</div>
      </div><br>
      <a href="{{ route('browse.compras.solicitudes.insumos.downloadxls', [$sistema, $solicitud]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><i class="fa fa-file-excel-o fa-fw"></i> Descargar</a>
      <hr>

      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Presupuesto (opcional) <span class="fa fa-paperclip fa-fw"></span> (Máx. 10MB)</label><br>
            @if($solicitud->solicitud_presupuesto)
              <a href="{{url('browse/compras/solicitudes/download/budget', [$sistema, $solicitud])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-col sm-12"><i class="fa fa-download fa-fw"></i> Descargar</a>
            @else
              <div class="w3-container">
                <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
                <h3></h3>
                <p>La solictud no tiene presupuesto adjunto.</p>
                </div>
              </div>
            @endif
      		</div>
      	</div>
      </div>
  		<hr>

      <div class="row">
      	<div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
  				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>3. Justificación</b></h4>
          </div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Justificación<span style="color: red;">*</span></label>
            {!! Form::textarea('solicitud_justificacion', $solicitud->solicitud_justificacion, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'rows' => '6', 'maxlength' => '255', 'placeholder' => 'Escribe la justificación de tu solicitud (2000 caracteres).', 'required', 'disabled']) !!}
      		</div>
      	</div>
      </div>
      <hr>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
                <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>4. Autorizaciones</b></h4>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          @php
            if($solicitud->solicitud_primera_firma != null)
            {
              $usuario = \App\Usuario::find($solicitud->solicitud_primera_firma);
              $usuario_cuenta = $usuario->cuenta;

              echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
              echo $usuario->email."<br>".$solicitud->solicitud_fecha_primera_firma."</center>";
            }
            else
            {
              echo "-";
            }
          @endphp
        </div>
        <div class="col-sm-4">
          @php
            if($solicitud->solicitud_segunda_firma != null)
            {
              $usuario = \App\Usuario::find($solicitud->solicitud_segunda_firma);
              $usuario_cuenta = $usuario->cuenta;

              echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
              echo $usuario->email."<br>".$solicitud->solicitud_fecha_segunda_firma."</center>";
            }
            else
            {
              echo "-";
            }
          @endphp
        </div>
        <div class="col-sm-4">
          @php
            if($solicitud->solicitud_tercera_firma != null)
            {
              $usuario = \App\Usuario::find($solicitud->solicitud_tercera_firma);
              $usuario_cuenta = $usuario->cuenta;

              echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
              echo $usuario->email."<br>".$solicitud->solicitud_fecha_tercera_firma."</center>";
            }
            else
            {
              echo "-";
            }
          @endphp
        </div>
      </div>
      <hr>

      <div class="w3-center">
        <a href="{{url()->previous()}}" class="w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>
      </div>
  {!! Form::close() !!}

  <!--Comentarios-->
  <div class="row">
    <div class="col-sm-12">
      <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
            <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>5. Comentarios</b></h4>
      </div>
    </div>
  </div>

  <!--Modal con el formulario para crear un nuevo comentario-->
  @if($solicitud->estado_id != 6)
    <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-right" data-toggle="modal" data-target="#nuevo_comentario"><span class="fa fa-comment-o fa-fw"></span> Nuevo Comentario</button>
  @endif
    {!! Form::open(['route' => ['browse.compras.solicitudes.comentarios.store', $sistema, $solicitud, 'filtro' => $filtro], 'method' => 'post', 'files' => 'true',]) !!}
      <!-- Modal -->
      <div id="nuevo_comentario" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><span class="fa fa-comment-o fa-fw"></span> Nuevo Comentario</h4>
            </div>
            <div class="modal-body">
              <span class="help-block">Escriba a continuación su comentario.</span>
              {!! Form::textarea('solicitudcomentario_comentario', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'maxlength' => '2000', 'placeholder' => 'Escriba su comentario. (2.000 caracteres)', 'required' => 'required']) !!}
            </div>
            <div class="modal-footer">
              <button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-send fa-fw"></span> Publicar</button>
              <button type="reset" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-paint-brush fa-fw"></span> Limpiar</button>  
              <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}

  @if(empty($solicitud->comentarios->toArray()))
    <div class="w3-panel w3-pale-red">
      <p class="w3-padding">
        <i>
        La solicitud aún no tiene comentarios.
        </i>
      </p>
    </div>
  @else
    @php
      $myaccount = \App\Model\Correos\Cuenta::find(\Auth::user()->id);
      $myaccount_extension = $myaccount->extension;
    @endphp
      
    @foreach($solicitud->comentarios as $comentario)
      @php
        $usuario = \App\Usuario::find($comentario->usuario_id);
        $usuario_cuenta = $usuario->cuenta;

        $cuenta = \App\Model\Correos\Cuenta::find($usuario->id);
        $cuenta_extension = $cuenta->extension;
        $cuenta_subarea = $cuenta->subarea;
        $cuenta_tipo = $cuenta->tipo;
      @endphp
      <div class="w3-panel w3-light-grey">
        <span style="font-size:100px;line-height:0.5em;opacity:0.2">❝</span>
        <p class="w3-medium" style="margin-top:-40px">
          <i>
            @php
              echo nl2br($comentario->solicitudcomentario_comentario);
            @endphp
          </i><br>
          <span class="w3-right" style="font-size:12px;""><b>publicado {{$comentario->created_at}} por <a href="javascript:void(0)" onclick="document.getElementById('perfil_{{$usuario->id}}').style.display='block'">{{$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido}}</a></b></span>
        </p>
      </div>
      <div id="perfil_{{$usuario->id}}" class="w3-modal w3-animate-zoom">
        <div class="w3-modal-content">
          <div class="w3-container w3-padding">
            <span onclick="document.getElementById('perfil_{{$usuario->id}}').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
            <div class="w3-row-padding w3-{{$sistema->sistema_colorclass}}">
              <div class="w3-col s12 w3-center">
                <h4><b><span class="fa fa-user fa-fw"></span>Perfil</b></h4>
              </div>
            </div><br>  
            <div class="w3-row-padding">
              <div class="w3-col s4">
                <center>
                  @if($cuenta->cuenta_genero == 'M')
                    <img src="{{asset('images/avatar/img_avatar3.png')}}" class="w3-image" alt="avatar_m">
                  @elseif($cuenta->cuenta_genero == 'F')
                    <img src="{{asset('images/avatar/img_avatar4.png')}}" class="w3-image" alt="avatar_f">
                  @endif
                </center>
              </div>
              <div class="w3-col s8">
                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Nombres</b>: {{$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Área</b>: {{$cuenta->subarea->subarea_nombre}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Cuenta</b>: {{$usuario->cuenta->cuenta_cuenta}} <b>Tipo</b>: {{$cuenta->tipo->tipo_nombre}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Teléfono</b>: (+57) (2) 3120038 <b>Extensión</b>: @if($myaccount->extension == null || $cuenta->extension == null)<span class="w3-red">No se puede hacer llamadas a este usuario</span>@else{{$cuenta->extension->extension_extension}}<br><br><a href="http://10.0.0.3/ippbx/index.php?r=peers/dircall&destino={{$cuenta->extension->extension_extension}}&origen={{$myaccount->extension->extension_extension}}" title="Llamar" class="w3-btn w3-green" target="open_call"><span class="fa fa-phone fa-fw"></span> Llamar</a><br><br>@endif
                      <iframe name="open_call" id="open_call" hidden></iframe>
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Rol SIA</b>: {{$usuario->rol_sia}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Rol Módulo</b>: {{$usuario->rol_modulo}}
                    </div>    
                  </div>
              </div>
            </div><br>
          </div>
        </div>
      </div>
      <hr>
    @endforeach
  @endif
@endsection

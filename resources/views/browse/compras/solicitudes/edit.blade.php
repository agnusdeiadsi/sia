@extends('browse.compras.menu')

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  {!! Form::open(['route' => ['browse.compras.solicitudes.update', $sistema, $solicitud, 'filtro' => $filtro], 'method' => 'put', 'files' => 'true',]) !!}
    <div class="row w3-text-{{$sistema->sistema_colorclass}}">
    	<div class="col-sm-12 text-center">
    		<h2><b>Formulario Solicitud de <br>Materiales, Insumos y Servicios</b></h2>
    	</div>
    </div>
    <br>
    <div class="row" style="display: none;">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Código</label>
    			<input type="text" name="solicitud_codigo_formulario" id="codigo_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="6A-7.2.4.9.4" readonly>
    		</div>
    	</div>
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Versión</label>
    			<input type="text" name="solicitud_version_formulario" id="version_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="1.0" readonly>
    		</div>
    	</div>
    </div>

    <div class="row">
    	<div class="col-sm-12">
    		<div class="w3-panel w3-pale-red w3-leftbar w3-border-red w3-padding"><p><b>Nota:</b> los campos marcados con asterisco (*) son campos obligatorios.</p></div>
    		<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
          <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>1. Datos del Solicitante</b></h4>
        </div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
    		<div class="form-group">
    			<label>Nombres y Apellidos<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_nombres_solicitante', $solicitud->solicitud_nombres_solicitante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Nombres y Apellidos', 'required' => true, 'readonly']) !!}
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>E-mail<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_email_solicitante', $solicitud->solicitud_email_solicitante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '100', 'placeholder' => 'E-mail', 'required' => true, 'readonly']) !!}
    		</div>
    	</div>
    	<div class="col-sm-4">
    		<div class="form-group">
    			<label>Teléfono Fijo o Móvil<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_telefono_solicitante', $solicitud->solicitud_telefono_solicitante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Teléfono', 'required' => true]) !!}
    		</div>
    	</div>
    	<div class="col-sm-2">
    		<div class="form-group">
    			<label>Extensión</label>
    			{!! Form::text('solicitud_extension_solicitante', $solicitud->solicitud_extension_solicitante, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Extensión']) !!}
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
    		<div class="form-group">
    			<label>Jefe Inmediato<span style="color: red;">*</span></label>
          {!! Form::text('solicitud_nombres_responsable', $solicitud->solicitud_nombres_responsable, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Jefe Inmediato', 'required' => true]) !!}
    		</div>
    	</div>
    </div>
  	<hr>

  		<div class="row">
  			<div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
  				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>2. Datos de la Solicitud</b></h4>
          </div>
  			</div>
  		</div>
  		<div class="row">
        @if(!isset($area))
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Área\Departamento\Dependencia\Facultad<span style="color: red;">*</span></label>
    					{!! Form::select('area_id', $areas->pluck('area_nombre', 'area_id'), $solicitud->area_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'area', 'onchange' => 'selectSubarea('.$filtro.', this.value)', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    				</div>
    			</div>
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Subárea<span style="color: red;">*</span></label>
    					<div id="cargar_subareas">
    						{!! Form::select('subarea_id', $subareas->pluck('subarea_nombre', 'subarea_id'), $solicitud->subarea_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    					</div>
    				</div>
    			</div>
        @else
          <div class="col-sm-5">
            <div class="form-group">
              <label>Área\Dependencia\Facultad<span style="color: red;">*</span></label>
              {!! Form::select('area_id', $areas->pluck('area_nombre', 'area_id'), $area, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'area', 'onchange' => 'selectSubarea('.$filtro.', this.value)', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
            </div>
          </div>
          <div class="col-sm-5">
            <div class="form-group">
              <label>Subárea<span style="color: red;">*</span></label>
              <div id="cargar_subareas">
                @if(count($subareas->where('area_id', $area)) > 0)
                {{ Form::select('subarea_id', $subareas->where('area_id', $area)->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) }}
                @else
                {{ Form::select('subarea_id', $subareas->where('area_id', $area)->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey w3-disabled', 'placeholder' => 'Seleccionar', 'disabled']) }}
                @endif
              </div>
            </div>
          </div>
        @endif
  			<div class="col-sm-2">
  				<div class="form-group">
  					<label>Tipo Solicitud<span style="color: red;">*</span></label>
            {!! Form::select('tipo_id', ['1' => 'Material o Insumo', '2' => 'Servicio'], $solicitud->tipo_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'required']) !!}
  				</div>
  			</div>
  		</div>
  		<hr>
  		<label>Centros de Operación (Sede)<span style="color: red;">*</span></label>
      <div class="row">
  			<div class="col-sm-12">
          <table id="tabla-centro-operacion" class="table table-condensed table-striped table-responsive">
          	<thead class="w3-{{$sistema->sistema_colorclass}}">
          		<th>Código</th>
          		<th>Descripción Centro</th>
          		<th></th>
          	</thead>
          	<tbody>
          		<tr class="fila-centro-operacion w3-text-grey">
          			<td>
                  <input type="text" name="centrooperacion_id[]" id="centrooperacion_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-centro-operacion" maxlength="100" placeholder="Código Centro" readonly disabled="true">
                </td>
          			<td>
                  <input type="text" name="centrooperacion_nombre[]" id="centrooperacion_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-centro-operacion" maxlength="100" placeholder="Descripción Centro" readonly>
                </td>
          			<td class="text-center eliminar-centro-operacion">
                  <span class="fa fa-minus-circle"></span>
                </td>
          		</tr>
              @foreach($solicitud->centrosOperacion as $centro_operacion)
                <tr class="w3-text-grey">
            			<td>
                    <input type="hidden" name="centrooperacion_id[]" id="centrooperacion_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-centro-operacion" maxlength="100" placeholder="Código Centro" value="{{$centro_operacion->centrooperacion_id}}" readonly>
                    <input type="text" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-centro-operacion" maxlength="100" placeholder="Código Centro" value="{{$centro_operacion->centrooperacion_codigo}}" readonly>
                  </td>
            			<td>
                    <input type="text" name="centrooperacion_nombre[]" id="centrooperacion_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-centro-operacion" maxlength="100" placeholder="Descripción Centro" value="{{$centro_operacion->centrooperacion_nombre}}" readonly>
                  </td>
            			<td class="text-center eliminar-centro-operacion">
                    <span class="fa fa-minus-circle"></span>
                  </td>
            		</tr>
              @endforeach
            </tbody>
            </table>
            <center><a href="#" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modelCentroOperacion" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
  			</div>
  		</div>

  		<!-- Modal -->
  		<div id="modelCentroOperacion" class="modal fade" role="dialog">
  		  <div class="modal-dialog">

  		    <!-- Modal content-->
  		    <div class="modal-content">
  		      <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
  		        <button type="button" class="close" data-dismiss="modal">&times;</button>
  		        <h4 class="modal-title">Centros de Operación</h4>
  		      </div>
  		      <div class="modal-body">
  		        <p>
                {!! Form::select('centrooperacion_select', $centros_operacion, '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'centrooperacion_select', 'placeholder' => 'Seleccionar']) !!}
  		        </p>
  		      </div>
  		      <div class="modal-footer">
  		      	<button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarCentroOperacion(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
  		        <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
  		      </div>
  		    </div>

  		  </div>
  		</div>
  		<hr>

      <label>Centros de Costo<span style="color: red;">*</span></label>
      <span class="help-block"><b>Atención:</b> Si el Centro de Costo de la solicitud es compartido, por favor selecciona los demás centros de costo que apliquen.</span><br>
      <div class="row">
        <div class="col-sm-12">
          <table id="tabla-centro" class="table table-condensed table-striped table-responsive">
            <thead class="w3-{{$sistema->sistema_colorclass}}">
              <th>Código</th>
              <th>Descripción Centro</th>
              <th></th>
            </thead>
            <tbody>
              <tr class="fila-centro w3-text-grey">
                <td><input type="text" name="centrocostos_id[]"  id="centrocostos_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="10" placeholder="Código Centro" readonly disabled="true"></td>
                <td><input type="text" name="centrocostos_nombre[]" id="centrocostos_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="100" placeholder="Descripción Centro" readonly></td>
                <td class="text-center eliminar-centro"><span class="fa fa-minus-circle"></span></td>
              </tr>

              @foreach($solicitud->centrosCostos as $centro_costos)
                 <tr class="w3-text-grey">
                   <td>
                    <input type="hidden" name="centrocostos_id[]"  id="centrocostos_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="10" placeholder="Código Centro" value="{{$centro_costos->centrocosto_id}}" readonly>

                    <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="10" placeholder="Código Centro" value="{{$centro_costos->centrocosto_codigo}}" readonly>
                   </td>
                   <td>
                     <input type="text" name="centrocostos_nombre[]" id="centrocostos_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-centro" maxlength="100" placeholder="Descripción Centro" value="{{$centro_costos->centrocosto_nombre}}" readonly>
                   </td>
                   <td class="text-center eliminar-centro">
                     <span class="fa fa-minus-circle"></span>
                   </td>
                 </tr>
              @endforeach
            </tbody>
          </table>
          <center><a href="#" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modalCentroCosto" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
        </div>
      </div>

      <!-- Modal -->
      <div id="modalCentroCosto" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Centros de Costos</h4>
            </div>
            <div class="modal-body">
              <p>
                {!! Form::select('centrocostos_select', $centros_costos->pluck('centrocosto_codigo', 'centrocosto_id'), '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'centrocostos_select', 'placeholder' => 'Seleccionar']) !!}
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarCentroCostos(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
              <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
            </div>
          </div>

        </div>
      </div>
      <hr>

      <label>Proyectos<span style="color: red;">*</span></label>
      <div class="row">
        <div class="col-sm-12">
          <table id="tabla-proyectos" class="table table-condensed table-striped table-responsive">
            <thead class="w3-{{$sistema->sistema_colorclass}}">
              <th>Código</th>
              <th>Descripción Proyecto</th>
              <th></th>
            </thead>
            <tbody>
              <tr class="fila-proyecto w3-text-grey">
                <td><input type="text" name="proyecto_id[]" id="proyecto_id" class="w3-input w3-text-grey w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="20" placeholder="Código Proyecto" readonly disabled="true"></td>
                <td><input type="text" name="proyecto_nombre[]" id="proyecto_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey input-proyecto" maxlength="100" placeholder="Proyecto" readonly></td>
                <td class="text-center eliminar-proyecto"><span class="fa fa-minus-circle"></span></td>
              </tr>
              {{-- muestra los registros de proyectos que se hayan definido en la solicitud --}}
              @foreach($solicitud->proyectos as $proyecto)
                <tr class="w3-text-grey">
                 <td>
                  <input type="hidden" name="proyecto_id[]"  id="proyecto_id" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="10" placeholder="Código Proyecto" value="{{$proyecto->proyecto_id}}" readonly>

                  <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="20" placeholder="Código Proyecto" value="{{$proyecto->proyecto_codigo}}" readonly>
                  </td>
                  <td>
                    <input type="text" name="proyecto_nombre[]" id="proyecto_nombre" class="w3-input w3-border-{{$sistema->sistema_colorclass}} input-proyecto" maxlength="100" placeholder="Descripción Proyecto" value="{{$proyecto->proyecto_nombre}}" readonly>
                  </td>
                  <td class="text-center eliminar-proyecto">
                   <span class="fa fa-minus-circle"></span>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" data-toggle="modal" data-target="#modelProyecto" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
        </div>
      </div>

      <!-- Modal -->
      <div id="modelProyecto" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Proyectos</h4>
            </div>
            <div class="modal-body">
              <p>
                {!! Form::select('proyecto_select', $proyectos->pluck('proyecto_codigo', 'proyecto_id'), '', ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass.'', 'id' => 'proyecto_select', 'placeholder' => 'Seleccionar']) !!}
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="agregarProyecto(); return false"><span class="fa fa-plus fa-fw"></span> Agregar</button>
              <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
            </div>
          </div>

        </div>
      </div>
  		<hr>

      <div class="row">
      	<div class="col-sm-12">
      		<label>Insumos, Materiales o Servicios</label>
      		<span class="help-block">
            <ul>
              <li>En el campo de <b>Costo Estimado</b> no uses separadores de miles, millones u otra cantidad. Tampoco decimales, solo valores redondeados.</li>
              <li>Para añadir más de un insumo, material o servicio, haz clic en el ícono (+).</li>
            </ul>
          </span>
      		<div class="w3-responsive">
      			<table id="tabla-insumos" class="table table-condensed table-striped table-responsive">
      				<thead class="w3-{{$sistema->sistema_colorclass}}">
      					<th></th>
      					<th>Descripción Referencia</th>
      					<th>Unidad de Medida</th>
      					<th>Cantidad Solicitada</th>
      					<th>Especificaciones y/o Características</th>
      					<th>Costo Estimado</th>
      					<th></th>
      				</thead>
              <tbody>
      					<tr class="fila-insumo w3-text-grey">
      						<td>
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_referencia[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="Referencia" required="required" disabled="true">
                  </td>
      						<td>
                    {{ form::select('solicitudinsumo_medida[]', $medidas->pluck('umedida_nombre', 'umedida_codigo'), null, ['id' => 'uni_item', 'class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-sm medida', 'placeholder' => 'Seleccionar', 'disabled' => 'true']) }}
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_cantidad[]" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm cantidad" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}" required="required" disabled="true">
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_caracteristica[]" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm caracteristica" maxlength="100" placeholder="Especificaciones" required="required" disabled="true">
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_costo[]" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm costo" maxlength="20" placeholder="Costo $" disabled="true">
                  </td>
      						<td class="text-center eliminar-insumo">
                    <span class="fa fa-minus-circle"></span>
                  </td>
      					</tr>

                @foreach ($solicitud->insumos as $insumo)
                  <tr class="w3-text-grey">
        						<td>
                      <input type="hidden" name="solicitudinsumo_id[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="ID" value="{{$insumo->solicitudinsumo_id}}" required="required" hidden="hidden">
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_referencia[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="Referencia" value="{{$insumo->solicitudinsumo_referencia}}" required="required">
                    </td>
        						<td>
                      {{-- <input type="text" name="solicitudinsumo_medida[]" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm medida" maxlength="10" placeholder="Unidad de Medida" title="Und, Ton, Kg, Lb, gr, Lt, ml, mt, etc." value="{{$insumo->solicitudinsumo_medida}}" required="required"> --}}

                      {{ form::select('solicitudinsumo_medida[]', $medidas->pluck('umedida_nombre', 'umedida_codigo'), $insumo->solicitudinsumo_medida, ['id' => 'uni_item', 'class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-sm medida', 'placeholder' => 'Seleccionar', 'required' => 'true']) }}
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_cantidad[]" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm cantidad" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}" value="{{$insumo->solicitudinsumo_cantidad}}" required="required">
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_caracteristica[]" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm caracteristica" maxlength="100" placeholder="Especificaciones" value="{{$insumo->solicitudinsumo_caracteristica}}" required="required">
                    </td>
        						<td>
                      <input type="text" name="solicitudinsumo_costo[]" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm costo" maxlength="20" placeholder="Costo $" value="{{$insumo->solicitudinsumo_costo}}">
                    </td>
        						<td class="text-center eliminar-insumo">
                      <span class="fa fa-minus-circle"></span>
                    </td>
        					</tr>
                @endforeach
      				</tbody>
      			</table>
      		</div>
      		<center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" onclick="agregarFilaInsumos(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
      	</div>
      </div>
      <hr>

      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Anexos (opcional) <span class="fa fa-paperclip fa-fw"></span>(Máx. 50MB)</label><br>
            @if($solicitud->solicitud_presupuesto)
              <a href="{{route('browse.compras.budget.download', [$sistema, $solicitud])}}" class="w3-btn w3-large w3-{{$sistema->sistema_colorclass}} w3-col sm-12"><i class="fa fa-download fa-fw"></i> Descargar</a>
            @else
              <div class="w3-container">
                <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
                <h3></h3>
                <p>La solictud no tiene anexos.</p>
                </div>
              </div>
            @endif
            {!! Form::file('solicitud_presupuesto', ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey']) !!}
      		</div>
      	</div>
      </div>
  		<hr>

      <div class="row">
      	<div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
  				      <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>3. Justificación</b></h4>
          </div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Justificación<span style="color: red;">*</span></label>
            {!! Form::textarea('solicitud_justificacion', $solicitud->solicitud_justificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'rows' => '6', 'maxlength' => '255', 'placeholder' => 'Escribe la justificación de tu solicitud (2000 caracteres).', 'required' => true]) !!}
      		</div>
      	</div>
      </div>
      <hr>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
                <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>4. Autorizaciones</b></h4>
          </div>
        </div>
      </div>
      @if($solicitud->solicitud_primera_firma == null && $solicitud->solicitud_segunda_firma == null && $solicitud->solicitud_tercera_firma == null)
        <i>La solicitud aún no ha sido revisada. Puede editar y guardar los cambios.</i>
      @else
        <div class="row">
          <div class="col-sm-4">
            @php
              if($solicitud->solicitud_primera_firma != null)
              {
                $usuario = \App\Usuario::find($solicitud->solicitud_primera_firma);
                $usuario_cuenta = $usuario->cuenta;

                echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
                echo $usuario->email."<br>".$solicitud->solicitud_fecha_primera_firma."</center>";
              }
              else
              {
                echo "-";
              }
            @endphp
          </div>
          <div class="col-sm-4">
            @php
              if($solicitud->solicitud_segunda_firma != null)
              {
                $usuario = \App\Usuario::find($solicitud->solicitud_segunda_firma);
                $usuario_cuenta = $usuario->cuenta;

                echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
                echo $usuario->email."<br>".$solicitud->solicitud_fecha_segunda_firma."</center>";
              }
              else
              {
                echo "-";
              }
            @endphp
          </div>
          <div class="col-sm-4">
            @php
              if($solicitud->solicitud_tercera_firma != null)
              {
                $usuario = \App\Usuario::find($solicitud->solicitud_tercera_firma);
                $usuario_cuenta = $usuario->cuenta;

                echo "<center><b>".$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido."</b><br>";
                echo $usuario->email."<br>".$solicitud->solicitud_fecha_tercera_firma."</center>";
              }
              else
              {
                echo "-";
              }
            @endphp
          </div>
        </div>
      @endif
      <hr>

      <div class="w3-center">
          @if($solicitud->solicitud_primera_firma == null && $solicitud->solicitud_segunda_firma == null && $solicitud->solicitud_tercera_firma == null)
            <button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-floppy-o"></span> Guardar Cambios</button>
          @else
            <div class="w3-content">
              <div class="w3-panel w3-pale-red w3-leftbar w3-rightbar w3-border-red">
                La solicitud no permite modificaciones porque ya ha sido autorizada por al menos un Autorizante. Si alguno de los autorizantes solicita revisión, entonces podrá editar la solicitud. Sin embargo, puede realizar comentarios.
              </div>
            </div>
          @endif
          <a href="{{url()->previous()}}" class="w3-btn w3-red"><span class="fa fa-close fa-fw"></span> Cancelar</a>
      </div>
      <hr>
  {!! Form::close() !!}

  <!--Comentarios-->
  <div class="row">
    <div class="col-sm-12">
      <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
            <h4 class="w3-text-{{$sistema->sistema_colorclass}}" style="padding: .2em"><b>5. Comentarios</b></h4>
      </div>
    </div>
  </div>

  <!--Modal con el formulario para crear un nuevo comentario-->
  <button type="button" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-right" data-toggle="modal" data-target="#nuevo_comentario"><span class="fa fa-comment-o fa-fw"></span> Nuevo Comentario</button>
  {!! Form::open(['route' => ['browse.compras.solicitudes.comentarios.store', $sistema, $solicitud, 'filtro' => $filtro], 'method' => 'post', 'files' => 'true',]) !!}
    <!-- Modal -->
    <div id="nuevo_comentario" class="modal fade w3-animate-zoom" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header w3-{{$sistema->sistema_colorclass}}">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><span class="fa fa-comment-o fa-fw"></span> Nuevo Comentario</h4>
          </div>
          <div class="modal-body">
            <span class="help-block">Escriba a continuación su comentario.</span>
            {!! Form::textarea('solicitudcomentario_comentario', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'maxlength' => '2000', 'placeholder' => 'Escriba su comentario. (2.000 caracteres)', 'required' => 'required']) !!}
          </div>
          <div class="modal-footer">
            <button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-send fa-fw"></span> Publicar</button>
            <button type="reset" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-paint-brush fa-fw"></span> Limpiar</button>  
            <button type="button" class="w3-btn w3-red" data-dismiss="modal"><span class="fa fa-remove fa-fw"></span> Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}

  @if(empty($solicitud->comentarios->toArray()))
    <div class="w3-panel w3-pale-red">
      <p class="w3-padding">
        <i>
        La solicitud aún no tiene comentarios.
        </i>
      </p>
    </div>
  @else
    @php
      $myaccount = \App\Model\Correos\Cuenta::find(\Auth::user()->id);
      $myaccount_extension = $myaccount->extension;
    @endphp
      
    @foreach($solicitud->comentarios as $comentario)
      @php
        $usuario = \App\Usuario::find($comentario->usuario_id);
        $usuario_cuenta = $usuario->cuenta;

        $cuenta = \App\Model\Correos\Cuenta::find($usuario->id);
        $cuenta_extension = $cuenta->extension;
        $cuenta_subarea = $cuenta->subarea;
        $cuenta_tipo = $cuenta->tipo;
      @endphp
      <div class="w3-panel w3-light-grey">
        <span style="font-size:100px;line-height:0.5em;opacity:0.2">❝</span>
        <p class="w3-medium" style="margin-top:-40px">
          <i>
            @php
              echo nl2br($comentario->solicitudcomentario_comentario);
            @endphp
          </i><br>
          <span class="w3-right" style="font-size:12px;""><b>publicado {{$comentario->created_at}} por <a href="javascript:void(0)" onclick="document.getElementById('perfil_{{$usuario->id}}').style.display='block'">{{$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido}}</a></b></span>
        </p>
      </div>
      <div id="perfil_{{$usuario->id}}" class="w3-modal w3-animate-zoom" role="dialog">
        <div class="w3-modal-content">
          <div class="w3-container w3-padding">
            <span onclick="document.getElementById('perfil_{{$usuario->id}}').style.display='none'" class="w3-button w3-display-topright">&times;</span><br><br>
            <div class="w3-row-padding w3-{{$sistema->sistema_colorclass}}">
              <div class="w3-col s12 w3-center">
                <h4><b><span class="fa fa-user fa-fw"></span>Perfil</b></h4>
              </div>
            </div><br>  
            <div class="w3-row-padding">
              <div class="w3-col s4">
                <center>
                  @if($cuenta->cuenta_genero == 'M')
                    <img src="{{asset('images/avatar/img_avatar3.png')}}" class="w3-image" alt="avatar_m">
                  @elseif($cuenta->cuenta_genero == 'F')
                    <img src="{{asset('images/avatar/img_avatar4.png')}}" class="w3-image" alt="avatar_f">
                  @endif
                </center>
              </div>
              <div class="w3-col s8">
                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Nombres</b>: {{$usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Área</b>: {{$cuenta->subarea->subarea_nombre}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Cuenta</b>: {{$usuario->cuenta->cuenta_cuenta}} <b>Tipo</b>: {{$cuenta->tipo->tipo_nombre}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Teléfono</b>: (+57) (2) 3120038 <b>Extensión</b>: @if($myaccount->extension == null || $cuenta->extension == null)<span class="w3-red">No se puede hacer llamadas a este usuario</span>@else{{$cuenta->extension->extension_extension}}<br><br><a href="http://10.0.0.3/ippbx/index.php?r=peers/dircall&destino={{$cuenta->extension->extension_extension}}&origen={{$myaccount->extension->extension_extension}}" title="Llamar" class="w3-btn w3-green" target="open_call"><span class="fa fa-phone fa-fw"></span> Llamar</a><br><br>@endif
                      <iframe name="open_call" id="open_call" hidden></iframe>
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Rol SIA</b>: {{$usuario->rol_sia}}
                    </div>    
                  </div>

                  <div class="w3-row-padding">
                    <div class="w3-col s12">
                      <b>Rol Módulo</b>: {{$usuario->rol_modulo}}
                    </div>    
                  </div>
              </div>
            </div><br>
          </div>
        </div>
      </div>
      <hr>
    @endforeach
  @endif
@endsection

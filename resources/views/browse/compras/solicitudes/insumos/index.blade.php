@extends('browse.compras.menu')

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  {!! Form::open(['route' => ['browse.compras.solicitudes.update', $sistema, $solicitud], 'method' => 'post', 'files' => 'true',]) !!}
    <div class="row w3-text-{{$sistema->sistema_colorclass}}">
    	<div class="col-sm-3 text-center">
    		<h3><img src="{{ asset('images/Unicatolica-color-azul-vertical.png') }}" alt="logo-unicatolica" width="150"></h3>
    	</div>
    	<div class="col-sm-6 text-center">
    		<h2><b>Formato Requerimiento de <br>Materiales, Insumos y Servicios<br>Insumos Solicitud N° {{$solicitud->solicitud_id}}</b></h2>
    	</div>
    	<div class="col-sm-3 text-center w3-text-grey w3-hide-small">
    		<div class="row text-left">
    			<div class="col-sm-6"><b>Código</b><br>6A-7.2.4.9.4</div>
    			<div class="col-sm-6"><b>Versión</b><br>1.0</div>
    		</div>
    		<div class="row text-left">
    			<div class="col-sm-12"><b>Fecha Vigencia</b><br></div>
    		</div>
    	</div>
    </div>
    <br>
    <div class="row" style="display: none;">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Código</label>
    			<input type="text" name="solicitud_codigo_formulario" id="codigo_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="6A-7.2.4.9.4" readonly>
    		</div>
    	</div>
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Versión</label>
    			<input type="text" name="solicitud_version_formulario" id="version_form" class="w3-input w3-border-{{$sistema->sistema_colorclass}}" value="1.0" readonly disabled>
    		</div>
    	</div>
    </div>
    <hr>

    <div class="row">
      <div class="col-sm-12">
    		<label>Insumos, Materiales o Servicios</label>
    		<span class="help-block">
            <ul>
              <li>En el campo de <b>Costo Estimado</b> no use separadores de miles, millones u otra cantidad. Tampoco decimales, solo numeros enteros.</li>
          </ul>
        </span>
    		<div class="w3-responsive">
    			<table id="tabla" class="w3-table w3-hoverable">
    				<thead class="w3-{{$sistema->sistema_colorclass}}">
    					<th>Descripción Referencia</th>
    					<th>Unidad de Medida</th>
    					<th>Cantidad Solicitada</th>
    					<th>Especificaciones y/o Características</th>
    					<th>Costo Estimado</th>
    					<th>Estado</th>
    				</thead>
            <tbody>
              @foreach ($solicitud->insumos as $insumo)
                <tr class="w3-text-grey">
      						<td>
                    <input type="text" name="solicitudinsumo_referencia[]" id="des_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm referencia" maxlength="100" placeholder="Referencia" value="{{$insumo->solicitudinsumo_referencia}}" disabled>
                  </td>
      						<td>
                    {{ form::select('solicitudinsumo_medida[]', $medidas->pluck('umedida_nombre', 'umedida_codigo'), $insumo->solicitudinsumo_medida, ['id' => 'uni_item', 'class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-sm medida', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) }}
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_cantidad[]" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm cantidad" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}" value="{{$insumo->solicitudinsumo_cantidad}}" disabled>
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_caracteristica[]" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm caracteristica" maxlength="100" placeholder="Especificaciones" value="{{$insumo->solicitudinsumo_caracteristica}}" disabled>
                  </td>
      						<td>
                    <input type="text" name="solicitudinsumo_costo[]" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm costo" maxlength="20" placeholder="Costo $" value="{{$insumo->solicitudinsumo_costo}}" disabled>
                  </td>
      						<td class="text-center">
                      @if($insumo->estado_id == 1)
                        @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
                          <a href="{{ route('browse.compras.solicitudes.insumos.update', [$sistema, $solicitud, $insumo, 4, 'filtro' => $filtro]) }}" class="w3-btn w3-green" title="Aprobar"><i class="fa fa-thumbs-up fa-fw"></i></a>
                          <a href="{{ route('browse.compras.solicitudes.insumos.update', [$sistema, $solicitud, $insumo, 5, 'filtro' => $filtro]) }}" class="w3-btn w3-red" title="Rechazar"><i class="fa fa-thumbs-down fa-fw"></i></a>
                        @else
                          <div class="w3-orange w3-padding w3-center">Pendiente</div>
                        @endif
                      @elseif($insumo->estado_id == 4)
                        <div class="w3-green w3-padding w3-center">Aprobado</div>
                      @elseif($insumo->estado_id == 5)
                        <div class="w3-red w3-padding w3-center">Rechazado</div>  
                      @endif            
                  </td>
      					</tr>
              @endforeach
    				</tbody>
    			</table>
    		</div>
      	</div>
      </div>
      <hr>

      <div class="w3-center">
        <a href="{{route('browse.compras.solicitudes.evaluate', [$sistema, $solicitud, 'filtro' => $filtro])}}" class="w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver a la solictud</a>
      </div>
  {!! Form::close() !!}
@endsection

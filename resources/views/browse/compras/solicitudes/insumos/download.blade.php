<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
  	<table>
  		<thead>
        <tr>
          <td align="center" valign="middle"><center><img src="images/Logo-Color-Vertical.png" alt="logo-unicatolica" width="120"></center></td>
          <td colspan="4" align="center" valign="middle"><h2><b>Formato Requerimiento de <br>Materiales, Insumos y Servicios<br>Insumos Solicitud N° {{ $solicitud->solicitud_id }}</b></h2></td>
        </tr>
        <tr>
    			<th>Descripción Referencia</th>
    			<th>Unidad de Medida</th>
    			<th>Cantidad Solicitada</th>
    			<th>Especificaciones y/o Características</th>
    			<th>Costo Estimado</th>
        </tr>
  		</thead>
      <tbody>
        @foreach($solicitud->insumos as $insumo)
          <tr>
  					<td>
              {{$insumo->solicitudinsumo_referencia}}
            </td>
  					<td>
              {{$insumo->solicitudinsumo_cantidad}}
              {{$insumo->medida->umedida_nombre}}
            </td>
  					<td>
              {{$insumo->solicitudinsumo_caracteristica}}
            </td>
  					<td>
              {{$insumo->solicitudinsumo_costo}}
            </td>
  				</tr>
        @endforeach
  		</tbody>
  	</table>
  </body>
</html>
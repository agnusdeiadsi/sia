@extends('browse.compras.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Mis Solicitudes
@endsection

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  {{--@if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Autorizante' || Auth::user()->rol_modulo == 'Solicitante')--}}

  {{-- Valida si el parametro enviado en la variable solicitantes es igual al usuario de la sesión --}}
  @if($solicitante == Auth::user()->id)
    <h2>Mis Solicitudes<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Para conocer el estado de sus solicitudes haga clic sobre cada una de las pestañas."><span class="fa fa-question-circle fa-fw"></span></a></h2>
    <hr />
    <div class="w3-row w3-bar w3-white">
      <div class="w3-col s3 tablink w3-bottombar" id="1" onclick="openTab(event,'procesamiento')">
        <a href="{{ route('browse.compras.solicitudes.myrequests', [$sistema, 'filtro' => '1', 'solicitante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Procesamiento
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$solicitudes->where('estado_id', 1)->count()}}
          </span>
        </a>
      </div>

      <div class="w3-col s3 tablink w3-bottombar" id="2" onclick="openTab(event,'comite')">
        <a href="{{ route('browse.compras.solicitudes.myrequests', [$sistema, 'filtro' => '2', 'solicitante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">En estudio
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$solicitudes->where('estado_id', 2)->count()}}
          </span>
        </a>
      </div>

      <div class="w3-col s3 tablink w3-bottombar" id="3" onclick="openTab(event,'aplazadas')">
        <a href="{{ route('browse.compras.solicitudes.myrequests', [$sistema, 'filtro' => '3', 'solicitante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Aplazadas
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$solicitudes->where('estado_id', 3)->count()}}
          </span>
        </a>
      </div>

      <div class="w3-col s3 tablink w3-bottombar" id="6" onclick="openTab(event,'historial')">
        <a href="{{ route('browse.compras.solicitudes.myrequests', [$sistema, 'filtro' => '6', 'solicitante' => Auth::user()->id]) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Historial
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$solicitudes->where('estado_id', 6)->count()}}
          </span>
        </a>
      </div>
    </div>

    <div id="procesamiento" class="w3-container city" style="display:none;">
      <h2>Procesamiento</h2>
      <span class="help-block">Estado que se presenta cuando la solicitud se encuentra en etapa de revisión por parte de los Autorizantes quienes finalmente darán el visto bueno o la firma para llevar la solicitud al Comité de Evaluación.</span>
      <p>
  			<table class="w3-table-all">
  				<thead>
  					<tr class="w3-{{$sistema->sistema_colorclass}}">
              <th>Código</th>
  						<th>Fecha Radicación</th>
  						<th>Solicitante</th>
  						<th>Tipo solicitud</th>
              <th>Actualización</th>
  						<th>Acción</th>
  					</tr>
  				</thead>
  				<tbody class="w3-text-grey">
            @php
              $total_procesamiento = 0;
            @endphp
  					@foreach($solicitudes->where('estado_id', 1) as $solicitud)
    						<tr>
                  <td>{{ $solicitud->solicitud_id }}</td>
    							<td>{{ $solicitud->created_at }}</td>
    							<td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
    							<td>
                    @if($solicitud->tipo_id == 1)
                      Material o Insumo
                    @elseif($solicitud->tipo_id == 2)
                      Servicio
                    @endif
                  </td>
                  <td>
                    {{$solicitud->updated_at}}
                  </td>
                  <td>
                    {{--<a href="{{route('browse.compras.solicitudes.show', [$sistema, $solicitud, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Ver"><span class="fa fa-eye fa-fw"></span></a>--}}
                    <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud, 'filtro' => '1'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>
                    @if(Auth::user()->rol_sia == 'Administrador')
                      <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                    @endif
                    <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                  </td>
    						</tr>
                @php
                  $total_procesamiento++;
                @endphp
  					@endforeach

            @if($total_procesamiento==0)
              <tr>
                <td colspan="6"><i>No hay registros</i></td>
              </tr>
            @endif
  				</tbody>
  			</table>

        <div class="w3-center">
          <div class="w3-bar">
            {!! $solicitudes->appends(['filtro' => '1', 'solicitante' => $solicitante])->render() !!}
          </div>
        </div>
      </p>
    </div>

    <div id="comite" class="w3-container city" style="display:none">
      <h2>En estudio</h2>
      <span class="help-block">Estado que se presenta cuando la solicitud ha alcanzado el total de firmas o vistos buenos por parte de los Autorizantes (mínimo 2, máximo 3 vistos buenos), entonces, la solicitud de requerimientos pasa a ser estudiada por el Comité de Compras quien aprobará o rechazará los insumos de la misma y finalmente, la archivará.</span>
      <p>
        <div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              @foreach($solicitudes->where('estado_id', 2) as $solicitud)
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{$solicitud->updated_at}}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.show', [$sistema, $solicitud, 'filtro' => '2'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud, 'filtro' => '2'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>
                      
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
              @endforeach

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="6"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {!! $solicitudes->appends(['filtro' => '2', 'solicitante' => $solicitante])->render() !!}
          </div>
        </div>
      </p>
    </div>

    <div id="aplazadas" class="w3-container city" style="display:none">
      <h2>Aplazadas (Solicitan revisión y edición)</h2>
      <span class="help-block">Estado que se presenta cuando alguno de los Autorizantes ha solicitado la revisión y edición parcial o total de la solicitud porque encuentra anomalías en la información. Cuando la solicitud cambie a este estado, la persona quien está realizando la solicitud (Solicitante) debe editar y guardar los cambios de acuerdo con las observaciones que le haya hecho el Autorizante para que la solicitud vuelva al estado “Procesamiento” y pueda ser estudiada nuevamente por los Autorizantes.</span>
      <p>
        <div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              @foreach($solicitudes->where('estado_id', 3) as $solicitud)
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{$solicitud->updated_at}}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud, 'filtro' => '3'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
              @endforeach

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="6"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {!! $solicitudes->appends(['filtro' => '3', 'solicitante' => $solicitante])->render() !!}
          </div>
        </div>
      </p>
    </div>

    <div id="historial" class="w3-container city" style="display:none">
      <h2>Historial</h2>
      <span class="help-block">Muestra todas las solicitudes que han sido archivadas o procesadas.</span>
      <p>
      	<div class="w3-responsive">
          <table class="w3-table-all">
            <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Fecha Radicación</th>
                <th>Solicitante</th>
                <th>Tipo solicitud</th>
                <th>Actualización</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="w3-text-grey">
              @php
                $total_procesamiento = 0;
              @endphp
              @foreach($solicitudes->where('estado_id', 6) as $solicitud)
                  <tr>
                    <td>{{ $solicitud->solicitud_id }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->solicitud_nombres_solicitante }}</td>
                    <td>
                      @if($solicitud->tipo_id == 1)
                        Material o Insumo
                      @elseif($solicitud->tipo_id == 2)
                        Servicio
                      @endif
                    </td>
                    <td>
                      {{$solicitud->updated_at}}
                    </td>
                    <td>
                      <a href="{{route('browse.compras.solicitudes.show', [$sistema, $solicitud, 'filtro' => '6'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Ver"><span class="fa fa-eye fa-fw"></span></a>
                      <a href="{{route('browse.compras.solicitudes.edit', [$sistema, $solicitud, 'filtro' => '6'])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil fa-fw"></span></a>
                      @if(Auth::user()->rol_sia == 'Administrador')
                        <a href="{{route('browse.compras.emails.new', [$sistema, $solicitud])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Enviar a e-mail"><span class="fa fa-envelope fa-fw"></span></a>
                      @endif
                      <a href="{{ route('browse.compras.solicitudes.download', [$sistema, $solicitud]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Descargar PDF"><i class="fa fa-download fa-fw"></i></a>
                    </td>
                  </tr>
                  @php
                    $total_procesamiento++;
                  @endphp
              @endforeach

              @if($total_procesamiento==0)
                <tr>
                  <td colspan="6"><i>No hay registros</i></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>

        <div class="w3-center">
          <div class="w3-bar">
            {!! $solicitudes->appends(['filtro' => '6', 'solicitante' => $solicitante])->render() !!}
          </div>
        </div>
      </p>
    </div>
@else
  @php
    Alert::error('Acceso denegado. Usuario no autorizado.')->html()->persistent('OK');
  @endphp
  <div class="w3-content">
    <div class="w3-panel w3-center">
      <h1><span class="fa fa-frown-o fa-5x"></span></h1>
      <h1>¡Oops!</h1>
    </div>
  </div>
@endif
<script>
document.getElementById({{$filtro}}).click();

function openTab(evt, tabName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-border-{{$sistema->sistema_colorclass}}", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " w3-border-{{$sistema->sistema_colorclass}}";
}
</script>
@endsection

@extends('browse.compras.menu')

@section('title-modulo')
  {{ ucwords($sistema->sistema_nombre_corto) }} | Autorizaciones
@endsection

@section('content-modulo')
  <script>
  function openCity(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" w3-border-cyan", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-cyan";
  }
  </script>

  <h3><span class="fa fa-lock fa-fw"></span> Autorizaciones<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Las Autorizaciones son los permisos que un usuario tiene asignados para firmar o autorizar una solicitud. Las autorizaciones pueden ser de dos tipos: por Áreas/Facultad/Dependencia/ Rectoría/Vicerrectoría o por Subareas/Programa/ Departamentos/Coordinación:"><span class="fa fa-question-circle fa-fw"></span></a></h3>
  <hr />
  <div class="w3-responsive">
    <table class="w3-table-all">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>Nombres</th>
          <th>Cuenta</th>
          <th>Rol</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="w3-text-grey">
        @if(!empty($usuarios))
          @foreach($usuarios as $usuario)
            @php
              //valida el tipo de rol que tiene el usuario en el módulo
              $permisos = \App\Permiso::where('usuario_id', $usuario->cuenta_id)->get();
            @endphp
            @foreach($sistema->roles as $rol)
              @php
                $permiso = $permisos->where('sistemarol_id', $rol->pivot->sistemarol_id);
              @endphp

              @if($permiso->count() > 0 && $rol->rol_nombre == "Autorizante")
                <tr>
                  <td>{{ $usuario->cuenta_primer_apellido.' '.$usuario->cuenta_segundo_apellido.' '.$usuario->cuenta_primer_nombre.' '.$usuario->cuenta_segundo_nombre }}
                  </td>
                  <td>
                    {{ $usuario->cuenta_cuenta }}
                  </td>
                  <td>
                    {{ $rol->rol_nombre }}
                  </td>
                  <td>
                    <a href="{{ route('browse.compras.autorizaciones.edit', [$sistema, $usuario->cuenta_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil"></span></a>
                  </td>
                </tr>
              @endif
            @endforeach
          @endforeach
        @else
          <tr>
            <td colspan="4"><i>No hay usuarios con permisos para este módulo.</i></td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {{$usuarios->render()}}
    </div>
  </div>

  <h3><span class="fa fa-eye fa-fw"></span> Revisores<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Las Autorizaciones son los permisos que un usuario tiene asignados para firmar o autorizar una solicitud. Las autorizaciones pueden ser de dos tipos: por Áreas/Facultad/Dependencia/ Rectoría/Vicerrectoría o por Subareas/Programa/ Departamentos/Coordinación:"><span class="fa fa-question-circle fa-fw"></span></a></h3>
  <hr />
  <div class="w3-responsive">
    <table class="w3-table-all">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>Nombres</th>
          <th>Cuenta</th>
          <th>Rol</th>
          <th></th>
        </tr>
      </thead>
      <tbody class="w3-text-grey">
        @if(!empty($usuarios))
          @foreach($usuarios as $usuario)
            @php
              //valida el tipo de rol que tiene el usuario en el módulo
              $permisos = \App\Permiso::where('usuario_id', $usuario->cuenta_id)->get();
            @endphp
            @foreach($sistema->roles as $rol)
              @php
                $permiso = $permisos->where('sistemarol_id', $rol->pivot->sistemarol_id);
              @endphp

              @if($permiso->count() > 0 && $rol->rol_nombre == "Revisor")
                <tr>
                  <td>{{ $usuario->cuenta_primer_apellido.' '.$usuario->cuenta_segundo_apellido.' '.$usuario->cuenta_primer_nombre.' '.$usuario->cuenta_segundo_nombre }}
                  </td>
                  <td>
                    {{ $usuario->cuenta_cuenta }}
                  </td>
                  <td>
                    {{ $rol->rol_nombre }}
                  </td>
                  <td>
                    <a href="{{ route('browse.compras.revisiones.edit', [$sistema, $usuario->cuenta_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Editar"><span class="fa fa-pencil"></span></a>
                  </td>
                </tr>
              @endif
            @endforeach
          @endforeach
        @else
          <tr>
            <td colspan="4"><i>No hay usuarios con permisos para este módulo.</i></td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
@endsection

@extends('browse.layouts.modulo')

@section('title-modulo')
	{{ ucwords($sistema->sistema_nombre) }}
@endsection

@section('menu-modulo')
	<div class="w3-bar-block">
		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey" onclick="accordion('accordion-info')"><i class="fa fa-file-text-o fa-fw"></i>  Solicitudes <span class="fa fa-caret-down"></span></a>
		<div id="accordion-info" class="w3-hide">
			<a href="{{ route('browse.compras.solicitudes.create', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Crear Solicitud</a>
			<a href="https://informatica.unicatolica.edu.co/tickets/sgyc/index.php" class="w3-bar-item w3-button w3-padding w3-hover-light-grey" target="_blank">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Mantenimiento</a>
			<a href="{{ route('browse.compras.solicitudes.myrequests', [$sistema, 'filtro' => '1', 'solicitante' => Auth::user()->id]) }}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Mis Solicitudes</a>
			@if(\Auth::user()->rol_sia == "Administrador" || \Auth::user()->rol_modulo == "Manager" || \Auth::user()->rol_modulo == "Autorizante" || \Auth::user()->rol_modulo == "Revisor" || \Auth::user()->rol_modulo == "Super Revisor")
				<a href="{{ route('browse.compras.solicitudes.index', [$sistema, 'filtro' => '1', 'autorizante' => Auth::user()->id]) }}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Gestionar Solicitudes</a>
			@endif
		</div>
		@if(\Auth::user()->rol_sia == "Administrador" || \Auth::user()->rol_modulo == "Manager")
			<a href="{{ route('browse.compras.autorizaciones', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-lock fa-fw"></i> Autorizaciones</a>
		@endif
	</div>
@stop

@section('content-modulo')
	<center><h1><b>{{$sistema->sistema_nombre}}</b></h1></center>
	<hr>
	<div class="w3-container">
		<center><h3><b>Aquí podrás:</b></h3></center>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-3">
				<center><img src="{{ asset('images/menu-icon/compras/icon-01.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/compras/icon-02.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/compras/icon-03.png') }}" class="w3-image"></center>
			</div>
		</div>
	</div>
@endsection

@component('mail::message')
<!-- Nueva solicitud -->
Estimado (a):<br />
<b>{{$solicitud->solicitud_nombres_solicitante}}</b><br />
<i>E-mail</i>: {{$solicitud->solicitud_email_solicitante}}
<br />
<br />
<p style="text-align: justify;"><b>SIA</b> le informa que su solicitud en la Aplicación <b>{{ $sistema->sistema_nombre }}</b>, con código <b>{{ $solicitud->solicitud_id }}</b>, ha sido <b>Actualizada</b>.</p>
@component('mail::button', ['url' => config('app.url')])
Consultar Solicitud
@endcomponent
Cordialmente,<br />
<table width="100%" cellspacing="0" style="border-collapse: collapse;">
	<tr>
		<td width="30%">
			<center>
				{{--<a href="{{ url(config('app.url')) }}" target="_blank"><img src="https://apps.unicatolica.edu.co/sia/public/images/sia-small.png" width="120"></a><br>--}}
				<a href="https://www.unicatolica.edu.co/" target="_blank"><img src="https://apps.unicatolica.edu.co/sia/public/images/Logo-Color-Vertical.png" width="120"></a>
			</center>
		</td>
		<td width="70%" style="border-left: 2px grey solid; padding-left: 5px;">
			<h3>{{ config('app.name') }} - Sistema Integrado de Aplicaciones</h3>
			Equipo de Soporte Plataforma / TIC - Tecnologías de la Información y Comunicación<br>
			Fundación Universitaria Católica Lumen Gentium<br>
			noresponder@unicatolica.edu.co<br>
			Tel.: (+57) 2 555 27 67 / (+57) 2 312 0038 ext.: 1052 - Campus Pance<br>
			Cali - Colombia<br>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<pre style="font-size: 9px; text-align: justify;">"Prueba Electrónica: al recibir el acuse de recibo por parte del destinatario o dependencia, se entenderá como aceptado y se recepcionará como documento prueba de la entrega del usuario (Ley 527 del 18/08/1999) - Reconocimiento jurídico de los mensajes de datos en forma electrónica a través de las redes telemáticas". 
<b>Aviso Legal:</b> este mensaje (incluyendo sus anexos), está destinado únicamente para el uso del individuo o entidad a la cual se ha direccionado y puede contener información que no es de carácter público, de uso privilegiado o de carácter confidencial. Si usted no es el destinatario intencional se le informa que cualquier uso, difusión, distribución o copiado de esta comunicación está terminantemente prohibido, tal como lo establece la Ley 1273 de enero de 2009. Si usted ha recibido esta comunicación por error, notifíquenos inmediatamente y elimine este mensaje. Este mensaje y sus anexos han sido revisados con software antivirus, para evitar que contenga código malicioso que pueda afectar sistemas de cómputo, sin embargo es responsabilidad del destinatario confirmar este hecho en el momento de su recepción.</pre>
		</td>
	</tr>
</table>
{{--![alt text](http://campusvirtual.unicatolica.edu.co/moodle/management/media/imagenes/firma_noresponder_moodle.png "Logo Title Text 1")--}}
@endcomponent

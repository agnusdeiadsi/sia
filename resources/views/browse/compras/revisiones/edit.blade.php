@extends('browse.compras.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Autorizaciones | Editar
@endsection

@section('content-modulo')
  @if(Auth::user()->rol_sia == "Administrador" || Auth::user()->rol_modulo == "Manager")
    <div class="w3-container">
      <div class="row">
      		<div class="col-sm-12">
      			<div class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} w3-text-grey">
                <h3><span class="fa fa-pencil"></span> Revisiones</h3>
          			<span class="help-block">
                  <li>Seleccione las áreas, dependencias o departamentos que el usuario debe revisar.</li>
                  <li>Si el usuario revisa un área, no puede revisar una subarea o subacategoría del área que está revisando.
                  <a href="#"><span class="fa fa-connectdevelop"></span> Ver mapa de revisiones.</a></li>
                </span>
            </div>
          </div>
      </div>

  		<span class="help-block"><h3>Revisiones Actuales</h3></span>
      {!! Form::open(['route' => ['browse.compras.revisiones.update', $sistema, $usuario], 'method' => 'PUT']) !!}
        <!--mostrar las areas y subareas que el usuario actualmente autoriza-->
        @foreach ($areas->chunk(3) as $areas_chuck)
          @foreach ($areas_chuck as $area_chuck)
            @if(!empty($usuario_area->toArray()))
              <!--Recorre el arreglo de autorizacionesAreas del usuario de la sesion-->
              @foreach($usuario_area->toArray() as $autorizacion_area)
                <!--Si el area del usuario coincide con alguna de las areas del modulo entonces checked = true-->
                @if($autorizacion_area->area_id == $area_chuck->area_id)
                  <div class="w3-col s4">
                    <ol>
                    <input type="checkbox" name="area_id[]" id="{{$area_chuck->area_id}}" class="area_{{$area_chuck->area_id}}" value="{{$area_chuck->area_id}}" onchange="disableArea(this.value); return false" checked="checked">
                    {{ Form::label('area_id', $area_chuck->area_nombre) }}<br>
                    </ol>
                  </div>
                @endif                      
              @endforeach
            @endif
          @endforeach
      
          @foreach ($subareas as $subarea)
            @if($area_chuck->area_id == $subarea->area_id)
              {{--Si el usuario tiene subareas para autorizar se compararán con las subareas del area--}}
              @if($usuario_subarea != null)
                {{--Recorre el arreglo de autorizacionesSubareas del usuario de la sesion--}}
                @foreach($usuario_subarea as $autorizacion_subarea)
                  {{--Si la subarea del usuario coincide con alguna de las subareas del modulo entonces checked = true--}}
                  @if($subarea->subarea_id == $autorizacion_subarea->subarea_id)
                    <ol>
                      <input type="checkbox" name="subarea_id[]" id="{{$subarea->subarea_id}}" class="{{$area_chuck->area_id}}" value="{{$subarea->subarea_id}}" onchange="disableSubarea({{$area_chuck->area_id}}, {{$subarea->subarea_id}}); return false" checked="checked">
                      {!! Form::label('subarea_id', $subarea->subarea_nombre, ['class' => 'w3-text-grey']) !!}

                      <input type="checkbox" name="area[]" value="{{$area_chuck->area_id}}" class="{{$area_chuck->area_id}}" hidden="hidden" checked="checked"><br>
                    </ol>
                  @endif
                @endforeach
              @endif
            @endif
          @endforeach
        @endforeach
        <br>
        <hr>

        <!--mostrar todas las areas y sus respectivas subareas-->
        <span class="help-block"><h3>Área/Subareas</h3></span>
        @foreach ($areas->chunk(3) as $areas)
          <div class="w3-row-padding">
            @foreach ($areas as $area)
              <div class="w3-col s4">
                <input type="checkbox" name="area_id[]" id="{{$area->area_id}}" class="area_{{$area->area_id}}" value="{{$area->area_id}}" onchange="disableArea(this.value); return false">
                {!! Form::label('area_id', $area->area_nombre) !!}<br>

                @foreach ($subareas as $subarea)
                  @if($area->area_id == $subarea->area_id)
                    <li>
                      <input type="checkbox" name="subarea_id[]" id="{{$subarea->subarea_id}}" class="{{$area->area_id}}" value="{{$subarea->subarea_id}}" onchange="disableSubarea({{$area->area_id}}, {{$subarea->subarea_id}}); return false">
                      {!! Form::label('subarea_id', $subarea->subarea_nombre, ['class' => 'w3-text-grey']) !!}
                      <input type="checkbox" name="area[]" value="{{$area->area_id}}" class="area_{{$area->area_id}}" hidden="hidden" checked="checked">
                    </li>
                  @endif
                @endforeach
              </div>
            @endforeach
          </div><hr>
        @endforeach
        <button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
        <a href="{{route('browse.compras.autorizaciones', $sistema)}}" class="w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>
      {!! Form::close() !!}
    </div>
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif

  <script>
    function disableArea(clase)
    {
      area = document.getElementById(clase).checked;
      elemento = document.getElementsByClassName(clase);

      if (area == true) {
        for(i=0;i<elemento.length;i++)
          elemento[i].disabled=true;
      }
      else
      {
        for(i=0;i<elemento.length;i++)
          elemento[i].disabled=false;
      }
    }

    function disableSubarea(area_id, subarea_id)
    {
      subarea = document.getElementById(subarea_id).checked;
      area = document.getElementsByClassName("area_"+area_id);
      elemento = document.getElementsByClassName(area_id);

      if(subarea == true)
      {
        for(i=0;i<area.length;i++)
          area[i].disabled=true;
      }
      else
      {
        for(i=0;i<area.length;i++)
          area[i].disabled=false;
      }
    }

    /*function autoselect(clase) {
      area = document.getElementById(clase).checked;
      elemento = document.getElementsByClassName(clase);

      if (area == true) {
        for(i=0;i<elemento.length;i++)
          elemento[i].checked=clase.checked;

        for(i=0;i<elemento.length;i++)
          elemento[i].checked=true;
      }else {
        for(i=0;i<elemento.length;i++)
          elemento[i].checked=clase.checked;
      }
    }

    function selectsubarea(clase, id) {
      elemento = document.getElementById(id).checked;
      if(elemento == false)
      {
        area = document.getElementById(clase).checked=false;
      }
    }*/
  </script>
@endsection

@extends('browse.correos.menu')

@section('title-modulo')
 Nombre_corto_modulo | Nombre_vista
@endsection

@section('content-modulo')
{{--Extraer parametros enviado por metodo GET (opcional)--}}
@php
	extract($_REQUEST);
@endphp

{{--Título página--}}
<h2>Recibir Certificado<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Descripción de ayuda."><span class="fa fa-question-circle fa-fw"></span></a></h2>

{{--Buscadores (opcional)--}}
<div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    <div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['browse.certificados.solicitudes', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {{Form::hidden('filtro', $filtro)}}
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por ID Solicitud, Código del Estudiante, nombres del Estudiante, Código de liquidación Banner o tipo de Certificado."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['browse.certificados.solicitudes', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {{Form::hidden('filtro', $filtro)}}
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por ID Solicitud, Código del Estudiante, nombres del Estudiante, Código de liquidación Banner o tipo de Certificado."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>
</div>

{{--Paginación superior--}}
<div class="w3-center">
	<div class="w3-bar">
	  {!! $objetos->render() !!}
	</div>
</div>

<div class="w3-responsive">
  	<table class="w3-table-all">
  		<thead>
  			<tr class="w3-{{$sistema->sistema_colorclass}}">
  				<th>Título</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody>
  			@foreach($objetos as $objeto)
  				<tr>
  					<td>{{$objeto->nombre_atributo}}</td>
  					<td>
  						<a href="{{ route('name') }}" class="w3-btn w3-white w3-border"><span class="fa fa-eye fa-fw"></span></a>
  						<a href="{{ route('name') }}" class="w3-btn w3-white w3-border"><span class="fa fa-pencil fa-fw"></span></a>
  						<a href="{{ route('name') }}" class="w3-btn w3-white w3-border"><span class="fa fa-eraser fa-fw"></span></a>
  					</td>
  				</tr>
  			@endforeach
  		</tbody>
  	</table>
</div>

{{--Paginación inferior--}}
<div class="w3-center">
	<div class="w3-bar">
	  {!! $objetos->render() !!}
	</div>
</div>
@endsection
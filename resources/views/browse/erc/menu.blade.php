@extends('browse.layouts.modulo')

@section('title-modulo')
  {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
	<div class="w3-bar-block">
		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey" onclick="accordion('accordion-recibos')"><i class="fa fa-file-text-o fa-fw"></i>  Recibos <span class="fa fa-caret-down"></span></a>
		<div id="accordion-recibos" class="w3-hide">
			<a href="{{ route('erc.recibos.create', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey">&nbsp;&nbsp;<i class="fa fa-caret-right fa-fw"></i> Generar</a>
			<a href="{{ route('erc.recibos.historial', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey">&nbsp;&nbsp;<i class="fa fa-caret-right fa-fw"></i> Historial</a>
		</div>
    	@if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == "Manager" || Auth::user()->rol_modulo == "Cartera")
		  <a href="{{ route('erc.recibos.cartera', [$sistema, 'filtro' => '1']) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-credit-card-alt fa-fw"></i>  Cartera</a>
    	@endif
	</div>
@endsection

@section('content-modulo')
	<center><h1><b>{{$sistema->sistema_nombre}}</b></h1></center>
	<hr>
	<div class="w3-container">
		<center><h3><b>Aquí podrás:</b></h3></center>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-2">
				<center><img src="{{ asset('images/menu-icon/erc/icon-01.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/erc/icon-02.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/erc/icon-03.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/erc/icon-04.png') }}" class="w3-image"></center>
			</div>
		</div>
	</div>
@endsection

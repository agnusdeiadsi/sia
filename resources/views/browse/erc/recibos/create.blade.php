@extends('browse.erc.menu')

@section('title-modulo')
  Crear eRC
@endsection

@section('content-modulo')
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Cartera' || Auth::user()->rol_modulo == 'Facturador')
    <div class="w3-container">
    <div class="w3-row">
      <div class="w3-col s12 w3-hide-small">
        {!! Form::open(['route' => ['erc.recibos.create', $sistema], 'method' => 'get']) !!}
          <div class="input-group w3-col s4 w3-right">
            {!! Form::text('codigo', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search', 'required']) !!}
            <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
            <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Para crear un recibo de caja e-RC digite el código o identificación del usuario. Si existen registros del usuario, los campos se rellenarán automáticamente. Sin embargo, también puede crear uno nuevo."><span class="fa fa-question-circle fa-fw"></span></a>
          </div>
        {!! Form::close() !!}
      </div>
      <div class="w3-col s12 w3-hide-large">
        {!! Form::open(['route' => ['erc.recibos.create', $sistema], 'method' => 'get']) !!}
          <div class="input-group w3-col s12 w3-right">
            {!! Form::text('codigo', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search', 'required']) !!}
            <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
            <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Para crear un recibo de caja e-RC digite el código o identificación del usuario. Si existen registros del usuario, los campos se rellenarán automáticamente. Sin embargo, también puede crear uno nuevo."><span class="fa fa-question-circle fa-fw"></span></a>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <hr />

  <div class="row w3-text-{{$sistema->sistema_colorclass}}">
    <div class="col-sm-12 text-center">
      <h2><b>Recibo de Caja</b></h2>
    </div>
  </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="w3-panel w3-{{$sistema->sistema_colorclass}} w3-padding">
          <span onclick="this.parentElement.style.display='none'" class="fa fa-remove w3-button w3-right w3-hover-none"></span>
          El usuario con código <b>{{ $matriculado->pers_id }}</b> ha sido encontrado en Banner</b>.
        </div>
      </div>
    </div>
  <hr>
    {!! Form::open(['route' => ['erc.recibos.store', $sistema], 'method' => 'post', 'multipart/form-data' => 'true']) !!}
  		<div class="row">
  			<div class="col-sm-12 w3-padding">
  				<div class="row">
  					<div class="col-sm-6">
  						<div class="form-group">
  							<label>Centro Operación*</label>
                {!! Form::select('centrooperacion_id', $centros_operacion, old('centrooperacion_id'), ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey' , 'onchange' => 'totalAPagar(); return false', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
  						</div>
  					</div>
  					<div class="col-sm-6">
  						<div class="form-group">
  							<label>N° Liquidación Banner*</label>
                {!! Form::text('recibo_codigo_liquidacion', old('recibo_codigo_liquidacion'), ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'liquidacion_banner', 'onchange' => 'totalAPagar(); return false', 'maxlength' => '20', 'placeholder' => 'Código Liquidación', 'required' => 'true']) !!}
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
      <hr>

  		<div class="row">
  			<div class="col-sm-5 w3-padding">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{ $sistema->sistema_colorclass }}">
                    <h4 style="padding: .2em"><b>Información Personal</b></h4>
              </div>
            </div>
          </div>
          <p>
            <label>Nombres*:</label>
            {!! Form::text('recibo_cliente_nombres', $matriculado->pers_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'nombres', 'maxlength' => '100', 'placeholder' => 'Nombres', 'required' => 'true']) !!}
          </p>
          <p>
			      <label>Identificación*:</label>
            {!! Form::text('recibo_cliente_identificacion', $matriculado->pers_num_doc, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true']) !!}
          </p>
          <p>
  				  <label>Programa:</label>
            {!! Form::text('recibo_cliente_programa', ($matriculado->matriculas->where('bmtr_periodo', '=', $matriculado->matriculas->max('bmtr_periodo'))->first() != null)?$matriculado->matriculas->where('bmtr_periodo', '=', $matriculado->matriculas->max('bmtr_periodo'))->first()->programa->stvmajr_desc:null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'programa', 'maxlength' => '100', 'placeholder' => 'Programa']) !!}
          </p>
          <p>
  				  <label>Dirección:</label>
            {!! Form::text('recibo_cliente_direccion', (!empty($matriculado->ubicacion) || $matriculado->ubicacion != null)?$matriculado->ubicacion->ubip_direccion:null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'direccion', 'maxlength' => '100', 'placeholder' => 'Dirección']) !!}
          </p>
          <p>
  				  <label>Teléfono*:</label>
            {!! Form::text('recibo_cliente_telefono', (!empty($matriculado->telefono) || $matriculado->telefono != null)?$matriculado->telefono->telp_telres:null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'telefono', 'maxlength' => '50', 'placeholder' => 'Teléfono', 'required' => 'true']) !!}
          </p>
          <p>
  				  <label>Ciudad:</label>
            {!! Form::text('recibo_cliente_ciudad', (!empty($matriculado->ubicacion) || $matriculado->ubicacion != null)?$matriculado->ubicacion->ubip_ciudad:null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'ciudad', 'maxlength' => '100', 'placeholder' => 'Ciudad']) !!}
          </p>
          <p>
  				  <label>Observaciones:</label>
            {!! Form::textarea('recibo_observaciones', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'observaciones', 'maxlength' => '255', 'rows' => '5', 'placeholder' => 'Observaciones (255 caracteres).']) !!}
          </p>
        </div>
  			<div class="col-sm-offset-1 col-sm-6 w3-padding">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{ $sistema->sistema_colorclass }}">
                    <h4 style="padding: .2em"><b>Conceptos de Caja</b></h4>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-{{$sistema->sistema_colorclass}} w3-padding">
                <span onclick="this.parentElement.style.display='none'" class="fa fa-remove w3-button w3-right w3-hover-none"></span>
                Utilizar punto (.) sólo para indicar decimales.
              </div>
            </div>
          </div>

          <div class="row">
      			<div class="col-sm-12">
      				<div id="alerta" class="w3-panel w3-pale-red w3-leftbar w3-border-red" hidden><h4><span class="fa fa-exclamation-triangle"></span> El valor debe ser mayor o igual $0 (Cero).</h4></div>
      			</div>
      		</div>

  				<table width="100%" height="320px">
  					<tr>
  						<th width="40%"></th>
  						<th>Valor $0.00</th>
  					</tr>
  					<tr>
  						<th> Matrícula</th>
  						<td>
                {!! Form::number('concepto_matricula', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'matricula', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Carnet</th>
  						<td>
                {!! Form::number('concepto_carnet', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'carnet', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Precooperativa</th>
  						<td>
                {!! Form::number('concepto_precooperativa', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'precooperativa', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Curso de Verano</th>
  						<td>
                {!! Form::number('concepto_curso_verano', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'curso_verano', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Certificados</th>
  						<td>
                {!! Form::number('concepto_certificado', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'certificados', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Derechos de Grado</th>
  						<td>
                {!! Form::number('concepto_derechos_grado', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'derechos_grado', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Habilitación</th>
  						<td>
                {!! Form::number('concepto_habilitacion', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'habilitacion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Seguro Estudiantil</th>
  						<td>
                {!! Form::number('concepto_seguro_estudiantil', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'seguro_estudiantil', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Estampilla Procultura</th>
  						<td>
                {!! Form::number('concepto_estampilla', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'estampilla', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Inscripción</th>
  						<td>
                {!! Form::number('concepto_inscripcion', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'inscripcion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Homologación</th>
  						<td>
                {!! Form::number('concepto_homologacion', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'homologacion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Otros Conceptos</th>
  						<td>
                {!! Form::number('concepto_otros', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'otros_conceptos', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> <b>TOTAL</b></th>
  						<th>
                {!! Form::number('concepto_total', 0,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'total', 'maxlength' => '20', 'placeholder' => '$0.0', 'readonly' => 'true']) !!}
              </th>
  					</tr>
  				</table>
  			</div>
  		</div>
  		<hr>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{ $sistema->sistema_colorclass }}">
                <h4 class="w3-text-{{ $sistema->sistema_colorclass }}" style="padding: .2em"><b>Modalidad de Pago</b></h4>
          </div>
        </div>
      </div>

      <div class="row">
  			<div class="col-sm-6 w3-padding">
          @foreach ($pagos as $pago)
              <p>
                {{ Form::checkbox('pago_id[]', $pago->pago_id, null, ['onchange' => 'displayPago(this.value); return false', 'id' => 'checkbox_'.$pago->pago_id]) }}
                {{ Form::label('pago_id', $pago->pago_nombre) }}
              </p>
              <p class="w3-margin-left">
                {{ Form::label('recibopago_valor', 'Valor', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                {{ Form::number('recibopago_valor[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Valor', 'style' => 'display:none;', 'disabled']) }}
              </p>
               @if($pago->pago_nombre == 'Cheque')
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_codigo', 'N° Cheque', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => $pago->pago_nombre, 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_plaza', 'Plaza', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                    </p>
               @elseif($pago->pago_nombre == 'Tarjeta')
                  <!-- visible input -->
                  <p class="w3-margin-left">
                    {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::label('recibopago_codigo', 'Voucher', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>

                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
               @elseif($pago->pago_nombre == 'Efectivo')
                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
               @elseif($pago->pago_nombre == 'Transferencia')
                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
               @endif
             </p>
          @endforeach
        </div>
      </div>
  	  <br>

      <div class="row w3-right">
  			<div class="col-sm-12">
  				<button type="submit" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
  				<a href="{{url()->previous()}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
  			</div>
  		</div>
    {!! Form::close() !!}
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection

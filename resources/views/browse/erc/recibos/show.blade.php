@extends('browse.erc.menu')

@section('title-modulo')
  Ver Recibo {{ $recibo->recibo_consecutivo_centro }}
@endsection

@section('content-modulo')
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Cartera')

    <div class="row w3-text-{{$sistema->sistema_colorclass}}">
      <div class="col-sm-12 text-center">
        <h2>
          <b>Recibo de Caja<br> N° {{ $recibo->recibo_consecutivo_centro }}
          </b>
        </h2>
      </div>
    </div>
    <hr>
    {!! Form::open(['route' => ['erc.recibos.update', $sistema, $recibo->recibo_id], 'method' => 'put', 'multipart/form-data' => 'true' ]) !!}
  		<div class="row">
  			<div class="col-sm-12 w3-padding">
  				<div class="row">
  					<div class="col-sm-6">
  						<div class="form-group">
  							<label>Centro Operación*</label>
                {!! Form::select('centrooperacion_id', $centros_operacion, $recibo->centrooperacion_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey' , 'onchange' => 'totalAPagar(); return false', 'placeholder' => 'Seleccionar', 'required' => 'true', 'disabled']) !!}
  						</div>
  					</div>
  					<div class="col-sm-6">
  						<div class="form-group">
  							<label>N° Liquidación Banner*</label>
                {!! Form::text('recibo_codigo_liquidacion', $recibo->recibo_codigo_liquidacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'liquidacion_banner', 'onchange' => 'totalAPagar(); return false', 'maxlength' => '20', 'placeholder' => 'Código Liquidación', 'required' => 'true', 'readonly']) !!}
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
      <hr>

  		<div class="row">
  			<div class="col-sm-5 w3-padding">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
                    <h4 style="padding: .2em"><b>Información Personal</b></h4>
              </div>
            </div>
          </div>
          <p>
            <label>Nombres*:</label>
            {!! Form::text('recibo_cliente_nombres', $recibo->recibo_cliente_nombres, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'nombres', 'maxlength' => '100', 'placeholder' => 'Nombres', 'required' => 'true', 'readonly']) !!}
          </p>
          <p>
			      <label>Identificación*:</label>
            {!! Form::text('recibo_cliente_identificacion', $recibo->recibo_cliente_identificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true', 'readonly']) !!}
          </p>
          <p>
  				  <label>Programa:</label>
            {!! Form::text('recibo_cliente_programa', $recibo->recibo_cliente_programa, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'programa', 'maxlength' => '100', 'placeholder' => 'Programa', 'readonly']) !!}
          </p>
          <p>
  				  <label>Dirección:</label>
            {!! Form::text('recibo_cliente_direccion', $recibo->recibo_cliente_direccion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'direccion', 'maxlength' => '100', 'placeholder' => 'Dirección', 'readonly']) !!}
          </p>
          <p>
  				  <label>Teléfono*:</label>
            {!! Form::text('recibo_cliente_telefono', $recibo->recibo_cliente_telefono, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'telefono', 'maxlength' => '20', 'placeholder' => 'Teléfono', 'required' => 'true', 'readonly']) !!}
          </p>
          <p>
  				  <label>Ciudad:</label>
            {!! Form::text('recibo_cliente_ciudad', $recibo->recibo_cliente_ciudad, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'ciudad', 'maxlength' => '100', 'placeholder' => 'Ciudad', 'readonly']) !!}
          </p>
          <p>
  				  <label>Observaciones:</label>
            {!! Form::textarea('recibo_observaciones', $recibo->recibo_observaciones, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'observaciones', 'maxlength' => '255', 'rows' => '5', 'placeholder' => 'Observaciones (255 caracteres).', 'readonly']) !!}
          </p>
        </div>
  			<div class="col-sm-offset-1 col-sm-6 w3-padding">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
                    <h4 style="padding: .2em"><b>Conceptos de Caja</b></h4>
              </div>
            </div>
          </div>

          <div class="row">
      			<div class="col-sm-12">
      				<div id="alerta" class="w3-panel w3-pale-red w3-leftbar w3-border-red" hidden><h4><span class="fa fa-exclamation-triangle"></span> El valor debe ser mayor o igual $0 (Cero).</h4></div>
      			</div>
      		</div>

  				<table width="100%" height="320px">
  					<tr>
  						<th width="40%"></th>
  						<th>Valor $0.0</th>
  					</tr>
  					<tr>
  						<th> Matrícula</th>
  						<td>
                {!! Form::number('concepto_matricula', $recibo->concepto->concepto_matricula,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'matricula', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Carnet</th>
  						<td>
                {!! Form::number('concepto_carnet', $recibo->concepto->concepto_carnet,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'carnet', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Precooperativa</th>
  						<td>
                {!! Form::number('concepto_precooperativa',  $recibo->concepto->concepto_precooperativa,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'precooperativa', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Curso de Verano</th>
  						<td>
                {!! Form::number('concepto_curso_verano',  $recibo->concepto->concepto_curso_verano,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'curso_verano', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Certificados</th>
  						<td>
                {!! Form::number('concepto_certificado',  $recibo->concepto->concepto_certificado,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'certificados', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Derechos de Grado</th>
  						<td>
                {!! Form::number('concepto_derechos_grado',  $recibo->concepto->concepto_derechos_grado,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'derechos_grado', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Habilitación</th>
  						<td>
                {!! Form::number('concepto_habilitacion',  $recibo->concepto->concepto_habilitacion,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'habilitacion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Seguro Estudiantil</th>
  						<td>
                {!! Form::number('concepto_seguro_estudiantil',  $recibo->concepto->concepto_seguro_estudiantil,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'seguro_estudiantil', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Estampilla Procultura</th>
  						<td>
                {!! Form::number('concepto_estampilla',  $recibo->concepto->concepto_estampilla,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'estampilla', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Inscripción</th>
  						<td>
                {!! Form::number('concepto_inscripcion',  $recibo->concepto->concepto_inscripcion,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'inscripcion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Homologación</th>
  						<td>
                {!! Form::number('concepto_homologacion',  $recibo->concepto->concepto_homologacion,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'homologacion', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> Otros Conceptos</th>
  						<td>
                {!! Form::number('concepto_otros',  $recibo->concepto->concepto_otros,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'otros_conceptos', 'onblur' => 'totalAPagar();', 'maxlength' => '20', 'placeholder' => '0.0', 'required' => 'true', 'readonly']) !!}
              </td>
  					</tr>
  					<tr>
  						<th> <b>TOTAL</b></th>
  						<th>
                {!! Form::number('concepto_total', $recibo->concepto->concepto_total,['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'total', 'maxlength' => '20', 'placeholder' => '$0.0', 'readonly' => 'true', 'readonly']) !!}
              </th>
  					</tr>
  				</table>
  			</div>
  		</div>
  		<hr>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
                <h4 style="padding: .2em"><b>Formas de Pago</b></h4>
          </div>
        </div>
      </div>

      <div class="row">
  			<div class="col-sm-6 w3-padding">
          @foreach ($pagos as $pago)
            @php
              $coincidencia = false; //
            @endphp
            @if(!empty($recibo->pagos->ToArray()))
              @foreach ($recibo->pagos->ToArray() as $recibo_pago)
                {{-- Si la modalidad de pago del recibo es igual a las modalidad de pago entonces checkbox es true --}}
                @if($recibo_pago['pivot']['pago_id'] == $pago->pago_id)
                    @php
                      $coincidencia = true;
                    @endphp
                    <p>
                      {{ Form::checkbox('pago_id[]', $pago->pago_id, true, ['onchange' => 'displayPago(this.value); return false', 'id' => 'checkbox_'.$pago->pago_id]) }}
                      {{ Form::label('pago_id', $pago->pago_nombre) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_valor', 'Valor', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;',]) }}
                      {{ Form::number('recibopago_valor[]', $recibo_pago['pivot']['recibopago_valor'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Valor', 'style' => 'display:block;',]) }}
                    </p>
                    @if($pago->pago_nombre == 'Cheque')
                          <p class="w3-margin-left">
                            {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;']) }}
                            {{ Form::text('recibopago_entidad_bancaria[]', $recibo_pago['pivot']['recibopago_entidad_bancaria'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:block;',]) }}
                          </p>
                          <p class="w3-margin-left">
                            {{ Form::label('recibopago_codigo', 'N° Cheque', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;']) }}
                            {{ Form::number('recibopago_codigo[]', $recibo_pago['pivot']['recibopago_codigo'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => $pago->pago_nombre, 'style' => 'display:block;',]) }}
                          </p>
                          <p class="w3-margin-left">
                            {{ Form::label('recibopago_plaza', 'Plaza', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;']) }}
                            {{ Form::number('recibopago_plaza[]', $recibo_pago['pivot']['recibopago_plaza'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:block;',]) }}
                          </p>
                    @elseif($pago->pago_nombre == 'Tarjeta')
                        <!-- visible input -->
                        <p class="w3-margin-left">
                          {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;']) }}
                          {{ Form::text('recibopago_entidad_bancaria[]', $recibo_pago['pivot']['recibopago_entidad_bancaria'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:block;']) }}
                        </p>
                        <p class="w3-margin-left">
                          {{ Form::label('recibopago_codigo', 'Voucher', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:block;']) }}
                          {{ Form::number('recibopago_codigo[]', $recibo_pago['pivot']['recibopago_codigo'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:block;',]) }}
                        </p>

                        <!-- hidden input -->
                        <p class="w3-margin-left">
                          {{ Form::number('recibopago_plaza[]', $recibo_pago['pivot']['recibopago_plaza'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;',]) }}
                        </p>
                    @elseif($pago->pago_nombre == 'Efectivo')
                        <!-- hidden input -->
                        <p class="w3-margin-left">
                          {{ Form::number('recibopago_codigo[]', $recibo_pago['pivot']['recibopago_codigo'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;',]) }}
                        </p>
                        <p class="w3-margin-left">
                          {{ Form::text('recibopago_entidad_bancaria[]', $recibo_pago['pivot']['recibopago_entidad_bancaria'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;',]) }}
                        </p>
                        <p class="w3-margin-left">
                          {{ Form::number('recibopago_plaza[]', $recibo_pago['pivot']['recibopago_plaza'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;',]) }}
                        </p>
                    @elseif($pago->pago_nombre == 'Transferencia')
                        <!-- hidden input -->
                        <p class="w3-margin-left">
                          {{ Form::number('recibopago_codigo[]', $recibo_pago['pivot']['recibopago_codigo'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;',]) }}
                        </p>
                        <p class="w3-margin-left">
                          {{ Form::text('recibopago_entidad_bancaria[]', $recibo_pago['pivot']['recibopago_entidad_bancaria'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;',]) }}
                        </p>
                        <p class="w3-margin-left">
                          {{ Form::number('recibopago_plaza[]', $recibo_pago['pivot']['recibopago_plaza'], ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;',]) }}
                        </p>
                    @endif
                  </p>
                @endif
            @endforeach

            @if ($coincidencia == false)
              <p>
                {{ Form::checkbox('pago_id[]', $pago->pago_id, null, ['onchange' => 'displayPago(this.value); return false', 'id' => 'checkbox_'.$pago->pago_id]) }}
                {{ Form::label('pago_id', $pago->pago_nombre) }}
              </p>
              <p class="w3-margin-left">
                {{ Form::label('recibopago_valor', 'Valor', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                {{ Form::number('recibopago_valor[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Valor', 'style' => 'display:none;', 'disabled']) }}
              </p>
              @if($pago->pago_nombre == 'Cheque')
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_codigo', 'N° Cheque', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => $pago->pago_nombre, 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_plaza', 'Plaza', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                    </p>
              @elseif($pago->pago_nombre == 'Tarjeta')
                  <!-- visible input -->
                  <p class="w3-margin-left">
                    {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::label('recibopago_codigo', 'Voucher', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>

                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
              @elseif($pago->pago_nombre == 'Efectivo')
                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
              @elseif($pago->pago_nombre == 'Transferencia')
                  <!-- hidden input -->
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                  </p>
                  <p class="w3-margin-left">
                    {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                  </p>
              @endif
            </p>
            @endif


            {{-- Si el arreglo de recibo_pago está vacío --}}
            @else
              <p>
                {{ Form::checkbox('pago_id[]', $pago->pago_id, null, ['onchange' => 'displayPago(this.value); return false', 'id' => 'checkbox_'.$pago->pago_id]) }}
                {{ Form::label('pago_id', $pago->pago_nombre) }}
              </p>
              <p class="w3-margin-left">
                {{ Form::label('recibopago_valor', 'Valor', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                {{ Form::number('recibopago_valor[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Valor', 'style' => 'display:none;', 'disabled']) }}
              </p>
                @if($pago->pago_nombre == 'Cheque')
                      <p class="w3-margin-left">
                        {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                        {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                      </p>
                      <p class="w3-margin-left">
                        {{ Form::label('recibopago_codigo', 'N° Cheque', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                        {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => $pago->pago_nombre, 'style' => 'display:none;', 'disabled']) }}
                      </p>
                      <p class="w3-margin-left">
                        {{ Form::label('recibopago_plaza', 'Plaza', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                        {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                      </p>
                @elseif($pago->pago_nombre == 'Tarjeta')
                    <!-- visible input -->
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_entidad_bancaria', 'Entidad Bancaria', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::label('recibopago_codigo', 'Voucher', ['class' => 'valor_'.$pago->pago_id, 'style' => 'display:none;']) }}
                      {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                    </p>

                    <!-- hidden input -->
                    <p class="w3-margin-left">
                      {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                @elseif($pago->pago_nombre == 'Efectivo')
                    <!-- hidden input -->
                    <p class="w3-margin-left">
                      {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                @elseif($pago->pago_nombre == 'Transferencia')
                    <!-- hidden input -->
                    <p class="w3-margin-left">
                      {{ Form::number('recibopago_codigo[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Voucher', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::text('recibopago_entidad_bancaria[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'maxlength' => '100', 'placeholder' => 'Entidad Bancaria', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                    <p class="w3-margin-left">
                      {{ Form::number('recibopago_plaza[]', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey valor_'.$pago->pago_id.' hide_'.$pago->pago_id, 'id' => 'valor_'.$pago->pago_id, 'placeholder' => 'Plaza', 'style' => 'display:none;', 'disabled']) }}
                    </p>
                @endif
              </p>
            @endif
        @endforeach
        </div>
      </div>
  	<br>

    <div class="w3-row w3-right">
			<div class="w3-col s12">
        @php
          // calcula los dias trancurridos entre la fecha de radicacion del recibo la fecha actual
          $diferencia = strtotime('now')-strtotime($recibo->recibo_created_at);
          $dias_tracurridos = intval($diferencia/60/60/24); //convierte a entero la diferencia
        @endphp

        <a href="{{ url('browse/erc/recibos/download', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-print fa-fw"></span> Imprimir</a>

        {{-- Si el estado del recibo es No Contabilizado entonces se puede realizar cambios --}}
        @if($recibo->estado_id == 1)
          <a href="{{ route('erc.recibos.contabilizar', [$sistema, $recibo->recibo_id, 'filtro' => $filtro]) }}" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-archive fa-fw"></span> Contabilizar</a>

  				<a href="{{ route('erc.recibos.cartera', [$sistema, 'filtro' => $filtro]) }}" class="w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>

        {{-- Si el estado del recibo es Contabilizado --}}
        @elseif($recibo->estado_id == 2)
  				<a href="{{ route('erc.recibos.cartera', [$sistema, 'filtro' => $filtro]) }}" class="w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>
        @endif
			</div>
		</div>
    {!! Form::close() !!}
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection

<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="../../lib/css.css">
	<script type="text/javascript">
	</script>
	<style type="text/css">
		body, table{
			font-family: calibri;
			font-size: 10.5px;
			border-spacing: 0;
			border-collapse: collapse;
		}
		#fila_total, #fila_conceptos{
			background-color: grey;
			font-size: 12px;
		}
		#tabla_encabezado, #tabla_pie{
			font-size: 12px;
		}

    .columna_etiqueta{
      text-align: left;
      vertical-align: text-top;
    }
	@page{
   		margin: 5px 10px 0px 10px;
	}

	</style>
</head>

<body>
  <div style="width:auto;">
  {{--si el recibo ha sido marcado como contabilizado entonces imprimir marca de agua --}}
  @if($recibo->estado_id == 2)
    <div style="z-index: 2; position: fixed; top: 0; left: 150; bottom: 0; right: 0; overflow: hidden;">
    	<h1 style="-webkit-transform: rotate(-55deg); /*transform: rotate(-70deg);*/">CONTABILIZADO</h1>
    </div>
  @endif

  {{--recibo--}}
  <table width="800px" height="750px;">

    <!--Encabezado-->
  	<tr>
  		<td colspan="3">
  			<table id="tabla_encabezado" style="width:100%;">
  				<tr>
  					<td style="width:30%;">
  						<center>
                <img src="{{ asset('images/logo_lastest.png') }}" width="150"><br>
  						  NIT 800.185.664-6
              </center>
  					</td>
  					<td style="width:40%; text-align:center; font-size: 14px;">
              <b>
                REGISTRO DE PAGO DE<br>
                SERVICIOS EDUCATIVOS {{ $recibo->centroOperacion->centrooperacion_codigo }}<br>
                N° E-RC {{ $recibo->recibo_consecutivo_centro }}
              </b>
  					</td>
  					<td style="width:30%; text-align:right;">
  						<b>Fecha Expedición</b><br>
  						{{ $recibo->recibo_created_at }}<br>
  						<b>Liquidación:</b> {{ $recibo->recibo_codigo_liquidacion }}
  					</td>
  				</tr>
  			</table>
  		</td>
  	</tr>
    <!--Fin encabezado-->

    <!--Información de impresión-->
  	<tr>
      @php
        //toma el usuario de la direccion de correo electrónico
        $usuario = explode('@', $usuario);
        $usuario = $usuario[0];
      @endphp
      <td colspan="3">
        <p style="font-size:8px; text-align: left;">
          Imprime: {{ $usuario }};
          Fecha: {{ date('Y/m/d H:i:s') }}
        </p>
      </td>
  	</tr>
    <!--Fin información de impresión-->

    <!--Fila central -->
  	<tr>
      <!--Columna datos personales -->
  		<td style="width:50%; vertical-align: top;">
  			<table border="1" width="100%">
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Nombres:</b><br />
  					  &nbsp;{{ $recibo->recibo_cliente_nombres }}
            </td>
  				</tr>
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Identificación:</b><br />
  					  &nbsp;{{ $recibo->recibo_cliente_identificacion }}
            </td>
  				</tr>
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Programa:</b><br />
  					  &nbsp;{{ $recibo->recibo_cliente_programa }}
            </td>
  				</tr>
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Dirección:</b><br />
  					  &nbsp;{{ $recibo->recibo_cliente_direccion }}
            </td>
  				</tr>
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Teléfono:</b><br />
  					  &nbsp;{{ $recibo->recibo_cliente_telefono }}
            </td>
  				</tr>
  				<tr>
  					<td class="columna_etiqueta">
              &nbsp;<b>Ciudad:</b><br />
              &nbsp;{{ $recibo->recibo_cliente_ciudad }}
            </td>
  				</tr>
  				<tr>
  					<td style="min-height:5%;" class="columna_etiqueta">
              &nbsp;<b>Observaciones:</b>
              &nbsp;{{ $recibo->recibo_observaciones }}
            </td>
  				</tr>
  			</table>
  		</td>
      <!--Fin Columna datos personales -->

      <!--Columna conceptos de caja -->
  		<td colspan="2" style="width:50%; vertical-align: top;">
  			<table border="1" width="100%">
  				<tr id="fila_conceptos">
  					<th width="40%">&nbsp;Conceptos de Caja</th>
  					<th class="valor"><center>&nbsp;Valor</center></th>
  				</tr>
  				<tr>
  					<td>&nbsp;Matrícula</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_matricula }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Carnet</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_carnet }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Precooperativa</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_precooperativa }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Curso de Verano</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_curso_verano }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Certificados</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_certificado }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Derechos de Grado</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_derechos_grado }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Habilitación</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_habilitacion }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Seguro Estudiantil</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_seguro_estudiantil }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Estampilla Procultura</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_estampilla }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Inscripción</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_inscripcion }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Homologación</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_homologacion }}</td>
  				</tr>
  				<tr>
  					<td>&nbsp;Otros Conceptos</td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_otros }}</td>
  				</tr>
  				<tr id="fila_total">
  					<td>&nbsp;<b>TOTAL</b></td>
  					<td>&nbsp;$ {{ $recibo->concepto->concepto_total }}</td>
  				</tr>
  			</table>
  		</td>
      <!--Fin columna conceptos de caja-->
  	</tr>
    <!--Fin fila central-->

    <!--Fila modalidades de pago-->
    <tr>
      <td colspan="3">
        <table border="1" width="100%">
          <tr id="fila_conceptos">
            @php
              if(empty($recibo->pagos->ToArray()))
              {
                $col = 1;
              }
              else {
                $col = count($recibo->pagos->ToArray());
              }
            @endphp
            <th colspan="{{$col}}">
              Modalidades de Pago
            </th>
          </tr>
          <tr>
            @if(!empty($recibo->pagos->ToArray()))
              @foreach ($recibo->pagos->ToArray() as $recibo_pago)

                  <td class="columna_etiqueta">
                    &nbsp;<b>Modalidad:</b>
                    &nbsp;{{ $recibo_pago['pago_nombre'] }}
                    <br />
                    &nbsp;<b>Valor:</b>
                    &nbsp;$ {{ $recibo_pago['pivot']['recibopago_valor'] }}

                 @if($recibo_pago['pago_nombre'] == 'Cheque')
                     <br />
                     &nbsp;<b>Entidad Bancaria:</b>
                     &nbsp;{{ $recibo_pago['pivot']['recibopago_entidad_bancaria'] }}
                     <br />
                     &nbsp;<b>N° Cheque:</b>
                     &nbsp;{{ $recibo_pago['pivot']['recibopago_codigo'] }}
                     <br />
                     &nbsp;<b>Plaza:</b>
                     &nbsp;{{ $recibo_pago['pivot']['recibopago_plaza'] }}
                   </td>
                  @elseif($recibo_pago['pago_nombre'] == 'Tarjeta')
                      <br />
                      &nbsp;<b>Entidad Bancaria:</b>
                      &nbsp;{{ $recibo_pago['pivot']['recibopago_entidad_bancaria'] }}
                      <br />
                      &nbsp;<b>Voucher:</b>
                      &nbsp;{{ $recibo_pago['pivot']['recibopago_codigo'] }}
                    </td>
                  @else
                    </td>
                  @endif
                </p>
              @endforeach
            @else
              <td>
                &nbsp;<b>Modalidad:</b>
                &nbsp;--
                <br />
                &nbsp;<b>Valor:</b>
                &nbsp;$ --
              </td>
            @endif
          </tr>
        </table>
      </td>
    </tr>
  	<!--<tr>
  		<td colspan="3">
  			<table id="tabla_pie" border="1" width="100%">
  				<tr style="height: 60px; vertical-align: top;">
  					<td class="fila-encabezado"><b>&nbsp;Entidad Bancaria</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Cheque</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Valor</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Plaza</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Número de voucher</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Fecha</b><br>
  								&nbsp;xxx</td>
  					<td class="fila-encabezado"><b>&nbsp;Firma y Sello</b></td>
  				</tr>
  			</table>
  		</td>
  	</tr>-->
  	<tr>
  		<td colspan="3" align="center" style="font-size: 9px;">
        <b>Campus Pance:</b>
        Cra. 122 No. 12 - 459
        Ext. 2102 - 2104 - 2106 - 0100;
        <b>Campus Meléndez:</b>
        Cra. 94 No. 4C - 04
        Ext. 3112 - 3114 - 3116 - 3118 - 4100
        <b>Sede Compartir:</b>
        Colegio Compartir
        Cra. 25A No. 89A
        Ext. 6020;
        <b>CERES Yumbo:</b>
        Institución Educativa San Francisco Javier
        Cra. 3 No. 5 - 50
        Ext. 6126;
        <b>CERES Alfonso López:</b>
        Colegio Santa Isabel de Hungría
        Cra. 7H Bis No. 76 - 25
        Ext. 6122;
        <b>CERES Jamundí:</b>
        Institución Educativa Parroquial Nuestra
        Señora del Rosario
        Cll. 9 No. 10 - 06
        Ext. 6210
  		</td>
  	</tr>
  </table>
  </div>
</html>

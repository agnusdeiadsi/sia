@extends('browse.erc.menu')

@section('content-modulo')
  @php
  extract($_REQUEST);
  @endphp

  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Cartera')
    <div class="w3-row">
      <div class="w3-col s12">
        <h2>Cartera<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="?"><span class="fa fa-question-circle fa-fw"></span></a></h2>
      </div>
    </div>

    <!--Buscador-->
    <div class="w3-row">
      <!--Buscador para small screens-->
      <div class="w3-col s12 w3-hide-large">
        {!! Form::open(['route' => ['erc.recibos.cartera', $sistema], 'method' => 'get']) !!}
          <div class="input-group w3-col s12 w3-right">
            {{Form::hidden('filtro', $filtro)}}
            {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
            <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
            <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
          </div>
        {!! Form::close() !!}
      </div>

      <!--Buscador para large screens-->
      <div class="w3-col 12 w3-hide-small">
        {!! Form::open(['route' => ['erc.recibos.cartera', $sistema], 'method' => 'get']) !!}
          <div class="input-group w3-col s4 w3-right">
            {{Form::hidden('filtro', $filtro)}}
            {{ Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) }}
            <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
            <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede buscar un recibo por centro de Operación, código de Liquidación, suscriptor, email de quien lo realizó, consecutivo o por fecha de radicación."><span class="fa fa-question-circle fa-fw"></span></a>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
    <hr />

    <!--Menu Tabs-->
    <div class="w3-row w3-bar w3-white">
      <div class="w3-col s6 tablink w3-bottombar" id="1" onclick="openTab(event,'nocontabilizados')">
        <a href="{{ route('erc.recibos.cartera', [$sistema, 'filtro' => '1']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">No Contabilizados</a>
      </div>

      <div class="w3-col s6 tablink w3-bottombar" id="2" onclick="openTab(event,'contabilizados')">
        <a href="{{ route('erc.recibos.cartera', [$sistema, 'filtro' => '2']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Contabilizados</a>
      </div>
    </div>

    <!--Tabs-->
    <div id="nocontabilizados" class="w3-container city">
      <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Radicado</th>
              <th>Centro de Operación</th>
              <th>N° eRC</th>
              <th>Código Liquidación</th>
              <th>Creado por</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_recibos = 0;
            @endphp
            @foreach($recibos as $recibo)
              @if($recibo->estado_id == 1)
                <tr>
                  <td>{{ $recibo->recibo_created_at }}</td>
                  <td>{{ $recibo->centrooperacion_codigo." - ".$recibo->centrooperacion_nombre }}</td>
                  <td>{{ $recibo->recibo_consecutivo_centro }}</td>
                  <td>{{ $recibo->recibo_codigo_liquidacion }}</td>
                  <td>{{ $recibo->email }}</td>
                  <td>
                    <a href="{{ route('erc.recibos.show', [$sistema, $recibo->recibo_id, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Ver"><i class="fa fa-eye fa-fw"></i></a>
                    <a href="{{ url('browse/erc/recibos/download', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
                  </td>
                </tr>
                @php
                  $total_recibos++;
                @endphp
              @endif
            @endforeach

            @if($total_recibos==0)
              <tr>
                <td colspan="6"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
      <div class="w3-center">
        <div class="w3-bar">
          {!! $recibos->appends(['filtro' => '1'])->render() !!}
        </div>
      </div>
      </p>
    </div>

    <div id="contabilizados" class="w3-container city" style="display:none">
      <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Radicado</th>
              <th>Centro de Operación</th>
              <th>N° eRC</th>
              <th>Código Liquidación</th>
              <th>Creado por</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_recibos = 0;
            @endphp
            @foreach($recibos as $recibo)
              @if($recibo->estado_id == 2)
                <tr>
                  <td>{{ $recibo->recibo_created_at }}</td>
                  <td>{{ $recibo->centrooperacion_codigo." - ".$recibo->centrooperacion_nombre }}</td>
                  <td>{{ $recibo->recibo_consecutivo_centro }}</td>
                  <td>{{ $recibo->recibo_codigo_liquidacion }}</td>
                  <td>{{ $recibo->email }}</td>
                  <td>
                    <a href="{{ route('erc.recibos.show', [$sistema, $recibo->recibo_id, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Ver"><i class="fa fa-eye fa-fw"></i></a>
                    <a href="{{ url('browse/erc/recibos/download', [$sistema, $recibo->recibo_id]) }}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass }}" title="Imprimir"><i class="fa fa-print fa-fw"></i></a>
                  </td>
                </tr>
                @php
                  $total_recibos++;
                @endphp
              @endif
            @endforeach

            @if($total_recibos==0)
              <tr>
                <td colspan="6"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
      <div class="w3-center">
        <div class="w3-bar">
          {!! $recibos->appends(['filtro' => '2'])->render() !!}
        </div>
      </div>
      </p>
    </div>
  @else
    <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
      <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
    </div>
  @endif

  <script>
  document.getElementById({{$filtro}}).click();

  function openTab(evt, tabName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-{{$sistema->sistema_colorclass}}", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " w3-border-{{$sistema->sistema_colorclass}}";
  }
  </script>
@endsection

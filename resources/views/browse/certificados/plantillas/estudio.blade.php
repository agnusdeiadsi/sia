@extends('browse.certificados.plantillas.master')

@section('cuerpo')
<p>
	Que {{ $solicitud->solicitud_matriculado_nombres }} ({{ $solicitud->matriculado_id }}), documento de identidad {{ $solicitud->matriculado_documento }} está cursando <b>xxxxxx (xx) semestre</b> del programa <b>{{ $datos->first()->cest_cod_programa }} {{ $datos->first()->cest_desc_programa }}</b> (SNIES {{ $datos->first()->cest_cod_snies }}), en el periodo {{ $datos->first()->cest_periodo }}.
	<br>
	<br>
	<br>
	Total de créditos académicos semanales matriculados: {{ $datos->first()->cest_creditos }}<br>
	Promedio horas de trabajo académico semanal: {{ $datos->first()->cest_hrs_semanales }}
</p>
@endsection
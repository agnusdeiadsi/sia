<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{ $solicitud->solicitud_id }}</title>
	<style>
		.page-break {
		    page-break-after: auto;
		}
		body
		{
			margin:4cm 3cm 2.5cm 3cm;
		}

		p {
			font-family: Cambria, Georgia, serif;
			font-size: 12pt;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
	</style>
</head>

<body>
	<div class="page-break">
		<div id="header">
			<p style="text-align: center;">EL DIRECTOR DE REGISTRO Y CONTROL ACADÉMICO DE LA<br>FUNDACIÓN UNIVERSITARIA CATÓLICA LUMEN GENTIUM<br>DE LA ARQUIDIÓCESIS DE CALI<br>(Aprobada por la Resolución No. 944 del 19 de marzo de 1996)<br>(NIT. 800.185.664-6)</p></center>
			<br>
			<p style="text-align: center;"><b>certifica</b></p>
		</div>
		<br>
		<br>
		<div id="body" style="text-align: justify;">
			@yield('cuerpo')
		</div>
		<br>
		<br>
		<div id="footer">
			<p style="text-align: justify;">Esta constancia se expide a solicitud del interesado(a) para efectos personales. Dado en Santiago de Cali, a los {{ date('d') }} días del mes de {{ date('M') }} de {{ date('Y') }}.</p>
		</div>
	</div>
</body>
</html>
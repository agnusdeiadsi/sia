@extends('browse.layouts.modulo')

@section('title-modulo')
  {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
	<div class="w3-bar-block">
		@if(\Auth::user()->rol_sia == "Administrador" || \Auth::user()->rol_modulo == "Manager" || \Auth::user()->rol_modulo == "Impresor" || \Auth::user()->rol_modulo == "Facturador")
			<a href="{{route('browse.certificados.solicitudes', [$sistema, 'filtro' => 1])}}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-exchange fa-fw"></i> Procesar</a>
		@endif
		@if(\Auth::user()->rol_sia == "Administrador" || \Auth::user()->rol_modulo == "Manager" || \Auth::user()->rol_modulo == "Secretaria" || \Auth::user()->rol_modulo == "Facturador")
			<a href="{{route('browse.certificados.solicitudes', [$sistema, 'filtro' => 2])}}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-handshake-o fa-fw"></i> Recibir</a>
			<a href="{{route('browse.certificados.solicitudes', [$sistema, 'filtro' => 3])}}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-files-o fa-fw"></i> Entregar</a>
		@endif
		<a href="{{route('browse.certificados.solicitudes', [$sistema, 'filtro' => 4])}}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-history fa-fw"></i> Historial</a>
	</div>
@endsection

@section('content-modulo')
	<center><h1><b>{{$sistema->sistema_nombre}}</b></h1></center>
  	<hr>
	<div class="w3-container">
		<center><h3><b>Aquí podrás:</b></h3></center>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-2">
				<center><img src="{{ asset('images/menu-icon/certificados/icon-01.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/certificados/icon-02.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/certificados/icon-03.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/certificados/icon-04.png') }}" class="w3-image"></center>
			</div>
		</div>
	</div>
@endsection

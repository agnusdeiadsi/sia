@extends('browse.certificados.menu')

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  @if($filtro == 1) {{--Estado Procesamiento--}}  
    <h2>Procesar Solicitudes<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Solicitudes en Procesamiento."><span class="fa fa-question-circle fa-fw"></span></a></h2>
    <a href="{{route('browse.certificados.solicitudes.downloadlist', [$sistema])}}" class="w3-btn w3-white w3-border w3-hover-{{ $sistema->sistema_colorclass}}" title="Descargar listado PDF"><i class="fa fa-download fa-fw"></i> Solicitudes Carnets</a>
  @elseif($filtro == 2) {{--Estado En Tranporte--}}
    <h2>Recibir Certificado<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Recibir Certificado elaborados."><span class="fa fa-question-circle fa-fw"></span></a></h2>
  @elseif($filtro == 3) {{--Estado En Sede--}}  
    <h2>Entregar Certificados<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Entregar Certificado al estudiante."><span class="fa fa-question-circle fa-fw"></span></a></h2>
  @elseif($filtro == 4) {{--Estado Entregado--}}  
    <h2>Historial<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Historial Certificados entregados."><span class="fa fa-question-circle fa-fw"></span></a></h2>
  @endif

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    <div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['browse.certificados.solicitudes', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {{Form::hidden('filtro', $filtro)}}
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por ID Solicitud, Código del Estudiante, nombres del Estudiante, Código de liquidación Banner o tipo de Certificado."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['browse.certificados.solicitudes', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {{Form::hidden('filtro', $filtro)}}
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por ID Solicitud, Código del Estudiante, nombres del Estudiante, Código de liquidación Banner o tipo de Certificado."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $solicitudes->appends(['filtro' => $filtro])->render() !!}
    </div>
  </div>

  <div class="w3-responsive">
    <table class="w3-table-all w3-hoverable">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>ID</th>
          <th>Fecha</th>
          <th>ID Estudiante</th>
          <th>Nombre</th>
          <th>Tipo</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @if($solicitudes->count() == 0)
          <tr>
            <td colspan="6"><i>No hay registros</i></td>
          </tr>
        @else
          @foreach($solicitudes as $solicitud)
            <tr>
              <td>{{$solicitud->solicitud_id}}</td>
              <td>{{$solicitud->created_at}}</td>
              <td>{{$solicitud->matriculado_id}}</td>
              <td>{{$solicitud->solicitud_matriculado_nombres}}</td>
              <td>
                @php
                  $tipo = \App\Model\Certificados\Tipo::find($solicitud->tipo_id);  
                @endphp
                {{$tipo->tipo_nombre}}
              </td>
              <td>
                <a href="{{route('browse.certificados.solicitudes.show', [$sistema, $solicitud, 'filtro' => $filtro])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Detalles"><span class="fa fa-eye fa-fw"></span></a>
                <a href="{{route('browse.certificados.solicitudes.print', [$sistema, $solicitud, 'filtro' => $filtro])}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Imprimir"><span class="fa fa-print fa-fw"></span></a>
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $solicitudes->appends(['filtro' => $filtro])->render() !!}
    </div>
  </div>
@endsection

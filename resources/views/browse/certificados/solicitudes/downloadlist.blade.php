<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="../../lib/css.css">
  <script type="text/javascript">
  </script>
  <style type="text/css">
    @page{
      margin: 5px 10px 5px 10px;
    }
    body, table{
      foabezado, #tabla_pie{
      font-siznt-family: calibri;
      font-size: 10.5px;
      border-spacing: 0;
      border-collapse: collapse;
    }

    .header,
    .footer {
        width: 100%;
        text-align: center;
        position: fixed;
    }
    .header {
        top: 0px;
    }
    .footer {
        bottom: 100px;
        margin: 0px;
    }
    .pagenum:before {
        content: counter(page);
    }
  </style>
</head>

<body>

<div class="header">
  <table width="100%" border="1" style="max-width: 2550px; padding: 5px; border-collapse: collapse; font-family: arial; font-size: 12px;">
    <thead>
      <tr>
        <td align="center" with="10%">
          <img src="{{asset('/images/Logo-Color-Vertical.png')}}" width="100" />
        </td>
        <td align="center" style="padding: 5px;" with="60%">
          <h2 style="color: #006894;"><b>Solicitud de Certificados Académicos y Financieros, y Carnets Estudiantiles</b></h2>
        </td>
        <td align="center" with="10%">
          <img src="{{asset('/images/sia-index.png')}}" width="150"/>
        </td>
      </tr>
    </thead>
  </table>
  <p style="text-align: left;">&nbsp;&nbsp;<b>Página <span class="pagenum"></span> | Descarga {{\Auth::user()->email}} | Fecha: {{date('Y/m/d H:i:s')}}</b></p>
  <table width="100%" border="1" style="max-width: 2550px; padding: 5px; border-collapse: collapse; font-family: arial; font-size: 12px;">
    <thead>
      <tr align="center">
        <th>N°</th>
        <th>ID Solicitud</th>
        <th>Código</th>
        <th>Nombres</th>
        <th>Programa</th>
        <th>Semestre</th>
        <th>Foto</th>
      </tr>
    </thead>
    <tbody>
      @php
        $numero = 0
      @endphp
      @foreach($solicitudes as $solicitud)
        <tr>
          <td>
            {{++$numero}}
          </td>
          <td>
            {{$solicitud->solicitud_id}}
          </td>
          <td>
            {{$solicitud->matriculado_id}}
          </td>
          <td>
            {{$solicitud->solicitud_matriculado_nombres}}
          </td>
          <td>
            @php
              $programa = \App\Subarea::where('subarea_codigo', '=', $solicitud->solicitud_matriculado_programa)->first();
            @endphp
            {{$programa->subarea_nombre}}
          </td>
          <td>
            @php
              $programa = \App\Semestre::find($solicitud->solicitud_matriculado_semestre);
            @endphp
            {{$programa->semestre_nombre}}
          </td>
          <td>
            <a href="{{url('browse/certificados/solicitudes/download/photo', $solicitud)}}">Descargar</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="footer">
  <div style="position: absolute; z-index: 1;">
    <center>
      <p style="color: white; font-size: 14px;">
        <b>Sistema Integrado de Aplicaciones<br>
        Vicerrectoría Académica - Registro y Control Académico<br>
        Fundación Universitaria Católica Lumen Gentium</b>
      </p>
    </center>
  </div>

  <div style="position: absolute; z-index: 0;">
    <img src="{{ asset('images/barra-inferior-formatos.png') }}" class="w3-image" style="width: 1060px;">
  </div>
</div>
</body>
</html>
@extends('browse.compras.compras')

@section('modulo')
  <div class="row">
  		<div class="col-sm-12">
  			<div class="w3-panel w3-pale-blue w3-leftbar w3-border-cyan w3-text-grey">
  				<h3>Actualizar Datos</h3>
  				<p>Si sus datos son incorrectos haga clic aquí para <b><a href="#" class="w3-text-cyan">ACTUALIZAR DATOS</a></b> e inténtelo nuevamente.</p>
  			</div>
  		</div>
  	</div>
    {!! Form::open(['url' => 'foo/bar', 'method' => 'post', 'multipart/form-data' => 'true' ]) !!}

    <div class="row w3-text-cyan">
    	<div class="col-sm-3 text-center">
    		<h3><img src="{{ asset('images/Unicatolica-color-azul-vertical.png') }}" alt="logo-unicatolica" width="150"></h3>
    	</div>
    	<div class="col-sm-6 text-center">
    		<h2><b>Formulario Solicitud de <br>Materiales, Insumos o Servicios</b></h2>
    	</div>
    	<div class="col-sm-3 text-center w3-text-grey w3-hide-small">
    		<div class="row text-left">
    			<div class="col-sm-6"><b>Código</b><br>6A-7.2.4.9.4</div>
    			<div class="col-sm-6"><b>Versión</b><br>1.0</div>
    		</div>
    		<div class="row text-left">
    			<div class="col-sm-12"><b>Fecha Vigencia</b><br></div>
    		</div>
    	</div>
    </div>
    <br>
    <div class="row" style="display: none;">
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Código</label>
    			<input type="text" name="codigo_form" id="codigo_form" class="w3-input" value="6A-7.2.4.9.4" readonly>
    		</div>
    	</div>
    	<div class="col-sm-6">
    		<div class="form-group">
    			<label>Versión</label>
    			<input type="text" name="version_form" id="version_form" class="w3-input" value="1.0" readonly>
    		</div>
    	</div>
    </div>

    <!--<form name="form_solicitud_compras" id="form_solicitud_compras" enctype="multipart/form-data" role="form">-->
      <div class="row">
      	<div class="col-sm-12">
      		<div class="w3-panel w3-pale-red w3-leftbar w3-border-red"><p><b>Nota:</b> los campos marcados con asterisco (*) son campos obligatorios.</p></div>
      		<div class="w3-panel w3-leftbar w3-border-cyan">
            <h4 class="w3-text-cyan" style="padding: .2em"><b>1. Datos del Solicitante</b></h4>
          </div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Nombres y Apellidos<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_nombres_solicitante', null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Nombres y Apellidos', 'required' => true, 'readonly']) !!}
      		</div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-6">
      		<div class="form-group">
      			<label>Email<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_email_solicitante', Auth::user()->email, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Email', 'required' => true, 'readonly']) !!}
      		</div>
      	</div>
      	<div class="col-sm-4">
      		<div class="form-group">
      			<label>Teléfono Fijo o Móvil<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_telefono_solicitante', null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Teléfono', 'required' => true]) !!}
      		</div>
      	</div>
      	<div class="col-sm-2">
      		<div class="form-group">
      			<label>Extensión</label>
      			{!! Form::text('solicitud_extension_solicitante', null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'maxlength' => '10', 'placeholder' => 'Entensión']) !!}
      		</div>
      	</div>
      </div>
      <div class="row">
      	<div class="col-sm-12">
      		<div class="form-group">
      			<label>Jefe Inmediato<span style="color: red;">*</span></label>
            {!! Form::text('solicitud_nombres_responsable', null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'maxlength' => '100', 'placeholder' => 'Jefe Inmediato', 'required' => true]) !!}
      		</div>
      	</div>
      </div>
    	<hr>

    		<div class="row">
    			<div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-cyan">
    				      <h4 class="w3-text-cyan" style="padding: .2em"><b>2. Datos de la Solicitud</b></h4>
            </div>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Área\Dependencia\Facultad<span style="color: red;">*</span></label>
    					{!! Form::select('area_id', $areas, '', ['class' => 'w3-input w3-border-cyan w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    				</div>
    			</div>
    			<div class="col-sm-5">
    				<div class="form-group">
    					<label>Subárea<span style="color: red;">*</span></label>
    					<div id="cargar_subareas">
    						{!! Form::select('subarea_id', $subareas, '', ['class' => 'w3-input w3-border-cyan w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    					</div>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="form-group">
    					<label>Tipo Solicitud<span style="color: red;">*</span></label>
              {!! Form::select('tipo_id', ['1' => 'Material o Insumo', '2' => 'Servicio'], null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    				</div>
    			</div>
    		</div>
    		<hr>
    		<label>Centros de Operacion (Sede)<span style="color: red;">*</span></label>
    		<div class="row">
    			<div class="col-sm-12">
            <table id="tabla-centro-operacion" class="table table-condensed table-striped table-responsive">
            	<thead class="w3-cyan w3-text-white">
            		<th>Código</th>
            		<th>Descripción Centro</th>
            		<th></th>
            	</thead>
            	<tbody>
            		<tr class="fila-centro-operacion w3-text-grey">
            			<td><input type="text" name="chk_centro[]" id="chk_centro" class="w3-input w3-border-cyan w3-text-grey input-centro-operacion" maxlength="100" placeholder="Código Centro" readonly></td>
            			<td><input type="text" name="chk_nombre[]" id="chk_nombre" class="w3-input w3-border-cyan w3-text-grey input-centro-operacion" maxlength="100" placeholder="Descripción Centro" readonly></td>
            			<td class="text-center eliminar-centro-operacion"><span class="fa fa-minus-circle"></span></td>
            		</tr>
              </tbody>
              </table>
              <center><a href="#" id="agregar" class="w3-btn w3-blue w3-round-jumbo" data-toggle="modal" data-target="#modelCentroOperacion" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
    			</div>
    		</div>

    		<!-- Modal -->
    		<div id="modelCentroOperacion" class="modal fade" role="dialog">
    		  <div class="modal-dialog">

    		    <!-- Modal content-->
    		    <div class="modal-content">
    		      <div class="modal-header">
    		        <button type="button" class="close" data-dismiss="modal">&times;</button>
    		        <h4 class="modal-title">Centros de Operación</h4>
    		      </div>
    		      <div class="modal-body">
    		        <p>
                  {!! Form::select('centro_id', $centros_operacion, '', ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    		        </p>
    		      </div>
    		      <div class="modal-footer">
    		      	<button type="button" class="w3-btn w3-green" onclick="agregarCentroOperacion(); return false">Agregar</button>
    		        <button type="button" class="w3-btn w3-red" data-dismiss="modal">Cerrar</button>
    		      </div>
    		    </div>

    		  </div>
    		</div>
    		<hr>

    		<div class="row">
    			<div class="col-sm-12">
    				<div class="form-group">
    					<label>Proyecto (Opcional)</label>
    					<?php
    						//$proyectos->mostrarProyectos(null);
    					?>
    				</div>
    			</div>
    		</div>
    		<hr>

    		<label>Centros de Costo<span style="color: red;">*</span></label>
    		<span class="help-block"><b>Atención:</b> Si el Centro de Costo de la solicitud es compartido por favor seleccione los demás Centros de Costo que apliquen.</span><br>
    		<div class="row">
    			<div class="col-sm-12">
            <table id="tabla-centro" class="table table-condensed table-striped table-responsive">
            	<thead class="w3-cyan w3-text-white">
            		<th>Código</th>
            		<th>Descripción Centro</th>
            		<th></th>
            	</thead>
            	<tbody>
            		<tr class="fila-centro w3-text-grey">
            			<td><input type="text" name="cen_costos[]" id="cen_costos" class="w3-input input-centro" maxlength="100" placeholder="Código Centro" readonly></td>
            			<td><input type="text" name="centro_nombre[]" id="centro_nombre" class="w3-input input-centro" maxlength="100" placeholder="Descripción Centro" readonly></td>
            			<td class="text-center eliminar-centro"><span class="fa fa-minus-circle"></span></td>
            		</tr>
              </tbody>
              </table>
              <center><a href="#" id="agregar" class="w3-btn w3-blue w3-round-jumbo" data-toggle="modal" data-target="#modalCentroCosto" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
    			</div>
    		</div>

    		<!-- Modal -->
    		<div id="modalCentroCosto" class="modal fade" role="dialog">
    		  <div class="modal-dialog">

    		    <!-- Modal content-->
    		    <div class="modal-content">
    		      <div class="modal-header">
    		        <button type="button" class="close" data-dismiss="modal">&times;</button>
    		        <h4 class="modal-title">Centros de Costos</h4>
    		      </div>
    		      <div class="modal-body">
    		        <p>
                  {!! Form::select('centro_id', $centros_costos, '', ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'required' => true]) !!}
    		        </p>
    		      </div>
    		      <div class="modal-footer">
    		      	<button type="button" class="w3-btn w3-green" onclick="agregarCentroCostos(); return false">Agregar</button>
    		        <button type="button" class="w3-btn w3-red" data-dismiss="modal">Cerrar</button>
    		      </div>
    		    </div>

    		  </div>
    		</div>
    		<hr>

        <div class="row">
        	<div class="col-sm-12">
        		<label>Insumos, Materiales o Servicios</label>
        		<span class="help-block">
                <ul>
                  <li>En el campo de <b>Costo Estimado</b> no use separadores de miles, millones u otra cantidad, solo use el punto (.) para indicar decimales. Ejemplo: 100253.16 = 100.253,16</li>
                  <li>Para añadir más de un insumo, material o servicio, haga clic en el mas (+).</li>
              </ul>
            </span>
        		<div class="w3-responsive">
        			<table id="tabla" class="table table-condensed table-striped table-responsive">
        				<thead class="w3-cyan w3-text-white">
        					<th></th>
        					<th>Descripción Referencia</th>
        					<th>Unidad de Medida</th>
        					<th>Cantidad Solicitada</th>
        					<th>Especificaciones y/o Características</th>
        					<th>Costo Estimado</th>
        					<th></th>
        				</thead>
        				<tbody>
        					<tr class="fila-base w3-text-grey">
        						<td></td>
        						<td><input type="text" name="des_item[]" id="des_item" class="w3-input w3-sm" maxlength="100" placeholder="Descripción"></td>
        						<td><input type="text" name="uni_item[]" id="uni_item" class="w3-input w3-sm" maxlength="10" placeholder="Unidad de Medida" title="Und, Ton, Kg, Lb, gr, Lt, ml, mt, etc."></td>
        						<td><input type="text" name="cant_item[]" id="cant_item" class="w3-input w3-sm" maxlength="10" placeholder="Cantidad" pattern="[0-9]{1,}"></td>
        						<td><input type="text" name="esp_item[]" id="esp_item" class="w3-input w3-sm" maxlength="100" placeholder="Especificaciones"></td>
        						<td><input type="text" name="cost_item[]" id="cost_item" class="w3-input w3-sm" maxlength="20" placeholder="Costo $"></td>
        						<td class="text-center eliminar"><span class="fa fa-minus-circle"></span></td>
        					</tr>
        				</tbody>
        			</table>
        		</div>
        		<center><a href="#" id="agregar" class="w3-btn w3-blue w3-round-jumbo" onclick="agregarFila(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
        	</div>
        </div>
        <hr>

        <div class="row">
        	<div class="col-sm-12">
        		<div class="form-group">
        			<label>Presupuesto (Opcional) <span class="fa fa-paperclip"></span></label>
              {!! Form::file('solicitud_presupuesto', ['class' => 'w3-input w3-border-cyan w3-text-grey']) !!}
        		</div>
        	</div>
        </div>
    		<hr>

        <div class="row">
        	<div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-cyan">
    				      <h4 class="w3-text-cyan" style="padding: .2em"><b>3. Justificación</b></h4>
            </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<div class="form-group">
        			<label>Justificación<span style="color: red;">*</span></label>
              {!! Form::textarea('solicitud_justificacion', null, ['class' => 'w3-input w3-border-cyan w3-text-grey', 'rows' => '6', 'maxlength' => '255', 'placeholder' => 'Escriba la justificación de la solicitud. (255 Caracteres)', 'required' => true]) !!}
        		</div>
        	</div>
        </div>

        <div class="w3-center">
    		    <button type="submit" class="w3-btn w3-green" id="btn_send_" onclick="/*validarSeleccionCentro(); return false*/"><span class="fa fa-floppy-o"></span> Guardar</button> <button type="reset" id="limpiar" class="w3-btn w3-red"><span class="fa fa-close"></span> Limpiar</button>
        </div>
    {!! Form::close() !!}
@endsection

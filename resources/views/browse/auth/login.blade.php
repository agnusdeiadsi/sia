@extends('layouts.auth')

@section('title')
    Iniciar Sesión
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-default">
                {{--<div class="panel-heading w3-transparent w3-text-white"><h4><b><span class="fa fa-lock"></span> Login</b></h4></div>--}}
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/browse/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-offset-2 col-md-8 text-left">
                                <label for="email" class="control-label w3-text-white"><span class="fa fa-envelope fa-fw"></span> E-Mail</label>
                                <input id="email" type="email" class="w3-input w3-text-grey" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-offset-2 col-md-8 text-left">
                                <label for="password" class="control-label w3-text-white"><span class="fa fa-lock fa-fw"></span> Contraseña</label>
                                <input id="password" type="password" class="w3-input w3-text-grey" name="password" placeholder="Contraseña" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-8 col-md-4 text-left">
                                <div class="checkbox">
                                    <label class="w3-text-white">
                                        <input type="checkbox" name="remember" {{old('remember') ? 'checked' : ''}}> Recordarme
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-6 text-left">
                                <button type="submit" class="w3-btn w3-white w3-border">
                                    <span class="fa fa-sign-in"></span> Entrar
                                </button>

                                <a class="btn btn-link w3-text-white" href="{{ url('/browse/password/reset') }}">
                                    ¿Olvidó su contraseña?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

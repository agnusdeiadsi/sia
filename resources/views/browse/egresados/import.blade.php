@extends('browse.egresados.menu')

@section('content-modulo')
<div class="w3-container">
    <div class="w3-row w3-bar w3-white">
        @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
          <a href="javascript:void(0)" class="w3-text-grey" onclick="openTab(event, 'estudiantes');">
            <div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding">Estudiantes</div>
          </a>
        @endif
        
        <a href="javascript:void(0)" class="w3-text-grey" onclick="openTab(event, 'grado');">
            <div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding">Grados</div>
        </a>
    </div>

    <div id="estudiantes" class="w3-container tab" style="display:none">
        <br>
        <div class="w3-row">
            <div class="col-sm-12">
                <h1 class="w3-text-2017-shaded-spruce w3-panel w3-pale-2017-shaded-spruce w3-leftbar w3-border-2017-shaded-spruce 
                        w3-text-2017-shaded-spruce"><b>Cargar estudiantes</b></h1>
            </div>
        </div>
    	<div class="w3-row" style="float: left;">
            {!! Form::open(['route' => ['uploadxls', $sistema], 'method' => 'POST', 'files' => true, 'id' => 'import-form']) !!}
            <p>
                <b>
                    {!! Form::label('Estudiantes', 'Archivo*') !!}
                    <a class="w3-bar-item" data-toggle="tooltip" title="Subir archivo de estudiantes en formato .xlsx.">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a>
                </b>
                <br>
                    {!! Form::file('estudiantes_file', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'required']) !!}
                </br>
            </p>
            <button class="w3-btn w3-{{$sistema->sistema_colorclass}}" type="submit" id="send-import">
                <span class="fa fa-check fa-fw"></span>
                Enviar
            </button>
            {!! Form::close() !!}
        </div>
    </div>

    <div id="grado" class="w3-container tab" style="display:none">
        <br>
        <div class="w3-row">
            <div class="col-sm-12">
                <h1 class="w3-text-2017-shaded-spruce w3-panel w3-pale-2017-shaded-spruce w3-leftbar w3-border-2017-shaded-spruce 
                        w3-text-2017-shaded-spruce"><b>Cargar grados</b></h1>
            </div>
        </div>
        <div class="w3-row" style="float: left;">
            {!! Form::open(['route' => ['uploadxls-grade', $sistema], 'method' => 'POST', 'files' => true, 'id' => 'import-form']) !!}
            <p>
                <b>
                    {!! Form::label('Estudiantes', 'Archivo*') !!}
                    <a class="w3-bar-item" data-toggle="tooltip" title="Subir archivo de grados en formato .xlsx.">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a>
                </b>
                <br>
                    {!! Form::file('grados_file', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'required']) !!}
                </br>
            </p>
            <button class="w3-btn w3-{{$sistema->sistema_colorclass}}" type="submit" id="send-import">
                <span class="fa fa-check fa-fw"></span>
                Enviar
            </button>
            {!! Form::close() !!}
        </div>
    </div>
</div>

  <script>

  function openTab(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" w3-border-{{ $sistema->sistema_colorclass}}", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-{{ $sistema->sistema_colorclass}}";
  }
  </script>


    <div class="loader" hidden=""></div>
    <script type="text/javascript">

        $('#import-form').submit(function () {
            $('.loader').show()
        })

    </script>
@endsection
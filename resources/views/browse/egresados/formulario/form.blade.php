@extends('browse.egresados.menu')

@section('content-modulo')
<div class="container">
{!! Form::open(['route' => ['browse.egresados.formulario.edit', $sistema], 'method' => 'get']) !!}
  <div class="row">
    <div class="col-sm-4">
      <button type="submit" class="w3-bar-item w3-button w3-{{$sistema->sistema_colorclass}} w3-right"><span class="fa fa-search"></span></button>
        <input type="text" name="search" class="w3-input w3-white w3-border-{{$sistema->sistema_colorclass}} w3-col s10 w3-right" maxlength="20" placeholder="Número de documento" required>
    </div>
  </div>
{!! Form::close() !!}
{{-- dd($egresado->matriculado->matriculadoLogros->first()->created_at) --}}
@if(isset($egresado))
  {{-- dd($egresado->matriculado->emprendimientos->first()->ciudades->first()->ciudad_nombre) --}}
    {!! Form::open(['route' => ['browse.egresados.update', $sistema, $egresado->matriculado_pidm], 'method' => 'post']) !!}
      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h3><b>Puntaje: {{$egresado->matriculado->grados->count() > 0
                              ? $egresado->matriculado->grados->first()->grado_puntaje 
                              : ''}}</b></h3>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">
           {!! Form::text('puntaje', null, ['class' => 'w3-input w3-text-grey input-number']) !!}
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h3><b>1. Informacion Personal</b></h3>    
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-3">
          <p>
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Numero identificacion<span style="color: red">*</span></label>
               
                {!! Form::text('numero', $egresado->identificacion_numero, ['class' => 'w3-input w3-text-grey', 'required' => 'true']) !!}
          </p>
        </div>
      </div>
      <div class="row">
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Primer nombre<span style="color: red">*</span></label>
                 
                  {!! Form::text('first_name', 
                                  $egresado->matriculado->matriculado_primer_nombre, 
                                  ['class' => 'w3-input w3-text-grey input-text', 
                                  'required']) !!}
              </p>
          </div>
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Segundo nombre</label>
                 
                  {!! Form::text('second_name', 
                                  $egresado->matriculado->matriculado_segundo_nombre, 
                                  ['class' => 'w3-input w3-text-grey input-text']) !!}
              </p>
          </div>
          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Primer apellido<span style="color: red">*</span></label>
                 
                  {!! Form::text('last_name_first', 
                                  $egresado->matriculado->matriculado_primer_apellido, 
                                  ['class' => 'w3-input w3-text-grey input-text', 
                                  'required']) !!}
              </p>
          </div>

          <div class="col-sm-3"> 
              <p>
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Segundo apellido<span style="color: red">*</span></label>
                 
                  {!! Form::text('last_name_second', 
                                  $egresado->matriculado->matriculado_segundo_apellido, 
                                  ['class' => 'w3-input w3-text-grey input-text']) !!}
              </p>
          </div>
        </div>
      <div class="row">
        <div class="col-sm-4">
            <p>
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Telfono Fijo<span style="color: red">*</span></label>
                {!! Form::number('tel', $egresado->matriculado->matriculado_telefono, ['class' => 'w3-input w3-text-grey', 'required' => 'true']) !!}
            </p>
        </div>
        <div class="col-sm-4">
            <p>
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono Celular<span style="color: red">*</span></label>
                {!! Form::number('cel', $egresado->matriculado->matriculado_celular, ['class' => 'w3-input w3-text-grey', 'required' => 'true']) !!}
            </p>
        </div>
        <div class="col-sm-4">
            <p>
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Correo<span style="color: red">*</span></label>
                {!! Form::text('email', $egresado->matriculado->matriculado_email, ['class' => 'w3-input w3-text-grey', 'required' => 'true']) !!}
            </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">
            <p>
               <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad de residencia<span style="color: red">*</span></label>
               {{--!! Form::select('ciudad_residencia', 
                                $ciudad->pluck('ciudad_nombre', 'ciudad_id'), 
                                $egresado->matriculado->ciudad_id, 
                                ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' 
                                w3-text-grey', 
                                'placeholder' => 'Seleccionar Uno']) !!--}}

                      {!! Form::text('ciudad_residencia', 
                                      $egresado->matriculado->ciudad != '' 
                                                ? $egresado->matriculado->ciudad->ciudad_nombre 
                                                : '',
                                      ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'w3-text-grey',
                                      'disabled']) !!}
            </p>
        </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>2. Informacion Laboral</b></h3>
            </div>
          </div>
        </div>

      <div>
        {{--@if($egresado->matriculado->laboral->count() > 0)--}}
          <br>
          <div class="row">
            <div class="col-sm-12"> 
                            <table class="table table-condensed table-striped table-responsive">
                <thead class="w3-{{$sistema->sistema_colorclass}}">
                  <th>
                    <center>
                      Fecha de ingreso
                    </center>
                  </th>
                  <th>
                    <center>
                      Situacion laboral
                    </center>
                  </th>
                  <th>
                    <center>
                      Empresa
                    </center>
                  </th>
                  <th>
                    <center>
                      Telefono
                    </center>
                  </th>
                  <th>
                    <center>
                      Cargo
                    </center>
                    
                  </th>
                  <th>
                    <center>
                      Duracion
                    </center>
                    
                  </th>
                  <th>
                    <center>
                      Contrato
                    </center>                
                  </th>
                  <th>
                    <center>
                      Vinculacion
                    </center>
                  </th>
                  <th>
                    <center>
                      Salario
                    </center>
                  </th>
                  
                </thead>
                <tbody>

                @if($egresado->matriculado->laboral->count() > 0)
                  @foreach($egresado->matriculado->laboral as $laboral) 
                    <tr>
                      <td>
                        <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->created_at}}">
                      </td>
                      <td>
                       <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->situacionlaboral->first()->situacionlaboral_nombre}}" >
                      </td>
                    @if($laboral->laboral_empresa != null)
                        <td>
                         <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->laboral_empresa}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->laboral_telefono}}">
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->cargos->first()->nivelcargo_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->duraciones->first()->duracionempresa_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->contratos->first()->contrato_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->vinculaciones->first()->vinculacion_nombre}}" >
                        </td>
                        <td>
                          <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$laboral->salarios->first()->rangosalario_nombre}}" >
                        </td>
                      @endif
                    </tr>
                  @endforeach
                @else
                    <tr>
                      <td>
                        <i>No existen resultados</i>
                      </td>
                    </tr>
                @endif
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <label class="w3-text-{{$sistema->sistema_colorclass}}">Situacion Laboral<span style="color: red">*</span></label>
               {!! Form::select('laboral_situacion', $situacionLaboral->pluck('situacionlaboral_nombre', 'situacionlaboral_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar', 'id' => 'situacionlaboral']) !!}
            </div>
          </div>
          <br>
          <div id="form-laboral" hidden="">
          <div class="row">
            <div class="col-sm-12">
              <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                <h4><b>2.1. Datos de la empresa</b></h4>    
              </div>
            </div>
          </div>
            <div class="row">
              <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                     {!! Form::text('name_empresa', null, ['class' => 'w3-input w3-text-grey habilitar', 'maxlength' => '50', 'placeholder' => 'Nombre empresa', 'id' => 'codigo', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono de la empresa<span style="color: red">*</span></label>
                     {!! Form::text('tel_empresa', null, ['class' => 'w3-input w3-text-grey habilitar input-number', 'maxlength' => '50', 'placeholder' => 'Telefono empresa', 'disabled']) !!}
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Tipo Empresa<span style="color: red">*</span></label>
               {!! Form::select('tipoEmpresa', $tipoEmpresa->pluck('tipoempresa_nombre', 'tipoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                  <label class="w3-text-{{$sistema->sistema_colorclass}}">Sector empresa<span style="color: red">*</span></label>
                 {!! Form::select('sector', $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar']) !!}
              </div>

              <div class="col-sm-4">
                  <div class="col-sm-6">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Departamento<span style="color: red">*</span></label>
                    {!! Form::select('select_departamento', $departamento->pluck('departamento_nombre', 'departamento_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccione', 'id' => 'departamento']) !!}
                  </div>
                  <div class="col-sm-6">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                   {!! Form::select('ciudad', 
                                    [], 
                                    null, 
                                    ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 
                                        'id' => 'select_Ciudad_Bu',
                                        'disabled']) !!}
                  </div>
              </div>
             
            </div>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>2.2. Datos Laborales</b></h4>    
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Area<span style="color: red">*</span></label>
               {!! Form::select('area', $areaPertenece->pluck('areapertenece_nombre', 'areapertenece_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Cargo<span style="color: red">*</span></label>
               {!! Form::select('cargo', $cargo->pluck('nivelcargo_nombre', 'nivelcargo_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Relacion con el Programa<span style="color: red">*</span></label>
               {!! Form::select('relacionPrograma', $relacionPrograma->pluck('relacionprograma_nombre', 'relacionprograma_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Ocupacion<span style="color: red">*</span></label>
               {!! Form::select('ocupacion', $ocupacion->pluck('ocupacion_nombre', 'ocupacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Duracion<span style="color: red">*</span></label>
               {!! Form::select('duracionEmpresa', $duracionEmpresa->pluck('duracionempresa_nombre', 'duracionempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
             {{-- <div class="col-sm-2">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Pais<span style="color: red">*</span></label>
               {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccionar']) !!}
              </div>--}}
            </div>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
                  <h4><b>2.3. Contratacion</b></h4>    
                </div>
              </div>
            </div>
            <div class="row">
               <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Vinculacion<span style="color: red">*</span></label>
               {!! Form::select('vinculacion', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Contrato<span style="color: red">*</span></label>
               {!! Form::select('contrato', $contrato->pluck('contrato_nombre', 'contrato_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
              <div class="col-sm-4">
                <label class="w3-text-{{$sistema->sistema_colorclass}}">Rango Salarial<span style="color: red">*</span></label>
               {!! Form::select('salario', $rangoSalarial->pluck('rangosalario_nombre', 'rangosalario_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>
            </div>
          </div>
        {{--@else
        <div>
          <div class="row">
            <div class="col-sm-12"> 
                  <p>
                      <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa donde Actualmente Labora<span style="color: red">*</span></label>
                      {!! Form::text('name_empresa', null, ['class' => 'w3-input w3-text-grey', 'maxlength' => '50', 'placeholder' => 'Nombre Empresa', 'id' => 'codigo', 'required' => 'true']) !!}
                  </p>
            </div>
          </div>
        </div>--}}
        {{--@endif--}}
        <hr>
        <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>3. Informacion Academica</b></h3>
            </div>
          </div>
        </div>
        <div>
         <label class="w3-text-{{$sistema->sistema_colorclass}}"><b>¿Ha realizado Otros estudios?</b></label>
          <div class="row">
          <div class="col-sm-12">
            <div class="w3-responsive">
              <table id="tabla-estudios" class="table table-condensed table-striped table-responsive">
                <thead class="w3-{{$sistema->sistema_colorclass}}">
                  <th></th>
                  <th>Nivel Academico</th>
                  <th>Nombre Institucion</th>
                  <th>Titulo Obtenido</th>
                  <th>Inicio</th>
                  <th>Fin</th>
                  <th></th>
                </thead>
                <tbody>

                  <tr class="fila-estudios w3-text-grey">
                    <td></td>
                    <td>
                      {!! Form::select('estudio_nivel[]', $nivel->pluck('nivelacademico_nombre', 'nivelacademico_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey nivel', 'placeholder' => 'Seleccionar Uno', 'disabled' => 'true']) !!}
                    </td>
                    <td>
                      <input type="text" name="estudio_nombreInstitucion[]"  class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm institucion" maxlength="100" placeholder="Nombre de la institucion" disabled="true">
                    </td>
                    <td>
                      <input type="text" name="estudio_titulo[]" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm titulo" maxlength="50" placeholder="Titulo Obtenido" disabled="true">
                    </td>
                    <td>
                      <input type="date" name="estudio_inicio[]" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm inicio" maxlength="100" disabled="true">
                    </td>
                    <td>
                      <input type="date" name="estudio_fin[]" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm fin" maxlength="20" disabled="true">
                    </td>
                    <td class="text-center eliminar-estudio">
                      <span class="fa fa-minus-circle"></span>
                    </td>
                  </tr>

                @foreach($egresado->matriculado->estudios as $data)
                  <tr class="w3-text-grey">
                    <td></td>
                    <td>
                      <input type="hidden" value="{{$data->nivelAcademico->nivelacademico_id}}">
                      <input type="text" name="estudio_nivel_" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm nivel" maxlength="100" placeholder="Nombre de la institucion" readonly="" value="{{$data->nivelAcademico->nivelacademico_nombre}}" >
                    </td>
                    <td>
                      <input type="text" id="uni_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm institucion" maxlength="100" placeholder="Nombre de la institucion" readonly="" value="{{$data->estudio_institucion}}" >
                    </td>
                    <td>
                      <input type="text" id="cant_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm titulo" maxlength="50" placeholder="Titulo Obtenido" readonly="" value="{{$data->estudio_titulo}}">
                    </td>
                    <td>
                      <input type="text" id="esp_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm inicio" maxlength="100" readonly="" value="{{$data->estudio_inicio}}">
                    </td>
                    <td>
                      <input type="text" id="cost_item" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm fin" maxlength="20" readonly="" value="{{$data->estudio_fin}}">
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <center><a href="javascript:void(0)" id="agregar" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-round-jumbo" onclick="agregarFilaEstudios(); return false" title="Agregar ítem/fila"><i class="fa fa-plus fa-fw"></i></a></center>
          </div>
        </div>
      </div><br>
      <div class="row">
          <div class="col-sm-12">
            <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
              <h3><b>4. Informacion de emprendimiento</b></h3>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table class="table table-condensed table-striped table-responsive">
            <thead class="w3-{{$sistema->sistema_colorclass}}">
              <td>
                Nombre empresa
              </td>
              <td>
                Direccion
              </td>
              <td>
                Telefono empresa
              </td>
              <td>
                Ciudad
              </td>
              <td>
                Sector
              </td>
              <td>
                Tamaño
              </td>
            </thead>
            <tbody>
              @if($egresado->matriculado->emprendimiento->count() > 0)

                @foreach($egresado->matriculado->emprendimiento as $emprendimiento)  
                  <tr>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_empresa}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_direccion}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->emprendimiento_telefono}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->ciudades->first()->ciudad_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->sectoresempresas->first()->sectorempresa_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$emprendimiento->tamanoempresas->first()->tamanoempresa_nombre}}">
                    </td>
                  </tr>
                @endforeach
              @else
                  <tr>
                    <td>
                      <i>No existen resultados</i>
                    </td>
                  </tr>
              @endif
              
            </tbody>
          </table>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-6">
          <label><h4>¿Tiene Empresa?</h4></label><br>
              <label class="w3-text-{{$sistema->sistema_colorclass}}">No tiene</label>
              {{ Form::radio('empresa', 'no', true, ['class' => 'show-form-em']) }}
              <br>
              <label class="w3-text-{{$sistema->sistema_colorclass}}" style="margin-right: 8px">Si tiene</label>          
              {{ Form::radio('empresa', 'si', false, ['class' => 'show-form-em']) }}
          </div>
        
      </div>

      <div id="form-emprendimiento" hidden="">
        <div class="row">
          <div class="col-sm-4">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Nombre de la empresa<span style="color: red">*</span></label>
                       {!! Form::text('nombre_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Nombre Empresa', 'disabled']) !!}
          </div>
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Direccion de la empresa<span style="color: red">*</span></label>
                       {!! Form::text('direccion_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Dirrecion Empresa', 'disabled']) !!}
          </div>
          <div class="col-sm-4">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Telefono de la empresa<span style="color: red">*</span></label>
                       {!! Form::number('telefono_empresa_em', null, ['class' => 'w3-input w3-text-grey habilitar_em', 'maxlength' => '50', 'placeholder' => 'Telefono Empresa', 'disabled']) !!}
          </div>
        </div>
        <br>
        <div class="row">

              {{--<div class="col-sm-2">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                   {!! Form::select('ciudad_em', $ciudad->pluck('ciudad_nombre', 'ciudad_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>--}}

              <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Sector empresa<span style="color: red">*</span></label>
                   {!! Form::select('sector_em', $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>

              <div class="col-sm-4">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Tamaño empresa<span style="color: red">*</span></label>
                   {!! Form::select('tamano_em', $tamanoEmpresa->pluck('tamanoempresa_nombre', 'tamanoempresa_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 'placeholder' => 'Seleccionar', 'disabled']) !!}
              </div>

              <div class="col-sm-4">
                  <div class="col-sm-6">
                     <label class="w3-text-{{$sistema->sistema_colorclass}}">Departamento<span style="color: red">*</span></label>
                    {!! Form::select('select_departamento', $departamento->pluck('departamento_nombre', 'departamento_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccione', 'id' => 'departamento_em']) !!}
                  </div>
                  <div class="col-sm-6">
                    <label class="w3-text-{{$sistema->sistema_colorclass}}">Ciudad<span style="color: red">*</span></label>
                   {!! Form::select('ciudad_em', 
                                    [], 
                                    null, 
                                    ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey habilitar_em', 
                                      'id' => 'ciudad_em',
                                      'disabled',
                                      'placeholder']) !!}
                  </div>
              </div>
        </div>  
      </div><br>
      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h4><b>5. Logros</b></h4>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <table class="table table-condensed table-striped table-responsive">
              <thead class="w3-{{$sistema->sistema_colorclass}}">
                <th>
                  Fecha y hora de ingreso
                </th>
                <th>
                  Fecha logro
                </th>
                <th>
                  Nombre Logro
                </th>
              </thead>
              <tbody>

                  @if($egresado->matriculado->matriculadoLogros->count() > 0)
                    @foreach ($egresado->matriculado->matriculadoLogros as $logro)
                      <tr>
                        <td>
                          <input type="text" 
                                  class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" 
                                  disabled="" 
                                  value="{{$logro->created_at}}">
                        </td>
                        <td>
                          <input date="text" 
                                  class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" 
                                  disabled="" 
                                  value="{{$logro->matriculadologro_fecha}}">
                        </td>
                        <td>
                          <input type="text" 
                                  class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" 
                                  disabled="" 
                                  value="{{$logro->logro->logro_nombre}}">
                        </td>
                      </tr>
                    @endforeach
                    
                  @else

                    <tr>
                      <td>
                        <i>No existen resultados</i>
                      </td>
                    </tr>
                  @endif
              </tbody>
          </table>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-3">
            <label class="w3-text-{{$sistema->sistema_colorclass}}">Logro</label>
            {!! Form::select('logroEgresado', 
                              $logros->pluck('logro_nombre', 'logro_id'),
                              null,
                              ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey',
                              'placeholder' => 'Seleccione'] ) !!}
        </div>
       
        <div class="col-sm-3">
             <label class="w3-text-{{$sistema->sistema_colorclass}}">Fecha</label>
            {!! Form::date('logroEgresadodate', null,
                              ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey',
                              'placeholder' => 'Seleccione'] ) !!}
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-12">
          <div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}}">
            <h4><b>6. Reconocimiento</b></h4>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <table class="table table-condensed table-striped table-responsive">
              <thead class="w3-{{$sistema->sistema_colorclass}}">
              <td>
                Nombre de reconocimiento
              </td>
              <td>
                Nombre de la organizacion
              </td>
              <td>
                Fecha de reconocimiento
              </td>
            </thead>
            <tbody>
              @if($egresado->matriculado->reconocimientos->count() > 0)
                @foreach($egresado->matriculado->reconocimientos as $reconocimiento)
                  <tr>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_nombre}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_organizacion}}">
                    </td>
                    <td>
                      <input type="text" class="w3-input w3-border-{{$sistema->sistema_colorclass}} w3-sm" disabled="" value="{{$reconocimiento->reconocimiento_fecha}}">
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td>
                    <i>No existen resultados</i>
                  </td>
                </tr>
              @endif
            
            </tbody>
          </table>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-12">
          <button class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
            <span class="fa fa-floppy-o fa-fw"></span>
              Guardar
          </button>
        </div>
      </div>
    {!!Form::close()!!}
@endif
</div>

<script type="text/javascript">
    
    $('#departamento_em').change(function () 
    {

        $.post(url+'/public/api/'+$(this).val()+'/departamento', function (data) {
          
          var html_select = ''
          var html_cb = ''

          for (var i = 0; i < data.length; i++) 
          {

            html_select += '<option value="'+data[i].ciudad_id+'">'+data[i].ciudad_nombre+'</option>'
          
          }

          $('#ciudad_em').html(html_select)

        })

    });

</script>
@endsection
@extends('browse.egresados.menu')

@section('content-modulo')
	{!! Form::open(['route' => ['browse.egresados.send', $sistema], 'method' => 'get', 'id' => 'send']) !!}
		<div class="row">
			<div class="col-sm-4">
		    		<p>
		                <b>
		                    {!! Form::label('label_facultad', 'Facultad:') !!}
		                    <span style="color: red;">*</span>
		                </b>
		                {!! Form::select('facultad', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-facu']) !!}
		            </p>
		    </div>

			<div class="col-sm-8">
				<p>
		            <b>
		                {!! Form::label('label_program', 'Programa:') !!}
		                <span style="color: red;">*</span>
		                <a class="w3-bar-item" data-toggle="tooltip" title="Para seleccionar mas de un programa, presiona ctrl y a continuacion el programa que se desea">
		                    <span class="fa fa-question-circle fa-fw">
		                    </span>
		                </a>
		            </b>
		                
		            {!! Form::select('name_program[]', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-program', 'multiple']) !!}
		        </p>

			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<p>
		            <b>
		                {!! Form::label('label_año_graduacion_send', 'Tiempo de haberse graduado:') !!}
		            </b>
		            {!! Form::select('name_year_graduate_send', ['1' => '1 Año',
		            										'3' => '3 Años',
		            										'5' => '5 Años'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'select-year-graduate', 'placeholder' => 'Seleccione',
		            										'required']) !!}
			    </p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<button class="w3-btn w3-{{$sistema->sistema_colorclass}}" id="submit-send">
					<span class="fa fa-check fa-fw"></span>
					Enviar
				</button>
			</div>
		</div>
	{!! Form::close() !!}
	{{--dd($no_email)--}}
	@if(isset($no_email) && count($no_email))
	<hr>
	<label>
		Las siguientes personas no tienen un correo electronico registrado ó no es valido
	</label>
	{{-- dd($no_email->first()->matriculado->referencia) --}}
		<table class="w3-table-all">
			
			<thead>
				<tr class="w3-{{ $sistema->sistema_colorclass }}">
					<th>
						Nombres
					</th>
					<th>
						Apellidos
					</th>
					<th>
						Celular
					</th>
					<th>
						Nombre referencia
					</th>
					<th>
						Parentesco
					</th>
					<th>
						Telefono referencia
					</th>
					<th>
						Celular referencia
					</th>
				</tr>
			</thead>
			
			
			<tbody>
				@foreach($no_email as $dato)
					<tr>
						<td>
							{{$dato->matriculado_primer_nombre.' '.$dato->matriculado_segundo_nombre}}
						</td>
						<td>
							{{$dato->matriculado_primer_apellido.' '.$dato->matriculado_segundo_apellido}}
						</td>
						<td>
							{{$dato->matriculado_celular}}
						</td>
						<td>
							{{$dato->referencia_nombres}}
						</td>
						<td>
							{{$dato->referencia_parentesco}}
						</td>
						<td>
							{{$dato->referencia_telefono}}
						</td>
						<td>
							{{$dato->referencia_celular}}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	<div class="row">
	    <div class="col-sm-12">
	        <center>
				{{$no_email->appends(array('name_year_graduate_send' => $name_year_graduate_send,
										   'name_program' => $name_program,
								   		   'paginate' => 'a' ))}}
	        </center>
	    </div>
	</div>

	@endif

	<div class="loader" hidden=""></div>
	<script type="text/javascript">

		$('#send').submit(function () {
			$('.loader').show()
		})

	</script>
@endsection
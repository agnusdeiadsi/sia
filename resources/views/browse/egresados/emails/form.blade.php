@component('mail::message')

Apreciado egresado.
Nuestro deseo es continuar fortaleciendo tu desarrollo profesional, para ello agradecemos diligenciar la siguiente encuesta (Link encuesta) 

Cordialmente,
Departamento de Egresados de UNICATÓLICA

@endcomponent
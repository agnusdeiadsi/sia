@extends('browse.layouts.modulo')

@section('title-modulo')
    {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
<div class="w3-bar-block">
   {{-- <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.reportes.prueba', $sistema) }}">
        <i class="fa fa-line-chart fa-fw">
        </i>
        Prueba
    </a>--}}
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.formulario.form', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Buscar
        </a>

        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.reportes.formData', $sistema) }}">
            <i class="fa fa-th-list fa-fw">
            </i>
            Informes
        </a>

        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.reportes.graficos', $sistema) }}">
            <i class="fa fa-bar-chart fa-fw">
            </i>
            Graficar
        </a>

        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.index-send', $sistema) }}">
            <i class="fa fa-fax fa-fw">
            </i>
            Enviar encuesta
        </a>
    @endif
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.import', $sistema) }}">
            <i class="fa fa-cloud-upload fa-fw">
            </i>
            Importar Archivo
        </a>
   
</div>
@endsection

@section('content-modulo')
<center>
    <h1>
        <b>
            {{$sistema->sistema_nombre}}
        </b>
    </h1>
</center>
<hr>
    <div class="w3-container">
        <center>
            <h3>
                <b>
                    Aquí podrás:
                </b>
            </h3>
        </center>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-1">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-01.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-02.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-03.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-04.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-05.png') }}"/>
                </center>
            </div>
        </div>
    </div>
    @endsection
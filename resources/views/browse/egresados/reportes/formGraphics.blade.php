@extends('browse.egresados.menu')

@section('content-modulo')

<div class="w3-container">
{!! Form::open(['method' => 'post']) !!}
	<div class="row">
		<div class="col-sm-12">
			<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
				w3-text-{{$sistema->sistema_colorclass}}}"><b>Graficas</b></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
	    		<p>
	                <b>
	                    {!! Form::label('label_facu', 'Facultad:') !!}
	                    <span style="color: red;">*</span>
	                </b>
	                {!! Form::select('name_facu', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-facu']) !!}
	            </p>
	    </div>

		<div class="col-sm-8">
			<p>
                <b>
                    {!! Form::label('label_program', 'Programa:') !!}
                    <span style="color: red;">*</span>
                    <a class="w3-bar-item" data-toggle="tooltip" title="Para seleccionar mas de un programa, presiona ctrl y a continuacion el programa que se desea">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a>
                </b>
                {!! Form::select('name_program', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-program', 'multiple']) !!}
	        </p>
		</div>
	</div>

	<hr>

	<div hidden="" id="year_filters">

		<div class="row">
			<div class="col-sm-12">
				<label class="w3-text-grey">Se debe seleccionar uno de los siguientes filtros</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-sm-3">
				<fieldset>
					<legend>
						<center>
						<b>
		                	{!! Form::label('label_period', 'Periodo:') !!}
		            	</b>
		            	</center>
		            </legend>
				<p>
		            
		            {!! Form::select('name_period', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey selectTime', 'id' => 'select-period']) !!}
			    </p>
			    </fieldset>
			</div>
			
			<div class="col-sm-3">
				<p>
		            <b>
		                {!! Form::label('label_period_since', 'Desde:') !!}
		            </b>
		            {!! Form::select('name_period_since', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'select-period-since']) !!}
			    </p>
			</div>

			<div class="col-sm-2">
				<p>
		            <b>
		                {!! Form::label('label_period_until', 'Hasta:') !!}
		            </b>
		            {!! Form::select('name_period_until', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey selectTime', 'id' => 'select-period-until']) !!}
			    </p>
			</div>

			<div class="col-sm-4">
				<fieldset>
					<legend>
						<center>
						<b>
		            		{!! Form::label('label_año_graduacion', 'Tiempo de haberse graduado:') !!}
		            	</b>
		            	</center>
		            </legend>
		            <p>
		            {!! Form::select('name_year_graduate', ['1' => '1 Año',
		            										'3' => '3 Años',
		            										'5' => '5 Años'], 
		            										null, 
		            										['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey selectTime', 
		            										'id' => 'select-year-graduate', 'placeholder' => 'Seleccione']) !!}
					</p>
				</fieldset>
			</div>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-sm-3" id="select-tipo-filtro" style="display: none;">
			<p>
	            <b>
	                {!! Form::label('label_tipo_filtro', 'Tipo de Filtro:') !!}
	                <span style="color: red;">*</span>
	            </b>
	            {!! Form::select('name_filter', ['laboral' => 'Laboral',	
	            								 'emprendimiento' => 'Emprendimiento'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'select-type-filter', 'placeholder' => 'Seleccionar el tipo de Filtro']) !!}
		    </p>
		</div>
	</div>

	<section>
		<div id="laboral" style="display: none;">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
						w3-text-{{$sistema->sistema_colorclass}}}" style="margin-left: 10px;"><b>INFORMACION LABORAL</b></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
				<p>
		            <b>
		                {!! Form::label('label_laboral', 'Filtro:') !!}
		                <span style="color: red;">*</span>
		            </b>
		            {!! Form::select('action', ['graphic_laboral' => 'Situacion Laboral',
		            							 'graphic_relacion' => 'Relacion con el Programa',
	            								 'graphic_cargo' => 'Cargo Laboral',	
	            								 'graphic_duracion' => 'Duracion en Ultimo Empleado',
	            								 'graphic_contrato' => 'Tipo Contrato',
	            								 'graphic_rangoSalarial' => 'Rango Devengado',
	            								 'graphic_sectorEmpresa' => 'Sector Empresa',
	            								 'graphic_ocupacion' => 'Ocupacion Actual',
	            								 'graphic_vinculacion' => 'Tipo Vinculacion',
	            								 'graphic_area' => 'Area o Dpto al que Pertenece', 
	            								 'graphic_tipoEmpresa' => 'Tipo Empresa'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'select-laboral-filter', 'placeholder' => 'Seleccionar el tipo de Filtro']) !!}
		    	</p>
				</div>
				<br>
				<div id="refrescar-laboral" hidden="">
					<div class="col-sm-4">
						<label id="refrescar-laboral-btn" class="w3-btn w3-{{$sistema->sistema_colorclass}}">Graficar</label>
					</div>
				</div>
				<div class="col-sm-12">
					<div style="overflow: scroll;">
						<table class="w3-table-all">
		                    <tr class="w3-{{ $sistema->sistema_colorclass }}" id="table-head">
		                        <th>
		                            Tabla
		                        </th>
		                        
		                    </tr>
		                    <tr id="content-table">
		                    	<td colspan="4">
		                    		
		                    	</td>
		                    </tr>
	                	</table>
					</div>
					
	                <br>
	                <div id="container-laboral" style="min-width: 200px; 
	                								   height: 400px; 
	                								   margin: 0 auto;"></div>
				</div>
       		 </div>	 
       	</div>
		{{--<div id="grado" style="display: none;">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
						w3-text-{{$sistema->sistema_colorclass}}}" style="margin-left: 10px;"><b>INFORMACION GRADO</b></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					asdas
				</div>
			</div>
		</div>--}}

		<div id="emprendimiento" style="display: none;">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
						w3-text-{{$sistema->sistema_colorclass}}}" style="margin-left: 10px;"><b>INFORMACION EMPRENDIMIENTO</b></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<p>
			            <b>
			                {!! Form::label('label_laboral', 'Filtro:') !!}
			                <span style="color: red;">*</span>
			            </b>
			            {!! Form::select('action', ['graphic_sectorEmpresa_Bu' => 'Sector Empresa',
			            							'graphic_tamanoEmpresa' => 'Tamaño de la Empresa',
			            							'graphic_ciudadEmpresa' => 'Ubicacion de la Empresa'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.'w3-text-grey', 'id' => 'select-business-filter', 'placeholder' => 'Seleccionar el tipo de Filtro']) !!}
			    	</p>
				</div>

				<br>
				<div id="refrescar-bussines" hidden="">
					<div class="col-sm-4">
						<label id="refrescar-bussines-btn" class="w3-btn w3-{{$sistema->sistema_colorclass}}">Graficar</label>
					</div>
				</div>
				<div class="col-sm-12">
					<table class="w3-table-all">
	                    <tr class="w3-{{ $sistema->sistema_colorclass }}" id="table-head-business">
	                        <th>
	                            Tabla
	                        </th>
	                        
	                    </tr>
	                    <tr id="content-table-business">
	                    	<td colspan="4">
	                    		
	                    	</td>
	                    </tr>
	                </table>
	                <br>
	                <div id="container-business" style="min-width: 200px; height: 400px; margin: 0 auto"></div>
				</div>

			</div>
		</div>

		{{--<div id="logros" style="display: none;">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
						w3-text-{{$sistema->sistema_colorclass}}}" style="margin-left: 10px;"><b>INFORMACION LOGROS</b></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					asdas
				</div>
			</div>
		</div>--}}
	</section>
{!! Form::close() !!}
</div>
@endsection
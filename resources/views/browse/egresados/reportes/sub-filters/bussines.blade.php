<div class="container">

    <div class="row separate" style="border-left: 6px solid #DCAC34; background-color: lightgrey;">
        <div class="col-sm-12">
            <label class="w3-text-grey">Solo son permitidos 4 filtros</label>
        </div>
    </div>

    <div class="row separate">
        <div class="col-sm-12">
            <button name="action" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-block"
                value="eDownload">
                <span class="fa fa-cloud-download fa-fw"></span>
                Descargar informacion de emprendimiento
            </button>
        </div>
    </div>
    
    <div class="row separate">
        <div class="col-sm-4">
            <div>
                <h6 class="labels-laboral" ><b>Sector de la Empresa</b></h6>
                {!! Form::select('select_sectorEmpresa_Bu[]', $sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_sectorEmpresa_Bu']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div>
                <h6 class="labels-laboral"><b>Tamaño de la Empresa</b></h6>
                {!! Form::select('select_tamaño_empresa[]', $tamanoEmpresa->pluck('tamanoempresa_nombre', 'tamanoempresa_id'),null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_tamaño_empresa']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <fieldset>
                <legend><h6><b><center>Ubicacion de la empresa</center></b></h6></legend>
                <div>
                    <h6 class="labels-laboral" ><b>Departamento Ubicación de la Empresa</b>
                        <a class="w3-bar-item" data-toggle="tooltip" title="El departamento no cuenta dentro de los 4 filtros permitidos">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a></h6>
                    {!! Form::select('select_departamento', $departamento->pluck('departamento_nombre', 'departamento_id'), null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'placeholder' => 'Seleccione', 'id' => 'departamento']) !!}

                    <h6 class="labels-laboral" ><b>Ciudad Ubicación de la Empresa</b>
                    <a class="w3-bar-item" data-toggle="tooltip" title="Para seleccionar mas de una ciudad, presiona ctrl y a continuacion la ciudad que se desea">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a></h6>
                    {!! Form::select('select_Ciudad_Bu[]', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey city', 'multiple' => 'multiple', 'id' => 'select_Ciudad_Bu']) !!}
                </div>
            </fieldset>  
        </div>
    </div>

</div>


<div class="container">

	<div class="row separate" style="border-left: 6px solid #DCAC34; background-color: lightgrey;">
        <div class="col-sm-12">
            <label class="w3-text-grey">Solo son permitidos 4 filtros</label>
        </div>
    </div>

	<div class="row separate">
        <div class="col-sm-12">
            <button name="action" 
            	class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-block"
                value="loDownload">
                <span class="fa fa-cloud-download fa-fw"></span>
                Descargar informacion de logros
            </button>
        </div>
    </div>

	<div class="row separate">
		<div class="col-sm-4">
	        <h6 class="labels-laboral"><b>Merito:</b></h6>
	        {!! Form::select('select_merito[]', $logros->pluck('logro_nombre', 'logro_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_merito']) !!}
		</div>
	    
	</div>

</div>
     
<div class="container">

<div class="row separate" style="border-left: 6px solid #DCAC34; background-color: lightgrey;">
    <div class="col-sm-12">
        <label class="w3-text-grey">Solo son permitidos 4 filtros</label>
    </div>
</div>

<div class="row separate">
    <div class="col-sm-12">
        <button name="action" class="w3-btn w3-{{$sistema->sistema_colorclass}} w3-block"
            value="lDownload">
            <span class="fa fa-cloud-download fa-fw"></span>
            Descargar informacion laboral
        </button>
    </div>
</div>

<div class="row separate">

    <div class="col-sm-4">
        <div>
            <h6  class="labels-laboral" ><b>Situacion Laboral</b></h6>
                {!! Form::select('select_situacion_laboral[]', $situacionLaboral->pluck('situacionlaboral_nombre', 'situacionlaboral_id'), null, ['class' => 'multiple-class select select_filtros  select_situacion_laboral', 'multiple' => 'multiple', 'id' => 'select_situacion_laboral']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Duracion en su ultimo empleo</b></h6>
            {!! Form::select('select_duracion[]', $duracionEmpresa->pluck('duracionempresa_nombre', 'duracionempresa_id'), 
                                            null, ['class' => 'multiple-class select select_filtros  select_duracion', 'multiple' => 'multiple', 'id' => 'select_duracion']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Cargo Laboral</b></h6>
            {!! Form::select('select_cargo[]', $cargo->pluck('nivelcargo_nombre', 'nivelcargo_id'), null, ['class' => 'multiple-class select select_filtros  select_cargo', 'multiple' => 'multiple', 'id' => 'select_cargo' ]) !!}
        </div>
    </div>
</div>

<div  class="row separate">
    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Tipo de Contrato</b></h6>
            {!! Form::select('select_contrato[]', $contrato->pluck('contrato_nombre', 'contrato_id'),null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_contrato']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Sector de la Empresa</b></h6>
            {!! Form::select('select_sectorEmpresa[]',$sectorEmpresa->pluck('sectorempresa_nombre', 'sectorempresa_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_sectorEmpresa']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Rango Devengado (Rango SMLV)</b></h6>
            {!! Form::select('select_rangoSalarial[]', $rangoSalarial->pluck('rangosalario_nombre', 'rangosalario_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_rangoSalarial' ]) !!}
        </div>
    </div>
</div>

<div class="row separate">
    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Relacion con el Programa</b></h6>
            {!! Form::select('select_relacionPrograma[]', $relacionPrograma->pluck('relacionprograma_nombre', 'relacionprograma_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_relacionPrograma' ]) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Tipo de Vinculacion</b></h6>
            {!! Form::select('select_tipoVinculacion[]', $vinculacion->pluck('vinculacion_nombre', 'vinculacion_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_tipoVinculacion']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Ocupacion Actual</b></h6>
            {!! Form::select('select_ocupacionActual[]',$ocupacion->pluck('ocupacion_nombre', 'ocupacion_id'), null, ['class' => 'select', 'multiple-class multiple' => 'multiple', 'id' => 'select_ocupacionActual' ]) !!}
        </div>
    </div>
</div>

<div class="row separate">

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Area o Depto donde Pertenece</b></h6>
            {!! Form::select('select_areaPertenece[]', $areaPertenece->pluck('areapertenece_nombre', 'areapertenece_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_areaPertenece']) !!}
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <h6 class="labels-laboral" ><b>Tipo de Empresa</b></h6>
            {!! Form::select('select_tipoEmpresa[]', $tipoEmpresa->pluck('tipoempresa_nombre', 'tipoempresa_id'), null, ['class' => 'multiple-class select', 'multiple' => 'multiple', 'id' => 'select_tipoEmpresa']) !!}
        </div>
    </div>

</div>

</div>

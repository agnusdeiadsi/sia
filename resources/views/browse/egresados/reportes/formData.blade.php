@extends('browse.egresados.menu')

@section('content-modulo')
@php
    extract($_REQUEST);
@endphp
<div class="w3-container">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="w3-text-{{$sistema->sistema_colorclass}} w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
				w3-text-{{$sistema->sistema_colorclass}}}"><b>Exportar y listar</b></h1>
		</div>
	</div>
{!! Form::open(['route' => ['form-Wizard', $sistema], 'method' => 'get', 'id' => 'form-data']) !!}
	<div class="row">
		<div class="col-sm-4">
	    		<p>
	                <b>
	                    {!! Form::label('label_facultad', 'Facultad:') !!}
	                    <span style="color: red;">*</span>
	                </b>
	                {!! Form::select('facultad', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-facu']) !!}
	            </p>
	    </div>

		<div class="col-sm-8">
			<p>
                <b>
                    {!! Form::label('label_program', 'Programa:') !!}
                    <span style="color: red;">*</span>
                    <a class="w3-bar-item" data-toggle="tooltip" title="Para seleccionar mas de un programa, presiona ctrl y a continuacion el programa que se desea">
                        <span class="fa fa-question-circle fa-fw">
                        </span>
                    </a>
                </b>
                    
                {!! Form::select('name_program[]', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'required' => true, 'id' => 'select-program', 'multiple']) !!}
	        </p>

		</div>
	</div>

	<hr>

	<div class="row" hidden="" id="year_filters">

		<div class="row">
			<div class="col-sm-12">
				<label class="w3-text-grey">Se debe seleccionar uno de los siguientes filtros</label>
			</div>
		</div>
		<br>
		<div class="row">	
		
			<div class="col-sm-3">
				<fieldset>
					<legend>
						<center>
						<b>
		                	{!! Form::label('label_period', 'Periodo:') !!}
		            	</b>
		            	</center>
		            </legend>
				<p>
		            
		            {!! Form::select('name_period', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey fppEnable', 'id' => 'select-period']) !!}
			    </p>
			    </fieldset>
			</div>

			<div class="col-sm-3">
				<p>
		            <b>
		                {!! Form::label('label_period_since', 'Desde:') !!}
		            </b>
		            {!! Form::select('name_period_since', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'select-period-since']) !!}
			    </p>
			</div>

			<div class="col-sm-2">
				<p>
		            <b>
		                {!! Form::label('label_period_until', 'Hasta:') !!}
		            </b>
		            {!! Form::select('name_period_until', [], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey fppEnable', 'id' => 'select-period-until']) !!}
			    </p>
			</div>

			<div class="col-sm-4">
				<fieldset>
					<legend>
						<center>
						<b>
		            		{!! Form::label('label_año_graduacion', 'Tiempo de haberse graduado:') !!}
		            	</b>
		            	</center>
		            </legend>
		            	<p>
	    	            {!! Form::select('name_year_graduate', ['1' => '1 Año',
	    										'3' => '3 Años',
	    										'5' => '5 Años'], null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey fppEnable', 'id' => 'select-year-graduate', 'placeholder' => 'Seleccione']) !!}
	    				</p>
				</fieldset>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<button name="action" class="w3-btn w3-{{$sistema->sistema_colorclass}}"
						disabled
						value="fppDownload"
						id="fppDownload">
					<span class="fa fa-cloud-download fa-fw"></span>
					Descargar
				</button>
			</div>
		</div>
	</div>

	<br>
	

	<div class="row" id="menu-laboral" hidden=""> 
	<hr>
		<div class="col-sm-4" >
            <label class="w3-btn w3-{{$sistema->sistema_colorclass}} show-section" style="padding-right: 150px; 
            														  padding-left: 125px; 
            														  padding-top: 10px; 
            														  padding-bottom: 10px;" id="section-laboral">
                Laboral
            </label>
		</div>
		<div class="col-sm-4">
			<label class="w3-btn w3-{{$sistema->sistema_colorclass}} show-section" style="padding-right: 105px; 
            														  padding-left: 105px; 
            														  padding-top: 10px; 
            														  padding-bottom: 10px;" id="section-business">
                Emprendimiento
            </label>
  		</div>
  		<div class="col-sm-4">
            <label class="w3-btn w3-{{$sistema->sistema_colorclass}} show-section" style="padding-right: 150px; 
            														  padding-left: 130px; 
            														  padding-top: 10px; 
            														  padding-bottom: 10px;" id="section-achievements"> 
                Logros
            </label>
  		</div>
	</div>
<section id="secondary-filters">


	<div class="section-laboral" id="laboral-filters" hidden="">
		@include('browse.egresados.reportes.sub-filters.laboral')
	</div>

	<div class="section-business" id="laboral-filters" hidden="">
		@include('browse.egresados.reportes.sub-filters.bussines')
	</div>

	<div class="section-achievements" id="laboral-filters" hidden="">
		@include('browse.egresados.reportes.sub-filters.achievements')
	</div>

	<div id="dialog-confirm" class="w3-modal">
	    <div class="w3-modal-content w3-animate-zoom">
	      <div class="w3-container">
	        <span onclick="document.getElementById('dialog-confirm').style.display='none'" class="w3-button w3-display-topright">&times;</span>
	        <br>
	        <p>
	           <b>El numero maximo de filtros son 4, se descargar un archivo con la informacion solicitada</b>
	        </p>
	        <hr>
	        <center>
	            <p>
	                <b>
	                    ¿ACEPTA?
	                </b>
	            </p>
	            <button name="action" value="export_option" class="w3-btn w3-{{ $sistema->sistema_colorclass }}" onclick="document.getElementById('dialog-confirm').style.display='none'">
	                Si
	            </button>
	            <a class="w3-btn" onclick="document.getElementById('dialog-confirm').style.display='none'" type="reset" style="background-color: #DCAC34; color: white;">
	                NO
	            </a>
	        </center>
	        <hr>
	      </div>
	    </div>
    </div> 

</section>

<section id="show-filters" hidden="">

	<hr>
	<div class="row">
		<div class="col-sm-6" id="div-situacion" style="display: none;">
			<h5 class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
				w3-text-{{$sistema->sistema_colorclass}}}"><b>Laboral:</b></h5>
			<div id="content-filters-laboral">
				No selecciono
			</div>			
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6" id="div-emprendimiento" style="display: none;">
			<h5 class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
				w3-text-{{$sistema->sistema_colorclass}}}"><b>Emprendimiento:</b></h5>
			<div id="content-filters-emprendimiento">
				No selecciono
			</div>			
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6" id="div-logros" style="display: none;">
			<h5 class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
				w3-text-{{$sistema->sistema_colorclass}}}"><b>Logros:</b></h5>
			<div id="content-filters-logros">
				No selecciono
			</div>			
		</div>
	</div>

	<div class="w3-right">
		<button name="action" class="w3-btn w3-{{ $sistema->sistema_colorclass }}" value="list" id="list_select">
			<span class="fa fa-list fa-fw"></span>
    		Listar
	    </button>
	    <button name="action" class="w3-btn w3-{{ $sistema->sistema_colorclass }}" value="export_email">
	    	<span class="fa fa-cloud-download fa-fw"></span>
	    	Exportar
	    </button>
	</div>
	
</section>
{!! Form::close() !!}

<div class="w3-right" style="margin-top: 50px;" id="buttom-finish" hidden="">

	<button class="w3-btn w3-{{$sistema->sistema_colorclass}}" id="reset">
		<span class="fa fa-close fa-fw"></span>
		Limpiar
	</button>

	<button class="w3-btn w3-{{$sistema->sistema_colorclass}}" type="submit" id="finish">
		<span class="fa fa-check fa-fw"></span>
		Terminar
	</button>
</div>

</div>

@endsection
@extends('browse.egresados.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Extensiones
@endsection

@section('content-modulo')
  @php
    extract($_REQUEST);
  @endphp

  <h2>Reportes<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Listado de Extensiones. Si no aparece en la lista, por favor actualice su informacion en el servicio: Correos Institucionales > Mi cuenta"><span class="fa fa-question-circle fa-fw"></span></a></h2>

  {{ Form::open(['route' => ['browse.egresados.reportes.index', $sistema], 'method' => 'get']) }}
    <div class="w3-row-padding">
      @if(empty($programa))
        <div class="w3-col s3">
          {{ Form::label('programa', 'Programa*') }}
          {{ Form::select('programa', $subareas->pluck('subarea_nombre', 'subarea_id'), null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'onchange' => '', 'required']) }}
        </div>
        <div class="w3-col s3">
          {{ Form::label('periodo', 'Periodo*') }}
          {{ Form::select('periodo', $periodos->pluck('periodo_id', 'periodo_id'), null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
        </div>
        <div class="w3-col s1">
          <button type="submit" class="w3-btn w3-white w3-border"><span class="fa fa-filter fa-fw"></span></button>
        </div>
      @else
        <div class="w3-col s3">
          {{ Form::label('programa', 'Programa*') }}
          {{ Form::select('programa', $subareas->pluck('subarea_nombre', 'subarea_id'), $programa, ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'onchange' => '', 'required']) }}
        </div>
        <div class="w3-col s3">
          {{ Form::label('periodo', 'Periodo*') }}
          {{ Form::select('periodo', $periodos->pluck('periodo_id', 'periodo_id'), null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'required']) }}
        </div>

        <div class="w3-col s3">
          {{ Form::label('ocupacion', 'Ocupación*') }}
          {{ Form::select('ocupacion', ['0' => 'Empleado', '1' => 'Desempleado', '2' => 'Empresario'], null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar']) }}
        </div>
        <div class="w3-col s1">
          <button type="submit" class="w3-btn w3-white w3-border"><span class="fa fa-filter fa-fw"></span></button>
        </div>
      @endif
    </div>
  {{ Form::close() }}
  <hr>

  <div class="w3-responsive">
    @if(!isset($programa) || isset($programa) && !isset($periodo))
      No hay filtros
    @elseif($programa != null && $periodo != null && !isset($ocupacion))
      <span class="fa fa-pie-chart fa-5x"></span>
    @elseif($programa != null && $periodo != null && isset($ocupacion))
      <table class="w3-table-all">
          <thead>
              <tr class="w3-{{$sistema->sistema_colorclass}}">
                <th>Código</th>
                <th>Nombre</th>
                <th></th>
              </tr>
          </thead>
          <tbody>
            @foreach($periodos as $egresado)
              @if($egresado->matriculado->laboral->count() > 0)
                @if($egresado->matriculado->laboral->first()->laboral_situacion == $ocupacion)
                  <tr>
                    <td>{{$egresado->academico_codigo}}</td>
                    <td>{{$egresado->matriculado->matriculado_nombres}}</td>
                    <td>
                      <a href=""><span class="fa fa-eye fa-fw"></span></a>
                    </td>
                  </tr>
                @endif
              @endif
            @endforeach
          </tbody>
      </table>
    @else
      No hay filtros
    @endif
  </div>
@endsection
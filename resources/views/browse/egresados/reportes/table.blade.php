@extends('browse.egresados.menu')

@section('content-modulo')
@php
    //dd($select_situacion_laboral);
    extract($_REQUEST);

@endphp    
<div class="w3-container"><br>
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="{{ route('browse.egresados.reportes.formData', $sistema) }}">Filtros</a></li>
        <li class="active"><span>Lista</span></li>
    </ol>
</div>       

<div class="row">
    <div class="col-sm-12">
        <div class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
            w3-text-{{$sistema->sistema_colorclass}}}">
            <h2><b>Reportes</b></h2>
        </div>
    </div>
</div>  
    @php 
        $var = 0;
    @endphp
<table class="w3-table-all">
                    <tr class="w3-{{ $sistema->sistema_colorclass }}">
                        <th>
                            Nombres
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Telefono
                        </th>
                        <th>
                            Direccion
                        </th>
                        <th>
                            Periodo
                        </th>
                       <th>
                            Programa
                        </th>
                        <th>
                            Fecha de grado
                        </th>
                        <th>
                            Acciones
                        </th>
                    </tr>
    {{--dd($posts)--}}  
    @if(isset($datos_list))
        @foreach($datos_list as $data)
            @php
                $var = 1;
            @endphp
            {{--@if($datos_list->count() > 1)--}}
                    <tr>    
                        <td>
                            {{$data->matriculado_primer_nombre}} {{$data->matriculado_segundo_nombre}} {{$data->matriculado_primer_apellido}} {{$data->matriculado_segundo_apellido}}
                        </td>
                        <td>
                            {{$data->matriculado_email}}
                        </td>
                        <td>
                            {{$data->matriculado_telefono}}
                        </td>
                        <td>
                            {{$data->matriculado_direccion}}
                        </td>
                        <td>
                            {{$data->grado_periodo}}
                        </td>
                        <th>
                            {{$data->programa_nombre}}
                        </th>
                        <th>
                            {{$data->grado_fecha}}
                        </th>
                        <td>
                            <a href="{{route('browse.egresados.formulario.edit-table', [$sistema, 'egresado' => $data->identificacion_numero])}}" class="w3-btn fa fa-pencil"></a>
                        </td>
                    </tr>
            @endforeach 
            {{--@endif--}}
            </table>
        @endif

<div class="row">
    <div class="col-sm-12">
        <center>
            {{ $datos_list->appends(array('name_program' => $program, 
                                          'name_period' => $period,
                                          'name_period_since' => $period_since,
                                          'name_period_until' => $period_until,
                                          'name_year_graduate' => $period_year,
                                          'select_situacion_laboral' => $select_situacion_laboral,
                                          'select_duracion' => $select_duracion,
                                          'select_cargo' => $select_cargo,
                                          'select_contrato' => $select_contrato,
                                          'select_sectorEmpresa' => $select_sectorEmpresa,
                                          'select_rangoSalarial' => $select_rangoSalarial,
                                          'select_relacionPrograma' => $select_relacionPrograma,
                                          'select_tipoVinculacion' => $select_tipoVinculacion,
                                          'select_ocupacionActual' => $select_ocupacionActual,
                                          'select_areaPertenece' => $select_areaPertenece,
                                          'select_tipoEmpresa' => $select_tipoEmpresa,
                                          'select_sectorEmpresa_Bu' => $select_sectorEmpresa_Bu,
                                          'select_Ciudad_Bu' => $select_Ciudad_Bu,
                                          'select_tamaño_empresa' => $select_tamaño_empresa,
                                          'select_merito' => $select_merito))->links() }}
        </center>
    </div>
</div>
{{--dd($datos_list)--}}
    @if($var == 0)
            <tr>
                <td colspan="6">
                    <h4 ><i><b>No Existen datos</b></i></h4>
                </td>    
            </tr>
            </table>
    @endif
    <br>
    <div class="row">
        <div class="col-sm-12">
            <h5 class="w3-panel w3-pale-{{$sistema->sistema_colorclass}} w3-leftbar w3-border-{{$sistema->sistema_colorclass}} 
            w3-text-{{$sistema->sistema_colorclass}}}"><b>Los filtros seleccionados son:</b></h5>
            @foreach($array as $data)
                {{ $data."," }}
            @endforeach
        </div>
    </div>

@endsection
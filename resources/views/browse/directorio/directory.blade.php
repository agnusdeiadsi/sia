@extends('browse.directorio.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Extensiones
@endsection

@section('content-modulo')
  <h2>Extensiones<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Listado de Extensiones. Si no aparece en la lista, por favor actualice su informacion en el servicio: Correos Institucionales > Mi cuenta"><span class="fa fa-question-circle fa-fw"></span></a></h2>

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    <div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['browse.directorio.extensiones.directory', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['browse.directorio.extensiones.directory', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>

  <div class="w3-responsive">
    <table class="w3-table-all w3-hoverable">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>Extensión</th>
          <th>Nombres</th>
          <th>Email</th>
          <th>Área</th>
          <th>Subarea</th>
          <th>Cargo</th>
          <th>Sede</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @if($extensiones->count() == 0)
          <tr>
            <td colspan="8"><i>No hay registros</i></td>
          </tr>
        @else
          @php
            $myaccount = \App\Model\Correos\Cuenta::find(\Auth::user()->id);
            $myaccount_extension = $myaccount->extension;
          @endphp
          @foreach($extensiones as $extension)
            <tr>
              <td>
                {{$extension->extension_extension}}
              </td>
              <td>
                {{$extension->cuenta_primer_nombre.' '.$extension->cuenta_segundo_nombre.' '.$extension->cuenta_primer_apellido.' '.$extension->cuenta_segundo_apellido}}
              </td>
              <td>
                {{$extension->cuenta_cuenta}}
              </td>
              <td>
                {{$extension->area_nombre}}
              </td>
              <td>
                {{$extension->subarea_nombre}}
              </td>
              <td>
                {{$extension->cargo_nombre}}
              </td>
              <td>
                {{$extension->sede_nombre}}
              </td>
              <td>
                @if(Auth::user()->cuenta->extension_id != null)
                  <a href="http://10.0.0.3/ippbx/index.php?r=peers/dircall&destino={{$extension->extension_extension}}&origen={{$myaccount->extension->extension_extension}}" class="w3-btn w3-white w3-border w3-hover-{{$sistema->sistema_colorclass}}" title="Llamar" target="open_call" onclick="document.getElementById('modalCall').style.display='block'"><span class="fa fa-phone fa-fw"></span></a>
                @endif
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>

  <iframe name="open_call" id="open_call" hidden></iframe>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>
  <div class="w3-container">
    <div id="modalCall" class="w3-modal">
      <div class="w3-modal-content">
        <header class="w3-container w3-{{ $sistema->sistema_colorclass }}"> 
          <span onclick="document.getElementById('modalCall').style.display='none'" 
          class="w3-button w3-display-topright">&times;</span>
          <h3><i class="fa fa-phone fa-fw" aria-hidden="true"></i>Llamando</h3>
        </header>
        <div class="w3-container w3-center w3-padding-16">
          <p>Recuerda que debes estar en el lugar de la extensión que has registrado con tu cuenta SIA. Levanta el teléfono para que salga la llamada.</p>
          <i class="fa fa-spinner w3-spin fa-3x" aria-hidden="true"></i>
          <br>
        </div>
        {{--<footer class="w3-container w3-teal">
          <p>Modal Footer</p>
        </footer>--}}
      </div>
    </div>
  </div>
@endsection

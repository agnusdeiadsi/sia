@extends('browse.directorio.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Extensiones
@endsection

@section('content-modulo')
  <h2>Extensiones<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Listado de Extensiones. Si no aparece en la lista, por favor actualice su informacion en el servicio: Correos Institucionales > Mi cuenta"><span class="fa fa-question-circle fa-fw"></span></a></h2>

  <div class="w3-row">
    <!--Buscador para pantallas pequeñas -->
    <div class="w3-col s12 w3-hide-large">
      {!! Form::open(['route' => ['browse.directorio.extensiones.index', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s12 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>

    <!--Buscador para pantallas grandes -->
    <div class="w3-col 12 w3-hide-small">
      {!! Form::open(['route' => ['browse.directorio.extensiones.index', $sistema], 'method' => 'get']) !!}
        <div class="input-group w3-col s4 w3-right">
          {!! Form::text('busqueda', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar', 'aria-describedby' => 'search',]) !!}
          <span class="input-group-addon" id="search"><i class="fa fa-search"></i></span>
          <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto bottom" title="Puede filtrar por número de extension, nombres, apellidos, email, área o subarea."><span class="fa fa-question-circle fa-fw"></span></a>
        </div>
      {!! Form::close() !!}
    </div>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>

  <div class="w3-responsive">
    <table class="w3-table-all w3-hoverable">
      <thead>
        <tr class="w3-{{$sistema->sistema_colorclass}}">
          <th>ID</th>
          <th>Extensión</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @if($extensiones->count() == 0)
          <tr>
            <td colspan="7"><i>No hay registros</i></td>
          </tr>
        @else
          @foreach($extensiones as $extension)
            <tr>
              <td>
                {{$extension->extension_id}}
              </td>
              <td>
                {{$extension->extension_extension}}
              </td>
              <td>
                <a href="{{ route('browse.directorio.extensiones.edit', [$sistema, $extension]) }}" class="w3-btn w3-white w3-border"><span class="fa fa-pencil fa-fw"></span></a>
              </td>
            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
  
  <div class="w3-center">
    <div class="w3-bar">
      {!! $extensiones->render() !!}
    </div>
  </div>
@endsection
@extends('browse.layouts.modulo')

@section('title-modulo')
  {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
	<div class="w3-bar-block">
		<a href="{{route('browse.directorio.extensiones.directory', [$sistema])}}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-address-book fa-fw"></i> Directorio</a>
		@if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
			<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey" onclick="accordion('accordion-info')"><i class="fa fa-cogs fa-fw"></i>  Administración <span class="fa fa-caret-down"></span></a>
			<div id="accordion-info" class="w3-hide w3-light-grey">
				<a href="{{route('browse.directorio.extensiones.create', [$sistema])}}" class="w3-bar-item w3-button w3-padding w3-hover-{{$sistema->sistema_colorclass}}">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Crear</a>
				<a href="{{route('browse.directorio.extensiones.index', [$sistema])}}" class="w3-bar-item w3-button w3-padding w3-hover-{{$sistema->sistema_colorclass}}">&emsp;&emsp;<i class="fa fa-caret-right fa-fw"></i> Extensiones</a>
			</div>
		@endif
	</div>
@stop

@section('content-modulo')
	<center><h1><b>{{$sistema->sistema_nombre}}</b></h1></center>
	<hr>
	<div class="w3-container">
		<center><h3><b>Aquí podrás:</b></h3></center>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-4">
				<center><img src="{{ asset('images/menu-icon/directorio/icon-01.png') }}" class="w3-image"></center>
			</div>
			<div class="col-sm-2">
				<center><img src="{{ asset('images/menu-icon/directorio/icon-02.png') }}" class="w3-image"></center>
			</div>
		</div>
	</div>
@endsection

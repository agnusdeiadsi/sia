@extends('browse.directorio.menu')

@section('title-modulo')
  {{$sistema->sistema_nombre}} | Crear
@endsection

@section('content-modulo')
	<h2>Crear Extensión</h2>
	{!! Form::open(['route' => ['browse.directorio.extensiones.store', $sistema], 'method' => 'post']) !!}
		<p>
			<label>Extensión</label>
			{{Form::text('extension_extension', null, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Extensión', 'maxlength' => '10', 'required'])}}
		</p>
		<div class="w3-row">
			<div class="w3-col s12 m3 l3">
				<button type="submit" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
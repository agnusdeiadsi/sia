<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- w3-css -->
    <link rel="stylesheet" href="{{ asset('w3css/w3-v4.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-camo.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-food.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-highway.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-safety.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-signal.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-vivid.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-2017.css') }}">
    <link rel="stylesheet" href="{{ asset('w3css/colors/w3-colors-windows.css') }}">

    <link rel="stylesheet" href="{{ asset('w3css/w3-theme-snorkel-blue.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'SIA') | Panel de Administración</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('breadcrumbs/breadcrumbs.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style>
    body, h1,h2,h3,h4,h5,h6 {
      font-family: "Montserrat", sans-serif;
      color: white;
    }

    @media (min-width: 1400px){
      body{
        background-image: url('{{ asset('images/1680x1050.png') }}');
        background-size: cover;
      }
    }

    @media (max-width: 1399px){
      body{
      background-image: url('{{ asset('images/768x1024.png') }}');
      background-size: cover;
      }
    }

    a.w3-hide-decoration{
      text-decoration: none;
    }

    .w3-row-padding img {margin-bottom: 12px}
    /* Set the width of the sidebar to 120px */
    .w3-sidebar {width: 120px;}
    /* Add a left margin to the "page content" that matches the width of the sidebar (120px) */
    #main {margin-left: 120px;}
    /* Remove margins from "page content" on small screens */
    @media only screen and (max-width: 600px) {#main {margin-left: 0; padding: 15px;}}
    </style>
</head>
<body class="w3-light-grey">
      <!-- Icon Bar (Sidebar - hidden on small screens) -->
      <nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center w3-2017-navy-peony">
      <!-- Avatar image in top left corner -->
      <a href="{{url('/site/home')}}">
        <br /><br />
        <img src="{{ asset('images/sia-small.png') }}" class="w3-padding" style="width:100%;" alt="logo">
        <br /><br /><hr />
      </a>
      <a href="{{ route('site.usuarios.index') }}" class="w3-bar-item w3-button w3-padding-large w3-hover-grey w3-hide-decoration">
        <i class="fa fa-users w3-xxlarge"></i>
        <p>USUARIOS</p>
      </a>
      <a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-button w3-padding-large w3-hover-grey w3-hide-decoration">
        <i class="fa fa-refresh w3-xxlarge"></i>
        <p>ROLES</p>
      </a>
      <a href="{{ route('sistemas.index') }}" class="w3-bar-item w3-button w3-padding-large w3-hover-grey w3-hide-decoration">
        <i class="fa fa-connectdevelop w3-xxlarge"></i>
        <p>SISTEMAS</p>
      </a>
      <a href="{{ route('site.correos.index') }}" class="w3-bar-item w3-button w3-padding-large w3-hover-grey w3-hide-decoration">
        <i class="fa fa-at w3-xxlarge"></i>
        <p>CORREOS</p>
      </a>
      <a href="{{url('/site/logout')}}" class="w3-bar-item w3-button w3-padding-large w3-hover-grey w3-hide-decoration" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out w3-xxlarge"></i>
        <p>SALIR</p>
        <form id="logout-form" action="{{ url('/site/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </a>
      </nav>

      <!-- Navbar on small screens (Hidden on medium and large screens) -->
      <div class="w3-top w3-hide-large w3-hide-medium" id="myNavbar">
        <div class="w3-bar w3-black w3-opacity w3-hover-opacity-off w3-center w3-small">
          <a href="{{ url('site/home') }}" class="w3-bar-item w3-button" style="width:25% !important">INICIO</a>
          <a href="{{ route('site.correos.index') }}" class="w3-bar-item w3-button" style="width:25% !important">USUARIOS</a>
          <a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-button" style="width:25% !important">ROLES</a>
          <a href="{{ route('sistemas.index') }}" class="w3-bar-item w3-button" style="width:25% !important">SISTEMAS</a>
          <a href="{{ route('site.correos.index') }}" class="w3-bar-item w3-button" style="width:25% !important">CORREOS</a>
          <a href="{{url('/site/logout')}}" class="w3-bar-item w3-button" style="width:25% !important" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">SALIR
            <form id="logout-form" action="{{ url('/site/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </a>
        </div>
      </div>

      <!-- Page Content -->
      <div class="w3-padding-large w3-text-white" id="main">
        <div class="w3-content w3-justify w3-text-grey w3-padding-64" id="about">
          @include('flash::message')
          @if(count($errors) > 0)
          <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
            <p>
            @foreach($errors->all() as $error)
              <li>
                {{ $error }}
              </li>
            @endforeach
            </p>
          </div>
          @endif

          @yield('content')
        </div>
      <!-- END PAGE CONTENT -->
      </div>

      <!-- Scripts -->
      <script src="/js/app.js"></script>
      <script src="/ajax/correos/accounts.js"></script>
</body>
</html>

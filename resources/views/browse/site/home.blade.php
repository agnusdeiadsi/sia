@extends('browse.layouts.app')

@section('title')
	Administración del Sitio
@endsection

@section('content')
  <!-- breadcrumbs -->
  <div class="w3-container"><br>
      <ol class="breadcrumb breadcrumb-arrow">
      <li><a href="{{ url('browse/home') }}">Inicio</a></li>
      <li class="active"><span>Administración del Sitio</span></li>
    </ol>
  </div>
  <!-- fin breadcrumbs -->

  <div class="w3-container w3-center" id="home">
    <h2><span class="fa fa-cogs fa-fw"></span> Administración del Sitio</h2>
    <hr>
    <div class="w3-row-padding">
    	<div class="w3-col s12 m4 l4"><a href="{{route('browse.site.sistemas.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-server fa-fw fa-2x"></span><br>Sistemas</a>
      </div>
      
    	<div class="w3-col s12 m4 l4"><a href="{{route('browse.site.usuarios.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-users fa-fw fa-2x"></span><br>Usuarios</a>
      </div>

    	<div class="w3-col s12 m4 l4"><a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-plug fa-fw fa-2x"></span><br>Roles</a>
      </div>
    </div>
    <br>
    <br>
    <div class="w3-row-padding">
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.contratos.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-file-o fa-fw fa-2x"></span><br>Contratos</a>
      </div>
      
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.sedes.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-building fa-fw fa-2x"></span><br>Sedes</a>
      </div>

      <div class="w3-col s12 m4 l4"><a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-graduation-cap fa-fw fa-2x"></span><br>Facultad y Programa</a>
      </div>
    </div>
    <br>
    <br>
    <div class="w3-row-padding">
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.sistemas.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-paint-brush fa-fw fa-2x"></span><br>Colores</a>
      </div>
      
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.usuarios.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-sitemap fa-fw fa-2x"></span><br>Nivel Cargos</a>
      </div>

      <div class="w3-col s12 m4 l4"><a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-steam fa-fw fa-2x"></span><br>Cargos Unicatólica</a>
      </div>
    </div>
    <br>
    <br>
    <div class="w3-row-padding">      
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.sistemas.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-map-marker fa-fw fa-2x"></span><br>Centros de Operación</a>
      </div>

      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.usuarios.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-money fa-fw fa-2x"></span><br>Centros de Costos</a>
      </div>

      <div class="w3-col s12 m4 l4"><a href="{{ route('browse.site.roles.index') }}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-trophy fa-fw fa-2x"></span><br>Proyectos</a>
      </div>
    </div>
    <br>
    <br>
    <div class="w3-row-padding">
      <div class="w3-col s12 m4 l4"><a href="{{route('browse.site.sistemas.index')}}" class="w3-bar-item w3-xxlarge w3-text-2017-navy-peony">
        <span class="fa fa-briefcase fa-fw fa-2x"></span><br>Áreas y Subáreas</a>
      </div>
    </div>
    <br>
    <br>
  </div>
@endsection

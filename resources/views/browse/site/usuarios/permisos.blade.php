@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Usuarios | Permisos
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	  <ol class="breadcrumb breadcrumb-arrow">
	  	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.usuarios.index') }}">Usuarios</a></li>
			<li class="active"><span>Permisos</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<h2><span class="fa fa-users fa-fw"></span> Permisos Módulos</h2>
		<hr style="width:400px" class="w3-opacity"/>

		<div class="w3-responsive">
			<table class="w3-table-all">
				<thead>
					<tr class="w3-2017-navy-peony">
						<th>Nombres</th>
						<th>Usuario</th>
	          			<th>Rol SIA</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							{{ $usuario->cuenta->cuenta_primer_apellido." ".$usuario->cuenta->cuenta_segundo_apellido." ".$usuario->cuenta->cuenta_primer_nombre." ".$usuario->cuenta->cuenta_segundo_nombre }}
						</td>
						<td>
							{{ $usuario->cuenta->cuenta_cuenta }}
						</td>
						<td>
							@if($usuario->rol_sia == 'Administrador')
							  <span class="w3-cyan w3-padding">Administrador</span>
							@elseif($usuario->rol_sia == 'Auditor')
							  <span class="w3-teal w3-padding">Auditor</span>
							@elseif($usuario->rol_sia == 'Académico')
							  <span class="w3-orange w3-padding">Académico</span>
							@elseif($usuario->rol_sia == 'Administrativo')
							  <span class="w3-red w3-padding">Administrativo</span>
							@endif
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	    <hr />
	    <div class="w3-responsive">
			<table class="w3-table-all">
				<thead>
					<tr class="w3-2017-navy-peony">
						<th>Módulo</th>
	          			<th>Versión</th>
						<th>Roles</th>
					</tr>
				</thead>
				<tbody>
				{!! Form::open(['route' => ['browse.site.usuarios.updatepermisos', $usuario], 'method' => 'PUT']) !!}
					@php
						foreach ($sistemas_roles as $sistemas) {
							echo "<tr>";
							echo "<td>".$sistemas['sistema']['sistema_nombre']."</td>";
	            			echo "<td>".$sistemas['sistema']['sistema_version']."</td>";
							echo "<td>";
									//$permisos = $usuario['permisos']->ToArray();
									$permisos = $usuario['enrolamientos']->ToArray();

									$var = 0; //no coincide
									foreach ($permisos as $permisos) {
										if($sistemas['sistema']['sistema_id'] == $permisos['sistema_id']) {
											//echo "El sistema coincide con el usuario.<br>";

											echo "<select name='sistemarol_id[]' class='w3-input' required>";
											echo "<option value=''>Seleccionar</option>";

											foreach($sistemas['rol'] as $roles) {
												if($roles['rol_id'] == $permisos['rol_id']) {
													echo "<option value='$permisos[sistemarol_id]' selected>$roles[rol_nombre]</option>";
												}
												else{
													$pivote = $roles['pivot'];
													$campo = $pivote['sistemarol_id'];

													echo "<option value='$campo'>$roles[rol_nombre]</option>";
												}
											}
											echo "</select>";

											$var = 1;
										}
									}

									if($var==0){
										echo "<select name='sistemarol_id[]' class='w3-input' required>";
										echo "<option value=''>Seleccionar</option>";

										foreach($sistemas['rol'] as $roles) {
											$pivote = $roles['pivot'];
											$campo = $pivote['sistemarol_id'];

											echo "<option value='$campo'>$roles[rol_nombre]</option>";
										}
										echo "</select>";
									}
							echo "</td>
							</tr>";
						}
					@endphp		
				</tbody>
			</table>
		</div>
		<br>
		<div class="row w3-right">
			<div class="col-sm-12">
				<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
				<a href="{{route('browse.site.usuarios.index')}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
 <br><br>
@endsection

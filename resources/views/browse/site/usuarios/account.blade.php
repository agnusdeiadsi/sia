@extends('browse.layouts.app')

@section('title')
	Mi Perfil
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
	    	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li class="active"><span>Mi perfil</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

    <h2><span class="fa fa-user fa-fw"></span> Mi Perfil<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="La información aquí registrada será usada en su cuenta de correo institucional y en la plataforma SIA."><span class="fa fa-question-circle fa-fw"></span></a></h2>
    <hr />
    <div class="w3-row w3-bar w3-white">
		<a href="javascript:void(0)" class="w3-text-grey" id="info-btn" onclick="openTab(event, 'info');">
			<div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><b>Información Personal</b></div>
		</a>

	    <a href="javascript:void(0)" class="w3-text-grey" onclick="openTab(event, 'ps');">
	    	<div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><b>Contraseña</b></div>
	    </a>
    </div>

    <!--Tabs-->
    <div id="info" class="w3-container tab" style="display:none">
    	<p>
			<div class="w3-row-padding">
				{!! Form::open(['route' => ['browse.site.usuarios.perfil.update'], 'method' => 'PUT', 'files' => true]) !!}
				<div class="row">
					<div class="col-sm-5 w3-padding">
						<div class="row">
							<div class="col-sm-12">
								<div class="w3-panel w3-leftbar w3-border-2017-navy-peony">
									<h4>Información Personal</h4>
								</div>
							</div>
						</div>
						<p>
							<label>Tipo Identificación*</label>
							{!! Form::select('cuenta_tipo_identificacion', ['CC' => 'Cédula Ciudadanía', 'CE' => 'Cédula Extranjería', 'PS' => 'Pasaporte', 'TI' => 'Tarjeta Identidad'], $cuenta->cuenta_tipo_identificacion, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sexo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Identificación*</label>
							{!! Form::text('cuenta_identificacion', $cuenta->cuenta_identificacion, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Primer Nombre*</label>
							{!! Form::text('cuenta_primer_nombre', $cuenta->cuenta_primer_nombre, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_nombre"); return false', 'onkeyup' => 'strToUpper("primer_nombre"); return false', 'placeholder' => 'Primer Nombre', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Segundo Nombre</label>
							{!! Form::text('cuenta_segundo_nombre', $cuenta->cuenta_segundo_nombre, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_nombre"); return false', 'onkeyup' => 'strToUpper("segundo_nombre"); return false', 'placeholder' => 'Segundo Nombre']) !!}
						</p>
						<p>
							<label>Primer Apellido*</label>
							{!! Form::text('cuenta_primer_apellido', $cuenta->cuenta_primer_apellido, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_apellido"); return false', 'onkeyup' => 'strToUpper("primer_apellido"); return false', 'placeholder' => 'Primer Apellido', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Segundo Apellido</label>
							{!! Form::text('cuenta_segundo_apellido', $cuenta->cuenta_segundo_apellido, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_apellido"); return false', 'onkeyup' => 'strToUpper("segundo_apellido"); return false', 'placeholder' => 'Segundo Apellido']) !!}
						</p>
						<p>
							<label>Correo Personal*</label>
							{!! Form::text('cuenta_email', $cuenta->cuenta_email, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'correo_personal', 'maxlength' => '100', 'placeholder' => 'Correo Personal', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Género*</label>
							{!! Form::select('cuenta_genero', ['M' => 'Masculino', 'F' => 'Femenino'], $cuenta->cuenta_genero, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar', 'required']) !!}
						</p>
						<p>
                			<label>Fecha Nacimiento*</label>
                			{!! Form::date('cuenta_fecha_nacimiento', $cuenta->cuenta_fecha_nacimiento, ['class' => 'w3-input w3-border-2017-navy-peony', 'id' => 'fecha_nacimiento', 'placeholder' => 'Seleccionar', 'required']) !!}
                    	</p>
					</div>
					<div class="col-sm-offset-1 col-sm-6 w3-padding">
						<div class="row">
							<div class="col-sm-12">
								<div class="w3-panel w3-leftbar w3-border-2017-navy-peony">
									<h4>Información Contractual</h4>
								</div>
							</div>
						</div>
						<p>
							<label>Tipo Contrato</label>
							{!! Form::select('contrato_id', $contratos, $cuenta->contrato_id, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'contrato', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Cargo*</label>
							{!! Form::select('cargo_id', $cargos->pluck('cargo_nombre', 'cargo_id'), $cuenta->cargo_id, ['class' => 'w3-input w3-border-2017-navy-peony', 'id' => 'cargo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
						</p>
						<p>
							<label>Área/Programa*</label>
							{!! Form::select('subarea_id', $subareas, $cuenta->subarea_id, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'subarea', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
						</p>
						<p>
		                	<label>Sede*</label>
		                	{!! Form::select('sede_id', $sedes->pluck('sede_nombre', 'sede_id'), $cuenta->sede_id, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sede', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
		                </p>
						<div class="row">
							<div class="col-sm-12">
								<div class="w3-panel w3-leftbar w3-border-2017-navy-peony">
									<h4>Cuenta/Directorio</h4>
								</div>
							</div>
						</div>

						<p>
							<label>Cuenta</label>
							{!! Form::text('cuenta', $cuenta->cuenta_cuenta, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'tipo', 'placeholder' => 'Cuenta', 'disabled' => 'true']) !!}
						</p>

						<p>
							<label>Extensión</label>
							{!! Form::select('extension_id', $extensiones, $cuenta->extension_id, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar']) !!}
						</p>

						<p><br />
							<div class="w3-row">
								<div class="w3-col s5">
									Acepto <a href="{{asset('files/correos/tratamiento-de-datos-unicatolica.pdf')}}" target="_blank">Terminos y Condiciones</a>
								</div>
								<div class="w3-col s1">
									{!! Form::checkbox('cuenta_condiciones', 'SÍ', $cuenta->cuenta_condiciones, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cuenta_condiciones', 'required' => 'true']) !!}
								</div>
							</div>
						</p>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-12 text-right">
						<button type="submit" id="guardar_formulario" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
						<a href="{{url('browse/home')}}" class="w3-btn w3-red"><span class="fa fa-remove"></span> Cancelar</a>
					</div>
				</div>
			{!! Form::close() !!}
			</div>
		</p>
	</div>

    <div id="ps" class="w3-container tab" style="display:none">
    	<p>
    		<div class="row">
				<div class="col-sm-12">
					<div class="w3-panel w3-leftbar w3-border-2017-navy-peony">
						<h4>Cambiar Contraseña</h4>
					</div>
				</div>
			</div>
    		{!! Form::open(['route' => ['browse.site.usuarios.perfil.updatepassword'], 'method' => 'put']) !!}
				<div class="row">
					<div class="col-sm-6">
						{{ Form::label('oldpassword', 'Contraseña Actual') }}
						{!! Form::password('oldpassword', ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Contraseña Actual', 'maxlength' => '30', 'required']) !!}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-6">
						{{ Form::label('password', 'Contraseña Nueva') }}
						{!! Form::password('password', ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Contraseña Nueva', 'maxlength' => '30', 'required']) !!}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-6">
						{{ Form::label('confirmpassword', 'Confirmar Contraseña') }}
						{!! Form::password('confirmpassword', ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Confirmar Contraseña', 'maxlength' => '30', 'required']) !!}
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						<button type="submit" class="w3-bar-item w3-btn w3-2017-navy-peony"><span class="fa fa-refresh fa-fw"></span> Cambiar Contraseña</button>
						<button type="reset" class="w3-bar-item w3-btn w3-2017-navy-peony"><span class="fa fa-paint-brush fa-fw"></span> Limpiar</button>
						<a href="{{url('browse/home')}}" class="w3-bar-item w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
					</div>
				</div>
			{!! Form::close() !!}
    	</p>
    </div>
    <hr>
</div>
    <script type="text/javascript">
	  	function crearUsuario(usuario)
	  	{
	  		var usuario = usuario.split("@", 1);
	  		document.getElementById('cuenta_usuario').value = usuario;
	  	}

	  	function strToUpper(idInput)
	  	{
	  		input = document.getElementById(idInput).value;
	  		strupper = input.toUpperCase();
	  		document.getElementById(idInput).value=strupper;
	  	}

	  	/*Tabs*/
	  	document.getElementById('info-btn').click();

	  	function openTab(evt, cityName) {
		    var i, x, tablinks;
		    x = document.getElementsByClassName("tab");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablink");
		    for (i = 0; i < x.length; i++) {
		       tablinks[i].className = tablinks[i].className.replace(" w3-border-2017-navy-peony", "");
		    }
		    document.getElementById(cityName).style.display = "block";
		    evt.currentTarget.firstElementChild.className += " w3-border-2017-navy-peony";
		}
  	</script>
@endsection

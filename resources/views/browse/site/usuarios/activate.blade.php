@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Usuarios | Activar
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
	    	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.usuarios.index') }}">Usuarios</a></li>
			<li class="active"><span>Activar</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->
	<div class="w3-content">
		<h2><span class="fa fa-power-off fa-fw"></span> Activar usuario en SIA</h2>
		<hr>

		{!! Form::open(['route' => 'browse.site.usuarios.storeactivation', 'method' => 'POST']) !!}
			<p>
			{!! Form::hidden('id', $cuenta->cuenta_id, ['class' => 'w3-input w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('cuenta_cuenta', 'Cuenta') !!}</b>
			{!! Form::text('email', $cuenta->cuenta_cuenta, ['class' => 'w3-input w3-text-grey', 'placeholder' => 'Seleccionar', 'readonly' => 'true', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('rol_sia', 'Rol') !!}</b>
			{!! Form::select('rol_sia', ['Administrador' => 'Administrador', 'Auditor' => 'Auditor', 'Académico' => 'Académico', 'Administrativo' => 'Administrativo'], null, ['class' => 'w3-input w3-text-grey', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
			</p>

			<div class="row w3-right">
				<div class="col-sm-12">
					<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-power-off fa-fw"></span> Activar</button>
					<a href="{{route('browse.site.usuarios.index')}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<br>
@endsection

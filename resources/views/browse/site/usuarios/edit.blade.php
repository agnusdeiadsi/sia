@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Usuarios | Editar
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
	    	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{url('browse/site/home')}}">Administración del Sitio</a></li>
			<li><a href="{{route('browse.site.usuarios.index')}}">Usuarios</a></li>
			<li class="active"><span>Editar</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->
	<div class="w3-content">
		<h2>Editar<a class="w3-bar-item" data-toggle="tooltip" title="Permite cambiar el rol del usuario en SIA." data-placement="auto"><span class="fa fa-question-circle fa-fw"></span></a></h2>
		<hr>

		{!! Form::open(['route' => ['browse.site.usuarios.update', $usuario], 'method' => 'PUT']) !!}
			<p>
			<!--{!! Form::label('cuenta', 'cuenta') !!}-->
			{!! Form::hidden('cuenta_id', $usuario->id, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'ID', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('nombres', 'Nombres') !!}</b>
			{!! Form::email('nombres', $usuario->cuenta->cuenta_primer_nombre.' '.$usuario->cuenta->cuenta_segundo_nombre.' '.$usuario->cuenta->cuenta_primer_apellido.' '.$usuario->cuenta->cuenta_segundo_apellido, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Nombres', 'readonly' => 'true', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('cuenta_cuenta', 'Cuenta') !!}</b>
			{!! Form::email('cuenta_cuenta', $usuario->cuenta->cuenta_cuenta, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'usuario@unicatolica.edu.co', 'readonly' => 'true', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('rol_id', 'Rol') !!}</b>
			{!! Form::select('rol_id', ['Administrador' => 'Administrador', 'Auditor' => 'Auditor', 'Académico' => 'Académico', 'Administrativo' => 'Administrativo'], $usuario->rol_sia, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
			</p>

			<div class="row w3-right">
				<div class="col-sm-12">
					<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
					<a href="{{route('browse.site.usuarios.index')}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<br><br>
@endsection

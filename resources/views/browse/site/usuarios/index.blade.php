@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Usuarios
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
	    	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li class="active"><span>Usuarios</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-row">
		<div class="w3-col s6">
			<h3><span class="fa fa-users fa-fw"></span> Usuarios SIA</h3>
		</div>
		<div class="w3-col s6"><br />
			<a href="{{route('browse.correos.cuentas.index', $sistema)}}" class="w3-btn w3-2017-navy-peony w3-right"><i class="fa fa-power-off fa-fw"></i> Activar Usuario</a>
		</div>
	</div>
	<hr>

	<div class="w3-responsive">
		<table class="w3-table-all">
			<thead>
				<tr class="w3-2017-navy-peony">
					<th>Nombres</th>
					<th>Cuenta</th>
					<th>Rol</th>
					<th>Fecha Activación</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($usuarios as $usuario)
					<tr>
						<td>{{ $usuario->cuenta_primer_apellido.' '.$usuario->cuenta_segundo_apellido.' '.$usuario->cuenta_primer_nombre.' '.$usuario->cuenta_segundo_nombre }}</td>
						<td>{{ $usuario->cuenta_cuenta }}</td>
						<td>
							@if($usuario->rol_sia == 'Administrador')
								<span class="w3-cyan w3-padding">Administrador</span>
							@elseif($usuario->rol_sia == 'Auditor')
								<span class="w3-teal w3-padding">Auditor</span>
							@elseif($usuario->rol_sia == 'Académico')
								<span class="w3-orange w3-padding">Académico</span>
							@elseif($usuario->rol_sia == 'Administrativo')
								<span class="w3-red w3-padding">Administrativo</span>
							@endif
						</td>
						<td>
							{{$usuario->created_at}}
						</td>
						<td>
							<a href="{{ route('browse.site.usuarios.edit', $usuario) }}" class="w3-btn w3-white w3-border" title="Cambiar rol">
								<i class="fa fa-pencil fa-fw"></i>
							</a>
							@if($usuario->rol_sia != 'Administrador')
								<a href="{{ route('browse.site.usuarios.permisos', $usuario) }}" class="w3-btn w3-white w3-border" title="Permisos">
									<i class="fa fa-users fa-fw"></i>
								</a>
							@else
								<button class="w3-btn w3-white w3-border w3-disabled"><i class="fa fa-users fa-fw"></i></button>
							@endif	
							<a href="#" class="w3-btn w3-white w3-border w3-disabled" title="Restablecer contraseña">
								<i class="fa fa-key fa-fw"></i>
							</a>
							<a href="#" onclick="return confirm('¿Está seguro que desea desactivar el usuario?');" class="w3-btn w3-white w3-border w3-disabled" title="Desactivar">
								<i class="fa fa-power-off fa-fw"></i>
							</a>
							<a href="{{ route('browse.site.usuarios.entrances', $usuario) }}" class="w3-btn w3-white w3-border" title="Historial">
								<i class="fa fa-history fa-fw"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="w3-center">
		<div class="w3-bar">
			{!! $usuarios->render() !!}
		</div>
	</div>
</div>
@endsection

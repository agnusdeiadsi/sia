@extends('site.layout.default')
@section('titulo')
	Crear Usuario
@endsection

@section('contenido')
	<h2 class="w3-text-light-grey">Activar Usuario</h2>
	<hr style="width:200px" class="w3-opacity">

	{!! Form::open(['route' => 'site.usuarios.store', 'method' => 'POST']) !!}
		<p>
		<b>{!! Form::label('cuenta_id', 'Correo') !!}</b>
		{!! Form::select('cuenta_id', $cuentas, '', ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
		</p>

		<p>
		<b>{!! Form::label('rol_id', 'Rol') !!}</b>
		{!! Form::select('rol_id', ['Administrador' => 'Administrador', 'Auditor' => 'Auditor', 'Académico' => 'Académico', 'Administrativo' => 'Administrativo'], null, ['class' => 'w3-input', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
		</p>

		<p>
		<i>{!! Form::label('password', 'Contraseña predeterminada: system2000') !!}</i>
		{!! Form::text('password', 'system2000', ['placeholder' => 'Contraseña', 'required' => 'true', 'hidden' => 'true']) !!}
		</p>

		<p>
		{!! Form::submit('Guardar', ['class' => 'w3-btn w3-white w3-border']) !!}
		</p>
	{!! Form::close() !!}
@endsection

@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Usuarios | Editar
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
	    	<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{url('browse/site/home')}}">Administración del Sitio</a></li>
			<li><a href="{{route('browse.site.usuarios.index')}}">Usuarios</a></li>
			<li class="active"><span>Registros</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<h2>Entradas<a class="w3-bar-item" data-toggle="tooltip" title="Registros de concurrencia del usuario." data-placement="auto"><span class="fa fa-question-circle fa-fw"></span></a></h2>
	<hr>

	<div class="w3-responsive">
		<div class="w3-center">
			{{ $entrances->render() }}
		</div>
		<table class="w3-table-all">
			<thead>
				<tr class="w3-2017-navy-peony">
					<th>Fecha</th>
					<th>Acción</th>
					<th>Url</th>
					<th>Módulo</th>
					<th>Rol</th>
				</tr>
			</thead>
			<body>
				@if(!empty($entrances))
					@foreach($entrances as $entrance)
						<tr>
							<td>{{ $entrance->entrance_startdate }}</td>
							<td>{{ $entrance->entrance_accion }}</td>
							<td>{{ $entrance->entrance_url_actual }}</td>
							<td>{{ (!empty($entrance->sistema))?$entrance->sistema->sistema_nombre:null }}</td>
							<td>
								@if($entrance->usuario_rol_modulo == null)
									{{ $entrance->usuario_rol_sia }}
								@else
									{{ $entrance->usuario_rol_modulo }}
								@endif
							</td>							
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="5"><i>No existen registros del usuario {{ $usuario->email }}</i></td>
					</tr>
				@endif
			</body>
		</table>
		<div class="w3-center">
			{{ $entrances->render() }}
		</div>
	</div>	
</div>
@endsection
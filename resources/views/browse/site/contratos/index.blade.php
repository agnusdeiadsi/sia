@extends('browse.layouts.app')

@section('title')
	Contratos
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li class="active"><span>Contratos</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<div class="w3-row">
			<div class="w3-col s6">
				<h2><span class="fa fa-file-o fa-fw"></span> Contratos</h2>
			</div>
			<div class="w3-col s6"><br>
				<a href="{{ route('browse.site.contratos.create') }}" class="w3-btn w3-2017-navy-peony w3-right"><i class="fa fa-plus fa-fw"></i>  Nuevo</a>
			</div>
		</div>
		<hr style="width:200px" class="w3-opacity">
		<div class="w3-responsive">
			<table class="w3-table-all">
				<thead>
					<tr class="w3-2017-navy-peony">
						<th>Código</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($contratos as $contrato)
						<tr>
							<td>{{ $contrato->contrato_codigo }}</td>
							<td>{{ $contrato->contrato_nombre }}</td>
							<td>{{ $contrato->contrato_descripcion }}</td>
							<td>
								<a href="{{ route('browse.site.contratos.edit', $contrato) }}" class="w3-btn w3-white w3-border" title="Editar"><i class="fa fa-pencil fa-fw"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="w3-center">
				<div class="w3-bar">
						{!! $contratos->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

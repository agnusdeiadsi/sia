@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Contratos | Editar
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.contratos.index') }}">Contratos</a></li>
			<li class="active"><span>Editar</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<h2><span class="fa fa-pencil fa-fw"></span> Editar Contrato</h2>
		<hr>
		{!! Form::open(['route' => ['browse.site.contratos.update', $contrato], 'method' => 'PUT']) !!}
			<p>
				{{ Form::label('contrato_codigo', 'Código*') }}
				{{ Form::text('contrato_codigo', $contrato->contrato_codigo, ['class' => 'w3-input w3-disabled', 'maxlength' => '20', 'readonly']) }}
			</p>

			<p>
				{{ Form::label('contrato_nombre', 'Nombre*') }}
				{{ Form::text('contrato_nombre', $contrato->contrato_nombre, ['class' => 'w3-input', 'maxlength' => '100', 'required']) }}
			</p>

			<p>
				{{ Form::label('contrato_descripcion', 'Descripción (Opcional)') }}
				{{ Form::textarea('contrato_descripcion', $contrato->contrato_descripcion, ['class' => 'w3-input', 'maxlength' => '255']) }}
			</p>

			<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>

			<a href="{{ route('browse.site.contratos.index') }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
		{!! Form::close() !!}
	</div>
</div>
@endsection

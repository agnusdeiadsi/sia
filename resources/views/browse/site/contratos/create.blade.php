@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Contratos | Crear
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.contratos.index') }}">Contratos</a></li>
			<li class="active"><span>Crear</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<h2><span class="fa fa-plus-square fa-fw"></span> Nuevo Contrato</h2>
		<hr>
		{!! Form::open(['route' => 'browse.site.contratos.store', 'method' => 'post']) !!}
			<p>
				{{ Form::label('contrato_codigo', 'Código*') }}
				{{ Form::text('contrato_codigo', null, ['class' => 'w3-input', 'maxlength' => '20', 'placeholder' => 'Código', 'required']) }}
			</p>

			<p>
				{{ Form::label('contrato_nombre', 'Nombre*') }}
				{{ Form::text('contrato_nombre', null, ['class' => 'w3-input', 'maxlength' => '100', 'placeholder' => 'Nombre', 'required']) }}
			</p>

			<p>
				{{ Form::label('contrato_descripcion', 'Descripción (Opcional)') }}
				{{ Form::textarea('contrato_descripcion', null, ['class' => 'w3-input', 'maxlength' => '255', 'placeholder' => 'Descripción']) }}
			</p>

			<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar</button>

			<a href="{{ route('browse.site.contratos.index') }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
		{!! Form::close() !!}
	</div>
</div>
@endsection
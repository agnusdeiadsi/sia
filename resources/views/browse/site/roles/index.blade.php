@extends('browse.layouts.app')

@section('title')
	Roles
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li class="active"><span>Roles</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<div class="w3-row">
			<div class="w3-col s6">
				<h2><span class="fa fa-plug fa-fw"></span>Roles</h2>
			</div>
			<div class="w3-col s6"><br>
				<a href="{{ route('browse.site.roles.create') }}" class="w3-btn w3-2017-navy-peony w3-border w3-right"><i class="fa fa-plus fa-fw"></i>  Nuevo</a>
			</div>
		</div>
		<hr style="width:200px" class="w3-opacity">

		<div class="w3-responsive">
			<table class="w3-table-all">
				<thead>
					<tr class="w3-2017-navy-peony">
						<th>Código</th>
						<th>Nombre</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($roles as $rol)
						<tr>
							<td>{{ $rol->rol_codigo }}</td>
							<td>{{ $rol->rol_nombre }}</td>
							<td><a href="{{ route('browse.site.roles.edit', $rol->rol_id) }}" class="w3-btn w3-white w3-border" title="Editar"><i class="fa fa-pencil fa-fw"></i></a> <a href="{{ route('browse.site.roles.destroy', $rol->rol_id.'/destroy') }}" onclick="return confirm('¿Está seguro que desea eliminar el usuario?');" class="w3-btn w3-white w3-border" title="Borrar"><i class="fa fa-eraser fa-fw"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<div class="w3-center">
			<div class="w3-bar">
					{!! $roles->render() !!}
			</div>
		</div>
	</div>
</div>
@endsection

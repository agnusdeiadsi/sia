@extends('site.layouts.default')

@section('title')
	Crear
@endsection

@section('content')
	<!-- breadcrumbs -->
	<div class="w3-content"><br>
	    <ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('site.roles.index') }}">Roles</a></li>
			<li class="active"><span>Crear</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<h2 class="w3-text-light-grey">Crear</h2>
	<hr style="width:200px" class="w3-opacity">

	{!! Form::open(['route' => 'roles.store', 'method' => 'POST']) !!}
		<p>
		{!! Form::label('rol_codigo', 'Código') !!}
		{!! Form::text('rol_codigo', null, ['class' => 'w3-input', 'placeholder' => 'Código', 'id' => 'codigo' , 'required' => 'true']) !!}
		</p>
		<p>
		{!! Form::label('rol_nombre', 'Nombre') !!}
		{!! Form::text('rol_nombre', null, ['class' => 'w3-input', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
		</p>
		<p>
		{!! Form::label('rol_descripcion', 'Descripción') !!}
		{!! Form::textarea('rol_descripcion', null, ['class' => 'w3-input', 'placeholder' => 'Descripción']) !!}
		</p>
		<p>
		{!! Form::submit('Guardar', ['class' => 'w3-btn w3-white w3-border']) !!}
		</p>
	{!! Form::close() !!}
@endsection

@extends('browse.layouts.app')

@section('title')
	Panel de Administración | Sistemas | Editar
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.sistemas.index') }}">Sistemas</a></li>
			<li class="active"><span>Editar</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->
	<div class="w3-content">
		<h2><span class="fa fa-pencil fa-fw"></span> Editar Sistema</h2>
		<hr>
		{!! Form::open(['route' => ['browse.site.sistemas.update', $sistema], 'method' => 'PUT', 'files' => true]) !!}
			<p>
			<b>{!! Form::label('sistema_codigo', 'Código*') !!}</b>
			{!! Form::text('sistema_codigo', $sistema->sistema_codigo, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '20', 'placeholder' => 'Código', 'id' => 'codigo' , 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_nombre', 'Nombre*') !!}</b>
			{!! Form::text('sistema_nombre', $sistema->sistema_nombre, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '100', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_nombre_corto', 'Nombre Corto* (Laravel Route)') !!}</b>
			{!! Form::text('sistema_nombre_corto', $sistema->sistema_nombre_corto, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '20', 'placeholder' => 'Nombre', 'required' => 'true', 'readonly']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_descripcion', 'Descripción*') !!}</b>
			{!! Form::textarea('sistema_descripcion', $sistema->sistema_descripcion, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '255', 'placeholder' => 'Descripción (255 caracteres)', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_released', 'Released*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Fecha de lanzamiento del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::date('sistema_released', $sistema->sistema_released, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Released', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_version', 'Versión*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Versión del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::text('sistema_version', $sistema->sistema_version, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '10', 'placeholder' => 'version', 'required' => 'true']) !!}
			</p>

			<p>
				<b>{!! Form::label('sistema_icon', 'Logo') !!}<a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Suba el logo del modulo con las siguientes caracteristicas: formato png sin fondo, color blanco y tamaño 256px/256px."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				<div class="w3-{{$sistema->sistema_colorclass}} w3-padding">
					<img src="{{ asset('/images/icon/'.$sistema->sistema_icon) }}" width="150">
				</div>
				<br>
				{!! Form::file('sistema_icon', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'accept' => 'image/.png']) !!}
			</p>

			<p>
				<b>{!! Form::label('sistema_manual', 'Manual') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Subir manual del módulo en formato PDF."><span class="fa fa-question-circle fa-fw"></span></a></b><br>

				@if($sistema->sistema_manual != null)
					<a href="{{url('browse/site/sistemas/download/manual', [$sistema])}}" class="w3-btn w3-{{$sistema->sistema_colorclass}} col-sm-12"><span class="fa fa-download fa-fw"></span> Descargar Manual</a><br><br>
				@endif

				{!! Form::file('sistema_manual', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'accept' => 'e.g:.pdf']) !!}
			</p>

			<p>
			<b>{!! Form::label('rol_id', 'Roles Sistema*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Seleccione los roles que el módulo tendrá. Para seleccionar varios roles puede seleccionarlos haciendo clic sostenido y arrantrando el cursor, o presionando la tecla ctrl+clic sobre cada rol."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
			{!! Form::select('rol_id[]', $roles, $sistema_rol, ['class' => 'w3-input w3-border-2017-navy-peony', 'multiple'=>'multiple', 'size' => '15']) !!}
			</p>

			<p>
				<b>{!! Form::label('sistema_colorclass', 'Color*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Elija el color que tendrá el módulo de la siguiente paleta de colores."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				{!! Form::select('sistema_colorclass', $colores, $sistema->sistema_colorclass, ['class' => 'w3-input w3-border-2017-navy-peony', 'onchange' => 'changeNameClass(this.value); return false', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}

				<div class="w3-white w3-padding" id="color-container">
					<div id="example" class="w3-padding w3-{{$sistema->sistema_colorclass}}">
						{{ $sistema->sistema_colorclass }}
					</div>
				</div>
			</p>

			<p>
			<b>{!! Form::label('sistema_desarrollador', 'Desarrollador*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Escriba los nombres del desarrollar del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::text('sistema_desarrollador', $sistema->sistema_desarrollador, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '100', 'placeholder' => 'Desarrollador', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('subarea_id', 'Área del Sistema*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Seleccione el área propietaria del módulo o quien administra el módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::select('subarea_id', $subareas, $sistema->subarea_id, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '100', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
			</p>

			<div class="row w3-right">
				<div class="col-sm-12">
					<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
					<a href="{{route('browse.site.sistemas.index')}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<script>
	function changeNameClass(name)
	{
		var example, container;
		container = document.getElementById("color-container");
		example = document.getElementById("example");

		container.style.display="block";
		example.className = "w3-padding w3-"+name;
		example.innerHTML=name;
	}
</script>
@endsection

@extends('browse.'.$sistema->sistema_nombre_corto.'.menu')

@section('title-modulo')
	{{$sistema->sistema_nombre}} | Manual
@endsection

@section('content-modulo')
	<div class="w3-container">
		<object width="100%" height="700" data="{{asset('files/manuales/'.$sistema->sistema_manual.'')}}"></object>
	</div>
@endsection
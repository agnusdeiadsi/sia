@extends('browse.layouts.app')

@section('title')
	Sistemas
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li class="active"><span>Sistemas</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-row">
		<div class="w3-col s6">
			<h2><span class="fa fa-server fa-fw"></span> Sistemas</h2>
		</div>
		<div class="w3-col s6"><br>
			<a href="{{ route('browse.site.sistemas.create') }}" class="w3-btn w3-2017-navy-peony w3-right"><i class="fa fa-plus fa-fw"></i>  Nuevo</a>
		</div>
	</div>
	<hr style="width:200px" class="w3-opacity">
	<div class="w3-responsive">
		<table class="w3-table-all">
			<thead>
				<tr class="w3-2017-navy-peony">
					<th>Nombre</th>
					<th>Laravel Route</th>
					<th>Released</th>
					<th>Color</th>
					<th>Desarrollador</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($sistemas as $sistema)
					<tr>
						<td>{{ $sistema->sistema_nombre }}</td>
						<td>{{ $sistema->sistema_nombre_corto }}</td>
						<td>{{ $sistema->sistema_released }}</td>
						<td>
							<div class="w3-padding w3-{{ $sistema->sistema_colorclass }}">
									{{ $sistema->sistema_colorclass }}
							</div>
						</td>
						<td>
								{{ $sistema->sistema_desarrollador }}
						</td>
						<td>
							<a href="{{ route('browse.site.sistemas.edit', $sistema) }}" class="w3-btn w3-white w3-border" title="Editar"><i class="fa fa-pencil fa-fw"></i></a>
							@if($sistema->enabled == 1)
								<a href="{{ route('browse.site.sistemas.disable', $sistema) }}" class="w3-btn w3-red" title="Deshabilitar"><i class="fa fa-power-off fa-fw"></i></a>
							@elseif($sistema->enabled == 0)
								<a href="{{ route('browse.site.sistemas.enable', $sistema) }}" class="w3-btn w3-green" title="Habilitar"><i class="fa fa-power-off fa-fw"></i></a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="w3-center">
			<div class="w3-bar">
					{!! $sistemas->render() !!}
			</div>
		</div>
	</div>
</div>
@endsection

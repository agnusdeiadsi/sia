@extends('browse.layouts.app')
@section('title')
	Panel de Administración | Sistemas | Nuevo
@endsection

@section('content')
<div class="w3-container">
	<!-- breadcrumbs -->
	<div class="w3-container"><br>
		<ol class="breadcrumb breadcrumb-arrow">
			<li><a href="{{ url('browse/home') }}">Inicio</a></li>
			<li><a href="{{ url('browse/site/home') }}">Administración del Sitio</a></li>
			<li><a href="{{ route('browse.site.sistemas.index') }}">Sistemas</a></li>
			<li class="active"><span>Nuevo</span></li>
		</ol>
	</div>
	<!-- fin breadcrumbs -->

	<div class="w3-content">
		<h2><span class="fa fa-plus-square fa-fw"></span> Nuevo Sistema</h2>
		<hr>
		{!! Form::open(['route' => 'browse.site.sistemas.store', 'method' => 'POST', 'files' => true]) !!}
			<p>
			<b>{!! Form::label('sistema_codigo', 'Código*') !!}</b>
			{!! Form::text('sistema_codigo', null, ['class' => 'w3-input', 'maxlength' => '20', 'placeholder' => 'Código', 'id' => 'codigo' , 'required' => 'true']) !!}
			</p>
			<p>
			<b>{!! Form::label('sistema_nombre', 'Nombre*') !!}</b>
			{!! Form::text('sistema_nombre', null, ['class' => 'w3-input', 'maxlength' => '100', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
			</p>
			<p>
			<b>{!! Form::label('sistema_nombre_corto', 'Nombre Corto* (Laravel Route)') !!}</b>
			{!! Form::text('sistema_nombre_corto', null, ['class' => 'w3-input', 'maxlength' => '20', 'placeholder' => 'Nombre Corto', 'id' => 'codigo' , 'required' => 'true']) !!}
			</p>
			<p>
			<b>{!! Form::label('sistema_descripcion', 'Descripción*') !!}</b>
			{!! Form::textarea('sistema_descripcion', null, ['class' => 'w3-input', 'maxlength' => '2000', 'placeholder' => 'Descripción (2000 caracteres)', 'required' => 'true']) !!}
			</p>
			<p>
			<b>{!! Form::label('sistema_released', 'Released*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Fecha de lanzamiento del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::date('sistema_released', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'placeholder' => 'Released', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('sistema_version', 'Versión*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Versión del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::text('sistema_version', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'maxlength' => '10', 'placeholder' => 'version', 'required' => 'true']) !!}
			</p>

			<p>
				<b>{!! Form::label('sistema_icon', 'Logo*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Suba el logo del modulo con las siguientes caracteristicas: formato png sin fondo, color blanco y tamaño 256px/256px."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				{!! Form::file('sistema_icon', null, ['class' => 'w3-input', 'accept' => 'image/.png', 'required' => 'true']) !!}
			</p>

			<p>
				<b>{!! Form::label('sistema_manual', 'Manual*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Subir manual del módulo en formato PDF."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				{!! Form::file('sistema_manual', null, ['class' => 'w3-input w3-border-2017-navy-peony', 'accept' => 'e.g:.pdf', 'required' => 'true']) !!}
			</p>

			<p>
				<b>{!! Form::label('rol_id', 'Roles Sistema*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Seleccione los roles que el módulo tendrá."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				@foreach($roles as $rol)
					{!! Form::checkbox('rol_id[]', $rol->rol_id, null) !!}
					<b>{!! Form::label('rol_id', $rol->rol_nombre) !!}</b><br>
				@endforeach
			</p>

			<p>
				<b>{!! Form::label('sistema_colorclass', 'Color*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Elija el color que tendrá el módulo de la siguiente paleta de colores."><span class="fa fa-question-circle fa-fw"></span></a></b><br>
				{!! Form::select('sistema_colorclass', $colores, null, ['class' => 'w3-input', 'onchange' => 'changeNameClass(this.value); return false', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}

				<div class="w3-white w3-padding" id="color-container" style="display:none;">
					<div id="example">
					</div>
				</div>
			</p>

			<p>
			<b>{!! Form::label('sistema_desarrollador', 'Desarrollador*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Escriba los nombres del desarrollar del módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::text('sistema_desarrollador', null, ['class' => 'w3-input', 'maxlength' => '100', 'placeholder' => 'Desarrollador', 'required' => 'true']) !!}
			</p>

			<p>
			<b>{!! Form::label('subarea_id', 'Área del Sistema*') !!}<a class="w3-bar-item" data-toggle="tooltip" title="Seleccione el área propietaria del módulo o quien administra el módulo."><span class="fa fa-question-circle fa-fw"></span></a></b>
			{!! Form::select('subarea_id', $subareas, null, ['class' => 'w3-input', 'maxlength' => '100', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
			</p>


			<div class="row w3-right">
				<div class="col-sm-12">
					<button type="submit" class="w3-btn w3-2017-navy-peony"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
					<a href="{{route('browse.site.sistemas.index')}}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<script>
	function changeNameClass(name)
	{
		var example, container;
		container = document.getElementById("color-container");
		example = document.getElementById("example");

		container.style.display="block";
		example.className = "w3-padding w3-"+name;
		example.innerHTML=name;
	}
</script>
@endsection

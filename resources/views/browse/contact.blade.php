@extends('browse.layouts.app')

@section('title')
	Contacto
@endsection
@section('content')
@php
	$auth = \App\Model\Correos\Cuenta::find(Auth::user()->id);
@endphp
<!-- Contact Section -->
  <div class="w3-container w3-padding-large w3-grey">
    <h4 id="contact"><b>Contáctenos</b></h4>
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
        <p>soportesia@unicatolica.edu.co</p>
      </div>
      <div class="w3-third w3-2017-navy-peony">
        <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
        <p>Campus Pance, Cali, Valle</p>
      </div>
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
        <p>(+57) 2 3120038 Ext: 1052</p>
      </div>
    </div>
    <hr class="w3-opacity">
    <form action="/action_page.php" target="_blank">
      <div class="w3-section">
        <label>Nombres*</label>
        <input class="w3-input w3-border" type="text" name="Name" value="{{$auth->cuenta_primer_nombre.' '.$auth->cuenta_segundo_nombre.' '.$auth->cuenta_primer_apellido.' '.$auth->cuenta_segundo_apellido}}" placeholder="Nombres" required>
      </div>
      <div class="w3-section">
        <label>Email*</label>
        <input class="w3-input w3-border" type="text" name="Email" value="{{Auth::user()->email}}" readonly required>
      </div>
      <div class="w3-section">
        <label>Mensaje*</label>
        <textarea class="w3-input w3-border w3-text-grey" name="Message" rows="5" placeholder="Mensaje" required></textarea>
      </div>
      <button type="submit" class="w3-button w3-white w3-border w3-right"><i class="fa fa-paper-plane w3-margin-right"></i>Enviar</button>
    </form>
  </div>
  <br>
  <div class="w3-panel w3-large w3-center">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
  </div>
@endsection
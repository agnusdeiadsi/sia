@extends('browse.layouts.modulo')

@section('title-modulo')
  {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
<style type="text/css">
  a{text-decoration: none;}
</style>
	<div class="w3-bar-block">
    <a href="{{ route('browse.correos.cuentas.account', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-user fa-fw"></i> Mi Cuenta</a>

    {{-- Acceso permitido al admministrador de SIA, Manager del modulo y solicitantes --}}
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == "Manager" || Auth::user()->rol_modulo == "Solicitante")
			<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey" onclick="accordion('accordion-cuentas')"><i class="fa fa-envelope fa-fw"></i>  Cuentas <span class="fa fa-caret-down"></span></a>
			<div id="accordion-cuentas" class="w3-hide">
        <a href="{{ route('browse.correos.cuentas.create', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&nbsp;&nbsp;<i class="fa fa-caret-right fa-fw"></i> Crear Cuenta</a>

        {{--Acceso permitido al admministrador de SIA o Manager del modulo --}}
        @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == "Manager")
				  <a href="{{route('browse.correos.cuentas.requests', [$sistema, 'filtro' => '1'])}}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&nbsp;&nbsp;<i class="fa fa-caret-right fa-fw"></i> Solicitudes</a>
        @endif

        {{--Accedo permitido al Adminsitrador de SIA--}}
        @if(Auth::user()->rol_sia == 'Administrador')
          <a href="{{ route('browse.correos.cuentas.index', $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey">&nbsp;&nbsp;<i class="fa fa-caret-right fa-fw"></i> Cuentas</a>
        @endif
			</div>
    @endif
	</div>
@endsection

@section('content-modulo')
	<center><h1><b>{{$sistema->sistema_nombre}}</b></h1></center>
  <hr>
  <div class="w3-container">
    <center><h3><b>Aquí podrás:</b></h3></center>
    <div class="row">
      <div class="col-sm-2 col-sm-offset-4">
        <center><img src="{{ asset('images/menu-icon/correos/icon-01.png') }}" class="w3-image"></center>
      </div>
      <div class="col-sm-2">
        <center><img src="{{ asset('images/menu-icon/correos/icon-02.png') }}" class="w3-image"></center>
      </div>
    </div>
  </div>
@endsection

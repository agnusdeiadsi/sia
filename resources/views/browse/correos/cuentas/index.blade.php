@extends('browse.correos.menu')

@section('title-modulo')
 Correos | Cuentas
@endsection

@section('content-modulo')
  @if(Auth::user()->rol_sia == 'Administrador')
    <div class="row">
      <div class="col-sm-8">
        <h2>Cuentas Institucionales</h2>
      </div>
      <div class="col-sm-4"><br />
        {!! Form::open(['route' => ['browse.correos.cuentas.index', $sistema], 'method' => 'get']) !!}
          <div class="input-group">
            {!! Form::text('filtrar', null, ['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass, 'placeholder' => 'Buscar por Identificación, nombres, apellidos o email', 'aria-describedby' => 'filtrar']) !!}
            <span class="input-group-addon" id="filtrar"><i class="fa fa-search fa-fw"></i></span>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
    <hr />

  <div class="w3-responsive">
  	<table class="w3-table-all">
  		<thead>
  			<tr class="w3-{{$sistema->sistema_colorclass}}">
  				<th>Nombres</th>
  				<th>Cuenta</th>
  				<th>Tipo</th>
  				<th>Estado</th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody>
  			@foreach($cuentas as $cuenta)
  				<tr>
  					<td>{{ $cuenta->cuenta_primer_apellido.' '.$cuenta->cuenta_segundo_apellido.' '.$cuenta->cuenta_primer_nombre.' '.$cuenta->cuenta_segundo_nombre }}</td>
  					<td>{{ $cuenta->cuenta_cuenta }}</td>
  					<td>{{ $cuenta->tipo_nombre }}</td>
  					<td>{{ $cuenta->estado_nombre }}</td>
  					<td>
  						<a href="{{ route('browse.correos.cuentas.edit', [$sistema, $cuenta]) }}" class="w3-btn w3-white w3-border" title="Editar"><i class="fa fa-pencil fa-fw"></i></a>

  						@if($cuenta->estado_nombre != 'Procesamiento')
  							<!--Solicitar restablecer la contraseña-->
  							<a href="{{ route('browse.correos.cuentas.process', [$sistema, $cuenta, 3]) }}" class="w3-btn w3-white w3-border" onclick="return confirm('¿Desea restablecer la contraseña de esta cuenta?');" title="Solicitar restablecer contraseña del correo"><i class="fa fa-lock fa-fw"></i></a>
              @else
                <!--Solicitar restablecer la contraseña-->
                <a href="{{ route('browse.correos.cuentas.process', [$sistema, $cuenta, 3]) }}" class="w3-btn w3-white w3-border" onclick="return confirm('¿Desea restablecer la contraseña de esta cuenta?');" title="Solicitar restablecer contraseña del correo"><i class="fa fa-lock fa-fw"></i></a>
  						@endif

              @if(empty($cuenta->usuario))
                @if($cuenta->estado_nombre == 'Procesamiento')
                  <!--Activa la cuenta como usuario en SIA-->
                  <a href="javascript:void(0)" class="w3-btn w3-green w3-border-green w3-disabled" title="Activar usuario SIA"><i class="fa fa-power-off fa-fw"></i></a>
                @else
                  <!--desactiva la cuenta del usuario en SIA-->
                  <a href="{{ route('browse.site.usuarios.activate', $cuenta) }}" class="w3-btn w3-green w3-border-green" title="Activar usuario SIA"><i class="fa fa-power-off fa-fw"></i></a>
                @endif
              @else
                <!--Activa la cuenta como usuario en SIA-->
                <a href="javascript:void(0)" class="w3-btn w3-red w3-border-red w3-disabled" title="Desactivar usuario SIA"><i class="fa fa-power-off fa-fw"></i></a>
              @endif

              @if($cuenta->estado_id != 2)
                <!--El estado de la cuenta cambia a Activo-->
                <a href="{{ route('browse.correos.cuentas.process', [$sistema, $cuenta, 2]) }}" class="w3-btn w3-white w3-border" title="Procesar Solicitud"><i class="fa fa-check fa-fw"></i></a>
              @else
                <a href="javascript:void(0)" class="w3-btn w3-white w3-border w3-disabled" title="Procesar Solicitud"><i class="fa fa-check fa-fw"></i></a>
              @endif
  					</td>
  				</tr>
  			@endforeach
  		</tbody>
  	</table>
  </div>

  <div class="w3-center">
    <div class="w3-bar">
      {!! $cuentas->render() !!}
    </div>
  </div>

  <center>
    <a href="{{url()->previous()}}" class="w3-bar-item w3-btn w3-red"><span class="fa fa-reply fa-fw"></span> Volver</a>
  </center>
  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection

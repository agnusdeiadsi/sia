@extends('browse.correos.menu')

@section('title-modulo')
  Correos | Solicitudes
@endsection

@section('content-modulo')
  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')

  @php
    extract($_REQUEST);
  @endphp

  <div class="w3-container">
    <h2>Solicitudes</h2>
    <hr />

    <div class="w3-row w3-bar w3-white">
      <div class="w3-col s4 tablink w3-bottombar" id="1" onclick="openTab(event,'procesamiento')">
        <a href="{{ route('browse.correos.cuentas.requests', [$sistema, 'filtro' => '1']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Procesamiento
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$cuentas->where('estado_id', 1)->count()}}
          </span>
        </a>
      </div>

      <div class="w3-col s4 tablink w3-bottombar" id="3" onclick="openTab(event,'restablecercontrasena')">
        <a href="{{ route('browse.correos.cuentas.requests', [$sistema, 'filtro' => '3']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Restablecer Contraseña
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$cuentas->where('estado_id', 3)->count()}}
          </span>
        </a>
      </div>

      <div class="w3-col s4 tablink w3-bottombar" id="6" onclick="openTab(event,'actualizardatos')">
        <a href="{{ route('browse.correos.cuentas.requests', [$sistema, 'filtro' => '6']) }}" class="w3-col s12 w3-bar-item w3-button w3-bar w3-white w3-padding" style="text-decoration: none;">Actualizar Datos
          <span class="w3-badge w3-{{$sistema->sistema_colorclass}}">
            {{$cuentas->where('estado_id', 6)->count()}}
          </span>
        </a>
      </div>
    </div>

    <!--TABS-->
    <div id="procesamiento" class="w3-container city">
      <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Solicitud</th>
              <th>Documento</th>
              <th>Nombres</th>
              <th>Correo Institucional</th>
              <th>Correo Personal</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_recibos = 0;
            @endphp
            @foreach($cuentas->where('estado_id', 1) as $cuenta)
              <tr>
                <td>{{ $cuenta->updated_at }}</td>
                <td>{{ $cuenta->cuenta_identificacion }}</td>
                <td>{{ $cuenta->cuenta_primer_nombre.' '.$cuenta->cuenta_segundo_nombre.' '.$cuenta->cuenta_primer_apellido.' '.$cuenta->cuenta_segundo_apellido }}</td>
                <td>{{ $cuenta->cuenta_cuenta }}</td>
                <td>{{ $cuenta->cuenta_email }}</td>
                <td>
                  <a href="{{ route('browse.correos.cuentas.processrequest', [$sistema, $cuenta, 2, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" onclick="return confirm('¿Está seguro que desea procesar esta solicitud?');" title="Procesar"><i class="fa fa-check fa-fw"></i></a>
                </td>
              </tr>
              @php
                $total_recibos++;
              @endphp
            @endforeach

            @if($total_recibos==0)
              <tr>
                <td colspan="6"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
      <div class="w3-center">
        <div class="w3-bar">
          {!! $cuentas->appends(['filtro' => '1'])->render() !!}
        </div>
      </div>
      </p>
    </div>

    <div id="restablecercontrasena" class="w3-container city" style="display:none">
      <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Solicitud</th>
              <th>Documento</th>
              <th>Nombres</th>
              <th>Correo Institucional</th>
              <th>Correo Personal</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_recibos = 0;
            @endphp
            @foreach($cuentas->where('estado_id', 3) as $cuenta)
              <tr>
                <td>{{ $cuenta->updated_at }}</td>
                <td>{{ $cuenta->cuenta_identificacion }}</td>
                <td>{{ $cuenta->cuenta_primer_nombre.' '.$cuenta->cuenta_segundo_nombre.' '.$cuenta->cuenta_primer_apellido.' '.$cuenta->cuenta_segundo_apellido }}</td>
                <td>{{ $cuenta->cuenta_cuenta }}</td>
                <td>{{ $cuenta->cuenta_email }}</td>
                <td>
                  <a href="{{ route('browse.correos.cuentas.processrequest', [$sistema, $cuenta, 2, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" onclick="return confirm('¿Está seguro que desea procesar esta solicitud?');" title="Procesar"><i class="fa fa-check fa-fw"></i></a>
                </td>
              </tr>
              @php
                $total_recibos++;
              @endphp
            @endforeach

            @if($total_recibos==0)
              <tr>
                <td colspan="6"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
        <div class="w3-center">
    			<div class="w3-bar">
    				{!! $cuentas->appends(['filtro' => '3'])->render() !!}
    			</div>
    		</div>
      </div>
      </p>
    </div>

    <div id="actualizardatos" class="w3-container city" style="display:none">
      <p>
      <div class="w3-responsive">
        <table class="w3-table w3-striped w3-hoverable">
          <thead>
            <tr class="w3-{{ $sistema->sistema_colorclass }}">
              <th>Fecha Solicitud</th>
              <th>Documento</th>
              <th>Nombres</th>
              <th>Correo Institucional</th>
              <th>Correo Personal</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="w3-text-grey">
            @php
              $total_recibos = 0;
            @endphp
            @foreach($cuentas->where('estado_id', 6) as $cuenta)
              <tr>
                <td>{{ $cuenta->updated_at }}</td>
                <td>{{ $cuenta->cuenta_identificacion }}</td>
                <td>{{ $cuenta->cuenta_primer_nombre.' '.$cuenta->cuenta_segundo_nombre.' '.$cuenta->cuenta_primer_apellido.' '.$cuenta->cuenta_segundo_apellido }}</td>
                <td>{{ $cuenta->cuenta_cuenta }}</td>
                <td>{{ $cuenta->cuenta_email }}</td>
                <td>
                  <a href="{{ route('browse.correos.cuentas.processrequest', [$sistema, $cuenta, 2, 'filtro' => $filtro]) }}" class="w3-btn w3-white w3-border" onclick="return confirm('¿Está seguro que desea procesar esta solicitud?');" title="Procesar"><i class="fa fa-check fa-fw"></i></a>
                </td>
              </tr>
              @php
                $total_recibos++;
              @endphp
            @endforeach

            @if($total_recibos==0)
              <tr>
                <td colspan="6"><i>No hay registros.</i></td>
              </tr>
            @endif
          </tbody>
        </table>
        <div class="w3-center">
          <div class="w3-bar">
            {!! $cuentas->appends(['filtro' => '6'])->render() !!}
          </div>
        </div>
      </div>
      </p>
    </div>
  </div>

  <script>
  document.getElementById({{$filtro}}).click();

  function openTab(evt, tabName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-{{$sistema->sistema_colorclass}}", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " w3-border-{{$sistema->sistema_colorclass}}";
  }
  </script>

  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection

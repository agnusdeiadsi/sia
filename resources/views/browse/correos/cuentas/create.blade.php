@extends('browse.correos.menu')

@section('title-modulo')
  Correos | Crear
@endsection

@section('content-modulo')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <script type="text/javascript">
  	function crearUsuario(usuario)
  	{
  		var usuario = usuario.split("@", 1);
  		document.getElementById('cuenta_usuario').value = usuario;
  	}

  	function strToUpper(idInput)
  	{
  		input = document.getElementById(idInput).value;
  		strupper = input.toUpperCase();
  		document.getElementById(idInput).value=strupper;
  	}
  </script>

  @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager' || Auth::user()->rol_modulo == 'Solicitante')

  @php
  extract($_REQUEST);
  $filtro=true;
  @endphp

  <div class="w3-container">
    <h2>Crear Cuenta <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="La información aquí registrada será usada en su cuenta de correo institucional y en la plataforma SIA. Existen dos tipos de cuenta: docencia y administrativa. Las cuentas tipo docencia son aquellas cuentas designadas exclusivamente para docentes ya que estan formadas por sus apellidos y nombres. Las cuentas tipo administrativa son de uso exclusivo para cargos. Ejemplo: direccion@unicatolica.edu.co o decanaturafce@unicatolica.edu.co."><span class="fa fa-question-circle fa-fw"></span></a></h2>
    <hr />
    <h4>Elige el tipo de correo que deseas crear.</h4>
    <div class="w3-row w3-bar w3-white">
      <a href="javascript:void(0)" class="w3-text-grey" onclick="openTab(event, 'docentes');">
        <div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><span class="fa fa-graduation-cap fa-fw"></span> Docentes</div>
      </a>

      @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
        <a href="javascript:void(0)" class="w3-text-grey" onclick="openTab(event, 'administrativos');">
          <div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><span class="fa fa-building-o fa-fw"></span> Administrativos</div>
        </a>
      @endif
    </div>

    <!--TABS-->
    <div id="docentes" class="w3-container tab" style="display:none">
      <p>
      	{!! Form::open(['route' => ['browse.correos.cuentas.store', $sistema], 'method' => 'post', 'multipart/form-data' => 'true']) !!}
      		<div class="row">
      			<div class="col-sm-5 w3-padding">
      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      									<h4>Información Personal</h4>
      						</div>
      					</div>
      				</div>
      				<p>
      					<label>Tipo Identificación*</label>
      					{!! Form::select('cuenta_tipo_identificacion', ['CC' => 'Cédula Ciudadanía', 'CE' => 'Cédula Extranjería', 'PS' => 'Pasaporte', 'TI' => 'Tarjeta Identidad'], null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sexo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Identificación*</label>
      					{!! Form::text('cuenta_identificacion', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Primer Nombre*</label>
      					{!! Form::text('cuenta_primer_nombre', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_nombre"); return false', 'onkeyup'=> 'strToUpper("primer_nombre"); return false', 'placeholder' => 'Primer Nombre', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Segundo Nombre</label>
      					{!! Form::text('cuenta_segundo_nombre', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_nombre"); return false', 'onkeyup'=> 'strToUpper("segundo_nombre"); return false', 'placeholder' => 'Segundo Nombre']) !!}
      				</p>
      				<p>
      					<label>Primer Apellido*</label>
      					{!! Form::text('cuenta_primer_apellido', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_apellido"); return false', 'onkeyup'=> 'strToUpper("primer_apellido"); return false', 'placeholder' => 'Primer Apellido', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Segundo Apellido</label>
      					{!! Form::text('cuenta_segundo_apellido', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_apellido"); return false', 'onkeyup'=> 'strToUpper("segundo_apellido"); return false', 'placeholder' => 'Segundo Apellido']) !!}
      				</p>
      				<p>
      					<label>Correo Personal*</label>
      					{!! Form::text('cuenta_email', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'correo_personal', 'maxlength' => '100', 'placeholder' => 'Correo Personal', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Género*</label>
      					{!! Form::select('cuenta_genero', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar', 'required']) !!}
      				</p>
              <p>
                <label>Fecha Nacimiento*</label>
                {!! Form::date('cuenta_fecha_nacimiento', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'fecha_nacimiento', 'placeholder' => 'Seleccionar', 'required']) !!}
              </p>
      			</div>
      			<div class="col-sm-offset-1 col-sm-6 w3-padding">
      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      									<h4>Información Contractual</h4>
      						</div>
      					</div>
      				</div>
      				<p>
      					<label>Tipo Contrato*</label>
      					{!! Form::select('contrato_id', $contratos, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'contrato', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
              <p>
                <label>Cargo*</label>
                {!! Form::select('cargo_id', $cargos->pluck('cargo_nombre', 'cargo_id'), null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cargo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </p>
      				<p>
      					<label>Área/Programa*</label>
      					{!! Form::select('subarea_id', $subareas, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'subarea', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
              <p>
                <label>Sede*</label>
                {!! Form::select('sede_id', $sedes->pluck('sede_nombre', 'sede_id'), null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sede', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </p>

      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      								<h4>Cuenta/Directorio</h4>
      						</div>
      					</div>
      				</div>

      				<p>
      					<label>Extensión</label>
      					{!! Form::select('extension_id', $extensiones, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar']) !!}
      				</p>

      				<p><br />
      					<div class="w3-row">
      						<div class="w3-col s5">
      							Acepto <a href="{{asset('files/correos/tratamiento-de-datos-unicatolica.pdf')}}" target="_blank">Términos y Condiciones</a>
      						</div>
      						<div class="w3-col s1">
      							{!! Form::checkbox('cuenta_condiciones', 'SÍ', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cuenta_condiciones', 'required' => 'true']) !!}
      						</div>
      					</div>
      				</p>
      			</div>
      		</div>
      		<hr>
      		<div class="row">
      			<div class="col-sm-12 text-right">
      				<button type="submit" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
      				<a href="{{ route($sistema->sistema_nombre_corto, $sistema) }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
      			</div>
      		</div>
      	{!! Form::close() !!}
      </p>
    </div>
    {{--muestra el formulario para crear correos de tipo administrativos --}}
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Manager')
    <div id="administrativos" class="w3-container tab" style="display:none">
      <p>
      	{!! Form::open(['route' => ['browse.correos.cuentas.storeadmin', $sistema->sistema_id], 'method' => 'post', 'multipart/form-data' => 'true']) !!}
      		<div class="row">
      			<div class="col-sm-5 w3-padding">
      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      									<h4>Información Personal</h4>
      						</div>
      					</div>
      				</div>
      				<p>
      					<label>Tipo Identificación*</label>
      					{!! Form::select('cuenta_tipo_identificacion', ['CC' => 'Cédula Ciudadanía', 'CE' => 'Cédula Extranjería', 'PS' => 'Pasaporte', 'TI' => 'Tarjeta Identidad'], null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sexo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Identificación*</label>
      					{!! Form::text('cuenta_identificacion', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true']) !!}
      				</p>
      				<p>
                <label>Primer Nombre*</label>
                {!! Form::text('cuenta_primer_nombre', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_nombre_admin', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_nombre_admin"); return false', 'onkeyup'=> 'strToUpper("primer_nombre_admin"); return false', 'placeholder' => 'Primer Nombre', 'required' => 'true']) !!}
              </p>
              <p>
                <label>Segundo Nombre</label>
                {!! Form::text('cuenta_segundo_nombre', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_nombre_admin', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_nombre_admin"); return false', 'onkeyup'=> 'strToUpper("segundo_nombre_admin"); return false', 'placeholder' => 'Segundo Nombre']) !!}
              </p>
              <p>
                <label>Primer Apellido*</label>
                {!! Form::text('cuenta_primer_apellido', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'primer_apellido_admin', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_apellido_admin"); return false', 'onkeyup'=> 'strToUpper("primer_apellido_admin"); return false', 'placeholder' => 'Primer Apellido', 'required' => 'true']) !!}
              </p>
              <p>
                <label>Segundo Apellido</label>
                {!! Form::text('cuenta_segundo_apellido', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'segundo_apellido_admin', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_apellido_admin"); return false', 'onkeyup'=> 'strToUpper("segundo_apellido_admin"); return false', 'placeholder' => 'Segundo Apellido']) !!}
              </p>
      				<p>
      					<label>Correo Personal*</label>
      					{!! Form::text('cuenta_email', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'correo_personal', 'maxlength' => '100', 'placeholder' => 'Correo Personal', 'required' => 'true']) !!}
      				</p>
      				<p>
      					<label>Género*</label>
      					{!! Form::select('cuenta_genero', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar', 'required']) !!}
      				</p>
              <p>
                <label>Fecha Nacimiento*</label>
                {!! Form::date('cuenta_fecha_nacimiento', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'fecha_nacimiento', 'placeholder' => 'Seleccionar', 'required']) !!}
              </p>
      			</div>
      			<div class="col-sm-offset-1 col-sm-6 w3-padding">
      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      									<h4>Información Contractual</h4>
      						</div>
      					</div>
      				</div>
      				<p>
      					<label>Tipo Contrato</label>
      					{!! Form::select('contrato_id', $contratos, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'contrato', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
              <p>
                <label>Cargo*</label>
                {!! Form::select('cargo_id', $cargos->pluck('cargo_nombre', 'cargo_id'), null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cargo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </p>
      				<p>
      					<label>Área/Programa*</label>
      					{!! Form::select('subarea_id', $subareas, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'subarea', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
      				</p>
              <p>
                <label>Sede*</label>
                {!! Form::select('sede_id', $sedes->pluck('sede_nombre', 'sede_id'), null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sede', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
              </p>

      				<div class="row">
      					<div class="col-sm-12">
      						<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
      								<h4>Cuenta/Directorio</h4>
      						</div>
      					</div>
      				</div>

              <p>
    						<label>Cuenta*</label>
    						{!! Form::text('cuenta_cuenta', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cuenta_cuenta', 'onkeyup' => 'crearUsuario(this.value)', 'onblur' => 'crearUsuario(this.value)', 'placeholder' => 'Ejemplo: director@unicatolica.edu.co', 'required']) !!}
    					</p>

              <p>
    						{!! Form::text('cuenta_usuario', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cuenta_usuario', 'placeholder' => 'usuario', 'hidden' => 'true']) !!}
    					</p>

      				<p>
      					<label>Extensión (Opcional) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Si no especificas una extensión aquí, no podrás realizar llamadas desde SIA, ni aparecerás en el Directorio Telefónico IP."><span class="fa fa-question-circle fa-fw"></span></a></label>
      					{!! Form::select('extension_id', $extensiones, null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar']) !!}
      				</p>

      				<p><br />
      					<div class="w3-row">
      						<div class="w3-col s5">
      							Acepto <a href="{{asset('files/correos/tratamiento-de-datos-unicatolica.pdf')}}" target="_blank">Términos y Condiciones</a>
      						</div>
      						<div class="w3-col s1">
      							{!! Form::checkbox('cuenta_condiciones', 'SÍ', null, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'cuenta_condiciones', 'required' => 'true']) !!}
      						</div>
      					</div>
      				</p>
      			</div>
      		</div>
      		<hr>
      		<div class="row">
      			<div class="col-sm-12 text-right">
      				<button type="submit" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-save fa-fw"></span> Guardar</button>
      				<a href="{{ route($sistema->sistema_nombre_corto, $sistema) }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
      			</div>
      		</div>
      	{!! Form::close() !!}
      </p>
    </div>
    @endif
  </div>

  <script>
  //document.getElementById({{$filtro}}).click();

  function openTab(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" w3-border-{{ $sistema->sistema_colorclass}}", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-{{ $sistema->sistema_colorclass}}";
  }
  </script>

  @else
    <div class="w3-content">
      <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
        <p class="w3-padding"><br />¡Lo sentimos! Usuario no autorizado.</p>
      </div>
    </div>
  @endif
@endsection

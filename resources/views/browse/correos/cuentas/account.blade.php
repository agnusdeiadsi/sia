@extends('browse.correos.menu')

@section('title-modulo')
	Correos | Mi Cuenta
@endsection

@section('content-modulo')
<script type="text/javascript">
	function crearUsuario(usuario)
	{
		var usuario = usuario.split("@", 1);
		document.getElementById('cuenta_usuario').value = usuario;
	}

	function strToUpper(idInput)
	{
		input = document.getElementById(idInput).value;
		strupper = input.toUpperCase();
		document.getElementById(idInput).value=strupper;
	}
</script>

    <h2><span class="fa fa-user fa-fw"></span> Mi Cuenta <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="La información aquí registrada será usada en tu cuenta de correo institucional y en la plataforma SIA."><span class="fa fa-question-circle fa-fw"></span></a></h2>
    <hr />

		<div class="w3-row-padding">
			{!! Form::open(['route' => ['browse.correos.cuentas.updateaccount', $sistema->sistema_id, $cuenta->cuenta_id], 'method' => 'PUT', 'id' => 'form-update-account', 'files' => true]) !!}
			<div class="row">
				<div class="col-sm-5 w3-padding">
					<div class="row">
						<div class="col-sm-12">
							<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
								<h4>Información Personal</h4>
							</div>
						</div>
					</div>
					<p>
						<label>Tipo Identificación*</label>
						{!! Form::select('cuenta_tipo_identificacion', ['CC' => 'Cédula Ciudadanía', 'CE' => 'Cédula Extranjería', 'PS' => 'Pasaporte', 'TI' => 'Tarjeta Identidad'], $cuenta->cuenta_tipo_identificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'sexo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
					</p>
					<p>
						<label>Identificación*</label>
						{!! Form::text('cuenta_identificacion', $cuenta->cuenta_identificacion, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'identificacion', 'maxlength' => '20', 'placeholder' => 'Identificación', 'required' => 'true']) !!}
					</p>
					<p>
						<label>Primer Nombre*</label>
						{!! Form::text('cuenta_primer_nombre', $cuenta->cuenta_primer_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'primer_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_nombre"); return false', 'placeholder' => 'Primer Nombre', 'required' => 'true']) !!}
					</p>
					<p>
						<label>Segundo Nombre</label>
						{!! Form::text('cuenta_segundo_nombre', $cuenta->cuenta_segundo_nombre, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'segundo_nombre', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_nombre"); return false', 'placeholder' => 'Segundo Nombre']) !!}
					</p>
					<p>
						<label>Primer Apellido*</label>
						{!! Form::text('cuenta_primer_apellido', $cuenta->cuenta_primer_apellido, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'primer_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("primer_apellido"); return false', 'placeholder' => 'Primer Apellido', 'required' => 'true']) !!}
					</p>
					<p>
						<label>Segundo Apellido</label>
						{!! Form::text('cuenta_segundo_apellido', $cuenta->cuenta_segundo_apellido, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'segundo_apellido', 'maxlength' => '100', 'onblur'=> 'strToUpper("segundo_apellido"); return false', 'placeholder' => 'Segundo Apellido']) !!}
					</p>
					<p>
						<label>Correo Personal*</label>
						{!! Form::text('cuenta_email', $cuenta->cuenta_email, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'correo_personal', 'maxlength' => '100', 'placeholder' => 'Correo Personal', 'required']) !!}
					</p>
					<p>
						<label>Género*</label>
						{!! Form::select('cuenta_genero', ['M' => 'Masculino', 'F' => 'Femenino'], $cuenta->cuenta_genero, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar', 'required']) !!}
					</p>
					<p>
                		<label>Fecha Nacimiento*</label>
                		{!! Form::date('cuenta_fecha_nacimiento', $cuenta->cuenta_fecha_nacimiento, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'id' => 'fecha_nacimiento', 'placeholder' => 'Seleccionar', 'required']) !!}
                    </p>
				</div>
				<div class="col-sm-offset-1 col-sm-6 w3-padding">
					<div class="row">
						<div class="col-sm-12">
							<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
								<h4>Información Contractual</h4>
							</div>
						</div>
					</div>
					<p>
						<label>Tipo Contrato*</label>
						{!! Form::select('contrato_id', $contratos, $cuenta->contrato_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'contrato', 'placeholder' => 'Seleccionar', 'required']) !!}
					</p>
					<p>
						<label>Cargo*</label>
						{!! Form::select('cargo_id', $cargos->pluck('cargo_nombre', 'cargo_id'), $cuenta->cargo_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass, 'id' => 'cargo', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
					</p>
					<p>
						<label>Área/Programa*</label>
						{!! Form::select('subarea_id', $subareas, $cuenta->subarea_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'subarea', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
					</p>
					<p>
	                	<label>Sede*</label>
	                	{!! Form::select('sede_id', $sedes->pluck('sede_nombre', 'sede_id'), $cuenta->sede_id, ['class' => 'w3-input w3-border-2017-navy-peony w3-text-grey', 'id' => 'sede', 'placeholder' => 'Seleccionar', 'required' => 'true']) !!}
	                </p>

					<div class="row">
						<div class="col-sm-12">
							<div class="w3-panel w3-leftbar w3-border-{{$sistema->sistema_colorclass}}">
								<h4>Cuenta / Directorio</h4>
							</div>
						</div>
					</div>

					<p>
						<label>Cuenta</label>
						{!! Form::text('cuenta', $cuenta->cuenta_cuenta, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'tipo', 'placeholder' => 'Cuenta', 'disabled' => 'true']) !!}
					</p>

					<p>
						<label>Extensión (Opcional) <a class="w3-bar-item" data-toggle="tooltip" data-placement="auto" title="Si no especificas una extensión aquí, no podrás realizar llamadas desde SIA, ni aparecerás en el Directorio Telefónico IP."><span class="fa fa-question-circle fa-fw"></span></a></label>
						{!! Form::select('extension_id', $extensiones, $cuenta->extension_id, ['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey', 'id' => 'extension', 'placeholder' => 'Seleccionar']) !!}
					</p>

					<p><br />
						<div class="w3-row">
							<div class="w3-col s5">
								Acepto <a href="{{asset('files/correos/tratamiento-de-datos-unicatolica.pdf')}}" target="_blank">Términos y Condiciones</a>
							</div>
							<div class="w3-col s1">
								{!! Form::checkbox('cuenta_condiciones', 'SÍ', $cuenta->cuenta_condiciones, ['class' => 'w3-input w3-border-{{$sistema->sistema_colorclass}} w3-text-grey', 'id' => 'cuenta_condiciones', 'required' => 'true']) !!}
							</div>
						</div>
					</p>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-12 text-right">
					<a href="javascript:void(0)" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="document.getElementById('modalResetPass').style.display='block'"><span class="fa fa-lock fa-fw"></span> Restablecer Contraseña</a>
					<button type="button" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}" onclick="document.getElementById('modalUpdate').style.display='block'"><span class="fa fa-save fa-fw"></span> Guardar Cambios</button>
					<a href="{{ route($sistema->sistema_nombre_corto, $sistema) }}" class="w3-btn w3-red"><span class="fa fa-remove fa-fw"></span> Cancelar</a>
				</div>
			</div>
		{!! Form::close() !!}
		</div>

<div class="w3-container">
	{{-- Modal Reset Password --}}
	<div id="modalResetPass" class="w3-modal">
	<div class="w3-modal-content">
	  <header class="w3-container w3-center w3-{{ $sistema->sistema_colorclass }}"> 
	    <span onclick="document.getElementById('modalResetPass').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h2><span class="fa fa-info-circle fa-fw"></span> Atención</h2>
	  </header>
	  <div class="w3-container w3-center w3-padding-16">
	    <p>¿Está seguro de que desea solicitar el restablecimiento de contraseña de su cuenta de correo institucional?</p>
	    <a href="{{ route('browse.correos.cuentas.process', [$sistema, $cuenta, 3]) }}" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}"><span class="fa fa-check fa-fw"></span> Sí</a>
	    <a href="javascript:voidd(0)" class="w3-btn w3-red" onclick="document.getElementById('modalResetPass').style.display='none'"><span class="fa fa-remove fa-fw"></span> Cerrar</a>
	  </div>
	  <footer class="w3-container w3-center w3-light-grey w3-text-grey">
	    <p>Sistema Integrado de Aplicaciones, SIA.<br>{{ date('Y') }}</p>
	  </footer>
	</div>
	</div>

	{{-- Modal Actualizacion de datos --}}
	<div id="modalUpdate" class="w3-modal">
	<div class="w3-modal-content">
	  <header class="w3-container w3-center w3-{{ $sistema->sistema_colorclass }}"> 
	    <span onclick="document.getElementById('modalUpdate').style.display='none'" 
	    class="w3-button w3-display-topright">&times;</span>
	    <h2><span class="fa fa-info-circle fa-fw"></span> Atención</h2>
	  </header>
	  <div class="w3-container w3-center w3-padding-16">
	    <p>Al guardar los cambios se creará la solicitud para actualizar los datos en su cuenta de correo institucional.<br><br>¿Desea continuar?</p>
	    <button type="submit" id="guardar_formulario" class="w3-btn w3-{{$sistema->sistema_colorclass}}" form="form-update-account"><span class="fa fa-check fa-fw"></span> Sí</button>
	    <a href="javascript:voidd(0)" class="w3-btn w3-red" onclick="document.getElementById('modalUpdate').style.display='none'"><span class="fa fa-remove fa-fw"></span> Cerrar</a>
	  </div>
	  <footer class="w3-container w3-center w3-light-grey w3-text-grey">
	    <p>Sistema Integrado de Aplicaciones, SIA.<br>{{ date('Y') }}</p>
	  </footer>
	</div>
	</div>
</div>
@endsection

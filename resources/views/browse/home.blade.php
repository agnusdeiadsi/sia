@extends('browse.layouts.app')

@section('title')
	SIA | Home
@endsection
@section('content')
@php
	$auth = \App\Model\Correos\Cuenta::find(Auth::user()->id);

	extract($_REQUEST);
@endphp
<style type="text/css">
	#descripcion, #titulo {
	    width:90%;
	    overflow:hidden;
	    white-space:nowrap;
	    text-overflow: ellipsis;
	}
</style>
<div class="w3-row w3-hide-small w3-hide-medium">
	<div class="w3-col s12 m12 l12 w3-center">
		<h1><img src="{{ asset('images/servicios-titulo.png') }}" width="100%"></h1>
	</div>
</div>
<div class="w3-container">
	<h1 class="w3-text-2017-navy-peony w3-hide-large"><i class="fa fa-th-large fa-fw"></i><b>Servicios</b></h1>
	@if(isset($buscar))
		<b><span class="fa fa-filter fa-fw"></span>Filtro</b>: <span class="w3-grey w3-padding">{{$buscar}}</span>
	@endif
	<div class="w3-center w3-hide-large">
		{!! Form::open(['route' => ['browse.home'], 'method' => 'get']) !!}
			<button type="submit" class="w3-bar-item w3-button w3-2017-navy-peony w3-right"><span class="fa fa-search"></span></button>
	    	<input type="text" name="buscar" class="w3-input w3-white w3-border-2017-navy-peony w3-col s10 w3-right" maxlength="20" placeholder="Buscar" required>
	    {!! Form::close() !!}
    </div>

    <div class="w3-center">
		<div class="w3-bar">
			{{$sistemas->render()}}
		</div>
	</div>

	@if(empty($sistemas->toArray()))
		<i>No se encontraron registros.</i>
	@else
		@foreach ($sistemas->where('enabled', '=', '1')->chunk(3) as $sistemas_chunk)
			<!--Menu for large screens-->
			<div class="w3-row-padding w3-hide-small w3-hide-medium">
			@foreach ($sistemas_chunk as $sistema)
				<div class="w3-col s4 w3-hover-opacity">
					<a href="{{ route(strtolower($sistema->sistema_nombre_corto), $sistema->sistema_id) }}" class="w3-text-grey w3-image w3-round-xlarge" style="text-decoration: none;">
						<div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
							<div class="w3-col m3 l3 w3-center">
								<div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-bottom-left-radius: 15px;">
									<img src="{{ asset('/images/icon/'.$sistema->sistema_icon) }}" width="100" class="w3-image">
								</div>
							</div>
							<div class="w3-col m9 l9">
								<div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}" style="border-top-right-radius: 15px;">
									<div class="w3-col m12 l12" style="padding:0px 5px 0px 15px;">
										<h4 id="titulo"><b>{{$sistema->sistema_nombre}}</b></h4>
									</div>
								</div>
								<div class="w3-row w3-text-dark-grey">
									<div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
										<p style="line-height: 1;" id="descripcion">{{$sistema->sistema_descripcion}}</p>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			@endforeach
			</div>
			<br>
		@endforeach
		
		<!--Menu for small/medium screens-->
		@foreach ($sistemas as $sistema)
			{{-- small --}}
			<div class="w3-row w3-hide-medium w3-hide-large">
				<div class="w3-col s12 w3-hover-opacity">
					<a href="{{ route(strtolower($sistema->sistema_nombre_corto), $sistema->sistema_id) }}" class="w3-text-grey w3-image w3-round-xlarge" style="text-decoration: none;">
						<div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
							<div class="w3-col m3 l3 w3-center">
								<div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-top-right-radius: 15px;">
									<img src="{{ asset('/images/icon/'.$sistema->sistema_icon) }}" width="100" class="w3-image">
								</div>
							</div>
							<div class="w3-col m9 l9">
								<div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}">
									<div class="w3-col m12 l12" style="padding:0px 5px 0 15px;">
										<h4><b>{{$sistema->sistema_nombre}}</b></h4>
									</div>
								</div>
								<div class="w3-row w3-text-dark-grey">
									<div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
										<p>{{$sistema->sistema_descripcion}}</p>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<br class="w3-hide-medium w3-hide-large">

			{{-- medium --}}
			<div class="w3-row w3-hide-small w3-hide-large">
				<div class="w3-col s12 w3-hover-opacity">
					<a href="{{ route(strtolower($sistema->sistema_nombre_corto), $sistema->sistema_id) }}" class="w3-text-grey w3-image w3-round-xlarge" style="text-decoration: none;">
						<div class="w3-row w3-border w3-round-xlarge" style="background-color: #dadad9;">
							<div class="w3-col m3 l3 w3-center">
								<div class="w3-{{$sistema->sistema_colorclass}}" style="padding: 10px; border-top-left-radius: 15px; border-bottom-left-radius: 15px;">
									<img src="{{ asset('/images/icon/'.$sistema->sistema_icon) }}" width="100" class="w3-image">
								</div>
							</div>
							<div class="w3-col m9 l9">
								<div class="w3-row w3-light-grey w3-text-{{$sistema->sistema_colorclass}}" style="border-top-right-radius: 15px;">
									<div class="w3-col m12 l12" style="padding:0px 5px 0 15px;">
										<h4><b>{{$sistema->sistema_nombre}}</b></h4>
									</div>
								</div>
								<div class="w3-row w3-text-dark-grey">
									<div class="w3-col m12 l12" style="padding:5px 5px 0 15px;">
										<p>{{$sistema->sistema_descripcion}}</p>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<br class="w3-hide-small w3-hide-large">
		@endforeach
	@endif
	<div class="w3-center">
		<div class="w3-bar">
			{{$sistemas->render()}}
		</div>
	</div>
</div>
@endsection

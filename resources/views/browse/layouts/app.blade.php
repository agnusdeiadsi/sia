<!DOCTYPE html>
<html lang="es">
<title>
    @yield('title')
</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
<meta name="theme-color" content="#ffffff">

<!-- w3-css -->
<link href="{{ asset('w3css/w3-v4.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/w3.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-camo.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-food.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-highway.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-safety.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-signal.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-vivid.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-2017.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/colors/w3-colors-windows.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/w3-theme-blue-grey.css') }}" rel="stylesheet"/>
<link href="{{ asset('w3css/css.css?family=Open+Sans') }}" rel="stylesheet"/>
<link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet"/>
<!-- CSRF Token -->
<meta content="{{ csrf_token() }}" name="csrf-token"/>
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
{{--<link href="{{ asset('css/compras.css') }}" rel="stylesheet"/>--}}
<link href="{{ asset('breadcrumbs/breadcrumbs.css') }}" rel="stylesheet"/>
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
    a.w3-bar-item {text-decoration: none; color: inherit;}
    a.w3-bar-item:link {color: inherit;}
    a.w3-btn {text-decoration: none; color: inherit;}
    a.w3-btn:link {color: inherit;}
</style>
<!-- Scripts -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

{{-- Sweetalert --}}
<link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.min.css') }}">
<script type="text/javascript" src="{{ asset('sweetalert/js/sweetalert-1-1-3.min.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/modulo.js') }}"></script>
{{--<script src="{{ asset('js/compras.js') }}"></script>--}}
<script src="{{ asset('js/certificados.js') }}"></script>
<script src="{{ asset('js/erc.js') }}"></script>


<!--cuerpo-->
<body onload="cargarCertificado(tipo_id.value); displayNewInputs(tipo_id.value);">
<!-- logo sia -->
<div class="w3-hide-small w3-hide-medium" style="position: absolute; z-index:1;">
    <center>
        <a class="w3-text-white" href="{{url('/browse/home')}}" style="text-decoration: none;">
            <img class="w3-image" src="{{ asset('images/sia-superior-home.png') }}" width="320">
        </a>
    </center>
</div>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" id="myOverlay" onclick="w3_close()" style="cursor:pointer" title="Cerrrar Menú">
</div>

<!-- !PAGE CONTENT! -->
<div class="w3-main w3-animate-top-">
    <!-- Header for large screens -->
    <header class="w3-hide-small w3-hide-medium">
        <a href="#">
            <img class="w3-right w3-margin w3-hide-large" src="{{asset('images/sia.png')}}" style="width:65px;"/>
        </a>
        <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()">
            <i class="fa fa-bars">
            </i>
        </span>
        <div class="w3-container" style="background-color: #e8e8e6;">
            @php
                $auth = \App\Model\Correos\Cuenta::find(Auth::user()->id);
            @endphp
            <div class="w3-text-2017-navy-peony" style="position: absolute; z-index: 1; width: 100%; margin-top: 10px;">
                <center><h3>Bienvenido <b>{{$auth->cuenta_primer_nombre.' '.$auth->cuenta_primer_apellido.' '.$auth->cuenta_segundo_apellido}}</b></h3></center>
            </div>

            <div class="w3-row" style="margin-top: 10px; position: relative; z-index: 2;">
                <div class="w3-col m12 l12 w3-margin-top">
                    <div class="w3-dropdown-click w3-hover-none w3-right">
                        <a class="w3-text-2017-navy-peony w3-hover-opacity" href="javascript:void(0);" onclick="myFunction('largeiconmenu')">
                            <span class="fa fa-user-circle fa-fw fa-3x">
                            </span>
                        </a>
                        <div class="w3-dropdown-content w3-bar-block w3-border w3-2017-navy-peony" id="largeiconmenu" style="right: 0; z-index: 4; width: 240px;">
                            <a class="w3-bar-item w3-button" href="{{route('browse.site.usuarios.perfil')}}" onclick="myFunction('largeiconmenu')">
                                <i class="fa fa-user fa-fw w3-margin-right">
                                </i>
                                Perfil
                            </a>
                            @php
                              $directorio = \App\Sistema::where('sistema_nombre_corto', '=', 'directorio')->first();
                            @endphp
                            <a class="w3-bar-item w3-button" href="{{route('browse.'.$directorio->sistema_nombre_corto.'.extensiones.directory', $directorio)}}" onclick="myFunction('largeiconmenu')">
                                <i class="fa fa-address-book fa-fw w3-margin-right">
                                </i>
                                Directorio
                            </a>
                            @if(Auth::user()->rol_sia == 'Administrador')
                            <a class="w3-bar-item w3-button" href="{{route('browse.site.home')}}" onclick="myFunction('largeiconmenu')">
                                <i class="fa fa-cogs fa-fw w3-margin-right">
                                </i>
                                Administración del Sitio
                            </a>
                            @endif
                            <a class="w3-bar-item w3-button w3-padding" href="{{ url('/browse/logout') }}" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-fw w3-margin-right">
                                </i>
                                Cerrar Sesión
                            </a>
                            <form action="{{ url('/browse/logout') }}" id="logout-form" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                    <!-- scope to search for services -->
                    {!! Form::open(['route' => ['browse.home'], 'method' => 'get']) !!}
                    <button class="w3-bar-item w3-button w3-medium w3-2017-navy-peony w3-mobile w3-right" type="submit">
                        <span class="fa fa-search">
                        </span>
                    </button>
                    <input class="w3-bar-item w3-input w3-medium w3-white w3-mobile w3-right w3-col m2 l2 w3-border-2017-navy-peony" maxlength="20" name="buscar" placeholder="Buscar" required="" type="text"/>
                        {!! Form::close() !!}
                </div>
            </div>
            {{-- <hr class="w3-grey" style="height: 1px;"/> --}}
            <br>
            <br>
        </div>
    </header>

    <!--Header for small screens -->
    <header class="w3-hide-large">
        <div class="w3-row-padding w3-2017-navy-peony">
            <div class="w3-col s8">
                <a href="{{ url('/browse/home') }}">
                    <img class="w3-margin w3-hide-large" src="{{asset('images/sia-small.png')}}" style="width:80px;"/>
                </a>
            </div>
            <div class="w3-col s4 w3-margin-top text-right">
                <div class="w3-dropdown-click w3-hover-none">
                    <a class="w3-text-white w3-hover-opacity" href="javascript:void(0);" onclick="myFunction('shorticonmenu')">
                        <span class="fa fa-user-circle fa-fw fa-2x">
                        </span>
                    </a>
                    <div class="w3-dropdown-content w3-bar-block w3-border" id="shorticonmenu" style="right: 0; z-index: 4; width: 300px;">
                        <a class="w3-bar-item w3-button" href="{{route('browse.site.usuarios.perfil')}}" onclick="myFunction('shorticonmenu')">
                            <i class="fa fa-user fa-fw w3-margin-right">
                            </i>
                            Perfil
                        </a>
                        @php
                            $directorio = \App\Sistema::where('sistema_nombre_corto', '=', 'directorio')->first();
                        @endphp
                        <a class="w3-bar-item w3-button" href="{{route('browse.'.$directorio->sistema_nombre_corto.'.extensiones.directory', $directorio)}}" onclick="myFunction('shorticonmenu')">
                            <i class="fa fa-address-book fa-fw w3-margin-right">
                            </i>
                            Directorio
                        </a>
                        @if(Auth::user()->rol_sia == 'Administrador')
                        <a class="w3-bar-item w3-button" href="{{route('browse.site.home')}}" onclick="myFunction('shorticonmenu')">
                            <i class="fa fa-cogs fa-fw w3-margin-right">
                            </i>
                            Administración del Sitio
                        </a>
                        @endif
                        <a class="w3-bar-item w3-button w3-padding" href="{{ url('/browse/logout') }}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out fa-fw w3-margin-right">
                            </i>
                            Cerrar Sesión
                        </a>
                        <form action="{{ url('/browse/logout') }}" id="logout-form" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <br class="w3-hide-small">
    
    @if($errors->any())
        @php
          Alert::error($errors->all(), 'Error')->html()->persistent('Cerrar');
        @endphp
    @endif

    {{--Alerta de errores--}}
    <div class="w3-container">
        @if(count($errors) > 0)
        <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
            <p>
                @foreach($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
                @endforeach
            </p>
        </div>
        @endif
    </div>
    {{--Fin alerta de errores--}}

    <!--muestra mensajes de flash-->
    @include('flash::message')
    
    <!-- muestra mensajes de sweet alert -->
    @include('sweet::alert')

    <!-- Servicios -->
    @yield('content')
    <br/>
    
    <!-- Footer for large screens -->
    <div class="w3-hide-medium w3-hide-small" style="position: relative; bottom: -10vh; width: 100%;">
        <footer class="w3-container w3-padding" style="background-color: #006894; background-image: url({{ asset('images/bg-footer.png') }});">
            <div class="w3-row-padding">
                <div class="w3-col s12">
                    <center>
                        <img class="w3-image" src="{{ asset('images/logo-blanco.png') }}"/>
                    </center>
                </div>
            </div>
        </footer>
        <div class="w3-2017-navy-peony w3-center w3-padding">
            <p>
                Área TIC - Vicerrectoría Académica | Fundación Universitaria Católica Lumen Gentium | Desarrollado por Agnusdei ADSI | Template Powered by
                <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">
                    w3.css
                </a>
                | {{ date('Y') }}
            </p>
            <p>
                Contáctenos Campus Pance, Cali, Valle / soporte@apps.unicatolica.edu.co / Teléfono: (+57) 2 3120038 ext. 1052
            </p>
        </div>
    </div>

    <!-- Footer for small screens -->
    <div class="w3-hide-large">
        <footer class="w3-container w3-padding" style="background-color: #006894; background-image: url({{ asset('images/bg-footer.png') }});">
            <div class="w3-row-padding">
                <div class="w3-col s12">
                    <center>
                        <img class="w3-image" src="{{ asset('images/logo-blanco.png') }}"/>
                    </center>
                </div>
            </div>
        </footer>
        <div class="w3-2017-navy-peony w3-center w3-padding">
            <p>
                Área TIC - Vicerrectoría Académica | Fundación Universitaria Católica Lumen Gentium | Desarrollado por Agnusdei ADSI | Template Powered by
                <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">
                    w3.css
                </a>
                | {{ date('Y') }}
            </p>
            <p>
                Contáctenos Campus Pance, Cali, Valle / soporte@apps.unicatolica.edu.co / Teléfono: (+57) 2 3120038 ext. 1052
            </p>
        </div>
    </div>
</div>
<!-- End page content -->
<!-- If using flash()->important() or flash()->overlay(), you'll need to pull in the JS for Twitter Bootstrap. -->
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
    $('#flash-overlay-modal').modal();
</script>
<script>
    // Script to open and close sidebar
function w3_open() {
    document.getElementById("small-header").style.display = "none";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
    document.getElementById("small-header").style.display = "block";
}

function myFunction(menu) {
    var x = document.getElementById(menu);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
</body>
</html>

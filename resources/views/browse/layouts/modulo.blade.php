<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title-modulo')</title>

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->
    <link href="{{ asset('css/egresados.css') }}" rel="stylesheet">
    <link href="{{ asset('css/grados.css') }}" rel="stylesheet">
    <link href="{{ asset('css/egresados/multiple-select.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/compras.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/w3-v4.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-camo.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-food.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-highway.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-safety.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-signal.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-vivid.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-2017.css') }}" rel="stylesheet">
    <link href="{{ asset('w3css/colors/w3-colors-windows.css') }}" rel="stylesheet">

    <link href="{{ asset('breadcrumbs/breadcrumbs.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('w3css/css.css?family=Open+Sans') }}" rel="stylesheet">--}}    

    <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
    </script>

    <!-- If using flash()->important() or flash()->overlay(), you'll need to pull in the JS for Twitter Bootstrap. -->
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    {{-- Sweetalert --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.min.css') }}">
    <script type="text/javascript" src="{{ asset('sweetalert/js/sweetalert-1-1-3.min.js') }}"></script>

    <style>
      body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif;}
      a.w3-bar-item {text-decoration: none; color: inherit;}
      a.w3-bar-item:link {color: inherit;}
      a.w3-btn {text-decoration: none; color: inherit;}
      a.w3-btn:link {color: inherit;}
    </style>
</head>

<body onload="displayNewInputs(document.getElementById('tipo_certificado').value); return false" id="body-modulo">
  <!--Header for small screens. Always first at top -->
  <header class="w3-hide-large" id="myNavbar">
    <div class="w3-row-padding w3-2017-navy-peony">
      <div class="w3-col s8">
        <a href="{{ url('/browse/home') }}"><img src="{{asset('images/sia-small.png')}}" style="width:65px;" class="w3-margin w3-hide-large"></a>
      </div>

      <div class="w3-col s4 w3-margin-top">
        <div class="w3-dropdown-click w3-hover-none w3-right">
          <a href="javascript:void(0);" onclick="myFunction('shorticonmenu')" class="w3-text-white w3-hover-text-grey"><span class="fa fa-user-circle fa-fw fa-2x"></span></a>
          <div id="shorticonmenu" class="w3-dropdown-content w3-bar-block w3-border" style="right: 0; z-index: 4; width: 300px;">
            <a href="{{route('browse.site.usuarios.perfil')}}" onclick="myFunction('shorticonmenu')" class="w3-bar-item w3-button"><i class="fa fa-user fa-fw w3-margin-right"></i>Perfil</a>
            @php
              $directorio = $sistemas->where('sistema_nombre_corto', '=', 'directorio')->first();
            @endphp
            <a href="{{route('browse.'.$directorio->sistema_nombre_corto.'.extensiones.directory', $directorio)}}" onclick="myFunction('shorticonmenu')" class="w3-bar-item w3-button"><i class="fa fa-address-book fa-fw w3-margin-right"></i>Directorio</a>
            @if(Auth::user()->rol_sia == 'Administrador')
              <a href="{{route('browse.site.home')}}" onclick="myFunction('shorticonmenu')" class="w3-bar-item w3-button"><i class="fa fa-cogs fa-fw w3-margin-right"></i>Administración del Sitio</a>
            @endif
            <a href="{{ url('/browse/logout') }}"
              onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" class="w3-bar-item w3-button w3-padding">
              <i class="fa fa-sign-out fa-fw w3-margin-right"></i>Cerrar Sesión
            </a>
            <form id="logout-form" action="{{ url('/browse/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
        </div>

        <div class="w3-dropdown-click w3-hover-none w3-right">
          <a href="javascript:void(0);" onclick="myFunction('servicios')" class="w3-text-white w3-hover-text-grey">
            <span class="fa fa-th fa-fw fa-2x"></span>
          </a>
          <div id="servicios" class="w3-dropdown-content w3-bar-block w3-border w3-padding" style="right: 0; z-index: 4; width: 300px; max-height: 300px; overflow: auto;">
            @foreach($sistemas->chunk(4) as $servicios)
              <div class="w3-row">
                @foreach($servicios as $servicio)
                    <div class="w3-col s3 w3-{{$servicio->sistema_colorclass}} w3-hover-opacity w3-padding">
                      <a href="{{route(strtolower($servicio->sistema_nombre_corto), $servicio->sistema_id)}}" title="{{$servicio->sistema_nombre}}">
                        <img src="{{asset('images/icon/'.$servicio->sistema_icon)}}" class="w3-image">
                      </a>
                    </div>
                @endforeach
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </header>

  {{-- @if (Auth::guest())
    <script>
      window.location.href='browse.auth.login';
    </script>

    <li><a href="{{ route('login') }}">Login</a></li>
    <li><a href="{{ route('register') }}">Register</a></li>
  @else --}}

  <!-- Module Menu -->
  <div class="w3-hide-large w3-{{$sistema->sistema_colorclass}}" style="z-index: 3; padding: 0px;" id="mySidenavHeader">
    <div class="w3-row">
      <div class="w3-col s2" style="background-size: inherit; min-height: 150px; z-index: -1;">
        <a class="w3-bar-item w3-btn w3-hover-none w3-{{$sistema->sistema_colorclass}}" onclick="w3_open();" style="transform: rotate(-90deg); margin-top: 50px; font-size: 20px;"><b>MENÚ</b> <i class="fa fa-caret-down"></i></a>
      </div>

      <div class="w3-col s10 w3-center w3-{{$sistema->sistema_colorclass}}">
        <img src="{{ asset('images/icon/'.$sistema->sistema_icon) }}" width="100"><br><br>
        <p class="w3-center" style="word-wrap: break-word;"><b>{{$sistema->sistema_nombre}}</b><br/>
        {{Auth::user()->rol_modulo}}</p>
        <!--<form id="form-buscar" action="default.php?id=buscar" method="post" role="form">
          <input type="text" name="q" class="w3-input w3-pull-left" placeholder="Buscar" required=""><button type="submit" class="w3-btn w3-green w3-pull-left"><span class="fa fa-search fa-fw"></span></button>
        </form>-->
      </div>
    </div>
  </div>

  <nav class="w3-sidebar w3-collapse w3-{{$sistema->sistema_colorclass}}" style="z-index: 3; width:240px;" id="mySidenav"><br>
    <div class="w3-container w3-row">
      <div class="w3-col s12 w3-center">
        <img src="{{ asset('images/icon/'.$sistema->sistema_icon) }}" width="100"><br><br>
        <p class="w3-center"><b>{{ $sistema->sistema_nombre }}</b><br />
        {{Auth::user()->rol_modulo}}</p>
      </div>
    </div>
    <hr>
    <div class="w3-bar-block">
      <a href="javascript:void(0)" class="w3-bar-item w3-button w3-animate-opacity w3-padding-16 w3-hide-large w3-dark-grey w3-hover-light-grey" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Cerrar Menú</a>
    </div>

    <div class="w3-bar-block">
      <a href="{{ url('/browse/home') }}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-th-large fa-fw"></i> Servicios</a>
      <a href="{{ route($sistema->sistema_nombre_corto, $sistema) }}" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-home fa-fw"></i> Inicio</a>
    </div>

    {{--Option module menu--}}
    @yield('menu-modulo')
    {{--End option module menu--}}

    <div class="w3-bar-block">
      <a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey" onclick="accordion('accordion-manuales')"><i class="fa fa-file-text-o fa-fw"></i>  Manuales <span class="fa fa-caret-down"></span></a>
      <div id="accordion-manuales" class="w3-hide">
        @if($sistema->sistema_manual != null)
          <a href="{{route('browse.site.sistemas.manual.readmanual', $sistema)}}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey w3-hide-small"><span class="fa fa-caret-right fa-fw"></span> Leer Manual</a>
          <a href="{{url('browse/site/sistemas/download/manual', [$sistema])}}" class="w3-bar-item w3-button w3-padding w3-hover-light-grey"><span class="fa fa-caret-right fa-fw"></span> Descargar Manual</a>
        @else
          <a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-remove fa-fw"></i>No existen manuales</a>
        @endif
      </div>
      <a href="javascript:void(0)" onclick="document.getElementById('modalContacto').style.display='block'" class="w3-bar-item w3-button w3-padding w3-hover-grey"><i class="fa fa-envelope fa-fw"></i> Contacto</a>
    </div>
  <!-- End module menu -->
  </nav>
  <div class="w3-container">
    @if($sistema)
    <div id="modalContacto" class="w3-modal w3-animate-opacity">
      <div class="w3-modal-content w3-card-4">
        <header class="w3-container w3-{{ $sistema->sistema_colorclass }} w3-center"> 
          <span onclick="document.getElementById('modalContacto').style.display='none'" 
          class="w3-button w3-large w3-display-topright">&times;</span>
          <h3><span class="fa fa-envelope fa-fw"></span> Contacto</h3>
        </header>
        <div class="w3-container w3-padding-16 w3-center">
          <p>Para mayor información acerca del módulo puedes comunicarte con <b>{{ $sistema->subarea->subarea_nombre }}</b>. En el Directorio IP podrás encontrar los correos electrónicos y teléfonos del área.</p>
          <a href="{{ route('browse.'.$directorio->sistema_nombre_corto.'.extensiones.directory', $directorio) }}" class="w3-btn w3-{{ $sistema->sistema_colorclass }}"><span class="fa fa-address-book fa-fw"></span> Directorio</a>
        </div>
        <footer class="w3-container w3-light-grey w3-center w3-padding">
          <p>SIA - Sistema Integrado de Aplicaciones<br>{{ date('Y') }}</p>
        </footer>
      </div>
    </div>
    @endif
  </div>

  @php
    $auth = \App\Model\Correos\Cuenta::find(Auth::user()->id);
  @endphp
  <!-- Header for large screens -->
  <header class="w3-hide-small w3-hide-medium">
    <a href="javascript:void(0)"><img src="{{asset('images/sia-small.png')}}" style="width:65px;" class="w3-right w3-margin w3-hide-large"></a>
    <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i class="fa fa-bars"></i></span>
    <div class="w3-row">
      <div class="w3-col s12">
        <div class="w3-dropdown-click w3-hover-none w3-right w3-margin-top">
          <a href="javascript:void(0);" onclick="myFunction('largeiconmenu')" class="w3-text-grey w3-hover-text-{{$sistema->sistema_colorclass}}"><span class="fa fa-user-circle fa-fw fa-2x"></span></a>
          <div id="largeiconmenu" class="w3-dropdown-content w3-bar-block w3-border" style="right: 0; z-index: 4; width: 240px;">
            <a href="{{route('browse.site.usuarios.perfil')}}" onclick="myFunction('largeiconmenu')" class="w3-bar-item w3-button"><i class="fa fa-user fa-fw w3-margin-right"></i>{{ $auth->cuenta_primer_nombre }} | Perfil</a>
            @php
              $directorio = $sistemas->where('sistema_nombre_corto', '=', 'directorio')->first();
            @endphp
            <a href="{{route('browse.'.$directorio->sistema_nombre_corto.'.extensiones.directory', $directorio)}}" onclick="myFunction('largeiconmenu')" class="w3-bar-item w3-button"><i class="fa fa-address-book fa-fw w3-margin-right"></i>Directorio</a>
            @if(Auth::user()->rol_sia == 'Administrador')
              <a href="{{route('browse.site.home')}}" onclick="myFunction('largeiconmenu')" class="w3-bar-item w3-button"><i class="fa fa-cogs fa-fw w3-margin-right"></i>Administración del Sitio</a>
            @endif
            <a href="{{ url('/browse/logout') }}"
              onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" class="w3-bar-item w3-button w3-padding">
              <i class="fa fa-sign-out fa-fw w3-margin-right"></i>Cerrar Sesión
            </a>
            <form id="logout-form" action="{{ url('/browse/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
        </div>

        <div class="w3-dropdown-click w3-hover-none w3-right w3-margin-top">
          <a href="javascript:void(0);" onclick="myFunction('servicios_large')" class="w3-text-grey w3-hover-text-{{$sistema->sistema_colorclass}}"><span class="fa fa-th fa-fw fa-2x"></span></a>
          <div id="servicios_large" class="w3-dropdown-content w3-bar-block w3-border w3-padding" style="right: 0; z-index: 4; width: 240px; max-height: 300px; overflow: auto;">
            @foreach($sistemas->chunk(4) as $servicios)
              <div class="w3-row">
                @foreach($servicios as $servicio)
                    <div class="w3-col s3 w3-{{$servicio->sistema_colorclass}} w3-hover-opacity w3-padding">
                      <a href="{{route(strtolower($servicio->sistema_nombre_corto), $servicio->sistema_id)}}" title="{{$servicio->sistema_nombre}}">
                        <img src="{{asset('images/icon/'.$servicio->sistema_icon)}}" class="w3-image">
                      </a>
                    </div>
                @endforeach
              </div>
            @endforeach
          </div>
        </div>
        <!--<h3 class="w3-right w3-margin-top" style="margin-right: 20px;"></h3>-->
      </div>
    </div>
    <hr class="w3-grey" style="height: 1px;">
  </header>

    <!-- Overlay effect when opening sidenav on small screens -->
    <div class="w3-overlay w3-hide-large w3-animate-opacity-" onclick="w3_close()" style="cursor:pointer" title="Cerrar Menú" id="myOverlay"></div>
    <!--END MENU-->


    <!-- !PAGE CONTENT! -->
    <div class="w3-main w3-text-{{$sistema->sistema_colorclass}}" style="margin-left:240px;">
      <div class="w3-container">
          {{--muestra mensajes de flash--}}
          {{--@include('flash::message')--}}

          {{--Error message--}}
          {{--@if($errors->any())
            <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
              <p>
              @foreach($errors->all() as $error)
                <li>
                  {{ $error }}
                </li>
              @endforeach
              </p>
            </div>
           @endif--}}
           {{--End error message--}}

          @if($errors->any())
            @php
              Alert::error($errors->all(), 'Error')->html()->persistent('Cerrar');
            @endphp
          @endif

          <!-- muestra mensajes de sweet alert -->
          @include('sweet::alert')

          {{--content--}}
          <div class="w3-hide-small w3-hide-medium" style="margin-top:10px; min-height: 75vh; height: auto !important; height: 75vh;">
            @yield('content-modulo')
          </div>

          <div class="w3-hide-large" style="margin-top:10px;">
            @yield('content-modulo')
          </div>
          {{--End content--}}
      </div>
      <br>

      {{--Footer for large screens--}}
      <div class="w3-hide-small w3-hide-medium">
        <footer class="w3-container w3-text-dark-grey w3-padding-16" style="background-color: #dadad9;">
          <h4>
            {{ $sistema->sistema_nombre }}
          </h4>
          <p>
            Versión {{ $sistema->sistema_version }} |
            Released {{ $sistema->sistema_released }} |
            Desarrollado por {{ $sistema->sistema_desarrollador }} |
            {{ $sistema->subarea->subarea_nombre }}
          </p>
        </footer>

        <footer class="w3-container w3-grey">
          <p>
            Área TIC - Vicerrectoría Académica | Fundación Universitaria Católica Lumen Gentium
            Template Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
          </p>
        </footer>
      </div>

      {{--Footer for small and medium screens--}}
      <div class="w3-hide-large">
        <footer class="w3-container w3-grey w3-text-dark-grey w3-padding-16">
          <h4>
            {{ $sistema->sistema_nombre }}
          </h4>
          <p>
            Versión {{ $sistema->sistema_version }} |
            Released {{ $sistema->sistema_released }} |
            Desarrollado por {{ $sistema->sistema_desarrollador }} |
            {{ $sistema->subarea->subarea_nombre }}
          </p>
        </footer>

        <footer class="w3-container w3-dark-grey">
          <p>
            Fundación Universitaria Católica Lumen Gentium | Vicerrectoría Académica - Área TIC |
            Template Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
          </p>
        </footer>
      </div>
      {{--Fin footer del módulo--}}

    <!-- END PAGE CONTENT -->
    </div>
    {{--@endif --}}

    <script>
        
    </script>

    <!-- Scripts -->

  <script>
    function myFunction(menu) {
      var x = document.getElementById(menu);
      if (x.className.indexOf("w3-show") == -1) {
          x.className += " w3-show";
      } else { 
          x.className = x.className.replace(" w3-show", "");
      }
    }

    function closeIconMenu(menu) {
      var x = document.getElementById(menu);
      foreach(x)
      {
        x.className = x.className.replace(" w3-show", "");
      }
    }

    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });

    $('#flash-overlay-modal').modal();
  </script>
  <script src="{{asset('js/modulo.js')}}"></script>
  <script src="{{asset('js/compras.js')}}"></script>
  <script src="{{asset('js/erc.js') }}"></script>
  <script src="{{asset('js/certificados.js')}}"></script>
  <script src="{{asset('js/app.js')}}"></script>
  <script src="{{asset('js/egresados/actions.js')}}"></script>
  {{--<script src="{{asset('js/egresados/selects-filters.js')}}"></script>--}}
  <script src="{{asset('js/egresados/multiple-select.js')}}"></script>
  <script src="{{asset('js/egresados/formData.js')}}"></script>
  <script src="{{asset('js/egresados/graphics.js')}}"></script>

  <!-- Highcharts -->
  <script src="{{asset('Highcharts-4.2.1/js/highcharts.js')}}"></script>
  <script src="{{asset('Highcharts-4.2.1/js/highcharts-3d.js')}}"></script>
  <script src="{{asset('Highcharts-4.2.1/js/modules/exporting.js')}}"></script>

  <!-- Grados -->
  <script src="{{asset('js/grados/action.js')}}"></script>

  <!-- Multi select -->
  <script>
        $(".select").multipleSelect({
            placeholder: "Seleccionar una opcion",
            width: 200,
            multipleWidth: 55
        });
  </script>
</body>
</html>

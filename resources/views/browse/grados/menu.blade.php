@extends('browse.layouts.modulo')

@section('title-modulo')
    {{$sistema->sistema_nombre}}
@endsection

@section('menu-modulo')
<div class="w3-bar-block">
   {{-- <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.egresados.reportes.prueba', $sistema) }}">
        <i class="fa fa-line-chart fa-fw">
        </i>
        Prueba
    </a>--}}
    {{--

            sefa = Secretaria de la facultad

    --}}
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Secretaria Facultad')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.sefa.index', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Secretaria Facultad
        </a>
    @endif
    
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Biblioteca')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.biblioteca.index', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Biblioteca
        </a>
    @endif
 
    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Egresados')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.egresados.index', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Egresados
        </a>
    @endif

    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Director programa')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.director.baseindex', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Director programa
        </a>
    @endif

    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Director programa')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.reaca.index', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Registro Academico
        </a>
    @endif

    @if(Auth::user()->rol_sia == 'Administrador' || Auth::user()->rol_modulo == 'Cartera')
        <a class="w3-bar-item w3-button w3-padding w3-hover-grey" href="{{ route('browse.grados.cartera.indexFinan', $sistema) }}">
            <i class="fa fa-search fa-fw">
            </i>
            Cartera
        </a>
    @endif
   
</div>
@endsection

@section('content-modulo')
<center>
    <h1>
        <b>
            {{$sistema->sistema_nombre}}
        </b>
    </h1>
</center>
<hr>
    {{--<div class="w3-container">
        <center>
            <h3>
                <b>
                    Aquí podrás:
                </b>
            </h3>
        </center>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-1">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-01.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-02.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-03.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-04.png') }}"/>
                </center>
            </div>
            <div class="col-sm-2">
                <center>
                    <img class="w3-image" src="{{ asset('images/menu-icon/egresados/icon-05.png') }}"/>
                </center>
            </div>
        </div>
    </div>--}}
    @endsection
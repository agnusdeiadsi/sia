@extends('browse.grados.base')

@section('content-page')
	{{--dd(Auth::user()->cuenta->subarea->area_id)--}}
	<br>
	<div class="container">
		<br>
		<div class="row">
			{!! Form::open(['route' => ['browse.grados.sefa.search', $sistema], 
										'method' => 'post',
										'class' => 'form']) !!}
			
					<div class="col-sm-4">
						<button type="submit" 
								class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
								<span class="fa fa-search"></span>
						</button>
			        	<input type="text" 
			        			name="codigo" 
			        			class="w3-input w3-white w3-border-{{$sistema->sistema_colorclass}} w3-col s10 w3-right" maxlength="20" 
			        			placeholder="Codigo ID" 
			        			required>
			    	</div>
			  	
			{!! Form::close() !!}


			{!! Form::open(['route' => ['browse.grados.egresados.pospro', $sistema], 
										'method' => 'post',
										'class' => 'form']) !!}
				
				<div class="col-sm-6 w3-right">

					<button class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
						<span class="fa fa-filter"></span>
					</button>
					
					{!! Form::select('programa', 
										[], 
										null, 
										['class' => 'w3-input w3-white w3-border-'.$sistema->sistema_colorclass.' w3-col s10 w3-right',
											'placeholder' => 'Seleccione un programa',
											'required',
											'id' => 'select-program-general']) !!}
				</div>
			
			{!!Form::close()!!}
		</div>

	<hr>
	<table class="w3-table w3-border w3-bordered w3-white">
		<thead>
			<tr class="w3-{{ $sistema->sistema_colorclass }}">
				<th>
					# Solicitud
				</th>
				{{--<th>
					Identificación
				</th>--}}
				<th>
					Nombres
				</th>
				<th>
					Apellidos
				</th>
				<th>
					Codigo
				</th>
				<th>
					Programa
				</th>
				<th>
					Estado
				</th>
				<th>
					Ver
				</th>
			</tr>
		</thead>

		<tbody>

			@if($postulantes->count() > 0)
				@foreach($postulantes as $postulante)
					@if($postulante->subarea_id == 71)
						<tr class="w3-pale-{{$postulante->estado_color}}">
					@endif
						<td>
							{{$postulante->solicitud_id}}	
						</td>
						{{--<th>
							{{$postulante->identificacion_numero}}
						</th>--}}
						<td>
							{{$postulante->matriculado_primer_nombre." ".$postulante->matriculado_segundo_nombre}}	
						</td>
						<td>
							{{$postulante->matriculado_primer_apellido." ".$postulante->matriculado_segundo_apellido}}	
						</td>
						<td>
							{{$postulante->academico_codigo}}
						</td>
						<td>
							{{$postulante->programa_nombre}}
						</td>
						@if($postulante->subarea_id == 71)
							<td>
								{{$postulante->estado_nombre}}	
							</td>
						@else
							<td>
									
							</td>
						@endif
						<td>
							<a class="w3-btn" href="{{route('browse.grados.egresados.search-table', 
														[$sistema, 'postulante' => $postulante->matriculado_pidm])
													}}" {{--class="w3-btn w3-{{$sistema->sistema_colorclass}}--}}">
								<span class="fa fa-eye fa-fw"></span></a>
						</td>
					</tr>
				@endforeach
			@endif
			
		</tbody>
	</table>

	<div class="row">
		<div class="col-sm-12">
			<center>
				{{ $postulantes->links() }}		
			</center>
		</div>
	</div>
	
	<div class="loader" hidden=""></div>

	</div>

	{{--dd($postulantes)--}}

@endsection
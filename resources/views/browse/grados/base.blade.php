@extends('browse.grados.menu')


@section('content-modulo')
	
	<div class="w3-row">
		<div class="col-sm-12">
			<center>
				<h2><label class="w3-text-{{$sistema->sistema_colorclass}}">Búsqueda de Postulante</label></h2>
				<br>
				<img width="200px" src="{{ asset('images/Busqueda-color.png')}}">
			</center>
		</div>
	</div>

	<div class="container">

		@yield('content-page')
	
	</div>

	<div class="loader" hidden=""></div>

@endsection
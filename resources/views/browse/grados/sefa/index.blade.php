@extends('browse.grados.base')

@section('content-page')
	{{--dd(Auth::user()->cuenta->subarea->area_id)--}}
	<br>
	@php
		$area = \App\Area::where('area_id', Auth::user()->cuenta->subarea->area_id)->get();
	@endphp
	<div class="container">
		{!! Form::open(['route' => ['browse.grados.sefa.search', $sistema], 
									'method' => 'post',
									'class' => 'form']) !!}
			<div class="row">
				<div class="col-sm-4">
					<button type="submit" 
							class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
							<span class="fa fa-search"></span>
					</button>
		        	<input type="text" 
		        			name="codigo" 
		        			class="w3-input w3-white w3-border-{{$sistema->sistema_colorclass}} w3-col s10 w3-right" maxlength="20" 
		        			placeholder="Codigo ID" 
		        			required>
		    	</div>
		  	</div>
		{!! Form::close() !!}

	<hr>
	<table class="w3-table w3-border w3-bordered w3-white">
		<thead>
			<tr class="w3-{{ $sistema->sistema_colorclass }}">
				<th>
					# Solicitud
				</th>
				{{--<th>
					Identificación
				</th>--}}
				<th>
					Nombres
				</th>
				<th>
					Apellidos
				</th>
				<th>
					Codigo
				</th>
				<th>
					Programa
				</th>
				<th>
					Estado
				</th>
				<th>
					Ver
				</th>
			</tr>
		</thead>

		<tbody>
			@if(isset($postulantes) && $postulantes->count() > 0)
				@foreach($postulantes as $postulante)
					
					@if($area->count() > 0 && $area->first()->area_codigo == $postulante->facultad_codigo)

						<tr class="w3-pale-{{$postulante->estado_color}}">
							<td>
								{{$postulante->solicitud_id}}	
							</td>
							{{--<th>
								{{$postulante->identificacion_numero}}
							</th>--}}
							<td>
								{{$postulante->matriculado_primer_nombre." ".$postulante->matriculado_segundo_nombre}}	
							</td>
							<td>
								{{$postulante->matriculado_primer_apellido." ".$postulante->matriculado_segundo_apellido}}	
							</td>
							<td>
								{{$postulante->academico_codigo}}
							</td>
							<td>
								{{$postulante->programa_nombre}}
							</td>
							<td>
								{{$postulante->estado_nombre}}	
							</td>
							<td>
								<a class="w3-btn" href="{{route('browse.grados.sefa.search-table', [$sistema, 'postulante' => $postulante->matriculado_pidm])}}" {{--class="w3-btn w3-{{$sistema->sistema_colorclass}}--}}">
									<span class="fa fa-eye fa-fw"></span></a>
							</td>
						</tr>
					@endif
				@endforeach
			@endif
		</tbody>
	</table>

	<div class="row">
		<div class="col-sm-12">
			<center>
				{{ $postulantes->links() }}		
			</center>
		</div>
	</div>

	<div class="loader" hidden=""></div>
	
	</div>

	{{--dd($postulantes)--}}

@endsection

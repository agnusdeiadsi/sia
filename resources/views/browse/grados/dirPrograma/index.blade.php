@extends('browse.grados.base')

@section('content-page')
	{{--dd(Auth::user()->cuenta->subarea->area_id)--}}
	<br>
	<div class="container">
		<div class="row">
			{!! Form::open(['route' => ['browse.grados.sefa.search', $sistema], 
										'method' => 'post',
										'class' => 'form']) !!}
			
					<div class="col-sm-4">
						<button type="submit" 
								class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
								<span class="fa fa-search"></span>
						</button>
			        	<input type="text" 
			        			name="codigo" 
			        			class="w3-input w3-white w3-border-{{$sistema->sistema_colorclass}} w3-col s10 w3-right" maxlength="20" 
			        			placeholder="Codigo ID" 
			        			required>
			    	</div>
			  	
			{!! Form::close() !!}


			{!! Form::open(['route' => ['browse.grados.director.pospro', $sistema], 
										'method' => 'post',
										'class' => 'form']) !!}
				
				<div class="col-sm-6 w3-right">

					<button class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
						<span class="fa fa-filter"></span>
					</button>
					
					{!! Form::select('programa', 
										$programas->pluck('programa_nombre', 'programa_id'), 
										null, 
										['class' => 'w3-input w3-white w3-border-'.$sistema->sistema_colorclass.' w3-col s10 w3-right',
											'placeholder' => 'Seleccione un programa',
											'required',
											'id' => 'select-program-director']) !!}
				</div>
			
			{!!Form::close()!!}
		</div>
	<hr>
	<table class="w3-table w3-border w3-bordered w3-white">
		<thead>
			<tr class="w3-{{ $sistema->sistema_colorclass }}">
				<th>
					# Solicitud
				</th>
				{{--<th>
					Identificación
				</th>--}}
				<th>
					Nombres
				</th>
				<th>
					Apellidos
				</th>
				<th>
					Codigo
				</th>
				<th>
					Programa
				</th>
				<th>
					Estado
				</th>
				<th>
					Ver
				</th>
			</tr>
		</thead>

		<tbody>
			{{--dd($postulantes)--}}
			@if(isset($postulantes) && $postulantes->count() > 0)
				@foreach($postulantes as $postulante)
					@if($postulante->subarea_id == 104)
						<tr class="w3-pale-{{$postulante->estado_color}}">
					@endif
						<td>
							{{$postulante->solicitud_id}}	
						</td>
						{{--<th>
							{{$postulante->identificacion_numero}}
						</th>--}}
						<td>
							{{$postulante->matriculado_primer_nombre." ".$postulante->matriculado_segundo_nombre}}	
						</td>
						<td>
							{{$postulante->matriculado_primer_apellido." ".$postulante->matriculado_segundo_apellido}}	
						</td>
						<td>
							{{$postulante->academico_codigo}}
						</td>
						<td>
							{{$postulante->programa_nombre}}
						</td>
						@if($postulante->subarea_id == 104)
							<td>
								{{$postulante->estado_nombre}}	
							</td>
						@else
							<td>
									
							</td>
						@endif
						<td>
							<a class="w3-btn" href="{{route('browse.grados.dirPrograma.search-table', 
														[$sistema, 'postulante' => $postulante->matriculado_pidm])
													}}" {{--class="w3-btn w3-{{$sistema->sistema_colorclass}}--}}">
								<span class="fa fa-eye fa-fw"></span></a>
						</td>
					</tr>
				@endforeach

			@else
				<td colspan="7">
					<i>No existen resultados</i>	
				</td>
			
			@endif
			
		</tbody>
	</table>

	@if(isset($postulantes))
		<div class="row">
			<div class="col-sm-12">
				<center>
					{{ $postulantes->links() }}		
				</center>
			</div>
		</div>
	@endif
	
	<div class="loader" hidden=""></div>

	</div>

	{{--dd($postulantes)--}}

	<script type="text/javascript">
		
		var url = '/sia'
		$.post(url+'/public/api/grados/'+<?php echo Auth::user()->cuenta->subarea_id;?>+'/programaDirector', 
			function (data) 
		{

		    var html_select = '<option value="" hidden="">Seleccione un programa</option>';

		    html_select += "<option value='all'>Todos los Programas</option>";

		    for (var i = 0; i < data.length; i++) 
		    {

		    	html_select += '<option value="'+data[i].programa_id+'">'+data[i].programa_nombre+' - '+data[i].convenio_nombre+'</option>';

		    }

		    $('#select-program-director').html(html_select);

		});
	</script>

@endsection
@extends('browse.grados.base')

@section('content-page')
	
	<div class="container">

		<div class="row">
			{!! Form::open(['route' => ['browse.grados.sefa.search', $sistema], 
										'method' => 'post',
										'class' => 'form']) !!}
			
					<div class="col-sm-4">
						<button type="submit" 
								class="w3-bar-item w3-btn w3-{{$sistema->sistema_colorclass}} w3-right">
								<span class="fa fa-search"></span>
						</button>
			        	<input type="text" 
			        			name="codigo" 
			        			class="w3-input w3-white w3-border-{{$sistema->sistema_colorclass}} w3-col s10 w3-right" maxlength="20" 
			        			placeholder="Codigo ID" 
			        			required>
			    	</div>
			  	
			{!! Form::close() !!}

		</div>	
		<hr>
		{{--@foreach($solicitudes as $solicitud)
			@php

				$postulante = App\Academico::where('');

			@endphp
		@endforeach--}}
		{{--dd($solicitud->first()->matriculado->matriculado_primer_nombre)--}}
		@if(isset($solicitud) && $solicitud->count() > 0)
			@php

				$generic_ = '/sia/public/storage';

			@endphp
			{{--dd(isset($solicitud) && $solicitud->count() > 0 ? $solicitud->first()->matriculado->matriculado_primer_nombre : '')--}}
			{{--dd($solicitud->first()->archivos)--}}
			<br>
			<div class="row w3-pale-{{$sistema->sistema_colorclass}}" style="border-radius: 4px;">
				<div class="col-sm-12">
					<h4 class="w3-text-{{$sistema->sistema_colorclass}} 
								w3-panel 
								{{--w3-pale-{{$sistema->sistema_colorclass}} --}}
								w3-leftbar 
								w3-border-{{$sistema->sistema_colorclass}}">
								<b>1. Información postulante</b></h4>
				</div>
			</div>
			<br>
			<div class="w3-row">
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Nombres
					</label>
					{!!Form::text('nombres', 
									isset($solicitud) && $solicitud->count() > 0 ? 
														$solicitud->first()->matriculado->matriculado_primer_nombre.' '.
														$solicitud->first()->matriculado->matriculado_segundo_nombre.' ' 
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}

				</div>

				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Apellidos
					</label>
					{!!Form::text('nombres', 
									isset($solicitud) && $solicitud->count() > 0 ? 
														$solicitud->first()->matriculado->matriculado_primer_apellido.' '.
														$solicitud->first()->matriculado->matriculado_segundo_apellido.' ' 
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
				
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Documento
					</label>
					{!!Form::text('numDocumento', 
									isset($solicitud) && $solicitud->count() > 0 ? 
														$solicitud->first()->matriculado->identificacion
														->sortByDesc('created_at')->first()->identificacion_numero 
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
				
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Codigo ID
					</label>
					{!!Form::text('sede', 
									isset($solicitud) && $solicitud->count() > 0 
									&& $solicitud->first()->matriculado->academico->count() > 0 ? 
														$solicitud->first()->matriculado
														->academico->sortByDesc('created_at')
														->first()->academico_codigo
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
			</div>
			<br>

			<div class="w3-row">
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Facultad
					</label>
					{!!Form::text('facultad', 
									isset($solicitud) && $solicitud->count() > 0 
									&& $solicitud->first()->matriculado->academico->count() > 0 ? 
														$solicitud->first()->matriculado
														->academico->sortByDesc('created_at')
														->first()->programa->facultad->facultad_nombre
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Programa
					</label>
					{!!Form::text('programa', 
									isset($solicitud) && $solicitud->count() > 0 
									&& $solicitud->first()->matriculado->academico->count() > 0 ? 
														$solicitud->first()->matriculado
														->academico->sortByDesc('created_at')
														->first()->programa->programa_nombre
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Sede
					</label>
					{!!Form::text('sede', 
									isset($solicitud) && $solicitud->count() > 0 
									&& $solicitud->first()->matriculado->academico->count() > 0 ? 
														$solicitud->first()->matriculado
														->academico->sortByDesc('created_at')
														->first()->sede->sede_nombre
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
				<div class="col-3 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<label class="w3-text-{{$sistema->sistema_colorclass}}">
						Periodo de finalización
					</label>
					{!!Form::text('periodo', 
									isset($solicitud) && $solicitud->count() > 0 
									&& $solicitud->first()->matriculado->academico->count() > 0 ? 
														$solicitud->first()->matriculado
														->academico->sortByDesc('created_at')
														->first()->academico_fecha_finalizacion
													  : '', 
									['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass.' w3-text-grey input-text',
									 'disabled'])!!}
				</div>
			</div>
			<br>
			<div class="row w3-pale-{{$sistema->sistema_colorclass}}" style="border-radius: 4px;">
				<div class="col-sm-12">
					<h4 class="w3-text-{{$sistema->sistema_colorclass}} 
								w3-panel 
								{{--w3-pale-{{$sistema->sistema_colorclass}} --}}
								w3-leftbar 
								w3-border-{{$sistema->sistema_colorclass}}">
								<b>2. Documentos adjuntos</b></h4>
				</div>
			</div>
			<br>
			@if(isset($solicitud) && $solicitud->first()->archivos->count() > 0)
				@foreach($solicitud->first()->archivos->chunk(2) as $chuck)
					<div class="row">
						@foreach($chuck as $archivo)
							<div class="col-sm-6">			
								<li>
									<a href="{{$generic_.$archivo->solicitudArchivos_ruta}}
											/{{$archivo->solicitudArchivos_nombre}}.{{$archivo->solicitudArchivos_tipo}}"
									target="_blanck">
									<b>{{$archivo->documento->solicitudDocumento_nombre}}</b>
									</a>
								</li>
								<br>
							</div>
						@endforeach
					</div>
				@endforeach
			@else
				<div class="row">
					<div class="col-sm-12">
						No existen archivos adjuntos
					</div>
				</div>
			@endif

			<br>
			<div class="row w3-pale-{{$sistema->sistema_colorclass}}" style="border-radius: 4px;">
				<div class="col-sm-12">
					<h4 class="w3-text-{{$sistema->sistema_colorclass}} 
								w3-panel 
								{{--w3-pale-{{$sistema->sistema_colorclass}} --}}
								w3-leftbar 
								w3-border-{{$sistema->sistema_colorclass}}">
								<b>3. Estado de solicitud</b></h4>
				</div>
			</div>
			<br>
			{{--@if(isset($solicitud) && $solicitud->count() > 0)
				@foreach($solicitud->first()->faseEstado as $fase)
					@if($fase->area_id == 1)
					@endif
				@endforeach	
				dd($solicitud->first()->faseEstado)
			@endif--}}
			{!! Form::open(['route' => ['browse.grados.postulacion.estado', $sistema], 
							'method' => 'post',
							'class' => 'form']) !!}

				@if(isset($solicitud) && $solicitud->count() > 0)
					<div class="row">
						<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
							<label class="w3-text-{{$sistema->sistema_colorclass}}">
								Estado de la solicitud
								<span style="color: red">
									*
								</span>
							</label>
							{!! Form::hidden('solicitud', $solicitud->first()->solicitud_id)!!}
							{!! Form::hidden('area', 'reaca')!!}
							{!! Form::hidden('email', $solicitud->first()->matriculado->matriculado_email) !!}
			                {!! Form::select('estado', 
			                				$estados->pluck('estado_nombre', 'estado_id'), 
			                				null, 
			                				['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass,
			                				'placeholder' => 'Selecione',
			                				'id' => 'estado',
			                				'required']) !!}
			                				<br>
						</div>

						<div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9" id="description" hidden="">
							<label>
								Descripción
								<span style="color: red">
									*
								</span>
							</label>
							{!! Form::textArea('description', 
												null, 
												['class' => 'w3-input w3-text-grey w3-border-'.$sistema->sistema_colorclass,
												'id' => 'description-txa',
												'style' => 'height:95px',
												'maxlength' => '200']) !!}
						</div>

					</div>
					<br>
					<div class="w3-row">
						 <div class="col-3">
							<button class="w3-btn w3-{{$sistema->sistema_colorclass}}">
				            	<span class="fa fa-check fa-fw"></span>
				            	Enviar
				            </button>
						 </div>
					</div>
				@else 
					<div class="row">
						<div class="col-sm-3">
							<label class="w3-text-{{$sistema->sistema_colorclass}}">
								Estado de la solicitud
							</label>
			                {!! Form::select('estado', 
			                				$estados->pluck('estado_nombre', 'estado_id'), 
			                				null, 
			                				['class' => 'w3-input w3-border-'.$sistema->sistema_colorclass,
			                				'placeholder' => '',
			                				'disabled',
			                				'required']) !!}
			                				<br>
			            <button class="w3-btn w3-{{$sistema->sistema_colorclass}}" disabled="">
			            	<span class="fa fa-check fa-fw"></span>
			            	Enviar
			            </button>
						</div>
					</div>
				@endif

			{!! Form::close()!!}

		@endif
	
	<div class="loader" hidden=""></div>

	</div>

	<script type="text/javascript">

		$('#estado').change(function () 
		{
			
			var estado = $(this).val()

			if (estado == 2) 
			{

				$('#description').show('slow')
				$('#description-txa').prop('required',true)
			
			} else {

				$('#description').hide('slow')
				$('#description-txa').removeAttr('required')
					
			}

		})

	</script>
@endsection

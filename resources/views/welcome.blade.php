<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Refresh" content="2; url={{url('browse/login')}}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- favicon -->
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />

        <!-- w3-css -->
        <link rel="stylesheet" href="{{ asset('w3css/w3.css') }}">
        <link rel="stylesheet" href="{{ asset('w3css/w3-v4.css') }}">
        <link rel="stylesheet" href="{{ asset('w3css/w3-theme-blue-grey.css') }}">
        <link rel='stylesheet' href="{{ asset('w3css/css.css?family=Open+Sans') }}">
        <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">

        <title>Sistema Integrado de Aplicaciones</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

          /* my style */
          @media (min-width: 1400px){
              body, .navbar{
              /*background-image: url('{{ asset('images/1680x1050.png') }}'); color: white;*/
              background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
              background-size: cover;
              }

              .navbar{
                  text-decoration: none;
              }

              .navbar li:hover{
                  color: inherit;
              }

              .logo{
                  width: 500px;
              }

              .content{
                  margin-top: 3em;
              }
          }

          @media (max-width: 1400px){
              body{
              /*background-image: url('{{ asset('images/1680x1050.png') }}'); color: white;*/
              background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
              background-size: cover;
              }

              .logo{
                  width: 350px;
              }

              .content{
                  margin-top: 3em;
              }
          }

          @media (max-width: 320px){
              body{
              /*background-image: url('{{ asset('images/768x1024.png') }}'); color: white;*/
              background-image: url('{{ asset('images/fondo-animado.gif') }}'); color: white;
              background-size: cover;
              }

              .logo{
                  width: 280px;
              }

              .content{
                  margin-top: 3em;
              }
        }
        .logo-navbar{
            width: 10%;
            padding: 0;
            margin: 0;
        }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
          {{--<div class="top-right links">
            <a href="{{url('browse/login')}}">Iniciar Sesión</a>
          </div>--}}
            {{--@if (Route::has('browse/login'))
                <div class="top-right links">
                    @if (Auth::guard('browse')->check())
                        <a href="{{ url('/home') }}">Home</a>
                        <script>
                          window.location.href='/browse';
                        </script>
                    @else
                        <a href="{{ url('browse/login') }}">Iniciar Sesión</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif--}}

            <div class="content">
                <img src="{{asset('images/sia.png')}}" class="logo w3-image" alt="logo">
                <p><i class="fa fa-spinner w3-spin fa-3x"></i></p>
                <div class="links">
                    <b>Fundación Universitaria Católica Lumen Gentium</b><br>
                    Sistema Integrado de Aplicaciones - SIA<br>
                    {{ date('Y') }}
                </div>
            </div>
        </div>
    </body>
</html>
